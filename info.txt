Node version: v0.12.2

Cordova version: 5.1.1

Config.xml file: 

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<widget
  xmlns="http://www.w3.org/ns/widgets"
  xmlns:tools="http://schemas.android.com/tools"
  xmlns:android="http://schemas.android.com/apk/res/android"
  id="io.cordova.LoveStamp"
  version="0.1.6"
>

  <access origin="*" />
  <access origin="http://127.0.0.1*" />

  <name>LoveStamp</name>
  <description>
    LoveStamp is your guide to businesses that appreciate you. They stamp your
    phone, you get “bits”. Use these via ChangeTip to tip others, access
    digital content, get items from around the world and much more.
  </description>

  <config-file target="*-Info.plist" parent="NSLocationAlwaysUsageDescription">
    <string>LoveStamp provides fans with real-time geotracking to identify participating merchants.</string>
  </config-file>

  <author href="http://lovestamp.io" email="hello@lovestamp.io">LoveStamp</author>
  <preference name="BackgroundColor" value="0xffffffff"/>

  <preference name="DisallowOverscroll" value="true" />
  <preference name="orientation" value="portrait" />
  <preference name="MediaPlaybackRequiresUserAction" value="false"/>
  <preference name="KeyboardDisplayRequiresUserAction" value="false"/>
  <preference name="SuppressesIncrementalRendering" value="true"/>
  <preference name="UIWebViewDecelerationSpeed" value="fast" />
  <preference name="BackupWebStorage" value="none" />

  <preference name="webviewbounce" value="false" />
  <preference name="UIWebViewBounce" value="false" />

  <preference name="prerendered-icon" value="true" />
  <preference name="ios-statusbarstyle" value="transparent" />
  <preference name="detect-data-types" value="true" />
  <preference name="exit-on-suspend" value="false" />

  <preference name="disable-cursor" value="false" />

  <feature name="SplashScreen">
      <param name="ios-package" value="CDVSplashScreen" />
  </feature>

  <preference name="SplashScreen" value="screen" />
  <preference name="SplashScreenDelay" value="4000" />

  <feature name="StatusBar">
    <param name="ios-package" value="CDVStatusBar" onload="true" />
  </feature>

  <access origin="http://*.segment.com" />
  <access origin="https://*.segment.com" />

  <access origin="http://*.segment.io" />
  <access origin="https://*.segment.io" />

  <access origin="http://googleapis.com" />
  <access origin="https://googleapis.com" />

  <access origin="http://fonts.googleapis.com" />
  <access origin="https://fonts.googleapis.com" />


  <access origin="http://*.cloudflare.com" />
  <access origin="https://*.cloudflare.com" />

  <access origin="http://*.stripe.com" />
  <access origin="https://*.stripe.com" />

  <access origin="http://*.cloudfront.net"></access>
  <access origin="https://*.cloudfront.net"></access>

  <access origin="http://google-analytics.com"></access>
  <access origin="https://google-analytics.com"></access>

  <access origin="http://www.google-analytics.com"></access>
  <access origin="https://www.google-analytics.com"></access>

  <access origin="https://*.mxpnl.com"></access>
  <access origin="http://*.mxpnl.com"></access>

  <access origin="http://*.tile.openstreetmap.org"></access>
  <access origin="https://*.tile.openstreetmap.org"></access>

  <access origin="http://*.openstreetmap.org"></access>
  <access origin="https://*.openstreetmap.org"></access>

  <access origin="http://*.mixpanel.com"></access>
  <access origin="https://*.mixpanel.com"></access>

  <access origin="http://*.gstatic.com"></access>
  <access origin="https://*.gstatic.com"></access>

  <access origin="http://*.cartocdn.com"></access>
  <access origin="https://*.cartocdn.com"></access>

  <access origin="http://*.basemaps.cartocdn.com"></access>
  <access origin="https://*.basemaps.cartocdn.com"></access>

  <access origin="http://*.firebaseio.com"></access>
  <access origin="https://*.firebaseio.com"></access>

  <access origin="http://lovestamp.firebaseio.com"></access>
  <access origin="https://lovestamp.firebaseio.com"></access>

  <access origin="http://*.firebase.com"></access>
  <access origin="https://*.firebase.com"></access>

  <access origin="http://auth.firebase.com"></access>
  <access origin="https://auth.firebase.com"></access>

  <allow-intent href="*.openstreetmap.org" />
  <allow-intent href="*.tiles.openstreetmap.org" />
  <allow-intent href="*.firebaseio.com" />
  <allow-intent href="auth.firebase.com" />

  <icon src="./app/resources/ios/icon76x2.png" />
  <plugin name="cordova-plugin-whitelist" version="1" />

  <allow-intent href="http://*/*" />
  <allow-intent href="https://*/*" />
  <allow-intent href="tel:*" />
  <allow-intent href="sms:*" />
  <allow-intent href="mailto:*" />
  <allow-intent href="geo:*" />

  <access origin="tel:*" launch-external="yes" />
  <access origin="geo:*" launch-external="yes" />
  <access origin="mailto:*" launch-external="yes" />
  <access origin="sms:*" launch-external="yes" />
  <access origin="market:*" launch-external="yes" />

  <platform name="ios">

    <preference name="FadeSplashScreen" value="false"/>
    <preference name="FadeSplashScreenDuration" value="2"/>
    <preference name="ShowSplashScreenSpinner" value="false"/>

    <preference name="Fullscreen" value="true" />
    <content src="index-device-enabled.html" />
    <allow-intent href="itms:*" />
    <allow-intent href="itms-apps:*" />
    <preference name="StatusBarStyle" value="darkcontent" />
    <icon height="57" src="./app/resources/ios/icon57.png" width="57" />
    <icon height="72" src="./app/resources/ios/icon72.png" width="72" />
    <icon height="114" src="./app/resources/ios/icon57x2.png" width="114" />
    <icon height="120" src="./app/resources/ios/ipad-ipadmini-120px.png" width="120" />
    <icon height="144" src="./app/resources/ios/icon72x2.png" width="144" />

    <splash height="480" src="./app/resources/ios/screen/iphone6-splash--320-480.png" width="320" />
    <splash height="960" src="./app/resources/ios/screen/iphone6-splash--640-960.png" width="640" />
    <splash height="1024" src="./app/resources/ios/screen/iphone6-splash--768-1024.png" width="768" />
    <splash height="768" src="./app/resources/ios/screen/iphone6-splash--1024-768.png" width="1024" />
    <splash src="./app/resources/ios/screen/iphone6--375-677.png" width="375"  height="667" />
    <splash src="./app/resources/ios/screen/iphone6--375-677x2.png" width="750"  height="1334" />
    <splash src="./app/resources/ios/screen/iphone6-splash--1242-2208.png" width="1242" height="2208" />
    <splash src="./app/resources/ios/screen/iphone6-splash--640-1136.png" width="640" height="1136" />

  </platform>

  <platform name="android">

    <config-file target="AndroidManifest.xml" parent="/*">

      <manifest
        xmlns:android="http://schemas.android.com/apk/res/android"
        package="io.cordova.LoveStamp"
        android:versionCode="2"
        android:versionName="1"
        android:installLocation="preferExternal" />

      <supports-screens
        android:anyDensity="true"
        android:resizeable="true"
        android:smallScreens="true"
        android:normalScreens="true"
        android:largeScreens="true"
        android:xlargeScreens="true"
      />

      <uses-permission android:name="android.permission.INTERNET" />
      <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
      <?ignore uses-permission android:name="android.permission.READ_CONTACTS" android:maxSdkVersion="22" / ?>
      <?ignore uses-permission android:name="android.permission.WRITE_CONTACTS" / ?>
      <?ignore uses-permission android:name="android.permission.GET_ACCOUNTS" / ?>
      <uses-permission android:name="android.permission.USE_CREDENTIALS" />

      <intent-filter>
        <action android:name="android.intent.action.VIEW" />
        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />
        <data android:scheme="file" />
        <data android:mimeType="*/*" />
        <data android:pathPattern=".*\\.txt" />
        <data android:host="*" />
      </intent-filter>
      <intent-filter>
        <action android:name="android.intent.action.VIEW" />
        <category android:name="android.intent.category.DEFAULT" />
        <data android:scheme="content" />
        <data android:pathPattern=".*\\.txt" />
        <data android:mimeType="*/*" />
      </intent-filter>
    </config-file>

    <preference name="android-minSdkVersion" value="15" />
    <preference name="android-maxSdkVersion" value="22" />
    <?ignore preference name="android-targetSdkVersion" value="22" / ?>

    <preference name="Fullscreen" value="false" />
    <preference name="android-windowSoftInputMode" value="adjustResize" />
    <preference name="webView" value="org.crosswalk.engine.XWalkWebViewEngine"/>

    <preference name="xwalkVersion" value="org.xwalk:xwalk_core_library_beta:10+" />
    <preference name="xwalkVersion" value="xwalk_core_library_beta:10+" />
    <preference name="xwalkVersion" value="10+" />
    <preference name="xwalkVersion" value="10" />


    <content src="index-device-android-enabled.html" />
    <allow-intent href="market:*" />
    <preference name="android-installLocation" value="auto" />

    <preference name="android-installLocation" value="auto" />
    <preference name="android-launchMode" value="singleTop" />
    <preference name="android-activity-hardwareAccelerated" value="true" />
    <preference name="android-manifest-hardwareAccelerated" value="true" />
    <preference name="android-configChanges" value="orientation" />
    <preference name="android-theme" value="@android:style/Theme.Black.NoTitleBar" />

    <splash density="port-ldpi" src="./app/resources/android/screen/splash--320-426.png" />
    <splash density="port-mdpi" src="./app/resources/android/screen/splash--320-470.png" />
    <splash density="port-hdpi" src="./app/resources/android/screen/splash--480-640.png" />
    <splash density="port-xhdpi" src="./app/resources/android/screen/splash--720-960.png" />

    <icon density="ldpi" src="./app/resources/android/icon-36-ldpi.png" />
    <icon density="mdpi" src="./app/resources/android/icon-48-mdpi.png" />
    <icon density="hdpi" src="./app/resources/android/icon-72-hdpi.png" />
    <icon density="xhdpi" src="./app/resources/android/icon-96-xhdpi.png" />

    <icon src="./app/resources/android/icon/drawable-ldpi-icon.png" density="ldpi"/>
    <icon src="./app/resources/android/icon/drawable-mdpi-icon.png" density="mdpi"/>
    <icon src="./app/resources/android/icon/drawable-hdpi-icon.png" density="hdpi"/>
    <icon src="./app/resources/android/icon/drawable-xhdpi-icon.png" density="xhdpi"/>
    <icon src="./app/resources/android/icon/drawable-xxhdpi-icon.png" density="xxhdpi"/>
    <icon src="./app/resources/android/icon/drawable-xxxhdpi-icon.png" density="xxxhdpi"/>
    <splash src="./app/resources/android/splash/drawable-land-ldpi-screen.png" density="land-ldpi"/>
    <splash src="./app/resources/android/splash/drawable-land-mdpi-screen.png" density="land-mdpi"/>
    <splash src="./app/resources/android/splash/drawable-land-hdpi-screen.png" density="land-hdpi"/>
    <splash src="./app/resources/android/splash/drawable-land-xhdpi-screen.png" density="land-xhdpi"/>
    <splash src="./app/resources/android/splash/drawable-land-xxhdpi-screen.png" density="land-xxhdpi"/>
    <splash src="./app/resources/android/splash/drawable-land-xxxhdpi-screen.png" density="land-xxxhdpi"/>
    <splash src="./app/resources/android/splash/drawable-port-ldpi-screen.png" density="port-ldpi"/>
    <splash src="./app/resources/android/splash/drawable-port-mdpi-screen.png" density="port-mdpi"/>
    <splash src="./app/resources/android/splash/drawable-port-hdpi-screen.png" density="port-hdpi"/>
    <splash src="./app/resources/android/splash/drawable-port-xhdpi-screen.png" density="port-xhdpi"/>
    <splash src="./app/resources/android/splash/drawable-port-xxhdpi-screen.png" density="port-xxhdpi"/>
    <splash src="./app/resources/android/splash/drawable-port-xxxhdpi-screen.png" density="port-xxxhdpi"/>
  </platform>

</widget>


Plugins: 

de.appplant.cordova.plugin.background-mode,de.appplant.cordova.plugin.local-notification,org.apache.cordova.device,org.transistorsoft.cordova.background-geolocation

Android platform:

Available Android targets:
----------
id: 1 or "android-17"
     Name: Android 4.2.2
     Type: Platform
     API level: 17
     Revision: 3
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : default/armeabi-v7a, default/mips, default/x86
----------
id: 2 or "android-18"
     Name: Android 4.3.1
     Type: Platform
     API level: 18
     Revision: 3
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : default/armeabi-v7a, default/x86
----------
id: 3 or "android-19"
     Name: Android 4.4.2
     Type: Platform
     API level: 19
     Revision: 4
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : default/armeabi-v7a, default/x86
----------
id: 4 or "android-20"
     Name: Android 4.4W.2
     Type: Platform
     API level: 20
     Revision: 2
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in, AndroidWearRound, AndroidWearSquare, AndroidWearRound, AndroidWearSquare
 Tag/ABIs : android-wear/armeabi-v7a, android-wear/x86
----------
id: 5 or "android-21"
     Name: Android 5.0.1
     Type: Platform
     API level: 21
     Revision: 2
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in, AndroidWearRound, AndroidWearSquare, AndroidWearRound, AndroidWearSquare
 Tag/ABIs : android-tv/armeabi-v7a, android-tv/x86, android-wear/armeabi-v7a, android-wear/x86, default/armeabi-v7a, default/x86, default/x86_64
----------
id: 6 or "android-22"
     Name: Android 5.1.1
     Type: Platform
     API level: 22
     Revision: 2
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : android-tv/armeabi-v7a, android-tv/x86, default/armeabi-v7a, default/x86, default/x86_64
----------
id: 7 or "android-MNC"
     Name: Android M (Preview)
     Type: Platform
     API level: MNC
     Revision: 2
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : android-tv/x86, default/arm64-v8a, default/armeabi-v7a, default/mips, default/x86, default/x86_64
----------
id: 8 or "Google Inc.:Google APIs:17"
     Name: Google APIs
     Type: Add-On
     Vendor: Google Inc.
     Revision: 4
     Description: Android + Google APIs
     Based on Android 4.2.2 (API level 17)
     Libraries:
      * com.google.android.media.effects (effects.jar)
          Collection of video effects
      * com.android.future.usb.accessory (usb.jar)
          API for USB Accessories
      * com.google.android.maps (maps.jar)
          API for Google Maps
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : default/armeabi-v7a
----------
id: 9 or "Google Inc.:Google APIs:18"
     Name: Google APIs
     Type: Add-On
     Vendor: Google Inc.
     Revision: 4
     Description: Android + Google APIs
     Based on Android 4.3.1 (API level 18)
     Libraries:
      * com.google.android.media.effects (effects.jar)
          Collection of video effects
      * com.android.future.usb.accessory (usb.jar)
          API for USB Accessories
      * com.google.android.maps (maps.jar)
          API for Google Maps
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : default/armeabi-v7a
----------
id: 10 or "Google Inc.:Glass Development Kit Preview:19"
     Name: Glass Development Kit Preview
     Type: Add-On
     Vendor: Google Inc.
     Revision: 11
     Description: Preview of the Glass Development Kit
     Based on Android 4.4.2 (API level 19)
     Libraries:
      * com.google.android.glass (gdk.jar)
          APIs for Glass Development Kit Preview
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : no ABIs.
----------
id: 11 or "Google Inc.:Google APIs:19"
     Name: Google APIs
     Type: Add-On
     Vendor: Google Inc.
     Revision: 14
     Description: Android + Google APIs
     Based on Android 4.4.2 (API level 19)
     Libraries:
      * com.google.android.media.effects (effects.jar)
          Collection of video effects
      * com.android.future.usb.accessory (usb.jar)
          API for USB Accessories
      * com.google.android.maps (maps.jar)
          API for Google Maps
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : default/armeabi-v7a
----------
id: 12 or "Google Inc.:Google APIs (x86 System Image):19"
     Name: Google APIs (x86 System Image)
     Type: Add-On
     Vendor: Google Inc.
     Revision: 14
     Description: Android x86 + Google APIs
     Based on Android 4.4.2 (API level 19)
     Libraries:
      * com.google.android.media.effects (effects.jar)
          Collection of video effects
      * com.android.future.usb.accessory (usb.jar)
          API for USB Accessories
      * com.google.android.maps (maps.jar)
          API for Google Maps
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : default/x86
----------
id: 13 or "Google Inc.:Google APIs:21"
     Name: Google APIs
     Type: Add-On
     Vendor: Google Inc.
     Revision: 1
     Description: Android + Google APIs
     Based on Android 5.0.1 (API level 21)
     Libraries:
      * com.google.android.media.effects (effects.jar)
          Collection of video effects
      * com.android.future.usb.accessory (usb.jar)
          API for USB Accessories
      * com.google.android.maps (maps.jar)
          API for Google Maps
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in, AndroidWearRound, AndroidWearSquare, AndroidWearRound, AndroidWearSquare
 Tag/ABIs : google_apis/armeabi-v7a, google_apis/x86, google_apis/x86_64
----------
id: 14 or "Google Inc.:Google APIs:22"
     Name: Google APIs
     Type: Add-On
     Vendor: Google Inc.
     Revision: 1
     Description: Android + Google APIs
     Based on Android 5.1.1 (API level 22)
     Libraries:
      * com.google.android.media.effects (effects.jar)
          Collection of video effects
      * com.android.future.usb.accessory (usb.jar)
          API for USB Accessories
      * com.google.android.maps (maps.jar)
          API for Google Maps
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : google_apis/armeabi-v7a, google_apis/x86, google_apis/x86_64

iOS platform:

Xcode 6.4
Build version 6E35b

