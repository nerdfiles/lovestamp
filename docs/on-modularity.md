#Modular Frontend Architectures, Concepts

@see http://www.youtube.com/watch?v=b5pFv9NB9fs&t=3m40s
@see http://www.youtube.com/watch?v=4nU9_SPBk7Y

##Network Performance

We can review network performance considerations here http://opensignal.com/networks/.

Whether we go Full AMD, Optimized AMD, or Compiled JavaScript depends on our 
Network Performance considerations. Today poor network performance in terms 
of latency tends to drive decision making when it comes building Web Applications.

##Why Modularity? Why AMD?

But network performance is a completely different issue from development 
philosophy and practice. Network performance should not be the sole factor as 
to how we develop our Web Applications. The Web, ultimately, is built on a 
substrate; conceptually the underlying layer (OSI, for instance) should not 
dictate the structure of the Web or its parts (Web Applications). The 
philosophical gist is expressed in a fairly interesting definition of the Web: 

    The Web is fundamentally a distributed hypermedia application.

###Modules On-Demand

The mantra we might follow up with is: "on-demand resources", where we 
(supposing FAMD or OAMD) "size and shape" the payload. One immediate benefit 
follows from the question: "Why serve the Contacts module when the user 
has only requested their Accounts Settings module?" It's a simple architectural 
observation that is often overlooked due to the conventionlistic attitude most 
developers assume. 

The idea here is a powerful one. With a modular architecture developers can 
test and develop features through their loosely-coupled modules. What is more, 
features can be integrated into server environments in an isolated way. We 
need not fail the entire build on account of one module, but we can add 
richness and specificity to alert protocols which indicate what "regions" of 
the application are down. That said, accountability and QA can be applied 
to modules, and the developer(s) of that module. Ultimately these points 
inform the concept of "continuous integration", where the application 
matures through the evolution of its modules, but it need not perish on 
account of one member of the species (say stopping the entire build on 
account of minor syntax errors ­ all or nothing architecture).

Since we're not developing in production, or pushing straight to production, 
a modular architecture would enable us to decompress the stress of developing 
as a concerted effort (everyone racing to the finish line at once).

###Module Definition

Typically convention drives development practices and in similar fashion, Web 
Development practices. For small applications developed by small teams 
convention is usually enough. We might see 5-10 ``<script>`` includes all 
intimately known to the intuitions of the development team, or lone developer. 
But what about large teams (5-10)? Or fluid teams? Or future teams?

Without documentation it becomes an interesting question what path one takes 
toward the introduction of a large-scale Web Application. One of the main goals 
involved in incorporating modularity to development practices is that self-
organization is promoted through the application architecture itself. In want 
of documentation, self-organizing code structures present to new developers 
a clear path for participation into a complex codebase. It may be complex, but 
need not be complicated.

###Modular Architecture

Modules should not know about one another, but should interface with a sandbox. 
Loosely coupling modules from one another, but tightly coupling to a central 
interface reflects a common feature of socially self-organizing teams. Developers 
take on stories, and they satisfy their stories to enhance the overall narrative 
that is the Web Application itself. The codebase itself might best serve developers 
if it reflects their own means and provisions of interacting with each other.


