(function() {
  require.config({
    baseUrl: 'scripts/',
    waitSeconds: 30,
    urlArgs: 'v=1',

    /**
    Aliases
     */
    paths: {
      async: 'ext/async',
      font: 'ext/font',
      goog: 'ext/goog',
      image: 'ext/image',
      json: 'ext/json',
      noext: 'ext/noext',
      mdown: 'ext/mdown',
      propertyParser: 'ext/propertyParser',
      markdownConverter: 'ext/Markdown.Converter',
      'chance': './ext/chance',
      'geojson-tools': './ext/geojson-tools.amd',

      /**
      Core Application Materials
       */
      'angular': 'ext/angular.min',
      'loggly.tracker': 'ext/loggly.tracker',
      'loggly.src': 'ext/logglyService',
      'loggly.service': 'ext/logglyService.min',
      'angularAMD': 'ext/angularAMD',
      'ngload': 'ext/ngload',
      'elastic-search': 'ext/elasticsearch.min',

      /**
      SnowShoe Implementation
       */
      'sss-client': 'ext/sss.client',
      'sss-util': 'ext/sss.util',
      'touchy': 'ext/touchy',
      'jquery-json': 'ext/jquery.json-2.4.min',
      'graham-scan': 'ext/grahamScan-modified',
      'stroll': 'ext/stroll',
      'moment': 'ext/moment.min',
      'numeral': 'ext/numeral.min',

      /**
      Visualization Dependency
       */
      'd3': 'ext/d3.min',
      'c3': 'ext/c3.min',

      /**
      Interactive Maps Dependency
       */
      'leaflet': 'ext/leaflet',
      'leaflet-tooltip': 'ext/leaflet.tooltip',
      'leaflet-awesome-markers': 'ext/leaflet.awesome-markers',
      'leaflet-osmbuildings': 'ext/OSMBuildings-Leaflet',
      'leaflet-providers': 'ext/leaflet-providers',
      'leaflet-draw': 'ext/leaflet.draw',
      'leaflet-knn': 'ext/leaflet-knn',
      'leaflet-oms': 'ext/oms',
      'leaflet-label': 'ext/leaflet.label',
      'angular-cordova': 'ext/ng-cordova.min',
      'pushController': 'plugins/push/controller',
      'tangle': 'ext/Tangle',
      'tangle.mootools': 'ext/mootools',
      'tangle.sprintf': 'ext/sprintf',
      'tangle.BVTouchable': 'ext/BVTouchable',
      'tangleKit': 'ext/TangleKit',

      /**
      AngularJS Dependency
       */
      'angular-route': 'ext/angular-route.min',
      'hammer': 'ext/hammer',
      'angular-hammer': 'ext/angular-hammer',
      'angular-local-storage': 'ext/angular-local-storage.min',
      'angular-leaflet-directive': 'ext/angular-leaflet-directive.min',
      'angular-animate': 'ext/angular-animate.min',
      'angular-sanitize': 'ext/angular-sanitize',
      'angular-touch': 'ext/angular-touch',
      'angular-material': 'ext/angular-material.min',
      'angular-aria': 'ext/angular-aria',
      'angular-geolocation': 'ext/angularjs-geolocation.min',
      'angular-twitter': 'ext/ng-twitter',
      'angular-messages': 'ext/angular-messages.min',
      'angular-payments': 'ext/angular-payments.min',
      'angular-infinite-scroll': 'ext/ng-infinite-scroll.min',
      'angular-geotranslation': 'ext/ng.geotranslation',
      'angular-elastic-search': 'ext/elasticsearch.angular',
      'angular-chosen-localytics': 'ext/angular-chosen-localytics',
      'chosen': 'ext/chosen.jquery',

      /**
      Firebase Dependency
       */
      'firebase': 'ext/firebase',
      'angular-fire': 'ext/angularfire',

      /**
      Application Front End Environment Configurations
       */
      'config': 'config/base',

      /**
      Native Application URIs
      
      So there's a Web Dashboard for Users to hit. Any user can log in, create a 
      stamp request, etc. We're deploying to a select for OSes, like Android and 
      iOS, but in general the entire application's source code starts with Web 
      Technologies which are wrapped and deployed via PhoneGap. The User should be 
      able to access her dashboard so long as she has a stamp.
       */
      'routes': 'routes',

      /**
       */
      'filters/date': './filters/dateFilters',

      /**
      Use-case Directives
       */
      'directives/search/simple': './modules/www/directives/simple-search',
      'directives/stats/static': './modules/www/directives/static-stats',
      'directives/authCheck': './directives/authCheck',
      'directives/site/navigation': './modules/www/directives/navigation',
      'directives/bitsDisplayCounter': './directives/bitsDisplayCounter',
      'directives/notification': './directives/notification',
      'directives/loader': './modules/www/controllers/login/directives/loader',

      /**
      Subcontrollers
      
      @description Potential ui-router subroutes.
       */
      'WpHeaderController': './modules/www/controllers/header/controllers/base',

      /**
      'Auth'
      
      Covers data marshalling and user authentication via Social Networks.
       */
      'auth/user/roll-up': './modules/authentication/directives/userRollup',
      'auth/access/conductor': './modules/authorization/directives/accessConductor',
      'auth/user/main': './modules/authentication/directives/main',

      /**
      Application-wide Directives
       */
      'ui/version': './directives/appVersion',
      'ui/customEnter': './directives/customEnter',
      'ui/customLoader': './directives/customLoader',
      'ui/relExternal': './directives/relExternal',

      /**
      Configuration Decorators
       */
      'config/exceptionHandler': './config/exceptionHandler',
      'config/frameBuster': './config/frameBuster',
      'decorators/base': './decorators/base',
      'decorators/prev': './decorators/prev',

      /**
      Client-side Services
       */
      'service/crypto': './services/crypto',
      'services/stamp': './services/stamp',
      'services/payments': './services/payments',
      'services/tip': './services/changeTip',
      'services/profile': './services/profile',
      'services/social': './services/social',
      'services/messaging': './services/messaging',
      'services/reference': './services/reference',
      'services/notification': './services/notification',
      'services/authentication': './services/authentication',
      'services/authorization': './services/authorization',
      'services/user/track': './services/userTrack',
      'services/bank': './services/bank',
      'crypto': './ext/Crypto',
      'crypto.BlockModes': './ext/BlockModes',
      'crypto.AES': './ext/AES',
      'crypto.PBKDF2': './ext/PBKDF2',
      'crypto.HMAC': './ext/HMAC',
      'crypto.SHA1': './ext/SHA1',
      'crypto.MD5': './ext/MD5',
      'lodash': './ext/lodash.min',
      'one-color': './ext/one-color',
      'add-to-homescreen': './ext/addtohomescreen.min',
      'interface': './interface',

      /**
      @deprecated
       */
      'utils/hammer': './utils/hammer',
      'utils/tangle': './utils/tangle',
      'utils/addToHomescreen': './utils/addToHomescreen',
      'utils/firebase': './utils/firebase-generic',
      'utils/firebase/email': './utils/firebase-email',
      'utils/dataDefender': './utils/dataDefender',
      'utils/modalWindow': './utils/modalWindow',
      'utils/googleMaps': './utils/googleMaps',
      'plugins/sss/spin': './ext/spin',
      'plugins/sss/stampsdk': './ext/stampsdk',
      'jquery': './ext/jquery.min',
      'jquery.easing': './ext/jquery.easing.min'
    },

    /**
    Dependency Chain
     */
    shim: {
      'jquery.easing': {
        'deps': ['jquery']
      },
      'chosen': {
        'deps': ['jquery', 'angular']
      },
      'geojson-tools': ['lodash'],
      'touchy': {
        deps: ['sss-client', 'sss-util']
      },
      'jquery': {
        exports: '$'
      },
      'jquery-json': {
        deps: ['jquery']
      },
      'leaflet': {
        deps: ['angular'],
        exports: 'L'
      },
      'angular': {
        'deps': ['jquery'],
        'exports': 'angular'
      },
      'tangle': {
        exports: 'Tangle'
      },
      'hammer': {
        exports: 'Hammer'
      },
      'lodash': {
        exports: '_'
      },
      'angular-payments': ['angular'],
      'angular-chosen-localytics': ['chosen'],
      'angular-infinite-scroll': ['jquery', 'angular'],
      'angular-geolocation': ['angular'],
      'angular-fire': ['firebase', 'angular'],
      'angular-aria': ['angular'],
      'angular-hammer': ['hammer'],
      'angular-material': ['angular', 'angular-aria', 'angular-animate', 'utils/hammer'],
      'angular-route': ['angular'],
      'angular-local-storage': ['angular'],
      'leaflet-osmbuildings': ['angular-leaflet-directive'],
      'leaflet-oms': ['angular-leaflet-directive'],
      'leaflet-label': ['angular-leaflet-directive'],
      'leaflet-providers': ['angular-leaflet-directive'],
      'leaflet-awesome-markers': ['angular-leaflet-directive'],
      'leaflet-draw': ['angular-leaflet-directive'],
      'leaflet-tooltip': ['angular-leaflet-directive'],
      'leaflet-knn': {
        deps: ['angular-leaflet-directive'],
        exports: 'leafletKnn'
      },
      'angular-leaflet-directive': ['leaflet'],
      'angularAMD': ['angular'],
      'ngload': ['angularAMD'],
      'angular-animate': ['angular'],
      'angular-cookies': ['angular'],
      'angular-resource': ['angular'],
      'angular-sanitize': ['angular'],
      'angular-messages': ['angular'],
      'angular-touch': ['angular'],
      'angular-elastic-search': ['angular', 'elastic-search'],
      'loggly.service': ['angular'],
      'interface': ['config', 'angularAMD'],
      'c3': ['d3'],
      'decorators/base': ['angular'],
      'decorators/prev': ['angular'],
      'config/exceptionHandler': ['angular'],
      'crypto': {
        exports: 'CryptoJS'
      },
      'crypto.AES': {
        exports: 'CryptoJS',
        deps: ['crypto', 'crypto.BlockModes', 'crypto.PBKDF2', 'crypto.HMAC', 'crypto.SHA1', 'crypto.MD5']
      },
      'crypto.MD5': {
        exports: 'CryptoJS',
        deps: ['crypto']
      },
      'crypto.BlockModes': {
        exports: 'CryptoJS',
        deps: ['crypto']
      },
      'crypto.PBKDF2': {
        exports: 'CryptoJS',
        deps: ['crypto']
      },
      'crypto.HMAC': {
        exports: 'CryptoJS',
        deps: ['crypto']
      },
      'crypto.SHA1': {
        exports: 'CryptoJS',
        deps: ['crypto']
      }
    },
    priority: ['angular'],
    deps: ['config/frameBuster', 'interface']
  });

}).call(this);
