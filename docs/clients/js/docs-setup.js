NG_DOCS={
  "sections": {
    "tutorial": "Onboarding",
    "client": "Thin Client Documentation"
  },
  "pages": [],
  "apis": {
    "tutorial": false,
    "client": false
  },
  "html5Mode": true,
  "editExample": true,
  "startPage": "/docs/clients",
  "discussions": {
    "shortName": "slack",
    "url": "https://lovestamp.slack.com",
    "dev": false
  },
  "scripts": [
    "boot.js"
  ]
};