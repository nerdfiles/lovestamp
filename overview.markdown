#Front-end Architecture

##High-level Overview

    Gruntconfig.json

    Contains a metadescription of application assets paths and environment variables, like SASS, JS, and Image files for both vendor and source assets.

    Gruntfile.js

    Contains build configurations and tasks which utilize paths and environment variables.

    app/

    Contains the application's source files.

    bower.json

    Contains configuration and versioning of Front End vendor files.

    bower_components/

    Contains Front End vendor files.

    build/

    Contains the application's build files.

    design/

    Contains creative source documents, like logos, wireframes, and design comps.

    node_modules/

    Contains essential Build dependencies for Grunt and various other Node.js applications integral to development of the application.

    package.json

    Contains a metadescription of application assets essential to Build dependencies for Grunt and various other Node.js applications.

    readme.markdown

    Contains an introduction to the application concerning TODO, CHANGELOG, and various important links.

    test/

    Contains (currently) End-to-End and Manual testing files. Ultimately, unit tests should live inside the application, or unit tests should exist here with a structure similar to 3 (./app).

##Application Scaffolding (CSS)

Exploring the presentation of our application, we will notice a quasi-modular structure which is predicated on Compass/SASS and is expressed via Organic CSS:

    README.md  

    Contains a brief description of each area of the presentation. Extend upon discovery.

    __variables.scss

    Contains ZURB Foundation variables for its parts.

    _app.scss

    Contains imports for our application's anatomy, currently from its Cells down to its Atoms.

    _defaults.scss

    Contains ZURB Foundation variables defaults.

    _utils.scss

    Contains miscellaneous utility functions for our presentation layer.

    atoms/

    Contains atomic CSS parts.

    cells/

    Contains cellular CSS parts.

    elements/

    Contains elemental CSS parts.

    fonts/

    Contains fonts to be used for typesetting our application.

    modules/

    Contains modules of the application which cohere with JavaScript modules' specific and unique needs.

    molecules/

    Contains modular CSS parts.

    organelles/

    Contains CSS organelles.

    overrides.scss

    Contains any arbitrary overrides to our application.

    pages/

    Contains page specific design specifications. Think in terms of the body's class attribute which will have taxonomic details of the application, like "page-login" or "page-facebook-oauth".

    plugins/

    Contains plugins for our application which may involve vendor overrides. This is just a more specific "overrides" section.

    responses/

    Contains responsive code which otherwise is not part of a module, or core responsive variables.

    style.scss

    Contains the main entry point (think "main" for RequireJS) of our application. Our app (like interface) is stored here, along with vendor imports.

    templates/

    Contains templates which may be used across pages. Pages and Templates are "legacy specifications" for ideas not immediately amenable to our Organic CSS description, and should readily fit into the conventional grammar of Web design.

##Application Scaffolding (JavaScript)

Diving deeper into our application, we will notice a modular structure which is predicated on AngularJS for our Front End MVC:

    boot.js  

    Contains the RequireJS "main" file. Our application "boots".

    config/

    Contains our base configuration file which describes API environment options as well as high-level front-end decorators to our application's behavior.

    directives/

    Contains high-level AngularJS directives.

    ext/

    Contains output build files given via Grunt tasks.

    filters/

    Contains high-level filters to deal with template formatting and data conversions otherwise not necessary to be done at the Client-side Service level.

    interface.js

    Contains our application dependency chain and application aliases for modules and vendor dependencies.

    jshintrc.json

    Contains our syntax checking validation rules for JavaScript source files. Files must pass validation before they are built.

    models/

    Contains our application's business objects, described as AngularJS factories.

    modules/

    Contains our application's modularized feature-set, like "authentication", "profile", and various other features which may or may not possess pages. Modules are exposed via "Module Controllers" facilitated by AngularAMD.

    routes.js

    Contains our application's AngularJS router specification which cohere with Module Controllers.

    services/

    Contains Client-side Services (non-business object models to be used in our front end, and (model) services which are combined with our base API configuration to load models under certain feature contexts of the application.

    spec/

    Specifications live with their modules. In essence, our root application JS files are part of the "core module" of the application, that facilitates an interface for submodules (features) of the application.

    utils/

    Vendor integration modules which map depend on RequireJS wrappers, like Leaflet or BreezeJS Adapters or Firebase Adapters.

##Development Server

Try:

    $ npm install

Then try:

    $ cd server; node server.js

A development server should run at http://localhost:8081/. Try testing out a basic endpoint:

    $ curl http://localhost:8081/states #to get a list of a few states in the U.S.

Development Environments

Our core dependency carries with it a lot of materials resources: https://github.com/marcoslin/generator-angularAMD

To start, try:

    $ npm install -g generator-angularamd

Then try:

    $ npm install 

If you do not have grunt-cli install:

    $ npm install -g grunt-cli

Then to modify Firebase Security Rules:

    $ npm install -g blaze_compiler

Now try out Firebase Tools:

    $ npm install -g firebase-tools

Firebase Tools works well with ``firebase.json``[0] to facilitate updates to 
our Security Rules and Authentication patterns which run on top of Firebase.

You will want to ensure that you are at the latest version on npm and node:

    $ npm update npm

Then try:

    $ sudo npm cache clean -f
    $ sudo npm install -g n
    $ sudo n stable

Then try:

    $ grunt devel

The application should be running locally at port http://localhost:8080/.
Server Configurations on Digital Ocean

Login as root

$ sshpass -p 5Ln5ObgYWQv3eQ== ssh root@ 104.131.89.71

## Configuration
1. $ sudo apt-get install build-essentials
2. $ sudo apt-ge install apache2
3. $ sudo apt-get update
4. $ sudo apt-get install tig

### Configuration of swap memory

Reference: https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04

1. $ sudo swapon -s # to check for swapfile:
2. Or try: $ free -m #check for memory allocations
3. $ sudo fallocate -l 4G /swapfile # create new swap file
4. $ ls -lh /swapfile # review new swap file
5. $ sudo chmod 600 /swapfile # set permissions
6. $ ls -lh /swapfile # check permissions
7. $ sudo mkswap /swapfile # set up the swap space
8. $ sudo swapon /swapfile # accordingly
9. $ sudo swapon -s # verify that it is running

#### Swappiness

1. $ cat /proc/sys/vm/swappiness # check for swappiness; we want a value closer to 10 for virtual private servers
2. $ sudo sysctl vm.swappiness=10
3. $ sudo vim /etc/sysctl.conf # @TODO Add ``vm.swappiness=10``

#### System Pressure

1. $ cat /proc/sys/vm/vfs_cache_pressure # will likely output 100, which implies that inode information is being removed too quickly
2. $ sudo sysctl vm.vfs_cache_pressure=50
3. $ sudo vim /etc/sysctl.conf # @TODO Add ``vm.vfs_cache_pressure = 50``

### Configuration of Firewall and Security

1.

### Configuration for Deployments

1.

### Configuration of proxyserver and load-balanced systems

1.


—
[0]: https://www.firebase.com/docs/hosting/guide/full-config.html

