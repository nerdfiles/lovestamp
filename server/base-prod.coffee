###
@fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module server
@description

  A basic implementation of a REST server that accepts all the crazy stuff our 
  controllers might throw at it, and dishing it out to 

    1. Firebase
    2. Twitter
    3. Google+
    4. Facebook
    5. SnowShoe
    6. ChangeTip
    7. a custom Dovecot mail

  Running on

    1. Node.js
    2. Express
    3. nginx reverse proxy
    4. Digital Ocean

###


### Arguments ###
args = process.env.NODE_PORT

### Dependencies ###

express = require('express')
compression = require('compression')

https = require('https')
http = require('http')

routes = require('./routes')
fs = require('fs')
gzippo = require('gzippo')
bodyParser = require 'body-parser'
path = require('path')

baseDir = __dirname + '/..'

cookieParser = require('cookie-parser')
session = require('express-session')

moment = require('moment')
_ = require('lodash')
Chance = require('chance')

try
    Q = require('q')
catch e
    Q = require('Q')

#fbgraph = require('fbgraphapi')

paymentSystem = require('./endpoints/economy/payment')
economyBudget = require('./endpoints/economy/budget')
socialSystem = require('./endpoints/social/connect')
sms = require('./endpoints/authentication/authorize')
#nearHere = require('./endpoints/near-here')
#notifications = require('./endpoints/notifications')

stampministryRequest = require('./endpoints/stampministry/request')
stampministryPurse = require('./endpoints/stampministry/purse')
stampministryRepost = require('./endpoints/stampministry/repost')
stampministryGetSerial = require('./endpoints/stampministry/get-serial')

#stampministry = require('./endpoints/base')
stampministryUser = require('./endpoints/stampministry/user')
stampministryValidation = require('./endpoints/stampministry/validation')
stampministryMoment = require('./endpoints/stampministry/moment')

textSearch = require('./endpoints/search/es')
economyTopUser = require('./endpoints/economy/top-user')

auth_changetip = require('./endpoints/authentication/changetip')
hasToken = require('./endpoints/authentication/has-token')


### Configuration Variables ###

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

urlPrefix =
  '@' : 'https://twitter.com/'
  'ᶠ' : 'https://facebook.com/'
  '✚' : 'https://plus.google.com/'


baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 


### Implementation Functions ###

momentDesignator = (timestamp) ->
  ###*
  @module utils
  @description

    Moment designator expiry. We may want semantic events around our semantic 
    levels for Tippees.

  ###

  momento = moment(timestamp)    # 1987-08-16T07:00:00.000Z
  day   = momento.date()         # 16
  month = momento.month() + 1    # 8
  year  = momento.year()         # 1987

  timeperiods =
    year  : moment(year, "YYYY").toISOString(),                                # 1987-01-01T08:00:00.000Z
    month : moment(month + "-" + year, "MM-YYYY").toISOString(),               # 1987-08-01T07:00:00.000Z
    day   : moment(day + "-" + month + "-" + year, "DD-MM-YYYY").toISOString() # 1987-08-16T07:00:00.000Z


#amqp = require('amqp')

#connection = amqp.createConnection({
  #host              : 'localhost'
  #port              : 5672
  #login             : 'guest'
  #password          : 'guest'
  #connectionTimeout : 10000
  #authMechanism     : 'AMQPLAIN'
  #vhost             : '/'
  #noDelay           : true
  #ssl               : { enabled: false }
#})

#connection.on('ready', () ->
  #connection.queue('my-queue', (q) ->
      #q.bind('#')

      #q.subscribe((message) ->
      #)
  #)
#)

app = express()
app.use(compression())
app.set('port', args)


###

           _
          (_)
     _   _ _ _____ _ _ _  ___
    | | | | | ___ | | | |/___)
     \ V /| | ____| | | |___ |
      \_/ |_|_____)\___/(___/

###

app.set 'views', path.join(baseDir + '../app/')
app.set 'view options',
  layout : false
  pretty : true
app.set 'view engine', 'jade'

app.use(cookieParser())
app.use(bodyParser.urlencoded(extended: false))
app.use(bodyParser.json())


sess =
  secret: 'nM#mE%',
  cookie: {}

if app.get('env') == 'production'
  app.set('trust proxy', 1) // trust first proxy
  sess.cookie.secure = true // serve secure cookies

app.use(session(sess))

###
Facebook Application-wide Authentication
###

#app.use(fbgraph.auth({
        #appId       : credentials.facebook.app_id
        #appSecret   : credentials.facebook.app_secret
        #redirectUri : "http: //0.0.0.0:8080/"
        #apiVersion  : "v2.2"
        #skipUrlPatterns: [
          #/^$/
        #]
    #}))


###
                            _
            _           _  (_)
      ___ _| |_ _____ _| |_ _  ____
     /___|_   _|____ (_   _) |/ ___)
    |___ | | |_/ ___ | | |_| ( (___
    (___/   \__)_____|  \__)_|\____)

###

app.use(express.static(path.join(__dirname, '../app')))
app.use('/assets', express.static(path.join(__dirname, '../app')))

app.use("/reports", express.static(path.join(__dirname, '../reports')))
app.use("/docs/api", express.static(path.join(__dirname, '../docs/api')))
app.use("/docs/code", express.static(path.join(__dirname, '../docs/autodoc')))
app.use("/docs/code/:path?", express.static(path.join(__dirname, '../docs/autodoc')))
app.use("/docs/clients", express.static(path.join(__dirname, '../docs/clients')))
app.use("/docs/server", express.static(path.join(__dirname, '../docs/server')))


###

     _____ ____ _   _
    | ___ |  _ \ | | |
    | ____| | | \ V /
    |_____)_| |_|\_/

###

#if app.get('env') is 'development'
  #app.use((req, res, next) ->
  #)
  #app.use((err, req, res, next) ->
  #)
# if (app.get('env') == 'production')


app.get '/humans.txt', routes.humans
app.get '/robots.txt', routes.robots
app.get '/package.json', routes.pkg

app.get('/changetip/oauth2/authentication', auth_changetip.authorization_code)
app.all('/changetip/code/callback', auth_changetip.access_token)
app.all('/changetip/oauth2/refresh', auth_changetip.refresh_token)

app.all('/has-token', hasToken)
#app.all("/near-here/channel", nearHere.channel)
#app.all("/near-here", nearHere.near_here)
#app.all('subscribe', notifications.subscribe)
#app.all('push/gcm', notifications.push_gcm)
#app.all('push/curl', notifications.push_curl)
#app.all('push/apn', notifications.push_apn)


economyBudget.budgetCronStart()
app.all '/budget/roll-up', economyBudget.rollUp
app.all '/budget/reset/:newBudget/:newRevenue', economyBudget.reset

app.all('/search', textSearch.search)
app.all('/payment/bitcoin/:username/:type', paymentSystem.payments_btc)
app.all('/payment/:username/:type', paymentSystem.payments_usd)
app.all('/social/connect', socialSystem.connect)
app.all('/top-user', economyTopUser.generateTopUser)
app.all('/authorize', sms.authorize)
app.all('/stamp/:username/request', stampministryRequest.request)
app.all('/purse', stampministryPurse.update_purse)
app.all('/stamp/get-serial', stampministryGetSerial.get_serial)
app.all('/stamp/repost', stampministryRepost.response)
app.all('/stamp/:username/:status/:handleType?', stampministryMoment.post)
app.all('/stamp/:username/validate/:merchantHandle/:point_data', stampministryValidation.parse)
app.all('/stamp/:username/local', stampministryUser.local)
app.all('/stamp/:username/error', stampministryUser.error)

app.all('/fan', routes.index_fan)
app.all('/merchant', routes.index_merchant)
app.all('/fan/login', routes.index_fan)
app.all('/merchant/login', routes.index_merchant)
app.get('*', routes.index)


### Serves via Load Balancer. ###

http.createServer(app).listen app.get('port'), ->
  console.log 'Loading LoveStamp . . . Please wait . . .'
  return
