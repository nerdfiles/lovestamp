###
###

gyftHandler = (credentials) ->
  apiKey = process.argv[2]
  apiSecret = process.argv[3]
  timeStamp = Math.round((new Date).getTime() / 1000).toString()
  stringToSign = apiKey + apiSecret + timeStamp
  sha256 = require('crypto').createHash('sha256')
  signature = sha256.update(stringToSign).digest('hex')
  http = require('http')
  http.get {
    hostname: 'apitest.gyft.com'
    port: 443
    method: 'GET'
    path: '/mashery/v1/reseller/account?api_key=' + apiKey + '&sig=' + signature
    headers: 'x-sig-timestamp': timeStamp
  }, (res) ->
    res.on 'data', (chunk) ->
      console.log chunk.toString()
      return
    return

module.exports = gyftHandler
