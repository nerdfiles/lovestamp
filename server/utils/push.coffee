###*
@fileOverview

                  _
                 | |
 ____  _   _  ___| |__
|  _ \| | | |/___)  _ \
| |_| | |_| |___ | | | |
|  __/|____/(___/|_| |_|
|_|

###

http = require('http')
apn = require('apn')
url = require('url')
gcm = require('node-gcm')

Curl = require("node-curl/lib/Curl")
_credentials = require('../config/credentials')
credentials = _credentials.credentials()

apnNotification = (requestBody) ->
    console.log requestBody

    launchImagePath = 'http://lovestamp.io:8080/images/launchImagePath.png'
    notificationSoundPath = 'http://lovestamp.io:8080/sounds/beep.wav'

    iPhone6 = '7f6c896c6491e32c61bbd2aac154ccb22568fab1b822052db1b81a99e9322048'
    iPhone5 = '3canc1493275761472e9a0b93fad10e0176e4b4692e1f48449462ff71fdbb013'

    device = new (apn.Device)(iPhone6)
    note = new (apn.Notification)
    note.badge = 1
    note.sound = notificationSoundPath # 'beep.wav'
    note.contentAvailable = 1
    note.alert =
      'body': message
      'action-loc-key': 'Show Me!'
      'launch-image': launchImagePath

    note.payload = 'messageFrom': 'LoveStamp'
    # additional payload
    note.device = device

    callback = (errorNum, notification) ->
      console.log 'Error is: %s', errorNum
      console.log 'Note ' + JSON.stringify(notification)
      return

    options = 
      gateway       : 'gateway.sandbox.push.apple.com'
      errorCallback : callback
      cert          : credentials.certPath # 'your-cert.pem'
      key           : credentials.keyPath # 'your-key.pem'
      passphrase    : credentials.passphrase # 'your-pw'
      port          : 2195
      enhanced      : true
      cacheLength   : 100
    apnsConnection = new (apn.Connection)(options)
    console.log 'Note ' + JSON.stringify(note)
    apnsConnection.sendNotification note


curlNotification = (credentials, message) ->

    p = console.log
    url = process.argv[2]
    curl = new Curl()
    url = 'https://android.googleapis.com/gcm/send'
    apiKey = credentials.google.android.apiKey
    registrationIDs = []
    messageConstruct = ''

    fields = [
        'registration_ids': registrationIDs
        'data': [{
            "message": message
        }]
    ]

    headers = [
        'Authorization: key=' + apiKey
        'Content-Type: application/json'
    ]

    curl.setopt "CONNECTTIMEOUT", 2
    curl.setopt "URL", url
    curl.setopt('POST', 1)
    curl.setopt('HTTPHEADER', headers)
    curl.setopt('RETURNTRANSFER', 1)
    curl.setopt('CURLOPT_POSTFIELDS', fields)

    # on 'data' must be returns chunk.length, or means interrupt the transfer
    curl.on "data", (chunk) ->
      p "receive " + chunk.length
      chunk.length

    curl.on "header", (chunk) ->
      p "receive header " + chunk.length
      chunk.length

    # curl.close() should be called in event 'error' and 'end' if the curl won't use any more.
    # or the resource will not release until V8 garbage mark sweep.
    curl.on "error", (e) ->
      p "error: " + e.message
      curl.close()
      return

    curl.on "end", ->
      p "code: " + curl.getinfo("RESPONSE_CODE")
      p "done."
      curl.close()
      return

    curl.perform()

gcmNotification = () ->
  message = new (gcm.Message)
  #API Server Key
  apiKey = 'AIzaSyCmTg7xqTWCCfvxWom-D7pNg1pEQLvkVZY'
  sender = new (gcm.Sender)(apiKey)
  registrationIds = []
  # Value the payload data to send...
  message.addData 'message', '✌ Peace, Love ❤ and PhoneGap ✆!'
  message.addData 'title', 'Push Notification Sample'
  message.addData 'msgcnt', '3'
  # Shows up in the notification in the status bar when you drag it down by the time
  #message.addData('soundname','beep.wav'); //Sound to play upon notification receipt - put in the www folder in app - may not work
  #message.collapseKey = 'demo';
  #message.delayWhileIdle = true; //Default is false
  message.timeToLive = 3000
  # Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.
  # At least one reg id/token is required
  registrationIds.push 'APA92cGtSvVZudbg8ef_CrzPiMiXasKlt1NzYi3FRX7a1z1rpBqBqvT6TvUJL7RHABYviYyL6Q9jw5jTbpRjpkLhrP0f2r11c3bHGUJuUm-eaOXP0QKp0aIpWJIQTHTVQ4prRHuwJYvi8S16cO_B_5LrhfuFthQ9uw5bQXQ3OGngF8N-dmJu-kE'

  ###*
  # Parameters: message-literal, registrationIds-array, No. of retries, callback-function
  ###

  sender.send message, registrationIds, 4, (result) ->
    console.log result
    #null is actually success
    return

module.exports =
    apnNotification  : apnNotification
    curlNotification : curlNotification
    gcmNotification  : gcmNotification
