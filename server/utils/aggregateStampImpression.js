
/**
@fileOverview

                           _                                   _
                          (_)                                 (_)
  ____  ____ ___  ____     _ ____  ____   ____ _____  ___  ___ _  ___  ____
 / ___)/ ___) _ \|  _ \   | |    \|  _ \ / ___) ___ |/___)/___) |/ _ \|  _ \
( (___| |  | |_| | | | |  | | | | | |_| | |   | ____|___ |___ | | |_| | | | |
 \____)_|   \___/|_| |_|  |_|_|_|_|  __/|_|   |_____|___/(___/|_|\___/|_| |_|
                                  |_|

@description

    A regularly scheduled roll-up of leftover monthly stamp impressions.
 */

(function() {
  var Chance, Decimal, Firebase, Q, baseDataApiUrl, budgetCron, budgetCronStart, moment, _;

  Firebase = require('firebase');

  Decimal = require('decimal.js');

  _ = require('lodash');

  Q = require('q');

  Chance = require('chance');

  moment = require('moment');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';


  /*
  @namespace budgetCron
  @usage
    budgetCron()
   */

  budgetCron = function() {
    var LSRoot, balancesRoot, c, clearedUserImpressions, def, defImpressions, impressionsContainer, imps, now, payoutBudget, payoutBudgetSource, revenue, revenueSource, setBudget, setBudgetSource, usersRoot, _c, _clearedUserImpressions;
    LSRoot = new Firebase(baseDataApiUrl);
    usersRoot = LSRoot.child('users');
    balancesRoot = LSRoot.child('balances');
    setBudgetSource = balancesRoot.child('setBudget');
    setBudget = Q.defer();
    setBudgetSource.once('value', function(setBudgetSnapshot) {
      return setBudget.resolve(setBudgetSnapshot.val());
    });
    payoutBudgetSource = balancesRoot.child('payoutBudget');
    payoutBudget = Q.defer();
    payoutBudgetSource.once('value', function(payoutBudgetSnapshot) {
      return payoutBudget.resolve(payoutBudgetSnapshot.val());
    });
    revenueSource = balancesRoot.child('revenue');
    revenue = Q.defer();
    revenueSource.once('value', function(revenueSnapshot) {
      return revenue.resolve(revenueSnapshot.val());
    });
    impressionsContainer = [];
    now = moment();
    defImpressions = Q.defer();
    def = Q.defer();
    clearedUserImpressions = Q.defer();
    _clearedUserImpressions = [];
    c = 1;
    _c = 0;
    imps = [];
    usersRoot.on("child_added", function(userSnapshot, key) {
      var users;
      c++;
      _c = userSnapshot.numChildren();
      users = _.map(userSnapshot.val(), function(userComponent, _key) {
        var _userObject;
        if (!_key) {
          return;
        }
        if (_key === 'impressions') {
          return _userObject = _.map(userComponent, function(impression) {
            var d, expiration;
            d = new Date(impression.expiry);
            expiration = moment(d);
            if (expiration.isBefore(now)) {
              _clearedUserImpressions.push({
                username: userSnapshot.key(),
                impressions: userComponent
              });
              return {
                imp: impression || ''
              };
            }
          });
        }
      });
      imps.push(users);
      if (_c <= c++) {
        clearedUserImpressions.resolve(_clearedUserImpressions);
        return def.resolve(imps);
      }
    });
    usersRoot.on("child_added", function(userSnapshot, key) {
      var impressions, user, userObject;
      user = userSnapshot;
      userObject = user.val();
      impressions = userObject.impressions;
      if (impressions) {
        _.forEach(impressions, function(impressionsItem, key) {
          return impressionsContainer.push(impressionsItem.quantity);
        });
      }
      return defImpressions.resolve(impressionsContainer);
    });
    defImpressions.promise.then(function(_aggregateStampImpression) {
      var aggregateStampImpression;
      console.log('/////');
      console.log(_aggregateStampImpression);
      console.log('/////');
      aggregateStampImpression = _.reduce(_aggregateStampImpression, function(sum, n) {
        return sum + n;
      });
      return def.promise.then(function(users) {
        var __r, _r, _users;
        _users = _.flatten(users, true);
        _r = _.filter(_users, function(_u) {
          return _u !== void 0;
        });
        __r = _.map(_r, function(_p) {
          return _p.imp.quantity;
        });
        console.log(__r);
        console.log(aggregateStampImpression);
        return payoutBudget.promise.then(function(_payoutBudget) {
          var X_1;
          X_1 = (_payoutBudget / aggregateStampImpression) * __r;
          revenue.promise.then(function(static_revenue) {
            return revenueSource.set(static_revenue + X_1);
          });
          payoutBudgetSource.set(_payoutBudget - X_1);
          return clearedUserImpressions.promise.then(function(clearedUsersList) {
            return _.forEach(clearedUsersList, function(user) {
              console.log("Reading user " + user);
              return _.forEach(user.impressions, function(item, key) {
                console.log("Removing impressions from " + user.username + " with key: " + key);
                return usersRoot.child(user.username).child('impressions').child(key).remove();
              });
            });
          });
        });
      });
    });
    return true;
  };


  /**
  @namespace budgetCronStart
  @return {object} jobStatus A status of the initiated job.
   */

  budgetCronStart = function() {
    var CronJob, job, jobStatus;
    CronJob = require('cron').CronJob;
    job = new CronJob({
      cronTime: '00 10 17 * * 1-7',
      onTick: function() {
        return budgetCron();
      },
      start: false,
      timeZone: "America/Chicago"
    });
    return jobStatus = job.start();
  };

  module.exports = {
    budgetCronStart: budgetCronStart,
    budgetCron: budgetCron
  };

}).call(this);
