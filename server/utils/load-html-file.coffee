fs = require('fs')
try
    Q = require('q')
catch e
    Q = require('Q')

loadHtmlFile = (_fp) ->
  def = Q.defer()
  fp = if _fp then _fp else '/test.txt'
  fullPath = fp
  fs.readFile(fullPath, (err, data) ->
    console.log err
    if err
      def.reject err
      return
    def.resolve data.toString()
  )
  def.promise


module.exports = loadHtmlFile
