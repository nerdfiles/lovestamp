
/**
@fileOverview

                  _     _
   _             (_)   (_)
 _| |_ ___  ____  _     _  ___ _____  ____
(_   _) _ \|  _ \| |   | |/___) ___ |/ ___)
  | || |_| | |_| | |___| |___ | ____| |
   \__)___/|  __/ \_____/(___/|_____)_|
           |_|

@description

    Ident for top user with most bits collected.
 */

(function() {
  var Firebase, Q, baseDataApiUrl, topUser, topUserCronStart, _;

  Firebase = require('firebase');

  _ = require('lodash');

  Q = require('q');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  topUser = function() {
    var Balances, Users;
    Users = new Firebase("" + baseDataApiUrl + "users");
    Balances = new Firebase("" + baseDataApiUrl + "balances");
    return Users.once('value', function(_users) {
      var bitsCollectedList, _topUserBitsCollected;
      bitsCollectedList = _.filter(_.pluck(_.filter(_.pluck(_.toArray(_users.val()), 'profile'), function(_profile) {
        return _profile;
      }), 'bitsCollected'), function(bitsCollected) {
        return bitsCollected;
      }).sort(function(a, b) {
        return a - b;
      });
      _topUserBitsCollected = bitsCollectedList.pop();
      return Balances.child('topUser').set(_topUserBitsCollected);
    });
  };


  /**
  @namespace topUserCronStart
  @return {object} jobStatus A status of the initiated job.
   */

  topUserCronStart = function() {
    var CronJob, job, jobStatus;
    CronJob = require('cron').CronJob;
    job = new CronJob({
      cronTime: '*/15 * * * *',
      onTick: function() {
        console.log('Run top user.');
        return topUser();
      },
      start: false,
      timeZone: "America/Chicago"
    });
    return jobStatus = job.start();
  };

  module.exports = {
    topUserCronStart: topUserCronStart,
    topUser: topUser
  };

}).call(this);
