(function() {
  var Q, e, fs, loadHtmlFile;

  fs = require('fs');

  try {
    Q = require('q');
  } catch (_error) {
    e = _error;
    Q = require('Q');
  }

  loadHtmlFile = function(_fp) {
    var def, fp, fullPath;
    def = Q.defer();
    fp = _fp ? _fp : '/test.txt';
    fullPath = fp;
    fs.readFile(fullPath, function(err, data) {
      console.log(err);
      if (err) {
        def.reject(err);
        return;
      }
      return def.resolve(data.toString());
    });
    return def.promise;
  };

  module.exports = loadHtmlFile;

}).call(this);
