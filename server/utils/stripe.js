(function() {
  var STRIPEPUBLISHABLE, STRIPESECRET, livemode, readStripePublishable, readStripeSecret;

  livemode = false;

  if (livemode) {
    readStripeSecret = fs.readFileSync(__dirname + '/../../stripe.live.secret', 'utf8');
    readStripePublishable = fs.readFileSync(__dirname + '/../../stripe.live.publishable', 'utf8');
  } else {
    readStripeSecret = fs.readFileSync(__dirname + '/../../stripe.test.secret', 'utf8');
    readStripePublishable = fs.readFileSync(__dirname + '/../../stripe.test.publishable', 'utf8');
  }

  STRIPESECRET = trim(readStripeSecret);

  STRIPEPUBLISHABLE = trim(readStripePublishable);

  module.exports = {
    STRIPESECRET: STRIPESECRET,
    STRIPEPUBLISHABLE: STRIPEPUBLISHABLE
  };

}).call(this);
