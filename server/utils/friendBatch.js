
/*
 * fileOverview

        ___      _                 _ ______                   _
       / __)    (_)               | (____  \         _       | |
     _| |__ ____ _ _____ ____   __| |____)  )_____ _| |_ ____| |__
    (_   __) ___) | ___ |  _ \ / _  |  __  ((____ (_   _) ___)  _ \
      | | | |   | | ____| | | ( (_| | |__)  ) ___ | | |( (___| | | |
      |_| |_|   |_|_____)_| |_|\____|______/\_____|  \__)____)_| |_|

 *# description

    User gets stamped,
    go through each individual stamp
    aggregate the yellow of their friends
    for each of these stamps, create a success for each of these (unless the stamp to create has already been created bc 
    that's the same stamp at inaugural stamping) and write blue. Yellow 
    increments as per normal. For each aggregation of +1, write that in 
    blue, if stamp doesn't exist, create stamp success and write 1 in blue.
    — James D
 */

(function() {
  var Firebase, baseDataApiUrl, e, friendsBatchIntensityRoundup, friendsBatchIntensityRoundupCron, q, _;

  Firebase = require('firebase');

  _ = require('lodash');

  try {
    q = require('Q');
  } catch (_error) {
    e = _error;
    q = require('q');
  }

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  friendsBatchIntensityRoundup = function(username, provider) {

    /*
    
    @description Utilities for collocating users by shared stampenings from 
                 particular merchants.
    
    @namespace friendsBatchIntensity
    @param {string} username
    @param {array} merchants Merchants of a given user's stats/success branch.
     */
    var Users, customerConstruct, def, socialProvidersAvailable, t, updatedStampImpressionsBlue, _currentCustomer, _friends, _stamps;
    Users = new Firebase("" + baseDataApiUrl + "users");
    updatedStampImpressionsBlue = false;
    def = q.defer();
    socialProvidersAvailable = {
      '@': 'twitter',
      '+': 'google',
      'ᶠ': 'facebook'
    };
    _currentCustomer = Users.child(username);
    _friends = _currentCustomer.child('friends').child(socialProvidersAvailable[provider]);
    _stamps = _currentCustomer.child('stamps').child('stats').child('success');
    t = [];
    customerConstruct = q.all([_stamps, _friends]);
    return customerConstruct.then(function(data) {
      var friends, stamps;
      stamps = data[0];
      friends = data[1];
      return stamps.on('value', function(stampData) {
        return friends.on('value', function(friendData, keyFriends) {
          var a, z, _z;
          a = [];
          _.forEach(friendData.val(), function(friendUsername, friendUserKey) {
            var friendHandle, _stampsFriend;
            friendHandle = friendUsername.handle;
            _stampsFriend = Users.child(friendHandle).child('stamps').child('stats').child('success');
            return _stampsFriend.once('value', function(s) {
              if (s.val() !== null) {
                return _.forEach(s.val(), function(ss, key) {
                  return a.push({
                    key: key,
                    stampImpressionsYellow: ss.stampImpressionsYellow
                  });
                });
              }
            });
          });
          z = _.groupBy(a, 'key');
          _z = _.map(z, function(stampeningHistory) {
            var _qty;
            _qty = 0;
            _.forEach(stampeningHistory, function(sh) {
              return _qty += sh.stampImpressionsYellow;
            });
            return {
              stampId: _.last(stampeningHistory).key,
              aggregateStampImpressionsYellow: _qty
            };
          });
          return _.forEach(stampData.val(), function(customerStampData, keyStamp) {
            return _.forEach(friendData.val(), function(friendUsername, friendUserKey) {
              var friendHandle, _stampsFriend;
              friendHandle = friendUsername.handle;
              _stampsFriend = Users.child(friendHandle).child('stamps').child('stats').child('success');
              return _stampsFriend.on('value', function(friendStampData) {
                return _.forEach(friendStampData.val(), function(_friendStampData, _keyFriendStampData) {
                  var CurrentStampStory, foundStamp;
                  if (keyStamp === _keyFriendStampData) {
                    foundStamp = _.find(_z, {
                      stampId: keyStamp
                    });
                    return CurrentStampStory = _stamps.child(keyStamp).child('stampImpressionsBlue').set(parseInt(foundStamp.aggregateStampImpressionsYellow, 10));
                  }
                });
              });
            });
          });
        });
      });
    });
  };

  friendsBatchIntensityRoundupCron = function() {

    /*
    @namespace friendsBatchIntensityRoundup
    @return {object} jobStatus A status of the initiated job.
     */
    var CronJob, job, jobStatus;
    CronJob = require('cron').CronJob;
    job = new CronJob({
      cronTime: '00 30 11 * * 1-7',
      onTick: function() {},
      start: false,
      timeZone: "America/Chicago"
    });
    return jobStatus = job.start();
  };

  module.exports = {
    friendsBatchIntensityRoundup: friendsBatchIntensityRoundup,
    friendsBatchIntensityRoundupCron: friendsBatchIntensityRoundupCron
  };

}).call(this);
