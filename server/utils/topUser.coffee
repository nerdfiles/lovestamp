###*
@fileOverview

                  _     _
   _             (_)   (_)
 _| |_ ___  ____  _     _  ___ _____  ____
(_   _) _ \|  _ \| |   | |/___) ___ |/ ___)
  | || |_| | |_| | |___| |___ | ____| |
   \__)___/|  __/ \_____/(___/|_____)_|
           |_|

@description

    Ident for top user with most bits collected.

###

Firebase = require('firebase')
_ = require('lodash')
Q = require('q')


baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 

topUser = () ->

    Users = new Firebase("#{baseDataApiUrl}users")
    Balances = new Firebase("#{baseDataApiUrl}balances")

    Users.once 'value', (_users) ->
        bitsCollectedList = _.filter(_.pluck(_.filter(_.pluck(_.toArray(_users.val()), 'profile'), (_profile) ->
            _profile
        ), 'bitsCollected' ), (bitsCollected) ->
            bitsCollected
        ).sort((a, b) ->
            a - b
        )

        _topUserBitsCollected = bitsCollectedList.pop()

        Balances.child('topUser').set _topUserBitsCollected

###*
@namespace topUserCronStart
@return {object} jobStatus A status of the initiated job.
###

topUserCronStart = () ->
  CronJob = require('cron').CronJob
  job = new CronJob({
    cronTime: '*/15 * * * *'
    onTick: () ->
      console.log 'Run top user.'
      # Runs every 15 minutes.
      topUser()
    start: false
    timeZone: "America/Chicago"
  })

  jobStatus = job.start()

# Export top user check API.
module.exports =
    topUserCronStart: topUserCronStart
    topUser: topUser
