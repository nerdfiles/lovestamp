
/*
 * fileOverview

     _                                   _
    (_)                                 (_)
     _ ____  ____   ____ _____  ___  ___ _  ___  ____
    | |    \|  _ \ / ___) ___ |/___)/___) |/ _ \|  _ \
    | | | | | |_| | |   | ____|___ |___ | | |_| | | | |
    |_|_|_|_|  __/|_|   |_____|___/(___/|_|\___/|_| |_|
            |_|

 *# description

    Registration code for detecting stamp unique sign stamp signatures prepared by Graham Scans[0].

    —
    [0]: http://en.wikipedia.org/wiki/Graham_scan
 */

(function() {
  var Chance, Firebase, Q, baseDataApiUrl, createImpression, fs, loadHtmlFile, moment, nodemailer, notifyMerchant, notifyMerchantCronStart, numeral, smtpTransport, stampCreated, stampImpression, tryCreateStamp, _;

  Firebase = require('firebase');

  numeral = require('numeral');

  _ = require('lodash');

  Q = require('q');

  Chance = require('chance');

  nodemailer = require('nodemailer');

  smtpTransport = require('nodemailer-smtp-transport');

  moment = require('moment');

  fs = require('fs');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  loadHtmlFile = function(_fp) {
    var def, fp, fullPath;
    def = Q.defer();
    fp = _fp ? _fp : '/test.txt';
    fullPath = __dirname + '/' + fp;
    fs.readFile(fullPath, function(err, data) {
      if (err) {
        def.reject(err);
        return;
      }
      return def.resolve(data.toString());
    });
    return def.promise;
  };

  notifyMerchant = function(merchant, expiryImpressions, expiryDateTime) {
    var UsersRoot, merchantRoot, merchantsGroupExpired, send, userStampImpressions;
    UsersRoot = new Firebase(baseDataApiUrl + 'users');
    merchantRoot = UsersRoot.child(merchant);
    userStampImpressions = merchantRoot.child('impressions');
    send = function(merchant, expiryImpressions, expiryDateTime) {
      var messageHtml, messageText, transport, _merchantEmail;
      merchantRoot = UsersRoot.child(merchant);
      transport = nodemailer.createTransport(smtpTransport({
        host: 'smtp.lovestamp.io',
        port: 587,
        debug: true,
        secure: false,
        secureConnection: false,
        ignoreTLS: false,
        tls: {
          rejectUnauthorized: false
        },
        auth: {
          user: 'aaron',
          pass: 'cCNkd0BpKQ3UHg=='
        }
      }));
      messageText = "Hello, fellow LoveStamper! \n\n You currently have " + expiryImpressions + " stamp impressions left and an expiry date of " + expiryDateTime + " \n\n Please top up <a href='http://merchant.lovestamp.io/store'>here</a>, so you can keep you're LoveStamp active.\n\nThe LoveStamp Mailman";
      messageHtml = "<p>Hello, fellow LoveStamper!</p> <p>You currently have " + expiryImpressions + " stamp impressions left and an expiry date of " + expiryDateTime + "</p><p>Please top up <a href='http://merchant.lovestamp.io/store'>here</a>, so you can keep you're LoveStamp active.</p><p>The LoveStamp Mailman</p>";
      _merchantEmail = merchantRoot.child('profile').child('overview').child('email');
      return _merchantEmail.once('value', function(merchantEmailSnapshot) {
        var htmlEmailTemplateFilePath, merchantEmail;
        merchantEmail = merchantEmailSnapshot.val();
        htmlEmailTemplateFilePath = '../../email/newsletter/purchase.inline.html';
        return loadHtmlFile(htmlEmailTemplateFilePath).then(function(htmlEmailConstruct) {
          return transport.sendMail({
            from: 'support@lovestamp.io',
            to: merchantEmail,
            subject: 'Your LoveStamp needs refueling!',
            text: messageText,
            html: htmlEmailConstruct
          });
        });
      });
    };
    if (merchant !== 'all') {
      userStampImpressions.once('value', function(snapshot) {
        return _.forEach(snapshot.val(), function(i, key) {
          var _expiry, _impression;
          _impression = parseInt(i.quantity, 10) - 1;
          _expiry = i.expiry;
          userStampImpressions.child(key).set({
            'quantity': _impression,
            'expiry': _expiry
          });
          return send(merchant, _impression, _expiry);
        });
      });
    } else {
      merchantsGroupExpired = UsersRoot;
      merchantsGroupExpired.once('value', function(merchantsGroupSnapshot) {
        return _.forEach(merchantsGroupSnapshot.val(), function(i, key) {
          var currentDateTime, expiry, impression, __i, _i, _merchant;
          _i = i.impressions;
          __i = _.last(_.toArray(_i));
          if (__i) {
            _merchant = key;
            impression = parseInt(__i.quantity, 10);
            expiry = __i.expiry;
            currentDateTime = moment().add(5, 'days');
            expiryDateTime = moment(expiry);
            if (currentDateTime.isAfter(expiryDateTime)) {
              return send(_merchant, impression, expiry);
            }
          }
        });
      });
    }
  };

  createImpression = function(path, stampId, impressionObject) {

    /*
    @namespace go
    @descriptinon 
    
    Pass a known stampId into our Firebase backend to check if the stamp 
    already exists.
     */
    var stampData, stampsRef, _def;
    _def = Q.defer();
    stampData = impressionObject;
    tryCreateStamp(path, stampId, stampData);
    stampsRef = new Firebase(baseDataApiUrl + path);
    stampsRef.child(stampId).push({
      bitsCollected: stampData.bitsCollected
    }, function(error) {
      if (error) {
        return _def.reject({
          status: 'error',
          $meta: error
        });
      } else {
        return _def.resolve(true);
      }
    });
    return _def.promise;
  };

  stampCreated = function(path, stampId, success, stampData) {

    /*
    @namespace stampCreated
    @description
    
    Messaging around created stamps.
     */
    if (!success) {
      console.log('Stamp ' + stampId + ' already exists!');
    } else {
      console.log('Successfully created ' + stampId);
    }
  };

  tryCreateStamp = function(path, stampId, stampData) {

    /*
    @namespace tryCreateStamp
    @description
    
    Tries to set /stamps/<stampId> to the specified data, but only
    if there's no data there already. Also tries to push a new 
    bitsCollected object and attempts to update stampImpressionsYellow.
     */
    var p, s, stampsRef;
    p = baseDataApiUrl + path;
    stampsRef = new Firebase(p);
    s = stampsRef.child(stampId);
    return s.transaction(function(currentStampData) {
      if (currentStampData !== null) {
        return currentStampData;
      } else {
        return stampData;
      }
    }, function(error, committed) {
      stampCreated(path, stampId, committed, stampData);
      s.child('bitsCollected').set(stampData.bitsCollected);
      return s.child('stampImpressionsYellow').set(stampData.stampImpressionsYellow);
    });
  };

  stampImpression = function(username, merchant, stampId, provider, createTipUrl) {

    /*
    @module utils
    @namespace stampImpression
    @description
    
    A typical stamp impression scenario.
    
    @param {string} username With Social Media prefix.
    @return {number} X_2
     */
    var balancesRoot, chance, defImpressions, defX, e, impressionsContainer, merchantRoot, payoutBudget, payoutBudgetSource, revenue, revenueSource, setBudget, setBudgetSource, updatedBitsCollected, updatedMerchantStampImpressionsRed, updatedPurse, updatedUserStampImpressionsYellow, userRoot, userStampImpressions, usersRoot, _aggregatePendingPool, _aggregatePendingPoolVal, _defX, _payoutShadowBudget, _payoutShadowBudgetSource;
    defX = Q.defer();
    _defX = Q.defer();
    chance = new Chance();
    balancesRoot = new Firebase(baseDataApiUrl + 'balances');
    usersRoot = new Firebase(baseDataApiUrl + 'users');
    merchantRoot = new Firebase(baseDataApiUrl + 'users/' + merchant);
    userRoot = new Firebase(baseDataApiUrl + 'users/' + username);
    userStampImpressions = merchantRoot.child('impressions');
    try {
      updatedUserStampImpressionsYellow = userRoot.child('stamps').child('stats').child('success').child(stampId).child('stampImpressionsYellow');
    } catch (_error) {
      e = _error;
      console.log('Could not find stampId stampImpressionsYellow for ', username);
    }
    try {
      updatedMerchantStampImpressionsRed = merchantRoot.child('stamps').child('stats').child('local').child('stampImpressionsRed');
    } catch (_error) {
      e = _error;
      console.log('Could not find stampImpressionsRed for ', merchant);
    }
    try {
      updatedBitsCollected = userRoot.child('stamps').child('stats').child('success').child(stampId).child('bitsCollected');
    } catch (_error) {
      e = _error;
      console.log('Could not find stampId bitsCollected ', username);
    }
    updatedPurse = userRoot.child('profile').child('purse');
    setBudgetSource = balancesRoot.child('setBudget');
    setBudget = Q.defer();
    setBudgetSource.once('value', function(setBudgetSnapshot) {
      return setBudget.resolve(setBudgetSnapshot.val());
    });
    payoutBudgetSource = balancesRoot.child('payoutBudget');
    payoutBudget = Q.defer();
    payoutBudgetSource.once('value', function(payoutBudgetSnapshot) {
      return payoutBudget.resolve(payoutBudgetSnapshot.val());
    });
    _payoutShadowBudgetSource = balancesRoot.child('payoutShadowBudget');
    _payoutShadowBudget = Q.defer();
    _payoutShadowBudgetSource.once('value', function(payoutShadowBudgetSnapshot) {
      return _payoutShadowBudget.resolve(payoutShadowBudgetSnapshot.val());
    });
    _aggregatePendingPool = 0;
    _aggregatePendingPoolVal = Q.defer();
    usersRoot.once('value', function(usersData) {
      _.forEach(usersData.val(), function(userData) {
        var _purse;
        if (userData.profile && userData.profile.purse) {
          _purse = parseFloat(numeral(userData.profile.purse).format('0.00'));
          return _aggregatePendingPool = parseFloat(numeral(_purse).format('0.00')) + parseFloat(numeral(_aggregatePendingPool).format('0.00'));
        }
      });
      return _aggregatePendingPoolVal.resolve(_aggregatePendingPool);
    });
    revenueSource = balancesRoot.child('revenue');
    revenue = Q.defer();
    revenueSource.once('value', function(revenueSnapshot) {
      return revenue.resolve(revenueSnapshot.val());
    });
    defImpressions = Q.defer();
    impressionsContainer = [];
    usersRoot.once("value", function(userSnapshot, key) {
      _.forEach(userSnapshot.val(), function(userData) {
        var impressions, userObject;
        userObject = userData;
        impressions = userObject.impressions;
        if (impressions) {
          return _.forEach(impressions, function(impressionItem, key) {
            var currentDateTime, expiry, expiryDateTime, qty;
            qty = parseInt(impressionItem.quantity, 10);
            expiry = impressionItem.expiry;
            currentDateTime = moment();
            expiryDateTime = moment(expiry);
            if (currentDateTime.isAfter(expiryDateTime) || currentDateTime.isSame(expiryDateTime)) {
              return;
            }
            if (qty === 0) {
              return;
            }
            return impressionsContainer.push(qty);
          });
        }
      });
      return defImpressions.resolve(impressionsContainer);
    });
    defImpressions.promise.then(function(_aggregateStampImpression) {
      var aggregateStampImpression;
      aggregateStampImpression = _.reduce(_aggregateStampImpression, function(sum, n) {
        return sum + n;
      });
      return payoutBudget.promise.then(function(_payoutBudget) {
        if (createTipUrl === true) {
          return updatedPurse.once('value', function(_purse) {
            var currentPurse;
            currentPurse = parseFloat(numeral(_purse.val()).format('0.00'));
            payoutBudgetSource.set(parseFloat(numeral(_payoutBudget).format('0.00')) - parseFloat(numeral(currentPurse).format('0.00')));
            return defX.resolve(0);
          });
        } else {
          return _payoutShadowBudget.promise.then(function(payoutShadowBudgetValue) {
            var X_1, X_2, maximumComponent, minimumComponent, tipConstruct, updated_revenue;
            X_1 = parseFloat(numeral(payoutShadowBudgetValue).format('0.00')) / parseFloat(numeral(aggregateStampImpression).format('0.00'));
            maximumComponent = (90 / 100) * X_1;
            minimumComponent = (.5 / 100) * X_1;
            tipConstruct = {
              max: maximumComponent,
              min: minimumComponent,
              fixed: 2
            };
            X_2 = chance.floating(tipConstruct);
            updated_revenue = X_1 - X_2;
            return userStampImpressions.once('value', function(snapshot) {
              if (snapshot.val() === null) {
                console.log('Merchant does not have any impressions available.');
                defX.resolve(false);
                return;
              }
              return _.forEach(snapshot.val(), function(i, key) {
                var currentDateTime, expiryDateTime, _expiry, _impression, _now;
                _impression = parseInt(i.quantity, 10) - 1;
                _expiry = i.expiry;
                userStampImpressions.child(key).set({
                  'quantity': _impression,
                  'expiry': i.expiry
                });
                _now = moment();
                currentDateTime = moment().add(3, 'days');
                expiryDateTime = moment(_expiry);
                if (_now.isAfter(expiryDateTime) || _impression === 0) {
                  defX.resolve(false);
                  return;
                } else {
                  revenue.promise.then(function(static_revenue) {
                    var _updatedRevenue;
                    _updatedRevenue = parseFloat(numeral(static_revenue).format('0.00')) + parseFloat(numeral(updated_revenue).format('0.00'));
                    revenueSource.set(_updatedRevenue);
                    return _aggregatePendingPoolVal.promise.then(function(static_aggregatePendingPool) {
                      var updatedPayoutShadowBudget;
                      updatedPayoutShadowBudget = parseFloat(numeral(_payoutBudget).format('0.00')) - (parseFloat(numera(static_aggregatePendingPool).format('0.00')) + parseFloat(numeral(static_revenue).format('0.00')));
                      return _payoutShadowBudgetSource.set(updatedPayoutShadowBudget);
                    });
                  });
                  defX.resolve(X_2);
                }
                if (currentDateTime.isAfter(expiryDateTime)) {
                  notifyMerchant(merchant, _impression, _expiry);
                }
                if (_impression < 2) {
                  return notifyMerchant(merchant, _impression, _expiry);
                }
              });
            });
          });
        }
      });
    });
    if (createTipUrl !== true) {
      defX.promise.then(function(bitsCollected) {
        var userBitsCollected;
        if (bitsCollected === false) {
          _defX.resolve(false);
          return;
        }
        _defX.resolve(bitsCollected);
        updatedUserStampImpressionsYellow.once('value', function(snapshot) {
          var exists, impressionObject, _val;
          exists = snapshot.val() !== null;
          if (exists) {
            _val = snapshot.val();
            impressionObject = {
              'stampImpressionsYellow': parseInt(_val, 10) + 1,
              'bitsCollected': bitsCollected
            };
          } else {
            impressionObject = {
              'stampImpressionsYellow': 1,
              'bitsCollected': bitsCollected
            };
          }
          return createImpression("users/" + username + "/stamps/stats/success", stampId, impressionObject).then(function(status) {
            console.log('createImpression');
            return console.log(status);
          }, function(errorData) {
            if (errorData.status === 'error') {
              console.log('errorData');
              return console.log(errorData);
            }
          });
        });
        userBitsCollected = userRoot.child('stamps').child('stats').child('success').child(stampId).child('bitsCollected').set(parseFloat(numeral(updatedBitsCollected).format('0.00')) + parseFloat(numeral(bitsCollected).format('0.00')));
        return updatedMerchantStampImpressionsRed.once('value', function(snapshot) {
          var exists, _val;
          exists = snapshot.val() !== null;
          if (exists) {
            _val = snapshot.val();
            return usersRoot.child(merchant).child('stamps').child('stats').child('local').set({
              'stampImpressionsRed': parseInt(_val, 10) + 1
            });
          } else {
            return usersRoot.child(merchant).child('stamps').child('stats').child('local').set({
              'stampImpressionsRed': 1
            });
          }
        });
      });
    } else {
      defX.promise.then(function(bitsCollected) {
        _defX.resolve(bitsCollected);
      });
    }
    return _defX.promise;
  };

  notifyMerchantCronStart = function() {
    var CronJob, job, jobStatus;
    CronJob = require('cron').CronJob;
    job = new CronJob({
      cronTime: '00 30 11 * * 1-7',
      onTick: function() {
        return notifyMerchant('all');
      },
      start: true,
      timeZone: "America/Chicago"
    });
    return jobStatus = job.start();
  };

  notifyMerchantCronStart();

  module.exports = stampImpression;

}).call(this);
