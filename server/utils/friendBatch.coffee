###
# fileOverview

        ___      _                 _ ______                   _
       / __)    (_)               | (____  \         _       | |
     _| |__ ____ _ _____ ____   __| |____)  )_____ _| |_ ____| |__
    (_   __) ___) | ___ |  _ \ / _  |  __  ((____ (_   _) ___)  _ \
      | | | |   | | ____| | | ( (_| | |__)  ) ___ | | |( (___| | | |
      |_| |_|   |_|_____)_| |_|\____|______/\_____|  \__)____)_| |_|

## description

    User gets stamped,
    go through each individual stamp
    aggregate the yellow of their friends
    for each of these stamps, create a success for each of these (unless the stamp to create has already been created bc 
    that's the same stamp at inaugural stamping) and write blue. Yellow 
    increments as per normal. For each aggregation of +1, write that in 
    blue, if stamp doesn't exist, create stamp success and write 1 in blue.
    — James D

###

Firebase = require('firebase')
_ = require('lodash')
try
    q = require('Q')
catch e
    q = require('q')

baseDataApiUrl = 'https://lovestamp.firebaseio.com/'

friendsBatchIntensityRoundup = (username, provider) ->
    ###

    @description Utilities for collocating users by shared stampenings from 
                 particular merchants.

    @namespace friendsBatchIntensity
    @param {string} username
    @param {array} merchants Merchants of a given user's stats/success branch.

    ###

    Users = new Firebase("#{baseDataApiUrl}users")
    updatedStampImpressionsBlue = false
    def = q.defer()

    socialProvidersAvailable =
        '@': 'twitter'
        '+': 'google'
        'ᶠ': 'facebook'

    _currentCustomer = Users.child(username)
    _friends = _currentCustomer
        .child('friends')
        .child(socialProvidersAvailable[provider])

    # Stamps IDs
    _stamps = _currentCustomer.
        child('stamps').
        child('stats').
        child('success')

    t = []

    customerConstruct = q.all [
        _stamps
        _friends
    ]

    customerConstruct.then (data) ->
        stamps = data[0]
        friends = data[1]

        stamps.on 'value', (stampData) ->
            friends.on 'value', (friendData, keyFriends) ->

                a = []
                _.forEach friendData.val(), (friendUsername, friendUserKey) ->

                    friendHandle = friendUsername.handle

                    _stampsFriend = Users.child(friendHandle).
                        child('stamps').
                        child('stats').
                        child('success')

                    _stampsFriend.once 'value', (s) ->

                        if s.val() != null
                            _.forEach s.val(), (ss, key) ->
                                a.push {
                                    key: key
                                    stampImpressionsYellow: ss.stampImpressionsYellow
                                }

                z = _.groupBy a, 'key'

                _z = _.map z, (stampeningHistory) ->
                    _qty = 0
                    _.forEach stampeningHistory, (sh) ->
                        _qty += sh.stampImpressionsYellow
                    {
                        stampId: _.last(stampeningHistory).key
                        aggregateStampImpressionsYellow: _qty
                    }


                _.forEach stampData.val(), (customerStampData, keyStamp) ->

                    _.forEach friendData.val(), (friendUsername, friendUserKey) ->

                        friendHandle = friendUsername.handle

                        _stampsFriend = Users.child(friendHandle).
                            child('stamps').
                            child('stats').
                            child('success')

                        _stampsFriend.on 'value', (friendStampData) ->

                            _.forEach friendStampData.val(), (_friendStampData, _keyFriendStampData) ->
                                if keyStamp == _keyFriendStampData
                                    foundStamp = _.find _z, { stampId: keyStamp }

                                    CurrentStampStory = _stamps.
                                        child(keyStamp).
                                        child('stampImpressionsBlue').
                                        set parseInt(foundStamp.aggregateStampImpressionsYellow, 10)


friendsBatchIntensityRoundupCron = () ->
    ###
    @namespace friendsBatchIntensityRoundup
    @return {object} jobStatus A status of the initiated job.
    ###

    CronJob = require('cron').CronJob
    job = new CronJob({
        cronTime: '00 30 11 * * 1-7'
        onTick: () ->
            # Runs every day at 11:30:00 AM.
            #friendsBatchIntensityRoundup()
        start: false
        timeZone: "America/Chicago"
    })

    jobStatus = job.start()


module.exports =
    friendsBatchIntensityRoundup     : friendsBatchIntensityRoundup
    friendsBatchIntensityRoundupCron : friendsBatchIntensityRoundupCron
