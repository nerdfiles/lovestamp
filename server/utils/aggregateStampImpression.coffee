###*
@fileOverview

                           _                                   _
                          (_)                                 (_)
  ____  ____ ___  ____     _ ____  ____   ____ _____  ___  ___ _  ___  ____
 / ___)/ ___) _ \|  _ \   | |    \|  _ \ / ___) ___ |/___)/___) |/ _ \|  _ \
( (___| |  | |_| | | | |  | | | | | |_| | |   | ____|___ |___ | | |_| | | | |
 \____)_|   \___/|_| |_|  |_|_|_|_|  __/|_|   |_____|___/(___/|_|\___/|_| |_|
                                  |_|

@description

    A regularly scheduled roll-up of leftover monthly stamp impressions.

###

Firebase = require('firebase')
Decimal = require('decimal.js')
_ = require('lodash')
Q = require('q')
Chance = require('chance')
moment = require('moment')

baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 


###
@namespace budgetCron
@usage
  budgetCron()
###

budgetCron = () ->
  LSRoot = new Firebase(baseDataApiUrl)
  usersRoot = LSRoot.child('users')
  balancesRoot = LSRoot.child('balances')

  setBudgetSource = balancesRoot.child('setBudget') # set from running, otherwise it inherits from payout day and is decremented by payouts
  setBudget = Q.defer()
  setBudgetSource.once 'value', (setBudgetSnapshot) ->
    setBudget.resolve setBudgetSnapshot.val()

  payoutBudgetSource = balancesRoot.child('payoutBudget')
  payoutBudget = Q.defer()
  payoutBudgetSource.once 'value', (payoutBudgetSnapshot) ->
    payoutBudget.resolve payoutBudgetSnapshot.val()

  revenueSource = balancesRoot.child('revenue')
  revenue = Q.defer()
  revenueSource.once 'value', (revenueSnapshot) ->
    revenue.resolve revenueSnapshot.val()

  impressionsContainer = []
  now = moment()

  defImpressions = Q.defer()
  def = Q.defer()
  clearedUserImpressions = Q.defer()
  _clearedUserImpressions = []

  c = 1
  _c = 0
  imps = []
  usersRoot.on("child_added", (userSnapshot, key) ->
    c++
    _c = userSnapshot.numChildren()
    users = _.map userSnapshot.val(), (userComponent, _key) ->
      return  if not _key
      if _key == 'impressions'
        return _userObject = _.map userComponent, (impression) ->
          d = new Date(impression.expiry)
          expiration = moment(d)
          if expiration.isBefore(now)
            _clearedUserImpressions.push {
              username: userSnapshot.key()
              impressions: userComponent
            }

            # Clear these out if they're completed.
            return {
              imp: impression or ''
            }

    #u = _.reject(users, (_u) ->
      #_u == undefined
    #)
    imps.push users

    if _c <= c++
      clearedUserImpressions.resolve _clearedUserImpressions
      def.resolve imps
  )

  usersRoot.on("child_added", (userSnapshot, key) ->
    user = userSnapshot
    userObject = user.val()
    impressions = userObject.impressions
    if impressions
      _.forEach impressions, (impressionsItem, key) ->
        impressionsContainer.push impressionsItem.quantity
    defImpressions.resolve impressionsContainer
  )

  defImpressions.promise.then (_aggregateStampImpression) ->
    console.log '/////'
    console.log _aggregateStampImpression
    console.log '/////'

    aggregateStampImpression = _.reduce(_aggregateStampImpression, (sum, n) ->
      sum + n
    )

    def.promise.then (users) ->
      _users = _.flatten(users, true)

      _r = _.filter _users, (_u) ->
        _u != undefined

      __r = _.map _r, (_p) ->
        _p.imp.quantity

      console.log __r
      console.log aggregateStampImpression

      payoutBudget.promise.then (_payoutBudget) ->

        X_1 = (_payoutBudget / aggregateStampImpression) * __r

        revenue.promise.then (static_revenue) ->
          revenueSource.set (static_revenue + X_1)

        payoutBudgetSource.set (_payoutBudget - X_1)

        clearedUserImpressions.promise.then (clearedUsersList) ->
          _.forEach clearedUsersList, (user) ->
            console.log "Reading user #{user}"

            _.forEach user.impressions, (item, key) ->
              console.log("Removing impressions from #{user.username} with key: #{key}")
              usersRoot.child(user.username).child('impressions').child(key).remove()

  return true


###*
@namespace budgetCronStart
@return {object} jobStatus A status of the initiated job.
###

budgetCronStart = () ->
  CronJob = require('cron').CronJob
  job = new CronJob({
    cronTime: '00 10 17 * * 1-7'
    #cronTime: '*/1 * * * * *'
    onTick: () ->
      # Runs every day at 11:30:00 AM.
      budgetCron()
    start: false
    timeZone: "America/Chicago"
  })

  jobStatus = job.start()

module.exports =
    budgetCronStart: budgetCronStart
    budgetCron: budgetCron
