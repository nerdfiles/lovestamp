
/**
@fileOverview

                  _
                 | |
 ____  _   _  ___| |__
|  _ \| | | |/___)  _ \
| |_| | |_| |___ | | | |
|  __/|____/(___/|_| |_|
|_|
 */

(function() {
  var Curl, apn, apnNotification, credentials, curlNotification, gcm, gcmNotification, http, url, _credentials;

  http = require('http');

  apn = require('apn');

  url = require('url');

  gcm = require('node-gcm');

  Curl = require("node-curl/lib/Curl");

  _credentials = require('../config/credentials');

  credentials = _credentials.credentials();

  apnNotification = function(requestBody) {
    var apnsConnection, callback, device, iPhone5, iPhone6, launchImagePath, note, notificationSoundPath, options;
    console.log(requestBody);
    launchImagePath = 'http://lovestamp.io:8080/images/launchImagePath.png';
    notificationSoundPath = 'http://lovestamp.io:8080/sounds/beep.wav';
    iPhone6 = '7f6c896c6491e32c61bbd2aac154ccb22568fab1b822052db1b81a99e9322048';
    iPhone5 = '3canc1493275761472e9a0b93fad10e0176e4b4692e1f48449462ff71fdbb013';
    device = new apn.Device(iPhone6);
    note = new apn.Notification;
    note.badge = 1;
    note.sound = notificationSoundPath;
    note.contentAvailable = 1;
    note.alert = {
      'body': message,
      'action-loc-key': 'Show Me!',
      'launch-image': launchImagePath
    };
    note.payload = {
      'messageFrom': 'LoveStamp'
    };
    note.device = device;
    callback = function(errorNum, notification) {
      console.log('Error is: %s', errorNum);
      console.log('Note ' + JSON.stringify(notification));
    };
    options = {
      gateway: 'gateway.sandbox.push.apple.com',
      errorCallback: callback,
      cert: credentials.certPath,
      key: credentials.keyPath,
      passphrase: credentials.passphrase,
      port: 2195,
      enhanced: true,
      cacheLength: 100
    };
    apnsConnection = new apn.Connection(options);
    console.log('Note ' + JSON.stringify(note));
    return apnsConnection.sendNotification(note);
  };

  curlNotification = function(credentials, message) {
    var apiKey, curl, fields, headers, messageConstruct, p, registrationIDs;
    p = console.log;
    url = process.argv[2];
    curl = new Curl();
    url = 'https://android.googleapis.com/gcm/send';
    apiKey = credentials.google.android.apiKey;
    registrationIDs = [];
    messageConstruct = '';
    fields = [
      {
        'registration_ids': registrationIDs,
        'data': [
          {
            "message": message
          }
        ]
      }
    ];
    headers = ['Authorization: key=' + apiKey, 'Content-Type: application/json'];
    curl.setopt("CONNECTTIMEOUT", 2);
    curl.setopt("URL", url);
    curl.setopt('POST', 1);
    curl.setopt('HTTPHEADER', headers);
    curl.setopt('RETURNTRANSFER', 1);
    curl.setopt('CURLOPT_POSTFIELDS', fields);
    curl.on("data", function(chunk) {
      p("receive " + chunk.length);
      return chunk.length;
    });
    curl.on("header", function(chunk) {
      p("receive header " + chunk.length);
      return chunk.length;
    });
    curl.on("error", function(e) {
      p("error: " + e.message);
      curl.close();
    });
    curl.on("end", function() {
      p("code: " + curl.getinfo("RESPONSE_CODE"));
      p("done.");
      curl.close();
    });
    return curl.perform();
  };

  gcmNotification = function() {
    var apiKey, message, registrationIds, sender;
    message = new gcm.Message;
    apiKey = 'AIzaSyCmTg7xqTWCCfvxWom-D7pNg1pEQLvkVZY';
    sender = new gcm.Sender(apiKey);
    registrationIds = [];
    message.addData('message', '✌ Peace, Love ❤ and PhoneGap ✆!');
    message.addData('title', 'Push Notification Sample');
    message.addData('msgcnt', '3');
    message.timeToLive = 3000;
    registrationIds.push('APA92cGtSvVZudbg8ef_CrzPiMiXasKlt1NzYi3FRX7a1z1rpBqBqvT6TvUJL7RHABYviYyL6Q9jw5jTbpRjpkLhrP0f2r11c3bHGUJuUm-eaOXP0QKp0aIpWJIQTHTVQ4prRHuwJYvi8S16cO_B_5LrhfuFthQ9uw5bQXQ3OGngF8N-dmJu-kE');

    /**
     * Parameters: message-literal, registrationIds-array, No. of retries, callback-function
     */
    return sender.send(message, registrationIds, 4, function(result) {
      console.log(result);
    });
  };

  module.exports = {
    apnNotification: apnNotification,
    curlNotification: curlNotification,
    gcmNotification: gcmNotification
  };

}).call(this);
