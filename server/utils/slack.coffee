
###
@description

  Extended management of backend business objects through 
  multi-platform messaging.

###

token = '5269b2af12df88ac503fa740c4f87f37'
SLACK_REQUESTS_WEBHOOK_URL = 'https://hooks.slack.com/services/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'
requests_slack = require('slack-notify')(SLACK_REQUESTS_WEBHOOK_URL)

SLACK_PAYMENTS_WEBHOOK_URL = 'https://hooks.slack.com/services/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'
payments_slack = require('slack-notify')(SLACK_PAYMENTS_WEBHOOK_URL)


module.exports =
  requests_slack: requests_slack
  payments_slack: payments_slack
