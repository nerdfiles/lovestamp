(function() {
  var onNotificationAPN, onNotificationGCM;

  onNotificationAPN = function(e) {
    var snd;
    if (e.alert) {
      console.log("push-notification: " + e.alert);
      navigator.notification.alert(e.alert);
    }
    if (e.sound) {
      snd = new Media(e.sound);
      snd.play();
    }
    if (e.badge) {
      pushNotification.setApplicationIconBadgeNumber(successHandler, e.badge);
    }
  };

  onNotificationGCM = function(event) {
    var my_media;
    switch (event.event) {
      case "registered":
        if (event.regid.length > 0) {
          console.log("REGISTERED -> REGID:" + event.regid);
          console.log("regID = " + event.regid);
          return fn({
            type: "registration",
            id: event.regid,
            device: "android"
          });
        }
        break;
      case "message":
        if (event.foreground) {
          console.log("INLINE NOTIFICATION");
          my_media = new Media("/android_asset/www/" + event.soundname);
          my_media.play();
        } else {
          if (event.coldstart) {
            console.log("COLDSTART NOTIFICATION");
          } else {
            console.log("BACKGROUND NOTIFICATION");
          }
        }
        navigator.notification.alert(event.payload.message);
        console.log("MESSAGE -> MSG: " + event.payload.message);
        console.log("MESSAGE -> MSGCNT: " + event.payload.msgcnt);
        return console.log("MESSAGE -> TIME: " + event.payload.timeStamp);
      case "error":
        return console.log("ERROR -> MSG:" + event.msg);
      default:
        return console.log("EVENT -> Unknown, an event was received and we do not know what it is");
    }
  };

}).call(this);
