###
# fileOverview

     _                                   _
    (_)                                 (_)
     _ ____  ____   ____ _____  ___  ___ _  ___  ____
    | |    \|  _ \ / ___) ___ |/___)/___) |/ _ \|  _ \
    | | | | | |_| | |   | ____|___ |___ | | |_| | | | |
    |_|_|_|_|  __/|_|   |_____|___/(___/|_|\___/|_| |_|
            |_|

## description

    Registration code for detecting stamp unique sign stamp signatures prepared by Graham Scans[0].

    —
    [0]: http://en.wikipedia.org/wiki/Graham_scan

###

Firebase = require('firebase')
numeral = require('numeral')
_ = require('lodash')
Q = require('q')
Chance = require('chance')
nodemailer = require('nodemailer')
smtpTransport = require('nodemailer-smtp-transport')
moment = require('moment')
fs = require('fs')

baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 

loadHtmlFile = (_fp) ->
  def = Q.defer()
  fp = if _fp then _fp else '/test.txt'
  fullPath = __dirname + '/' + fp
  fs.readFile(fullPath, (err, data) ->
    #console.log err
    if err
      def.reject err
      return
    def.resolve data.toString()
  )
  def.promise


notifyMerchant = (merchant, expiryImpressions, expiryDateTime) ->
  UsersRoot = new Firebase(baseDataApiUrl + 'users')
  merchantRoot = UsersRoot.child(merchant)
  userStampImpressions = merchantRoot.child('impressions')

  send = (merchant, expiryImpressions, expiryDateTime) ->

    merchantRoot = UsersRoot.child(merchant)
    transport = nodemailer.createTransport(smtpTransport({
      host             : 'smtp.lovestamp.io'
      port             : 587
      debug            : true
      secure           : false
      secureConnection : false
      ignoreTLS        : false
      tls:
        rejectUnauthorized:false
        #ciphers:'SSLv3'
      auth:
        user: 'aaron'
        pass: 'cCNkd0BpKQ3UHg=='
    }))

    messageText = "Hello, fellow LoveStamper! \n\n You currently have #{expiryImpressions} stamp impressions left and an expiry date of #{expiryDateTime} \n\n Please top up <a href='http://merchant.lovestamp.io/store'>here</a>, so you can keep you're LoveStamp active.\n\nThe LoveStamp Mailman"
    messageHtml = "<p>Hello, fellow LoveStamper!</p> <p>You currently have #{expiryImpressions} stamp impressions left and an expiry date of #{expiryDateTime}</p><p>Please top up <a href='http://merchant.lovestamp.io/store'>here</a>, so you can keep you're LoveStamp active.</p><p>The LoveStamp Mailman</p>"

    _merchantEmail = merchantRoot.
      child('profile').
      child('overview').
      child('email')

    _merchantEmail.once 'value', (merchantEmailSnapshot) ->
      merchantEmail = merchantEmailSnapshot.val()

      htmlEmailTemplateFilePath = '../../email/newsletter/purchase.inline.html'

      loadHtmlFile(htmlEmailTemplateFilePath).then (htmlEmailConstruct) ->

        transport.sendMail(
          from    : 'support@lovestamp.io'
          to      : merchantEmail
          subject : 'Your LoveStamp needs refueling!'
          text    : messageText
          html    : htmlEmailConstruct
        )

  if merchant != 'all'

    userStampImpressions.once('value', (snapshot) ->

      _.forEach(snapshot.val(), (i, key) ->

        _impression = parseInt(i.quantity, 10) - 1
        _expiry = i.expiry

        userStampImpressions.child(key).set {
          'quantity' : _impression
          'expiry'   : _expiry
        }

        send(merchant, _impression, _expiry)
      )
    )

  else
    merchantsGroupExpired = UsersRoot
    merchantsGroupExpired.once 'value', (merchantsGroupSnapshot) ->
      _.forEach(merchantsGroupSnapshot.val(), (i, key) ->

        _i = i.impressions

        __i = _.last(_.toArray(_i))

        if __i

          _merchant = key

          impression = parseInt(__i.quantity, 10)
          expiry = __i.expiry

          currentDateTime = moment().add(5, 'days')
          expiryDateTime = moment(expiry)

          if currentDateTime.isAfter( expiryDateTime )

            #console.log '_merchant'
            #console.log _merchant
            send(_merchant, impression, expiry)

      )

  return

createImpression = (path, stampId, impressionObject) ->
  ###
  @namespace go
  @descriptinon 

  Pass a known stampId into our Firebase backend to check if the stamp 
  already exists.

  ###

  _def = Q.defer()
  stampData = impressionObject

  tryCreateStamp(path, stampId, stampData)

  #console.log stampData
  #console.log(baseDataApiUrl + path)
  #console.log stampId
  #console.log stampData
  stampsRef = new Firebase(baseDataApiUrl + path)

  stampsRef.child(stampId).push({
    bitsCollected: stampData.bitsCollected
  }, (error) ->
    #console.log 'error check'
    #console.log error
    if error
      #console.log 'error'
      #console.dir error
      _def.reject {
        status: 'error'
        $meta: error
      }
    else
      _def.resolve true
  )

  _def.promise


stampCreated = (path, stampId, success, stampData) ->
  ###
  @namespace stampCreated
  @description

  Messaging around created stamps.

  ###

  if ! success
    console.log('Stamp ' + stampId + ' already exists!')
  else
    console.log('Successfully created ' + stampId)

  return


tryCreateStamp = (path, stampId, stampData) ->
  ###
  @namespace tryCreateStamp
  @description

  Tries to set /stamps/<stampId> to the specified data, but only
  if there's no data there already. Also tries to push a new 
  bitsCollected object and attempts to update stampImpressionsYellow.

  ###

  p = (baseDataApiUrl + path)
  stampsRef = new Firebase p
  s = stampsRef.child(stampId)

  #s.once('value', (snapshot) ->
    #if snapshot.hasChild(stampId)
        #_stamp = snapshot.child(stampId)
        #_stamp.child('bitsCollected').set stampData.bitsCollected
        #_stamp.child('stampImpressionsYellow').set stampData.stampImpressionsYellow
  #)

  s.transaction((currentStampData) ->
    if currentStampData isnt null
        return currentStampData
    else
        return stampData
  , (error, committed) ->
    stampCreated(path, stampId, committed, stampData)
    s.child('bitsCollected').set stampData.bitsCollected
    s.child('stampImpressionsYellow').set stampData.stampImpressionsYellow
  )


stampImpression = (username, merchant, stampId, provider, createTipUrl) ->
  ###
  @module utils
  @namespace stampImpression
  @description

  A typical stamp impression scenario.

  @param {string} username With Social Media prefix.
  @return {number} X_2
  ###
  defX = Q.defer()
  _defX = Q.defer()
  #console.log 'def' + username

  chance = new Chance()
  balancesRoot = new Firebase(baseDataApiUrl + 'balances')
  usersRoot = new Firebase(baseDataApiUrl + 'users')
  merchantRoot = new Firebase(baseDataApiUrl + 'users/' + merchant)
  userRoot = new Firebase(baseDataApiUrl + 'users/' + username)
  #userStampImpressions = userRoot.child('impressions')
  userStampImpressions = merchantRoot.child('impressions')

  ## Getters

  try
    updatedUserStampImpressionsYellow = userRoot.
      child('stamps').
      child('stats').
      child('success').
      child(stampId).
      child('stampImpressionsYellow')
  catch e
    console.log 'Could not find stampId stampImpressionsYellow for ', username

  try
    updatedMerchantStampImpressionsRed = merchantRoot.
      child('stamps').
      child('stats').
      child('local').
      child('stampImpressionsRed')
  catch e
    console.log 'Could not find stampImpressionsRed for ', merchant

  try
    updatedBitsCollected = userRoot.
      child('stamps').
      child('stats').
      child('success').
      child(stampId).
      child('bitsCollected')
  catch e
    console.log 'Could not find stampId bitsCollected ', username

  updatedPurse = userRoot.
    child('profile').
    child('purse')

  setBudgetSource = balancesRoot.child('setBudget') # set from running, otherwise it inherits from payout day and is decremented by payouts
  setBudget = Q.defer()
  setBudgetSource.once 'value', (setBudgetSnapshot) ->
    setBudget.resolve setBudgetSnapshot.val()

  payoutBudgetSource = balancesRoot.child('payoutBudget')
  payoutBudget = Q.defer()
  payoutBudgetSource.once 'value', (payoutBudgetSnapshot) ->
    payoutBudget.resolve payoutBudgetSnapshot.val()

  # Shadow Budget is used for on the fly calculations of the application's randomizer.
  # It is used to set the min/max for randomization.
  _payoutShadowBudgetSource = balancesRoot.child('payoutShadowBudget')
  _payoutShadowBudget = Q.defer()
  _payoutShadowBudgetSource.once 'value', (payoutShadowBudgetSnapshot) ->
    _payoutShadowBudget.resolve payoutShadowBudgetSnapshot.val()

  # Calculate the necessary aggregate pending pool for updating the application's 
  # revenue model.
  _aggregatePendingPool = 0
  _aggregatePendingPoolVal = Q.defer()
  usersRoot.once 'value', (usersData) ->
    _.forEach usersData.val(), (userData) ->

      if userData.profile and userData.profile.purse
        _purse = parseFloat(numeral(userData.profile.purse).format('0.00'))
        _aggregatePendingPool = (parseFloat(numeral(_purse).format('0.00')) + parseFloat(numeral(_aggregatePendingPool).format('0.00')))

    _aggregatePendingPoolVal.resolve _aggregatePendingPool

  revenueSource = balancesRoot.child('revenue')
  revenue = Q.defer()
  revenueSource.once 'value', (revenueSnapshot) ->
    revenue.resolve revenueSnapshot.val()

  defImpressions = Q.defer()
  impressionsContainer = []

  usersRoot.once("value", (userSnapshot, key) ->
    _.forEach userSnapshot.val(), (userData) ->
      userObject = userData
      #console.dir userObject
      impressions = userObject.impressions
      if impressions
        _.forEach impressions, (impressionItem, key) ->
          qty = parseInt(impressionItem.quantity, 10)
          expiry = impressionItem.expiry

          currentDateTime = moment()
          expiryDateTime = moment(expiry)

          if currentDateTime.isAfter( expiryDateTime ) or currentDateTime.isSame( expiryDateTime )
            return

          if qty is 0
            return

          impressionsContainer.push qty
    defImpressions.resolve impressionsContainer
  )

  defImpressions.promise.then((_aggregateStampImpression) ->
    #console.log _aggregateStampImpression

    aggregateStampImpression = _.reduce(_aggregateStampImpression, (sum, n) ->
      sum + n
    )

    payoutBudget.promise.then((_payoutBudget) ->

      # Modify our budget per cashout
      if createTipUrl == true
        updatedPurse.once('value', (_purse) ->
          currentPurse = parseFloat(numeral(_purse.val()).format('0.00'))
          payoutBudgetSource.set(parseFloat(numeral(_payoutBudget).format('0.00')) - parseFloat(numeral(currentPurse).format('0.00')))

          # Tell storeBits to set purse to 0
          defX.resolve 0
        )

      else

        _payoutShadowBudget.promise.then((payoutShadowBudgetValue) ->

          X_1 = parseFloat(numeral(payoutShadowBudgetValue).format('0.00')) / parseFloat(numeral(aggregateStampImpression).format('0.00'))
          #console.log payoutShadowBudgetValue
          #console.log aggregateStampImpression

          maximumComponent = (90 / 100) * X_1 # load from Firebase budget setting
          minimumComponent = (.5 / 100) * X_1
          tipConstruct =
            max   : maximumComponent
            min   : minimumComponent
            fixed : 2

          X_2 = chance.floating tipConstruct

          updated_revenue = (X_1 - X_2)

          # Updates the impressions used for the merchant
          userStampImpressions.once('value', (snapshot) ->
            if snapshot.val() == null
              console.log 'Merchant does not have any impressions available.'
              defX.resolve false
              return

            _.forEach(snapshot.val(), (i, key) ->

              _impression = parseInt(i.quantity, 10) - 1
              _expiry = i.expiry

              userStampImpressions.child(key).set {
                'quantity' : _impression
                'expiry'   : i.expiry
              }

              _now = moment()
              currentDateTime = moment().add(3, 'days')
              expiryDateTime = moment(_expiry)

              if _now.isAfter(expiryDateTime) or _impression == 0
                defX.resolve(false)
                return
              else
                revenue.promise.then (static_revenue) ->

                  _updatedRevenue = parseFloat(numeral(static_revenue).format('0.00')) + parseFloat(numeral(updated_revenue).format('0.00'))
                  revenueSource.set _updatedRevenue

                  _aggregatePendingPoolVal.promise.then (static_aggregatePendingPool) ->

                    updatedPayoutShadowBudget = parseFloat(numeral(_payoutBudget).format('0.00')) - (parseFloat(numera(static_aggregatePendingPool).format('0.00')) + parseFloat(numeral(static_revenue).format('0.00')))
                    _payoutShadowBudgetSource.set updatedPayoutShadowBudget

                defX.resolve(X_2)

              if currentDateTime.isAfter( expiryDateTime )

                notifyMerchant(merchant, _impression, _expiry)

              if _impression < 2

                notifyMerchant(merchant, _impression, _expiry)

            )
          )
        )
    )

  )

  # Wrap up normal stamp impression
  if createTipUrl != true

    defX.promise.then (bitsCollected) ->
      if bitsCollected == false
        _defX.resolve false
        return

      _defX.resolve(bitsCollected)

      updatedUserStampImpressionsYellow.once 'value', (snapshot) ->

        exists = (snapshot.val() != null)

        if exists
          _val = snapshot.val()
          impressionObject =
            'stampImpressionsYellow' : parseInt(_val, 10) + 1
            'bitsCollected'          : bitsCollected
        else
          impressionObject =
            'stampImpressionsYellow' : 1
            'bitsCollected'          : bitsCollected

        #console.log 'impressionObject:'
        #console.dir impressionObject
        createImpression("users/" + username + "/stamps/stats/success", stampId, impressionObject).then((status) ->
          console.log 'createImpression'
          console.log status
        , (errorData) ->
          if errorData.status == 'error'
            console.log 'errorData'
            console.log errorData
            # Even if we cannot update our color, we should pass our financial data on.
            #_defX.reject errorData
        )

      # Set last bitsCollected
      userBitsCollected = userRoot.
        child('stamps').
        child('stats').
        child('success').
        child(stampId).
        child('bitsCollected').set(parseFloat(numeral(updatedBitsCollected).format('0.00')) + parseFloat(numeral(bitsCollected).format('0.00')))

      updatedMerchantStampImpressionsRed.once('value', (snapshot) ->
          exists = (snapshot.val() != null)

          if exists
              _val = snapshot.val()
              usersRoot.child(merchant).
                child('stamps').
                child('stats').
                child('local').
                set { 'stampImpressionsRed': parseInt(_val, 10) + 1 }
          else
              usersRoot.child(merchant).
                child('stamps').
                child('stats').
                child('local').
                set { 'stampImpressionsRed': 1 }
      )

  else

    defX.promise.then (bitsCollected) ->
      _defX.resolve bitsCollected
      return

  _defX.promise


notifyMerchantCronStart = () ->
  CronJob = require('cron').CronJob
  job = new CronJob({
    cronTime: '00 30 11 * * 1-7'
    onTick: () ->
      # Runs every day (usually at reboot).
      notifyMerchant('all')
    start: true
    timeZone: "America/Chicago"
  })

  jobStatus = job.start()


notifyMerchantCronStart()
#notifyMerchant('all')

module.exports = stampImpression
