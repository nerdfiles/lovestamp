
/**
 * Module dependencies.
 */

(function() {
  var InternalOAuthError, OAuth2Strategy, Strategy, util;

  util = require('util');

  OAuth2Strategy = require('passport-oauth').OAuth2Strategy;

  InternalOAuthError = require('passport-oauth').InternalOAuthError;


  /**
   * Inherit from `OAuth2Strategy`.
   */

  Strategy = function(options, verify) {

    /**
     * `Strategy` constructor.
     *
     * The ChangeTip authentication strategy authenticates requests by delegating to
     * ChangeTip using the OAuth 2.0 protocol.
     *
     * Applications must supply a `verify` callback which accepts a `accessToken`,
     * `refreshToken` and service-specific `profile`, and then calls the `done`
     * callback supplying a `user`, which should be set to `false` if the
     * credentials are not valid.  If an exception occured, `err` should be set.
     *
     * Options:
     *   - `clientID`     identifies client to ChangeTip
     *   - `clientSecret`  secret used to establish ownership of the client key
     *   - `callbackURL`     URL to which ChangeTip will redirect the user after obtaining authorization
     *
     * Examples:
     *
     *     passport.use(new ChangeTipStrategy({
     *         clientID: '123-456-789',
     *         clientSecret: 'shhh-its-a-secret'
     *       },
     *       function(accessToken, refreshToken, profile, done) {
     *         console.log(accessToken);
     *         return done(null, profile);
     *       }
     *     ));
     *
     * @param {Object} options
     * @param {Function} verify
     * @api public
     */
    var _options;
    _options = options || {};
    _options.authorizationURL = options.authorizationURL;
    _options.tokenURL = options.tokenURL;
    _options.clientID = options.clientID;
    _options.clientSecret = options.clientSecret;
    _options.callbackURL = options.callbackURL;
    _options.passReqToCallback = options.passReqToCallback;
    _options.accessType = 'offline';
    _options.approvalPrompt = 'force';
    OAuth2Strategy.call(this, _options, verify);
    this.name = 'changetip';
  };

  util.inherits(Strategy, OAuth2Strategy);

  Strategy.prototype.userProfile = function(accessToken, done) {

    /**
     * Retrieve user profile from ChangeTip.
     *
     * This function constructs a normalized profile, with the following properties:
     *
     *   - `id`
     *   - `username`
     *   - `displayName`
     *
     * @param {String} token
     * @param {String} tokenSecret
     * @param {Object} params
     * @param {Function} done
     * @api protected
     */
    this._oauth2.get('https://api.changetip.com/v2/users', accessToken, function(err, body, res) {
      var e, json, profile;
      if (err) {
        return done(new InternalOAuthError('failed to fetch user profile', err));
      }
      try {
        json = JSON.parse(body);
        profile = {
          provider: 'changetip'
        };
        profile.id = json._id;
        profile.username = json.name;
        profile.displayName = json.display_name;
        profile.email = json.email;
        profile._raw = body;
        profile._json = json;
        done(null, profile);
      } catch (_error) {
        e = _error;
        done(e);
      }
    });
  };


  /**
   * Expose `Strategy`.
   */

  module.exports = Strategy;

}).call(this);
