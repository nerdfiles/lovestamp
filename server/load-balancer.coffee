###*
@fileOverview

 _                 _       _           _
| |               | |     | |         | |
| | ___  _____  __| |_____| |__  _____| | _____ ____   ____ _____  ____
| |/ _ \(____ |/ _  (_____)  _ \(____ | |(____ |  _ \ / ___) ___ |/ ___)
| | |_| / ___ ( (_| |     | |_) ) ___ | |/ ___ | | | ( (___| ____| |
 \_)___/\_____|\____|     |____/\_____|\_)_____|_| |_|\____)_____)_|

@description

  Addresses to use in the round robin proxy, since Node.js Express is hosting 
  multiple key resources: docs, reports, the main API, REST endpoints, etc., 
  and a load-balancer gives us a single point of failure.

  Node.js runs the application itself, while nginx deals with PR site.

@see https://mazira.com/blog/introduction-load-balancing-nodejs
@usage

    $ pm2 start ./apps.json

@TODO Loggly implementation.
###

args = process.env.NODE_PORT

https = require('https')
fs = require('fs')
httpProxy = require('http-proxy')
servers = [
  'http://127.0.0.1:8083'
  'http://127.0.0.1:8084'
  'http://127.0.0.1:8085'
]
proxy = httpProxy.createProxyServer()
count = 0

loadBalanceProxy = (req, res) ->
  cur = currentServer % servers.length
  currentServer++
  target = servers[cur]
  proxy.web req, res, target: target
  return

ssl_opts =
  cert: fs.readFileSync(__dirname + '/../certs/lovestamp.io.new.crt', 'utf8')
  key: fs.readFileSync(__dirname + '/../certs/lovestamp.io.key', 'utf8')

https.createServer(ssl_opts, (req, res) ->
  loadBalanceProxy req, res
  return
).listen args

currentServer = 1
