(function() {
  var path;

  path = require('path');

  exports.index = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../app/index.html'));
  };

  exports.index_fan = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../app/index-fan.html'));
  };

  exports.index_merchant = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../app/index-merchant.html'));
  };

  exports.reports = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../reports/index.html'));
  };

  exports.api = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../docs/api/index.html'));
  };

  exports.docs_clients = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../docs/clients/index.html'));
  };

  exports.docs_server = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../docs/server/index.html'));
  };

  exports.humans = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../humans.txt'));
  };

  exports.robots = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../robots.txt'));
  };

  exports.pkg = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../package.json'));
  };

  exports.pkg = function(req, res) {
    return res.sendFile(path.resolve(__dirname + '../../../crossdomain.xml'));
  };

}).call(this);
