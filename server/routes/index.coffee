path = require('path')

exports.index = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../app/index.html'))

exports.index_fan = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../app/index-fan.html'))

exports.index_merchant = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../app/index-merchant.html'))

exports.reports = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../reports/index.html'))

exports.api = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../docs/api/index.html'))

exports.docs_clients = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../docs/clients/index.html'))

exports.docs_server = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../docs/server/index.html'))

exports.humans = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../humans.txt'))

exports.robots = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../robots.txt'))

exports.pkg = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../package.json'))

exports.pkg = (req, res) ->
  res.sendFile(path.resolve(__dirname + '../../../crossdomain.xml'))
