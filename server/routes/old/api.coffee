exports.states = (req, res) ->
    res.json({
         "data": {
              "locations": [
                {
                  "option": "state_na",
                  "queryText": "TX",
                  "displayText": "Texas - TX",
                  "addType": "search"
                },    {
                  "option": "state_na",
                  "queryText": "GA",
                  "displayText": "Georgia - GA",
                  "addType": "search"
                },   {
                  "option": "state_na",
                  "queryText": "VA",
                  "displayText": "Virginia - VA",
                  "addType": "search"
            }
        ]}
    })
