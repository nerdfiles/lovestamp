###
# fileOverview

           _                          _______ _
          | |                        (_______|_)
      ____| |__  _____ ____   ____ _____ _    _ ____
     / ___)  _ \(____ |  _ \ / _  | ___ | |  | |  _ \
    ( (___| | | / ___ | | | ( (_| | ____| |  | | |_| |
     \____)_| |_\_____|_| |_|\___ |_____)_|  |_|  __/
                            (_____|            |_|

@module server/social  

## description

Transactions with the [ChangeTip API](https://www.changetip.com/api).

###

Firebase = require('firebase')
request = require('request')
Chance = require('chance')
_ = require('lodash')
ChangeTip = require('changetip')
#_credentials = require('../../config/credentials')
_credentials = require('../config/credentials')
credentials = _credentials.credentials()
numeral = require('numeral')


try
  Q = require('q')
catch e
  Q = require('Q')

impression = require('../utils/impression')

#Bitly = require('bitly')
#bitly = new Bitly('o_22cms3saof', 'R_3363d1fb0bd44ee39ef06ac60e513a82')

baseDataApiUrl = 'https://lovestamp.firebaseio.com/'

storeBits = (customer, payout) ->
  ###
  @namespace storeBits
  @inner
  ###

  _currentPurse = Q.defer()

  LSRoot = new Firebase(baseDataApiUrl)
  usersRoot = LSRoot.child('users')
  u = usersRoot.child(customer)
  currentProfile = u.child('profile')
  currentPurse = currentProfile.child('purse')

  getBitsCollected = u.
    child('profile').
    child('bitsCollected')

  # Update with randomized payout
  if parseFloat(payout) > 0

    currentPurse.transaction((currentPurseSnapshot) ->
      if currentPurseSnapshot is null
        currentPurseSnapshot = 0
      u = parseFloat(numeral(currentPurseSnapshot).format('0.00')) + parseFloat(numeral(payout).format('0.00'))
      _currentPurse.resolve u
      return u
    , (error, committed) ->
      return
    )

    getBitsCollected.once 'value', (bitsCollectedSnapshot) ->
      _currentBitsCollected = bitsCollectedSnapshot.val() or 0
      getBitsCollected.set(parseFloat(numeral(_currentBitsCollected).format('0.00')) + parseFloat(numeral(payout).format('0.00')))
      return

  else

    currentPurse.once 'value', (currentPurseSnapshot) ->
      v = parseFloat(numeral(currentPurseSnapshot.val()).format('0.00'))
      _currentPurse.resolve(v)
    , (error) ->
      if error
        _currentPurse.resolve 'error'

  _currentPurse.promise


changetipHandler = (merchant, customer, stampId, __credentials, createTipUrl, domain) ->
  ###
  @description  
    The following function is an implementation of Natural Language Generation 
    models which are currently schemas for creating StoryBook Generators and 
    Document Micro/planners.
  @template  
    Message just do @{customer} you've been love stamped by @{merchant}! Bam {bits}.
  @example  
    curl https://api.changetip.com/v1/tips/?api_key=abcdef \
          -H "Content-Type: application/json" \
          -d '{"sender":"nick@changecoin","receiver":"jimlyndon@changecoin","context_uid":"64eee6d2a","message":"100 bits","channel":"slack"}'
  @example.response
        {
            "sender"         : "RattelyrDragon",
            "id"             : 850692,
            "magic_url"      : "http://tip.me/once/G3WE-Fm18kRoF",
            "amount_display" : "1 bit"
        }
  ###

  def = Q.defer()
  signalRandomizer = new Chance()
  exclamationList = [
    "Booya!"
    "Huzzah!"
    "Great!"
    "Cheers!"
    "Yahoo!"
  ]
  exclamationPick = Math.floor(Math.random() * exclamationList.length)
  exclamation = exclamationList[exclamationPick]
  urlPrefix =
    '@': 'https://twitter.com/'
    'ᶠ': 'https://facebook.com/'
    '✚': 'https://plus.google.com/'
  socialMediaNetwork =
    '@': 'twitter'
    'ᶠ': 'facebook'
    '✚': 'google'
  socialMediaAvailableHandles =
    'google'   : '101600582096097255226'
    'twitter'  : 'LoveStampio'
    'facebook' : 'LoveStamp IO' # 'LoveStamp IO' or '100009935520102'
  prefix = customer.charAt(0)
  provider = socialMediaNetwork[prefix]
  LSRoot = new Firebase(baseDataApiUrl)
  usersRoot = LSRoot.child('users')
  snapshotAuthData = undefined
  authData = usersRoot.child(customer)

  authData.once 'value', (_authData) ->
    snapshotAuthData = _authData.val()
    #console.log snapshotAuthData
    #
  currentProfile = authData.child('profile')

  # Capture purse value before calculations to revert...
  currentPurse = currentProfile.child('purse')
  _currentPurseData = null
  currentPurse.once 'value', (currentPurseSnapshot) ->
    _currentPurseData = currentPurseSnapshot.val()

  # Capture budget value before calculations to revert...
  balancesRoot = LSRoot.child('balances')
  payoutBudgetSource = balancesRoot.child('payoutBudget')
  _p = null
  payoutBudgetSource.once 'value', (payoutBudgetSnapshot) ->
    _p = payoutBudgetSnapshot.val()

  if merchant

    merchantPrefix = merchant.charAt(0)
    merchantProvider = socialMediaNetwork[merchantPrefix]
    merchantLink = merchant.replace(merchantPrefix, urlPrefix[merchantPrefix])

    merchantAuthData = usersRoot.child(merchant)

    merchantAuthDataAvailableAssociatedNetworks = merchantAuthData.
      child('profile').
      child('networks')

    merchantAuthDataAssociatedNetwork = merchantAuthDataAvailableAssociatedNetworks.
      child(merchantProvider)

    customerPickedMerchantAuthDataAssociatedNetwork = merchantAuthDataAvailableAssociatedNetworks.
      child(provider)

    __customerPickedMerchantAuthDataAssociatedNetwork = undefined
    merchantAuthDataAssociatedNetworkPick = undefined
    _networks = undefined
    snapshotMerchantAuthData = undefined

    merchantAuthData.on 'value', (_merchantAuthData) ->
      snapshotMerchantAuthData = _merchantAuthData.val()
      #console.log snapshotMerchantAuthData

    merchantAuthDataAvailableAssociatedNetworks.on 'value', (networks) ->
      _networks = networks.val()
      #console.log _networks

    merchantAuthDataAssociatedNetwork.on 'value', (_merchantAuthDataAssociatedNetworkPick) ->
      merchantAuthDataAssociatedNetworkPick = _merchantAuthDataAssociatedNetworkPick.val()
      #console.log merchantAuthDataAssociatedNetworkPick

    customerPickedMerchantAuthDataAssociatedNetwork.on 'value', (_customerPickedMerchantAuthDataAssociatedNetwork) ->
      __customerPickedMerchantAuthDataAssociatedNetwork = _customerPickedMerchantAuthDataAssociatedNetwork.val()
      #console.log __customerPickedMerchantAuthDataAssociatedNetwork

  console.log('createTipUrl:' + createTipUrl)

  impression(customer, merchant, stampId, provider, createTipUrl).then (_payout) ->
    console.log('payout:', _payout)
    ### Gives back 0 or != 0 for _payout ###
    if _payout == false
      def.resolve _payout
      return

    console.log('_payout: ' + _payout)
    tipApi = 'https://api.changetip.com/v2/tips/?'
    merchantComponent = merchant
    merchantComponentCleaned = merchantComponent
    attributionStreamComponent = "you've been LoveStamped by"
    satelliteStreamComponent = "Bam"
    moodStreamComponent = '!'
    moodStreamComponentFollowup = '.'
    moniker = 'bits'
    sep = ' '
    payout = _payout

    randomizedPayout = payout
    bitsComponent = randomizedPayout + sep + moniker
    channelComponent = if provider == 'google' then 'googleplus' else provider
    senderComponent = socialMediaAvailableHandles[provider]
    receiverComponent = customer.replace(prefix, '')
    receiverComponentCleaned = receiverComponent

    signalComponent = signalRandomizer.string(
      length : 9
      pool   : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    )

    messageComponentList = [
      receiverComponentCleaned
      attributionStreamComponent
      merchantComponentCleaned + moodStreamComponent
      satelliteStreamComponent
      bitsComponent + moodStreamComponentFollowup
    ]

    messageComponent = messageComponentList.join sep 

    payloadConstruct =
      sender      : senderComponent
      receiver    : receiverComponent
      context_uid : signalComponent # We may need to randomize the "signals" here.
      message     : messageComponent
      channel     : channelComponent
    console.log payloadConstruct

    customerLink = customer.replace(prefix, urlPrefix[prefix])
    console.log('customerLink:' + customerLink)

    storeBits(customer, payout).then((updatedPurse) ->

      if createTipUrl

        credentials.changeTip.load_access_token(domain).then (at) ->

          change_tip = new ChangeTip(
            api_key_or_access_token: at
            api_version: 2
          )

          uniqueId = signalRandomizer.string(
            length : 9
            pool   : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
          )

          contextUrl = "http://merchant.lovestamp.io/users/#{customer}/profile"
          sender = senderComponent
          receiver = receiverComponent
          channel = channelComponent

          displayName = snapshotAuthData.displayName

          msg = "#{customer}, you have bits awaiting for you!"
          _up = numeral(updatedPurse).format('0.00')
          change_tip.tip_url(_up, 'bits', msg).then (_result) ->

            r = JSON.parse(_result)
            if r.magic_url
              ob =
                response         : r
                body             : r
                messageConstruct : r.amount_display
                tipUrl           : r.magic_url
                updatedPurse     : updatedPurse

              def.resolve ob
            else

              try
                console.log 'Reverting purse...'
                currentPurse.set(parseFloat(numeral(_currentPurseData).format('0.00')))
                console.log 'Reverting budget...'
                payoutBudgetSource.set(parseFloat(numeral(_p).format('0.00')))
              catch e
                console.log 'Purse update error.'
                console.log e

              ob =
                response         : null
                body             : null
                messageConstruct : null
                tipUrl           : null
                updatedPurse     : null

              def.resolve ob
      else

        _customer = customer
        foundNetwork = undefined

        if snapshotMerchantAuthData and snapshotMerchantAuthData.profile and snapshotMerchantAuthData.profile.overview and snapshotMerchantAuthData.profile.overview.merchantPublicName
          merchantDisplayName = snapshotMerchantAuthData.profile.overview.merchantPublicName
        else
          merchantDisplayName = snapshotMerchantAuthData.displayName or merchant

        #console.log merchantDisplayName

        foundNetworkCheck = false

        _foundNetwork = _.first(_.values(_networks))

        _.forEach _foundNetwork, (givenNetwork) ->
          if foundNetworkCheck != true
            foundNetwork = givenNetwork
            foundNetworkCheck = true

        displayName = snapshotAuthData.displayName # fan displayName

        #console.log displayName

        merchantCallout = ''

        try
          _merchantAuthDataAssociatedNetworkPickConstruct = if merchantAuthDataAssociatedNetworkPick.length > 1 then merchantAuthDataAssociatedNetworkPick.split(':') else null

          _customerPickedMerchantAuthDataAssociatedNetworkPickConstruct = if __customerPickedMerchantAuthDataAssociatedNetwork.length > 1 then __customerPickedMerchantAuthDataAssociatedNetwork.split(':') else null

          if __customerPickedMerchantAuthDataAssociatedNetwork isnt null
            _merchantAuthDataAssociatedNetworkPickLink = urlPrefix[prefix] + _customerPickedMerchantAuthDataAssociatedNetworkPickConstruct[1]
          else
            _merchantAuthDataAssociatedNetworkPickLink = urlPrefix[merchantPrefix] + _merchantAuthDataAssociatedNetworkPickConstruct[1]

          #console.log _merchantAuthDataAssociatedNetworkPickLink

          if _customerPickedMerchantAuthDataAssociatedNetworkPickConstruct is null and _merchantAuthDataAssociatedNetworkPickConstruct is null
            merchantNetworkAvailable = foundNetwork.split(':')
            _merchantAuthDataAssociatedNetworkPickLink = urlPrefix[merchantNetworkAvailable[0]] + merchantNetworkAvailable[1]

          merchantCallout = "by #{merchantDisplayName} (#{_merchantAuthDataAssociatedNetworkPickLink})"
        catch e
          console.log 'Could not retrieve merchant display name.'
          #prefixCheck = if prefix == '@' then 'twitter' else if prefix == '✚' then 'google' else 'facebook'
          merchantCallout = "by #{merchantLink}"

        if prefix != '@'
          messageConstruct = "#{displayName} (#{customerLink}) got LoveStamped #{merchantCallout} for #{bitsComponent}. #{exclamation}"
        else
          # Twitter needs no obfuscation or display setting
          messageConstruct = "#{customer} got LoveStamped by #{merchant} for #{bitsComponent}. #{exclamation}"

        #console.log messageConstruct

        ob =
          updatedPurse     : updatedPurse
          messageConstruct : messageConstruct
          payout           : payout
          body             : null
          response         : true

        console.log '_payout check:'
        console.log _payout
        if _payout.status and _payout.status == 'error'
          def.resolve false
          return

        def.resolve ob
    ).done()

    def.promise

module.exports = changetipHandler
