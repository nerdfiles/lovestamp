
/*
 * fileOverview

                 _
       _        (_)  _     _
     _| |_ _ _ _ _ _| |_ _| |_ _____  ____
    (_   _) | | | (_   _|_   _) ___ |/ ___)
      | |_| | | | | | |_  | |_| ____| |
       \__)\___/|_|  \__)  \__)_____)_|

Functional nomenclature around Twitter OAuth interactions.

@note We should really decouple the Facebook and G+ API callbacks from this 
area and provide them as Promises to the ChangeTip Handlers.

 *# Strategy

Google and Facebook are pulling entire lists from the respective social 
networks, and this we are cross-checking one large list against another 
large list — our Firebase /users/ list. ``createFriend`` simply depends 
on the existence of the user within one list, whereas for Twitter, the 
endpoint is only checking against a list of 1 since the endpoint is 
tailored to the friendship relation.
 */

(function() {
  var Firebase, Twit, baseDataApiUrl, createFriend, e, facebookHandler, friendBatch, friendByLookup, friendByManual, friendByRoll, friendCreated, gapi, googleHandler, merchantLoadedAdjacentStampImpressions, postToTwitter, q, qs, request, tryCreateFriend, twitterAPI, _;

  Twit = require('twit');

  twitterAPI = require('node-twitter-api');

  Firebase = require('firebase');

  googleHandler = require('./google');

  facebookHandler = require('./facebook');

  request = require('request');

  qs = require('qs');

  _ = require('lodash');

  gapi = require('googleapis');

  friendBatch = require('../utils/friendBatch');

  try {
    q = require('q');
  } catch (_error) {
    e = _error;
    q = require('Q');
  }

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  merchantLoadedAdjacentStampImpressions = function(merchant, provider, adjacentFan, fan, $stampId) {

    /*
     * merchantLoadedAdjacentStampImpressions
    
    @param {Data} merchant
    @param {string} provider
    @param {string} adjacentFan
    @param {string} fan
    @param {string} stampSerial Hopefully it's a string... May require object introspection.
    @return {Promise:object} _stampCollection
     */
    var AdjacentFan, AdjacentFanSuccesses, def;
    def = q.defer();
    AdjacentFan = new Firebase(baseDataApiUrl + 'users' + '/' + adjacentFan);
    AdjacentFanSuccesses = AdjacentFan.child('stamps').child('stats').child('success');
    merchant.once('value', function(merchant) {
      var _merchant;
      _merchant = merchant.val();
      if (_merchant.stamps && _merchant.stamps.local) {
        _.forEach(_merchant.stamps.local, function(localStamp, stampId) {
          return AdjacentFanSuccesses.once('value', function() {
            var StampCollection, s;
            StampCollection = AdjacentFanSuccesses.child(stampId);
            s = new Firebase(baseDataApiUrl + 'users' + '/' + adjacentFan + '/stamps/stats/success/' + stampId + '/stampImpressionsBlue');
            s.once('value', function(stampImpressionsBlue) {
              if (stampImpressionsBlue === null || stampImpressionsBlue === 0) {
                return friendBatch.friendsBatchIntensityRoundup(fan, provider, stampId);
              }
            });
            StampCollection.once('value', function(stampCollection) {
              var _sId, _stampCollection;
              _stampCollection = stampCollection.val();
              _sId = !(_stampCollection && _stampCollection.stampImpressionsBlue) ? 0 : _stampCollection.stampImpressionsBlue;
              if (_stampCollection !== null && stampId === $stampId) {
                s.transaction(function(sId) {
                  if (sId && sId.stampImpressionsBlue) {
                    return sId.stampImpressionsBlue + 1;
                  } else {
                    return _sId + 1;
                  }
                }, function(error, committed) {
                  console.log('comm');
                  console.log(committed);
                  return def.resolve(_stampCollection);
                });
              }
            });
          });
        });
      }
    });
    return def.promise;
  };

  createFriend = function(path, friendId, impressionObject, Merchant, prefix, fan, $stampId) {

    /*
     * createFriend
    
    Pass a known friendId into our Firebase backend to check if the friend 
    already exists.
    
    @param {string} path
    @param {string} friendId
    @param {object} impressionObject
    @param {Data} Merchant
    @param {string} prefix
    @param {string} fan
    @return {undefined}
     */
    var friendData, rootFan;
    friendData = impressionObject;
    rootFan = fan;
    tryCreateFriend(path, friendId, friendData, rootFan);
    if (prefix && Merchant && fan) {
      merchantLoadedAdjacentStampImpressions(Merchant, prefix, friendId, fan, $stampId).then(function(uploadedStampImpressions) {
        console.log('Uploaded stampImpressionYellow');
        return console.dir(uploadedStampImpressions);
      });
    }
  };

  friendCreated = function(path, friendId, success, friendData, rootFan) {

    /*
    
    Messaging around created friends.
    
    @inner
    @param {string} path
    @param {string} friendId
    @param {boolean} success
    @param {object} friendData Unused.
    @return {undefined}
     */
    var provider, rootFanRef;
    if (!success) {
      console.log('Friend ' + friendId + ' already exists!');
    } else {
      console.log('Successfully created ' + friendId + ': now reverse adding...');
      provider = rootFan.charAt(0) === '@' ? 'twitter' : rootFan.charAt(0) === '✚' ? 'google' : 'facebook';
      rootFanRef = new Firebase(baseDataApiUrl + 'users/' + rootFan + '/' + 'friends' + '/' + provider + '/' + friendId);
      rootFanRef.transaction(function(currentRootFanData) {
        var fanConstruct;
        if (currentRootFanData === null) {
          fanConstruct = {
            active: true,
            handle: rootFan
          };
          return fanConstruct;
        } else {
          console.log('Root Friend already exists!');
        }
      }, function(error, committed, snapshot) {
        if (error) {
          return console.log('Friend addition failed abnormally', error);
        } else if (!committed) {
          return console.log("We aborted the friend addition (because " + rootFan + " already exists).");
        } else {
          return console.log("Friend " + rootFan + " added!");
        }
      });
    }
  };

  tryCreateFriend = function(path, friendId, friendData, rootFan) {

    /*
    
    Tries to set /friends/<friendId> to the specified data, but only
    if there's no data there already. Also tries to push a new 
    bitsCollected object and attempts to update friendImpressionsYellow.
    
    @inner
    @param {string} path
    @param {string} friendId
    @param {object} friendData
    @return {null}
     */
    var f, friendsRef;
    friendsRef = new Firebase(baseDataApiUrl + path + '/' + friendId);
    f = friendsRef;
    return f.transaction(function(currentFriendData) {
      if (currentFriendData !== null) {
        return currentFriendData;
      } else {
        return friendData;
      }
    }, function(error, committed) {
      return friendCreated(path, friendId, committed, friendData, rootFan);
    });
  };

  friendByLookup = function(credentials, fan, merchant, $stampId) {

    /*
    
    @param {object} credentials
    @param {string} fan
    @param {string} merchant
    @return {undefined}
     */
    var Users, prefix, sep, _prefix;
    Users = new Firebase(baseDataApiUrl + 'users');
    sep = '/';
    prefix = fan.charAt(0);
    _prefix = prefix === '✚' ? 'google' : prefix === '@' ? 'twitter' : 'facebook';
    Users.once('value', function(_users) {
      var Merchant, activeUsers, blank, friendshipUrl, hasBap, hasToken, oauth, params, socialUrlsAvailable, t, url, _activeUsers;
      _activeUsers = _.map(_users.val(), function(user, username) {
        return {
          handle: username,
          active: user.active,
          _id: user._id ? user._id : ''
        };
      });
      activeUsers = _.filter(_activeUsers, function(user) {
        return user.active;
      });
      oauth = {
        consumer_key: credentials.twitter.consumer_key,
        consumer_secret: credentials.twitter.consumer_secret,
        token: credentials.twitter.access_token,
        token_secret: credentials.twitter.access_token_secret
      };
      Merchant = new Firebase(baseDataApiUrl + 'users' + sep + merchant);
      socialUrlsAvailable = {
        'twitter': 'https://api.twitter.com/1.1/friends/ids.json?',
        'facebook': 'https://graph.facebook.com/fql?',
        'google': 'http://plus.google.com'
      };
      if (_prefix === 'twitter') {

        /*
        
                         _
               _        (_)  _     _
             _| |_ _ _ _ _ _| |_ _| |_ _____  ____
            (_   _) | | | (_   _|_   _) ___ |/ ___)
              | |_| | | | | | |_  | |_| ____| |
               \__)\___/|_|  \__)  \__)_____)_|
         */
        url = socialUrlsAvailable[_prefix];
        blank = '';
        params = {
          screen_name: fan.replace(fan.charAt(0), '')
        };
        friendshipUrl = url + qs.stringify(params);
        t = null;
        hasBap = new Firebase(baseDataApiUrl + 'users' + '/' + fan + '/' + 'hasBap');
        hasBap.on('value', function(bapSnapshot) {
          if (!bapSnapshot.val()) {
            hasBap.set('true');
          } else {
            _.forEach(activeUsers, function(user) {
              var handle;
              handle = user.handle;
              if (handle.charAt(0) === '@') {
                console.log('fan check');
                console.log(fan);
                return createFriend(("users/" + fan + "/friends/") + _prefix, handle, user, Merchant, prefix, fan, $stampId);
              }
            });
            return;
          }
          return request.get({
            url: friendshipUrl,
            oauth: oauth,
            json: true
          }, function(e, response) {
            var activeUsersList, foundActiveUsers, resultList;
            if (!e) {
              resultList = response.body.ids;
              activeUsersList = _.filter(_.map(activeUsers, function(user) {
                var _id;
                if (user._id) {
                  return _id = parseInt(user._id);
                }
              }), function(a) {
                return a;
              });
              foundActiveUsers = _.intersection(activeUsersList, resultList);
              _activeUsers = _.filter(activeUsers, function(user) {
                var found;
                found = false;
                _.forEach(foundActiveUsers, function(fau) {
                  if (user._id && fau === parseInt(user._id)) {
                    return found = true;
                  }
                });
                return found;
              });
              return _.forEach(_activeUsers, function(user) {
                var handle;
                handle = user.handle;
                return createFriend(("users/" + fan + "/friends/") + _prefix, handle, user, Merchant, prefix, fan, $stampId);
              });
            }
          });
        });
      }
      if (_prefix === 'google') {

        /*
        
                                     _
                                    | |
              ____  ___   ___   ____| | _____
             / _  |/ _ \ / _ \ / _  | || ___ |
            ( (_| | |_| | |_| ( (_| | || ____|
             \___ |\___/ \___/ \___ |\_)_____)
            (_____|           (_____|
         */
        hasToken = new Firebase(baseDataApiUrl + 'users' + '/' + fan + '/' + 'hasToken');
        hasToken.on('value', function(tokenSnapshot) {
          var _token;
          _token = tokenSnapshot.val();
          params = {
            userId: fan,
            circleId: 'friends',
            access_token: _token
          };
          return googleHandler.friendByLookup(credentials, params).then(function(circleData) {
            var visibleFriendsList;
            visibleFriendsList = circleData.items;
            return _.forEach(activeUsers, function(user) {
              var handle;
              handle = user.handle;
              if (handle.charAt(0) === '✚' && user.active && handle.replace(prefix, '') !== fan.replace(prefix, '')) {
                return _.forEach(visibleFriendsList, function(_friendData) {
                  var friendId;
                  friendId = friendData.id;
                  if (friendId === handle.replace(prefix, '')) {
                    return createFriend(("users/" + fan + "/friends/") + _prefix, handle, user, Merchant, prefix, fan, $stampId);
                  }
                });
              }
            });
          });
        });
      }
      if (_prefix === 'facebook') {

        /*
        
                ___                   _                 _
               / __)                 | |               | |
             _| |__ _____  ____ _____| |__   ___   ___ | |  _
            (_   __|____ |/ ___) ___ |  _ \ / _ \ / _ \| |_/ )
              | |  / ___ ( (___| ____| |_) ) |_| | |_| |  _ (
              |_|  \_____|\____)_____)____/ \___/ \___/|_| \_)
         */
        hasToken = new Firebase(baseDataApiUrl + 'users' + '/' + fan + '/' + 'hasToken');
        hasToken.on('value', function(tokenSnapshot) {
          var _token;
          _token = tokenSnapshot.val();
          return _.forEach(activeUsers, function(user, username) {
            var handle;
            handle = user.handle;
            params = {
              id: fan.replace(prefix, ''),
              access_token: _token,
              friendId: handle
            };
            if (handle.charAt(0) === 'ᶠ' && user.active && handle.replace(prefix, '') !== fan.replace(prefix, '')) {
              return facebookHandler.friendByLookup(params).then(function(friendData) {
                if (friendData) {
                  return createFriend(("users/" + fan + "/friends/") + _prefix, handle, user, Merchant, prefix, fan, $stampId);
                }
              });
            }
          });
        });
      }
    });
  };

  friendByRoll = function(credentials, customer, cursor) {

    /*
     * friendByRoll
    
    @descriptinon
    
    Check against a friends list for a particular Twitter use. Create stampImpressionsBlue 
    for all users who are following on Twitter, and who exist in LoveStamp's users tree.
    
    @param {object} credentials
    @param {object} customer
    @param {string||number} cursor
    @return {undefined}
     */
    var Users, blank, customerCleaned, existingFriends, isManualAddition, oauth, params, prefix, sep, t, url, _cursor, _prefix;
    _cursor = !cursor ? -1 : cursor;
    isManualAddition = _.isString(cursor) ? true : false;
    Users = new Firebase(baseDataApiUrl + 'users');
    prefix = customer.charAt(0);
    blank = '';
    sep = ' ';
    _prefix = prefix === '✚' ? 'google' : prefix === '@' ? 'twitter' : 'facebook';
    customerCleaned = customer.replace(prefix, blank);
    existingFriends = Users.child(customer).child('friends').child(_prefix);
    oauth = {
      consumer_key: credentials.twitter.consumer_key,
      consumer_secret: credentials.twitter.consumer_secret,
      token: credentials.twitter.access_token,
      token_secret: credentials.twitter.access_token_secret
    };
    url = 'https://api.twitter.com/1.1/friends/list.json?';
    params = {
      screen_name: customer.replace(prefix, ''),
      skip_status: true,
      include_user_entities: false,
      cursor: _cursor,
      count: 200
    };
    url += qs.stringify(params);
    t = null;
    request.get({
      url: url,
      oauth: oauth,
      json: true
    }, function(e, r, result) {
      var c;
      c = 0;
      return t = setTimeout(function() {
        _.forEach(result.users, function(user) {
          var found, friendId, friendObject;
          if (result.next_cursor === -1) {
            return;
          }
          if (user && !user.id) {
            return;
          }
          found = false;
          _.forEach(Users, function(_user, username) {
            var _username;
            _username = username.replace(username.charAt(0), '');
            if (_username === user.screen_name) {
              return found = true;
            }
          });
          if (found && user.following && !user.blocking) {
            friendId = user.screen_name;
            friendObject = {
              'id': user.id,
              'handle': user.screen_name,
              'url': user.url,
              'verified': user.verified,
              'protected': user["protected"],
              'profile_image_url': user.profile_image_url,
              'profile_image_url_https': user.profile_image_url_https,
              'lang': user.lang,
              'follow_request_sent': user.follow_request_sent,
              'following': user.following,
              'description': user.description,
              'created_at': user.created_at,
              'blocking': user.blocking,
              'blocked_by': user.blocked_by
            };
            return createFriend(("users/" + customer + "/friends/") + _prefix, friendId, friendObject, customer);
          }
        });
        friendByRoll(credentials, customerCleaned, result.next_cursor);
        if (c > 1000) {
          clearTimeout(t);
          return;
        }
        return c += 1000;
      }, 1000, false);
    });
  };

  postToTwitter = function(credentials, merchant, customer, logglyClient, messageConstruct, stamp) {

    /*
     * postToTwitter
    
    Post via OAuth to Twitter. Update stampImpressionsBlue on successful posts.
    
    @param {object} credentials
    @param {string} merchant Typically a string construction that prefixes a 
                    special symbol to represent the base social network 
                    of the user.
    @param {string} customer See above description param.
    @param {object} logglyClient
    @param {object} messageConstruct
    @return {undefined}
     */
    var $stampId, customerComponent, merchantComponent, sourceIp, twitter;
    $stampId = stamp;
    sourceIp = '0.0.0.0';
    twitter = new twitterAPI({
      consumerKey: credentials.twitter.consumer_key,
      consumerSecret: credentials.twitter.consumer_secret,
      callback: 'https://auth.firebase.com/v2/lovestamp/auth/twitter/callback'
    });
    merchantComponent = merchant;
    customerComponent = customer;
    twitter.statuses("update", {
      status: messageConstruct
    }, credentials.twitter.access_token, credentials.twitter.access_token_secret, function(error, data, response) {
      if (error) {
        logglyClient.log("" + sourceIp, ['twitterError'], function(error, result) {
          return console.log('Twitter error: ', error);
        });
      } else {
        logglyClient.log("" + sourceIp + " - " + data, ['twitterSuccess'], function(err, result) {
          return console.log('Twitter post: ', result);
        });
      }
      return friendByLookup(credentials, customer, merchant, $stampId);
    });
  };

  friendByManual = function(credentials, customer, cursor) {

    /*
    
    Use to discover users who have public conversations on Twitter.
    
    @usage
    
        friendByManual(credentials, 'handle'[, 'handleToAddBy'])
    
    @param {object} credentials
    @param {string} customer
    @param {}
     */
    var T, Users, blank, customerCleaned, isManualAddition, manuallyAddedFan, prefix, sep, _cursor, _prefix;
    _cursor = !cursor ? -1 : cursor;
    isManualAddition = _.isString(cursor) ? true : false;
    Users = new Firebase(baseDataApiUrl + 'users');
    prefix = customer.charAt(0);
    blank = '';
    sep = ' ';
    _prefix = prefix === '✚' ? 'google' : prefix === '@' ? 'twitter' : 'facebook';
    customerCleaned = customer.replace(prefix, blank);
    T = new Twit({
      consumer_key: credentials.twitter.consumer_key,
      consumer_secret: credentials.twitter.consumer_secret,
      access_token: credentials.twitter.access_token,
      access_token_secret: credentials.twitter.access_token_secret
    });
    if (isManualAddition) {
      manuallyAddedFan = cursor;
      T.get('search/tweets', {
        q: '@' + manuallyAddedFan + sep + '@' + customerCleaned,
        count: 10
      }, function(err, data, response) {
        var friendId, friendObject, statuses, user;
        if (err !== null) {
          return;
        }
        statuses = data.statuses;
        user = data;
        friendId = manuallyAddedFan;
        friendObject = {
          handle: friendId
        };
        if (_.isArray(statuses) && statuses.length) {
          return createFriend(("users/" + customer + "/friends/") + _prefix, friendId, friendObject, customer);
        }
      });
    }
  };

  module.exports = {
    postToTwitter: postToTwitter
  };

}).call(this);
