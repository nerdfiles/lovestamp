
/*
 * fileOverview

        ___                   _                 _
       / __)                 | |               | |
     _| |__ _____  ____ _____| |__   ___   ___ | |  _
    (_   __|____ |/ ___) ___ |  _ \ / _ \ / _ \| |_/ )
      | |  / ___ ( (___| ____| |_) ) |_| | |_| |  _ (
      |_|  \_____|\____)_____)____/ \___/ \___/|_| \_)

 *# description

@see https://github.com/Thuzi/facebook-node-sdk,
     https://developers.facebook.com/docs/graph-api/common-scenarios/
 */

(function() {
  var Firebase, Q, baseDataApiUrl, e, friendByLookup, graph, https, postToFacebook, request, _getFriendById, _getFriends, _getFriendsBatch;

  graph = require('fbgraph');

  Firebase = require('firebase');

  https = require('https');

  request = require('request');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  try {
    Q = require('Q');
  } catch (_error) {
    e = _error;
    Q = require('q');
  }

  _getFriends = function(access_token, apiPath) {

    /*
    getFriends
     */
    var buffer, def, options;
    def = Q.defer();
    options = {
      host: 'graph.facebook.com',
      port: 443,
      path: apiPath + '&access_token=' + access_token,
      method: 'GET'
    };
    buffer = '';
    console.dir(options);
    request = https.get(options, function(result) {
      result.setEncoding('utf8');
      result.on('data', function(chunk) {
        return buffer += chunk;
      });
      return result.on('end', function() {
        return def.resolve(buffer);
      });
    });
    request.on('error', function(e) {
      return def.reject(e);
    });
    request.end();
    return def.promise;
  };

  _getFriendsBatch = function(access_token, relative_url) {

    /*
    Get Friends Batch
     */
    var def, friendsBatchQuery;
    def = Q.defer();
    graph.setAccessToken(access_token);
    friendsBatchQuery = {
      method: 'GET',
      relative_url: relative_url
    };
    graph.batch([friendsBatchQuery], function(error, responseData) {
      if (!error) {
        return def.resolve(responseData);
      }
    });
    return def.promise;
  };

  _getFriendById = function(access_token, relative_url) {

    /*
    Get Friend By Id
     */
    var def, friendsBatchQuery;
    def = Q.defer();
    graph.setAccessToken(access_token);
    friendsBatchQuery = {
      method: 'GET',
      relative_url: relative_url
    };
    graph.batch([friendsBatchQuery], function(error, responseData) {
      if (!error) {
        return def.resolve(responseData);
      }
    });
    return def.promise;
  };

  friendByLookup = function(params) {

    /*
    
    @param {object} params
    @return {Promise} friendData
     */
    var access_token, def, friendId, id, relative_url;
    def = Q.defer();
    id = params.id;
    friendId = params.friendId;
    relative_url = "" + id + "/friends/" + friendId;
    access_token = params.access_token;
    _getFriendById(access_token, relative_url).then(function(friendData) {
      return def.resolve(friendData);
    });
    return def.promise;
  };

  postToFacebook = function(credentials, merchant, customer, logglyClient, messageConstruct, stamp) {

    /*
     */
    var $stampId, body, id, sourceIp, wallPost, _messageConstruct, _obj;
    $stampId = stamp;
    sourceIp = '0.0.0.0';
    _messageConstruct = {
      message: messageConstruct
    };
    id = 'LoveStampio';
    _obj = {
      id: id,
      _messageConstruct: _messageConstruct
    };
    graph.setAccessToken(credentials.facebook.access_token);
    body = messageConstruct;
    wallPost = {
      message: body
    };
    graph.post("" + id + "/feed", wallPost, function(res) {
      console.log(res);
      if (res.error) {
        console.log(!res ? 'error occurred' : res.error);
        return;
      }
      logglyClient.log("" + id, ['facebookSuccess'], function(error, result) {
        return console.log('Facebook post:', result);
      });
    });
  };

  module.exports = {
    postToFacebook: postToFacebook,
    friendByLookup: friendByLookup
  };

}).call(this);
