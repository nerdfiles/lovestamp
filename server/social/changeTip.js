
/*
 * fileOverview

           _                          _______ _
          | |                        (_______|_)
      ____| |__  _____ ____   ____ _____ _    _ ____
     / ___)  _ \(____ |  _ \ / _  | ___ | |  | |  _ \
    ( (___| | | / ___ | | | ( (_| | ____| |  | | |_| |
     \____)_| |_\_____|_| |_|\___ |_____)_|  |_|  __/
                            (_____|            |_|

@module server/social  

 *# description

Transactions with the [ChangeTip API](https://www.changetip.com/api).
 */

(function() {
  var Chance, ChangeTip, Firebase, Q, baseDataApiUrl, changetipHandler, credentials, e, impression, numeral, request, storeBits, _, _credentials;

  Firebase = require('firebase');

  request = require('request');

  Chance = require('chance');

  _ = require('lodash');

  ChangeTip = require('changetip');

  _credentials = require('../config/credentials');

  credentials = _credentials.credentials();

  numeral = require('numeral');

  try {
    Q = require('q');
  } catch (_error) {
    e = _error;
    Q = require('Q');
  }

  impression = require('../utils/impression');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  storeBits = function(customer, payout) {

    /*
    @namespace storeBits
    @inner
     */
    var LSRoot, currentProfile, currentPurse, getBitsCollected, u, usersRoot, _currentPurse;
    _currentPurse = Q.defer();
    LSRoot = new Firebase(baseDataApiUrl);
    usersRoot = LSRoot.child('users');
    u = usersRoot.child(customer);
    currentProfile = u.child('profile');
    currentPurse = currentProfile.child('purse');
    getBitsCollected = u.child('profile').child('bitsCollected');
    if (parseFloat(payout) > 0) {
      currentPurse.transaction(function(currentPurseSnapshot) {
        if (currentPurseSnapshot === null) {
          currentPurseSnapshot = 0;
        }
        u = parseFloat(numeral(currentPurseSnapshot).format('0.00')) + parseFloat(numeral(payout).format('0.00'));
        _currentPurse.resolve(u);
        return u;
      }, function(error, committed) {});
      getBitsCollected.once('value', function(bitsCollectedSnapshot) {
        var _currentBitsCollected;
        _currentBitsCollected = bitsCollectedSnapshot.val() || 0;
        getBitsCollected.set(parseFloat(numeral(_currentBitsCollected).format('0.00')) + parseFloat(numeral(payout).format('0.00')));
      });
    } else {
      currentPurse.once('value', function(currentPurseSnapshot) {
        var v;
        v = parseFloat(numeral(currentPurseSnapshot.val()).format('0.00'));
        return _currentPurse.resolve(v);
      }, function(error) {
        if (error) {
          return _currentPurse.resolve('error');
        }
      });
    }
    return _currentPurse.promise;
  };

  changetipHandler = function(merchant, customer, stampId, __credentials, createTipUrl, domain) {

    /*
    @description  
      The following function is an implementation of Natural Language Generation 
      models which are currently schemas for creating StoryBook Generators and 
      Document Micro/planners.
    @template  
      Message just do @{customer} you've been love stamped by @{merchant}! Bam {bits}.
    @example  
      curl https://api.changetip.com/v1/tips/?api_key=abcdef \
            -H "Content-Type: application/json" \
            -d '{"sender":"nick@changecoin","receiver":"jimlyndon@changecoin","context_uid":"64eee6d2a","message":"100 bits","channel":"slack"}'
    @example.response
          {
              "sender"         : "RattelyrDragon",
              "id"             : 850692,
              "magic_url"      : "http://tip.me/once/G3WE-Fm18kRoF",
              "amount_display" : "1 bit"
          }
     */
    var LSRoot, authData, balancesRoot, currentProfile, currentPurse, customerPickedMerchantAuthDataAssociatedNetwork, def, exclamation, exclamationList, exclamationPick, merchantAuthData, merchantAuthDataAssociatedNetwork, merchantAuthDataAssociatedNetworkPick, merchantAuthDataAvailableAssociatedNetworks, merchantLink, merchantPrefix, merchantProvider, payoutBudgetSource, prefix, provider, signalRandomizer, snapshotAuthData, snapshotMerchantAuthData, socialMediaAvailableHandles, socialMediaNetwork, urlPrefix, usersRoot, __customerPickedMerchantAuthDataAssociatedNetwork, _currentPurseData, _networks, _p;
    def = Q.defer();
    signalRandomizer = new Chance();
    exclamationList = ["Booya!", "Huzzah!", "Great!", "Cheers!", "Yahoo!"];
    exclamationPick = Math.floor(Math.random() * exclamationList.length);
    exclamation = exclamationList[exclamationPick];
    urlPrefix = {
      '@': 'https://twitter.com/',
      'ᶠ': 'https://facebook.com/',
      '✚': 'https://plus.google.com/'
    };
    socialMediaNetwork = {
      '@': 'twitter',
      'ᶠ': 'facebook',
      '✚': 'google'
    };
    socialMediaAvailableHandles = {
      'google': '101600582096097255226',
      'twitter': 'LoveStampio',
      'facebook': 'LoveStamp IO'
    };
    prefix = customer.charAt(0);
    provider = socialMediaNetwork[prefix];
    LSRoot = new Firebase(baseDataApiUrl);
    usersRoot = LSRoot.child('users');
    snapshotAuthData = void 0;
    authData = usersRoot.child(customer);
    authData.once('value', function(_authData) {
      return snapshotAuthData = _authData.val();
    });
    currentProfile = authData.child('profile');
    currentPurse = currentProfile.child('purse');
    _currentPurseData = null;
    currentPurse.once('value', function(currentPurseSnapshot) {
      return _currentPurseData = currentPurseSnapshot.val();
    });
    balancesRoot = LSRoot.child('balances');
    payoutBudgetSource = balancesRoot.child('payoutBudget');
    _p = null;
    payoutBudgetSource.once('value', function(payoutBudgetSnapshot) {
      return _p = payoutBudgetSnapshot.val();
    });
    if (merchant) {
      merchantPrefix = merchant.charAt(0);
      merchantProvider = socialMediaNetwork[merchantPrefix];
      merchantLink = merchant.replace(merchantPrefix, urlPrefix[merchantPrefix]);
      merchantAuthData = usersRoot.child(merchant);
      merchantAuthDataAvailableAssociatedNetworks = merchantAuthData.child('profile').child('networks');
      merchantAuthDataAssociatedNetwork = merchantAuthDataAvailableAssociatedNetworks.child(merchantProvider);
      customerPickedMerchantAuthDataAssociatedNetwork = merchantAuthDataAvailableAssociatedNetworks.child(provider);
      __customerPickedMerchantAuthDataAssociatedNetwork = void 0;
      merchantAuthDataAssociatedNetworkPick = void 0;
      _networks = void 0;
      snapshotMerchantAuthData = void 0;
      merchantAuthData.on('value', function(_merchantAuthData) {
        return snapshotMerchantAuthData = _merchantAuthData.val();
      });
      merchantAuthDataAvailableAssociatedNetworks.on('value', function(networks) {
        return _networks = networks.val();
      });
      merchantAuthDataAssociatedNetwork.on('value', function(_merchantAuthDataAssociatedNetworkPick) {
        return merchantAuthDataAssociatedNetworkPick = _merchantAuthDataAssociatedNetworkPick.val();
      });
      customerPickedMerchantAuthDataAssociatedNetwork.on('value', function(_customerPickedMerchantAuthDataAssociatedNetwork) {
        return __customerPickedMerchantAuthDataAssociatedNetwork = _customerPickedMerchantAuthDataAssociatedNetwork.val();
      });
    }
    console.log('createTipUrl:' + createTipUrl);
    return impression(customer, merchant, stampId, provider, createTipUrl).then(function(_payout) {
      var attributionStreamComponent, bitsComponent, channelComponent, customerLink, merchantComponent, merchantComponentCleaned, messageComponent, messageComponentList, moniker, moodStreamComponent, moodStreamComponentFollowup, payloadConstruct, payout, randomizedPayout, receiverComponent, receiverComponentCleaned, satelliteStreamComponent, senderComponent, sep, signalComponent, tipApi;
      console.log('payout:', _payout);

      /* Gives back 0 or != 0 for _payout */
      if (_payout === false) {
        def.resolve(_payout);
        return;
      }
      console.log('_payout: ' + _payout);
      tipApi = 'https://api.changetip.com/v2/tips/?';
      merchantComponent = merchant;
      merchantComponentCleaned = merchantComponent;
      attributionStreamComponent = "you've been LoveStamped by";
      satelliteStreamComponent = "Bam";
      moodStreamComponent = '!';
      moodStreamComponentFollowup = '.';
      moniker = 'bits';
      sep = ' ';
      payout = _payout;
      randomizedPayout = payout;
      bitsComponent = randomizedPayout + sep + moniker;
      channelComponent = provider === 'google' ? 'googleplus' : provider;
      senderComponent = socialMediaAvailableHandles[provider];
      receiverComponent = customer.replace(prefix, '');
      receiverComponentCleaned = receiverComponent;
      signalComponent = signalRandomizer.string({
        length: 9,
        pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
      });
      messageComponentList = [receiverComponentCleaned, attributionStreamComponent, merchantComponentCleaned + moodStreamComponent, satelliteStreamComponent, bitsComponent + moodStreamComponentFollowup];
      messageComponent = messageComponentList.join(sep);
      payloadConstruct = {
        sender: senderComponent,
        receiver: receiverComponent,
        context_uid: signalComponent,
        message: messageComponent,
        channel: channelComponent
      };
      console.log(payloadConstruct);
      customerLink = customer.replace(prefix, urlPrefix[prefix]);
      console.log('customerLink:' + customerLink);
      storeBits(customer, payout).then(function(updatedPurse) {
        var displayName, foundNetwork, foundNetworkCheck, merchantCallout, merchantDisplayName, merchantNetworkAvailable, messageConstruct, ob, _customer, _customerPickedMerchantAuthDataAssociatedNetworkPickConstruct, _foundNetwork, _merchantAuthDataAssociatedNetworkPickConstruct, _merchantAuthDataAssociatedNetworkPickLink;
        if (createTipUrl) {
          return credentials.changeTip.load_access_token(domain).then(function(at) {
            var change_tip, channel, contextUrl, displayName, msg, receiver, sender, uniqueId, _up;
            change_tip = new ChangeTip({
              api_key_or_access_token: at,
              api_version: 2
            });
            uniqueId = signalRandomizer.string({
              length: 9,
              pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
            });
            contextUrl = "http://merchant.lovestamp.io/users/" + customer + "/profile";
            sender = senderComponent;
            receiver = receiverComponent;
            channel = channelComponent;
            displayName = snapshotAuthData.displayName;
            msg = "" + customer + ", you have bits awaiting for you!";
            _up = numeral(updatedPurse).format('0.00');
            return change_tip.tip_url(_up, 'bits', msg).then(function(_result) {
              var ob, r;
              r = JSON.parse(_result);
              if (r.magic_url) {
                ob = {
                  response: r,
                  body: r,
                  messageConstruct: r.amount_display,
                  tipUrl: r.magic_url,
                  updatedPurse: updatedPurse
                };
                return def.resolve(ob);
              } else {
                try {
                  console.log('Reverting purse...');
                  currentPurse.set(parseFloat(numeral(_currentPurseData).format('0.00')));
                  console.log('Reverting budget...');
                  payoutBudgetSource.set(parseFloat(numeral(_p).format('0.00')));
                } catch (_error) {
                  e = _error;
                  console.log('Purse update error.');
                  console.log(e);
                }
                ob = {
                  response: null,
                  body: null,
                  messageConstruct: null,
                  tipUrl: null,
                  updatedPurse: null
                };
                return def.resolve(ob);
              }
            });
          });
        } else {
          _customer = customer;
          foundNetwork = void 0;
          if (snapshotMerchantAuthData && snapshotMerchantAuthData.profile && snapshotMerchantAuthData.profile.overview && snapshotMerchantAuthData.profile.overview.merchantPublicName) {
            merchantDisplayName = snapshotMerchantAuthData.profile.overview.merchantPublicName;
          } else {
            merchantDisplayName = snapshotMerchantAuthData.displayName || merchant;
          }
          foundNetworkCheck = false;
          _foundNetwork = _.first(_.values(_networks));
          _.forEach(_foundNetwork, function(givenNetwork) {
            if (foundNetworkCheck !== true) {
              foundNetwork = givenNetwork;
              return foundNetworkCheck = true;
            }
          });
          displayName = snapshotAuthData.displayName;
          merchantCallout = '';
          try {
            _merchantAuthDataAssociatedNetworkPickConstruct = merchantAuthDataAssociatedNetworkPick.length > 1 ? merchantAuthDataAssociatedNetworkPick.split(':') : null;
            _customerPickedMerchantAuthDataAssociatedNetworkPickConstruct = __customerPickedMerchantAuthDataAssociatedNetwork.length > 1 ? __customerPickedMerchantAuthDataAssociatedNetwork.split(':') : null;
            if (__customerPickedMerchantAuthDataAssociatedNetwork !== null) {
              _merchantAuthDataAssociatedNetworkPickLink = urlPrefix[prefix] + _customerPickedMerchantAuthDataAssociatedNetworkPickConstruct[1];
            } else {
              _merchantAuthDataAssociatedNetworkPickLink = urlPrefix[merchantPrefix] + _merchantAuthDataAssociatedNetworkPickConstruct[1];
            }
            if (_customerPickedMerchantAuthDataAssociatedNetworkPickConstruct === null && _merchantAuthDataAssociatedNetworkPickConstruct === null) {
              merchantNetworkAvailable = foundNetwork.split(':');
              _merchantAuthDataAssociatedNetworkPickLink = urlPrefix[merchantNetworkAvailable[0]] + merchantNetworkAvailable[1];
            }
            merchantCallout = "by " + merchantDisplayName + " (" + _merchantAuthDataAssociatedNetworkPickLink + ")";
          } catch (_error) {
            e = _error;
            console.log('Could not retrieve merchant display name.');
            merchantCallout = "by " + merchantLink;
          }
          if (prefix !== '@') {
            messageConstruct = "" + displayName + " (" + customerLink + ") got LoveStamped " + merchantCallout + " for " + bitsComponent + ". " + exclamation;
          } else {
            messageConstruct = "" + customer + " got LoveStamped by " + merchant + " for " + bitsComponent + ". " + exclamation;
          }
          ob = {
            updatedPurse: updatedPurse,
            messageConstruct: messageConstruct,
            payout: payout,
            body: null,
            response: true
          };
          console.log('_payout check:');
          console.log(_payout);
          if (_payout.status && _payout.status === 'error') {
            def.resolve(false);
            return;
          }
          return def.resolve(ob);
        }
      }).done();
      return def.promise;
    });
  };

  module.exports = changetipHandler;

}).call(this);
