###
# fileOverview

                 _
       _        (_)  _     _
     _| |_ _ _ _ _ _| |_ _| |_ _____  ____
    (_   _) | | | (_   _|_   _) ___ |/ ___)
      | |_| | | | | | |_  | |_| ____| |
       \__)\___/|_|  \__)  \__)_____)_|

Functional nomenclature around Twitter OAuth interactions.

@note We should really decouple the Facebook and G+ API callbacks from this 
area and provide them as Promises to the ChangeTip Handlers.

## Strategy

Google and Facebook are pulling entire lists from the respective social 
networks, and this we are cross-checking one large list against another 
large list — our Firebase /users/ list. ``createFriend`` simply depends 
on the existence of the user within one list, whereas for Twitter, the 
endpoint is only checking against a list of 1 since the endpoint is 
tailored to the friendship relation.

###


Twit = require('twit')
twitterAPI = require('node-twitter-api')

Firebase = require('firebase')
googleHandler = require('./google')
facebookHandler = require('./facebook')
request = require('request')
qs = require('qs')
_ = require('lodash')
gapi = require('googleapis')

friendBatch = require('../utils/friendBatch')

try
    q = require('q')
catch e
    q = require('Q')

baseDataApiUrl = 'https://lovestamp.firebaseio.com/'


merchantLoadedAdjacentStampImpressions = (merchant, provider, adjacentFan, fan, $stampId) ->
    ###
    # merchantLoadedAdjacentStampImpressions

    @param {Data} merchant
    @param {string} provider
    @param {string} adjacentFan
    @param {string} fan
    @param {string} stampSerial Hopefully it's a string... May require object introspection.
    @return {Promise:object} _stampCollection
    ###

    def = q.defer()

    AdjacentFan = new Firebase(baseDataApiUrl + 'users' + '/' + adjacentFan)
    AdjacentFanSuccesses = AdjacentFan.child('stamps').child('stats').child('success')

    #return  if ! AdjacentFanSuccesses

    merchant.once('value', (merchant) ->

        _merchant = merchant.val()
        if _merchant.stamps and _merchant.stamps.local
            _.forEach _merchant.stamps.local, (localStamp, stampId) ->

                AdjacentFanSuccesses.once 'value', () ->

                    StampCollection = AdjacentFanSuccesses.child(stampId)

                    s = new Firebase(baseDataApiUrl + 'users' + '/' + adjacentFan + '/stamps/stats/success/' + stampId + '/stampImpressionsBlue')

                    s.once 'value', (stampImpressionsBlue) ->
                        if stampImpressionsBlue is null or stampImpressionsBlue is 0
                            friendBatch.friendsBatchIntensityRoundup(fan, provider, stampId)

                    StampCollection.once 'value', (stampCollection) ->

                        _stampCollection = stampCollection.val()
                        _sId = if !(_stampCollection and _stampCollection.stampImpressionsBlue) then 0 else _stampCollection.stampImpressionsBlue
                        if _stampCollection isnt null and stampId is $stampId

                            s.transaction((sId) ->
                                # Updates stamp impression blue if its null or 0 to reflect actual friendship value
                                if sId and sId.stampImpressionsBlue
                                    return (sId.stampImpressionsBlue) + 1
                                else
                                    return (_sId + 1)

                            , (error, committed) ->
                                console.log 'comm'
                                console.log committed
                                def.resolve _stampCollection
                            )

                        return
                    return
        return
    )

    def.promise


createFriend = (path, friendId, impressionObject, Merchant, prefix, fan, $stampId) ->
    ###
    # createFriend

    Pass a known friendId into our Firebase backend to check if the friend 
    already exists.

    @param {string} path
    @param {string} friendId
    @param {object} impressionObject
    @param {Data} Merchant
    @param {string} prefix
    @param {string} fan
    @return {undefined}
    ###

    friendData = impressionObject

    rootFan = fan
    tryCreateFriend(path, friendId, friendData, rootFan)

    if prefix and Merchant and fan
        merchantLoadedAdjacentStampImpressions(Merchant, prefix, friendId, fan, $stampId).then (uploadedStampImpressions) ->
            console.log 'Uploaded stampImpressionYellow'
            console.dir uploadedStampImpressions

    return


friendCreated = (path, friendId, success, friendData, rootFan) ->
    ###

    Messaging around created friends.

    @inner
    @param {string} path
    @param {string} friendId
    @param {boolean} success
    @param {object} friendData Unused.
    @return {undefined}
    ###

    if ! success
        console.log('Friend ' + friendId + ' already exists!')
    else
        console.log('Successfully created ' + friendId + ': now reverse adding...')

        provider = if rootFan.charAt(0) == '@' then 'twitter' else if rootFan.charAt(0) == '✚' then 'google' else 'facebook'

        rootFanRef = new Firebase(baseDataApiUrl + 'users/' + rootFan + '/' + 'friends' + '/' + provider + '/' + friendId)

        rootFanRef.transaction((currentRootFanData) ->

          if currentRootFanData == null
            fanConstruct =
              active : true
              handle : rootFan
            return fanConstruct
          else
            console.log 'Root Friend already exists!'
            return

        , (error, committed, snapshot) ->

          if error
            console.log('Friend addition failed abnormally', error)
          else if !committed
            console.log "We aborted the friend addition (because #{rootFan} already exists)."
          else
            console.log "Friend #{rootFan} added!"
        )
    return


tryCreateFriend = (path, friendId, friendData, rootFan) ->
    ###

    Tries to set /friends/<friendId> to the specified data, but only
    if there's no data there already. Also tries to push a new 
    bitsCollected object and attempts to update friendImpressionsYellow.

    @inner
    @param {string} path
    @param {string} friendId
    @param {object} friendData
    @return {null}
    ###

    friendsRef = new Firebase(baseDataApiUrl + path + '/' + friendId)

    f = friendsRef

    f.transaction((currentFriendData) ->

        if currentFriendData isnt null
            return currentFriendData
        else
            return friendData

    , (error, committed) ->
        friendCreated(path, friendId, committed, friendData, rootFan)
    )


friendByLookup = (credentials, fan, merchant, $stampId) ->
    ###

    @param {object} credentials
    @param {string} fan
    @param {string} merchant
    @return {undefined}
    ###

    Users = new Firebase(baseDataApiUrl + 'users')
    sep = '/'

    prefix = fan.charAt(0)
    _prefix = if prefix is '✚' then 'google' else if prefix is '@' then 'twitter' else 'facebook'

    Users.once('value', (_users) ->

        _activeUsers = _.map _users.val(), (user, username) ->
            {
                handle: username,
                active: user.active
                _id: if user._id then user._id else ''
            }

        activeUsers = _.filter _activeUsers, (user) ->
            user.active

        # @see https://api.twitter.com/1.1/friends/list.json?cursor=-1&screen_name=twitterapi&skip_status=true&include_user_entities=false
        oauth =
            consumer_key    : credentials.twitter.consumer_key
            consumer_secret : credentials.twitter.consumer_secret
            token           : credentials.twitter.access_token
            token_secret    : credentials.twitter.access_token_secret

        Merchant = new Firebase(baseDataApiUrl + 'users' + sep + merchant)

        socialUrlsAvailable =
            'twitter'  : 'https://api.twitter.com/1.1/friends/ids.json?'
            'facebook' : 'https://graph.facebook.com/fql?'
            'google'   : 'http://plus.google.com'

        if _prefix == 'twitter'

            ###

                             _
                   _        (_)  _     _
                 _| |_ _ _ _ _ _| |_ _| |_ _____  ____
                (_   _) | | | (_   _|_   _) ___ |/ ___)
                  | |_| | | | | | |_  | |_| ____| |
                   \__)\___/|_|  \__)  \__)_____)_|

            ###

            url = socialUrlsAvailable[_prefix]

            blank = ''

            params =
                screen_name : fan.replace(fan.charAt(0), '')

            friendshipUrl = url + qs.stringify(params)

            t = null

            hasBap = new Firebase(baseDataApiUrl + 'users' + '/' + fan + '/' + 'hasBap')
            hasBap.on('value', (bapSnapshot) ->

                if !bapSnapshot.val()
                  hasBap.set 'true'
                else

                  _.forEach activeUsers, (user) ->

                      handle = user.handle
                      if handle.charAt(0) == '@'
                          console.log 'fan check'
                          console.log fan

                          createFriend("users/#{fan}/friends/" + _prefix, handle, user, Merchant, prefix, fan, $stampId)

                  return


                request.get(
                    url          : friendshipUrl
                    oauth        : oauth
                    json         : true
                , (e, response) ->
                    if !e
                        resultList = response.body.ids

                        activeUsersList = _.filter(_.map(activeUsers, (user) ->
                            if user._id
                                return _id = parseInt(user._id)
                        ), (a) ->
                          a
                        )

                        foundActiveUsers = _.intersection(activeUsersList, resultList)

                        _activeUsers = _.filter activeUsers, (user) ->
                            found = false
                            _.forEach foundActiveUsers, (fau) ->
                                if user._id and fau == parseInt(user._id)
                                    found = true
                            found

                        _.forEach _activeUsers, (user) ->
                            handle = user.handle
                            createFriend("users/#{fan}/friends/" + _prefix, handle, user, Merchant, prefix, fan, $stampId)

                )
            )

        if _prefix == 'google'

            ###

                                         _
                                        | |
                  ____  ___   ___   ____| | _____
                 / _  |/ _ \ / _ \ / _  | || ___ |
                ( (_| | |_| | |_| ( (_| | || ____|
                 \___ |\___/ \___/ \___ |\_)_____)
                (_____|           (_____|

            ###

            hasToken = new Firebase(baseDataApiUrl + 'users' + '/' + fan + '/' + 'hasToken')
            hasToken.on('value', (tokenSnapshot) ->

                _token = tokenSnapshot.val()

                params =
                    userId       : fan
                    circleId     : 'friends'
                    access_token : _token

                # Pull in list of all Google+ Users who are visible and within the given fan's circles
                googleHandler.friendByLookup(credentials, params).then (circleData) ->

                    visibleFriendsList = circleData.items

                    _.forEach(activeUsers, (user) ->

                        handle = user.handle

                        if handle.charAt(0) == '✚' and user.active and handle.replace(prefix, '') isnt fan.replace(prefix, '')
                            _.forEach(visibleFriendsList, (_friendData) ->

                                # @note We don't need any of this data, and 
                                # can likely pull it from the client-side as 
                                # needed.
                                friendId = friendData.id

                                #friendData = _friendData
                                #friendUrl = friendData.url 
                                #friendDisplayName = friendData.displayName
                                #friendImageUrl = friendData.image.url

                                if friendId == handle.replace(prefix, '')
                                    createFriend("users/#{fan}/friends/" + _prefix, handle, user, Merchant, prefix, fan, $stampId)
                            )
                    )
            )

        if _prefix == 'facebook'

            ###

                    ___                   _                 _
                   / __)                 | |               | |
                 _| |__ _____  ____ _____| |__   ___   ___ | |  _
                (_   __|____ |/ ___) ___ |  _ \ / _ \ / _ \| |_/ )
                  | |  / ___ ( (___| ____| |_) ) |_| | |_| |  _ (
                  |_|  \_____|\____)_____)____/ \___/ \___/|_| \_)

            ###

            hasToken = new Firebase(baseDataApiUrl + 'users' + '/' + fan + '/' + 'hasToken')
            hasToken.on('value', (tokenSnapshot) ->

                _token = tokenSnapshot.val()

                _.forEach(activeUsers, (user, username) ->

                    handle = user.handle

                    params =
                        id           : fan.replace(prefix, '')
                        access_token : _token
                        friendId     : handle

                    if handle.charAt(0) == 'ᶠ' and user.active and handle.replace(prefix, '') isnt fan.replace(prefix, '')

                        # @see https://developers.facebook.com/docs/reference/login/#permissions
                        facebookHandler.friendByLookup(params).then (friendData) ->

                            #visibleFriendsList = _.last(friendData)
                            #_visibleFriendsList = visibleFriendsList.body.data
                            #_.forEach _visibleFriendsList, (friend) ->

                            #if handle.replace(prefix, '') == friend.id
                            if friendData
                                createFriend("users/#{fan}/friends/" + _prefix, handle, user, Merchant, prefix, fan, $stampId)

                )

            )

        return
    )

    return


friendByRoll = (credentials, customer, cursor) ->
    ###
    # friendByRoll

    @descriptinon

    Check against a friends list for a particular Twitter use. Create stampImpressionsBlue 
    for all users who are following on Twitter, and who exist in LoveStamp's users tree.

    @param {object} credentials
    @param {object} customer
    @param {string||number} cursor
    @return {undefined}
    ###

    _cursor          = if not cursor then -1 else cursor
    isManualAddition = if _.isString(cursor) then true else false
    Users            = new Firebase(baseDataApiUrl + 'users')
    prefix           = customer.charAt(0)
    blank            = ''
    sep              = ' '
    _prefix          = if prefix is '✚' then 'google' else if prefix is '@' then 'twitter' else 'facebook'
    customerCleaned  = customer.replace(prefix, blank)

    existingFriends = Users
      .child(customer)
        .child('friends')
            .child(_prefix)

    # @see https://api.twitter.com/1.1/friends/list.json?cursor=-1&screen_name=twitterapi&skip_status=true&include_user_entities=false
    oauth =
        consumer_key    : credentials.twitter.consumer_key
        consumer_secret : credentials.twitter.consumer_secret
        token           : credentials.twitter.access_token
        token_secret    : credentials.twitter.access_token_secret

    url = 'https://api.twitter.com/1.1/friends/list.json?'

    params =
        screen_name           : customer.replace(prefix, '')
        skip_status           : true
        include_user_entities : false
        cursor                : _cursor
        count                 : 200

    url += qs.stringify(params)

    t = null

    request.get(
        url   : url
        oauth : oauth
        json  : true
    , (e, r, result) ->
        c = 0
        t = setTimeout(() ->
            #console.log result

            _.forEach result.users, (user) ->

                return  if result.next_cursor is -1

                # Bail in invalid Twitter users
                return  if user and not user.id

                found = false

                _.forEach Users, (_user, username) ->
                    _username = username.replace(username.charAt(0), '')
                    if _username is user.screen_name
                        found = true

                if found and user.following and ! user.blocking

                    friendId = user.screen_name

                    friendObject =
                        'id'                      : user.id
                        'handle'                  : user.screen_name,
                        'url'                     : user.url,
                        'verified'                : user.verified,
                        'protected'               : user.protected,
                        'profile_image_url'       : user.profile_image_url,
                        'profile_image_url_https' : user.profile_image_url_https,
                        'lang'                    : user.lang,
                        'follow_request_sent'     : user.follow_request_sent,
                        'following'               : user.following,
                        'description'             : user.description,
                        'created_at'              : user.created_at,
                        'blocking'                : user.blocking,
                        'blocked_by'              : user.blocked_by

                    createFriend("users/#{customer}/friends/" + _prefix, friendId, friendObject, customer)

            friendByRoll(credentials, customerCleaned, result.next_cursor)

            if c > 1000
                clearTimeout(t)
                return

            c += 1000
        , 1000, false)
    )
    return


postToTwitter = (credentials, merchant, customer, logglyClient, messageConstruct, stamp) ->
    ###
    # postToTwitter

    Post via OAuth to Twitter. Update stampImpressionsBlue on successful posts.

    @param {object} credentials
    @param {string} merchant Typically a string construction that prefixes a 
                    special symbol to represent the base social network 
                    of the user.
    @param {string} customer See above description param.
    @param {object} logglyClient
    @param {object} messageConstruct
    @return {undefined}
    ###

    $stampId = stamp

    sourceIp = '0.0.0.0'

    #T = new Twit
        #consumer_key        : credentials.twitter.consumer_key
        #consumer_secret     : credentials.twitter.consumer_secret
        #access_token        : credentials.twitter.access_token
        #access_token_secret : credentials.twitter.access_token_secret

    twitter = new twitterAPI(
        consumerKey    : credentials.twitter.consumer_key,
        consumerSecret : credentials.twitter.consumer_secret,
        callback       : 'https://auth.firebase.com/v2/lovestamp/auth/twitter/callback'
    )

    merchantComponent = merchant
    customerComponent = customer

    twitter.statuses("update", {
            status: messageConstruct
        },
        credentials.twitter.access_token,
        credentials.twitter.access_token_secret,
        (error, data, response) ->

            if error

                logglyClient.log "#{sourceIp}", [ 'twitterError' ], (error, result) ->
                    console.log('Twitter error: ', error)

            else

                logglyClient.log "#{sourceIp} - #{data}", [ 'twitterSuccess' ], (err, result) ->
                    console.log('Twitter post: ', result)

            friendByLookup(credentials, customer, merchant, $stampId)

    )

    #T.post('statuses/update', messageConstruct, (err, data, response) ->

        #console.log err
        ##console.log data
        ##console.log response

        #if !err

            #logglyClient.log "#{sourceIp} - #{data}", [ 'twitterSuccess' ], (err, result) ->
                #console.log('Twitter post: ', result)

        #else

            #logglyClient.log "#{sourceIp} - #{err}", [ 'twitterError' ], (error, result) ->
                #console.log('Twitter error: ', error)

        #friendByLookup(credentials, customer, merchant, $stampId)

    #)

    return


friendByManual = (credentials, customer, cursor) ->
    ###

    Use to discover users who have public conversations on Twitter.

    @usage

        friendByManual(credentials, 'handle'[, 'handleToAddBy'])

    @param {object} credentials
    @param {string} customer
    @param {}
    ###

    _cursor          = if not cursor then -1 else cursor
    isManualAddition = if _.isString(cursor) then true else false
    Users            = new Firebase(baseDataApiUrl + 'users')
    prefix           = customer.charAt(0)
    blank            = ''
    sep              = ' '
    _prefix          = if prefix is '✚' then 'google' else if prefix is '@' then 'twitter' else 'facebook'
    customerCleaned  = customer.replace(prefix, blank)

    T = new Twit
        consumer_key        : credentials.twitter.consumer_key
        consumer_secret     : credentials.twitter.consumer_secret
        access_token        : credentials.twitter.access_token
        access_token_secret : credentials.twitter.access_token_secret

    #if isManualAddition
        #T.get('followers/ids', { screen_name: customerCleaned }, (err, data, response) ->
          #console.log(data)
        #)

    if isManualAddition
        manuallyAddedFan = cursor

        T.get('search/tweets', { q: '@' + manuallyAddedFan + sep + '@' + customerCleaned, count: 10 }, (err, data, response) ->

            return  if err isnt null

            statuses = data.statuses
            user = data
            friendId = manuallyAddedFan

            friendObject =
                handle: friendId

            if _.isArray(statuses) and statuses.length
                createFriend("users/#{customer}/friends/" + _prefix, friendId, friendObject, customer)
        )

    return


module.exports =
    postToTwitter  : postToTwitter
