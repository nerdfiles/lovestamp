###
# fileOverview

                             _
                            | |
      ____  ___   ___   ____| | _____
     / _  |/ _ \ / _ \ / _  | || ___ |
    ( (_| | |_| | |_| ( (_| | || ____|
     \___ |\___/ \___/ \___ |\_)_____)
    (_____|           (_____|

@module social  

@see https://github.com/google/google-api-nodejs-client/

## description

Transactions with the Moment system of Google's Content Stream APIs.

Pass a credentials object along to notify Google's Content Stream APIs that LoveStamp has successlly
communicated with the ChangeTip API. Merchant and Customer handles must be readily available to 
identify the proper social networking protocol to use, and then pass in the narratological constructs 
received from earlier steps in the program flow; likely informed by the works of lovebot.

###

gapi = require('googleapis')
Batchelor = require('batchelor')
path = require('path')
Chance = require('chance')
Firebase = require('firebase')


try
    Q = require('Q')
catch e
    Q = require('q')

baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 


_postToGoogle = (credentials, merchant, customer, logglyClient, messageConstruct, _localControlStamp) ->
    ###
    Use a query to find a friend for a given user in a G+ Circle.

    @see http://stackoverflow.com/a/27283759/412244,
         https://developers.google.com/+/domains/authentication/delegation#delegate_domain-wide_authority_to_your_service_account,
         https://developers.google.com/admin-sdk/directory/v1/guides/delegation

    @see http://christianreyes.azurewebsites.net/google-api-node-js-authentication-and-usage-2/,
         https://developers.google.com/+/web/api/rest/oauth#scopes,
         https://www.firebase.com/docs/web/guide/login/google.html#section-logging-in

    @param {object} credentials  
      Some keys.
    @param {object} params  
      Some auth and userId data.
    ###

    # Set up deferred, asynchronous object to pass to Twitter.
    def = Q.defer()

    serviceEmail = '179206523209-3t43lgu9uillkjdfa6jnv14l4hnlj3ev@developer.gserviceaccount.com'
    impersonationEmail = 'lovestampreward@gmail.com'
    scopes = [
      'https://www.googleapis.com/auth/plus.me'
      'https://www.googleapis.com/auth/plus.media.upload'
      'https://www.googleapis.com/auth/plus.profiles.read'
      'https://www.googleapis.com/auth/plus.stream.read'
      'https://www.googleapis.com/auth/plus.stream.write'
      'https://www.googleapis.com/auth/plus.circles.read'
      'https://www.googleapis.com/auth/plus.circles.write'
      #'https://www.googleapis.com/auth/plus.me'
      #'https://www.googleapis.com/auth/plus.stream.write'
      #'https://www.googleapis.com/auth/plus.login'
      #'https://www.googleapis.com/auth/plus.circles.read'
    ]
    # @note Takes an additional argument that is the e-mail of the Domains+ to impersonate.
    keyPath = path.join(__dirname, '../../certs/lovestamp-io-cc72c7288021.pem')
    key = null

    jwtClient = new gapi.auth.JWT(serviceEmail, keyPath, key, scopes)

    CLIENT_ID = credentials.google.client_id
    CLIENT_SECRET = credentials.google.client_secret
    REDIRECT_URL = credentials.google.redirect_url

    OAuth2 = gapi.auth.OAuth2

    #Plus = gapi.plus('v1')
    PlusDomains = gapi.plusDomains('v1')

    oauth2Client = new OAuth2(
        CLIENT_ID,
        CLIENT_SECRET,
        REDIRECT_URL
    )

    prefix = '✚'
    username = prefix + '101600582096097255226'

    hasToken = new Firebase(baseDataApiUrl + 'users' + '/' + username + '/' + 'hasToken')
    hasToken.on('value', (tokenSnapshot) ->

      # Retrieve tokens via token exchange explained above or set them:
      oauth2Client.setCredentials({
          access_token: tokenSnapshot.val()
          #refresh_token: crednetials.google.refresh_token
      })

      messageName = 'You have been LoveStamped!'

      signalRandomizer = new Chance()
      idGen = signalRandomizer.string(
        length : 20
        pool   : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
      )

      jwtClient.authorize (err, tokens) ->
        if err
          throw err

        targetObject =
          id           : idGen
          originalContent  : messageConstruct
          content  : [
            '<p>'
            messageConstruct
            '</p>'
          ].join ''

        #addActivity = 'http://schemas.google.com/AddActivity'

        # @see https://developers.google.com/+/domains/api/activities#resource
        PlusDomains.activities.insert {
          #auth    : oauth2Client
          auth     : jwtClient
          userId   : '101600582096097255226'
          preview  : false

          resource :
            object : targetObject
            verb   : 'post'
            access   :
              items : [{
                  type : 'public'
              }],
              domainRestricted: false
        }, (err, response) ->
          console.log err
          console.log response
          if err
            def.reject err.code
          else
            def.resolve response
          return
    )

    def.promise



friendByLookup = (credentials, params) ->
    ###
    Use a query to find a friend for a given user in a G+ Circle.

    @see http://stackoverflow.com/a/27283759/412244,
         https://developers.google.com/+/domains/authentication/delegation#delegate_domain-wide_authority_to_your_service_account,
         https://developers.google.com/admin-sdk/directory/v1/guides/delegation

    @see http://christianreyes.azurewebsites.net/google-api-node-js-authentication-and-usage-2/,
         https://developers.google.com/+/web/api/rest/oauth#scopes,
         https://www.firebase.com/docs/web/guide/login/google.html#section-logging-in

    @param {object} credentials  
      Some keys.
    @param {object} params  
      Some auth and userId data.
    ###

    # Set up deferred, asynchronous object to pass to Twitter.
    def = Q.defer()

    # Needful strings
    blank = ''
    symbol_plus = '+'

    # Needful Google OAuth
    OAuth2 = gapi.auth.OAuth2

    # Obligatory Google OAuth setup
    CLIENT_ID = credentials.google.client_id
    CLIENT_SECRET = credentials.google.client_secret
    REDIRECT_URL = credentials.google.redirect_url

    # Configure OAuth credentials
    oauth2Client = new OAuth2(
        CLIENT_ID,
        CLIENT_SECRET,
        credentials.google.redirect_url_local
    )

    # Configure parameters for Google+ People List
    _params =
        userId : params.userId.replace('✚', blank)
        auth   : oauth2Client

    # Set credentials
    _params.auth.setCredentials(access_token: params.access_token)

    # Plus v1
    Plus = gapi.plus('v1')

    # Call list
    Plus.people.list {
        userId     : _params.userId
        collection : 'visible'
        auth       : _params.auth
    }, (error, circles) ->
        if !error
            def.resolve circles
        else
            def.reject error

    def.promise


friendByLookupBatch = (credentials, params) ->
    ###
    Friend by Circle

    @unused
    @param {object} credentials
      Some keys.
    @param {object} params
      Some auth and userId data.
    @return {Promise:response} response
      Some JSON.
    ###

    serviceEmail = '179206523209-3t43lgu9uillkjdfa6jnv14l4hnlj3ev@developer.gserviceaccount.com'
    impersonationEmail = 'lovestampreward@gmail.com'
    scopes = [
      'https://www.googleapis.com/auth/plus.me'
      'https://www.googleapis.com/auth/plus.stream.write'
    ]
    # @note Takes an additional argument that is the e-mail of the Domains+ to impersonate.
    keyPath = path.join(__dirname, '../../certs/lovestamp-io-cc72c7288021.pem')
    key = null

    jwtClient = new gapi.auth.JWT(serviceEmail, keyPath, key, scopes)

    def = Q.defer()
    blank = ''
    symbol_plus = '+'

    #console.log params
    userId = params.userId.replace('✚', blank)

    jwtClient.authorize (err, tokens) ->
      if err
        throw err

      batch = new Batchelor(
          'uri'     : 'https://www.googleapis.com/batch'
          'method'  : 'POST'
          'auth'    : 'bearer': [
              tokens.access_token
          ]
          'headers' : 'Content-Type' : 'multipart/mixed')

      batch.add({
          'method' : 'GET',
          'path'   : "/plusDomains/v1/people/#{userId}/circles"
      })

      batch.run((response) ->
          #res.json(response)
          def.resolve response
      )

    def.promise


_friendByLookupBatch = (credentials, params) ->
    ###
    Friend by Circle

    @unused
    @param {object} credentials
      Some keys.
    @param {object} params
      Some auth and userId data.
    @return {Promise:response} response
      Some JSON.
    ###

    def = Q.defer()

    blank = ''
    symbol_plus = '+'

    # Needful Google OAuth
    OAuth2 = gapi.auth.OAuth2

    # Obligatory Google OAuth setup
    CLIENT_ID = credentials.google.client_id
    CLIENT_SECRET = credentials.google.client_secret
    REDIRECT_URL = credentials.google.redirect_url

    # Configure OAuth credentials
    oauth2Client = new OAuth2(
        CLIENT_ID,
        CLIENT_SECRET,
        credentials.google.redirect_url_local
    )

    # Configure parameters for Google+ People List
    _params =
        userId : params.userId.replace('✚', blank)
        auth   : oauth2Client

    # Set credentials
    _params.auth.setCredentials(
        access_token : params.access_token
    )

    batch = new Batchelor(
        uri     : 'https://www.googleapis.com/batch'
        method  : 'POST'
        auth    : 'bearer': [ tokens.access_token ]
        headers : 'Content-Type' : 'multipart/mixed'
    )

    batch.add(
        method : 'GET',
        path   : "/plusDomains/v1/people/#{userId}/circles"
    )

    batch.run((response) ->
        #res.json(response)
        def.resolve response
    )

    def.promise


_streamInsert = (credentials, merchant, customer) ->
    ###
    Stream (Moment?) Insert

    Google now supports the Domains+ API for applications (May 26th, 2015).

    @unused
    @param {object} credentials
      Some keys.
    @param {object} params
      Some auth and userId data.
    @return {undefined}
    ###

    CLIENT_ID = credentials.google.client_id
    CLIENT_SECRET = credentials.google.client_secret
    REDIRECT_URL = credentials.google.redirect_url

    OAuth2 = gapi.auth.OAuth2

    Plus = gapi.plus('v1')

    oauth2Client = new OAuth2(
        CLIENT_ID,
        CLIENT_SECRET,
        REDIRECT_URL
    )

    # Retrieve tokens via token exchange explained above or set them:
    oauth2Client.setCredentials({
        access_token: credentials.google.access_token
        #refresh_token: crednetials.google.refresh_token
    })

    userId = 'me' # comes from client angularjs app
    loveStampMomentId =
        'userId'     : userId
        'collection' : 'vault'

    loveStampMomentMessageName = ''
    loveStampMomentMessageDescription = ''
    loveStampMomentMessageBody = ''
    loveStampMoment =
        "type":"http://schemas.google.com/CreateActivity",
        "target":
            "id": "target-id-2",
            "type": "http://schema.org/CreativeWork",
            "name": loveStampMomentMessageName,
            "description": loveStampMomentMessageDescription,
            "text": loveStampMomentMessageBody

    #google_scope = "https://www.googleapis.com/auth/plus.me,
                   #https://www.googleapis.com/auth/plus.media.upload,
                   #https://www.googleapis.com/auth/plus.profiles.read,
                   #https://www.googleapis.com/auth/plus.stream.read,
                   #https://www.googleapis.com/auth/plus.stream.write,
                   #https://www.googleapis.com/auth/plus.circles.read,
                   #https://www.googleapis.com/auth/plus.circles.write"

    oauth2Client.client.plus.moments.insert(
        loveStampMomentId,
        loveStampMoment
    ).execute((result) ->
        console.log(result)
    )
    return


module.exports =
    friendByLookup : friendByLookup
    postToGoogle   : _postToGoogle
    friendByCircle : _friendByLookupBatch
