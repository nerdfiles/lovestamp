###
# fileOverview

        ___                   _                 _
       / __)                 | |               | |
     _| |__ _____  ____ _____| |__   ___   ___ | |  _
    (_   __|____ |/ ___) ___ |  _ \ / _ \ / _ \| |_/ )
      | |  / ___ ( (___| ____| |_) ) |_| | |_| |  _ (
      |_|  \_____|\____)_____)____/ \___/ \___/|_| \_)

## description

@see https://github.com/Thuzi/facebook-node-sdk,
     https://developers.facebook.com/docs/graph-api/common-scenarios/

###

graph = require('fbgraph')

Firebase = require('firebase')
https = require('https')
request = require('request')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/'

try
  Q = require('Q')
catch e
  Q = require('q')


_getFriends = (access_token, apiPath) ->
    ###
    getFriends
    ###

    def = Q.defer()
    options =
        host   : 'graph.facebook.com'
        port   : 443
        path   : apiPath + '&access_token=' + access_token #apiPath example : '/me/friends'
        method : 'GET'

    buffer = '' # this buffer will be populated with the chunks of the data received from facebook

    console.dir options
    request = https.get(options, (result) ->

        result.setEncoding('utf8')

        result.on('data', (chunk) ->
            buffer += chunk
        )

        result.on('end', () ->
            def.resolve buffer
        )

    )

    request.on('error', (e) ->
        # @schema e.message
        def.reject e
    )

    request.end()

    def.promise

_getFriendsBatch = (access_token, relative_url) ->
    ###
    Get Friends Batch
    ###

    def = Q.defer()

    graph.setAccessToken access_token

    friendsBatchQuery =
        method       : 'GET'
        relative_url : relative_url

    graph.batch([
      friendsBatchQuery
    ], (error, responseData) ->
      if !error
        def.resolve responseData
    )

    def.promise


_getFriendById = (access_token, relative_url) ->
    ###
    Get Friend By Id
    ###

    def = Q.defer()

    graph.setAccessToken access_token

    friendsBatchQuery =
        method       : 'GET'
        relative_url : relative_url

    graph.batch([
      friendsBatchQuery
    ], (error, responseData) ->
      if !error
        def.resolve responseData
    )

    def.promise

friendByLookup = (params) ->
    ###

    @param {object} params
    @return {Promise} friendData
    ###

    def = Q.defer()

    #url = 'fql?q=' + params.q
    #_getFriends(access_token, url).then (friendData) ->
    #console.dir friendData
    #def.resolve friendData

    #limitQuery = 1000
    id = params.id
    friendId = params.friendId

    #relative_url = "#{id}/friends?limit=#{limitQuery}"
    relative_url = "#{id}/friends/#{friendId}"

    access_token = params.access_token

    _getFriendById(access_token, relative_url).then((friendData) ->
      def.resolve friendData
    )

    #_getFriendsBatch(access_token, relative_url).then((friendData) ->
        ##console.dir friendData
        #def.resolve friendData
    #)

    def.promise


#_postToFacebook = (obj, cb) ->
    ####
    #Post to Facebook

    #@inner
    ####

    #req = https.request({
        #host: 'graph.facebook.com'
        #path: "/#{obj.id}/feed"
        #method: 'POST'
    #}, (res) ->
        #res.setEncoding 'utf8'
        #res.on 'data', (chunk) ->
          #console.log 'got chunk ' + chunk
          #return
        #res.on 'end', ->
          #console.log 'response end with status ' + res.status
          #cb()
          #return
        #return
    #)
    #msg = obj._messageConstruct.message
    #req.end 'message=' + encodeURIComponent(msg) + '&access_token=' + encodeURIComponent(credentials.facebook.access_token)
    #console.log 'sent'
    #return


postToFacebook = (credentials, merchant, customer, logglyClient, messageConstruct, stamp) ->
    ###
    ###

    $stampId = stamp

    sourceIp = '0.0.0.0'
    _messageConstruct =
      message: messageConstruct

    id = 'LoveStampio'
    #id = 'the.spinozist'

    _obj =
        id: id
        _messageConstruct: _messageConstruct

    graph.setAccessToken credentials.facebook.access_token

    body = messageConstruct

    wallPost =
        message: body

    graph.post "#{id}/feed", wallPost, (res) ->
        console.log res
        if res.error
          console.log if !res then 'error occurred' else res.error
          return

        logglyClient.log "#{id}", [ 'facebookSuccess' ], (error, result) ->
            console.log('Facebook post:', result)

        return

    return

module.exports =
    postToFacebook : postToFacebook
    friendByLookup : friendByLookup
