(function() {
  var app, express, request, should;

  request = require('supertest');

  should = require('should');

  express = require('express');

  app = express();

  app.use(express.cookieParser());

  describe('request.agent(app)', function() {
    var agent;
    app = express();
    app.use(express.cookieParser());
    app.get('/', function(req, res) {
      res.cookie('cookie', 'hey');
      res.send();
    });
    app.get('/return', function(req, res) {
      if (req.cookies.cookie) {
        res.send(req.cookies.cookie);
      } else {
        res.send(':(');
      }
    });
    agent = request.agent(app);
    it('should save cookies', function(done) {
      agent.get('/').expect('set-cookie', 'cookie=hey; Path=/', done);
    });
    it('should send cookies', function(done) {
      agent.get('/return').expect('hey', done);
    });
  });

}).call(this);
