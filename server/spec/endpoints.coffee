
request = require('supertest')
should = require('should')
express = require('express')
app = express()
app.use express.cookieParser()

describe 'request.agent(app)', ->
  app = express()
  app.use express.cookieParser()
  app.get '/', (req, res) ->
    res.cookie 'cookie', 'hey'
    res.send()
    return
  app.get '/return', (req, res) ->
    if req.cookies.cookie
      res.send req.cookies.cookie
    else
      res.send ':('
    return
  agent = request.agent(app)
  it 'should save cookies', (done) ->
    agent.get('/').expect 'set-cookie', 'cookie=hey; Path=/', done
    return
  it 'should send cookies', (done) ->
    agent.get('/return').expect 'hey', done
    return
  return
