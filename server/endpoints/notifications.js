(function() {
  var credentials, push, push_apn, push_curl, push_gcm, _credentials;

  push = require('../utils/push');

  _credentials = require('../config/credentials');

  credentials = _credentials.credentials();

  push_gcm = function(request, response) {
    var requestBody;
    requestBody = request.body;
    push.gcmNotification(requestBody);
    return response.send(true);
  };

  push_curl = function(request, response) {
    var requestBody;
    requestBody = request.body;
    push.curlNotification(requestBody);
    return response.send(true);
  };

  push_apn = function(request, response) {
    var requestBody;
    requestBody = request.body;
    push.apnNotification(requestBody);
    return response.send(true);
  };

  module.exports = {
    push_gcm: push_gcm,
    push_apn: push_apn,
    push_curl: push_curl
  };

}).call(this);
