(function() {
  var FirebaseTokenGenerator, _connect;

  FirebaseTokenGenerator = require("firebase-token-generator");

  _connect = function(request, response) {

    /*
    @namespace social/connect
     */
    var provider, requestBody, token, tokenGenerator, uid, username;
    requestBody = request.body;
    provider = requestBody.provider;
    username = requestBody.username;
    uid = requestBody.uid;
    tokenGenerator = new FirebaseTokenGenerator("xKRQJypIBygR6rfgt3gPbGiI6NsBqaPxrJueOC27");
    token = tokenGenerator.createToken({
      uid: "social_connect : " + uid,
      username: username,
      provider: provider
    });
    return res.send(token);
  };

  module.exports = {
    connect: _connect
  };

}).call(this);
