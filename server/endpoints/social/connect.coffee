FirebaseTokenGenerator = require("firebase-token-generator")

_connect = (request, response) ->
  ###
  @namespace social/connect
  ###

  requestBody = request.body
  provider = requestBody.provider
  username = requestBody.username
  uid = requestBody.uid

  tokenGenerator = new FirebaseTokenGenerator("xKRQJypIBygR6rfgt3gPbGiI6NsBqaPxrJueOC27")

  token = tokenGenerator.createToken(
    uid      : "social_connect : " + uid
    username : username
    provider : provider
  )

  res.send token


module.exports =
  connect: _connect
