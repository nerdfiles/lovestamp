push = require('../utils/push')
_credentials = require('../config/credentials')
credentials = _credentials.credentials()

push_gcm = (request, response) ->
  requestBody = request.body
  push.gcmNotification(requestBody)
  response.send(true)


push_curl = (request, response) ->
  requestBody = request.body
  push.curlNotification(requestBody)
  response.send(true)

push_apn = (request, response) ->
  requestBody = request.body
  push.apnNotification(requestBody)
  response.send(true)


module.exports =
  push_gcm  : push_gcm
  push_apn  : push_apn
  push_curl : push_curl
