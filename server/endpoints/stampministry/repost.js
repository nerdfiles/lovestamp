
/*
@endpoints stampministry stamp stamps
 */

(function() {
  var Firebase, Q, baseDataApiUrl, changetipHandler, credentials, e, logglyClient, moment, repost, twitterHandler, _, _credentials;

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  changetipHandler = require('../../social/changeTip');

  twitterHandler = require('../../social/twitter');

  _ = require('lodash');

  _credentials = require('../../config/credentials');

  credentials = _credentials.credentials();

  logglyClient = require('../../utils/loggly');

  try {
    Q = require('q');
  } catch (_error) {
    e = _error;
    Q = require('Q');
  }

  moment = require('moment');

  repost = function(req, res, next) {

    /*
    @description
    
    A simple end-point for reposting to social networks under failover conditions.
    
    @return {Response:boolean} true
     */
    var Users, createTipUrl, def, foundStamp, host, localControlStamp, merchant, provider, receipt, requestBody, username;
    requestBody = req.body;
    merchant = requestBody.merchant;
    username = requestBody.username;
    createTipUrl = requestBody.createTipUrl;
    receipt = requestBody.receipt;
    host = req.headers.host;
    def = Q.defer();
    Users = new Firebase("" + baseDataApiUrl + "users");
    localControlStamp = Users.child(username).child('stamps').child('success').child(merchant).push({
      snowshoe: receipt
    });
    provider = username.charAt(0) === '@' ? 'twitter' : username.charAt(0) === 'ᶠ' ? 'facebook' : 'google';
    console.log('test----------------------' + provider);
    foundStamp = false;
    Users.child(merchant).child('stamps').child('local').once('value', function(v, k) {
      var _v;
      _v = v.val();
      return _.forEach(_v, function(stamp, keyName) {
        var domain, firstImpressionDate, statusSaturation, _localControlStamp;
        if (receipt.stamp.serial === stamp.receipt.validation.stamp.serial && foundStamp === false) {
          foundStamp = true;
          firstImpressionDate = moment().format("MM/DD/YYYY HH:mm:ss");
          statusSaturation = Users.child(merchant).child('stamps').child('local').child(keyName).child('statusSaturation');
          statusSaturation.once('value', function(_statusSaturation) {
            if (_statusSaturation.val() === 2 && _statusSaturation.val() !== 3) {
              Users.child(merchant).child('stamps').child('local').child(keyName).child('firstImpressionDate').set(firstImpressionDate.format("MM/DD/YYYY HH:mm:ss"));
              return statusSaturation.set(3);
            }
          });
          _localControlStamp = keyName;
          domain = host.replace(/\./g, '-');
          return changetipHandler(merchant, username, _localControlStamp, credentials, createTipUrl, domain).then(function(responseObject) {
            var messageConstruct, respostConstruct;
            console.log('responseObject');
            console.dir(responseObject);
            if (responseObject.status && responseObject.status === 'error') {
              def.reject(responseObject);
              return;
            }
            if (responseObject.tipUrl === null) {
              def.reject({
                status: 'error'
              });
              return;
            }
            messageConstruct = responseObject.messageConstruct;
            responseObject.stamp = receipt.stamp;
            if (createTipUrl === false) {
              try {
                twitterHandler.postToTwitter(credentials, merchant, username, logglyClient, messageConstruct, _localControlStamp);
              } catch (_error) {
                e = _error;
                console.log(e);
              }
            }
            respostConstruct = responseObject;
            return def.resolve(respostConstruct);
          }, function(error) {
            return def.reject({
              status: 'error'
            });
          });
        }
      });
    });
    def.promise.then(function(repostConstruct) {
      return res.send(repostConstruct);
    }, function(errorData) {
      return res.send(errorData);
    }).done();
  };

  module.exports = {
    response: repost
  };

}).call(this);
