sss = require('../authentication/sss')
auth = sss.auth

_get_serial = (req, res) ->
  ###
  Get Stamp Serial

  @namespace stamp/get-serial
  @response {object} validation A validation receipt from SnowShoe's API.
  @description

  A simple end-point for validation of Stamps via SnowShoe.

  Used with /stamp/repost

  ###
  requestBody = req.body
  auth.validateStamp(requestBody, (json_validation) ->
    validation = JSON.parse(json_validation)
    res.send(validation)
  )

module.exports =
  get_serial: _get_serial
