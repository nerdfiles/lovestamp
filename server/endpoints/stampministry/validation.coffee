Firebase = require('firebase')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 
changetipHandler = require('../../social/changeTip')
twitterHandler = require('../../social/twitter')
sss = require('../authentication/sss')
auth = sss.auth
_credentials = require('../../config/credentials')
credentials = _credentials.credentials
_ = require('lodash')
logglyClient = require('../../utils/loggly')
moment = require('moment')


try
    Q = require('q')
catch e
    Q = require('Q')


_validate_point_data = (req, res) ->
  ###*
  @module endpoints
  @namespace stamp/:username/validate/:merchantHandle/:point_data
  @description

    For validation conditions if stampIdent is false and custom validation is bypassed.

  ###

  username = req.params.username
  requestBody = req.body
  try
    createTipUrl = requestBody.createTipUrl
  catch e
    console.log 'Failed to capture tip URL mode.'

  requestConstruct = requestBody.data
  stampData = requestBody.stampData
  merchant = req.params.merchantHandle
  point_data_validation = req.params.point_data
  bufValidation = new Buffer(point_data_validation, 'base64')
  stamp_signature = JSON.parse(bufValidation)
  Users = new Firebase("#{baseDataApiUrl}users")
  stampFound = false

  #stampCollection = (serial) ->
    ####
    #@inner
    ####

    #stampDef = Q.defer()
    #Users.once('value', (usersSnapshot) ->
      #_u = usersSnapshot.val()
      #_.forEach(_u, (userSnapshot, key) ->
        #user = userSnapshot
        #username = key
        #if user.stamps and user.stamps.local
          #locals = user.stamps.local
          ## User has local stamps
          #_.forEach(locals, (local, localKeyname) ->
            #if local.receipt and local.receipt.validation and local.receipt.validation.stamp
              #if serial == local.receipt.validation.stamp.serial
                #stampFound = true
                #stampDef.resolve localKeyname
          #)
      #)
      #if stampFound == false
        #stampDef.reject stampFound
    #)
    #stampDef.promise

  #auth.validateStamp(requestBody, (json_validation) ->
    #ss_validation = JSON.parse(json_validation)
    #serial = ss_validation.stamp.serial

    #stampCollection(serial).then((stampId) ->

      #console.log 'ss_validation'
  rt = _.last(stamp_signature)
  a = null

  try
    a = _.extend {}, {
      serial: stampData.receipt.validation.stamp.serial
      runTotal: rt.runTotal
      pointData: requestConstruct
      created: moment().format("MM/DD/YYYY HH:mm:ss")
    }
  catch e
    console.log e

  if stampData.stampOwner == merchant
    ref = Users.
      child(username).
      child('stamps').
      child('success').
      child(merchant).
      child('custom')

  foundKey = false
  receiptKey = null

  ref.once 'value', (snapshot) ->
    #v = snapshot.val()
    #_.forEach v, (rec, key) ->
      #if foundKey == false
        #if rec.snowshoe and rec.snowshoe.stamp and rec.snowshoe.serial and rec.snowshoe.stamp.serial == stampData.receipt.validation.stamp.serial
          #receiptKey = key
          #foundKey = true

    ref.push a
      #_ref.once 'value', (_snap) ->

      #ref.on 'child_added', (snapshot) ->
        #v = snapshot.val()
        #ref.set _v
    res.send a


      #changetipHandler(merchant, username, stampId, credentials).then((responseObject) ->

          #messageConstruct = responseObject.messageConstruct

          #_.extend responseObject,
              #validation        : validation
              #merchant          : merchant
              #localControlStamp : localControlStamp
              #stamp             : localControlStamp

          #if createTipUrl == false
            #try
              #twitterHandler.postToTwitter(credentials, merchant, username, logglyClient, messageConstruct, stampId)
            #catch e
              #console.log e

          #console.log responseObject
          #res.send responseObject

      #, (error) ->
        #_errorObject = _.extend {},
            #$meta             : error
            #validation        : ss_validation
            #merchant          : merchant
            #localControlStamp : localControlStamp.key()
            #stamp             : localControlStamp.key()
        #console.log _errorObject
        #res.send _errorObject
      #)
    #).done()

  #, (errorData) ->
    #res.send {
      #$meta             : errorData
      #validation        : false
      #merchant          : merchant
      #localControlStamp : false
      #stamp             : false
    #}
  #)

  return


module.exports =
  parse : _validate_point_data
