mailer = require('../mailer/base')
transport = mailer.mailerConstruct()
#requests_slack = require('../../utils/slack')
#loadHtmlFile = require('../../utils/load-html-file')
Firebase = require('firebase')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 
twitterHandler = require('../../social/twitter')
logglyClient = require('../../utils/loggly')
sss = require('../authentication/sss')
auth = sss.auth



try
    Q = require('q')
catch e
    Q = require('Q')

post = (request, response) ->
  ###
  @namespace stamp/:username/success/:merchant
  @schema {"stamp": {"serial": "lovestamp3"}, "receipt": "TQxTnWxFDwr3kwuQ+9UGvWNEOB4=", "secure": true, "created": "2015-01-17 15:06:15.482545"}
  @response {arrayLike} {validation: validation, merchant: user}
  @description

  The critical "moment" (think g+ "moments", most broadly) where a merchant 
  stamps a customer. Expect Touchy API endpoint arrays to pass to SnowShoe's 
  API.

  ###

  requestBody = request.body

  username = request.params.username
  handleType = request.params.handleType
  handler = request.params.status
  type = if handleType == 'local' then 'local' else if handleType == 'success' then 'success' else handleType
  hostname = request.headers.host
  portSpec = if request.headers.port != "" then (':' + request.headers.port) else ''
  protocol = request.headers.protocol
  hostname = 'http:' + '//' + hostname
  merchantDef = Q.defer()

  try
    createTipUrl = requestBody.createTipUrl
  catch e
    console.log 'Failed to capture tip URL mode.'

  Users = new Firebase("#{baseDataApiUrl}users")
  # Use SSS validation API.
  auth.validateStamp(requestBody, (json_validation) ->

    validation = JSON.parse(json_validation)

    sourceIp = '0.0.0.0'
    sourceMsg = JSON.stringify(validation)

    userCollection = () ->

      merchantDef = Q.defer()

      usernameFound = false
      Users.once('value', (usersSnapshot) ->
        usersSnapshot.forEach((userSnapshot) ->
          user = userSnapshot
          username = userSnapshot.key()

          locals = user.child('stamps').child('local')
          locals.forEach((local) ->
            if local.receipt and local.receipt.stamp
              if validation.stamp.serial == local.receipt.stamp.serial
                merchantDef.resolve username

          )
        )
        merchantDef.reject usernameFound
      )
      merchantDef.promise

    if validation.error

      Users
        .child(username)
          .child('stamps')
            .child('error')
              .child(validation.stamp.serial)
                .push(validation)

      logglyClient.log("#{sourceIp} - #{sourceMsg}", [ 'snowshoestampError' ], (err, result) ->
      )

    else

      # Update existing receipt...
      if handleType.charAt(0) is '-'
        merchantDef.promise.then (merchant) ->
          Users
            .child(username)
              .child('stamps')
                .child(status)
                  .child(merchant)
                    .child(handleType)
                      .child('receipt')
                        .push(validation)

      else
        merchantDef.promise.then (merchant) ->
          Users
            .child(username)
              .child('stamps')
                .child(status)
                  .child(merchant)
                    .child(validation.stamp.serial)
                      .push(validation)

      logglyClient.log("#{sourceIp} - #{sourceMsg}", [ 'snowshoestampSuccess' ], (err, result) ->
      )

      merchantDef.promise.then (merchant) ->
        if createTipUrl == false

          twitterHandler.postToTwitter(merchant, username)

    userCollection().then((user) ->
      return response.send({
        validation: validation
        merchant: user
      })
    , () ->
      return response.send({
        validation: validation
      })
    )
  )


#_method = (request, response) ->

  #requestBody = request.body

  #username = request.params.username
  #handleType = request.params.handleType
  #handler = request.params.status

  ##merchant = request.params.merchant

  #type = if handleType == 'local' then 'local' else if handleType == 'success' then 'success' else handleType
  ##passes = requestBody.passes

  #hostname = request.headers.host
  #portSpec = if request.headers.port != "" then (':' + request.headers.port) else ''
  #protocol = request.headers.protocol
  #hostname = 'http:' + '//' + hostname
  #merchantDef = Q.defer()
  #stampDef = Q.defer()
  #Users = new Firebase("#{baseDataApiUrl}users")

  ## @inner
  #stampCollection = (validation) ->

    #checked = false
    #stampDef = Q.defer()

    #Users.once('value', (usersSnapshot) ->
      #usersSnapshot.forEach((userSnapshot) ->
        #user = userSnapshot
        #username = userSnapshot.key()
        #locals = user.child('stamps').child('local')
        #locals.forEach((local, localKeyname) ->
          #if local.receipt and local.receipt.stamp
            #if validation.stamp.serial == local.receipt.stamp.serial
              #checked = true
              #stampDef.resolve localKeyname
        #)
      #)
      #if checked == false
        #stampDef.reject stampFound
    #)
    #stampDef.promise

  ## @inner
  #userCollection = (validation) ->

    #checked = false
    #merchantDef = Q.defer()

    #usernameFound = false
    #Users.once('value', (usersSnapshot) ->
      #usersSnapshot.forEach((userSnapshot) ->
        #user = userSnapshot
        #username = userSnapshot.key()

        #locals = user.child('stamps').child('local')
        #locals.forEach((local, localKeyname) ->
          #if local.receipt and local.receipt.stamp
            #if validation.stamp.serial == local.receipt.stamp.serial
              #checked = true
              #merchantDef.resolve username
        #)
      #)
      #if checked == false
          #merchantDef.reject usernameFound
    #)
    #merchantDef.promise

  ## Use SSS validation API.
  #auth.validateStamp(requestBody, (json_validation) ->

    #validation = JSON.parse(json_validation)

    #stampFound = false
    #sourceIp = '0.0.0.0'
    #sourceMsg = JSON.stringify(validation)

    ## Apply Promises
    #if validation.error

      #logglyClient.log("#{sourceIp} - #{sourceMsg}", [ 'snowshoestampError' ], (err, result) ->
        #Users
          #.child(username)
            #.child('stamps')
              #.child('error')
                #.child(validation.stamp.serial)
                  #.push(validation)

      #)

    #else

      #userCollection(validation).then (merchant) ->
        ## Update existing receipt...
        #if handleType.charAt(0) is '-'
            #Users
              #.child(username)
                #.child('stamps')
                  #.child(status)
                    #.child(merchant)
                      #.child(handleType)
                        #.child('receipt')
                          #.push(validation)
        #else
            #Users
              #.child(username)
                #.child('stamps')
                  #.child(status)
                    #.child(merchant)
                      #.child(validation.stamp.serial)
                        #.push(validation)

        #logglyClient.log("#{sourceIp} - #{sourceMsg}", [ 'snowshoestampSuccess' ], (err, result) ->
        #)

        #stampCollection(validation).then (stampId) ->
            #changetipHandler(merchant, username, stampId, credentials).then((responseObject) ->
                #messageConstruct = responseObject.messageConstruct
                #twitterHandler.postToTwitter(credentials, merchant, username, logglyClient, messageConstruct)
                #_.extend responseObject,
                  #validation: validation
                  #merchant: user
                #response.send responseObject
            #, (error) ->
                #_.extend responseObject,
                  #validation: validation
                  #merchant: error
                #response.send responseObject
            #)
        #, (error) ->
  #)



module.exports =
  post : post
