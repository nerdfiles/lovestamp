(function() {
  var auth, sss, _get_serial;

  sss = require('../authentication/sss');

  auth = sss.auth;

  _get_serial = function(req, res) {

    /*
    Get Stamp Serial
    
    @namespace stamp/get-serial
    @response {object} validation A validation receipt from SnowShoe's API.
    @description
    
    A simple end-point for validation of Stamps via SnowShoe.
    
    Used with /stamp/repost
     */
    var requestBody;
    requestBody = req.body;
    return auth.validateStamp(requestBody, function(json_validation) {
      var validation;
      validation = JSON.parse(json_validation);
      return res.send(validation);
    });
  };

  module.exports = {
    get_serial: _get_serial
  };

}).call(this);
