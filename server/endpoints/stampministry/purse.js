(function() {
  var Firebase, baseDataApiUrl, changetipHandler, credentials, _credentials, _purseUpdate;

  changetipHandler = require('../../social/changeTip');

  _credentials = require('../../config/credentials');

  credentials = _credentials.credentials();

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  _purseUpdate = function(req, res) {
    var LSRoot, createTipUrl, currentProfile, currentPurse, domain, handle, host, merchant, requestBody, u, vendor, _localControlStamp;
    requestBody = req.body;
    handle = requestBody.handle;
    createTipUrl = requestBody.createTipUrl;
    vendor = requestBody.vendorId;
    host = req.headers.host;
    merchant = null;
    _localControlStamp = null;
    LSRoot = new Firebase(baseDataApiUrl);
    u = LSRoot.child('users').child(handle);
    currentProfile = u.child('profile');
    currentPurse = currentProfile.child('purse');
    domain = host.replace(/\./g, '-');
    return changetipHandler(merchant, handle, _localControlStamp, credentials, createTipUrl, domain).then(function(responseObject) {
      if (responseObject.tipUrl !== null) {
        currentPurse.set(0);
        return res.send(responseObject);
      } else {
        return res.send(responseObject);
      }
    });
  };

  module.exports = {
    update_purse: _purseUpdate
  };

}).call(this);
