mailer = require('../mailer/base')
transport = mailer.mailerConstruct()
slack = require('../../utils/slack')
requests_slack = slack.requests_slack
loadHtmlFile = require('../../utils/load-html-file')
Firebase = require('firebase')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 


_request = (request, response) ->
  ###
  @namespace stamp/:username/request
  @description

    Stamp request via Get a Stamp controllers.

  @response boolean
  ###

  requestBody = request.body
  username = request.params.username
  hostname = request.headers.host
  portSpec = if request.headers.port != "" then (':' + request.headers.port) else ''
  protocol = request.headers.protocol
  hostname = 'http:' + '//' + hostname

  LSRoot = new Firebase('https://lovestamp.firebaseio.com/requests')

  LSRoot
    .child(username)
      .push requestBody
  requesterEmailHandle = null
  textBody = []
  htmlBody = ['<dl>']
  for i of requestBody
    label = i.split('contact')
    _label = label[1]
    if _label.indexOf('mail') != -1 and requestBody[i].indexOf('@') != -1
      requesterEmailHandle = requestBody[i]
    #validpattern = new RegExp('[A-Z]')
    htmlBody.push '<dt>Contact ' + _label + '</dt>'
    htmlBody.push '<dd>' + requestBody[i] + '</dd>'
    textBody.push 'Contact ' + _label
    textBody.push requestBody[i]
  messageText = textBody.join('\n')
  htmlBody.push('</dl>')
  messageHtml = htmlBody.join('')

  prefix = username.charAt(0)

  requests_slack.send(
      text     : 'Howdy! LoveStamp has a request to activate user: ' + hostname + '/admin/request/' + username + ' [click to activate]. \n\n Social Profile: ' + urlPrefix[prefix] + username.replace(prefix, '') + '\n\nForm details:\n' + messageText
      channel  : '#requests'
      username : 'LoveBot'
  )

  htmlEmailTemplateFilePath = __dirname + '/../../../email/newsletter/request.inline.html'

  loadHtmlFile(htmlEmailTemplateFilePath).then (htmlEmailConstruct) ->

    transport.sendMail(
      from    : 'webmaster@lovestamp.io'
      to      : requesterEmailHandle
      subject : 'Your request to activate user: ' + username
      text    : 'Howdy ' + username + '! Thanks for your request to LoveStamp. \n\n'
      html    : htmlEmailConstruct
      #html    : '<p>Howdy ' + username + '! Thanks for your request to LoveStamp.</p>'
    )

  transport.sendMail(
    from    : 'webmaster@lovestamp.io'
    to      : 'support@lovestamp.io'
    subject : 'LoveStamp requests to activate user: ' + username
    text    : 'Howdy! LoveStamp has a request to activate user: <a href="' + hostname + '/admin/request/'+username+'">' + username + '</a> [click to activate]. \n\n Social Profile: ' + urlPrefix[prefix] + username.replace(prefix, '') + '\n\nForm details:' + messageText
    html    : '<p>Howdy! LoveStamp has a request to activate user: <a href="' + hostname + '/admin/request/'+username+'">' + username + '</a> [click to activate].</p><p>Social Profile: ' + urlPrefix[prefix] + username.replace(prefix, '') + '</p><p>Form details:</p>' + messageHtml
  )

  response.send true


module.exports =
  request: _request
