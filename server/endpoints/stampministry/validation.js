(function() {
  var Firebase, Q, auth, baseDataApiUrl, changetipHandler, credentials, e, logglyClient, moment, sss, twitterHandler, _, _credentials, _validate_point_data;

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  changetipHandler = require('../../social/changeTip');

  twitterHandler = require('../../social/twitter');

  sss = require('../authentication/sss');

  auth = sss.auth;

  _credentials = require('../../config/credentials');

  credentials = _credentials.credentials;

  _ = require('lodash');

  logglyClient = require('../../utils/loggly');

  moment = require('moment');

  try {
    Q = require('q');
  } catch (_error) {
    e = _error;
    Q = require('Q');
  }

  _validate_point_data = function(req, res) {

    /**
    @module endpoints
    @namespace stamp/:username/validate/:merchantHandle/:point_data
    @description
    
      For validation conditions if stampIdent is false and custom validation is bypassed.
     */
    var Users, a, bufValidation, createTipUrl, foundKey, merchant, point_data_validation, receiptKey, ref, requestBody, requestConstruct, rt, stampData, stampFound, stamp_signature, username;
    username = req.params.username;
    requestBody = req.body;
    try {
      createTipUrl = requestBody.createTipUrl;
    } catch (_error) {
      e = _error;
      console.log('Failed to capture tip URL mode.');
    }
    requestConstruct = requestBody.data;
    stampData = requestBody.stampData;
    merchant = req.params.merchantHandle;
    point_data_validation = req.params.point_data;
    bufValidation = new Buffer(point_data_validation, 'base64');
    stamp_signature = JSON.parse(bufValidation);
    Users = new Firebase("" + baseDataApiUrl + "users");
    stampFound = false;
    rt = _.last(stamp_signature);
    a = null;
    try {
      a = _.extend({}, {
        serial: stampData.receipt.validation.stamp.serial,
        runTotal: rt.runTotal,
        pointData: requestConstruct,
        created: moment().format("MM/DD/YYYY HH:mm:ss")
      });
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    if (stampData.stampOwner === merchant) {
      ref = Users.child(username).child('stamps').child('success').child(merchant).child('custom');
    }
    foundKey = false;
    receiptKey = null;
    ref.once('value', function(snapshot) {
      ref.push(a);
      return res.send(a);
    });
  };

  module.exports = {
    parse: _validate_point_data
  };

}).call(this);
