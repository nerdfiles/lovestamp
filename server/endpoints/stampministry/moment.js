(function() {
  var Firebase, Q, auth, baseDataApiUrl, e, logglyClient, mailer, post, sss, transport, twitterHandler;

  mailer = require('../mailer/base');

  transport = mailer.mailerConstruct();

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  twitterHandler = require('../../social/twitter');

  logglyClient = require('../../utils/loggly');

  sss = require('../authentication/sss');

  auth = sss.auth;

  try {
    Q = require('q');
  } catch (_error) {
    e = _error;
    Q = require('Q');
  }

  post = function(request, response) {

    /*
    @namespace stamp/:username/success/:merchant
    @schema {"stamp": {"serial": "lovestamp3"}, "receipt": "TQxTnWxFDwr3kwuQ+9UGvWNEOB4=", "secure": true, "created": "2015-01-17 15:06:15.482545"}
    @response {arrayLike} {validation: validation, merchant: user}
    @description
    
    The critical "moment" (think g+ "moments", most broadly) where a merchant 
    stamps a customer. Expect Touchy API endpoint arrays to pass to SnowShoe's 
    API.
     */
    var Users, createTipUrl, handleType, handler, hostname, merchantDef, portSpec, protocol, requestBody, type, username;
    requestBody = request.body;
    username = request.params.username;
    handleType = request.params.handleType;
    handler = request.params.status;
    type = handleType === 'local' ? 'local' : handleType === 'success' ? 'success' : handleType;
    hostname = request.headers.host;
    portSpec = request.headers.port !== "" ? ':' + request.headers.port : '';
    protocol = request.headers.protocol;
    hostname = 'http:' + '//' + hostname;
    merchantDef = Q.defer();
    try {
      createTipUrl = requestBody.createTipUrl;
    } catch (_error) {
      e = _error;
      console.log('Failed to capture tip URL mode.');
    }
    Users = new Firebase("" + baseDataApiUrl + "users");
    return auth.validateStamp(requestBody, function(json_validation) {
      var sourceIp, sourceMsg, userCollection, validation;
      validation = JSON.parse(json_validation);
      sourceIp = '0.0.0.0';
      sourceMsg = JSON.stringify(validation);
      userCollection = function() {
        var usernameFound;
        merchantDef = Q.defer();
        usernameFound = false;
        Users.once('value', function(usersSnapshot) {
          usersSnapshot.forEach(function(userSnapshot) {
            var locals, user;
            user = userSnapshot;
            username = userSnapshot.key();
            locals = user.child('stamps').child('local');
            return locals.forEach(function(local) {
              if (local.receipt && local.receipt.stamp) {
                if (validation.stamp.serial === local.receipt.stamp.serial) {
                  return merchantDef.resolve(username);
                }
              }
            });
          });
          return merchantDef.reject(usernameFound);
        });
        return merchantDef.promise;
      };
      if (validation.error) {
        Users.child(username).child('stamps').child('error').child(validation.stamp.serial).push(validation);
        logglyClient.log("" + sourceIp + " - " + sourceMsg, ['snowshoestampError'], function(err, result) {});
      } else {
        if (handleType.charAt(0) === '-') {
          merchantDef.promise.then(function(merchant) {
            return Users.child(username).child('stamps').child(status).child(merchant).child(handleType).child('receipt').push(validation);
          });
        } else {
          merchantDef.promise.then(function(merchant) {
            return Users.child(username).child('stamps').child(status).child(merchant).child(validation.stamp.serial).push(validation);
          });
        }
        logglyClient.log("" + sourceIp + " - " + sourceMsg, ['snowshoestampSuccess'], function(err, result) {});
        merchantDef.promise.then(function(merchant) {
          if (createTipUrl === false) {
            return twitterHandler.postToTwitter(merchant, username);
          }
        });
      }
      return userCollection().then(function(user) {
        return response.send({
          validation: validation,
          merchant: user
        });
      }, function() {
        return response.send({
          validation: validation
        });
      });
    });
  };

  module.exports = {
    post: post
  };

}).call(this);
