(function() {
  var Firebase, baseDataApiUrl, logglyClient, _, _error, _local;

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  _ = require('lodash');

  logglyClient = require('../../utils/loggly');

  _error = function(request, response) {

    /**
    @module endpoints
    @namespace stamp/:username/error
    @description
      For storing errant validations.
     */
    var LSRoot, genericError, hostname, portSpec, protocol, username;
    username = request.params.username;
    hostname = request.headers.host;
    portSpec = request.headers.port !== "" ? ':' + request.headers.port : '';
    protocol = request.headers.protocol;
    hostname = request.headers.protocol + '//' + hostname + portSpec;
    LSRoot = new Firebase(baseDataApiUrl);
    genericError = {
      responseStatus: response.status(500)
    };
    LSRoot.child('users').child(username).child('stamps').child('error').push(genericError);
    return response.redirect(hostname + '/');
  };

  _local = function(request, response) {

    /*
    @namespace stamp/:username/local
    @description
    
    For Control Stamp creations.
     */
    var Users, hostname, portSpec, protocol, requestBody, sourceIp, sourceMsg, stampBuf, username;
    hostname = request.headers.host;
    portSpec = request.headers.port !== "" ? ':' + request.headers.port : '';
    protocol = request.headers.protocol;
    hostname = 'http:' + '//' + hostname;
    requestBody = request.body;
    stampBuf = new Buffer(requestBody.data, 'base64');
    username = request.params.username;
    Users = new Firebase("" + baseDataApiUrl + "users");
    Users.child(username).child('stamps').child('local').push(stampBuf);
    sourceIp = '0.0.0.0';
    sourceMsg = JSON.stringify(stampBuf);
    logglyClient.log("" + sourceIp + " - " + sourceMsg, ['stampLocalize'], function(err, result) {});
    return response.send(true);
  };

  module.exports = {
    error: _error,
    local: _local
  };

}).call(this);
