Firebase = require('firebase')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 
_ = require('lodash')
logglyClient = require('../../utils/loggly')


_error = (request, response) ->
  ###*
  @module endpoints
  @namespace stamp/:username/error
  @description
    For storing errant validations.

  ###

  username = request.params.username
  hostname = request.headers.host

  portSpec = if request.headers.port != "" then (':' + request.headers.port) else ''
  protocol = request.headers.protocol
  hostname = request.headers.protocol + '//' + hostname + portSpec

  LSRoot = new Firebase(baseDataApiUrl)
  genericError =
    responseStatus: response.status(500)
  LSRoot
    .child('users')
    .child(username)
      .child('stamps')
      .child('error')
      .push(genericError)
  return response.redirect(hostname + '/')


_local = (request, response) ->
  ###
  @namespace stamp/:username/local
  @description

  For Control Stamp creations.

  ###

  hostname = request.headers.host
  portSpec = if request.headers.port != "" then (':' + request.headers.port) else ''
  protocol = request.headers.protocol
  hostname = 'http:' + '//' + hostname

  requestBody = request.body
  stampBuf = new Buffer(requestBody.data, 'base64')
  username = request.params.username
  Users = new Firebase("#{baseDataApiUrl}users")

  Users
    .child(username)
    .child('stamps')
    .child('local')
    .push(stampBuf)

  sourceIp = '0.0.0.0'
  sourceMsg = JSON.stringify(stampBuf)

  logglyClient.log("#{sourceIp} - #{sourceMsg}", [ 'stampLocalize' ], (err, result) ->
  )

  response.send(true)


module.exports =
  error : _error
  local : _local
