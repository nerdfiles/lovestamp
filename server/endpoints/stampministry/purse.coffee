changetipHandler = require('../../social/changeTip')
# Load static credentials
_credentials = require('../../config/credentials')
credentials = _credentials.credentials()

Firebase = require('firebase')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 

_purseUpdate = (req, res) ->

    requestBody = req.body
    handle = requestBody.handle
    createTipUrl = requestBody.createTipUrl
    vendor = requestBody.vendorId
    host = req.headers.host

    merchant = null
    _localControlStamp = null
    LSRoot = new Firebase(baseDataApiUrl)

    u = LSRoot.child('users').child(handle)
    currentProfile = u.child('profile')
    currentPurse = currentProfile.child('purse')

    domain = host.replace(/\./g, '-')

    changetipHandler(merchant, handle, _localControlStamp, credentials, createTipUrl, domain).then((responseObject) ->

      if responseObject.tipUrl != null
        currentPurse.set 0
        res.send responseObject
      else
        res.send responseObject

    )


module.exports =
  update_purse: _purseUpdate
