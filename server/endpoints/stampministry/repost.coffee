###
@endpoints stampministry stamp stamps
###

Firebase = require('firebase')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 
changetipHandler = require('../../social/changeTip')
twitterHandler = require('../../social/twitter')
_ = require('lodash')
# Load static credentials
_credentials = require('../../config/credentials')
credentials = _credentials.credentials()

logglyClient = require('../../utils/loggly')

try
    Q = require('q')
catch e
    Q = require('Q')


moment = require('moment')

repost = (req, res, next) ->
  ###
  @description

  A simple end-point for reposting to social networks under failover conditions.

  @return {Response:boolean} true
  ###

  requestBody = req.body
  merchant = requestBody.merchant
  username = requestBody.username
  createTipUrl = requestBody.createTipUrl
  receipt = requestBody.receipt
  host = req.headers.host
  def = Q.defer()

  Users = new Firebase("#{baseDataApiUrl}users")

  localControlStamp = Users.
    child(username).
    child('stamps').
    child('success').
    child(merchant).
      push({
        snowshoe: receipt
      })

  provider = if username.charAt(0) == '@' then 'twitter' else if username.charAt(0) == 'ᶠ' then 'facebook' else 'google'
  console.log('test----------------------' + provider)

  foundStamp = false

  Users.child(merchant).child('stamps').child('local').once('value', (v, k) ->

    _v = v.val()

    _.forEach _v, (stamp, keyName) ->

      if receipt.stamp.serial == stamp.receipt.validation.stamp.serial and foundStamp == false
        foundStamp = true

        firstImpressionDate = moment().format("MM/DD/YYYY HH:mm:ss")

        statusSaturation = Users.child(merchant).child('stamps').child('local').child(keyName).child('statusSaturation')

        statusSaturation.once 'value', (_statusSaturation) ->
          if _statusSaturation.val() == 2 and _statusSaturation.val() != 3
            Users.child(merchant).child('stamps').child('local').child(keyName).child('firstImpressionDate').set firstImpressionDate.format("MM/DD/YYYY HH:mm:ss")
            statusSaturation.set 3

        _localControlStamp = keyName

        domain = host.replace(/\./g, '-')

        changetipHandler(merchant, username, _localControlStamp, credentials, createTipUrl, domain).then((responseObject) ->
          console.log 'responseObject'
          console.dir responseObject

          # Could not make impression because stamp not found or in range or some other reason.
          if responseObject.status and responseObject.status == 'error'
            def.reject responseObject
            return

          if responseObject.tipUrl == null
            def.reject {
              status: 'error'
            }
            return

          messageConstruct = responseObject.messageConstruct
          responseObject.stamp = receipt.stamp

          if createTipUrl == false
            try
              twitterHandler.postToTwitter(credentials, merchant, username, logglyClient, messageConstruct, _localControlStamp)
            catch e
              console.log e

          respostConstruct = responseObject
          def.resolve(respostConstruct)

        , (error) ->
          def.reject { status: 'error' }
        )

  )

  def.promise.then((repostConstruct) ->

    res.send repostConstruct
  , (errorData) ->
    res.send errorData
  ).done()

  return


module.exports =
  response: repost
