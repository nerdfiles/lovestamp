
/*

                      _                 
                     | |                
     ____  _   _  ___| |__  _____  ____ 
    |  _ \| | | |/___)  _ \| ___ |/ ___)
    | |_| | |_| |___ | | | | ____| |    
    |  __/|____/(___/|_| |_|_____)_|    
    |_|
 */


/*
@namespace near-here
@description

Basically, clients cannot sort 'near here' data. Our backend will need to sort 
some of this data so that the client is not burdened (geoscopic scale). Principally 
the UI should analogize "infinite scroll" DOM W3C event model to on-demand loading of 
parameterized geoscopic metadata queries. "zoomend" might align with scroll direction 
downward, and so on. Very simple.
 */

(function() {
  var Pusher, pusherChannel, _channel, _near_here;

  Pusher = require('pusher');

  pusherChannel = new Pusher({
    appId: '119724',
    key: '5f8d32296b6e593ef1bc',
    secret: '6910b5ed2a8ec0a72c59'
  });

  _channel = function(req, res) {

    /*
    @endpoint /near-here/channel
    @inner
     */
    pusherChannel.trigger('near_here', 'merchant_stamp_nearby', {
      "class": "stamp--leaflet--active",
      "": ""
    });
  };

  _near_here = function(req, res) {
    var webhook;
    webhook = pusher.webhook(req);
    console.log("data:", webhook.getData());
    console.log("events:", webhook.getEvents());
    console.log("time:", webhook.getTime());
    console.log("valid:", webhook.isValid());
    return res.send(true);
  };

  module.exports = {
    channel: _channel,
    near_here: _near_here
  };

}).call(this);
