Firebase = require('firebase')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/'


module.exports = (req, res) ->
  requestBody = req.body
  username = '✚'+requestBody.userId
  hasToken = requestBody.hasToken
  UsersRoot = new Firebase(baseDataApiUrl + 'users')
  UsersRoot
    .child(username)
    .child('hasToken')
    .set hasToken
