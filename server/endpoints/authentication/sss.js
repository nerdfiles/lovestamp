(function() {
  var APPKEY, APPSECRET, SnowShoeStamp, auth, fs, readAPPKEY, readAPPSECRET, trim;

  fs = require('fs');

  trim = require('trim');

  SnowShoeStamp = require('snowshoestamp');

  readAPPKEY = fs.readFileSync(__dirname + '/../../snowshoestamp.key', 'utf8');

  readAPPSECRET = fs.readFileSync(__dirname + '/../../snowshoestamp.secret', 'utf8');

  APPKEY = trim(readAPPKEY);

  APPSECRET = trim(readAPPSECRET);

  auth = new SnowShoeStamp(APPKEY, APPSECRET);

  module.exports = {
    auth: auth
  };

}).call(this);
