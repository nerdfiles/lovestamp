(function() {
  var Firebase, baseDataApiUrl;

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  module.exports = function(req, res) {
    var UsersRoot, hasToken, requestBody, username;
    requestBody = req.body;
    username = '✚' + requestBody.userId;
    hasToken = requestBody.hasToken;
    UsersRoot = new Firebase(baseDataApiUrl + 'users');
    return UsersRoot.child(username).child('hasToken').set(hasToken);
  };

}).call(this);
