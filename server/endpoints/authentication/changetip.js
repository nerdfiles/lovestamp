(function() {
  var Firebase, OAuth2, baseDataApiUrl, credentials, oauth, refresh_token_cron, requests_slack, scope_list, slack, __refresh_token, _access_token, _authentication, _credentials, _scope_list;

  OAuth2 = require("oauth").OAuth2;

  _credentials = require('../../config/credentials');

  credentials = _credentials.credentials();

  slack = require('../../utils/slack');

  requests_slack = slack.requests_slack;

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  scope_list = ['read_user_basic', 'read_user_full', 'create_tip_urls', 'read_and_create_withdrawals', 'read_my_tips_on_channel', 'send_my_tips_on_channel', 'read_my_tips_all_channels', 'send_my_tips_all_channels'];

  _scope_list = scope_list.join(' ');

  oauth = new OAuth2(credentials.changeTip.oauth2.clientID, credentials.changeTip.oauth2.clientSecret, credentials.changeTip.oauth2.site, credentials.changeTip.oauth2.authorizationPath, credentials.changeTip.oauth2.tokenPath);

  _authentication = function(req, res) {

    /*
    @description
    
    Generate getAuthorizeUrl.
     */
    var authUrl, host, protocol;
    host = req.headers.host;
    protocol = req.headers.protocol;
    authUrl = oauth.getAuthorizeUrl({
      response_type: 'code',
      redirect_uri: credentials.changeTip.oauth2.redirect_uri(protocol, host),
      scope: _scope_list
    });
    console.log('Generated Authorization URL...');
    return res.redirect(authUrl);
  };

  _access_token = function(req, res) {

    /*
    @description
    
    Callback for getAuthorizeUrl.
     */
    var access_token, domain, host, protocol, refresh_token, requestQueryCode;
    host = req.headers.host;
    protocol = req.headers.protocol;
    domain = host.replace(/\./g, '-');
    access_token = new Firebase("" + baseDataApiUrl + "app/auth/" + domain + "/access_token");
    refresh_token = new Firebase("" + baseDataApiUrl + "app/auth/" + domain + "/refresh_token");
    requestQueryCode = req.query.code;
    console.log('Requesting Authorization Access Token...');
    return oauth.getOAuthAccessToken(requestQueryCode, {
      grant_type: 'authorization_code',
      redirect_uri: credentials.changeTip.oauth2.redirect_uri(protocol, host)
    }, function(err, _access_token, _refresh_token) {
      var _protocol;
      _protocol = void 0;
      if (err) {
        console.log(err);
        res.end('error: ' + JSON.stringify(err));
      } else {
        access_token.set(_access_token);
        credentials.changeTip._load_access_token(_access_token);
        refresh_token.set(_refresh_token);
        credentials.changeTip._load_refresh_token(_refresh_token);
        if (host.indexOf('local') !== -1) {
          _protocol = 'http';
        } else {
          _protocol = 'https';
        }
        return res.redirect(("" + _protocol + "://") + host + "/merchant/login");
      }
    });
  };

  __refresh_token = function() {

    /*
    @description
    
    Refresh tokenization.
     */
    var access_token, domain, refresh_token;
    domain = 'lovestamp-io';
    access_token = new Firebase("" + baseDataApiUrl + "app/auth/" + domain + "/access_token");
    refresh_token = new Firebase("" + baseDataApiUrl + "app/auth/" + domain + "/refresh_token");
    console.log('Refreshing Access Token...');
    credentials.changeTip.load_refresh_token(domain).then(function(rf) {
      oauth.getOAuthAccessToken(rf, {
        grant_type: 'refresh_token'
      }, function(err, _access_token, _refresh_token) {
        if (err) {
          console.log(err);
          requests_slack.send({
            text: 'ChangeTip automatic Refresh Token attempt has failed. Try to manually reset the token: https://lovestamp.io/changetip/oauth2/authentication',
            channel: '#issue-log',
            username: 'LoveBot'
          });
        } else {
          access_token.set(_access_token);
          credentials.changeTip._load_access_token(_access_token);
          refresh_token.set(_refresh_token);
          credentials.changeTip._load_refresh_token(_refresh_token);
          requests_slack.send({
            text: 'ChangeTip automatic Refresh Token attempt was successful. You can also manually reset the token: https://lovestamp.io/changetip/oauth2/authentication',
            channel: '#engineering',
            username: 'LoveBot'
          });
        }
      });
    });
  };

  refresh_token_cron = function() {

    /*
    @description
    
    Refresh cron.
     */
    var CronJob, job, jobStatus;
    CronJob = require('cron').CronJob;
    job = new CronJob({
      cronTime: '30 3 * * *',
      onTick: function() {
        return __refresh_token();
      },
      start: false,
      timeZone: "America/Chicago"
    });
    return jobStatus = job.start();
  };

  refresh_token_cron();

  module.exports = {
    authorization_code: _authentication,
    access_token: _access_token,
    refresh_token: __refresh_token
  };

}).call(this);
