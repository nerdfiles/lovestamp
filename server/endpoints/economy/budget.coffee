Firebase = require('firebase')
aggregateStampImpression = require('../../utils/aggregateStampImpression')


budgetReset = (newBudget, newRevenue) ->
  ###
  @module utils
  @namespace budgetReset
  @usage

    > budgetSet(1000000)
    > budgetSet 1000000, 50

  @param {number} newBudget A new budget to reset in the LoveStamp's balances sheet.
  @param {number} newRevenue A new revenue to reset in the LoveStamp's balances sheet.
  @return {response} A sensible object that tells the controller nice JSON-y things.
  ###

  newBudget = if not newBudget then 0 else parseInt(newBudget, 10)
  newRevenue = if not newRevenue then 0 else parseInt(newRevenue, 10)
  balancesRoot = new Firebase('https://lovestamp.firebaseio.com/balances')

  try
    payoutShadowBudgetSource = balancesRoot.child('payoutShadowBudget')
    payoutShadowBudgetSource.set newBudget
  catch e
    return false

  return true


rollUp = (req, res) ->
  ###
  @namespace budget/roll-up
  @return {res:Boolean}
  ###

  if aggregateStampImpression.budgetCron()
    aggregateStampImpression.budgetCronStart()
    res.send true
  else
    res.send false


# Run once, and start cron on success, and allow for subsequent requests via 
# API.
budgetCronStart = () ->
  aggregateStampImpression.budgetCronStart()


reset = (req, res) ->
  ###
  @namespace budget/reset/:newBudget/:newRevenue
  @description

    Budget resetter.

  ###

  newBudget = req.params.newBudget
  newRevenue = req.params.newRevenue
  if budgetReset( newBudget, newRevenue )
    res.send true
  else
    res.send false


module.exports =
  reset  : reset
  rollUp : rollUp
  budgetCronStart: budgetCronStart
