(function() {
  var topUser, topUserUtil;

  topUserUtil = require('../../utils/topUser');

  topUserUtil.topUserCronStart();

  topUser = function(request, response) {

    /* Manually check and set the top user via REST-ful endpoint. */
    return topUserUtil.topUser();
  };

  module.exports = {
    generateTopUser: topUser
  };

}).call(this);
