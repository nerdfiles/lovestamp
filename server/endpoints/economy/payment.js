(function() {
  var Firebase, Q, STRIPEPUBLISHABLE, STRIPESECRET, baseDataApiUrl, e, fs, impressionsUpdate, livemode, loadHtmlFile, mailer, moment, payments_btc, payments_slack, payments_usd, readStripePublishable, readStripeSecret, slack, stripe, transport, trim;

  fs = require('fs');

  trim = require('trim');

  livemode = false;

  try {
    Q = require('q');
  } catch (_error) {
    e = _error;
    Q = require('Q');
  }

  if (livemode) {
    readStripeSecret = fs.readFileSync(__dirname + '/../../stripe.live.secret', 'utf8');
    readStripePublishable = fs.readFileSync(__dirname + '/../../stripe.live.publishable', 'utf8');
  } else {
    readStripeSecret = fs.readFileSync(__dirname + '/../../stripe.test.secret', 'utf8');
    readStripePublishable = fs.readFileSync(__dirname + '/../../stripe.test.publishable', 'utf8');
  }

  STRIPESECRET = trim(readStripeSecret);

  STRIPEPUBLISHABLE = trim(readStripePublishable);

  stripe = require("stripe")(STRIPESECRET);

  mailer = require('../mailer/base');

  transport = mailer.mailerConstruct();

  slack = require('../../utils/slack');

  payments_slack = slack.payments_slack;

  loadHtmlFile = require('../../utils/load-html-file');

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  moment = require('moment');

  impressionsUpdate = function(username, qty) {

    /*
    @description
    
    Used in payment/:username/:type. Updates impressions count or latest 
    date for a particular merchant.
    
    @tood The data structure is a list, though it shouldn't be.
     */
    var LSRoot, def, impressionsBase, now, quantity, usersRoot;
    quantity = parseInt(qty, 10);
    def = Q.defer();
    LSRoot = new Firebase(baseDataApiUrl);
    usersRoot = LSRoot.child('users');
    now = moment();
    impressionsBase = usersRoot.child(username).child('impressions');
    try {
      impressionsBase.once('value', function(snapshot) {
        _.forEach(snapshot.val(), function(i, key) {
          var current_expiry, __current_expiry, __old_expiry, _current_expiry, _old_expiry, _quantity;
          _quantity = parseInt(i.quantity, 10);
          _old_expiry = new Date(i.expiry);
          __old_expiry = moment(_old_expiry);
          current_expiry = new Date();
          _current_expiry = moment(current_expiry);
          __current_expiry = _current_expiry.add(qty / 10, 'days');
          if (__current_expiry.isAfter(__old_expiry)) {
            return impressionsBase.child(key).set({
              'quantity': quantity + _quantity,
              'expiry': __current_expiry.toString()
            });
          } else {
            return impressionsBase.child(key).set({
              'quantity': quantity + _quantity,
              'expiry': __old_expiry.toString()
            });
          }
        });
        return def.resolve(snapshot);
      }, function(error) {
        console.log(error);
        return def.reject(error);
      });
    } catch (_error) {
      e = _error;
      console.log(error);
      def.reject(e);
    }
    return def.promise;
  };

  payments_btc = function(request, response) {

    /**
    @module utils
    @description
    
      An instance of nodemailer's SMTP Transport object[0].
    
      —
      [0]: https://github.com/andris9/nodemailer-smtp-transport
     */
    var PurchaserProfile, Users, UsersRoot, amount, bitcoinAddy, currency, customerSpecification, description, email, handle, hostname, messageHtml, messageText, packagedCharge, paymentSpecification, portSpec, prefix, protocol, quantity, rememberMe, requestBody, response_id, type, username, _multiplier, _type;
    Users = new Firebase("" + baseDataApiUrl + "users");
    transport = nodemailer.createTransport(smtpTransport({
      host: 'smtp.lovestamp.io',
      port: 587,
      debug: true,
      secure: false,
      secureConnection: false,
      ignoreTLS: false,
      tls: {
        rejectUnauthorized: false
      },
      auth: {
        user: 'aaron',
        pass: 'cCNkd0BpKQ3UHg=='
      }
    }));
    username = request.params.username;
    type = request.params.type;
    requestBody = request.body;
    _type = requestBody.metadata.type;
    _multiplier = _type === '1' ? 50 : 30;
    currency = requestBody.currency;
    response_id = requestBody.response_id;
    quantity = requestBody.quantity;
    description = requestBody.description;
    email = requestBody.email;
    bitcoinAddy = requestBody.bitcoinAddy;
    rememberMe = requestBody.rememberMe;
    hostname = request.headers.host;
    portSpec = request.headers.port !== "" ? ':' + request.headers.port : '';
    protocol = request.headers.protocol;
    hostname = 'http:' + '//' + hostname;
    amount = quantity * _multiplier;
    messageText = "Sound the alarms!";
    messageText += "\n\nLoveStamp user, <a href='" + hostname + "/users/" + username + "'>" + username + "</a>, has made a purchase of " + quantity + " " + type + ".";
    if (type === 'stamps') {
      messageText += "\n\nPlease review available stamps in our <a href='" + hostname + "/admin'>Admin Command Center</a>.";
    }
    messageHtml = "<p>Sound the alarms!</p>";
    messageHtml += "<p>LoveStamp user, <a href='" + hostname + "/users/" + username + "'>" + username + "</a>, has made a purchase of " + quantity + " " + type + ".</p>";
    if (type === 'stamps') {
      messageHtml += "<p>Please review available stamps in our <a href='" + hostname + "/admin'>Admin Command Center</a>.</p>";
    }
    prefix = username.charAt(0);
    handle = urlPrefix[prefix] + username;
    paymentSpecification = {
      amount: amount,
      currency: currency,
      description: quantity + ' ' + type + " for " + username
    };
    customerSpecification = {
      source: response_id
    };
    UsersRoot = new Firebase(baseDataApiUrl + 'users');
    PurchaserProfile = UsersRoot.child(username);
    if (!rememberMe) {
      packagedCharge = stripe.charges.create(paymentSpecification, function(err, charge) {
        var ImpressionsBase, PaymentsRoot, activeStatus, impressioned;
        transport.sendMail({
          from: 'webmaster@lovestamp.io',
          to: 'support@lovestamp.io',
          subject: 'A LoveStamp user has made a purchase of ' + amount + ' ' + currency.toUpperCase(),
          text: messageText,
          html: messageHtml
        });
        PurchaserProfile.once('value', function(profileData) {
          var htmlEmailTemplateFilePath, purchaserEmail, purchaserMessageText;
          purchaserEmail = profileData.purchaseEmail;
          purchaserMessageText = "Thanks " + username + " for your purchase of " + quantity + " " + type + " for " + amount + ".";
          htmlEmailTemplateFilePath = '../email/newsletter/purchase.inline.html';
          return loadHtmlFile(htmlEmailTemplateFilePath).then(function(htmlEmailConstruct) {
            return transport.sendMail({
              from: 'webmaster@lovestamp.io',
              to: purchaserEmail,
              subject: username + ', you have made a purchase of ' + amount + ' ' + currency.toUpperCase(),
              text: purchaserMessageText,
              html: htmlEmailConstruct
            });
          });
        });
        payments_slack.send({
          text: "" + handle + " has made a purchase of " + quantity + " " + type + ".",
          channel: '#payments',
          username: 'LoveBot'
        });
        PaymentsRoot = new Firebase(baseDataApiUrl + 'payments');
        PaymentsRoot.child(username).child(type).push(charge);
        Users = new Firebase("" + baseDataApiUrl + "users");
        UsersRoot = Users;
        activeStatus = UsersRoot.child(username).child('active');
        ImpressionsBase = UsersRoot.child(username).child('impressions');
        if (type === 'stamps') {
          activeStatus.set(true);
        } else {
          impressioned = impressionsUpdate(username, quantity);
          impressioned.then(function(impression) {
            var d, impressionsSnapshot, __d, _d, _e;
            if (impression.val() === null) {
              d = new Date();
              e = new Date();
              _d = moment(d);
              _e = moment(e);
              __d = _d.add(quantity / 10, 'days');
              impressionsSnapshot = {
                quantity: quantity,
                expiry: __d.toString()
              };
              return ImpressionsBase.push(impressionsSnapshot);
            }
          });
        }
        return response.send(true);
      });
      return;
    }
    stripe.customers.create(customerSpecification).then(function(customer) {
      paymentSpecification.customer = customer.id;
      return packagedCharge = stripe.charges.create(paymentSpecification, function(err, charge) {
        var ImpressionsBase, PaymentsRoot, activeStatus, impressioned;
        transport.sendMail({
          from: 'webmaster@lovestamp.io',
          to: 'support@lovestamp.io',
          subject: 'A LoveStamp user has made a purchase of ' + amount + ' ' + currency.toUpperCase(),
          text: messageText,
          html: messageHtml
        });
        PurchaserProfile.once('value', function(profileData) {
          var htmlEmailTemplateFilePath, purchaserEmail, purchaserMessageText;
          purchaserEmail = profileData.purchaseEmail;
          purchaserMessageText = "Thanks " + username + " for your purchase of " + quantity + " " + type + " for " + amount + ".";
          htmlEmailTemplateFilePath = '../email/newsletter/purchase.inline.html';
          return loadHtmlFile(htmlEmailTemplateFilePath).then(function(htmlEmailConstruct) {
            return transport.sendMail({
              from: 'webmaster@lovestamp.io',
              to: purchaserEmail,
              subject: username + ', you have made a purchase of ' + amount + ' ' + currency.toUpperCase(),
              text: purchaserMessageText,
              html: htmlEmailConstruct
            });
          });
        });
        payments_slack.send({
          text: "" + handle + " has made a purchase of " + quantity + " " + type + ".",
          channel: '#payments',
          username: 'LoveBot'
        });
        PaymentsRoot = new Firebase(baseDataApiUrl + 'payments');
        PaymentsRoot.child(username).child(type).push(charge);
        Users = new Firebase("" + baseDataApiUrl + "users");
        UsersRoot = Users;
        activeStatus = UsersRoot.child(username).child('active');
        ImpressionsBase = UsersRoot.child(username).child('impressions');
        if (type === 'stamps') {
          activeStatus.set(true);
        } else {
          impressioned = impressionsUpdate(username, quantity);
          impressioned.then(function(impression) {
            var d, impressionsSnapshot, __d, _d, _e;
            if (impression.val() === null) {
              d = new Date();
              e = new Date();
              _d = moment(d);
              _e = moment(e);
              __d = _d.add(quantity / 10, 'days');
              impressionsSnapshot = {
                quantity: quantity,
                expiry: __d.toString()
              };
              return ImpressionsBase.push(impressionsSnapshot);
            }
          });
        }
        return response.send(true);
      });
    });
  };

  payments_usd = function(request, response) {

    /*
    @see https://stripe.com/docs/api/node#create_charge
    @namespace payment/:username/:type
    @description
    
    Payment controllers speak to Stripe via Stripe.js data requests, and they 
    dish back receipts depends on instance of nodemailer's SMTP Transport 
    object[0].
    
    —
    [0]: https://github.com/andris9/nodemailer-smtp-transport
     */
    var PurchaserProfile, UsersRoot, amount, bitcoinAddy, currency, email, handle, hostname, messageHtml, messageText, packagedCharge, paymentSpecification, portSpec, prefix, protocol, purchaserMessageHtml, purchaserMessageText, quantity, rememberMe, requestBody, token, type, username, _multiplier, _type;
    transport = nodemailer.createTransport(smtpTransport({
      host: 'smtp.lovestamp.io',
      port: 587,
      debug: true,
      secure: false,
      secureConnection: false,
      ignoreTLS: false,
      tls: {
        rejectUnauthorized: false
      },
      auth: {
        user: 'aaron',
        pass: 'cCNkd0BpKQ3UHg=='
      }
    }));
    username = request.params.username;
    type = request.params.type;
    requestBody = request.body;
    currency = requestBody.currency;
    token = requestBody.token;
    quantity = requestBody.quantity;
    email = requestBody.email;
    bitcoinAddy = requestBody.bitcoinAddy;
    rememberMe = requestBody.rememberMe;
    _type = requestBody.metadata.type;
    _multiplier = _type === '1' ? 50 : 30;
    hostname = request.headers.host;
    portSpec = request.headers.port !== "" ? ':' + request.headers.port : '';
    protocol = request.headers.protocol;
    hostname = 'http:' + '//' + hostname;
    UsersRoot = new Firebase(baseDataApiUrl + 'users');
    PurchaserProfile = UsersRoot.child(username);
    purchaserMessageText = "Sound the alarms!";
    purchaserMessageText += "Thanks " + username + " for your purchase of " + quantity + " " + type + ".";
    purchaserMessageHtml = "<p>Sound the alarms!</p>";
    purchaserMessageHtml += "<p>Thanks " + username + " for your purchase of " + quantity + " " + type + ".</p>";
    amount = quantity * _multiplier;
    paymentSpecification = {
      amount: amount,
      currency: currency,
      card: token,
      description: quantity + ' ' + type + " for " + username
    };
    messageText = "Sound the alarms!";
    messageText += "\n\nLoveStamp user, <a href='" + hostname + "/users/" + username + "'>" + username + "</a>, has made a purchase of " + quantity + " " + type + ".";
    if (type === 'stamps') {
      messageText += "\n\nPlease review available stamps in our <a href='" + hostname + "/admin'>Admin Command Center</a>.";
    }
    messageHtml = "<p>Sound the alarms!</p>";
    messageHtml += "<p>LoveStamp user, <a href='" + hostname + "/users/" + username + "'>" + username + "</a>, has made a purchase of " + quantity + " " + type + ".</p>";
    if (type === 'stamps') {
      messageHtml += "<p>Please review available stamps in our <a href='" + hostname + "/admin'>Admin Command Center</a>.</p>";
    }
    transport.sendMail({
      from: 'webmaster@lovestamp.io',
      to: 'support@lovestamp.io',
      subject: 'A LoveStamp user has made a purchase of ' + amount + ' ' + currency.toUpperCase(),
      text: messageText,
      html: messageHtml
    });
    PurchaserProfile.once('value', function(profileData) {
      var htmlEmailTemplateFilePath, purchaserEmail;
      purchaserEmail = profileData.purchaseEmail;
      purchaserMessageText = "Thanks " + username + " for your purchase of " + quantity + " " + type + " for " + amount + ".";
      htmlEmailTemplateFilePath = __dirname + '/../../../email/newsletter/purchase.inline.html';
      return loadHtmlFile(htmlEmailTemplateFilePath).then(function(htmlEmailConstruct) {
        return transport.sendMail({
          from: 'webmaster@lovestamp.io',
          to: purchaserEmail,
          subject: username + ', you have made a purchase of ' + amount + ' ' + currency.toUpperCase(),
          text: purchaserMessageText,
          html: htmlEmailConstruct
        });
      });
    });
    prefix = username.charAt(0);
    handle = urlPrefix[prefix] + username;
    payments_slack.send({
      text: "" + handle + " has made a purchase of " + quantity + " " + type + ".",
      channel: '#payments',
      username: 'LoveBot'
    });
    if (!rememberMe) {
      packagedCharge = stripe.charges.create(paymentSpecification, function(err, charge) {
        var ImpressionsBase, PaymentsRoot, Users, activeStatus, impressioned;
        PaymentsRoot = new Firebase(baseDataApiUrl + 'payments');
        PaymentsRoot.child(username).child(type).push(charge);
        Users = new Firebase("" + baseDataApiUrl + "users");
        UsersRoot = Users;
        activeStatus = UsersRoot.child(username).child('active');
        ImpressionsBase = UsersRoot.child(username).child('impressions');
        if (type === 'stamps') {
          activeStatus.set(true);
        } else {
          impressioned = impressionsUpdate(username, quantity);
          impressioned.then(function(impression) {
            var d, impressionsSnapshot, __d, _d, _e;
            if (impression.val() === null) {
              d = new Date();
              e = new Date();
              _d = moment(d);
              _e = moment(e);
              __d = _d.add(quantity / 10, 'days');
              impressionsSnapshot = {
                quantity: quantity,
                expiry: __d.toString()
              };
              return ImpressionsBase.push(impressionsSnapshot);
            }
          });
        }
        return response.send(true);
      });
      return;
    }
    stripe.customers.create({
      source: token,
      description: ("" + email) + (quantity + ' ' + type + " for " + username)
    }).then(function(customer) {
      paymentSpecification.customer = customer.id;
      return packagedCharge = stripe.charges.create(paymentSpecification, function(err, charge) {
        var ImpressionsBase, PaymentsRoot, Users, activeStatus, impressioned;
        PaymentsRoot = new Firebase(baseDataApiUrl + 'payments');
        PaymentsRoot.child(username).child(type).push(charge);
        Users = new Firebase("" + baseDataApiUrl + "users");
        UsersRoot = Users;
        activeStatus = UsersRoot.child(username).child('active');
        ImpressionsBase = UsersRoot.child(username).child('impressions');
        if (type === 'stamps') {
          activeStatus.set(true);
        } else {
          impressioned = impressionsUpdate(username, quantity);
          impressioned.then(function(impression) {
            var d, impressionsSnapshot, __d, _d, _e;
            if (impression.val() === null) {
              d = new Date();
              e = new Date();
              _d = moment(d);
              _e = moment(e);
              __d = _d.add(quantity / 10, 'days');
              impressionsSnapshot = {
                quantity: quantity,
                expiry: __d.toString()
              };
              return ImpressionsBase.push(impressionsSnapshot);
            }
          });
        }
        return response.send(true);
      });
    });
  };

  module.exports = {
    payments_btc: payments_btc,
    payments_usd: payments_usd
  };

}).call(this);
