
topUserUtil = require('../../utils/topUser')

topUserUtil.topUserCronStart()

topUser = (request, response) ->
  ### Manually check and set the top user via REST-ful endpoint. ###
  topUserUtil.topUser()

module.exports =
  generateTopUser: topUser
