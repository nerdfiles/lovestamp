fs = require('fs')
trim = require('trim')
livemode = false

try
    Q = require('q')
catch e
    Q = require('Q')

if livemode
  readStripeSecret = fs.readFileSync(__dirname + '/../../stripe.live.secret', 'utf8')
  readStripePublishable = fs.readFileSync(__dirname + '/../../stripe.live.publishable', 'utf8')
else
  readStripeSecret = fs.readFileSync(__dirname + '/../../stripe.test.secret', 'utf8')
  readStripePublishable = fs.readFileSync(__dirname + '/../../stripe.test.publishable', 'utf8')

STRIPESECRET = trim(readStripeSecret)
STRIPEPUBLISHABLE = trim(readStripePublishable)
stripe = require("stripe")(
  STRIPESECRET
)

mailer = require('../mailer/base')
transport = mailer.mailerConstruct()
slack = require('../../utils/slack')
payments_slack = slack.payments_slack
loadHtmlFile = require('../../utils/load-html-file')
Firebase = require('firebase')
baseDataApiUrl = 'https://lovestamp.firebaseio.com/' 
moment = require('moment')

impressionsUpdate = (username, qty) ->
  ###
  @description

  Used in payment/:username/:type. Updates impressions count or latest 
  date for a particular merchant.

  @tood The data structure is a list, though it shouldn't be.
  ###

  quantity = parseInt qty, 10
  def = Q.defer()
  LSRoot = new Firebase(baseDataApiUrl)
  usersRoot = LSRoot.child('users')
  now = moment() # @note Should be timestamp of Stripe receipt, formatted with moment.
  impressionsBase = usersRoot.child(username).child('impressions')
  try
    impressionsBase.once 'value', (snapshot) ->
      _.forEach snapshot.val(), (i, key) ->
        _quantity = parseInt(i.quantity, 10)
        _old_expiry = new Date(i.expiry)
        __old_expiry = moment(_old_expiry)
        current_expiry = new Date()
        _current_expiry = moment(current_expiry)
        __current_expiry = _current_expiry.add((qty/10), 'days')

        if __current_expiry.isAfter(__old_expiry)
          impressionsBase.child(key).set {
            'quantity': (quantity + _quantity)
            'expiry': __current_expiry.toString()
          }
        else
          impressionsBase.child(key).set {
            'quantity': (quantity + _quantity)
            'expiry': __old_expiry.toString()
          }

      def.resolve snapshot
    , (error) ->
      console.log error
      def.reject error
  catch e
    console.log error
    def.reject e
  return def.promise



payments_btc = (request, response) ->
  ###*
  @module utils
  @description

    An instance of nodemailer's SMTP Transport object[0].

    —
    [0]: https://github.com/andris9/nodemailer-smtp-transport

  ###

  Users = new Firebase("#{baseDataApiUrl}users")
  transport = nodemailer.createTransport(smtpTransport({
    host: 'smtp.lovestamp.io'
    port: 587
    debug: true
    secure: false
    secureConnection: false
    ignoreTLS: false
    tls:
      rejectUnauthorized:false
      #ciphers:'SSLv3'
    auth:
      user: 'aaron'
      pass: 'cCNkd0BpKQ3UHg=='
  }))

  username = request.params.username
  type = request.params.type

  requestBody = request.body

  _type       = requestBody.metadata.type
  _multiplier = if _type is '1' then 50 else 30
  currency    = requestBody.currency
  response_id = requestBody.response_id
  quantity    = requestBody.quantity
  description = requestBody.description
  email       = requestBody.email
  bitcoinAddy = requestBody.bitcoinAddy
  rememberMe  = requestBody.rememberMe

  hostname = request.headers.host
  portSpec = if request.headers.port != "" then (':' + request.headers.port) else ''
  protocol = request.headers.protocol
  hostname = 'http:' + '//' + hostname

  #if type == 'stamps'
    ## Increments of 1
    #amount = quantity * 50
  #else
    ## Increments of 300
    #amount = (quantity / 300) * 30

  amount = quantity * _multiplier

  messageText = "Sound the alarms!"
  messageText += "\n\nLoveStamp user, <a href='#{hostname}/users/#{username}'>#{username}</a>, has made a purchase of #{quantity} #{type}."
  if type == 'stamps'
    messageText += "\n\nPlease review available stamps in our <a href='#{hostname}/admin'>Admin Command Center</a>."

  messageHtml = "<p>Sound the alarms!</p>"
  messageHtml += "<p>LoveStamp user, <a href='#{hostname}/users/#{username}'>#{username}</a>, has made a purchase of #{quantity} #{type}.</p>"
  if type == 'stamps'
    messageHtml += "<p>Please review available stamps in our <a href='#{hostname}/admin'>Admin Command Center</a>.</p>"

  prefix = username.charAt(0)

  handle = urlPrefix[prefix] + username

  #receiver = stripe.bitcoinReceivers.create({
    #amount   : amount
    #currency : "usd"
    #email    : email
  #})

  paymentSpecification =
    amount      : amount,
    currency    : currency,
    description : (quantity + ' ' + type + " for " + username)

  customerSpecification =
    source      : response_id

  UsersRoot = new Firebase(baseDataApiUrl + 'users')
  PurchaserProfile = UsersRoot.child(username)

  if ! rememberMe

    packagedCharge = stripe.charges.create(paymentSpecification, (err, charge) ->

      transport.sendMail(
        from    : 'webmaster@lovestamp.io'
        to      : 'support@lovestamp.io'
        subject : 'A LoveStamp user has made a purchase of ' + amount + ' ' + currency.toUpperCase()
        text    : messageText
        html    : messageHtml
      )

      PurchaserProfile.once 'value', (profileData) ->
        purchaserEmail = profileData.purchaseEmail

        purchaserMessageText = "Thanks #{username} for your purchase of #{quantity} #{type} for #{amount}."

        #purchaserMessageHtml = "<p>Thanks #{username} for your purchase of #{quantity} #{type}.</p>"

        htmlEmailTemplateFilePath = '../email/newsletter/purchase.inline.html'

        loadHtmlFile(htmlEmailTemplateFilePath).then (htmlEmailConstruct) ->

          transport.sendMail(
            from    : 'webmaster@lovestamp.io'
            to      : purchaserEmail
            subject : username + ', you have made a purchase of ' + amount + ' ' + currency.toUpperCase()
            text    : purchaserMessageText
            html    : htmlEmailConstruct
          )

      payments_slack.send(
          text     : "#{handle} has made a purchase of #{quantity} #{type}."
          channel  : '#payments'
          username : 'LoveBot'
      )

      PaymentsRoot = new Firebase(baseDataApiUrl + 'payments')
      PaymentsRoot
        .child(username)
          .child(type)
            .push(charge)

      Users = new Firebase("#{baseDataApiUrl}users")
      UsersRoot = Users
      activeStatus = UsersRoot.child(username).child('active')
      ImpressionsBase = UsersRoot.child(username).child('impressions')


      if type == 'stamps'
        activeStatus.set true
      else
        # Must cron job to check expiry date for Merchant dashboard (if expiry date has elapsed, set to 0)
        # Find difference of stamp impressions remaining
        # Normalize remaining stamp impression by currentBalance / totalStampImpressions (allotted to entire population)
        #
        # 1. At budget set, the current allowance becomes Actual and Revenue value is set to 0.
        # 
        # Budget Update after User Stamping or New Budget Total
        # 1. currentAllowance = Payout Budget + Revenue (used in rolling up remaining impressions)
        #    1.1. Amount available on ChangeTip
        # 2. payoutBudget = (1) - (3) (at first run, 2 is set to Current Allowance)
        # 3. revenueTotal = Profit
        # 4. budgetSetTotal = [$] (when set, update Current Allowance)
        #
        # Cron Layout
        # 0. Within timeframe of month.
        # 1. Cron should check ((2) / AggregateStampImpression) * RemainingStampImpressionsMerchant
        #    1.1. Add to (3).
        #    1.2. Reduce from (2).
        # 2. Set to RemainingStampImpressionsMerchant to 0 after check.

        impressioned = impressionsUpdate(username, quantity)
        impressioned.then (impression) ->
          if impression.val() == null
            d = new Date()
            e = new Date()
            _d = moment(d)
            _e = moment(e)
            __d = _d.add((quantity/10), 'days')
            impressionsSnapshot =
              quantity: quantity
              expiry: __d.toString()
            ImpressionsBase.push impressionsSnapshot

      response.send true
    )
    return

  stripe.customers.create(customerSpecification).then((customer) ->

    paymentSpecification.customer = customer.id

    packagedCharge = stripe.charges.create(paymentSpecification, (err, charge) ->

      transport.sendMail(
        from    : 'webmaster@lovestamp.io'
        to      : 'support@lovestamp.io'
        subject : 'A LoveStamp user has made a purchase of ' + amount + ' ' + currency.toUpperCase()
        text    : messageText
        html    : messageHtml
      )

      PurchaserProfile.once 'value', (profileData) ->
        purchaserEmail = profileData.purchaseEmail

        purchaserMessageText = "Thanks #{username} for your purchase of #{quantity} #{type} for #{amount}."

        #purchaserMessageHtml = "<p>Thanks #{username} for your purchase of #{quantity} #{type}.</p>"

        htmlEmailTemplateFilePath = '../email/newsletter/purchase.inline.html'

        loadHtmlFile(htmlEmailTemplateFilePath).then (htmlEmailConstruct) ->

          transport.sendMail(
            from    : 'webmaster@lovestamp.io'
            to      : purchaserEmail
            subject : username + ', you have made a purchase of ' + amount + ' ' + currency.toUpperCase()
            text    : purchaserMessageText
            html    : htmlEmailConstruct
          )

      payments_slack.send(
          text     : "#{handle} has made a purchase of #{quantity} #{type}."
          channel  : '#payments'
          username : 'LoveBot'
      )

      PaymentsRoot = new Firebase(baseDataApiUrl + 'payments')
      PaymentsRoot
        .child(username)
          .child(type)
            .push(charge)

      Users = new Firebase("#{baseDataApiUrl}users")
      UsersRoot = Users
      activeStatus = UsersRoot.child(username).child('active')
      ImpressionsBase = UsersRoot.child(username).child('impressions')


      if type == 'stamps'
        activeStatus.set true
      else
        # Must cron job to check expiry date for Merchant dashboard (if expiry date has elapsed, set to 0)
        # Find difference of stamp impressions remaining
        # Normalize remaining stamp impression by currentBalance / totalStampImpressions (allotted to entire population)
        #
        # 1. At budget set, the current allowance becomes Actual and Revenue value is set to 0.
        # 
        # Budget Update after User Stamping or New Budget Total
        # 1. currentAllowance = Payout Budget + Revenue (used in rolling up remaining impressions)
        #    1.1. Amount available on ChangeTip
        # 2. payoutBudget = (1) - (3) (at first run, 2 is set to Current Allowance)
        # 3. revenueTotal = Profit
        # 4. budgetSetTotal = [$] (when set, update Current Allowance)
        #
        # Cron Layout
        # 0. Within timeframe of month.
        # 1. Cron should check ((2) / AggregateStampImpression) * RemainingStampImpressionsMerchant
        #    1.1. Add to (3).
        #    1.2. Reduce from (2).
        # 2. Set to RemainingStampImpressionsMerchant to 0 after check.

        impressioned = impressionsUpdate(username, quantity)
        impressioned.then (impression) ->
          if impression.val() == null
            d = new Date()
            e = new Date()
            _d = moment(d)
            _e = moment(e)
            __d = _d.add((quantity/10), 'days')
            impressionsSnapshot =
              quantity: quantity
              expiry: __d.toString()
            ImpressionsBase.push impressionsSnapshot

      response.send true
    )
  )
  return

payments_usd = (request, response) ->
  ###
  @see https://stripe.com/docs/api/node#create_charge
  @namespace payment/:username/:type
  @description

  Payment controllers speak to Stripe via Stripe.js data requests, and they 
  dish back receipts depends on instance of nodemailer's SMTP Transport 
  object[0].

  —
  [0]: https://github.com/andris9/nodemailer-smtp-transport
  ###

  transport = nodemailer.createTransport(smtpTransport({
    host: 'smtp.lovestamp.io'
    port: 587
    debug: true
    secure: false
    secureConnection: false
    ignoreTLS: false
    tls:
      rejectUnauthorized:false
      #ciphers:'SSLv3'
    auth:
      user: 'aaron'
      pass: 'cCNkd0BpKQ3UHg=='
  }))

  username = request.params.username
  type = request.params.type

  requestBody = request.body
  currency    = requestBody.currency
  token       = requestBody.token
  quantity    = requestBody.quantity
  email       = requestBody.email
  bitcoinAddy = requestBody.bitcoinAddy
  rememberMe  = requestBody.rememberMe

  _type       = requestBody.metadata.type
  _multiplier = if _type is '1' then 50 else 30

  hostname = request.headers.host
  portSpec = if request.headers.port != "" then (':' + request.headers.port) else ''
  protocol = request.headers.protocol
  hostname = 'http:' + '//' + hostname

  UsersRoot = new Firebase(baseDataApiUrl + 'users')
  PurchaserProfile = UsersRoot.child(username)

  purchaserMessageText = "Sound the alarms!"
  purchaserMessageText += "Thanks #{username} for your purchase of #{quantity} #{type}."

  purchaserMessageHtml = "<p>Sound the alarms!</p>"
  purchaserMessageHtml += "<p>Thanks #{username} for your purchase of #{quantity} #{type}.</p>"

  amount = quantity * _multiplier

  paymentSpecification =
    amount      : amount
    currency    : currency
    card        : token
    description : (quantity + ' ' + type + " for " + username)

  messageText = "Sound the alarms!"
  messageText += "\n\nLoveStamp user, <a href='#{hostname}/users/#{username}'>#{username}</a>, has made a purchase of #{quantity} #{type}."
  if type == 'stamps'
    messageText += "\n\nPlease review available stamps in our <a href='#{hostname}/admin'>Admin Command Center</a>."

  messageHtml = "<p>Sound the alarms!</p>"
  messageHtml += "<p>LoveStamp user, <a href='#{hostname}/users/#{username}'>#{username}</a>, has made a purchase of #{quantity} #{type}.</p>"
  if type == 'stamps'
    messageHtml += "<p>Please review available stamps in our <a href='#{hostname}/admin'>Admin Command Center</a>.</p>"

  transport.sendMail(
    from    : 'webmaster@lovestamp.io'
    to      : 'support@lovestamp.io'
    subject : 'A LoveStamp user has made a purchase of ' + amount + ' ' + currency.toUpperCase()
    text    : messageText
    html    : messageHtml
  )

  PurchaserProfile.once 'value', (profileData) ->
    purchaserEmail = profileData.purchaseEmail

    purchaserMessageText = "Thanks #{username} for your purchase of #{quantity} #{type} for #{amount}."

    #purchaserMessageHtml = "<p>Thanks #{username} for your purchase of #{quantity} #{type}.</p>"

    htmlEmailTemplateFilePath = __dirname + '/../../../email/newsletter/purchase.inline.html'

    loadHtmlFile(htmlEmailTemplateFilePath).then (htmlEmailConstruct) ->

      transport.sendMail(
        from    : 'webmaster@lovestamp.io'
        to      : purchaserEmail
        subject : username + ', you have made a purchase of ' + amount + ' ' + currency.toUpperCase()
        text    : purchaserMessageText
        html    : htmlEmailConstruct
      )

  prefix = username.charAt(0)

  handle = urlPrefix[prefix] + username

  payments_slack.send(
      text     : "#{handle} has made a purchase of #{quantity} #{type}."
      channel  : '#payments'
      username : 'LoveBot'
  )

  if ! rememberMe
    packagedCharge = stripe.charges.create(paymentSpecification, (err, charge) ->
      PaymentsRoot = new Firebase(baseDataApiUrl + 'payments')
      PaymentsRoot
        .child(username)
          .child(type)
            .push(charge)

      Users = new Firebase("#{baseDataApiUrl}users")
      UsersRoot = Users
      activeStatus = UsersRoot.child(username).child('active')
      ImpressionsBase = UsersRoot.child(username).child('impressions')


      if type == 'stamps'
        activeStatus.set true
      else
        # Must cron job to check expiry date for Merchant dashboard (if expiry date has elapsed, set to 0)
        # Find difference of stamp impressions remaining
        # Normalize remaining stamp impression by currentBalance / totalStampImpressions (allotted to entire population)
        #
        # 1. At budget set, the current allowance becomes Actual and Revenue value is set to 0.
        # 
        # Budget Update after User Stamping or New Budget Total
        # 1. currentAllowance = Payout Budget + Revenue (used in rolling up remaining impressions)
        #    1.1. Amount available on ChangeTip
        # 2. payoutBudget = (1) - (3) (at first run, 2 is set to Current Allowance)
        # 3. revenueTotal = Profit
        # 4. budgetSetTotal = [$] (when set, update Current Allowance)
        #
        # Cron Layout
        # 0. Within timeframe of month.
        # 1. Cron should check ((2) / AggregateStampImpression) * RemainingStampImpressionsMerchant
        #    1.1. Add to (3).
        #    1.2. Reduce from (2).
        # 2. Set to RemainingStampImpressionsMerchant to 0 after check.

        impressioned = impressionsUpdate(username, quantity)
        impressioned.then (impression) ->
          if impression.val() == null
            d = new Date()
            e = new Date()
            _d = moment(d)
            _e = moment(e)
            __d = _d.add((quantity/10), 'days')
            impressionsSnapshot =
              quantity: quantity
              expiry: __d.toString()
            ImpressionsBase.push impressionsSnapshot

      response.send true
    )
    return

  stripe.customers.create(
    source      : token
    description : "#{email}" + (quantity + ' ' + type + " for " + username)
  ).then((customer) ->
    paymentSpecification.customer = customer.id
    packagedCharge = stripe.charges.create(paymentSpecification, (err, charge) ->
      PaymentsRoot = new Firebase(baseDataApiUrl + 'payments')
      PaymentsRoot
        .child(username)
          .child(type)
            .push(charge)

      Users = new Firebase("#{baseDataApiUrl}users")
      UsersRoot = Users
      activeStatus = UsersRoot.child(username).child('active')
      ImpressionsBase = UsersRoot.child(username).child('impressions')


      if type == 'stamps'
        activeStatus.set true
      else
        # Must cron job to check expiry date for Merchant dashboard (if expiry date has elapsed, set to 0)
        # Find difference of stamp impressions remaining
        # Normalize remaining stamp impression by currentBalance / totalStampImpressions (allotted to entire population)
        #
        # 1. At budget set, the current allowance becomes Actual and Revenue value is set to 0.
        # 
        # Budget Update after User Stamping or New Budget Total
        # 1. currentAllowance = Payout Budget + Revenue (used in rolling up remaining impressions)
        #    1.1. Amount available on ChangeTip
        # 2. payoutBudget = (1) - (3) (at first run, 2 is set to Current Allowance)
        # 3. revenueTotal = Profit
        # 4. budgetSetTotal = [$] (when set, update Current Allowance)
        #
        # Cron Layout
        # 0. Within timeframe of month.
        # 1. Cron should check ((2) / AggregateStampImpression) * RemainingStampImpressionsMerchant
        #    1.1. Add to (3).
        #    1.2. Reduce from (2).
        # 2. Set to RemainingStampImpressionsMerchant to 0 after check.

        impressioned = impressionsUpdate(username, quantity)
        impressioned.then (impression) ->
          if impression.val() == null
            d = new Date()
            e = new Date()
            _d = moment(d)
            _e = moment(e)
            __d = _d.add((quantity / 10), 'days')
            impressionsSnapshot =
              quantity : quantity
              expiry   : __d.toString()
            ImpressionsBase.push impressionsSnapshot

      response.send true
    )
  )
  return


module.exports = 
  payments_btc: payments_btc
  payments_usd: payments_usd
