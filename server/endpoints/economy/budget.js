(function() {
  var Firebase, aggregateStampImpression, budgetCronStart, budgetReset, reset, rollUp;

  Firebase = require('firebase');

  aggregateStampImpression = require('../../utils/aggregateStampImpression');

  budgetReset = function(newBudget, newRevenue) {

    /*
    @module utils
    @namespace budgetReset
    @usage
    
      > budgetSet(1000000)
      > budgetSet 1000000, 50
    
    @param {number} newBudget A new budget to reset in the LoveStamp's balances sheet.
    @param {number} newRevenue A new revenue to reset in the LoveStamp's balances sheet.
    @return {response} A sensible object that tells the controller nice JSON-y things.
     */
    var balancesRoot, e, payoutShadowBudgetSource;
    newBudget = !newBudget ? 0 : parseInt(newBudget, 10);
    newRevenue = !newRevenue ? 0 : parseInt(newRevenue, 10);
    balancesRoot = new Firebase('https://lovestamp.firebaseio.com/balances');
    try {
      payoutShadowBudgetSource = balancesRoot.child('payoutShadowBudget');
      payoutShadowBudgetSource.set(newBudget);
    } catch (_error) {
      e = _error;
      return false;
    }
    return true;
  };

  rollUp = function(req, res) {

    /*
    @namespace budget/roll-up
    @return {res:Boolean}
     */
    if (aggregateStampImpression.budgetCron()) {
      aggregateStampImpression.budgetCronStart();
      return res.send(true);
    } else {
      return res.send(false);
    }
  };

  budgetCronStart = function() {
    return aggregateStampImpression.budgetCronStart();
  };

  reset = function(req, res) {

    /*
    @namespace budget/reset/:newBudget/:newRevenue
    @description
    
      Budget resetter.
     */
    var newBudget, newRevenue;
    newBudget = req.params.newBudget;
    newRevenue = req.params.newRevenue;
    if (budgetReset(newBudget, newRevenue)) {
      return res.send(true);
    } else {
      return res.send(false);
    }
  };

  module.exports = {
    reset: reset,
    rollUp: rollUp,
    budgetCronStart: budgetCronStart
  };

}).call(this);
