Firebase = require('firebase')
ElasticClient = require('elasticsearchclient')

search = (req, res) ->
  ###*
  @namespace search
  ###

  queue = new Firebase('https://lovestamp.firebaseio.com/search')
  client = new ElasticClient({
    host: 'localhost'
    port: 9200
  })

  # @inner
  processRequest = (snap) ->
    snap.ref().remove()
    data = snap.val()
    client.search(data.index, data.type, {
      query:
        'query_string':
          query: data.query
    })
    .on('data', (data) ->
      queue
        .child('response/' + snap.key())
        .set(results)
    )
    .on('error', (error) ->
    )
    .exec()

  queue.
    child('request').
    on('child_added', processRequest)



module.exports =
  search: search
