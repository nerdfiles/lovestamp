(function() {
  var ElasticClient, Firebase, search;

  Firebase = require('firebase');

  ElasticClient = require('elasticsearchclient');

  search = function(req, res) {

    /**
    @namespace search
     */
    var client, processRequest, queue;
    queue = new Firebase('https://lovestamp.firebaseio.com/search');
    client = new ElasticClient({
      host: 'localhost',
      port: 9200
    });
    processRequest = function(snap) {
      var data;
      snap.ref().remove();
      data = snap.val();
      return client.search(data.index, data.type, {
        query: {
          'query_string': {
            query: data.query
          }
        }
      }).on('data', function(data) {
        return queue.child('response/' + snap.key()).set(results);
      }).on('error', function(error) {}).exec();
    };
    return queue.child('request').on('child_added', processRequest);
  };

  module.exports = {
    search: search
  };

}).call(this);
