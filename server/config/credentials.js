
/*

                       _                  _       _
                      | |             _  (_)     | |
  ____  ____ _____  __| |_____ ____ _| |_ _ _____| |  ___
 / ___)/ ___) ___ |/ _  | ___ |  _ (_   _) (____ | | /___)
( (___| |   | ____( (_| | ____| | | || |_| / ___ | ||___ |
 \____)_|   |_____)\____|_____)_| |_| \__)_\_____|\_|___/
 */

(function() {
  var Firebase, Q, at, auth_changetip, baseDataApiUrl, credentials, e, rt, scope_list, _createCredentials, _scope_list;

  Firebase = require('firebase');

  baseDataApiUrl = 'https://lovestamp.firebaseio.com/';

  auth_changetip = new Firebase("" + baseDataApiUrl + "app/auth");

  try {
    Q = require('q');
  } catch (_error) {
    e = _error;
    Q = require('Q');
  }

  scope_list = ['read_user_basic', 'read_user_full', 'create_tip_urls', 'read_and_create_withdrawals', 'read_my_tips_on_channel', 'send_my_tips_on_channel', 'read_my_tips_all_channels', 'send_my_tips_all_channels'];

  at = null;

  rt = null;

  _scope_list = scope_list.join(' ');

  credentials = {
    certPath: 'cert-apn.pem',
    keyPath: 'key-apn.pem',
    passphrase: 'elKEr3T+cMpuHw==',
    changeTip: {
      _load_refresh_token: function(_refresh_token) {
        if (_refresh_token) {
          credentials.changeTip.refresh_token = rt = _refresh_token;
          return _refresh_token;
        }
      },
      _load_access_token: function(_access_token) {
        if (_access_token) {
          credentials.changeTip.access_token = at = _access_token;
          return _access_token;
        }
      },
      load_refresh_token: function(domain) {
        var def, _refresh_token;
        def = Q.defer();
        _refresh_token = auth_changetip.child(domain).child('refresh_token');
        _refresh_token.once('value', function(_refresh_token_snapshot) {
          return def.resolve(_refresh_token_snapshot.val());
        });
        return def.promise;
      },
      load_access_token: function(domain) {
        var def, _access_token;
        def = Q.defer();
        _access_token = auth_changetip.child(domain).child('access_token');
        _access_token.once('value', function(_access_token_snapshot) {
          return def.resolve(_access_token_snapshot.val());
        });
        return def.promise;
      },
      oauth2: {
        api_key: '317a15f2552b1800f8d6eebc3eafad5db221ba61',
        access_token: 'MxOdznlpu5PWDUQAGteFGKWU42exWp',
        refresh_token: null,
        client_id: '47bvNoTvKEMW1PibNxu5ARFv9CPf2G4n2DtDvSj5',
        clientID: '47bvNoTvKEMW1PibNxu5ARFv9CPf2G4n2DtDvSj5',
        client_secret: 'eGpHO9Tr8a6xdXz3h5rIxe1oiQFN1kzC77i0lodtlH8z2TPck9et209CbrkNmkKmHouYeK61pSH4Cwf6daeaw0TxvAo2UgiWL0DkWHa9WeNd5KEPkOdaQBwPJmL3dNWt',
        clientSecret: 'eGpHO9Tr8a6xdXz3h5rIxe1oiQFN1kzC77i0lodtlH8z2TPck9et209CbrkNmkKmHouYeK61pSH4Cwf6daeaw0TxvAo2UgiWL0DkWHa9WeNd5KEPkOdaQBwPJmL3dNWt',
        authorization_path: 'o/authorize/?response_type=code',
        authorizationPath: 'o/authorize/',
        authorizationPathFull: 'https://www.changetip.com/o/authorize/',
        site: 'https://www.changetip.com/',
        scope: _scope_list,
        token_path: 'o/token/',
        tokenPath: 'o/token/',
        tokenPathFull: 'https://www.changetip.com/o/token/',
        revocation_path: 'o/revoke/',
        revocationPath: 'o/revoke/',
        redirect_uri: function(_protocol, _host) {
          var host, protocol;
          host = void 0;
          if (!_host) {
            host = 'lovestamp.io';
          } else {
            host = _host;
          }
          protocol = void 0;
          if (_host.indexOf('local') !== -1) {
            protocol = 'http';
          } else {
            protocol = 'https';
          }
          return "" + protocol + "://" + host + "/changetip/code/callback";
        },
        response_type: 'code',
        grant_types: {
          authorization_code: 'authorization_code',
          refresh_token: 'refresh_token',
          password: 'password',
          client_credentials: 'client_credentials'
        }
      }
    },
    google: {
      android: {
        apiKey: ''
      },
      api_key: 'AIzaSyDBh-l5nogixYIQCoTAWCO6Ne0SC1X1ZH4',
      client_id: '179206523209-to386dfbphh0o6j3pgjr2f0pvfc4sias.apps.googleusercontent.com',
      client_secret: 'p05i4GOTX1-irc_c9aZzwzU-',
      redirect_url: 'https://auth.firebase.com/v2/lovestamp/auth/google/callback',
      redirect_url_local: 'urn:ietf:wg:oauth:2.0:oob'
    },
    twitter: {
      consumer_key: 'fc9FyQ1NleuREOhUgTVQM5ZVF',
      consumer_secret: '709eM11C7WpEGPCD3uIAvNIP5bbszFqgEsDhw8pCpcGZHdwtQQ',
      access_token: '2965571608-WINL0ohJAvkOy2bwOtVebtJhr3CVkQTY7AOzfZY',
      access_token_secret: 'FOEuYitOhjR1ZX4slgouhy9xYIAEeSvuLD8AW8QfC9rQK'
    },
    facebook: {
      client_token: '2a9d912f24cbbd289e0a3a182232b261',
      app_id: '363241763860876',
      app_secret: '0eef722bb946f6fc19c2c3ccda08ef72'
    }
  };

  _createCredentials = function(domain) {
    var _access_token, _refresh_token;
    if (!domain) {
      return credentials;
    }
    _access_token = auth_changetip.child(domain).child('access_token');
    _refresh_token = auth_changetip.child(domain).child('refresh_token');
    if (!at) {
      _access_token.once('value', function(access_token_snapshot) {
        console.log(access_token_snapshot);
        return at = credentials.changeTip.access_token = access_token_snapshot.val();
      });
    } else {
      credentials.changeTip.access_token = at;
    }
    if (!rt) {
      _refresh_token.once('value', function(refresh_token_snapshot) {
        return credentials.changeTip.refresh_token = refresh_token_snapshot.val();
      });
    } else {
      credentials.changeTip.refresh_token = rt;
    }
    console.log('Updated credentials');
    console.log(credentials);
    return credentials;
  };

  module.exports = {
    credentials: _createCredentials,
    scope_list: _scope_list
  };

}).call(this);
