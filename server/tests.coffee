Firebase = require('firebase')
_ = require('lodash')
Chance = require('chance')
Q = require('Q')
moment = require('moment')

###
@description
Moment designator expiry.
###
momentDesignator = (timestamp) ->
  momento = moment(timestamp)    # 1987-08-16T07:00:00.000Z
  day   = momento.date()         # 16
  month = momento.month() + 1    # 8
  year  = momento.year()         # 1987

  timeperiods =
    year  : moment(year, "YYYY").toISOString(),                                # 1987-01-01T08:00:00.000Z
    month : moment(month + "-" + year, "MM-YYYY").toISOString(),               # 1987-08-01T07:00:00.000Z
    day   : moment(day + "-" + month + "-" + year, "DD-MM-YYYY").toISOString() # 1987-08-16T07:00:00.000Z


budgetCron()
#budgetSet 1000000
#normalStampImpression('@RattelyrDragon')
