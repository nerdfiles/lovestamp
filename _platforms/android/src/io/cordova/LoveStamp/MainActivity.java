/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package io.cordova.LoveStamp;

import android.os.Bundle;
import org.apache.cordova.*;
import android.webkit.WebSettings;
import android.webkit.WebView;
//import android.webkit.View;

public class MainActivity extends CordovaActivity
{
    WebView view;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        loadUrl(launchUrl);
        view = new WebView(this);
        //setContentView(view);
        view.getSettings().setAppCacheEnabled(false);
        view.getSettings().setJavaScriptEnabled(true);
        //view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        view.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        view.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        view.getSettings().setPluginState(android.webkit.WebSettings.PluginState.ON_DEMAND);
    }
    /*
     *@Override
     *public void onCreate(Bundle savedInstanceState) {
     *    super.onCreate(savedInstanceState);
     *    super.loadUrlTimeoutValue = 15000;
     *    loadUrl("file:///android_asset/www/index.html");
     *    if (Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN)
     *        appView.getSettings().setAllowUniversalAccessFromFileURLs(true);
     *    appView.getSettings().setNavDump(false);
     *}
     */
}
