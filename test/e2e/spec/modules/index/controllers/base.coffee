###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

## description

###

baseUrl = 'http://local.lovestamp.io:8080'
util = require('util')
Pby = protractor.By
handlePromise = null
webdriver = require('selenium-webdriver')
#$apply = require('../../../../utils/apply')(browser)
#$wait = require('../../../../utils/wait')(browser)
#$selectWindow = require('../../../../utils/selectWindow')
#$waitForSelector = require('../../../../utils/waitForSelector')


$apply = () ->
  ###
  $scope phase refresher.
  ###

  browser.driver.sleep(1)
  browser.waitForAngular()


$wait = (_elm, label) ->
  ###
  Wait for an element when browser.ignoreSynchronization == true
  ###

  browser.driver.wait(() ->
    elm = element(_elm)
    if elm.isPresent()
      return elm
  , 40000, label + " did not load")


$selectWindow = (index) ->
  ###
  [selectWindow Focus the browser to the index window. Implementation by http://stackoverflow.com/questions/21700162/protractor-e2e-testing-error-object-object-object-has-no-method-getwindowha]
  @param  {object}
  @param  {object} index
    [Is the index of the window. E.g., 0=browser, 1=FBpopup]
  @return {!webdriver.promise.Promise.<void>}
    [Promise resolved when the index window is focused.]
  ###

  # wait for handels[index] to exists
  browser.driver.wait ->
    browser.driver.getAllWindowHandles().then (handles) ->

      ###*
      # Assume that handles.length >= 1 and index >=0.
      # So when i call selectWindow(index) i return
      # true if handles contains that window.
      ###

      if handles.length > index
        return true
      return
  # here i know that the requested window exists
  # switch to the window
  browser.driver.getAllWindowHandles().then (handles) ->
    browser.driver.switchTo().window handles[index]


$waitForSelector = (promiseFn, testFn) ->
  ###
  @param {object} object
  @param {object} protractor
  @param {function} promiseFn
  @param {function} testFn
  @usage
      waitForSelector(element(by.id('some-element')).isPresent,
        (isPresent) ->
          return !isPresent
        )
  ###

  browser.driver.wait ->
    deferred = protractor.promise.defer()
    promiseFn().then (data) ->
      deferred.fulfill testFn(data)
      return
    deferred.promise
  return


###

      ___  ____ _____ ____  _____  ___
     /___)/ ___) ___ |  _ \| ___ |/___)
    |___ ( (___| ____| | | | ____|___ |
    (___/ \____)_____)_| |_|_____|___/

###

describe 'merchant login', ->

  shouldAccessTwitterOAuthWindow = ->
    ###
    Login via Firebase with a Twitter OAuth token.
    ###

    #authWithTwitterButton = $waitForSelector(element(By.css('.auth-with-twitter')).isPresent,
      #(isPresent) ->
        #console.log 'Found...'
        #console.log isPresent
        #return !isPresent
    #)

    element(By.css('.auth-with-twitter')).click()

    handlePromise = browser.getAllWindowHandles().then((handles) ->
      browser.driver.switchTo().window(handles[1])
      browser.driver.close()
      browser.driver.switchTo().window(handles[0])
    )

    $apply()
    return


  shouldAccessFacebookOAuthWindow = ->
    ###
    Login via Firebase with a Facebook OAuth token.
    ###

    element(By.css('.auth-with-facebook')).click()

    handlePromise = browser.getAllWindowHandles().then((handles) ->
      browser.driver.switchTo().window(handles[1])
      browser.driver.close()
      browser.driver.switchTo().window(handles[0])
    )

    $apply()
    return


  shouldAccessGoogleOAuthWindow = ->
    ###
    Login via Firebase with a Google+ OAuth token.
    ###

    element(By.css('.auth-with-google')).click()

    handlePromise = browser.getAllWindowHandles().then((handles) ->
      browser.driver.switchTo().window(handles[1])
      browser.driver.close()
      browser.driver.switchTo().window(handles[0])
    )

    $apply()
    return


  beforeEach ->
    browser.get baseUrl + '/merchant/login'

    $apply()
    return
  , 60000

  #it 'should access Twitter OAuth window', shouldAccessTwitterOAuthWindow
  #it 'should access Facebook OAuth window', shouldAccessFacebookOAuthWindow
  #it 'should access Google+ OAuth window', shouldAccessGoogleOAuthWindow

  it 'should authentication via Twitter OAuth window', ->

    element(By.css('.auth-with-twitter')).click()

    handlePromise = browser.getAllWindowHandles()

    handlePromise.then((handles) ->
      popUpHandle = handles[1]

      # Before accessing our popup window, we need to ignore Angular-type capabilities, 
      # and use webdriver itself to control windows and the DOM.
      browser.ignoreSynchronization = false

      #$selectWindow(1).then (_window) ->
        #if _window
          #console.log 'Found popup window...'
        #else
          #console.log 'Could not find popup window!'

      # Switch to our OAuth-able window.
      browser.switchTo().window(popUpHandle)

      # Check if the client correctly calls the Social Network's OAuth entry point.
      #expect(browser.getCurrentUrl()).toEqual('popup/url')

      #browser.driver.executeScript((e) ->
        #console.log e
      #)

      console.log('Now Opening Window:', popUpHandle)

      submitButton = By.css('.button.submit')
      oauthTokenInForm = By.id('oauth_token')
      username_or_email = By.id('username_or_email')
      password = By.id('password')

      $wait(username_or_email, 'Username entry field').then (a) ->
        element(a).sendKeys('filesofnerds')

      $wait(username_or_email, 'Password entry field').then (a) ->
        element(a).sendKeys('t0d4y1sth3d4y!')

      $wait(oauthTokenInForm, 'OAuth token in form'). then (a) ->
        formToken = a.getAttribute('value')
        console.log formToken

      $wait(submitButton, 'OAuth submit button').then (a) ->
        element(a).click()

      # Restore interest in Angularized documents, giving us access to ``element``, etc.
      browser.ignoreSynchronization = true

      browser.driver.close()

      # Switch back to our Angularized platform.
      browser.switchTo().window(handles[0])

      # Check if the Social Network satisfied our OAuth Callback Specification.
      #expect(browser.getCurrentUrl()).toEqual('original/url')
      return
    )

    $apply()
    return


  it 'should log out', () ->
    ###
    No actual module controller corresponds to this route. The application 
    checks at $locationChangeStart to decide what to do with the given user.
    ###

    browser.get baseUrl + '/logout'

    $apply()
    return
