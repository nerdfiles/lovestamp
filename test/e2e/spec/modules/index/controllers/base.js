
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

 *# description
 */

(function() {
  var $apply, $selectWindow, $wait, $waitForSelector, Pby, baseUrl, handlePromise, util, webdriver;

  baseUrl = 'http://local.lovestamp.io:8080';

  util = require('util');

  Pby = protractor.By;

  handlePromise = null;

  webdriver = require('selenium-webdriver');

  $apply = function() {

    /*
    $scope phase refresher.
     */
    browser.driver.sleep(1);
    return browser.waitForAngular();
  };

  $wait = function(_elm, label) {

    /*
    Wait for an element when browser.ignoreSynchronization == true
     */
    return browser.driver.wait(function() {
      var elm;
      elm = element(_elm);
      if (elm.isPresent()) {
        return elm;
      }
    }, 40000, label + " did not load");
  };

  $selectWindow = function(index) {

    /*
    [selectWindow Focus the browser to the index window. Implementation by http://stackoverflow.com/questions/21700162/protractor-e2e-testing-error-object-object-object-has-no-method-getwindowha]
    @param  {object}
    @param  {object} index
      [Is the index of the window. E.g., 0=browser, 1=FBpopup]
    @return {!webdriver.promise.Promise.<void>}
      [Promise resolved when the index window is focused.]
     */
    browser.driver.wait(function() {
      return browser.driver.getAllWindowHandles().then(function(handles) {

        /**
         * Assume that handles.length >= 1 and index >=0.
         * So when i call selectWindow(index) i return
         * true if handles contains that window.
         */
        if (handles.length > index) {
          return true;
        }
      });
    });
    return browser.driver.getAllWindowHandles().then(function(handles) {
      return browser.driver.switchTo().window(handles[index]);
    });
  };

  $waitForSelector = function(promiseFn, testFn) {

    /*
    @param {object} object
    @param {object} protractor
    @param {function} promiseFn
    @param {function} testFn
    @usage
        waitForSelector(element(by.id('some-element')).isPresent,
          (isPresent) ->
            return !isPresent
          )
     */
    browser.driver.wait(function() {
      var deferred;
      deferred = protractor.promise.defer();
      promiseFn().then(function(data) {
        deferred.fulfill(testFn(data));
      });
      return deferred.promise;
    });
  };


  /*
  
        ___  ____ _____ ____  _____  ___
       /___)/ ___) ___ |  _ \| ___ |/___)
      |___ ( (___| ____| | | | ____|___ |
      (___/ \____)_____)_| |_|_____|___/
   */

  describe('merchant login', function() {
    var shouldAccessFacebookOAuthWindow, shouldAccessGoogleOAuthWindow, shouldAccessTwitterOAuthWindow;
    shouldAccessTwitterOAuthWindow = function() {

      /*
      Login via Firebase with a Twitter OAuth token.
       */
      element(By.css('.auth-with-twitter')).click();
      handlePromise = browser.getAllWindowHandles().then(function(handles) {
        browser.driver.switchTo().window(handles[1]);
        browser.driver.close();
        return browser.driver.switchTo().window(handles[0]);
      });
      $apply();
    };
    shouldAccessFacebookOAuthWindow = function() {

      /*
      Login via Firebase with a Facebook OAuth token.
       */
      element(By.css('.auth-with-facebook')).click();
      handlePromise = browser.getAllWindowHandles().then(function(handles) {
        browser.driver.switchTo().window(handles[1]);
        browser.driver.close();
        return browser.driver.switchTo().window(handles[0]);
      });
      $apply();
    };
    shouldAccessGoogleOAuthWindow = function() {

      /*
      Login via Firebase with a Google+ OAuth token.
       */
      element(By.css('.auth-with-google')).click();
      handlePromise = browser.getAllWindowHandles().then(function(handles) {
        browser.driver.switchTo().window(handles[1]);
        browser.driver.close();
        return browser.driver.switchTo().window(handles[0]);
      });
      $apply();
    };
    beforeEach(function() {
      browser.get(baseUrl + '/merchant/login');
      $apply();
    }, 60000);
    it('should authentication via Twitter OAuth window', function() {
      element(By.css('.auth-with-twitter')).click();
      handlePromise = browser.getAllWindowHandles();
      handlePromise.then(function(handles) {
        var oauthTokenInForm, password, popUpHandle, submitButton, username_or_email;
        popUpHandle = handles[1];
        browser.ignoreSynchronization = false;
        browser.switchTo().window(popUpHandle);
        console.log('Now Opening Window:', popUpHandle);
        submitButton = By.css('.button.submit');
        oauthTokenInForm = By.id('oauth_token');
        username_or_email = By.id('username_or_email');
        password = By.id('password');
        $wait(username_or_email, 'Username entry field').then(function(a) {
          return element(a).sendKeys('filesofnerds');
        });
        $wait(username_or_email, 'Password entry field').then(function(a) {
          return element(a).sendKeys('t0d4y1sth3d4y!');
        });
        $wait(oauthTokenInForm, 'OAuth token in form').then(function(a) {
          var formToken;
          formToken = a.getAttribute('value');
          return console.log(formToken);
        });
        $wait(submitButton, 'OAuth submit button').then(function(a) {
          return element(a).click();
        });
        browser.ignoreSynchronization = true;
        browser.driver.close();
        browser.switchTo().window(handles[0]);
      });
      $apply();
    });
    return it('should log out', function() {

      /*
      No actual module controller corresponds to this route. The application 
      checks at $locationChangeStart to decide what to do with the given user.
       */
      browser.get(baseUrl + '/logout');
      $apply();
    });
  });

}).call(this);
