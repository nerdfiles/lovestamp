###*
# fileOverview

@see https://github.com/angular/protractor/blob/master/docs/faq.md

The Authentication Suite is a special case for Administrators.

###

describe 'Auth', ->

  ptor = undefined

  beforeEach ->
    ptor = protractor.getInstance()
    browser.get '/merchant/login'
    # ptor.get('/login');
    ptor.waitForAngular()
    return

  it 'should present a pristine login form.', ->
    form$ = element(By.css('#loginForm'))
    form$.getTagName().then (tag) ->
      expect(tag).toBe 'form'
      return
    form$.getAttribute('class').then (attr) ->
      expect(attr).toContain 'ng-pristine'
      return
    return

  it 'should log in with valid Admin user.', ->
    # Send arbitrary known successful user.
    username$ = element(By.css('#login-username'))
    username$.clear()
    username$.sendKeys 'testAdminUser'
    password$ = element(By.css('#login-password'))
    password$.clear()
    password$.sendKeys 'password'
    button$ = element(By.css('#loginForm button[type="submit"]'))
    button$.click()
    expect(ptor.getCurrentUrl()).not.toContain 'login'
    return

  it 'should log in with valid non-Admin user.', ->
    # Send arbitrary known successful user.
    username$ = element(By.css('#login-username'))
    username$.clear()
    username$.sendKeys 'fn-functionaltest'
    password$ = element(By.css('#login-password'))
    password$.clear()
    password$.sendKeys 'password'
    button$ = element(By.css('#loginForm button[type="submit"]'))
    button$.click()
    expect(ptor.getCurrentUrl()).not.toContain 'login'
    return

  it 'should fail authentication.', ->
    # Send arbitrary known successful user.
    username$ = element(By.css('#login-username'))
    username$.clear()
    username$.sendKeys 'fn-functionaltest'
    password$ = element(By.css('#login-password'))
    password$.clear()
    password$.sendKeys 'failingpassword'
    button$ = element(By.css('#loginForm button[type="submit"]'))
    button$.click()
    expect(ptor.getCurrentUrl()).toContain 'login'
    return

  return
