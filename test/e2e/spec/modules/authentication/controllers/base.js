
/**
 * fileOverview

@see https://github.com/angular/protractor/blob/master/docs/faq.md

The Authentication Suite is a special case for Administrators.
 */

(function() {
  describe('Auth', function() {
    var ptor;
    ptor = void 0;
    beforeEach(function() {
      ptor = protractor.getInstance();
      browser.get('/merchant/login');
      ptor.waitForAngular();
    });
    it('should present a pristine login form.', function() {
      var form$;
      form$ = element(By.css('#loginForm'));
      form$.getTagName().then(function(tag) {
        expect(tag).toBe('form');
      });
      form$.getAttribute('class').then(function(attr) {
        expect(attr).toContain('ng-pristine');
      });
    });
    it('should log in with valid Admin user.', function() {
      var button$, password$, username$;
      username$ = element(By.css('#login-username'));
      username$.clear();
      username$.sendKeys('testAdminUser');
      password$ = element(By.css('#login-password'));
      password$.clear();
      password$.sendKeys('password');
      button$ = element(By.css('#loginForm button[type="submit"]'));
      button$.click();
      expect(ptor.getCurrentUrl()).not.toContain('login');
    });
    it('should log in with valid non-Admin user.', function() {
      var button$, password$, username$;
      username$ = element(By.css('#login-username'));
      username$.clear();
      username$.sendKeys('fn-functionaltest');
      password$ = element(By.css('#login-password'));
      password$.clear();
      password$.sendKeys('password');
      button$ = element(By.css('#loginForm button[type="submit"]'));
      button$.click();
      expect(ptor.getCurrentUrl()).not.toContain('login');
    });
    it('should fail authentication.', function() {
      var button$, password$, username$;
      username$ = element(By.css('#login-username'));
      username$.clear();
      username$.sendKeys('fn-functionaltest');
      password$ = element(By.css('#login-password'));
      password$.clear();
      password$.sendKeys('failingpassword');
      button$ = element(By.css('#loginForm button[type="submit"]'));
      button$.click();
      expect(ptor.getCurrentUrl()).toContain('login');
    });
  });

}).call(this);
