###
# fileOverview
           ______
          (_____ \
     _____  ____) )_____
    | ___ |/ ____/| ___ |
    | ____| (_____| ____|
    |_____)_______)_____)

## description

## note

Note about browser vs. protractor.getInstance(): they’re exactly the same
object! ``browser`` is the new preferred syntax.

###

exports.config =
  seleniumAddress: 'http://local.lovestamp.io:4444/wd/hub'
  specs: [
    #'spec/**/*.js'
    #'spec/modules/authentication/controllers/base.js'
    #'spec/modules/authentication/directives/userRollup.js'
    'spec/modules/index/controllers/base.js'
  ]
  multiCapabilities: [
    { 'browserName': 'chrome' }
  ]
  baseUrl: 'http://local.lovestamp.io:8080'
  jasmineNodeOpts:
    onComplete: null
    isVerbose: true
    showColors: true
    includeStackTrace: true
    defaultTimeoutInterval: 10000

