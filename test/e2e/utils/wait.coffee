browser = null

$wait = (_elm, label) ->
  ###
  Wait for an element when browser.ignoreSynchronization == true
  ###

  browser.driver.wait(() ->
    elm = element(_elm)
    if elm.isPresent()
      return elm
  , 20000, label + " did not load")


module.exports = (_browser) ->
  browser = _browser
  new $wait
