(function() {
  var $apply, browser;

  browser = null;

  $apply = function() {

    /*
    $scope phase refresher.
     */
    browser.driver.sleep(1);
    return browser.waitForAngular();
  };

  module.exports = function(_browser) {
    browser = _browser;
    return $apply;
  };

}).call(this);
