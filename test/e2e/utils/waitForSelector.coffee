$waitForSelector = (browser, protractor, promiseFn, testFn) ->
  ###
  @param {object} object
  @param {object} protractor
  @param {function} promiseFn
  @param {function} testFn
  @usage
      waitForSelector(element(by.id('some-element')).isPresent,
        (isPresent) ->
          return !isPresent
        )
  ###

  browser.driver.wait ->
    deferred = protractor.promise.defer()
    promiseFn().then (data) ->
      deferred.fulfill testFn(data)
      return
    deferred.promise
  return

module.exports = $waitForSelector
