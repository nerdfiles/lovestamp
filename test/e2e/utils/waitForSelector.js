(function() {
  var $waitForSelector;

  $waitForSelector = function(browser, protractor, promiseFn, testFn) {

    /*
    @param {object} object
    @param {object} protractor
    @param {function} promiseFn
    @param {function} testFn
    @usage
        waitForSelector(element(by.id('some-element')).isPresent,
          (isPresent) ->
            return !isPresent
          )
     */
    browser.driver.wait(function() {
      var deferred;
      deferred = protractor.promise.defer();
      promiseFn().then(function(data) {
        deferred.fulfill(testFn(data));
      });
      return deferred.promise;
    });
  };

  module.exports = $waitForSelector;

}).call(this);
