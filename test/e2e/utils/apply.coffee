browser = null

$apply = () ->
  ###
  $scope phase refresher.
  ###

  browser.driver.sleep(1)
  browser.waitForAngular()

module.exports = (_browser) ->
  browser = _browser
  $apply
