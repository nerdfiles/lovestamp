(function() {
  var $wait, browser;

  browser = null;

  $wait = function(_elm, label) {

    /*
    Wait for an element when browser.ignoreSynchronization == true
     */
    return browser.driver.wait(function() {
      var elm;
      elm = element(_elm);
      if (elm.isPresent()) {
        return elm;
      }
    }, 20000, label + " did not load");
  };

  module.exports = function(_browser) {
    browser = _browser;
    return new $wait;
  };

}).call(this);
