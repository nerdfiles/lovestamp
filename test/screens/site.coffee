fs        = require('fs')
utils     = require('utils')
#cache    = require('cache')
#mimetype = require('mimetype')
response  = undefined

baseUrl = "http://local.lovestamp.io:8080/"
homeUrl = "http://local.lovestamp.io:8080/#/"

responses = {}
responses.large = {
  name: 'large'
  width: 1148
  height: 800
}
responses.medium = {
  name: 'medium'
  width: 768
  height: 1024
}
responses.small = {
  name: 'small'
  width: 320
  height: 480
}
casper = require('casper').create {
  verbose        : true
  logLevel       : 'debug'
  waitTimeout    : 10000
  stepTimeout    : 10000
  retryTimeout   : 150
  pageSettings:
    loadImages         : true
    loadPlugins        : true
    userAgent          : 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'
    webSecurityEnabled : false
    ignoreSslErrors    : true
  onWaitTimeout: () ->
    @echo('wait timeout')
    @clear()
    @page.stop()
  onStepTimeout: (timeout, step) ->
    @echo('step timeout')
    @clear()
    @page.stop()
}

response = casper.cli.get(0)
casper.options.viewportSize = responses["#{response}"]

pageName = casper.cli.get(1) or '.page--login'


# print out all the messages in the headless browser context
casper.on('remote.message', (msg) ->
  @echo('remote message caught: ' + msg)
)

# print out all the messages in the headless browser context
casper.on('page.error', (msg, trace) ->
   this.echo('Error: ' + msg, 'ERROR')
   for i in trace
     step = trace[i]
     @echo('   ' + step.file + ' (line ' + step.line + ')', 'ERROR')
)


links = [
  "#/"
]

x = pageName = _pageName = undefined
i = -1
casper.start homeUrl, ->
  # now x is an array of links
  x = links
  return

casper.then ->
  @each x, ->
    ++i
    @thenOpen (baseUrl + x[i]), ->
      @waitForSelector '.page--active', () ->
        @wait 1000 # wait for transition
        @capture "responses/#{responses["#{response}"].name}/"+@getTitle().replace(/\|/g, '-') + '.png'
        return
      return
    return
  return

casper.run()

