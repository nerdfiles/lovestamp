!function () {
    'use strict';

   	/*
    Create list of file to run in test.  Making sure that app_test.js is
    always the first to run
    */
    var firstFile, firstFileREGEXP = /interface\.js$/i,
        testFiles = [], testFilesREGEXP = /\.js$/i
    ;

    Object.keys(window.__karma__.files).forEach(function (file) {
        if (firstFileREGEXP.test(file)) {
            firstFile = file;
        } else if (testFilesREGEXP.test(file)) {
            testFiles.push(file);
        }
    });

    if (firstFile) {
        testFiles.unshift(firstFile);
    }

    require.config({
        baseUrl: '/Users/nerdfiles/Projects/lovestamp/app/scripts',

        paths: {

          async             : 'ext/async',
          font              : 'ext/font',
          goog              : 'ext/goog',
          image             : 'ext/image',
          json              : 'ext/json',
          noext             : 'ext/noext',
          mdown             : 'ext/mdown',
          propertyParser    : 'ext/propertyParser',
          markdownConverter : 'ext/Markdown.Converter',

          /**
           * Core Application Materials
           */
          'angular'                   : 'ext/angular',
          'angularAMD'                : 'ext/angularAMD',
          'ngload'                    : 'ext/ngload',

          /**
           * Visualization Dependency
           */
          'd3'                        : 'ext/d3',
          'c3'                        : 'ext/c3',

          /**
           * Interactive Maps Dependency
           */
          'leaflet'                   : 'ext/leaflet',
          'leaflet-tooltip'           : 'ext/leaflet.tooltip',
          'leaflet-awesome-markers'   : 'ext/leaflet.awesome-markers', // @TODO Decorate?
          'leaflet-osmbuildings'      : 'ext/OSMBuildings-Leaflet',

          /**
           * AngularJS Dependency
           */
          'angular-route'             : 'ext/angular-route',
          'hammer'                    : 'ext/hammer',
          'angular-local-storage'     : 'ext/angular-local-storage',
          'angular-leaflet-directive' : 'ext/angular-leaflet-directive',
          'angular-animate'           : 'ext/angular-animate',
          'angular-cookies'           : 'ext/angular-cookies',
          'angular-resource'          : 'ext/angular-resource',
          'angular-sanitize'          : 'ext/angular-sanitize',
          'angular-touch'             : 'ext/angular-touch',
          'angular-material'          : 'ext/angular-material',
          'angular-aria'              : 'ext/angular-aria',
          'angular-geolocation'       : 'ext/angularjs-geolocation.min',
          'angular-twitter'           : 'ext/ng-twitter', // for feeds, likely

          /**
           * Firebase Dependency
           */
          'firebase'                  : 'ext/firebase',
          'angular-fire'              : 'ext/angularfire',

          /**
           * Front End ORM
           *
           * @note Advised against using this Firebase. We call Firebase URLs which
           * load our objects, where BreezeJS expects to load objects into entity
           * managers.
           */
          'breeze'                    : 'ext/breeze.debug',
          'breeze-angular'            : 'ext/breeze.angular',
          'breeze-directives'         : 'ext/breeze.directives',

          /**
           * Application Front End Environment Configurations
           */
          'config'                    : 'config/base',

          /**
           * Native Application URIs
           */
          'routes'                    : 'routes',

          /**
           * Use-case Directives
           */
          'directives/search/simple' : './modules/www/directives/simple-search', // Shared across www module controllers.
          'directives/stats/static'  : './modules/www/directives/static-stats',  // Shared across www module controllers.

          /**
           * "Auth"
           *
           * Covers data marshalling and user authentication via Social Networks.
           */
          'auth/user/roll-up'        : './modules/authentication/directives/userRollup',
          'auth/access/conductor'    : './modules/authorization/directives/accessConductor',


          /**
           * Application-wide Directives
           */
          'ui/version'                   : './directives/appVersion',
          'ui/customEnter'               : './directives/customEnter',
          'ui/customLoader'              : './directives/customLoader',
          'ui/relExternal'               : './directives/relExternal',


          /**
           * Configuration Decorators
           */
          'config/exceptionHandler'      : './config/exceptionHandler',
          'config/frameBuster'           : './config/frameBuster',


          /**
           * Client-side Services
           */
          'services/stamp'               : './services/stamp',
          'services/crypto'              : './services/crypto',

          'utils/hammer'                 : './utils/hammer',
          'utils/firebase'               : './utils/firebase-generic',
          'utils/firebase/email'         : './utils/firebase-email',

          'plugins/sss/spin'             : './ext/spin',
          'plugins/sss/stampsdk'         : './ext/stampsdk',

          'jquery'                       : './ext/jquery.min',

          'crypto'                       : './ext/Crypto',
          'crypto.BlockModes'            : './ext/BlockModes',
          'crypto.AES'                   : './ext/AES',
          'crypto.PBKDF2'                : './ext/PBKDF2',
          'crypto.HMAC'                  : './ext/HMAC',
          'crypto.SHA1'                  : './ext/SHA1',
          'crypto.MD5'                   : './ext/MD5',

          'lodash'                       : './ext/lodash',

          'interface'                    : './interface'


        },

        shim: {

          'touchy': {
            deps: [
              'sss-client',
              'sss-util'
            ]
          },

          'jquery-json': {
            deps: ['jquery']
          },

          'hammer': {
            exports: 'Hammer'
          },

          'lodash': {
            exports: '_'
          },

          'angular-hammer': ['hammer'],

          'leaflet-providers': ['angular-leaflet-directive'],

          'services/crypto': ['crypto.AES'],

          'crypto.AES': {
            deps: [
              'crypto',
              'crypto.BlockModes',
              'crypto.PBKDF2',
              'crypto.HMAC',
              'crypto.SHA1',
              'crypto.MD5'
            ]
          },

          'leaflet': {
            'exports': 'L'
          },

          'breeze-angular': [
            'breeze',
            'angular'
          ],

          'angular-geolocation': [
            'angular'
          ],

          'angular-fire': [
            'firebase',
            'angular'
          ],

          'angular-aria': [
              'angular'
          ],

          'angular-material': [
            'angular',
            'angular-aria',
            'angular-animate',
            'utils/hammer'
          ],

          'angular-route': [
            'angular'
          ],

          'angular-local-storage': [
            'angular'
          ],

          'leaflet-osmbuildings': [
            'angular-leaflet-directive'
          ],

          'leaflet-awesome-markers': [
            'angular-leaflet-directive'
          ],

          'angular-leaflet-directive': [
            'leaflet',
            'angular'
          ],

          'angularAMD': [
            'angular'
          ],

          'ngload': [
            'angularAMD'
          ],

          'angular-animate': [
            'angular'
          ],

          'angular-cookies': [
            'angular'
          ],

          'angular-resource': [
            'angular'
          ],

          'angular-sanitize': [
            'angular'
          ],

          'angular-touch': [
            'angular'
          ],

          'interface': [
            'angular-leaflet-directive'
          ],

          'c3': [
            'd3'
          ]

        },

        deps: testFiles,

        callback: window.__karma__.start,

        waitSeconds: 20

    });
}();
