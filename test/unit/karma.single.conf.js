module.exports = function (config) {

  config.set({


    basePath: 'app',


    frameworks: [
      'jasmine',
      'requirejs'
    ],


    /**
     * @description
     *
     *     List of files / patterns to load in the browser
     *
     */
    files: [
      '../../../app/scripts/ext/*.js',
      '../../../app/scripts/plugins/snowshoestamp/spec/**/*.js',
      '../../../app/scripts/spec/*.js',
      '../boot.js'
      /*
       *'*.js',
       *'../test/*.js'
       */
    ],


    plugins: [
      'karma-requirejs',
      'karma-jasmine',
      'karma-chrome-launcher',
      'karma-phantomjs-launcher'
    ],


    exclude: [],


    /**
     * @options 'dots', 'progress', 'junit', 'growl', 'coverage'
     */
    reporters: ['progress'],


    // web server port
    port: 9876,


    // cli runner port
    runnerPort: 9100,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // @options karma.LOG_DISABLE || karma.LOG_ERROR || karma.LOG_WARN || karma.LOG_INFO || karma.LOG_DEBUG
    logLevel: config.LOG_DEBUG,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],


    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true,

    coverageReporter: {
      type: 'html',
      dir: 'coverage'
    }

  });

};
