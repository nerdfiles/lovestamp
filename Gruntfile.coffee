module.exports = (grunt) ->

  fs = require("fs")
  try
    q = require 'Q'
  catch e
    q = require 'q'
  path = require("path")

  gruntConfig = grunt.file.readJSON("Gruntconfig.json")
  packageConfig = grunt.file.readJSON("package.json")

  grunt.initConfig

    cvars: gruntConfig.configVars
    pkg: packageConfig

    githooks:
      all:
        #options:
          #template: '<%= cvars.app %>githooks/base'
        #'pre-commit': 'jshint test:unit'
        'post-merge':
          taskNames: 'bower:setup'
          #template: '<%= cvars.app %>/githooks/template'

    bower:
      setup:
        options:
          install: true
          copy: false


    protractor_webdriver:

      webDriverStart:
        options:
          keepAlive: true
          path: "node_modules/protractor/bin/"
          command: "webdriver-manager start"


    shell:

      openCodeAnalysisReport:
        command: "open http://localhost:8080"
        options:
          async: true

      analyzecss:
        command: "analyze-css -p --file <%= cvars.app %>/styles/compass/stylesheets/boot.css > ./reports/analyzecss.json"
        options:
          async: true

      coffeedoc:
        command: "cd app/scripts; coffeedoc . -o ../../docs/autodoc/ --parser requirejs; cd ../..;"
        options:
          async: true

      changetipAuth:
        command: 'open http://localhost:8080/changetip/oauth2/authentication'
        options:
          timeout: 5000
          async: true


    copy:

      setup:
        files: [
          {
            cwd     : "bower_components"
            expand  : true
            flatten : true
            dest    : "<%= cvars.app %>/<%= cvars.appjs %>/ext/utils/"
            src     : gruntConfig.asyncBowerFiles
          }
          {
            cwd     : "bower_components"
            expand  : true
            flatten : true
            dest    : "<%= cvars.app %>/<%= cvars.appjs %>/ext/"
            src     : gruntConfig.bowerFiles
          }
          {
            cwd     : "node_modules"
            expand  : true
            flatten : true
            dest    : "<%= cvars.app %>/<%= cvars.appjs %>/ext/"
            src     : gruntConfig.npmFiles
          }
          {
            cwd     : "bower_components"
            expand  : true
            flatten : true
            dest    : "<%= cvars.app %>/<%= cvars.appjs %>/ext/images"
            src     : gruntConfig.imageFiles
          }
          {
            cwd     : "<%= cvars.app %>/<%= cvars.appcss %>/compass"
            expand  : true
            flatten : false
            dest    : "<%= cvars.app %>/<%= cvars.appcss %>/ext/"
            src     : gruntConfig.cssFiles
          }

        ]

      version:
        files: [
          cwd    : "<%= cvars.base %>"
          expand : true
          dest   : "<%= cvars.app %>"
          src    : gruntConfig.versionFiles
        ]

      www:
        files: [
          cwd    : "<%= cvars.app %>"
          expand : true
          dest   : "<%= cvars.www %>"
          src    : gruntConfig.buildFiles
        ]

      deploy:
        files: [
          cwd    : "<%= cvars.www %>"
          expand : true
          dest   : "<%= cvars.dist %>"
          src    : [
            "css/**"
            "images/**"
            "sprites/**"
            "data/**"
          ]
        ]

    clean:

      options:
        force: true

      src: [
        "<%= cvars.app %>/**/*.js"
      ]
      www: [
        "<%= cvars.www %>/*"
        #"!<%= cvars.www %>/config.xml"
      ]
      deploy: [
        "<%= cvars.dist %>/*"
      ]
      sprites: [
        "<%= cvars.app %>/<%= cvars.sprites %>/sprite*"
      ]

    cssmin:

      www:
        files:
          "<%= cvars.www %>/<%= cvars.wwwcss %>/boot.css": ["<%= cvars.app %>/<%= cvars.appcss %>/compass/stylesheets/**/*.css"]
          #"<%= cvars.www %>/<%= cvars.appcss %>/boot.css": ["<%= cvars.app %>/<%= cvars.appcss %>/compass/stylesheets/**/*.css"]

    preprocess:

      www:
        src  : "<%= cvars.app %>/index.html"
        dest : "<%= cvars.www %>/index-enabled.html"
        options:
          context:
            WEB_EXPORT    : 'enabled'
            DEVICE_EXPORT : 'disabled'

      www_fan:
        src  : "<%= cvars.app %>/index-fan.html"
        dest : "<%= cvars.www %>/index-fan-enabled.html"
        options:
          context:
            WEB_EXPORT    : 'enabled'
            DEVICE_EXPORT : 'disabled'

      www_merchant:
        src  : "<%= cvars.app %>/index-merchant.html"
        dest : "<%= cvars.www %>/index-merchant-enabled.html"
        options:
          context:
            WEB_EXPORT    : 'enabled'
            DEVICE_EXPORT : 'disabled'

      prod_www:
        src  : "<%= cvars.app %>/index-prod.html"
        dest : "<%= cvars.www %>/index-prod-enabled.html"
        options:
          context:
            WEB_EXPORT    : 'enabled'
            DEVICE_EXPORT : 'disabled'

      prod_www_fan:
        src  : "<%= cvars.app %>/index-prod-fan.html"
        dest : "<%= cvars.www %>/index-prod-fan-enabled.html"
        options:
          context:
            WEB_EXPORT    : 'enabled'
            DEVICE_EXPORT : 'disabled'

      prod_www_merchant:
        src  : "<%= cvars.app %>/index-prod-merchant.html"
        dest : "<%= cvars.www %>/index-prod-merchant-enabled.html"
        options:
          context:
            WEB_EXPORT    : 'enabled'
            DEVICE_EXPORT : 'disabled'

      prod_device:
        src  : "<%= cvars.app %>/index-device.html"
        dest : "<%= cvars.www %>/index-device-enabled.html"
        options:
          context:
            WEB_EXPORT    : 'disabled'
            DEVICE_EXPORT : 'enabled'

      prod_device_android:
        src  : "<%= cvars.app %>/index-device-android.html"
        dest : "<%= cvars.www %>/index-device-android-enabled.html"
        options:
          context:
            WEB_EXPORT    : 'disabled'
            DEVICE_EXPORT : 'enabled'

      js_www:
        files:
          '<%= cvars.app %>/<%= cvars.appjs %>/loaders/base.js'   : '<%= cvars.app %>/<%= cvars.appjs %>/loaders/base.js'
          '<%= cvars.app %>/<%= cvars.appjs %>/loaders/device.js' : '<%= cvars.app %>/<%= cvars.appjs %>/loaders/device.js'
        options:
          inline: true
          context:
            DEVICE_EXPORT : 'disabled'
            WEB_EXPORT    : 'enabled' # implies HTML5 is disabled for AngularJS

      device:
        files:
          '<%= cvars.app %>/<%= cvars.appjs %>/loaders/base.js'   : '<%= cvars.app %>/<%= cvars.appjs %>/loaders/base.js'
          '<%= cvars.app %>/<%= cvars.appjs %>/loaders/device.js' : '<%= cvars.app %>/<%= cvars.appjs %>/loaders/device.js'
        options:
          inline: true
          context:
            DEVICE_EXPORT : 'enabled'
            WEB_EXPORT    : 'disabled' # implies HTML5 is disabled for AngularJS

    htmlmin:

      # @see https://github.com/yeoman/grunt-usemin/issues/44 for using 2 passes
      www:
        options:
          removeEmptyElements           : false # Cannot remove empty elements with angular directives
          collapseBooleanAttributes     : true
          collapseWhitespace            : true
          removeAttributeQuotes         : true
          removeComments                : true # Only if you don't use comment directives!
          removeEmptyAttributes         : true
          removeRedundantAttributes     : true
          removeScriptTypeAttributes    : true
          removeStyleLinkTypeAttributes : true

        files: [
          {
            "<%= cvars.www %>/index.min.html": "<%= cvars.www %>/index-enabled.html"
          }
          {
            "<%= cvars.www %>/index-prod.min.html": "<%= cvars.www %>/index-prod-enabled.html"
          }
          {
            "<%= cvars.www %>/index-device.min.html": "<%= cvars.www %>/index-device-enabled.html"
          }
          {
            "<%= cvars.www %>/index-device-android.min.html": "<%= cvars.www %>/index-device-android-enabled.html"
          }
          {
            cwd     : "<%= cvars.app %>/scripts/partials/"
            expand  : true
            flatten : false
            dest    : "<%= cvars.www %>/scripts/partials/"
            src     : ["*.html"]
          }
          {
            cwd     : "<%= cvars.app %>/scripts/components/snowshoestamp/partials/"
            expand  : true
            flatten : false
            dest    : "<%= cvars.www %>/scripts/components/snowshoestamp/partials/"
            src     : ["*.html"]
          }
          {
            cwd     : "<%= cvars.app %>/scripts/modules/www/controllers/"
            expand  : true
            flatten : false
            dest    : "<%= cvars.www %>/scripts/modules/www/controllers/"
            src     : ["**/partials/*.html"]
          }
          {
            cwd     : "<%= cvars.app %>/scripts/modules/"
            expand  : true
            flatten : false
            dest    : "<%= cvars.www %>/scripts/modules/"
            src     : ["**/partials/*.html"]
          }
        ]

      deploy:
        options:
          removeEmptyElements           : false # Cannot remove empty elements with angular directives
          collapseBooleanAttributes     : true
          collapseWhitespace            : true
          removeAttributeQuotes         : true
          removeComments                : true # Only if you don't use comment directives!
          removeEmptyAttributes         : true
          removeRedundantAttributes     : true
          removeScriptTypeAttributes    : true
          removeStyleLinkTypeAttributes : true

        files: [
          {
            "<%= cvars.dist %>/index.min.html": "<%= cvars.www %>/index-enabled.html"
          }
          {
            "<%= cvars.dist %>/index-prod.min.html": "<%= cvars.www %>/index-prod-enabled.html"
          }
          {
            "<%= cvars.dist %>/index-device.min.html": "<%= cvars.www %>/index-device-enabled.html"
          }
          {
            "<%= cvars.dist %>/index-device-android.min.html": "<%= cvars.www %>/index-device-android-enabled.html"
          }
          {
            cwd     : "<%= cvars.www %>/scripts/partials/"
            expand  : true
            flatten : false
            dest    : "<%= cvars.dist %>/scripts/partials/"
            src     : ["*.html"]
          }
          {
            cwd     : "<%= cvars.www %>/scripts/components/snowshoestamp/partials/"
            expand  : true
            flatten : false
            dest    : "<%= cvars.dist %>/scripts/components/snowshoestamp/partials/"
            src     : ["*.html"]
          }
          {
            cwd     : "<%= cvars.www %>/scripts/modules/www/controllers/"
            expand  : true
            flatten : false
            dest    : "<%= cvars.dist %>/scripts/modules/www/controllers/"
            src     : ["**/partials/*.html"]
          }
          {
            cwd     : "<%= cvars.www %>/scripts/modules/"
            expand  : true
            flatten : false
            dest    : "<%= cvars.dist %>/scripts/modules/"
            src     : ["**/partials/*.html"]
          }
        ]

    plato:
      srcAnalysis:
        jshint: grunt.file.readJSON("app/scripts/jshintrc.json")
        options: {}
        files:
          reports: [
            "<%= cvars.app %>/**/*.js"
            "<%= cvars.dev %>/**/*.js"
            "!<%= cvars.app %>/scripts/ext/*.js"
            "!<%= cvars.app %>/scripts/ext/utils/*.js"
            "!<%= cvars.app %>/styles/compass/javascripts/foundation/*.js"
          ]

    requirejs:

      # @note Using AMD with Android for the interim. But we should want to 
      # pass a compressed partials ecosystem as *-templates.js with a 
      # minified/uglified boot.coffee (now 'loaders/base' )machinated in 
      # R.js to produce a compressed.boot
      device:
        options:
          baseUrl                    : "<%= cvars.app %>/scripts"
          mainConfigFile             : "<%= cvars.app %>/scripts/loaders/base.js"
          removeCombined             : false
          findNestedDependencies     : true
          preserveLicenseComments    : false
          optimizeAllPluginResources : true
          optimize                   : 'uglify2'
          logLevel                   : 0
          name                       : "interface"
          out                        : './<%= cvars.device %>/scripts/loaders/base.js' # Single-file builds
          #dir                       : "<%= cvars.www %>/scripts/" # Multi-file builds
          #modules: [
            #{
              #name: "boot"
            #}
            #{
              #name: "modules/index/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/profile/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/admin/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/dashboard/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/social/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/stamp-signup/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/stamps/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/store/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/login/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/legal/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "components/snowshoestamp/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "components/snowshoestamp/controllers/error"
              #exclude: ["boot"]
            #}
            #{
              #name: "components/snowshoestamp/controllers/help"
              #exclude: ["boot"]
            #}
            #{
              #name: "components/snowshoestamp/controllers/success"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/notFound/controllers/base"
              #exclude: ["boot"]
            #}
          #]

      www:
        options:
          baseUrl                    : "<%= cvars.app %>/scripts"
          mainConfigFile             : "<%= cvars.app %>/scripts/loaders/base.js"
          removeCombined             : false
          findNestedDependencies     : true
          preserveLicenseComments    : false
          optimizeAllPluginResources : true
          optimize                   : 'uglify2'
          logLevel                   : 4
          name                       : "interface"
          out                        : './<%= cvars.www %>/scripts/loaders/base-compressed.js' # Single-file builds
          #dir                       : "<%= cvars.www %>/scripts/" # Multi-file builds
          #modules: [
            #{
              #name: "boot"
            #}
            #{
              #name: "modules/index/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/profile/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/admin/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/dashboard/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/social/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/stamp-signup/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/stamps/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/store/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/login/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/www/controllers/legal/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "components/snowshoestamp/controllers/base"
              #exclude: ["boot"]
            #}
            #{
              #name: "components/snowshoestamp/controllers/error"
              #exclude: ["boot"]
            #}
            #{
              #name: "components/snowshoestamp/controllers/help"
              #exclude: ["boot"]
            #}
            #{
              #name: "components/snowshoestamp/controllers/success"
              #exclude: ["boot"]
            #}
            #{
              #name: "modules/notFound/controllers/base"
              #exclude: ["boot"]
            #}
          #]

    uglify:

      deploy:
        options:
          preserveComments: "some"

        files: [
          cwd: "<%= cvars.www %>/js/"
          expand: true
          dest: "<%= cvars.dist %>/js/"
          src: [
            "*.js"
            "ext/require.js"
            "controller/*.js"
          ]
        ]

    csscss:

      prod:
        options:
          outputJson: true
        files:
            'reports/csscss.json': ['<%= cvars.app %>/styles/compass/stylesheets/boot.css']

      dev:
        src: ['<%= cvars.app %>/styles/compass/stylesheets/boot.css']

    uncss:

      dist:
        options:
          #ignore       : ['#added_at_runtime', /test\-[0-9]+/],
          #media        : ['(min-width: 700px) handheld and (orientation: landscape)'],
          #raw          : 'h1 { color: green }',
          #stylesheets  : ['lib/bootstrap/dist/css/bootstrap.css', 'src/public/css/main.css'],
          #ignoreSheets : [/fonts.googleapis/],
          #timeout      : 1000,
          #htmlroot     : 'public',
          #report       : 'min'
          ignore: [
              '.ng-scope'
              '.ng-move'
              '.ng-enter'
              '.ng-leave'
              '.chosen'
              '.created_by_jQuery'
          ]
        files: [{
          src: gruntConfig.viewFiles
          dest: '<%= cvars.app %>/styles/cleaned/boot.css'
        }]
      options: {
        csspath: '<%= cvars.app %>/styles/compass/stylesheets/'
      }

    csslint:
      strict:
        options:
          csslintrc: '.strict.csslintrc'
        src: ['<%= cvars.app %>/styles/compass/stylesheets/boot.css']
      lax:
        options:
          csslintrc: '.lax.csslintrc'
        src: ['<%= cvars.app %>/styles/compass/stylesheets/boot.css']

    jshint:
      www:
        options:
          jshintrc: "<%= cvars.app %>/scripts/jshintrc.json"

        files:
          src: [
            "<%= cvars.app %>/scripts/*.js"
            "!<%= cvars.app %>/scripts/templates/*-templates.js"
            "<%= cvars.app %>/scripts/controller/*.js"
            "<%= cvars.app %>/scripts/directive/*.js"
            "<%= cvars.app %>/scripts/provider/*.js"
          ]

    ngdocs:

      options:
        dest: 'docs/clients',
        scripts: ['<%= cvars.app %>/scripts/boot.js'],
        html5Mode: true,
        startPage: '/docs/clients',
        title: "LoveStamp Automated Documentation",
        image: "http://lovestamp.io:8080/images/email_signature.png",
        imageLink: "http://lovestamp.io",
        titleLink: "/docs/clients",
        bestMatch: true,
        analytics:
          account: 'UA-61100900-1',
          domainName: 'lovestamp.io'
        discussions:
          shortName: 'slack',
          url: 'https://lovestamp.slack.com',
          dev: false
      tutorial:
        src: [
          'content/onboarding/*.ngdoc'
        ],
        title: 'Onboarding'
      client:
        src: [
          '<%= cvars.app %>/scripts/**/*.coffee'
        ]
        title: 'Thin Client Documentation'

    jsdoc:
      api:
        src: [
          '<%= cvars.api %>/**/*.coffee'
        ],
        options:
          destination: 'docs/api'
      server:
        src: [
          '<%= cvars.dev %>/**/*.coffee'
        ]
        options:
          destination: 'docs/server'

    notify_hooks:
      options:
        enabled                  : true
        max_jshint_notifications : 5              # maximum number of notifications from jshint output
        title                    : "@project <%= pkg.name %>" # defaults to the name in package.json, or will use project directory's name
        success                  : true           # whether successful grunt executions should be notified automatically
        duration                 : 3              # the duration of notification in seconds, for `notify-send only

    notify:
      scss:
        options:
          title: 'SCSS Preprocess',
          message: 'SCSS is all gravy !'
      coffee:
        options:
          title: 'CoffeeScript Preprocess',
          message: 'Coffee is all good !'
      js:
        options:
          title: 'jshint Preprocess',
          message: () ->
            'JS is all good !'
        dist:
          options:
            title: 'Production Preprocess',
            message: 'Ready for production !'

    watch:

      options:
        nospawn: true
        livereload: 35731

      www:
        files: [
          "<%= cvars.app %>/styles/compass/sass/**/*.scss"
          "<%= cvars.app %>/scripts/modules/www/controllers/**/styles/*.scss"
          "<%= cvars.app %>/scripts/components/snowshoestamp/style/*.scss"
        ]

        tasks: [
          "compass:dev"
          "notify_hooks"
          "notify:scss"
        ]

      server:
        files: [
          "<%= cvars.dev %>/**/*.coffee"
        ]
        tasks: [
          "coffee"
          "notify_hooks"
          "notify:coffee"
        ]

      unit:
        files: ["<%= cvars.app %>/scripts/components/snowshoestamp/spec/*.coffee"]
        tasks: [
          "coffee"
          "karma:unit"
          "notify_hooks"
          "notify:coffee"
        ]

      e2eConfig:
        files: [
          "test/e2e/protractor.conf.coffee"
        ]
        tasks: [
          "coffee:e2eConfig"
        ]

      src:
        files: [
          "<%= cvars.app %>/**/*.coffee"
          "test/e2e/spec/modules/authentication/**/*.coffee"
          "test/e2e/spec/modules/index/**/*.coffee"
        ]
        tasks: [
          "coffee:glob_to_multiple"
          "coffee:spec"
          #"protractor_webdriver:webDriverStart"
          #"protractor:e2e_test"
          "shell:coffeedoc"
          "notify_hooks"
          "notify:coffee"
        ]

    crossdomainxml:
      dist:
        dest: "app/"
        siteControl: "master-only"
        allowAccessFrom: [
          {
            domain: "*"
          }
          {
            domain: "*.osmwwwings.org"
            secure: false
          }
          {
            domain: "*.tile.thunderforest.com"
            secure: false
          }
        ]

        #'toPorts': ['9865', '9000:9100']
        allowHttpRequestHeadersFrom: [
          {
            domain: "localhost:8089"
            secure: false
          }
          {
            domain: "*.osmwwwings.org"
            secure: false
          }
          {
            domain: "*.tile.thunderforest.com"
            secure: false
          }
        ]

    karma:
      unit:
        configFile: "test/unit/karma.conf.js"
        background: false

      unitSingle:
        configFile: "test/unit/karma.single.conf.js"

    coffee:

      glob_to_multiple:
        options:
          base: true
          sourceMap: true
        expand : true
        flatten : false
        cwd : "<%= cvars.app %>"
        dest : "<%= cvars.app %>"
        ext : ".js"
        src : [
          "**/*.coffee"
          "**/*.spec.coffee"
          "**/*.unit.coffee"
        ]

      dist:
        options:
          base: true
          sourceMap: true
        expand               : true
        flatten              : false
        cwd                  : "<%= cvars.app %>"
        dest                 : "<%= cvars.dist %>"
        ext                  : ".js"
        src                  : [
          "**/*.coffee"
          "**/*.spec.coffee"
          "**/*.unit.coffee"
        ]

      www:
        options:
          base: true
          sourceMap: true
        expand               : true
        flatten              : false
        cwd                  : "<%= cvars.app %>"
        dest                 : "<%= cvars.www %>"
        ext                  : ".js"
        src                  : [
          "**/*.coffee"
          "**/*.spec.coffee"
          "**/*.unit.coffee"
        ]

      e2eConfig:
        files:
          "test/e2e/protractor.conf.js": "test/e2e/protractor.conf.coffee"

      spec:
        options:
          base: false
          sourceMap: false
        expand : true
        flatten : false
        cwd : "test/e2e/spec"
        dest : "test/e2e/spec"
        ext : ".js"
        src : [
          "**/*.coffee"
          "**/**/*.coffee"
          "**/**/**/*.coffee"
        ]

      specUtils:
        options:
          base: false
          sourceMap: false
        expand : true
        flatten : false
        cwd : "test/e2e/utils"
        dest : "test/e2e/utils"
        ext : ".js"
        src : [
          "*.coffee"
        ]

      server:
        files:
          "server/base.js"                               : "server/base.coffee"
          "server/base-prod.js"                          : "server/base-prod.coffee"
          "server/base-prod-secure.js"                   : "server/base-prod-secure.coffee"
          "server/load-balancer.js"                      : "server/load-balancer.coffee"
          #"server/routes/api.js"                        : "server/routes/api.coffee" @deprecated
          #"server/routes/stamp.js"                      : "server/routes/stamp.coffee" @deprecated
          "server/routes/index.js"                       : "server/routes/index.coffee"
          "server/social/changeTip.js"                   : "server/social/changeTip.coffee"
          "server/social/google.js"                      : "server/social/google.coffee"
          "server/social/twitter.js"                     : "server/social/twitter.coffee"
          "server/social/facebook.js"                    : "server/social/facebook.coffee"
          "server/utils/impression.js"                   : "server/utils/impression.coffee"
          "server/utils/aggregateStampImpression.js"     : "server/utils/aggregateStampImpression.coffee"
          "server/utils/friendBatch.js"                  : "server/utils/friendBatch.coffee"
          "server/utils/push.js"                         : "server/utils/push.coffee"
          "server/utils/apn.js"                          : "server/utils/apn.coffee"
          "server/spec/endpoints.js"                     : "server/spec/endpoints.coffee"
          "server/oauth20.js"                            : "server/oauth20.coffee"
          "server/strategy.js"                           : "server/strategy.coffee"
          "server/utils/loggly.js"                       : "server/utils/loggly.coffee"
          "server/utils/topUser.js"                      : "server/utils/topUser.coffee"
          "server/utils/slack.js"                        : "server/utils/slack.coffee"
          "server/utils/stripe.js"                       : "server/utils/stripe.coffee"
          "server/utils/load-html-file.js"               : "server/utils/load-html-file.coffee"
          "server/endpoints/authentication/authorize.js" : "server/endpoints/authentication/authorize.coffee"
          "server/endpoints/authentication/changetip.js" : "server/endpoints/authentication/changetip.coffee"
          "server/endpoints/authentication/has-token.js" : "server/endpoints/authentication/has-token.coffee"
          "server/endpoints/authentication/sss.js"       : "server/endpoints/authentication/sss.coffee"
          "server/endpoints/economy/budget.js"           : "server/endpoints/economy/budget.coffee"
          "server/endpoints/economy/payment.js"          : "server/endpoints/economy/payment.coffee"
          "server/endpoints/economy/top-user.js"         : "server/endpoints/economy/top-user.coffee"
          "server/endpoints/mailer/base.js"              : "server/endpoints/mailer/base.coffee"
          "server/endpoints/search/es.js"                : "server/endpoints/search/es.coffee"
          "server/endpoints/social/connect.js"           : "server/endpoints/social/connect.coffee"
          "server/endpoints/stampministry/get-serial.js" : "server/endpoints/stampministry/get-serial.coffee"
          "server/endpoints/stampministry/moment.js"       : "server/endpoints/stampministry/moment.coffee"
          "server/endpoints/stampministry/purse.js"      : "server/endpoints/stampministry/purse.coffee"
          "server/endpoints/stampministry/repost.js"     : "server/endpoints/stampministry/repost.coffee"
          "server/endpoints/stampministry/request.js"    : "server/endpoints/stampministry/request.coffee"
          "server/endpoints/stampministry/user.js"       : "server/endpoints/stampministry/user.coffee"
          "server/endpoints/stampministry/validation.js" : "server/endpoints/stampministry/validation.coffee"
          "server/endpoints/near-here.js"                : "server/endpoints/near-here.coffee"
          "server/endpoints/notifications.js"             : "server/endpoints/notifications.coffee"
          "server/config/credentials.js"                  : "server/config/credentials.coffee"

    debug:
      options:
        open: true # open node-inspector in Chrome automatically 

    protractor:

      options:
        #configFile: "node_modules/protractor/referenceConf.js",
        keepAlive: true

      e2e_test:
        options:
          configFile: "<%= cvars.test %>/e2e/protractor.conf.js"

    compass:

      dist:
        options:
          sassDir: "<%= cvars.app %>/styles/compass/sass"
          cssDir: "<%= cvars.app %>/styles/compass/stylesheets"
          config: "<%= cvars.app %>/styles/compass/config.rb"
          environment: "production"
          importPath: [
            path.join(__dirname, "bower_components/foundation/scss")
            path.join(__dirname, "bower_components/organic-css/src")
            path.join(__dirname, "bower_components/organic-css/src/molecules")
          ]

      dev:
        options:
          sassDir: "<%= cvars.app %>/styles/compass/sass"
          cssDir: "<%= cvars.app %>/styles/compass/stylesheets"
          config: "<%= cvars.app %>/styles/compass/config.rb"
          importPath: [
            path.join(__dirname, "bower_components/foundation/scss")
            path.join(__dirname, "bower_components/organic-css/src")
            path.join(__dirname, "bower_components/organic-css/src/molecules")
          ]

    sprite:

      all:
        engine: "gmsmith"
        src: "./app/sprites/*.png"
        dest: "./app/sprites/spritesheet.png"
        destCss: "./app/sprites/sprites.css"

      prod:
        engine: "gmsmith"
        src: "./app/sprites/*.png"
        dest: "./www/sprites/spritesheet.png"
        destCss: "./www/sprites/sprites.css"

    cordovacli:

      options:
        path : '<%= cvars.www %>'
        cli  : 'cordova'

      cordova:
        options:
          path      : '<%= cvars.dist %>'
          id        : 'io.cordova.LoveStamp'
          name      : 'LoveStamp'
          command   : [
            #'create'
            #'platform'
            #'plugin'
            'build'
          ]
          platforms : [
            'ios'
            'android'
          ]
          plugins: [
              'cordova-plugin-transport-security'
              'cordova-plugin-battery-status'
              'cordova-plugin-console'
              'cordova-plugin-contacts'
              'cordova-plugin-device'
              'cordova-plugin-device-motion'
              'cordova-plugin-device-orientation'
              'cordova-plugin-dialogs'
              'cordova-plugin-geolocation'
              'cordova-plugin-globalization'
              'cordova-plugin-inappbrowser'
              'cordova-plugin-network-information'
              'cordova-plugin-splashscreen'
              'cordova-plugin-push-notification'
              'cordova-plugin-android-support-v4'
              'cordova-plugin-android-support-v7-appcompat'
              #'cordova-plugin-camera'
              #'cordova-plugin-file'
              #'cordova-plugin-media'
              #'cordova-plugin-media-capture'
              #'cordova-plugin-vibration'
              #'io.litehelpers.cordova.sqlite'
              #'cordova-plugin-whitelist'
          ]

      create:
        options:
          command : 'create'
          id        : 'io.cordova.LoveStamp'
          name      : 'LoveStamp'

      add_platforms:
        options:
          command   : 'platform'
          action    : 'add'
          platforms : ['ios', 'android']

      rm_platforms:
        options:
          command   : 'platform'
          action    : 'rm'
          platforms : ['ios', 'android']

      rm_plugins:
        options:
          command : 'plugin'
          action  : 'rm'
          plugins: [
              'cordova-plugin-transport-security'
              'cordova-plugin-battery-status'
              'com.jamiestarke.webviewdebug'
              'cordova-plugin-console'
              #'cordova-plugin-file'
              #'cordova-plugin-contacts'
              'cordova-plugin-device'
              #'org.apache.cordova.device'
              #'cordova-plugin-device-motion'
              #'cordova-plugin-device-orientation'
              'cordova-plugin-dialogs'
              'cordova-plugin-geolocation'
              #'cordova-plugin-globalization'
              'cordova-plugin-inappbrowser'
              'cordova-plugin-network-information'
              'cordova-plugin-splashscreen'
              #'cordova-plugin-push-notification'
              'cordova-plugin-android-support-v4'
              'cordova-plugin-android-support-v7-appcompat'
              #'de.appplant.cordova.plugin.background-mode'
              'cordova-plugin-background-mode'
              #'org.transistorsoft.cordova.background-geolocation'
              'cordova-plugin-background-geolocation'
              #'de.appplant.cordova.plugin.local-notification'
              'cordova-plugin-local-notifications'
              'cordova-plugin-crosswalk-webview'
              #'cordova-plugin-camera'
              'cordova-plugin-file'
              'cordova-plugin-media'
              #'cordova-plugin-media-capture'
              #'cordova-plugin-vibration'
              #'io.litehelpers.cordova.sqlite'
              'cordova-plugin-whitelist'
          ]

      add_plugins:
        options:
          command : 'plugin'
          action  : 'add'
          plugins: [
              'cordova-plugin-transport-security'
              #'com.jamiestarke.webviewdebug'
              'cordova-plugin-battery-status'
              'cordova-plugin-console'
              'cordova-plugin-contacts'
              'cordova-plugin-device'
              'cordova-plugin-dialogs'
              'cordova-plugin-geolocation'
              'cordova-plugin-globalization'
              'cordova-plugin-inappbrowser'
              'cordova-plugin-network-information'
              'cordova-plugin-splashscreen'
              'cordova-plugin-push-notification'
              'cordova-plugin-android-support-v4'
              'cordova-plugin-android-support-v7-appcompat'
              'https://github.com/katzer/cordova-plugin-background-mode.git'
              'cordova-plugin-background-geolocation'
              'cordova-plugin-local-notifications'
              'cordova-plugin-crosswalk-webview'
              'cordova-plugin-file'
              'cordova-plugin-media'
              'cordova-plugin-whitelist'
          ]

      all_plugins:
        options:
          command : 'plugin'
          action  : 'add'
          plugins: [
              'cordova-plugin-transport-security'
              #'com.jamiestarke.webviewdebug'
              'cordova-plugin-battery-status'
              'cordova-plugin-console'
              'cordova-plugin-contacts'
              'cordova-plugin-device'
              'cordova-plugin-dialogs'
              'cordova-plugin-geolocation'
              'cordova-plugin-globalization'
              'cordova-plugin-inappbrowser'
              'cordova-plugin-network-information'
              'cordova-plugin-splashscreen'
              'cordova-plugin-push-notification'
              'cordova-plugin-android-support-v4'
              'cordova-plugin-android-support-v7-appcompat'
              'https://github.com/katzer/cordova-plugin-background-mode.git'
              'cordova-plugin-background-geolocation'
              'cordova-plugin-local-notifications'
              'cordova-plugin-crosswalk-webview'
              'cordova-plugin-file'
              'cordova-plugin-media'
              'cordova-plugin-whitelist'
          ]

      remove_plugin:
        options:
          command : 'plugin'
          action  : 'rm'
          plugins : [
              'cordova-plugin-battery-status'
          ]

      build_ios:
        options:
          command   : 'build'
          platforms : ['ios']

      build_android:
        options:
          command   : 'build'
          platforms : ['android']
          args      : [
            #'--release'
            '--debug'
          ]

      emulate_ios:
        options:
          command   : 'emulate'
          platforms : ['ios']

      emulate_android_default19:
        options:
          command   : 'emulate'
          platforms : ['android']
          args      : [
            '--target'
            'Default19'
            '--debug'
          ]

      emulate_android_test10:
        options:
          command   : 'emulate'
          platforms : ['android']
          args      : [
            '--target'
            'Test10'
            '--debug'
          ]

      emulate_android_gs6:
        options:
          command   : 'emulate'
          platforms : ['android']
          args      : [
            '--target'
            'g--s6'
          ]

      emulate_android_444:
        options:
          command  : 'emulate'
          platforms : ['android']
          args       : [
            '--target'
            'GalaxyS5Android444'
            '--debug'
          ]

      emulate_android_N5G:
        options:
          command  : 'emulate'
          platforms : ['android']
          args       : [
            '--target'
            'N5G'
            #'-partition-size'
            #'1024'
          ]

      emulate_android_551:
        options:
          command  : 'emulate'
          platforms : ['android']
          args       : [
            '--target'
            'test_block'
            #'-partition-size'
            #'1024'
          ]

      emulate_android_442:
        options:
          command   : 'emulate'
          platforms : ['android']
          args      : [
            '--target'
            'Working-LoveStamp'
          ]

    jscrambler:
      obfuscation:
        files: [{
          expand: true,
          src: [
            #'<%= cvars.dist %>/lovestamp.js'
            #'<%= cvars.app %>/<%= cvars.appjs %>/*.js'
            './<%= cvars.device %>/scripts/loaders/base.js'
          ]
          dest: 'output/'
          ext: '.min.js'
        }],
        options:
          keys:
            accessKey: 'F7E87B5C854EA4D38C6CB8343CCB1B04A0FA0EA9'
            secretKey: '35009F13E50888B83DDF2A4F300FAF29378D62C8'
          params:
            string_splitting         : '%DEFAULT%'
            function_reorder         : '%DEFAULT%'
            function_outlining       : '%DEFAULT%'
            dot_notation_elimination : '%DEFAULT%'
            expiration_date          : '2199-01-01'
            rename_local             : '%DEFAULT%'
            whitespace               : '%DEFAULT%'
            literal_duplicates       : '%DEFAULT%'

    #jscrambler:
      #main:
        #options:
          #keys:
            #accessKey: '',
            #secretKey: ''
          #params:
            #rename_local : '%DEFAULT%'
            #whitespace   : '%DEFAULT%'
        #files: [
          #{
            #src: [
              #'<%= cvars.dist %>/lovestamp.js'
            #]
            #dest: 'output/'
          #}
        #]

    bump:
      options:
        updateConfigs      : [],
        commit             : true,
        commitMessage      : 'Release v%VERSION%',
        commitFiles        : ['package.json'],
        createTag          : true,
        tagName            : 'v%VERSION%',
        tagMessage         : 'Version %VERSION%',
        push               : false,
        gitDescribeOptions : '--tags --always --abbrev=1 --dirty=-d',
        globalReplace      : false,
        prereleaseName     : false,
        regExp             : false
        files : [
          'package.json'
          'app/package.json'
          'bower.json'
        ]

    ngtemplates:
      controller_modules :
        src  : '<%= cvars.app %>/<%= cvars.appjs %>/modules/www/controllers/**/partials/**.html'
        dest : '<%= cvars.app %>/<%= cvars.appjs %>/templates/controller--modules-templates.js'
        options :
          htmlmin :  '<%= htmlmin.www %>'
      www_modules :
        src  : '<%= cvars.app %>/<%= cvars.appjs %>/modules/www/partials/**.html'
        dest : '<%= cvars.app %>/<%= cvars.appjs %>/templates/www--modules-templates.js'
        options :
          htmlmin :  '<%= htmlmin.www %>'
      modules :
        src  : '<%= cvars.app %>/<%= cvars.appjs %>/modules/**/partials/**.html'
        dest : '<%= cvars.app %>/<%= cvars.appjs %>/templates/modules-templates.js'
        options :
          htmlmin :  '<%= htmlmin.www %>'
      components :
        src  : '<%= cvars.app %>/<%= cvars.appjs %>/components/**/partials/**.html'
        dest : '<%= cvars.app %>/<%= cvars.appjs %>/templates/components-templates.js'
        options :
          htmlmin :  '<%= htmlmin.www %>'
      partials :
        src  : '<%= cvars.app %>/<%= cvars.appjs %>/partials/**.html'
        dest : '<%= cvars.app %>/<%= cvars.appjs %>/templates/partials-templates.js'
        options :
          htmlmin :  '<%= htmlmin.www %>'

    sitemap:
      dist:
        pattern: [
          '<%= cvars.app %>/<%= cvars.appjs &>/partials/*.html',
        ],
        siteRoot: '<%= cvars.app %>'

    changelog:
      releaseNotes:
        options:
          dest: 'release-notes/<%= pkg.version %>.txt',
          partials:
            features: '{{#each features}}{{> feature}}{{/each}}',
            feature: '[NEW] {{this}}\n',
            fixes: '{{#each fixes}}{{> fix}}{{/each}}',
            fix: '[FIX] {{this}}\n'
      changeLog:
        options:
          dest: 'changelog/<%= pkg.version %>.txt',
          logArguments: [
            '--pretty=* %h - %ad: %s',
            '--no-merges',
            '--date=short'
          ],
          template: '{{> features}}',
          featureRegex: /^(.*)$/gim,
          partials:
            features: '{{#if features}}{{#each features}}{{> feature}}{{/each}}{{else}}{{> empty}}{{/if}}\n',
            feature: '- {{this}} {{this.date}}\n'

    conventionalChangelog:
      options:
        pkg:
          path: '<%= cvars.app %>/package.json'
        changelogOpts:
          preset       : 'angular'
          releaseCount : 5
          append       : true
        context: {}
        gitRawCommitsOpts: {}
        parserOpts: {}
        writerOpts: {}
      release:
        src: 'CHANGELOG.md'

    net:
      production:
        host: 'lovestamp.io'
        port: 5004
        tasks: ['gitpull']

    gitpull:
      production:
        options:
          remote: 'origin'
          branch: 'master'

    mochaRunner:
      all:
        src    : ['<%= cvars.api %>/base.js']
        spec   : ['<%= cvars.api %>/spec/endpoints.js']
        #styles : ['styles/*.css']

    express:

      options: {}

      dev:
        options:
          port: gruntConfig.configVars.port
          hostname: "0.0.0.0"
          server: path.resolve("./server/base.js")

      #ssl:
        #options:
          #port: 443,
          #hostname: '0.0.0.0',
          #key: fs.readFileSync('./server/privatekey.pem', 'utf8'),
          #cert: fs.readFileSync('./server/certificate.pem', 'utf8'),
          #server: path.resolve('./server/base.js')
      #livereload:
        #options:
          #server: path.resolve('./server/base'),
          #livereload: true,
          #serverreload: false,
          #bases: [path.resolve('./.tmp'), path.resolve(__dirname, gruntConfig.configVars.app)]
      #test:
        #options:
          #server: path.resolve('./server/base'),
          #bases: [path.resolve('./.tmp'), path.resolve(__dirname, 'test')]
      #dist:
        #options:
          #server: path.resolve('./server/base'),
          #bases: path.resolve(__dirname, gruntConfig.configVars.dist)

  require("load-grunt-tasks") grunt
  grunt.loadNpmTasks('grunt-angular-templates')
  #grunt.loadNpmTasks('grunt-net')
  #grunt.loadNpmTasks('grunt-git')

  grunt.registerTask "setup", [
    "bower:setup"
    "copy:setup"
  ]

  grunt.registerTask "devel", [
    "copy:version"
    "coffee:specUtils"
    "coffee:spec"
    "express"
    'shell:changetipAuth'
    "watch"
  ]

  grunt.registerTask "www", [
    "jshint:www"
    "clean:www"
    "preprocess:www"
    "preprocess:prod_www"
    #"preprocess:prod_device"
    "htmlmin:www"
    "ngtemplates"
    "compass"
    "cssmin:www"
    #"csslint"
    "coffee:glob_to_multiple"
    "coffee:dist"
    "preprocess:js_www"
    "requirejs:www"
    "copy:www"
  ]

  grunt.registerTask "deploy", [
    "jshint:www"
    "clean:deploy"
    "htmlmin:deploy"
    "compass"
    "csslint"
    "uglify:deploy"
    "copy:deploy"
  ]

  grunt.registerTask "docs", [
    "ngdocs"
    "jsdoc"
  ]

  grunt.registerTask "docs/code", [
    "shell:coffeedoc"
    #"watch:coffeedoc"
  ]

  grunt.registerTask "codeAnalysis", ["plato"]

  grunt.registerTask "cssAnalysis", [
    "csslint"
    "csscss"
    "shell:analyzecss"
  ]

  grunt.registerTask "e2e", [
    #"express"
    #'shell:webDriverStart',
    "coffee:specUtils"
    "coffee:spec"
    "protractor_webdriver:webDriverStart"
    "protractor"
  ]

  grunt.registerTask "unitTest", ["karma:unit"]
  grunt.registerTask "unitTestApi", ["mochaRunner:all"]
  grunt.registerTask "unitTestSingle", ["karma:unitSingle"]

  grunt.registerTask "build/sprite", [
    "clean:sprites"
    "sprite"
  ]

  grunt.registerTask 'build/web', ['requirejs:www']

  grunt.registerTask 'templatize', [
    'jshint'
    'ngtemplates'
    'htmlmin'
  ]

  grunt.registerTask "add_platforms", [
    "cordovacli:add_platforms"
  ]

  grunt.registerTask "rm_platforms", [
    "cordovacli:rm_platforms"
  ]

  grunt.registerTask "add_plugins", [
    "cordovacli:add_plugins"
  ]

  grunt.registerTask "rm_plugins", [
    "cordovacli:rm_plugins"
  ]

  grunt.registerTask "all_plugins", [
    "cordovacli:all_plugins"
  ]

  grunt.registerTask "ios/add_plugins", [
    "cordovacli:add_plugins"
  ]

  grunt.registerTask "android/add_plugins", [
    "cordovacli:add_plugins"
  ]

  grunt.registerTask "ios/build", [
    "jshint:www"
    "clean:www"
    "preprocess:www"
    "preprocess:prod_www"
    "preprocess:prod_device"
    "preprocess:prod_device_android"
    "htmlmin:www"
    "ngtemplates"
    "compass"
    "cssmin:www"
    #"csslint"
    "coffee:www"
    "preprocess:device"
    "copy:www"
    "clean:deploy"
    "cordovacli:build_ios"
  ]

  grunt.registerTask "android/build", [
    "jshint:www"
    "clean:www"
    "preprocess:www"
    "preprocess:prod_www"
    "preprocess:prod_device"
    "preprocess:prod_device_android"
    "htmlmin:www"
    "ngtemplates"
    "compass"
    "cssmin:www"
    #"csslint"
    "coffee:www"
    "preprocess:device"
    "copy:www"
    "clean:deploy"
    "cordovacli:build_android"
  ]

  grunt.registerTask "ios/emu", [
    "cordovacli:emulate_ios"
  ]

  grunt.registerTask 'emulate/android/default', [
    'cordovacli:emulate_android_default19'
  ]

  grunt.registerTask 'emulate/android/test10', [
    'cordovacli:emulate_android_test10'
  ]

  grunt.registerTask "android/emu/android442", [
    "cordovacli:emulate_android_442"
  ]

  grunt.registerTask "android/emu/android444", [
    "cordovacli:emulate_android_444"
  ]

  grunt.registerTask "android/emu/android551", [
    "cordovacli:emulate_android_551"
  ]

  grunt.registerTask "android/emu/N5G", [
    "cordovacli:emulate_android_N5G"
  ]

  grunt.registerTask "android/emu/gs6", [
    "cordovacli:emulate_android_gs6"
  ]

  grunt.registerTask "changetip/auth", [
    'shell:changetipAuth'
  ]

  grunt.registerTask 'put/out', [
    'jscrambler'
  ]

  grunt.registerTask 'net/production', [
    'net:production'
  ]

  return
