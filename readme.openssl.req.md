openssl req -new -key mykey.key -out CertificateSigningRequest.certSigningRequest -subj "/emailAddress=hello@nerdfiles.net, CN=nerdfiles, C=US" 

http://developer.android.com/tools/publishing/app-signing.html

keytool -genkey -v -keystore my-release-key.keystore -alias LoveStamp -keyalg RSA -keysize 2048 -validity 10000

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore LoveStamp-release.apk LoveStamp

jarsigner -verify -verbose -certs LoveStamp-release.apk

zipalign -v 4 your_project_name-unaligned.apk your_project_name.apk
zipalign -v 4 LoveStamp-release.apk LoveStamp-release.aligned.apk