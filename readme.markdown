# lovestamp

An app for enabling merchants to show customers love; a tipping compass for a 
new gamification platform that 'pataphorizes[0] consumers’ economic realities.

---
[0]: http://www.pataphor.com/whatisapataphor.html

## Documentation

1. [API](http://lovestamp.io/docs/api)
2. [Clients](http://lovestamp.io/docs/clients)
3. [Server](http://lovestamp.io/docs/server)
4. [Code](http://lovestamp.io/docs/code)

## SnowShoe Development Config

    9da8da12326263876b34
    App Secret

    8d07fd8a2af42de5f669587d79b41e969b6bf8ef
    Stamp Screen

    http://beta.snowshoestamp.com/applications/client/9da8da12326263876b34/
    Debug Callback

    http://beta.snowshoestamp.com/applications/client/9da8da12326263876b34/callback/
    Serial Number Identifier

    http://beta.snowshoestamp.com/applications/application/869/stamp_test/

## Code Inspiration

1. http://codepen.io/saadeghi/pen/IBhfJ

## Docs Generation

    $ cd ./app/scripts/
    $ coffeedoc . -o ../../docs/autodoc/ --parser requirejs

## Ideas

1. Implement http://schema.org/Demand over <leafleft> directive tooltips: http://aratcliffe.github.io/Leaflet.tooltip/examples/index.html from 
   Mixpanel or http://www.alchemyapi.com/api/sentiment/htmlc.html Sentiment APIs which map to ListViews of tipping behavior classifications.

## Graded Browser Support List

### Automated Testing

1. See https://saucelabs.com/ which integrates nicely with Protractor.

### Cordova/PhoneGap Building

Essentially it is possible to build without XCode.

1. See https://github.com/calvinl/ng-phonegap (updated 24 days ago).

### Mobile and Tablet Browsers

#### Mobile

##### Samsung

    Galaxy S5 1080x19204.4
    Galaxy S4 1080x19204.3
    Galaxy S3 720x12804.1
    Galaxy S2 480x8002.3
    Galaxy S 480x8002.2
    Galaxy Note 3 1080x19204.3
    Galaxy Note 2 720x12804.1
    Galaxy Note 800x12802.3
    Galaxy S5 Mini 720x12804.4

##### Google

    * Nexus 5 1080x19205
    Nexus 4 768x12804.2
    Nexus 720x12804

##### HTC

    One M8 1080x19204.4
    One X 720x12804
    Wildfire 240x3202.2
    Hero 320x4801.5

##### Motorola

    Droid Razr 540x9602.3
    Razr 540x9604
    Razr Maxx HD 720x12804.1

##### Sony

    Xperia X10 480x8541.6
    Xperia Tipo 320x4804

#### Tablet

##### Amazon

    * Kindle Fire HDX 7 1200x19204.3
    * Kindle Fire 2 600x10244
    * Kindle Fire HD 8.9 1200x19204

##### Google

    Nexus 7 1280x8004.1

##### Samsung

    Galaxy Tab 4 10.1 1280x8004.4
    Galaxy Note 10.1 800x12804
    Galaxy Tab 2 10.1 1280x8004

### Desktop Browsers

1. IE7-8
2. Chrome (latest, obviously)
3. Firefox (20+)
4. Opera (9+)


### Passwords

Internal passwords for configuration management and development planning.

admin / IzeIoK4rRVVMJg==
prefix: CbdpiPJLh235OA

wpadmin: admin
wppass: Q8paRjwrS4h4Zw==

user: adam
pass: 8a4TD6rvYF7kXg==

servermail db:
mailadmin / rPUZ/IC9H4VTXA==

    Test Secret Key: sk_test_mlrAVVdnfdybEDPxydKQWH4R
    Test Publishable Key: pk_test_IngHIKiNxnpwobugse6bqqOP
    Live Secret Key: sk_live_42u3CperMA67GpjIA6QkRtI1
    Live Publishable Key: pk_live_D6VVTjIEY5uJ1ca3CRoB6V6L

NonCT Users can be directed to here to login via whatever channel they logged in via within our app.
1. https://www.changetip.com/login/{{channel}}/
1.1. New Window
2. Public/Private on Customer UIs > Replies/PM
3. Nested Accounts { channels_array }
4. Sound mapping to events (harmonize)
5. (tentative) CasperJS OAuth Flow on ChangeTip LS Profile for balance
5.1. http://blog.oauth.io/casperjs-automated-testing-oauth-flow/
6. Stripe payment
7. Admin
8. Rolling counter on tip display

### ChangeTip Examples

    880bbc3f1d29a678a4c4d184c737a563ddfb3f09 

    curl https://api.changetip.com/v1/tips/?api_key=880bbc3f1d29a678a4c4d184c737a563ddfb3f09 \
          -H "Content-Type: application/json" \
          -d '{"sender":"filesofnerds","receiver":"RattelyrDragon","context_uid":"76agd8w3","message":"50 bits","channel":"twitter"}'

    curl 'https://api.changetip.com/v1/tips/?tips=a643931,38a5a4c01&channel=twitter&api_key=880bbc3f1d29a678a4c4d184c737a563ddfb3f09'

    curl 'https://api.changetip.com/v1/tips/?tips=&channel=twitter&api_key=880bbc3f1d29a678a4c4d184c737a563ddfb3f09'

## Analytics

### Mixpanel

    <!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
    for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
    mixpanel.init("56d0bf7a2c760cdaafb3a76f8813514a");</script><!-- end Mixpanel -->

## Needful

$ sudo apt-get install php5-gd && sudo service apache2 restart
$ sudo apt-get install libcurl3-dev

### Deployments

1. Check ``./device/`` or ``./platforms`` or ``./plugins`` to nose dive in.

GruntJS provides recipes of tasks to deploy and build to native clients 
using Cordova, or for JScrambler.

Try:

    $ npm install cordova-lib
    $ npm install grunt-cordovacli --save-dev
    $ npm install
    $ cordova build # Will build to iOS and Android.

Or:

    $ npm install
    $ npm install -g grunt-cli
    $ grunt requirejs

### Performance Testing

Follow https://mazira.com/blog/introduction-load-balancing-nodejs for guidance 
on horizontal scaling through load balancing.

We’re using jmeter via [grunt-jmeter](https://github.com/pcw216/grunt-jmeter) to measure app weight/performance against network 
availability reports at http://opensignal.com metrics. But we need to modify 
the ``grunt-jmeter`` package to download the correct version, which is 2.13.

And then some patching is necessary to get the symlinks right for local builds.

    cd $HOME/Projects/lovestamp/node_modules/grunt-jmeter

Then modify ``package.json``:

    "jmeterVersions": {
      "jmeter": "2.13"
    }

Then:

    cd $HOME/Projects/lovestamp/node_modules/grunt-jmeter/bin

Amend the ``jmeter`` runner according to the following:

    #!/bin/bash
    #BINARY=`readlink ${BASH_SOURCE[0]}` # resolve the node_modules/.bin link
    #DIR="$( cd "$( dirname "$BINARY" )" && pwd )" # Identify the grunt-jmeter bin directory
    #$DIR/../jmeter/apache-jmeter-2.13/bin/jmeter "$@" # Run the distro jmeter
    export DIR=$HOME/Projects/lovestamp/node_modules/grunt-jmeter/jmeter
    $DIR/apache-jmeter-2.13/bin/jmeter "$@" # Run the distro jmeter

### Signing

    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore LoveStamp-Production-release-key.keystore /Users/nerdfiles/Projects/lovestamp/platforms/android/build/outputs/apk/android-armv7-release-unsigned.apk LoveStamp-Production
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD

=======
>>>>>>> df58881e2319bdeecc366c48dacdd81cbe356bdd
=======

>>>>>>> f95968dcb4c4b5b2aefe888b74e2a0f58a0fa5cd
=======
>>>>>>> df58881e2319bdeecc366c48dacdd81cbe356bdd
    Password: 
    t0d4y1sth3d4y!
    Alias:
    LoveStamp-Production

    ~/Downloads/adt-bundle-mac-x86_64-20140702/sdk/build-tools/22.0.1/zipalign -v 4 /Users/nerdfiles/Projects/lovestamp/platforms/android/build/outputs/apk/android-armv7-release-unsigned.apk /Users/nerdfiles/Projects/lovestamp/platforms/android/build/outputs/apk/android-armv7-release-signed.apk

<<<<<<< HEAD
#### Refresh

    rm -rf /Users/nerdfiles/Projects/lovestamp/platforms/android/build/outputs/apk/android-armv7-release-unsigned.apk /Users/nerdfiles/Projects/lovestamp/platforms/android/build/outputs/apk/android-armv7-release-signed.apk

=======
>>>>>>> df58881e2319bdeecc366c48dacdd81cbe356bdd
### Workflows

    $ grunt devel

    $ grunt coffee && grunt compass

    $ grunt ios/buid

    $ grunt android/build

    $ grunt all_plugins

<<<<<<< HEAD
### Server Setup for nginx

    sudo apt-get install build-essential zlib1g-dev libssl-dev libreadline5-dev
    sudo apt-get install make binutils autoconf automake autotools-dev libtool pkg-config zlib1g-dev libcunit1-dev libssl-dev libxml2-dev libev-dev libevent-dev libjansson-dev libjemalloc-dev python3.4-dev g++ g++-mingw-w64-i686 git python3-setuptools
    sudo apt-get install libpcre3 libpcre3-dev

    wget -O - http://nginx.org/keys/nginx_signing.key | sudo apt-key add -
    echo "deb-src http://nginx.org/packages/mainline/ubuntu/ trusty nginx" | sudo tee -a /etc/apt/sources.list
    echo "deb http://nginx.org/packages/mainline/ubuntu/ trusty nginx" | sudo tee -a /etc/apt/sources.list

    apt-get clean && apt-get install nginx

    apt-get upgrade nginx

### CipherSuite

    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK;
    ssl_ciphers EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS !RC4;

    # HSTS
    add_header Strict-Transport-Security "max-age=31536000; includeSubdomains";

### Compiling nginx on the Spot

    sudo ./configure --prefix=/opt/nginx --user=nginx --group=nginx --with-http_ssl_module

### Just In Case

    sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib

=======
>>>>>>> df58881e2319bdeecc366c48dacdd81cbe356bdd
