# LoveStamp Application

LoveStamp is your guide to businesses that appreciate you. They stamp your 
phone, you get “bits”. Use these via ChangeTip to tip others, access digital 
content, get items from around the world and much more.

Requirements: You need:
[1] a Twitter, Google+ or Facebook account to sign up,  
[2] to enable location services,  
[3] Internet connectivity.

## Environment and Build Configuration

We use Grunt[0]. If you cannot access one of the URLs provided below, then 
find the relevant registered Grunt task from our ``../Gruntfile.coffee``, or 
the relevant file from our ``../Gruntconfig.json`` to determine its availability.

## Internal Entry Points

Our [Server Index](http://lovestamp.io:8080/docs/server/).

## API Endpoints (REST-like)

Our [API Index](http://data.lovestamp.io/docs/api/) description.

Also see ./scripts/config/api.md.

## Documentation

### Source

Our [CoffeeDoc](http://data.lovestamp.io/docs/code/) entry point.

### Devices

Our Configurable Front End Environment Specification is located 

    ./scripts/config/README.md

## Code Analysis

Our [Plato](http://data.lovestamp.io:8080/docs/reports/) entry point.

## Bug Tracking

Our [Redmine](http://lovestamp.io:8082/) entry point.

## Security

1. See ./scripts/config/frameBuster.coffee.

## Front End

The LoveStamp front end should be littered throughout first-tier 
abstractions, like ``./scripts/components/`` or ``./scripts/directives/``. 
Modules may or may not require design specification; but one thing 
is clear: SCSS is a meta-language, not an object language, like CSS.

### Implementation

Check out ./styles/compass/sass/. LoveStamp’s front end is a demonstration 
of Organic CSS, a superset of the Generic BEM (block-element-modifer) paradigm.

Have fun with that[1].

### Problem-set

    There are only two hard problems in computer science: cache invalidation, 
    naming things, and off-by-one errors.

LoveStamp has an opinion about CSS, and it attempts to solve it, by first 
assuming that CSS is more like assessmbly languages than conventionally 
thought. From a behavioral perspective, which will not be discussed here, 
since the JavaScript speaks for itself: Asynchronous Module Definition 
is used to address “cache invalidation”, while Organic CSS is leveraged to 
address the “naming things” horn of the dilemma.

#### A Glance at CSS as a Cousin to Assembly

CSS is “low-level”, in a way similar to assembly language. Look at this 
common code structure in a popular assembly language:

    $ gdb static
    (gdb) break natural_generator
    (gdb) run
    (gdb) disassemble
    Dump of assembler code for function natural_generator:
    push   %rbp
    mov    %rsp,%rbp
    movl   $0x1,-0x4(%rbp)
    mov    0x177(%rip),%eax        # 0x100001018 <natural_generator.b>
    add    $0x1,%eax
    mov    %eax,0x16c(%rip)        # 0x100001018 <natural_generator.b>
    mov    -0x4(%rbp),%eax
    add    0x163(%rip),%eax        # 0x100001018 <natural_generator.b>
    pop    %rbp
    retq   
    End of assembler dump.

This has so much more in common with CSS than easily deducible, and our 
other writings demonstrate. Here is a common occurrence of SASS, a 
language which emphasizes this commonality:

    @function color-diff($color-a, $color-b) {
      $hue: hue($color-a) - hue($color-b);
      $saturation: saturation($color-a) - saturation($color-b);
      $lightness: lightness($color-a) - lightness($color-b);

      $function-hue: 'adjust-hue';
      $function-saturation: if($saturation > 0, 'desaturate', 'saturate');
      $function-lightness: if($lightness > 0, 'darken', 'lighten');

      @return (
        #{$function-hue}: -($hue),
        #{$function-saturation}: abs($saturation),
        #{$function-lightness}: abs($lightness),
      );
    }

Generally, the two object language paradigms parallel in CSS’s lexical 
block semantic space configuration and assembly’s functional space 
translation, say disassembled from C. All we’re saying in CSS involves 
a state description of some location in the DOM, assuming that it is 
an element, and only if it is an element: CSS does not ``create`` the 
elements, just as assembly does not ``create`` memory addresses.

Here’s a more concrete example, as SASS makes grid system development 
and innovation more readily handy:

    @mixin grid-movements($cols: $ninesixty-columns) {
      #{enumerate(".push", 1, $cols - 1, $ninesixty-class-separator)},
      #{enumerate(".pull", 1, $cols - 1, $ninesixty-class-separator)} {
        @include grid-move-base
      }
      @for $n from 1 through $cols - 1 {
        .push#{$ninesixty-class-separator}#{$n} {
          @include grid-move-push($n, $cols);
        }
        .pull#{$ninesixty-class-separator}#{$n} {
          @include grid-move-pull($n, $cols);
        }
      }
    }

And the assembly comparison:

    0x0000000100000f67 <main+23>:   mov    -0x8(%rbp),%ecx
    0x0000000100000f6a <main+26>:   add    $0x6,%ecx
    0x0000000100000f70 <main+32>:   mov    %ecx,-0xc(%rbp)

My defense here follows by analogy[2]:

1. CSS is highly variable across browser implementations; in the world of 
Browser as an OS (think SAAS), CSS is the assembly language of Genetic 
Ages. CSS is the basic presentation language of thought; browsers are the 
most basic implementation to facilitate virtual experience present over 
the substrate of the Internet, organized on the principles of HTTP/2.
2. CSS is inexpressive.

These features make CSS, and not JavaScript, most like assembly languages.

### Architecture

Directories are themselves abstractions, contingent upon variegated 
meanings regarding “files”, and what we think they entail when they 
exist, or fail to exist, in certain places in time, certainly through 
the incremental evolution of a working codebase architecture.

### Style Guide

We’re using Material Design via Angular Material (implementation syntax), and 
we get an aesthetically flat design for free where wireframes may be wanting 
on decision procedures about functional design elements (in terms of 
accessibility, layout, data state integration, event-state interaction and 
namespace pseudoelement triggers in CSS, etc.).h

—
[0]: https://twitter.com/filesofnerds/status/630097230716743680
[1]: This entire conceptual sphere begins with the idea that CSS lacks variables, 
     and this makes it most like assembly. Also, the lack of variables makes CSS 
     the most fun: https://twitter.com/mlane/status/611198382015705089. Assembly 
     has to deal with the hardware pointer-constriction dynamic; and CSS the 
     vendor prefix problem. These are fun problems, and innovations like SASS and 
     LESS expose them for their true nature, by giving us a way to craft macros 
     of CSS to expected browser states and pseudo states.
[2]: http://blog.izs.me/post/10213512387/javascript-is-not-web-assembly
