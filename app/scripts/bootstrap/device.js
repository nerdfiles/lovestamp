
/*
 * fileOverview

     _
    | |                 _          _
    | |__   ___   ___ _| |_  ___ _| |_  ____ _____ ____
    |  _ \ / _ \ / _ (_   _)/___|_   _)/ ___|____ |  _ \
    | |_) ) |_| | |_| || |_|___ | | |_| |   / ___ | |_| |
    |____/ \___/ \___/  \__|___/   \__)_|   \_____|  __/
                                                  |_|

 *# description
 */

(function() {
  require(['angularAMD', 'angular', 'ionic', 'cordova-ios'], function(angularAMD, angular, ionic) {
    var doc, initialize;
    doc = document;
    initialize = function() {
      return require(['interface']);
    };
    doc.addEventListener('deviceready', initialize, false);
  });

}).call(this);

//# sourceMappingURL=device.js.map
