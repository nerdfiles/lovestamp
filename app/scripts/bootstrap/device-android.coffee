###
# fileOverview

     _
    | |                 _          _
    | |__   ___   ___ _| |_  ___ _| |_  ____ _____ ____
    |  _ \ / _ \ / _ (_   _)/___|_   _)/ ___|____ |  _ \
    | |_) ) |_| | |_| || |_|___ | | |_| |   / ___ | |_| |
    |____/ \___/ \___/  \__|___/   \__)_|   \_____|  __/
                                                  |_|

## description

###

require [
  'angularAMD'
  'angular'
  'ionic'
  #'cordova' # Hardcode cordova directly to the bone (HTML)
], (angularAMD, angular) ->

  doc = document
  ngDoc = angular.element(doc)

  window.is_android_loaded = false

  initialize = () ->
    try
      window.is_android_loaded = true
      require ['interface']
    catch e
      console.log e

  #doc.addEventListener 'deviceready', initialize, false

  window.setTimeout () ->
    if window.is_android_loaded == false
      angular.element().ready ->
        try
          initialize()
        catch e
          console.error e.stack or e.message or e
        return
  , 0
