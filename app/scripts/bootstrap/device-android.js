
/*
 * fileOverview

     _
    | |                 _          _
    | |__   ___   ___ _| |_  ___ _| |_  ____ _____ ____
    |  _ \ / _ \ / _ (_   _)/___|_   _)/ ___|____ |  _ \
    | |_) ) |_| | |_| || |_|___ | | |_| |   / ___ | |_| |
    |____/ \___/ \___/  \__|___/   \__)_|   \_____|  __/
                                                  |_|

 *# description
 */

(function() {
  require(['angularAMD', 'angular', 'ionic'], function(angularAMD, angular) {
    var doc, initialize, ngDoc;
    doc = document;
    ngDoc = angular.element(doc);
    window.is_android_loaded = false;
    initialize = function() {
      var e;
      try {
        window.is_android_loaded = true;
        return require(['interface']);
      } catch (_error) {
        e = _error;
        return console.log(e);
      }
    };
    return window.setTimeout(function() {
      if (window.is_android_loaded === false) {
        return angular.element().ready(function() {
          var e;
          try {
            initialize();
          } catch (_error) {
            e = _error;
            console.error(e.stack || e.message || e);
          }
        });
      }
    }, 0);
  });

}).call(this);

//# sourceMappingURL=device-android.js.map
