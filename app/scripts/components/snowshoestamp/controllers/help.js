
/**
@fileOverview

 _           _
| |         | |
| |__  _____| | ____
|  _ \| ___ | ||  _ \
| | | | ____| || |_| |
|_| |_|_____)\_)  __/
               |_|

@description

  Stamp help.
 */

(function() {
  define(["config", "auth/user/roll-up"], function(environment) {
    var StampHelpController;
    StampHelpController = function($scope, $location) {};
    return ["$scope", "$location", StampHelpController];
  });

}).call(this);

//# sourceMappingURL=help.js.map
