
/*
 *fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

 *# description

  Stamp Controller for "stamp here" screens.

  Stamp Actions should be anchored to the Bitcoin Blockchain where randomized 
  client nodes upon purse drag initiate transaction. Stamp Actions should be 
  registered as assets in Openchain, where merchants purchase from asset 
  issuances.

  The problem this solves is that I can easily deface a wifi hotspot and 
  override CSS with clickjacking links, like the recent LinkedIn hacks. 
  This practice is called CSSi. User can drop a link that spans the 
  entire stamp screen to intercept the stampening. This kind of attack 
  is generally "injection", and with CSS it is actually nearly impossible 
  to stop without doing full pentest regression testing on CSS selectors; 
  we would need to be able to minimally test :visited and :active and maybe 
  even :focus pseudo-selectors in different browsers. The Native App minimizes 
  this behavior, but fundamentally the problem is still the same:

  When people log in to wifi hotspots, weird shit happens. I've reviewed lots 
  of the loading code for Native Apps using logcat on Android, and Apple's 
  API is even simpler... It's very easy to tell a phone upon establishing 
  connection to a wifi hotspot that certain scripts should be overridden. 
  And many of these newer botnet kits includes libs to do App-to-App attacks. 
  So they might symbol select for Cordova apps, Ionic apps, pure Native apps.

  In Android it is almost painfully obvious when to capture the signal request 
  to browser, renderers, sound, devices like microphone, etc. There are numbers 
  of opportunities to inject scripts that override dependent scripts before 
  the script engine executes the window. This may be a fundamental flaw with 
  Web in Native, but that still requires more work from the symbol interpreter 
  of the botnet toolkit. CSSi is subtle but not impossible to detect, but 
  generally some clients will be compromised not just because someone opened 
  a CSS or script they should not have from an e-mail — which might compromise 
  the LoveStamp WEB Fan View's stamp screen, etc. The wifi hotspot can be tapped 
  just as easily with everyday rootkits. The problem here is stamp forwarding 
  after validation, a kind of MitM attack.

  If less than half of the clients contain proper validations, stamp screen 
  presentation times can correspond to leases. The stamp screen between any two 
  users participating in the transaction screen cannot lease each other within 
  an allotted timeframe. So there has to be a necessary hold time that is 
  proportional to the number of observer nodes that can process the upstream 
  of participating stamp screens. Once a stamp screen is pressed, it creates 
  a new lease for another stamp screen. If an attack clickjacks a screen, 
  the lease will not be created, thus disrupting the participation in the 
  stream, and the observers recognizing that lease can count the unending 
  "lease window creation" as misbehavior. QED.
 */

(function() {
  define(["config", "chance", "moment", "auth/user/roll-up", "directives/stamp", "services/stamp"], function(config, Chance, moment) {
    var StampController;
    StampController = function($scope, stampService, $location, userRequire, $timeout) {

      /*
      Stamp Controller.
       */
      var _alias, _userRequire;
      $scope.aliasRandomizer = new Chance();
      $scope.stampReceipt = null;
      $scope.stampPasses = [];
      $scope.stamp = {};
      $scope.appKey = config.snowshoestamp.appKey;
      _alias = $scope.aliasRandomizer.string({
        length: 15
      });
      _userRequire = userRequire();

      /* Implementation functions */
      $scope.loadHome = function() {

        /*
        For normal stamp screen use-case.
         */
        var t;
        t = $timeout(function() {
          $location.path('/fan');
          $timeout.cancel(t);
        }, 250);
      };
      $scope.stampAdded = false;
      $scope.goToAdmin = function() {

        /*
        For Stamp Recall use-case.
         */
        return $location.path('/merchant/admin');
      };
      $scope.addStamp = function() {

        /*
        Add Stamp signature to be validated at server side.
         */
        var birth;
        if ($scope.stampAdded === true) {
          return;
        }
        $scope.stampAdded = true;
        birth = moment();
        _userRequire.then(function(authData) {
          $scope.stampSig = {
            username: authData.handle,
            creator: authData.handle,
            alias: _alias,
            passes: $scope.stampPasses,
            receipt: $scope.stampReceipt,
            birthDate: birth.format("MM/DD/YYYY HH:mm:ss"),
            statusSaturation: 1
          };
          stampService.addStamp($scope.stampSig);
          return $location.path('/merchant/admin');
        });
      };
    };
    return ["$scope", "stamp.service", "$location", "user.require", "$timeout", StampController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
