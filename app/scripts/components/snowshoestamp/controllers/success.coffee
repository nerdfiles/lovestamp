###
# fileOverview

      ___ _   _  ____ ____ _____  ___  ___
     /___) | | |/ ___) ___) ___ |/___)/___)
    |___ | |_| ( (__( (___| ____|___ |___ |
    (___/|____/ \____)____)_____|___/(___/

## description

Stamp success conditions include the following:

    1. "Delivered" Success
    2. "Out_for_Delivery" Success
    3. "Tip Delivery Bug" Success (?)

###

define [
  "config"
  "numeral"
  "auth/user/roll-up"
  'directives/bitsDisplayCounter'
  'directives/relExternal'
  'directives/purse'
  'services/authentication'
  'services/notification'
  'services/profile'
  'services/stamp'
  'services/bank'
  'services/browser'
], (environment, numeral) ->

  blank = ''
  socialPrefix =
    twitter  : '@' # at/about-symbol
    facebook : 'ᶠ' # modifier letter small f
    google   : '✚' # not-quus

  tipUrlBase = 'https://www.changetip.com/collect/'
  tipUrl = 'https://www.changetip.com/tipme/'

  StampSuccessController = ($scope, $rootScope, notificationService, profileService, stampService, userRequire, $timeout, bankService, $location, browserService) ->
    ###
    Displays bits collected.
    ###

    _userRequire = userRequire()

    $scope.loadHome = () ->
      t = $timeout ->
        $location.path('/fan')
        $timeout.cancel t
        return
      , 250
      return


    $scope.onPurseCompleteDrop = (data, event) ->
      ###
      On Purse Reset.
      ###
      return

    $scope.onPurseCompleteDrag = (data, event) ->
      ###
      User drags to reset.

      @param {Promise:object} Data should be a post to the server.
      ###
      return

    homeTimeoutDelay = 10000

    $scope.$on '$destroy', () ->
      $timeout.cancel notificationService.successTimeout
      return

    $scope.$on 'home:redirect', (event) ->
      profileService.clearValidations()
      notificationService.successTimeout = $timeout () ->
        $location.path '/fan'
      , homeTimeoutDelay

    $scope.$on '$locationChangeStart', (event) ->
        profileService.clearValidations()
        $timeout.cancel notificationService.successTimeout
        return

    $scope.loadProfilePage = () ->
        _userRequire.then (_user) ->
            handle = _user.handle
            $location.path('/fan/user/profile')


    _userRequire.then (_user) ->

        console.log _user

        $scope.generatedClaimantUrl = [
            tipUrl
            _user.handle.
                replace(socialPrefix.twitter, blank).
                replace(socialPrefix.google, blank).
                replace(socialPrefix.facebook, blank)
        ].join ''

        $scope.collectedCurrency = 'bits'

        filteredValidations = _.filter profileService.runningValidations, (v) ->
            v.updatedPurse and v.payout and v.stamp and v.stamp.serial

        if _.size(filteredValidations) == 0
            return

        _lastValidation = _.last(filteredValidations)

        console.log _user.handle

        profileService.getUserStatus(_user.handle).$loaded().then (profileData) ->
          console.log profileData
          if profileData.profile.purse
            _updatedPurse = parseFloat(profileData.profile.purse)
            $scope.updatedPurse = numeral(_updatedPurse).format(bankService.currencyFormat)

        $scope.messageConstruct = _lastValidation.messageConstruct

        # Check if our validations for the current user exist
        if _lastValidation

            lastValidationSerial = _lastValidation.stamp.serial

            # Use most recent validation for lookup
            stampService.getStampIdBySerial(lastValidationSerial).then (stampId) ->

                # Get bits collected for last stamp
                stampService.getLatestBitsCollected(stampId, _user.handle).then (bitsReport) ->

                    $scope._collectedCurrency = _.filter(_.keys(bitsReport), (label) ->
                        _.endsWith(label, 'Collected')
                    )

                    $scope._collectedCurrency = _.first($scope._collectedCurrency).split(/(?=[A-Z])/)

                    $scope.collectedCurrency = _.first($scope._collectedCurrency)

                    _bitsCollected = bitsReport.bitsCollected

                    bitsCollectedValue = if parseFloat(_bitsCollected) > 0 then parseFloat(_bitsCollected).toFixed(2) else 0

                    $scope.$broadcast 'bitsCollectedValue:loaded',
                        val: numeral(bitsCollectedValue).format(bankService.currencyFormat)

                    $scope.$broadcast 'home:redirect', { pageStatus: true }


                , (error) ->
                    $scope.$broadcast 'bitsCollectedValue:loaded',
                        val: 3.14

            return
        return
    return

  [
    "$scope"
    "$rootScope"
    "notification.service"
    "profile.service"
    "stamp.service"
    "user.require"
    "$timeout"
    "bank.service"
    "$location"
    "browser.service"
    StampSuccessController
  ]
