###*
@fileOverview

 _____  ____ ____ ___   ____
| ___ |/ ___) ___) _ \ / ___)
| ____| |  | |  | |_| | |
|_____)_|  |_|   \___/|_|


@description

   Error

###

define [
  "interface"
  "config"
  "auth/user/roll-up"
  'services/notification'
  'services/profile'
  'services/browser'
], (__interface__, environment ) ->

  StampErrorController = ($scope, $location, notificationService, profileService, $timeout, browserService) ->

    $scope.openContact = () ->
      config =
        url     : 'https://lovestamp.io/contact'
        target  : "_blank"
        options : []
      browserService.open(config)
      return

    $scope.redirect = (_page) ->
        $location.path _page

    $scope.loadHome = () ->
      t = $timeout ->
        $location.path('/fan')
        $timeout.cancel t
        return
      , 250
      return


    return

  [
    "$scope"
    "$location"
    "notification.service"
    "profile.service"
    '$timeout'
    'browser.service'
    StampErrorController
  ]
