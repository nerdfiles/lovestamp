
/*
 * fileOverview

      ___ _   _  ____ ____ _____  ___  ___
     /___) | | |/ ___) ___) ___ |/___)/___)
    |___ | |_| ( (__( (___| ____|___ |___ |
    (___/|____/ \____)____)_____|___/(___/

 *# description

Stamp success conditions include the following:

    1. "Delivered" Success
    2. "Out_for_Delivery" Success
    3. "Tip Delivery Bug" Success (?)
 */

(function() {
  define(["config", "numeral", "auth/user/roll-up", 'directives/bitsDisplayCounter', 'directives/relExternal', 'directives/purse', 'services/authentication', 'services/notification', 'services/profile', 'services/stamp', 'services/bank', 'services/browser'], function(environment, numeral) {
    var StampSuccessController, blank, socialPrefix, tipUrl, tipUrlBase;
    blank = '';
    socialPrefix = {
      twitter: '@',
      facebook: 'ᶠ',
      google: '✚'
    };
    tipUrlBase = 'https://www.changetip.com/collect/';
    tipUrl = 'https://www.changetip.com/tipme/';
    StampSuccessController = function($scope, $rootScope, notificationService, profileService, stampService, userRequire, $timeout, bankService, $location, browserService) {

      /*
      Displays bits collected.
       */
      var homeTimeoutDelay, _userRequire;
      _userRequire = userRequire();
      $scope.loadHome = function() {
        var t;
        t = $timeout(function() {
          $location.path('/fan');
          $timeout.cancel(t);
        }, 250);
      };
      $scope.onPurseCompleteDrop = function(data, event) {

        /*
        On Purse Reset.
         */
      };
      $scope.onPurseCompleteDrag = function(data, event) {

        /*
        User drags to reset.
        
        @param {Promise:object} Data should be a post to the server.
         */
      };
      homeTimeoutDelay = 10000;
      $scope.$on('$destroy', function() {
        $timeout.cancel(notificationService.successTimeout);
      });
      $scope.$on('home:redirect', function(event) {
        profileService.clearValidations();
        return notificationService.successTimeout = $timeout(function() {
          return $location.path('/fan');
        }, homeTimeoutDelay);
      });
      $scope.$on('$locationChangeStart', function(event) {
        profileService.clearValidations();
        $timeout.cancel(notificationService.successTimeout);
      });
      $scope.loadProfilePage = function() {
        return _userRequire.then(function(_user) {
          var handle;
          handle = _user.handle;
          return $location.path('/fan/user/profile');
        });
      };
      _userRequire.then(function(_user) {
        var filteredValidations, lastValidationSerial, _lastValidation;
        console.log(_user);
        $scope.generatedClaimantUrl = [tipUrl, _user.handle.replace(socialPrefix.twitter, blank).replace(socialPrefix.google, blank).replace(socialPrefix.facebook, blank)].join('');
        $scope.collectedCurrency = 'bits';
        filteredValidations = _.filter(profileService.runningValidations, function(v) {
          return v.updatedPurse && v.payout && v.stamp && v.stamp.serial;
        });
        if (_.size(filteredValidations) === 0) {
          return;
        }
        _lastValidation = _.last(filteredValidations);
        console.log(_user.handle);
        profileService.getUserStatus(_user.handle).$loaded().then(function(profileData) {
          var _updatedPurse;
          console.log(profileData);
          if (profileData.profile.purse) {
            _updatedPurse = parseFloat(profileData.profile.purse);
            return $scope.updatedPurse = numeral(_updatedPurse).format(bankService.currencyFormat);
          }
        });
        $scope.messageConstruct = _lastValidation.messageConstruct;
        if (_lastValidation) {
          lastValidationSerial = _lastValidation.stamp.serial;
          stampService.getStampIdBySerial(lastValidationSerial).then(function(stampId) {
            return stampService.getLatestBitsCollected(stampId, _user.handle).then(function(bitsReport) {
              var bitsCollectedValue, _bitsCollected;
              $scope._collectedCurrency = _.filter(_.keys(bitsReport), function(label) {
                return _.endsWith(label, 'Collected');
              });
              $scope._collectedCurrency = _.first($scope._collectedCurrency).split(/(?=[A-Z])/);
              $scope.collectedCurrency = _.first($scope._collectedCurrency);
              _bitsCollected = bitsReport.bitsCollected;
              bitsCollectedValue = parseFloat(_bitsCollected) > 0 ? parseFloat(_bitsCollected).toFixed(2) : 0;
              $scope.$broadcast('bitsCollectedValue:loaded', {
                val: numeral(bitsCollectedValue).format(bankService.currencyFormat)
              });
              return $scope.$broadcast('home:redirect', {
                pageStatus: true
              });
            }, function(error) {
              return $scope.$broadcast('bitsCollectedValue:loaded', {
                val: 3.14
              });
            });
          });
          return;
        }
      });
    };
    return ["$scope", "$rootScope", "notification.service", "profile.service", "stamp.service", "user.require", "$timeout", "bank.service", "$location", "browser.service", StampSuccessController];
  });

}).call(this);

//# sourceMappingURL=success.js.map
