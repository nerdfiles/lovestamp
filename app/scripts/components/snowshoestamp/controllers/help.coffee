###*
@fileOverview

 _           _
| |         | |
| |__  _____| | ____
|  _ \| ___ | ||  _ \
| | | | ____| || |_| |
|_| |_|_____)\_)  __/
               |_|

@description

  Stamp help.

###

define [
  "config"
  "auth/user/roll-up"
], (environment) ->

  StampHelpController = ($scope, $location) ->

    return

  [
    "$scope"
    "$location"
    StampHelpController
  ]
