
/**
@fileOverview

 _____  ____ ____ ___   ____
| ___ |/ ___) ___) _ \ / ___)
| ____| |  | |  | |_| | |
|_____)_|  |_|   \___/|_|


@description

   Error
 */

(function() {
  define(["interface", "config", "auth/user/roll-up", 'services/notification', 'services/profile', 'services/browser'], function(__interface__, environment) {
    var StampErrorController;
    StampErrorController = function($scope, $location, notificationService, profileService, $timeout, browserService) {
      $scope.openContact = function() {
        var config;
        config = {
          url: 'https://lovestamp.io/contact',
          target: "_blank",
          options: []
        };
        browserService.open(config);
      };
      $scope.redirect = function(_page) {
        return $location.path(_page);
      };
      $scope.loadHome = function() {
        var t;
        t = $timeout(function() {
          $location.path('/fan');
          $timeout.cancel(t);
        }, 250);
      };
    };
    return ["$scope", "$location", "notification.service", "profile.service", '$timeout', 'browser.service', StampErrorController];
  });

}).call(this);

//# sourceMappingURL=error.js.map
