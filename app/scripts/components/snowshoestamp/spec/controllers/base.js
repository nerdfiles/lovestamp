
/**
@fileOverview

 _
| |
| |__  _____  ___ _____
|  _ \(____ |/___) ___ |
| |_) ) ___ |___ | ____|
|____/\_____(___/|_____)

@depends elastic-search
@description

    For testing ChangeTip API responses as JSON payloads.
 */

(function() {
  define(['angularAMD'], function(__interface__) {
    beforeEach(function() {
      var $scope, companyService, config;
      $scope = {
        $apply: jasmine.createSpy(),
        $on: jasmine.createSpy()
      };
      config = {
        url: {
          VALIDATE_STAMP: 'validateStamp'
        }
      };
      companyService = {
        findDuplicates: jasmine.createSpy()
      };
      return module('LS.plugins.snowshoestamp.controllers');
    });
    beforeEach(inject(function($controller, $httpBackend) {
      var controller, httpBackend;
      httpBackend = $httpBackend;
      return controller = $controller('StampController', {
        $scope: $scope,
        config: config,
        stampService: stampService
      });
    }));
    it('validateStamp success', function() {
      var resp, url;
      url = '/stamp/:username/:status/:handleType?';
      resp = {
        points: []
      };
      setupFindMatchesForm();
      httpBackend.whenGET(url).respond(resp);
      httpBackend.expectGET(url);
      $scope.findMatches();
      expect($scope.loading).toBe(true);
      httpBackend.flush();
      return expect($scope.loading).toBe(false);
    });
    beforeEach(function() {
      return window.setTimeout(function() {}, 0);
    }, false);
    describe('unit', function() {
      return it('should', function() {});
    });
    return __interface__.directive().controller().filter();
  });

}).call(this);

//# sourceMappingURL=base.js.map
