###*
@fileOverview

 _
| |
| |__  _____  ___ _____
|  _ \(____ |/___) ___ |
| |_) ) ___ |___ | ____|
|____/\_____(___/|_____)

@depends elastic-search
@description

    For testing ChangeTip API responses as JSON payloads.

###

define [
    'angularAMD'
], (__interface__) ->

    beforeEach(() ->
        $scope =
            $apply: jasmine.createSpy()
            $on: jasmine.createSpy()
        config =
            url:
                VALIDATE_STAMP: 'validateStamp'
        companyService =
            findDuplicates: jasmine.createSpy()
        module('LS.plugins.snowshoestamp.controllers')
    )
    beforeEach(inject(($controller, $httpBackend) ->
        httpBackend = $httpBackend
        controller = $controller('StampController',
            $scope: $scope,
            config: config,
            stampService: stampService
        )
    ))

    it('validateStamp success', () ->
        url = '/stamp/:username/:status/:handleType?'
        resp = {points: []}

        setupFindMatchesForm()

        httpBackend.whenGET(url).respond(resp)
        httpBackend.expectGET(url)

        $scope.findMatches()

        expect($scope.loading).toBe(true)

        httpBackend.flush()

        expect($scope.loading).toBe(false)
        # TODO: fix
        # expect($scope.matchResults).toBe(resp.companies)
        # expect($scope.getCompanies()).toBe(resp.companies)
    )

    beforeEach(() ->
        window.setTimeout () ->
            return
        , 0
    , false)

    describe('unit', () ->

        it('should', () ->

        )

    )
    __interface__
        .directive()
        .controller()
        .filter()
