(function() {
  define(['../controllers/base'], function() {
    return describe("Directive:", function() {
      describe("template", function() {
        var $compile, $httpBackend, $scope;
        $compile = void 0;
        $scope = void 0;
        $httpBackend = void 0;
        beforeEach(module("templates"));
        beforeEach(inject(function(_$compile_, _$rootScope_) {
          $compile = _$compile_;
          $scope = _$rootScope_.$new();
        }));
        it("should render the header and text as passed in by $scope", inject(function() {
          var previousHeader, previousText, template, templateAsHtml;
          template = $compile("<div your-directive></div>")($scope);
          $scope.header = "This is a header";
          $scope.text = "Lorem Ipsum";
          $scope.$digest();
          templateAsHtml = template.html();
          expect(templateAsHtml).toContain($scope.header);
          expect(templateAsHtml).toContain($scope.text);
          previousHeader = $scope.header;
          previousText = $scope.text;
          $scope.header = "A completely different header";
          $scope.text = "Something completely different";
          $scope.$digest();
          templateAsHtml = template.html();
          expect(templateAsHtml).toContain($scope.header);
          expect(templateAsHtml).toContain($scope.text);
          expect(templateAsHtml).toNotContain(previousHeader);
          expect(templateAsHtml).toNotContain(previousText);
        }));
      });
    });
  });

}).call(this);

//# sourceMappingURL=stamp.js.map
