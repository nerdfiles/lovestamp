define [
  '../controllers/base'
],
() ->

  describe "Directive:", ->

    #beforeEach angular.mock.module("lovestamp")

    describe "template", ->

      $compile = undefined
      $scope = undefined
      $httpBackend = undefined
      # Load the templates module
      beforeEach module("templates")
      # Angular strips the underscores when injecting
      beforeEach inject((_$compile_, _$rootScope_) ->
        $compile = _$compile_
        $scope = _$rootScope_.$new()
        return
      )

      it "should render the header and text as passed in by $scope", inject(->
        # $compile the template, and pass in the $scope.
        # This will find your directive and run everything
        template = $compile("<div your-directive></div>")($scope)

        # Set some values on your $scope
        $scope.header = "This is a header"
        $scope.text = "Lorem Ipsum"

        # Now run a $digest cycle to update your template with new data
        $scope.$digest()

        # Render the template as a string
        templateAsHtml = template.html()

        # Verify that the $scope variables are in the template
        expect(templateAsHtml).toContain $scope.header
        expect(templateAsHtml).toContain $scope.text

        # Do it again with new values
        previousHeader = $scope.header
        previousText = $scope.text

        $scope.header = "A completely different header"
        $scope.text = "Something completely different"
        # Run the $digest cycle again
        $scope.$digest()
        templateAsHtml = template.html()

        expect(templateAsHtml).toContain $scope.header
        expect(templateAsHtml).toContain $scope.text
        expect(templateAsHtml).toNotContain previousHeader
        expect(templateAsHtml).toNotContain previousText

        return
      )
      return

    return

