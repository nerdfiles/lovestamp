
/*
 * fileOverview

            _
      ___ _| |_ _____ ____  ____
     /___|_   _|____ |    \|  _ \
    |___ | | |_/ ___ | | | | |_| |
    (___/   \__)_____|_|_|_|  __/
                           |_|

 *# description

  Primary logic around stamp impression collection under various use-case scenarios:

  1. Normal Customer Stamp Impression
  2. Control Stamp Impression
  3. Recall Stamp Impression; TBD
 */

(function() {
  define(['interface', 'lodash', 'graham-scan', 'jquery-json', 'touchy', 'services/authentication', 'services/stamp', 'services/notification', 'services/profile', 'services/crypto'], function(__interface__, _, GrahamScan) {
    var ngStamp;
    ngStamp = function($http, userRequire, $timeout, $interval, $window, $q, $location, stampService, purseService, notificationService, profileService, cryptoService) {
      var linker;
      return {

        /*
        Custom validation directive.
        
        @inner
         */
        restrict: 'A',
        scope: {
          app_key: '=appkey',
          stampIdent: '=stampident',
          username: '=username',
          stampPasses: '=stampPasses',
          stampReceipt: '=stampReceipt'
        },
        link: linker = function($scope, element, attr) {
          var apiBase, apiBaseConstruct, apiPrefix, apiProductionConstruct, body, endErrorCheck, errorCheck, errorStart, errorTime, incrementCount, insufficientCount, portSpec, signalOffStampAnimation, signalStampAnimation, t, timeouts, touchDone, _timeout, _userRequire;
          $scope.localValidatorStamps = stampService.localValidatorStamps;
          $scope.stampReceipt = null;
          $scope.stampPasses = [];
          $scope.validStamp = false;
          $scope.stamped = false;
          body = document.getElementsByTagName("body")[0];
          $scope.isValidating = false;
          _timeout = null;
          console.log($scope.localValidatorStamps);
          signalStampAnimation = function(event) {
            element.removeClass('bounce-in-left');
            return $scope.$on('isValidating', function(event, newState) {
              return $timeout(function() {
                return $scope.$apply(function() {
                  return element.addClass('stamp--validating');
                });
              }, 0);
            });
          };
          signalOffStampAnimation = function(event) {
            return _timeout = $timeout(function() {
              element.removeClass('tada');
              return $timeout.cancel(_timeout);
            }, 1000);
          };
          angular.element(element[0]).on('touchstart mouseover', signalStampAnimation);
          angular.element(element[0]).on('touchend mouseout', signalOffStampAnimation);

          /*
          
                       _  _     _                    ______
                      | |(_)   | |        _         / _____) _
           _   _ _____| | _  __| |_____ _| |_ _____( (____ _| |_ _____ ____  ____
          | | | (____ | || |/ _  (____ (_   _) ___ |\____ (_   _|____ |    \|  _ \
           \ V // ___ | || ( (_| / ___ | | |_| ____|_____) )| |_/ ___ | | | | |_| |
            \_/ \_____|\_)_|\____\_____|  \__)_____|______/  \__)_____|_|_|_|  __/
                                                                            |_|
          
          @namespace validateStamp
          @description
          
            Pass a point array and a set of coordinates to locate base point.
          
          @param {array} pointObjects
          @param {number} x
          @param {number} y
          @return {number} We return a planar number sorting into linear time 
                           lying on a parabola, or by "a single sorting step 
                           followed by a linear amount of additional work".
           */
          $scope.validateStamp = function(pointObjects, x, y) {
            var x1, x2, y1, y2;
            x2 = pointObjects[y][0];
            x1 = pointObjects[x][0];
            y2 = pointObjects[y][1];
            y1 = pointObjects[x][1];
            return Math.sqrt((Math.pow(x2 - x1, 2)) + (Math.pow(y2 - y1, 2)));
          };

          /*
          
           _                   _ _     _       _  _     _             _
          | |                 | (_)   (_)     | |(_)   | |        _  (_)
          | | ___   ____ _____| |_     _ _____| | _  __| |_____ _| |_ _  ___  ____
          | |/ _ \ / ___|____ | | |   | (____ | || |/ _  (____ (_   _) |/ _ \|  _ \
          | | |_| ( (___/ ___ | |\ \ / // ___ | || ( (_| / ___ | | |_| | |_| | | | |
           \_)___/ \____)_____|\_)\___/ \_____|\_)_|\____\_____|  \__)_|\___/|_| |_|
          
          @description
          
            Accepting the following "STAMP SCREEN" scenarios:
          
            1. stamp/id
            2. stamp/
            3. stamp/recall
           */
          $scope.$on('localValidation', function(event, validationObject) {
            var customValidatedStampReceipt, data, local, _handle;
            _handle = validationObject.handle;
            data = validationObject._data;
            local = validationObject.local;
            customValidatedStampReceipt = validationObject.customReceipt || null;
            if (_handle) {
              if (local) {
                _handle = 'local/' + _handle;
              } else {
                _handle = 'success/' + _handle;
              }
              return $http.post($scope.callbackURL + _handle, {
                data: sss.util.base64.encode($.toJSON(data)),
                createTipUrl: false
              }).success(function(validationSSS) {
                if (local && validationSSS && validationSSS.validation && validationSSS.validation.stamp.serial) {
                  profileService.addValidation(validationSSS);
                  $timeout(function() {
                    return $scope.$apply(function() {
                      return $scope.stampReceipt = validationSSS;
                    });
                  }, 0);
                  if ($location.$$path.indexOf('recall') !== -1) {
                    notificationService.displayError('SSS Serial: ' + validationSSS.validation.stamp.serial);
                  } else {
                    notificationService.displayError('Thanks!');
                  }
                } else {
                  notificationService.displayError('Reload / Try again!');
                }
                return $scope.isValidating = false;
              }).error(function(errorData) {
                notificationService.displayError('Please try again!');
                return $scope.isValidating = false;
              });
            }
          });
          $scope.username = null;
          portSpec = $window.location.port !== "" ? ':' + $window.location.port : '';
          apiPrefix = window.is_device || portSpec === '' ? '/data' : '';
          apiBaseConstruct = $window.location.protocol + '//' + $window.location.hostname + portSpec + apiPrefix;
          apiProductionConstruct = 'https://lovestamp.io/data';
          apiBase = window.is_device === true ? apiProductionConstruct : apiBaseConstruct;
          _userRequire = userRequire();
          _userRequire.then(function(authData) {
            var handle, p;
            $scope.user = authData;
            handle = authData.handle;
            $scope.username = authData[authData.provider].username;
            p = authData.provider;
            $scope.username_prefix = p === 'twitter' ? '@' : p === 'google' ? '✚' : 'ᶠ';
            $scope._username = $scope.username_prefix + $scope.username;
            $scope.callbackURL = apiBase + '/stamp/' + $scope._username + '/';
            $scope.getStampBySerial = apiBase + '/stamp/get-serial';
            $scope.repostURL = apiBase + '/stamp/repost';
            return $scope.errorURL = apiBase + '/stamp/' + $scope._username + '/error';
          });
          $scope.runTotalBounds = 0;
          errorCheck = function(number) {
            var errorStart;
            return;
            if (insufficientCount > 2) {
              $window.location = $scope.errorURL;
            }
            timeouts[number] = $timeout(function() {
              $window.location = $scope.errorURL;
            }, 2000);
            errorStart = new Date();
            if (errorTime > 2000) {
              $window.location = $scope.errorURL;
            }
          };
          endErrorCheck = function(number) {
            errorTime += new Date() - errorStart;
            clearTimeout(timeouts[number]);
          };
          incrementCount = function(_hand) {
            var def, touchDone;
            def = $q.defer();
            $timeout((function() {
              var touchDone;
              def.resolve(touchDone = true);
            }), 30);
            if (touchDone === true) {
              insufficientCount++;
              def.reject(touchDone = false);
            }
          };
          errorStart = void 0;
          errorTime = 0;
          insufficientCount = 0;
          touchDone = false;
          timeouts = {};
          t = null;
          $scope.$on('$locationChangeStart', function(event, oldUrl, newUrl) {});
          t = Touchy(body, {
            one: function(hand, finger) {
              hand.on("start", function(points) {
                errorCheck(1);
              });
              hand.on("end", function(points) {
                endErrorCheck(1);
              });
            },
            two: function(hand, finger1, finger2) {
              hand.on("start", function(points) {
                errorCheck(2);
              });
              hand.on("end", function(points) {
                endErrorCheck(2);
              });
            },
            three: function(hand, finger1, finger2, finger3) {
              hand.on("start", function(points) {
                errorCheck(3);
              });
              hand.on("end", function(points) {
                endErrorCheck(3);
              });
            },
            four: function(hand, finger1, finger2, finger3, finger4) {
              hand.on("start", function(points) {
                errorCheck(4);
              });
              hand.on("end", function(points) {
                endErrorCheck(4);
              });
            },
            five: function(hand, finger1, finger2, finger3, finger4, finger5) {
              var stampValidation;
              stampValidation = {};
              stampValidation.validity = [];
              if ($scope.stampIdent === false && $scope.stamped === true) {
                return;
              }
              hand.on("start", function(points) {
                _userRequire.then(function(authData) {
                  var c, data, fPts, firstRun_magicNumber, firstRun_maxPolarList, firstRun_pts, maxPolar, minPolar, sPts, secondRun_magicNumber, secondRun_minPolarList, secondRun_pts, sortConfig, sum, userHandle, x, y;
                  userHandle = authData.handle;
                  if ($scope.stampIdent === false && $scope.stamped === true) {
                    $scope.stamped = true;
                    return;
                  }
                  $scope.$broadcast('isValidating', {
                    status: true
                  });
                  data = [];
                  firstRun_pts = _.map(points, function(d, i) {
                    return [d.x, d.y];
                  });
                  secondRun_pts = _.map(points, function(d, i) {
                    return [d.x, d.y];
                  });
                  firstRun_pts = GrahamScan(firstRun_pts, 'forward');
                  secondRun_pts = GrahamScan(secondRun_pts, 'reverse');
                  fPts = _.find(firstRun_pts, function(pointObject) {
                    return !pointObject.polar;
                  });
                  sPts = _.find(secondRun_pts, function(pointObject) {
                    return !pointObject.polar;
                  });
                  firstRun_pts = _.sortByAll(firstRun_pts, ['polar', 'distance']);
                  sortConfig = {
                    polar: "desc",
                    distance: "asc"
                  };
                  secondRun_pts.keySort(sortConfig);
                  firstRun_maxPolarList = [];
                  secondRun_minPolarList = [];
                  minPolar = _.min(secondRun_pts, 'polar');
                  maxPolar = _.max(firstRun_pts, 'polar');
                  $scope.polishedFirstRun_pts = _.filter(firstRun_pts, function(point) {
                    if (point.polar === maxPolar.polar) {
                      firstRun_maxPolarList.push(point);
                      return;
                    }
                    return point;
                  });
                  $scope.polishedSecondRun_pts = _.filter(secondRun_pts, function(point) {
                    if (point.polar === minPolar.polar) {
                      secondRun_minPolarList.push(point);
                      return;
                    }
                    return point;
                  });
                  firstRun_maxPolarList.sort(function(a, b) {
                    return b.distance - a.distance;
                  });
                  secondRun_minPolarList.sort(function(a, b) {
                    return b.distance - a.distance;
                  });
                  $scope.polishedFirstRun_pts.unshift(fPts);
                  $scope.polishedFirstRun_pts.pop();
                  $scope.polishedFirstRun_pts = $scope.polishedFirstRun_pts.concat(firstRun_maxPolarList);
                  $scope.polishedSecondRun_pts = $scope.polishedSecondRun_pts.concat(secondRun_minPolarList);
                  $scope.firstRun_sum = 0;
                  $scope.secondRun_sum = 0;
                  sum = 0;
                  c = 0;
                  for (x in points) {
                    data[x] = [points[x]["x"], points[x]["y"]];
                    x = x.int();
                    if (!_.isNaN(x)) {
                      y = x;
                      if (x >= 0) {
                        y++;
                      }
                      if (x === 4) {
                        y = 4;
                        x = 0;
                      }
                      firstRun_magicNumber = $scope.validateStamp($scope.polishedFirstRun_pts, x, y);
                      secondRun_magicNumber = $scope.validateStamp($scope.polishedSecondRun_pts, x, y);
                      $scope.firstRun_sum += firstRun_magicNumber;
                      $scope.secondRun_sum += secondRun_magicNumber;
                      c++;
                    }
                  }
                  $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 0, 2);
                  $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 1, 3);
                  $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 3, 0);
                  $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 4, 1);
                  $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 4, 2);
                  $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 0, 2);
                  $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 1, 3);
                  $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 3, 0);
                  $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 4, 1);
                  $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 4, 2);
                  $scope.runTotalAllSides = $scope.firstRun_sum + $scope.secondRun_sum;
                  $scope.stampPasses.push({
                    runTotal: $scope.runTotalAllSides,
                    signature: cryptoService.encryptAES($scope.polishedFirstRun_pts.join(';') + '|' + $scope.polishedSecondRun_pts.join(';'), userHandle)
                  });
                  $scope.allLocalValidatorStamps = $scope.localValidatorStamps;
                  if ($scope.stampIdent === false) {

                    /*
                    Bounds checker.
                     */
                    t.stop();
                    $scope._filteredLocalValidatorStamps = _.filter($scope.allLocalValidatorStamps, function(_merchantConstruct) {
                      var boundEnd, boundStart, controlStampBound, errorMarginLower, errorMarginUpper, merchantConstruct, merchantStampActive, merchantStampStatus, passCount, _controlStampBound;
                      $scope.boundsCheck = false;
                      _controlStampBound = 0;
                      errorMarginLower = .985;
                      errorMarginUpper = 1.015;
                      merchantStampStatus = _merchantConstruct.status;
                      merchantConstruct = _merchantConstruct.merchantObject;
                      merchantStampActive = merchantConstruct.active;
                      passCount = merchantConstruct.passes.length;
                      _.forEach(merchantConstruct.passes, function(pass) {
                        return _controlStampBound += pass.runTotal;
                      });
                      controlStampBound = _controlStampBound / passCount;
                      boundStart = $scope.runTotalAllSides > (errorMarginLower * controlStampBound);
                      boundEnd = $scope.runTotalAllSides < (errorMarginUpper * controlStampBound);
                      return $scope.boundsCheck = boundStart && boundEnd && merchantStampStatus && merchantStampActive ? true : false;
                    });
                    if (!_.isEmpty($scope._filteredLocalValidatorStamps)) {
                      $scope.merchantHandle = $scope._filteredLocalValidatorStamps[0].merchantObject.stampOwner;
                      profileService.getUserStatus($scope.merchantHandle).$loaded().then(function(profileData) {
                        var currentDateTime, expiry, expiryDateTime, impression, validationSuccess, _impressionConstruct, _point_data;
                        if ($scope._filteredLocalValidatorStamps && $scope._filteredLocalValidatorStamps.length === 1) {

                          /*
                          Custom validation.
                           */
                          validationSuccess = true;
                          if (!profileData.impressions) {
                            $scope.impressionsUnavailable = true;
                            validationSuccess = false;
                          }
                          _impressionConstruct = _.last(_.toArray(profileData.impressions));
                          if (!_impressionConstruct) {
                            $scope.impressionsUnavailable = true;
                            validationSuccess = false;
                          }
                          impression = parseInt(_impressionConstruct.quantity, 10);
                          expiry = _impressionConstruct.expiry;
                          currentDateTime = moment();
                          expiryDateTime = moment(expiry);
                          if ((currentDateTime.isAfter(expiryDateTime) || currentDateTime.isSame(expiryDateTime)) && _.size($scope._filteredLocalValidatorStamps) === 1) {
                            $scope.expiredStatus = $scope.isExpired = true;
                            validationSuccess = false;
                          }
                          if (impression === 0) {
                            $scope.expiredStatus = $scope.isExpired = true;
                            validationSuccess = false;
                          }
                          if (validationSuccess) {
                            _point_data = sss.util.base64.encode($.toJSON($scope.stampPasses));
                            return $timeout(function() {
                              return $http.post(apiBase + ("/stamp/" + userHandle + "/validate/" + $scope.merchantHandle + "/" + _point_data), {
                                data: sss.util.base64.encode($.toJSON(data)),
                                stampData: _.last($scope._filteredLocalValidatorStamps).merchantObject,
                                createTipUrl: false
                              }).success(function(customValidatedStampReceipt) {
                                console.log(customValidatedStampReceipt);
                                return $scope.$broadcast('localValidation', {
                                  _data: data,
                                  local: false,
                                  customReceipt: customValidatedStampReceipt
                                });
                              }).error(function(errorData) {
                                return console.log(errorData);
                              });
                            }, 1000);
                          }
                        }
                      });
                    } else {
                      console.log('Custom validation failed.');
                    }
                    if ($scope.allLocalValidatorStamps && $scope.allLocalValidatorStamps.length > 0) {

                      /*
                      Failover validation.
                       */
                      $http.post($scope.getStampBySerial, {
                        data: sss.util.base64.encode($.toJSON(data))
                      }).success(function(validationSSS) {
                        var stampOwner, _merchantList, _stampOwner;
                        $scope.validationSSS = validationSSS;
                        if (validationSSS.error) {
                          console.log('Stamp not found');
                          $location.path('/fan/stamp/error');
                          return;
                        }
                        profileService.addValidation(validationSSS);
                        _merchantList = _.filter($scope.allLocalValidatorStamps, function(stampData) {
                          var passingStamp;
                          passingStamp = stampData.merchantObject.receipt && stampData.merchantObject.stampOwner && stampData.merchantObject.active;
                          if (passingStamp) {
                            return stampData.merchantObject.receipt.validation.stamp.serial === validationSSS.stamp.serial;
                          }
                        });
                        if (_.isEmpty(_merchantList)) {
                          console.log('Merchant not found!');
                          $location.path('/fan/stamp/error');
                          return;
                        }
                        _stampOwner = _.last(_merchantList);
                        if (!_stampOwner) {
                          $location.path('/fan/stamp/error');
                          return;
                        }
                        stampOwner = _stampOwner.merchantObject.stampOwner;
                        return profileService.getUserStatus(stampOwner).$loaded().then(function(profileData) {
                          var cb, currentDateTime, expiry, expiryDateTime, impression, _impressionConstruct;
                          if (!profileData.impressions) {
                            $location.path('/fan/stamp/error');
                            $scope.impressionsUnavailable = true;
                            return;
                          }
                          _impressionConstruct = _.last(_.toArray(profileData.impressions));
                          if (!_impressionConstruct) {
                            $scope.impressionsUnavailable = true;
                            $location.path('/fan/stamp/error');
                            return;
                          }
                          impression = parseInt(_impressionConstruct.quantity, 10);
                          expiry = _impressionConstruct.expiry;
                          currentDateTime = moment();
                          expiryDateTime = moment(expiry);
                          if (currentDateTime.isAfter(expiryDateTime) || currentDateTime.isSame(expiryDateTime)) {
                            $scope.expiredStatus = $scope.isExpired = true;
                            $location.path('/fan/stamp/error');
                            return;
                          }
                          if (impression === 0) {
                            $scope.expiredStatus = $scope.isExpired = true;
                            $location.path('/fan/stamp/error');
                            return;
                          }
                          cb = function() {
                            return $http({
                              url: $scope.repostURL,
                              method: 'POST',
                              data: {
                                merchant: stampOwner,
                                username: userHandle,
                                receipt: $scope.validationSSS,
                                createTipUrl: false
                              }
                            }).success(function(v) {
                              var msg;
                              if (v.status && v.status === 'error') {
                                $location.path('/fan/stamp/error');
                                return;
                              }
                              profileService.addValidation(v);
                              msg = v.messageConstruct;
                              $location.path('/fan/stamp/success');
                            }).error(function(reason) {
                              $location.path('/fan/stamp/error');
                            });
                          };
                          purseService.add(cb(), {
                            drag: null
                          }, userHandle);
                        });
                      });
                    }
                  }
                  if ($scope.stampIdent === true) {
                    if ($scope.isValidating === false) {
                      notificationService.displayError('Stamp captured!');
                      $scope.isValidating = true;
                      $scope.$broadcast('localValidation', {
                        handle: userHandle,
                        _data: data,
                        local: true
                      });
                    }
                  }
                });
              });
            },
            any: function(hand, finger) {
              finger.on("start", function() {
                incrementCount(hand);
              });
            }
          });
        }
      };
    };
    __interface__.directive('ngStamp', ['$http', 'user.require', '$timeout', '$interval', '$window', '$q', '$location', 'stamp.service', 'purse.service', 'notification.service', 'profile.service', 'crypto.service', ngStamp]);
  });

}).call(this);

//# sourceMappingURL=stamp.js.map
