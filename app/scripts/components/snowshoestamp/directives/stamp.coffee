###
# fileOverview

            _
      ___ _| |_ _____ ____  ____
     /___|_   _|____ |    \|  _ \
    |___ | | |_/ ___ | | | | |_| |
    (___/   \__)_____|_|_|_|  __/
                           |_|

## description

  Primary logic around stamp impression collection under various use-case scenarios:

  1. Normal Customer Stamp Impression
  2. Control Stamp Impression
  3. Recall Stamp Impression; TBD

###

define [
  'interface'
  'lodash'
  'graham-scan'
  'jquery-json'
  'touchy'
  'services/authentication'
  'services/stamp'
  'services/notification'
  'services/profile'
  'services/crypto'
], (__interface__, _, GrahamScan) ->

  ngStamp = ($http, userRequire, $timeout, $interval, $window, $q, $location, stampService, purseService, notificationService, profileService, cryptoService) ->
    ###
    Custom validation directive.

    @inner
    ###
    restrict: 'A'
    scope:
      app_key      : '=appkey'
      stampIdent   : '=stampident'
      username     : '=username'
      stampPasses  : '=stampPasses'
      stampReceipt : '=stampReceipt'
    link: linker = ($scope, element, attr) ->

      # @note Needful.
      $scope.localValidatorStamps = stampService.localValidatorStamps
      $scope.stampReceipt = null
      $scope.stampPasses = []
      $scope.validStamp = false
      $scope.stamped = false
      body = document.getElementsByTagName("body")[0]
      $scope.isValidating = false
      _timeout = null

      console.log $scope.localValidatorStamps

      signalStampAnimation = (event) ->
        element.removeClass 'bounce-in-left'
        $scope.$on 'isValidating', (event, newState) ->
          $timeout ->
            $scope.$apply () ->
              element.addClass 'stamp--validating'
          , 0

      signalOffStampAnimation = (event) ->
        _timeout = $timeout ->
          element.removeClass 'tada'
          $timeout.cancel _timeout
        , 1000

      angular.element(element[0]).on 'touchstart mouseover', signalStampAnimation
      angular.element(element[0]).on 'touchend mouseout', signalOffStampAnimation

      ###

                   _  _     _                    ______
                  | |(_)   | |        _         / _____) _
       _   _ _____| | _  __| |_____ _| |_ _____( (____ _| |_ _____ ____  ____
      | | | (____ | || |/ _  (____ (_   _) ___ |\____ (_   _|____ |    \|  _ \
       \ V // ___ | || ( (_| / ___ | | |_| ____|_____) )| |_/ ___ | | | | |_| |
        \_/ \_____|\_)_|\____\_____|  \__)_____|______/  \__)_____|_|_|_|  __/
                                                                        |_|

      @namespace validateStamp
      @description

        Pass a point array and a set of coordinates to locate base point.

      @param {array} pointObjects
      @param {number} x
      @param {number} y
      @return {number} We return a planar number sorting into linear time 
                       lying on a parabola, or by "a single sorting step 
                       followed by a linear amount of additional work".
      ###

      $scope.validateStamp = (pointObjects, x, y) ->

        x2 = pointObjects[y][0]
        x1 = pointObjects[x][0]
        y2 = pointObjects[y][1]
        y1 = pointObjects[x][1]

        Math.sqrt(
            (Math.pow(x2 - x1, 2)) +
            (Math.pow(y2 - y1, 2))
          )


      ###

       _                   _ _     _       _  _     _             _
      | |                 | (_)   (_)     | |(_)   | |        _  (_)
      | | ___   ____ _____| |_     _ _____| | _  __| |_____ _| |_ _  ___  ____
      | |/ _ \ / ___|____ | | |   | (____ | || |/ _  (____ (_   _) |/ _ \|  _ \
      | | |_| ( (___/ ___ | |\ \ / // ___ | || ( (_| / ___ | | |_| | |_| | | | |
       \_)___/ \____)_____|\_)\___/ \_____|\_)_|\____\_____|  \__)_|\___/|_| |_|

      @description

        Accepting the following "STAMP SCREEN" scenarios:

        1. stamp/id
        2. stamp/
        3. stamp/recall

      ###

      $scope.$on 'localValidation', (event, validationObject) ->

        # @comment The core assumption here is that localValidation events 
        #          will necessarily involve handle passing of names across SSL POST objects.
        #          This is crucial in determining the type of "STAMP SCREEN" namespace we expose to 
        #          clients who can never be trusted with trustless systems unless their frequency 
        #          of authentication can be distributed across a performative network that is equally 
        #          decollocated in terms of decentralized executions over hardware implementations scopes.
        _handle = validationObject.handle
        data = validationObject._data
        local = validationObject.local
        customValidatedStampReceipt = validationObject.customReceipt or null

        if _handle
          if local
            _handle = 'local/' + _handle
          else
            _handle = 'success/' + _handle

          $http.
            post($scope.callbackURL + _handle,
              data: sss.util.base64.encode($.toJSON(data))
              createTipUrl: false
            ).

              success((validationSSS) ->
                # {
                #   "validation": {
                #     "stamp"    : {
                #         "serial" : "SERIAL"
                #       },
                #     "receipt"  : "RECEIPT",
                #     "secure"   : BOOLEAN,
                #     "created"  : "DATETIME"
                #   }
                # }

                if local and validationSSS and validationSSS.validation and validationSSS.validation.stamp.serial
                  profileService.addValidation validationSSS

                  $timeout ->
                    $scope.$apply () ->
                      $scope.stampReceipt = validationSSS
                  , 0

                  if $location.$$path.indexOf('recall') isnt -1
                    notificationService.displayError('SSS Serial: ' + validationSSS.validation.stamp.serial)
                  else
                    notificationService.displayError 'Thanks!'
                else
                  notificationService.displayError 'Reload / Try again!'

                $scope.isValidating = false
              ).

              error (errorData) ->
                notificationService.displayError 'Please try again!'
                $scope.isValidating = false

        # @comment This seems to be a declarative implementation for the 
        #          state flow implementation of stampData.
        #if customValidatedStampReceipt

          #_id = customValidatedStampReceipt
          #_handle = _id
          #console.log _id

          #$http.

            #post($scope.callbackURL + 'success' + '/' + _handle,
              #data: sss.util.base64.encode($.toJSON(data))
            #).

              #success((validationSSS) ->

                #profileService.addValidation validationSSS

                #$scope.customerStampedReceipt = validationSSS

                #notificationService.displayError 'Thanks!'
              #).

              #error((errorData) ->
                #$location.path('/fan/stamp/error')
              #)

      $scope.username = null

      portSpec = if $window.location.port != "" then (':' + $window.location.port) else ''
      apiPrefix = if window.is_device or portSpec == '' then '/data' else ''
      apiBaseConstruct = ($window.location.protocol + '//' + $window.location.hostname + portSpec + apiPrefix)
      apiProductionConstruct = 'https://lovestamp.io/data'
      apiBase = if window.is_device is true then apiProductionConstruct else apiBaseConstruct

      _userRequire = userRequire()

      _userRequire.then (authData) ->

        $scope.user = authData
        handle = authData.handle
        $scope.username = authData[authData.provider].username
        p = authData.provider
        $scope.username_prefix = if p == 'twitter' then '@' else if p == 'google' then '✚' else 'ᶠ'
        $scope._username = $scope.username_prefix + $scope.username
        $scope.callbackURL = apiBase + '/stamp/' + $scope._username + '/'
        $scope.getStampBySerial = apiBase + '/stamp/get-serial'
        $scope.repostURL = apiBase + '/stamp/repost'
        $scope.errorURL = apiBase + '/stamp/' + $scope._username + '/error'

      $scope.runTotalBounds = 0

      # @inner
      errorCheck = (number) ->
        return

        $window.location = $scope.errorURL  if insufficientCount > 2

        timeouts[number] = $timeout(->
          $window.location = $scope.errorURL
          return
        , 2000)

        errorStart = new Date()

        $window.location = $scope.errorURL  if errorTime > 2000

        return

      # @inner
      endErrorCheck = (number) ->
        errorTime += new Date() - errorStart
        clearTimeout timeouts[number]
        return

      # @inner
      incrementCount = (_hand) ->
        def = $q.defer()
        $timeout (->
          def.resolve(touchDone = true)
          return
        ), 30
        if touchDone is true
          insufficientCount++
          def.reject(touchDone = false)
        return

      errorStart = undefined
      errorTime = 0

      insufficientCount = 0
      touchDone = false
      timeouts = {}

      t = null

      $scope.$on('$locationChangeStart', (event, oldUrl, newUrl) ->
        #t.stop()
      )

      # Implement fallback model which
      # 1. Runs our client side bound check which first pulls the stamp ident data from the Firebase store of stamps within /users/stamps/ids/
      # 2. Fall back to sss validation which will post to our success callback
      # 3. If client side validation is passed then, we will call the same success callback but with different but similar data points.
      # 4. (But we should pass our "stampIdent" param to inform the endpoint of what its logic should be.)

      t = Touchy body,

        one: (hand, finger) ->
          hand.on "start", (points) ->
            errorCheck 1
            return
          hand.on "end", (points) ->
            endErrorCheck 1
            return
          return

        two: (hand, finger1, finger2) ->
          hand.on "start", (points) ->
            errorCheck 2
            return
          hand.on "end", (points) ->
            endErrorCheck 2
            return
          return

        three: (hand, finger1, finger2, finger3) ->
          hand.on "start", (points) ->
            errorCheck 3
            return
          hand.on "end", (points) ->
            endErrorCheck 3
            return
          return

        four: (hand, finger1, finger2, finger3, finger4) ->
          hand.on "start", (points) ->
            errorCheck 4
            return
          hand.on "end", (points) ->
            endErrorCheck 4
            return
          return

        five: (hand, finger1, finger2, finger3, finger4, finger5) ->

          stampValidation = {}
          stampValidation.validity = []

          #console.log $scope

          if $scope.stampIdent is false and $scope.stamped == true
            return

          hand.on "start", (points) ->
            _userRequire.then (authData) ->

              userHandle = authData.handle

              if $scope.stampIdent is false and $scope.stamped == true
                $scope.stamped = true
                return

              $scope.$broadcast 'isValidating', status: true

              data = []

              firstRun_pts = _.map points, (d, i) ->
                [d.x, d.y]
              secondRun_pts = _.map points, (d, i) ->
                [d.x, d.y]

              # Max/Minima Reflection
              firstRun_pts = GrahamScan(firstRun_pts, 'forward')
              secondRun_pts = GrahamScan(secondRun_pts, 'reverse')

              fPts = _.find firstRun_pts, (pointObject) -> ! pointObject.polar
              sPts = _.find secondRun_pts, (pointObject) -> ! pointObject.polar
              firstRun_pts = _.sortByAll(firstRun_pts, ['polar', 'distance'])

              sortConfig =
                polar:  "desc"
                distance: "asc"
              secondRun_pts.keySort(sortConfig)

              firstRun_maxPolarList = []
              secondRun_minPolarList = []
              minPolar = _.min(secondRun_pts, 'polar')
              maxPolar = _.max(firstRun_pts, 'polar')

              $scope.polishedFirstRun_pts = _.filter firstRun_pts, (point) ->
                if point.polar == maxPolar.polar
                  firstRun_maxPolarList.push point
                  return
                return point

              $scope.polishedSecondRun_pts = _.filter secondRun_pts, (point) ->
                if point.polar == minPolar.polar
                  secondRun_minPolarList.push point
                  return
                return point

              firstRun_maxPolarList.sort (a, b) ->
                b.distance - a.distance
              secondRun_minPolarList.sort (a, b) ->
                b.distance - a.distance

              $scope.polishedFirstRun_pts.unshift fPts
              $scope.polishedFirstRun_pts.pop()
              $scope.polishedFirstRun_pts = $scope.polishedFirstRun_pts.concat firstRun_maxPolarList
              $scope.polishedSecondRun_pts = $scope.polishedSecondRun_pts.concat secondRun_minPolarList
              $scope.firstRun_sum = 0
              $scope.secondRun_sum = 0
              sum = 0
              c = 0

              for x of points

                data[x] = [
                  points[x]["x"]
                  points[x]["y"]
                ]

                x = x.int()

                if ! _.isNaN(x)
                  y = x
                  if x >= 0
                    y++
                  if x == 4
                    y = 4
                    x = 0

                  firstRun_magicNumber = $scope.validateStamp($scope.polishedFirstRun_pts, x, y)
                  secondRun_magicNumber = $scope.validateStamp($scope.polishedSecondRun_pts, x, y)

                  $scope.firstRun_sum += firstRun_magicNumber
                  $scope.secondRun_sum += secondRun_magicNumber
                  c++

              $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 0, 2)
              $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 1, 3)
              $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 3, 0)
              $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 4, 1)
              $scope.firstRun_sum += $scope.validateStamp($scope.polishedFirstRun_pts, 4, 2)

              $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 0, 2)
              $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 1, 3)
              $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 3, 0)
              $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 4, 1)
              $scope.secondRun_sum += $scope.validateStamp($scope.polishedSecondRun_pts, 4, 2)

              $scope.runTotalAllSides = $scope.firstRun_sum + $scope.secondRun_sum

              # For stamp control creation.
              $scope.stampPasses.push
                runTotal: $scope.runTotalAllSides
                signature: cryptoService.encryptAES($scope.polishedFirstRun_pts.join(';') + '|' + $scope.polishedSecondRun_pts.join(';'), userHandle)

              # @unit.example
              #   Upperbound: 3878.315
              #   Lowerbound: 3763.685
              #   Input:      3821

              $scope.allLocalValidatorStamps = $scope.localValidatorStamps

              if $scope.stampIdent is false
                ###
                Bounds checker.
                ###

                t.stop()

                # Filter merchants by bounds check.
                $scope._filteredLocalValidatorStamps = _.filter $scope.allLocalValidatorStamps, (_merchantConstruct) ->

                  $scope.boundsCheck = false
                  _controlStampBound = 0
                  errorMarginLower = .985
                  errorMarginUpper = 1.015

                  merchantStampStatus = _merchantConstruct.status # timecard status
                  merchantConstruct = _merchantConstruct.merchantObject
                  merchantStampActive = merchantConstruct.active # manual active status
                  passCount = merchantConstruct.passes.length

                  _.forEach merchantConstruct.passes, (pass) ->
                    _controlStampBound += pass.runTotal

                  controlStampBound = _controlStampBound / passCount

                  # To determine uniqueness of a particular LoveStamp.
                  boundStart = $scope.runTotalAllSides > (errorMarginLower * controlStampBound)
                  boundEnd = $scope.runTotalAllSides < (errorMarginUpper * controlStampBound)

                  $scope.boundsCheck = if (boundStart and boundEnd and merchantStampStatus and merchantStampActive) then true else false

                if !_.isEmpty($scope._filteredLocalValidatorStamps)

                  $scope.merchantHandle = $scope._filteredLocalValidatorStamps[0].merchantObject.stampOwner

                  profileService.getUserStatus($scope.merchantHandle).$loaded().then (profileData) ->

                    # If one unique merchant, validate against customer prepared 
                    # endpoint.
                    if $scope._filteredLocalValidatorStamps and $scope._filteredLocalValidatorStamps.length is 1
                    #if $scope.stampIdent is false and $scope._filteredLocalValidatorStamps
                      ###
                      Custom validation.
                      ###

                      validationSuccess = true

                      if !profileData.impressions
                        $scope.impressionsUnavailable = true
                        validationSuccess = false

                      _impressionConstruct = _.last(_.toArray(profileData.impressions))

                      if !_impressionConstruct
                        $scope.impressionsUnavailable = true
                        validationSuccess = false

                      impression = parseInt(_impressionConstruct.quantity, 10)
                      expiry = _impressionConstruct.expiry

                      currentDateTime = moment()
                      expiryDateTime = moment(expiry)

                      if (currentDateTime.isAfter( expiryDateTime ) or currentDateTime.isSame( expiryDateTime )) and _.size($scope._filteredLocalValidatorStamps) == 1
                        $scope.expiredStatus = $scope.isExpired = true
                        #return
                        validationSuccess = false

                      if impression is 0
                        $scope.expiredStatus = $scope.isExpired = true
                        validationSuccess = false

                      if validationSuccess 
                        _point_data = sss.
                          util.
                          base64.
                          encode $.toJSON($scope.stampPasses)

                        $timeout ->
                          $http.

                            post(apiBase + "/stamp/#{userHandle}/validate/#{$scope.merchantHandle}/#{_point_data}",
                              data         : sss.util.base64.encode($.toJSON(data))
                              stampData    : _.last($scope._filteredLocalValidatorStamps).merchantObject
                              createTipUrl : false
                            ).

                              success((customValidatedStampReceipt) ->
                                console.log customValidatedStampReceipt

                                $scope.$broadcast 'localValidation',
                                  _data         : data
                                  local         : false
                                  customReceipt : customValidatedStampReceipt

                                #profileService.addValidation customValidatedStampReceipt

                                #pending = customValidatedStampReceipt.pending

                                #notificationService.displayError 'Thanks!'

                                #if pending
                                  #$location.path('/stamp/pending')
                                #else
                                  #$location.path('/stamp/success')

                            ).

                            error (errorData) ->
                              console.log errorData
                        , 1000

                    # 1. If no merchants found, validate using SS API with all 
                    # merchants in geolist.
                    # 2. If no merchants found, validate using SS API with distance 
                    # filtered merchants in geolist and subset of custom 
                    # validation applied.
                else
                  console.log 'Custom validation failed.'

                if $scope.allLocalValidatorStamps and $scope.allLocalValidatorStamps.length > 0
                #if $scope.stampIdent is false and $scope._filteredLocalValidatorStamps
                  ###
                  Failover validation.
                  ###

                  $http.post($scope.getStampBySerial, {
                      data: sss.util.base64.encode($.toJSON(data))
                  }).success((validationSSS) ->

                      $scope.validationSSS = validationSSS

                      if validationSSS.error
                        console.log 'Stamp not found'
                        $location.path('/fan/stamp/error')
                        return

                      profileService.addValidation validationSSS

                      # Pass merchant from front end geolist to backend repost URL
                      #_merchantList = _.filter $scope.allLocalValidatorStamps, (stampData) ->
                      _merchantList = _.filter $scope.allLocalValidatorStamps, (stampData) ->
                        passingStamp = stampData.merchantObject.receipt and stampData.merchantObject.stampOwner and stampData.merchantObject.active
                        if passingStamp
                          return stampData.merchantObject.receipt.validation.stamp.serial == validationSSS.stamp.serial

                      if _.isEmpty(_merchantList)
                        console.log 'Merchant not found!'
                        $location.path('/fan/stamp/error')
                        return

                      #if $scope.isExpired
                        #console.log 'Stamp has expired!'
                        #$location.path('/fan/stamp/error')
                        #return

                      _stampOwner = _.last(_merchantList)

                      if !_stampOwner
                        $location.path('/fan/stamp/error')
                        return

                      stampOwner = _stampOwner.merchantObject.stampOwner

                      profileService.getUserStatus(stampOwner).$loaded().then (profileData) ->

                        if !profileData.impressions
                          $location.path('/fan/stamp/error')
                          $scope.impressionsUnavailable = true
                          return

                        _impressionConstruct = _.last(_.toArray(profileData.impressions))

                        if !_impressionConstruct
                          $scope.impressionsUnavailable = true
                          $location.path('/fan/stamp/error')
                          return

                        impression = parseInt(_impressionConstruct.quantity, 10)
                        expiry = _impressionConstruct.expiry

                        currentDateTime = moment()
                        expiryDateTime = moment(expiry)

                        if currentDateTime.isAfter( expiryDateTime ) or currentDateTime.isSame( expiryDateTime )
                          $scope.expiredStatus = $scope.isExpired = true
                          $location.path('/fan/stamp/error')
                          return

                        if impression is 0
                          $scope.expiredStatus = $scope.isExpired = true
                          $location.path('/fan/stamp/error')
                          return

                        cb = () ->
                          return $http({
                            url: $scope.repostURL
                            method: 'POST'
                            data:
                              merchant     : stampOwner
                              username     : userHandle
                              receipt      : $scope.validationSSS
                              createTipUrl : false
                          }).success((v) ->

                            if v.status and v.status == 'error'
                              $location.path('/fan/stamp/error')
                              return

                            profileService.addValidation v

                            msg = v.messageConstruct

                            #status_display = v.body.tip.status_display # "Delivered" || "Out for Delivery" || "Tip Delivery Bug"

                            #if status_display != 'Delivered'
                              #$location.path('/stamp/pending')
                            #else

                            $location.path('/fan/stamp/success')

                            return

                          ).error (reason) ->
                            $location.path '/fan/stamp/error'
                            return

                        purseService.add(cb(), drag: null, userHandle)
                        return
                  )

              if $scope.stampIdent is true

                if $scope.isValidating == false
                  notificationService.displayError 'Stamp captured!'
                  $scope.isValidating = true
                  $scope.$broadcast 'localValidation',
                    handle : userHandle # Use currently logged in user as assignee
                    _data  : data
                    local  : true

              return
            return
          return

        any: (hand, finger) ->

          finger.on "start", ->

            incrementCount(hand)
            return
          return

      return

  __interface__

    .directive 'ngStamp', [
      '$http'
      'user.require'
      '$timeout'
      '$interval'
      '$window'
      '$q'
      '$location'
      'stamp.service'
      'purse.service'
      'notification.service'
      'profile.service'
      'crypto.service'
      ngStamp
    ]

  return
