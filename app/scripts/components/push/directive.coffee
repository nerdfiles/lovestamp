###
# fileOverview

                      _     _                 _
                     (_)   (_)               (_)
     _____ ____  ____ _     _ _____  ____ ___ _  ___  ____
    (____ |  _ \|  _ \ |   | | ___ |/ ___)___) |/ _ \|  _ \
    / ___ | |_| | |_| \ \ / /| ____| |  |___ | | |_| | | | |
    \_____|  __/|  __/ \___/ |_____)_|  (___/|_|\___/|_| |_|
          |_|   |_|

## description

A simple application version display.

@usage

    <div device-push-notification></div>

###

define [
    "interface"
], (__interface__) ->

    pushNotificationCtrl = ($scope, $cordovaPush, $cordovaDialogs, $cordovaMedia, $cordovaToast, $http, $timeout) ->

      handleAndroid = (notification) ->

        # ** NOTE: ** You could add code for when app is in foreground or not, or coming from coldstart here too
        #                         via the console fields as shown.
        console.log "In foreground " + notification.foreground + " Coldstart " + notification.coldstart

        if notification.event is "registered"
          $scope.regId = notification.regid
          storeDeviceToken "android"

        else if notification.event is "message"
          $cordovaDialogs.alert notification.message, "LoveStamper nearby. Get stamped!"
          $scope.$apply ->
            $scope.notifications.push JSON.stringify(notification.message)
            return

        else if notification.event is "error"
          $cordovaDialogs.alert notification.msg, "Push notification error event"
        else
          $cordovaDialogs.alert notification.event, "Push notification handler - Unprocessed Event"
        return

      handleIOS = (notification) ->

        mlib =
          message1 : "LoveStamper nearby. Go get some love!"
          message2 : "LoveStamper nearby. Get stamped!"
          message3 : "LoveStamper nearby. Choose the red pill!"
          message4 : "LoveStamper nearby. Get impressed!"
          message5 : "LoveStamper nearby. Don't get Trumped, get LoveStamped!"
          message6 : "LoveStamper nearby. Get stamped and make America great again!"
          message7 : "LoveStamper nearby. Follow the blue line Neo!"
          message8 : 'LoveStamper nearby. Be impressed!'
          message9 : 'LoveStamper nearby. Get stamped!'
          message10 : 'LoveStamper nearby. Go get some love!'
          message11 : 'LoveStamper nearby. Get stamped and make America great again!'
          message12 : 'LoveStamper nearby. Ain\'t everybody got time for that!'
          message13 : 'LoveStamper nearby. Yo mama got stamped already!'
        messageList = _.toArray(mlib)

        if notification.foreground is "1"

          if notification.sound
            mediaSrc = $cordovaMedia.newMedia(notification.sound)
            mediaSrc.promise.then $cordovaMedia.play(mediaSrc.media)

          if notification.body and notification.messageFrom
            $cordovaDialogs.alert messageList.randomPrint(), notification.messageFrom
          else
            #$cordovaDialogs.alert notification.alert, messageList.randomPrint()
            $cordovaDialogs.alert messageList.randomPrint(), notification.body

          $scope.$broadcast('notify', () ->
            if notification.body and notification.messageFrom
              $cordovaDialogs.alert messageList.randomPrint(), notification.messageFrom
            else
              #$cordovaDialogs.alert notification.alert, messageList.randomPrint()
              $cordovaDialogs.alert messageList.randomPrint(), notification.body
          )

          if notification.badge
            $cordovaPush.setBadgeNumber(notification.badge).then ((result) ->
              console.log "Set badge success " + result
              return
            ), (err) ->
              console.log "Set badge error " + err
              return

          #if notification.body and notification.messageFrom
            #$cordovaDialogs.alert notification.body, "(RECEIVED WHEN APP IN BACKGROUND) " + notification.messageFrom
          #else
            #$cordovaDialogs.alert notification.alert, "(RECEIVED WHEN APP IN BACKGROUND) Push Notification Received"
        return

      # @type: ios, android
      storeDeviceToken = (type) ->

        # Create a random userid to store with it
        user =
          user  : "user" + Math.floor((Math.random() * 10000000) + 1)
          type  : type
          token : $scope.regId

        console.log "Post token for registered device with data " + JSON.stringify(user)

        #$http.post("http://" + $window.location.hostname + "/subscribe", JSON.stringify(user)).success((data, status) ->
        h = $window.location.hostname
        h = 'local.lovestamp.io:8080'
        $http.post("https://" + h + "/push/apn", JSON.stringify(user)).success((data, status) ->
          console.log "Token stored, device is successfully subscribed to receive push notifications."
          return
        ).error (data, status) ->
          console.log "Error storing device token." + data + " " + status
          return

        return

      removeDeviceToken = ->

        tkn = token: $scope.regId
        $http.post("https://" + $window.location.hostname + "/unsubscribe", JSON.stringify(tkn)).success((data, status) ->
          console.log "Token removed, device is successfully unsubscribed and will not receive push notifications."
          return
        ).error (data, status) ->
          console.log "Error removing device token." + data + " " + status
          return

        return

      $scope.notifications = []

      $scope.register = () ->

        if ionic.Platform.isAndroid()
          # Server API Key: AIzaSyCmTg7xqTWCCfvxWom-D7pNg1pEQLvkVZY
          # Sender ID: 179206523209
          config =
            senderID: "179206523209" # https://console.developers.google.com/project/179206523209

        else if ionic.Platform.isIOS()
          config =
            badge: "false"
            sound: "false"
            alert: "true"

        $cordovaPush.register(config).then ((result) ->

          console.log "Register success " + result
          $cordovaToast.showShortCenter "Registered for push notifications"
          $scope.registerDisabled = true

          if ionic.Platform.isIOS()
            console.log 'storeDeviceToken'
            console.log result
            $scope.regId = result
            storeDeviceToken "ios"
          return

        ), (err) ->
          console.log "Register error " + err
          return

        return

      $timeout ->
        document.addEventListener("deviceready", () ->
          #$scope.register()
          console.log 'deviceready'
        , false)
      , 500

      $scope.$on "$cordovaPush:notificationReceived", (event, notification) ->

        #console.log JSON.stringify([notification])

        if ionic.Platform.isAndroid()
          handleAndroid notification

        else if ionic.Platform.isIOS()
          handleIOS notification

          #$scope.$apply ->
            #$scope.notifications.push JSON.stringify(notification.alert)
            #return

        return

      $scope.unregister = ->
        console.log "Unregister called"
        removeDeviceToken()
        $scope.registerDisabled = false
        return

      $scope.$on '$locationChangeStart', (e, newUrl) ->
        if newUrl and newUrl.indexOf('logout') != -1
          $scope.unregister()

      return

    devicePushNotification = ($http) ->
      ###
      Device Push Directive

      @param {service} $http
      ###

      linker = ($scope, element) ->
        ###
        @inner
        @param {object} $scope
        @param {object} element
        ###

        mAppInBackground = false
        document.addEventListener('pause', () -> mAppInBackground = true )
        document.addEventListener('resume', () -> mAppInBackground = false )

        BGN = null

        document.addEventListener 'deviceready', () ->

          window.plugin.notification.local.promptForPermission()

          #BGN = window.plugins.backgroundNotification

          #notificationCallback = (_notification) ->
            #console.log _notification
            #$scope.$on 'notify', (event, cb) ->
              #cb()

            #BGN.finish()

          #BGN.configure(notificationCallback)

        console.log $scope
        return

      restrict   : 'A'
      link       : linker
      controller : 'pushNotificationCtrl'

    __interface__
      .controller "pushNotificationCtrl", [
        '$scope'
        '$cordovaPush'
        '$cordovaDialogs'
        '$cordovaMedia'
        '$cordovaToast'
        '$http'
        '$timeout'
        pushNotificationCtrl
      ]

    __interface__
      .directive "devicePushNotification", [
        '$http'
        devicePushNotification
      ]

    return

