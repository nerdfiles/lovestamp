
/*
 * fileOverview

                      _     _                 _
                     (_)   (_)               (_)
     _____ ____  ____ _     _ _____  ____ ___ _  ___  ____
    (____ |  _ \|  _ \ |   | | ___ |/ ___)___) |/ _ \|  _ \
    / ___ | |_| | |_| \ \ / /| ____| |  |___ | | |_| | | | |
    \_____|  __/|  __/ \___/ |_____)_|  (___/|_|\___/|_| |_|
          |_|   |_|

 *# description

A simple application version display.

@usage

    <div device-push-notification></div>
 */

(function() {
  define(["interface"], function(__interface__) {
    var devicePushNotification, pushNotificationCtrl;
    pushNotificationCtrl = function($scope, $cordovaPush, $cordovaDialogs, $cordovaMedia, $cordovaToast, $http, $timeout) {
      var handleAndroid, handleIOS, removeDeviceToken, storeDeviceToken;
      handleAndroid = function(notification) {
        console.log("In foreground " + notification.foreground + " Coldstart " + notification.coldstart);
        if (notification.event === "registered") {
          $scope.regId = notification.regid;
          storeDeviceToken("android");
        } else if (notification.event === "message") {
          $cordovaDialogs.alert(notification.message, "LoveStamper nearby. Get stamped!");
          $scope.$apply(function() {
            $scope.notifications.push(JSON.stringify(notification.message));
          });
        } else if (notification.event === "error") {
          $cordovaDialogs.alert(notification.msg, "Push notification error event");
        } else {
          $cordovaDialogs.alert(notification.event, "Push notification handler - Unprocessed Event");
        }
      };
      handleIOS = function(notification) {
        var mediaSrc, messageList, mlib;
        mlib = {
          message1: "LoveStamper nearby. Go get some love!",
          message2: "LoveStamper nearby. Get stamped!",
          message3: "LoveStamper nearby. Choose the red pill!",
          message4: "LoveStamper nearby. Get impressed!",
          message5: "LoveStamper nearby. Don't get Trumped, get LoveStamped!",
          message6: "LoveStamper nearby. Get stamped and make America great again!",
          message7: "LoveStamper nearby. Follow the blue line Neo!",
          message8: 'LoveStamper nearby. Be impressed!',
          message9: 'LoveStamper nearby. Get stamped!',
          message10: 'LoveStamper nearby. Go get some love!',
          message11: 'LoveStamper nearby. Get stamped and make America great again!',
          message12: 'LoveStamper nearby. Ain\'t everybody got time for that!',
          message13: 'LoveStamper nearby. Yo mama got stamped already!'
        };
        messageList = _.toArray(mlib);
        if (notification.foreground === "1") {
          if (notification.sound) {
            mediaSrc = $cordovaMedia.newMedia(notification.sound);
            mediaSrc.promise.then($cordovaMedia.play(mediaSrc.media));
          }
          if (notification.body && notification.messageFrom) {
            $cordovaDialogs.alert(messageList.randomPrint(), notification.messageFrom);
          } else {
            $cordovaDialogs.alert(messageList.randomPrint(), notification.body);
          }
          $scope.$broadcast('notify', function() {
            if (notification.body && notification.messageFrom) {
              return $cordovaDialogs.alert(messageList.randomPrint(), notification.messageFrom);
            } else {
              return $cordovaDialogs.alert(messageList.randomPrint(), notification.body);
            }
          });
          if (notification.badge) {
            $cordovaPush.setBadgeNumber(notification.badge).then((function(result) {
              console.log("Set badge success " + result);
            }), function(err) {
              console.log("Set badge error " + err);
            });
          }
        }
      };
      storeDeviceToken = function(type) {
        var h, user;
        user = {
          user: "user" + Math.floor((Math.random() * 10000000) + 1),
          type: type,
          token: $scope.regId
        };
        console.log("Post token for registered device with data " + JSON.stringify(user));
        h = $window.location.hostname;
        h = 'local.lovestamp.io:8080';
        $http.post("https://" + h + "/push/apn", JSON.stringify(user)).success(function(data, status) {
          console.log("Token stored, device is successfully subscribed to receive push notifications.");
        }).error(function(data, status) {
          console.log("Error storing device token." + data + " " + status);
        });
      };
      removeDeviceToken = function() {
        var tkn;
        tkn = {
          token: $scope.regId
        };
        $http.post("https://" + $window.location.hostname + "/unsubscribe", JSON.stringify(tkn)).success(function(data, status) {
          console.log("Token removed, device is successfully unsubscribed and will not receive push notifications.");
        }).error(function(data, status) {
          console.log("Error removing device token." + data + " " + status);
        });
      };
      $scope.notifications = [];
      $scope.register = function() {
        var config;
        if (ionic.Platform.isAndroid()) {
          config = {
            senderID: "179206523209"
          };
        } else if (ionic.Platform.isIOS()) {
          config = {
            badge: "false",
            sound: "false",
            alert: "true"
          };
        }
        $cordovaPush.register(config).then((function(result) {
          console.log("Register success " + result);
          $cordovaToast.showShortCenter("Registered for push notifications");
          $scope.registerDisabled = true;
          if (ionic.Platform.isIOS()) {
            console.log('storeDeviceToken');
            console.log(result);
            $scope.regId = result;
            storeDeviceToken("ios");
          }
        }), function(err) {
          console.log("Register error " + err);
        });
      };
      $timeout(function() {
        return document.addEventListener("deviceready", function() {
          return console.log('deviceready');
        }, false);
      }, 500);
      $scope.$on("$cordovaPush:notificationReceived", function(event, notification) {
        if (ionic.Platform.isAndroid()) {
          handleAndroid(notification);
        } else if (ionic.Platform.isIOS()) {
          handleIOS(notification);
        }
      });
      $scope.unregister = function() {
        console.log("Unregister called");
        removeDeviceToken();
        $scope.registerDisabled = false;
      };
      $scope.$on('$locationChangeStart', function(e, newUrl) {
        if (newUrl && newUrl.indexOf('logout') !== -1) {
          return $scope.unregister();
        }
      });
    };
    devicePushNotification = function($http) {

      /*
      Device Push Directive
      
      @param {service} $http
       */
      var linker;
      linker = function($scope, element) {

        /*
        @inner
        @param {object} $scope
        @param {object} element
         */
        var BGN, mAppInBackground;
        mAppInBackground = false;
        document.addEventListener('pause', function() {
          return mAppInBackground = true;
        });
        document.addEventListener('resume', function() {
          return mAppInBackground = false;
        });
        BGN = null;
        document.addEventListener('deviceready', function() {
          return window.plugin.notification.local.promptForPermission();
        });
        console.log($scope);
      };
      return {
        restrict: 'A',
        link: linker,
        controller: 'pushNotificationCtrl'
      };
    };
    __interface__.controller("pushNotificationCtrl", ['$scope', '$cordovaPush', '$cordovaDialogs', '$cordovaMedia', '$cordovaToast', '$http', '$timeout', pushNotificationCtrl]);
    __interface__.directive("devicePushNotification", ['$http', devicePushNotification]);
  });

}).call(this);

//# sourceMappingURL=directive.js.map
