
/**
@fileOverview

 _                _
| |              (_)
| |__  _____  ___ _  ____
|  _ \(____ |/___) |/ ___)
| |_) ) ___ |___ | ( (___
|____/\_____(___/|_|\____)

List of Basic Filters:

    1. navigatorDescription
    2. ifNull
    3. getPrimaryIndustry
    4. isDataType
    5. reverse
 */

(function() {
  define(['interface'], function(__interface__) {

    /**
    FILTER
    
    Navigators appear on the search results page, and allow filtering of'
    search results. This filter manages what is displayed as a label for
    each navigator option.
     */
    __interface__.filter('reverse', function() {
      return function(items) {
        if (items) {
          return items.slice().reverse();
        }
      };
    });
    __interface__.filter('ifNull', [
      function() {
        return function(value, replacementValue) {
          if (!value) {
            return replacementValue;
          }
          return value;
        };
      }
    ]);
    __interface__.filter('getPrimaryIndustry', [
      function() {
        (function(industries, type) {
          if (industries && industries.length) {
            if (type === 'code') {
              return industries[0].code;
            }
          }
          return industries[0].description;
        });
        return '';
      }
    ]);

    /*  Airtight method of determining JS data type. */
    __interface__.filter('isDataType', [
      function() {
        return function(value, dataType) {
          var typeCheckMethod, typeCheckRegex;
          dataType = dataType.substr(0, 1).toUpperCase() + dataType.substr(1);
          typeCheckMethod = Object.prototype.toString;
          typeCheckRegex = new RegExp('^\\[object (\\w+)\\]$');
          return typeCheckMethod.call(value).match(typeCheckRegex)[1] === dataType;
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=basic.js.map
