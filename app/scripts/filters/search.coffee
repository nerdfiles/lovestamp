###*
@fileOverview


List of Search Filters:

    1. stampository

###

define [ 'interface' ], (__interface__) ->

  ###*
  FILTER

  Navigators appear on the search results page, and allow filtering of'
  search results. This filter manages what is displayed as a label for
  each navigator option.
  ###

  __interface__.filter 'search', ->
    # @note Consider flipping these searchers, or lexically loosening it altogether.
    (input, search) ->
      out = []
      temp = []

      if search and search.queryAlias
        angular.forEach input, (e) ->
          if e.localStamp.alias.indexOf(search.queryAlias) >= 0
            temp.push e
          return
      else
        temp = input

      out = temp

      if search and search.queryOwner
        out = []
        angular.forEach temp, (e) ->
          if e.user.$id.indexOf(search.queryOwner) >= 0
            out.push e
          return
      out

  ###* Search Filter ###

  __interface__.filter 'searchMerchant', ->
    (entities, searchText) ->
      if searchText == undefined
          return entities
      result = []
      i = 0
      while i < entities.length
          if entities[i].merchantName == searchText
              result.push entities[i]
        ++i
      result

  return

