###*
@fileOverview

 _                _
| |              (_)
| |__  _____  ___ _  ____
|  _ \(____ |/___) |/ ___)
| |_) ) ___ |___ | ( (___
|____/\_____(___/|_|\____)

List of Basic Filters:

    1. navigatorDescription
    2. ifNull
    3. getPrimaryIndustry
    4. isDataType
    5. reverse

###

define [ 'interface' ], (__interface__) ->

  ###*
  FILTER

  Navigators appear on the search results page, and allow filtering of'
  search results. This filter manages what is displayed as a label for
  each navigator option.
  ###

  __interface__.filter 'reverse', ->
    (items) ->
      if items
          return items.slice().reverse()
      return
  __interface__.filter 'ifNull', [ ->
    (value, replacementValue) ->
      if !value
          return replacementValue
      value
  ]
  __interface__.filter 'getPrimaryIndustry', [ ->
    (industries, type) ->
      if industries and industries.length
          if type == 'code'
              return industries[0].code
        return industries[0].description
    ''
  ]

  ###  Airtight method of determining JS data type. ###

  __interface__.filter 'isDataType', [ ->
    (value, dataType) ->
      dataType = dataType.substr(0, 1).toUpperCase() + dataType.substr(1)
      typeCheckMethod = Object::toString
      typeCheckRegex = new RegExp('^\\[object (\\w+)\\]$')
      typeCheckMethod.call(value).match(typeCheckRegex)[1] == dataType
  ]

  return
