###*
@fileOverview

     _
    | |        _
  __| |_____ _| |_ _____
 / _  (____ (_   _) ___ |
( (_| / ___ | | |_| ____|
 \____\_____|  \__)_____)

@description

List of Date Filters:

    1. universalToUsDate
    2. universalToUsTime
    3. universalToEpochTime

###

define [
  "interface"
  "moment"
], (__interface__) ->

  __interface__.

  filter("universalToUsDate", [
    "$filter"
    ($filter) ->
      return (universalDateString) ->
        return ""  if universalDateString is `undefined`
        date = (if $filter("isDataType")(new Date(universalDateString), "date") then new Date(universalDateString) else new Date(Date.parse(universalDateString)))
        $filter("date") date, "MM/dd/yy"
  ]).

  filter("universalToUsTime", [
    "$filter"
    ($filter) ->
      return (universalDateString) ->
        return ""  if universalDateString is `undefined`
        date = (if $filter("isDataType")(new Date(universalDateString), "date") then new Date(universalDateString) else new Date(Date.parse(universalDateString)))
        $filter("date") date, "hh:mm a"
  ]).

  filter("universalToEpochTime", [
    "$filter"
    ($filter) ->
      return (universalDateString) ->
        return ""  if universalDateString is `undefined`
        date = (if $filter("isDataType")(new Date(universalDateString), "date") then new Date(universalDateString) else new Date(Date.parse(universalDateString)))
        date.getTime()
  ])

  return
