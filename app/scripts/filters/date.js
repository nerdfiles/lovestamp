
/**
@fileOverview

     _
    | |        _
  __| |_____ _| |_ _____
 / _  (____ (_   _) ___ |
( (_| / ___ | | |_| ____|
 \____\_____|  \__)_____)

@description

List of Date Filters:

    1. universalToUsDate
    2. universalToUsTime
    3. universalToEpochTime
 */

(function() {
  define(["interface", "moment"], function(__interface__) {
    __interface__.filter("universalToUsDate", [
      "$filter", function($filter) {
        return function(universalDateString) {
          var date;
          if (universalDateString === undefined) {
            return "";
          }
          date = ($filter("isDataType")(new Date(universalDateString), "date") ? new Date(universalDateString) : new Date(Date.parse(universalDateString)));
          return $filter("date")(date, "MM/dd/yy");
        };
      }
    ]).filter("universalToUsTime", [
      "$filter", function($filter) {
        return function(universalDateString) {
          var date;
          if (universalDateString === undefined) {
            return "";
          }
          date = ($filter("isDataType")(new Date(universalDateString), "date") ? new Date(universalDateString) : new Date(Date.parse(universalDateString)));
          return $filter("date")(date, "hh:mm a");
        };
      }
    ]).filter("universalToEpochTime", [
      "$filter", function($filter) {
        return function(universalDateString) {
          var date;
          if (universalDateString === undefined) {
            return "";
          }
          date = ($filter("isDataType")(new Date(universalDateString), "date") ? new Date(universalDateString) : new Date(Date.parse(universalDateString)));
          return date.getTime();
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=date.js.map
