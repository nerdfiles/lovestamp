
/**
@fileOverview


List of Search Filters:

    1. stampository
 */

(function() {
  define(['interface'], function(__interface__) {

    /**
    FILTER
    
    Navigators appear on the search results page, and allow filtering of'
    search results. This filter manages what is displayed as a label for
    each navigator option.
     */
    __interface__.filter('search', function() {
      return function(input, search) {
        var out, temp;
        out = [];
        temp = [];
        if (search && search.queryAlias) {
          angular.forEach(input, function(e) {
            if (e.localStamp.alias.indexOf(search.queryAlias) >= 0) {
              temp.push(e);
            }
          });
        } else {
          temp = input;
        }
        out = temp;
        if (search && search.queryOwner) {
          out = [];
          angular.forEach(temp, function(e) {
            if (e.user.$id.indexOf(search.queryOwner) >= 0) {
              out.push(e);
            }
          });
        }
        return out;
      };
    });

    /** Search Filter */
    __interface__.filter('searchMerchant', function() {
      return function(entities, searchText) {
        var i, result;
        if (searchText === void 0) {
          return entities;
        }
        result = [];
        i = 0;
        while (i < entities.length) {
          if (entities[i].merchantName === searchText) {
            result.push(entities[i]);
          }
        }
        ++i;
        return result;
      };
    });
  });

}).call(this);

//# sourceMappingURL=search.js.map
