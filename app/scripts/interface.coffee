###
# fileOverview

     _                           ___
    (_)       _                 / __)
    _ ____ _| |_ _____  ____ _| |__ _____  ____ _____
    | |  _ (_   _) ___ |/ ___|_   __|____ |/ ___) ___ |
    | | | | || |_| ____| |     | |  / ___ ( (___| ____|
    |_|_| |_| \__)_____)_|     |_|  \_____|\____)_____)

@module interface

## description

  Application's central interface for modular inputs.

## note

jQuery can be loaded as needed within Module Controllers defined in our routes.
The basic idea is that Continuous Integration involves the dynamic configuration 
of payloads per unique URL, such that modules can specify their unique dependencies 
without weighing down the active performance of other pages within a continuous 
experience of the application. jQuery is ~90KB. We may not want to introduce this 
sizable payload "everywhere" given the applications URL namespace. CI enables micro-
regression testing and contractual cache invalidation, such that bookmarks, for example,
become stateless, idempotent representations of the application. Cool URLs are stateless, 
in that they express a configuration of the application without session or followed line 
of a use-case scenario, or E2E test. The anchors and UI functions of the application at 
that URL possess Hypermedia Controls which determine future view states. 

RequireJS makes it possible to decouple materials like jQuery from stateless representations, 
on top of AngularJS's dependency injection (DI) which makes statelessness fashionable as an 
interface grammar (decoupling "app modules" from routes such that memory allocation is rigid, 
following an organic shim in the code implementation). Angular ui-router[0] or Anguler detour[1] 
are good extensions to the dependency injection concept.

[0]: https://github.com/marcoslin/angularAMD/issues/113#issuecomment-63229298
[1]: https://github.com/afterglowtech/angular-detour/blob/master/samples/3-json-config/js/app.js

###

define [
  'angularAMD'
  'angular'
  #'jquery'
  'config'
  'routes'
  'lodash'
  'moment'
  'ionic-angular'
  'angular-cordova'
  #'leaflet-osmbuildings'
  'leaflet-markercluster'
  'leaflet-oms'
  'angular-messages-bus'
  'angular-ui-router'
  'angular-once'
  'angular-chosen-localytics'
  # @note Sometimes we may toggle vendor source code aliases.
  #'loggly.service'
  'loggly.tracker'
  'loggly.src'
  'angular-route'
  'ngIOS9UIWebViewPatch'
  'angular-animate'
  # @note I prefer HTML, actually, with explicit closing syntax.
  'angular-messages'
  'material-date-picker'
  'angular-local-storage'
  'angular-geolocation'
  'angular-leaflet-directive'
  #'leaflet-tooltip'
  #'leaflet-awesome-markers'
  #'leaflet-draw'
  'leaflet-knn'
  'leaflet-label'
  #'angular-resource'
  'angular-sanitize'
  'angular-draggable'
  'angular-aria'
  'angular-touch'
  'angular-material' # @assume utils/hammer
  'angular-fire'
  'angular-aria'
  'angular-payments'
  'angular-elastic-search'
  'angular-base64'
  'angular-pusher'
  'angular-translate'
  'angular-audio'
  'angular-infinite-scroll'
  #'decorators/prev'
  #'decorators/base'
  #'config/exceptionHandler'
], (angularAMD, angular, environment, routes, _, moment) ->

  Math.log1p = Math.log1p or (x) -> Math.log(1 + x)

  # Recast as int.
  Number.prototype.int = String.prototype.int = () -> parseInt(@, 10)

  Array::randomPrint = () ->
    arr = @
    len = arr.length
    a = Math.floor(Math.random() * len)
    arr[a]

  String::userTypeCheck = (type) ->
    ###
    @namespace userTypeCheck  
    @description  

    A function for normalizing users into an index based on the baptized social 
    network of their choosing.

    @TODO Move this into our client-side services layer.  
    @prototype String  
    @return {string}  
    ###

    str = this
    # Use Latin Small Letter F (f) to distinguish Facebook users.
    symbol = if type is 'twitter' then '@' else if type is 'facebook' then 'f' else '+'
    return (symbol + str)


  Array::keySort = (keys) ->
    ###
    @module utils  
    @namespace Array::keySort  
    ###

    keys = keys or {}
    # via
    # http://stackoverflow.com/questions/5223/length-of-javascript-object-ie-associative-array
    obLen = (obj) ->
      size = 0
      key = undefined
      for key of obj
        size++  if obj.hasOwnProperty(key)
      size
    obIx = (obj, ix) ->
      size = 0
      key = undefined
      for key of obj
        if obj.hasOwnProperty(key)
          return key  if size is ix
          size++
      false
    keySort = (a, b, d) ->
      d = (if d isnt null then d else 1)
      return 0  if a is b
      (if a > b then 1 * d else -1 * d)
    KL = obLen(keys)
    return @sort(keySort)  unless KL
    for k of keys
      # asc unless desc or skip
      keys[k] = (if keys[k] is 'desc' or keys[k] is -1 then -1 else ((if keys[k] is 'skip' or keys[k] is 0 then 0 else 1)))
    @sort (a, b) ->
      sorted = 0
      ix = 0
      while sorted is 0 and ix < KL
        k = obIx(keys, ix)
        if k
          dir = keys[k]
          sorted = keySort(a[k], b[k], dir)
          ix++
      sorted
    this


  protocol = if window.location.protocol then (window.location.protocol + '//') else 'https://'
  baseUrl = if window.location.host then (window.location.host) else 'lovestamp.io'

  appInit = ($rootScope, $stateParams, $location, $window, orbitalLoader) ->
    ###
    @ngdoc run  
    @name interface.run:appInit  
    @function appInit  
    @description

    Initialization setup for the application.

    ###

    $rootScope.userLanded = false
    $rootScope.isActive = false

    filterList = [
      'merchant/stamp/id'
      'merchant/stamps'
      'merchant/legal'
      'merchant/dashboard'
      'merchant/admin'
      'merchant/store'
      'merchant/social'
      'merchant/requests'
      'merchant/admin/payments'
    ]


    history = []
    bodyClass = []
    bodyClass.push 'js-enabled'
    bodyClass.push 'js'
    bodyClass.push 'lovestamp_io'
    bodyClass.push 'base-template'
    pageNameConstruct = $location.path().replace(/\//g, '--')

    if pageNameConstruct == '--'
      bodyClass.push 'index'
    else
      bodyClass.push pageNameConstruct.substring(1).substring(1)

    l = $location.path().replace($location.path().charAt(0), '')

    a = _.filter(filterList, (_l) ->
      _l == l
    )

    if _.size(a)
      bodyClass.push 'admin-like'
      bodyClass.push l

    $rootScope.bodyClass = bodyClass.join(' ')

    $rootScope.$on '$locationChangeStart', () ->
      bodyClass = []
      bodyClass.push 'lovestamp_io'
      bodyClass.push 'js-enabled'
      bodyClass.push 'js'
      bodyClass.push 'base-template'
      pageNameConstruct = $location.path().replace(/\//g, '--')
      if pageNameConstruct == '--'
        bodyClass.push 'index'
      else
        bodyClass.push pageNameConstruct.substring(1).substring(1)

      l = $location.path().replace($location.path().charAt(0), '')

      a = _.filter(filterList, (_l) ->
        _l == l
      )

      if _.size(a)
        bodyClass.push 'admin-like'
        bodyClass.push l

      $rootScope.bodyClass = bodyClass.join(' ')
      return

    $rootScope.domainUrl = protocol + baseUrl
    $rootScope.siteUrl = protocol + baseUrl

    ###
    To be used in setting the presentation on index controller's map view.
    ###
    $rootScope.mapHeight = ->
      $window.innerHeight

    ###
    Main loading display.
    ###
    $rootScope.$on 'loading:show', () ->
      $rootScope.loadingSubpage = false
      orbitalLoader.show()
    $rootScope.$on 'loading:hide', () ->
      $rootScope.loadingSubpage = true
      orbitalLoader.hide()

    $rootScope.$on '$locationChangeStart', ->
      history.push $location.$$path
      return

    # @namespace auth/user/roll-up
    $rootScope.$on 'auth/user/roll-up', (event, authData) ->
      $rootScope.authData = authData
      $rootScope.user = authData.authData
      $rootScope.isActive = true  if authData.authData
      return

    # @inner
    $rootScope.back = ->
      prevUrl = (if history.length > 1 then history.splice(-2)[0] else '/')
      $location.path prevUrl
      return

    # Easy access to route params. Empty before routeChangeSuccess.
    $rootScope.params = $stateParams

    ###*
    Wrapper for angular.isArray, isObject, etc checks for use in the view

    @inner
    @param {string} type The name of the check (casing sensitive).
    @param {string} value Value to check.
    ###
    $rootScope.is = (type, value) ->
      angular['is' + type] value

    ###*
    Wrapper for $.isEmptyObject()

    @inner
    @param value {mixed} Value to be tested
    @return boolean
    ###
    $rootScope.empty = (value) ->
      angular.isEmptyObject value

    $rootScope._ = _

    if $window.is_device and cordova.InAppBrowser
      $rootScope.InAppBrowserOpen = cordova.InAppBrowser.open
    else
      $rootScope.InAppBrowserOpen = $window.open

    $rootScope.is_device = $window.is_device

    if $window.is_device
      $rootScope.image_path_prefix = './'
    else
      $rootScope.image_path_prefix = '/assets/'

    $rootScope.moment = (dateObject, formatSpecification) ->
      ###
      ###
      return moment(dateObject).format formatSpecification


    $rootScope.iOS = if navigator.userAgent.match(/(iPad|iPhone|iPod)/g) then true else false


    ###*
    Debugging Tools

    Allows you to execute debug functions from the view.
    ###
    if !window.DEBUG

      $rootScope.log = (variable) ->
        ###*
        @inner
        @return undefined
        ###
        console.log variable
        return


      $rootScope.alert = (text) ->
        ###*
        @inner
        @return undefined
        ###
        alert text
        return

    return

  base_applicationDependencies = [
    'firebase'
    'ionic'
    'ngCordova'
    'leaflet-directive'
    'ui.router'
    'ngAria'
    'ngRoute'
    'ngMaterial'
    'ngAnimate'
    'geolocation'
    'LocalStorageModule'
    #'ngResource'
    'ngSanitize'
    'ngTouch'
    'ngMessages'
    'angularPayments'
    'elasticsearch'
    'loggly'
    'hmTouchEvents'
    'ngDraggable'
    'base64'
    'messageBusModule'
    'materialDatePicker'
    'once'
    'localytics.directives'
    'doowb.angular-pusher'
    'pascalprecht.translate'
    'ngAudio'
    'infinite-scroll'
  ]

  ios_applicationDependencies = [
    'firebase'
    'ionic'
    'ngCordova'
    'leaflet-directive'
    'ui.router'
    'ngAria'
    'ngRoute'
    'ngIOS9UIWebViewPatch'
    'ngMaterial'
    'ngAnimate'
    'geolocation'
    'LocalStorageModule'
    #'ngResource'
    'ngSanitize'
    'ngTouch'
    'ngMessages'
    'angularPayments'
    'elasticsearch'
    'loggly'
    'hmTouchEvents'
    'ngDraggable'
    'base64'
    'messageBusModule'
    'materialDatePicker'
    'once'
    'localytics.directives'
    'doowb.angular-pusher'
    'pascalprecht.translate'
    'ngAudio'
  ]

  # HTTP Configurations

  applicationDependencies = if window.is_ios then ios_applicationDependencies else base_applicationDependencies

  # Declare our LoveStamp application.
  app = angular.module 'lovestamp', applicationDependencies


  # Path Configurations
  app.constant 'FIREBASE_URL', 'https://lovestamp.firebaseio.com'
  app.constant 'loginRedirectPath', '/login'


  leafletExtended = () ->
    ###
    @ngdoc factory  
    @name interface.config:leafletExtended  
    @function leafletExtended  
    @description

      Extensions to Leaflet map UI.

    ###

    serviceInterface = {}
    #serviceInterface.OSMBuildings = window.OSMBuildings
    serviceInterface.OverlappingMarkerSpiderfier = window.OverlappingMarkerSpiderfier
    serviceInterface

  app.factory('leaflet.extended', [
    leafletExtended
  ])


  # Implement routes.
  app.config([
    '$routeProvider'
    '$httpProvider'
    '$locationProvider'
    '$stateProvider'
    '$urlRouterProvider'
    routes
  ])


  # Implement appInit.
  app.run([
    '$rootScope'
    '$stateParams'
    '$location'
    '$window'
    'orbitalLoader'
    appInit
  ])

  geolocationAvailable = ($rootScope, $window, geolocation) ->
    serviceInterface = {}
    serviceInterface.geolocationAvailable = false

    unitedStatesLocationConstruct =
      coords:
        latitude: 37.6,
        longitude: -95.665

    _getLocation = geolocation.getLocation()

    _getLocation.then (d) ->
      serviceInterface.geolocationAvailable = d
      angular.element(document).ready () ->
        $rootScope.$broadcast 'geolocationAvailable', d
        return
      return
    , () ->
      d = unitedStatesLocationConstruct
      serviceInterface.geolocationAvailable = d
      angular.element(document).ready () ->
        $rootScope.$broadcast 'geolocationAvailable', d
        return
      return

    serviceInterface

  app.factory('geolocationAvailable', geolocationAvailable)

  cacheTemplates = ($templateCache, $stateProvider, $http, $timeout) ->
    if window.is_device == false
      t = $timeout ->
        $http.get('/assets/scripts/modules/www/controllers/stamps/partials/page.html', {
          cache: $templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/store/partials/page.html', {
          cache: $templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/social/partials/page.html', {
          cache: $templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/dashboard/partials/page.html', {
          cache: $templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/admin/partials/base.html', {
          cache: $templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/requests/partials/base.html', {
          cache: $templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/payments/partials/base.html', {
          cache:$templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/payments/partials/alt-base.html', {
          cache:$templateCache
        })

        $http.get('/assets/scripts/modules/www/controllers/stamp-signup/partials/page.html', {
          cache:$templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/header/partials/nav.html', {
          cache:$templateCache
        })
        $http.get('/assets/scripts/modules/www/controllers/footer/partials/socket-region.html', {
          cache:$templateCache
        })
        $http.get('/assets/scripts/modules/index/partials/base.html', {
          cache:$templateCache
        })
        $http.get('/assets/scripts/modules/index/partials/bottom-sheet-grid-template.html', {
          cache:$templateCache
        })
        $timeout.cancel t
        return
      , 7000
    return

  #app.run([
    #'$templateCache'
    #'$route'
    #'$http'
    #'$timeout'
    #cacheTemplates
  #])

  #require ['decorators/base']
  #require ['decorators/prev']
  #require ['config/exceptionHandler']


  fanIntercept = ($window, $location, $rootScope, $injector) ->
    ###
    @ngdoc run  
    @name interface.config:fanInterceptConfig  
    @function fanInterceptConfig  
    @description

      Fan intercept URL interface.

    ###

    $rootScope.$on('$locationChangeStart', () ->
        if $location.path().indexOf('admin') isnt -1 and environment.isFanSubdomain
            $injector.get('$state').go 'login-fan-auth'
    )

    if $location.host().indexOf('fan') isnt -1 and environment.isFanSubdomain == true
        if $window.is_ios
          $injector.get('$state').go 'home-ios'
        else
          $injector.get('$state').go 'home'

    if $location.host().indexOf('merchant') isnt -1 and environment.isFanSubdomain == false
        $injector.get('$state').go 'dashboard'

    return

  # Implement fanInterceptConfig.
  app.run [
    '$window'
    '$location'
    '$rootScope'
    '$injector'
    fanIntercept
  ]


  logglyConfig = (loggly) ->
    ###
    @ngdoc run  
    @name interface.config:logglyConfig  
    @function logglyConfig  
    @description

      Loggly client configuration.

    ###
    loggly.setApiKey environment.loggly.logglyApiKey
    return

  # Implement logglyConfig.
  app.run([
    'loggly'
    logglyConfig
  ])


  basicAuthorization = ($httpProvider) ->
    ###
    @ngdoc config  
    @name interface.config:basicAuthorization  
    @function basicAuthorization  
    @description

      Basic route authorization decoration.

      Authorization for certain routes by stamp ownership or blessed profile 
      status.

    ###

    $httpProvider.interceptors.push ['$q', '$injector', '$rootScope', ($q, $injector, $rootScope) ->
      return {

        'request': (config) ->

          $rootScope.$on '$stateChangeError', (event, toState, toParams, fromState, fromParams, error) ->
            # @TODO Test
            if error.status == 401
              #authenticationService.deniedState = toState.name
              $injector.get('$state').go 'login-fan-auth'
                #{
                  #location : 'replace'
                  ##relative : $injector.get('$state').$current
                  ##inherit  : false
                  #notify   : true
                #}

          $rootScope.$on 'auth/user/roll-up', (event, authData) ->
            if authData and authData.authData
              $rootScope.$broadcast 'map:ready'
              $rootScope.$broadcast 'auth:ready', authData
              $rootScope.userLanded = true
            else

              if window.is_fan
                $injector.get('$state').go 'login-fan-auth'
              else if window.is_merchant
                $injector.get('$state').go 'login-merchant-auth'
              else
                $injector.get('$state').go 'login-fan-auth'

            return

          $rootScope.$on 'user:hasStampOrIsActive', (event, userHasStampOrIsActive) ->
            if userHasStampOrIsActive isnt true
              $rootScope.loadingSubpage = true
              $injector.get('$state').go 'store-request'

          #$rootScope.$broadcast('loading:show')
          config

        'response': (response) ->
          #$rootScope.$broadcast('loading:hide')
          $q.when response

        'responseError': (reason) ->

          if reason.status == 401
            if window.is_fan
              $injector.get('$state').go 'login-fan-auth'
            else if window.is_merchant
              $injector.get('$state').go 'login-merchant-auth'
            else
              $injector.get('$state').go 'login-fan-auth'

          $q.reject(reason)
        }
    ]

  # Implement basicAuthorization.
  app.config([
    '$httpProvider'
    basicAuthorization
  ])


  # Implement authHttpResponseInterceptor.
  #app.factory('AuthHttpResponseInterceptor', [
    #'$q',
    #'$location',
    #'$rootScope'
    #'$timeout'
    #authHttpResponseInterceptor
  #])


  localStorageConfig = (localStorageServiceProvider) ->
    ###
    @ngdoc config  
    @name interface.config:localStorageConfig
    @function localStorageConfig
    @description

      localStorage configuration.

    ###

    localStorageServiceProvider
      .setPrefix('LoveStamp')
      .setStorageType('localStorage')
      .setNotify(true, true)

    return

  # Implement localStorageConfig.
  app.config([
    'localStorageServiceProvider'
    localStorageConfig
  ])


  orbitalLoaderConfig = ($log, $rootScope) ->
    ###
    @ngdoc factory  
    @name interface.factory:orbitalLoaderConfig  
    @function orbitalLoaderConfig  
    @description

      Orbital loader configuration.

    ###

    serviceInterface = {}
    serviceInterface.show = () ->
      $rootScope.loader = true
      return
    serviceInterface.hide = () ->
      $rootScope.loader = false
      return
    serviceInterface


  # Implement orbitalLoaderConfig.
  app.factory('orbitalLoader', [
    '$log'
    '$rootScope'
    orbitalLoaderConfig
  ])

  app.config ($provide) ->
    $provide.decorator '$q', ($delegate) ->

      allSettled = (promises) ->
        deferred = $delegate.defer()
        counter = 0
        results = if angular.isArray(promises) then [] else {}
        angular.forEach promises, (promise, key) ->
          counter++
          $delegate.when(promise).then (value) ->
            if results.hasOwnProperty(key)
              return
            results[key] =
              status: 'fulfilled'
              value: value
            if !--counter
              deferred.resolve results
            return
          , (reason) ->
            if results.hasOwnProperty(key)
              return
            results[key] =
              status: 'rejected'
              reason: reason
            if !--counter
              deferred.resolve results
            return
          return
        if counter == 0
          deferred.resolve results
        deferred.promise

      $delegate.allSettled = allSettled
      $delegate
    return

  app.config ($provide) ->
    $provide.decorator '$q', ($delegate) ->

      isPromiseLike = (obj) ->
        obj and angular.isFunction(obj.then)

      serial = (tasks) ->
        # Fake a "previous task" for our initial iteration
        prevPromise = undefined
        error = new Error()
        angular.forEach tasks, (task, key) ->
          success = task.success or task
          fail = task.fail
          notify = task.notify
          nextPromise = undefined

          if !prevPromise
            nextPromise = success()
            if !isPromiseLike(nextPromise)
              error.message = 'Task ' + key + ' did not return a promise.'
              throw error
          else
            # Wait until the previous promise has resolved or rejected to execute the next task
            nextPromise = prevPromise.then((data) ->
              if !success
                return data
              ret = success(data)
              if !isPromiseLike(ret)
                error.message = 'Task ' + key + ' did not return a promise.'
                throw error
              ret
            , (reason) ->
              if !fail
                return $delegate.reject(reason)
              ret = fail(reason)
              if !isPromiseLike(ret)
                error.message = 'Fail for task ' + key + ' did not return a promise.'
                throw error
              ret
            , notify)
          prevPromise = nextPromise
          return
        prevPromise or $delegate.when()

      $delegate.serial = serial
      $delegate
    return

  productTourStage = ($rootScope, leafletData, $timeout, $location) ->
      ###
      @ngdoc directive  
      @name interface.directive:productTourStage  
      @element body  
      @function productTourStage  
      @description  

      A product tour of the LoveStamp application that uses image overlays 
      organized and managed by AngularJS directive.

      @example  

          <body
            class="{{ bodyClass }}"
            product-tour-stage
            itemscope
            itemtype="https://schema.org/WebPage"
          >
            <!-- a full on page -->
          </body>

      ###

      return {
          restrict: 'A'
          link: ($scope, element) ->

              element.on 'click', (e) ->
                  return  if e.target.tagName is 'svg' or e.target.tagName is 'img' or e.target.tagName is 'path'
                  if angular.element(e.target).hasClass 'md-bottom-sheet-backdrop'
                      element.removeClass('pt--undefined')
                      element.removeClass('pt--hangout')
                      element.removeClass('pt--mail')
                      element.removeClass('pt--message')
                      element.removeClass('pt--copy')
                      element.removeClass('pt--facebook')
                      element.removeClass('pt--twitter')
                      element.removeClass('pt--google')
                      $rootScope.productTourLoaded = true

                  if $rootScope.productTourLoaded
                    leafletData.getMap('index').then (map) ->
                      map.invalidateSize(false)

              ch = (newValue, timeout, _switch) ->
                cl = angular.element(element).attr 'class'
                _cl = cl.split ' '
                element.addClass 'exiting'
                $timeout ->
                    _.forEach _cl, (na) ->
                        if na.indexOf('pt--') isnt -1
                            element.removeClass na
                    element.removeClass 'exiting'
                    element.addClass('pt--' + newValue)
                    if _switch or newValue
                        element.removeClass 'show--product-tour--'
                        element.addClass 'show--product-tour'
                , timeout

              absent = -1

              $scope.$on 'hide:pt', () ->
                  element.removeClass 'show--product-tour--'
                  element.removeClass 'show--product-tour'

              $scope.$watch('icon', (newValue) ->

                  if newValue
                      timeout = 250
                      _switch = true
                      if $location.path().indexOf('product-tour') isnt absent
                          ch(newValue, timeout, 'undefined')

                  #if $location.path().indexOf('product-tour') isnt absent
                      #ch(true, 0, 'undefined')

              )

              $rootScope.$on('$locationChangeSuccess', () ->
                  timeout = 0
                  if $location.path().indexOf('product-tour') isnt absent
                      ch('undefined', timeout)
              )

              return
      }

  # Implement productTourStage.
  app.directive('productTourStage', [
    '$rootScope',
    'leafletData',
    '$timeout',
    '$location',
    productTourStage
  ])


  ## Implement JS enabled.
  #app.directive('jsEnabled', [
    #() ->
      #restrict: 'A'
      #link: ($scope, element) ->
        #htmlClass = element.attr 'class'
        #element.attr('class', htmlClass.replace('no-js', 'js'))
        #return
  #])


  clickMonad = ($timeout) ->
    ###
    @ngdoc directive  
    @name interface.directive:clickMonad  
    @element a  
    @function clickMonad  
    @description  

    Prevent double trigger on click events for ng-click directives, and possibly 
    other simple interactive captures.

    @example  

        <a
          module="interface"
          click-monad
          ng-click="someFunction()"
        >
          <span class="fa fa-flash"></span>
        </a>

    ###

    delay = 500   # min milliseconds between clicks

    return {
        restrict: 'A',
        priority: -1,   # cause out postLink function to execute before native `ngClick`'s
                        # ensuring that we can stop the propagation of the 'click' event
                        # before it reaches `ngClick`'s listener
        link: ($scope, element) ->
            disabled = false

            clickHandler = (event) ->
              if disabled
                event.preventDefault()
                event.stopImmediatePropagation()
              else
                disabled = true
                $timeout () ->
                  disabled = false
                  return
                , delay, false

            $scope.$on '$destroy', () ->
              element.off 'click', clickHandler

            element.on 'click', clickHandler

            return
    }

  # Implement clickMonad.
  app.directive('clickMonad', [
    '$timeout'
    clickMonad
  ])


  #messagesBusServiceConfig = ($httpBackend, $rootScope, $location, messagesBusService) ->
      ####
      #@ngdoc run  
      #@name interface.run:messagesBusServiceConfig  
      #@function messagesBusServiceConfig  
      #@description

      #AngularJS handles events through HTML directives, as mad as that sounds. 
      #They depend on controller definitions of $scope, and they have their own 
      #unruly order, nestings, etc. But here we wish to tokenize our templates 
      #before they are issued to the DOM, with channel-based tokenizers to 
      #decorate presentation elements of blocks of timeless clients.

      #I'm just pulling this strategy from hn-heartbeat, where I load CSRF tokens 
      #into templates like this (some stray HAML template lying around):

           #%meta{
             #:name    => 'x-csrf-token',
             #:content => '{{ csrf_token }}'
           #}

      #Like AngularAMD decorates the @resolve of Routers' Promise:when, we will 
      #run a COMM service to implement channel-based templates where route 
      #contracts can exist corresponding to the lifetime of hashlocks on a 
      #Blockchain. The use case I have in mind is a validation protocol for 
      #chain stamping (CS) and trustless stamp screens (TSS), which will 
      #interplay in our codebase as decorators to plugins/snowshoestamp/directives/stamp.coffee.

      #I simply do not believe it to be wise to allow stamp screens to 
      #implicitly authenticate long-running wait timeframes. So, essentially 
      #an implementation opportunity lives in our authorization service.

      #\#\#\#\# messagesBusService available API

          #apiVersion
          #getEvent
          #getSubscriber
          #publish
          #register
          #registry
          #unregister

      ####

      #messagesBusService.register 'csrfTemplateTokenizer', {
              #name: 'csrfTemplateTokenizer-sub'
              #handler: (authorizationData) ->
                  #$rootScope.$on '$locationChangeStart', () ->
                      #return  if $location.path().indexOf('stamp') is -1
                      #$rootScope.authorizationChannel = authorizationData.decorateTemplate
                      #return
                  #return
          #}
      #return

  ## Implement messagesBusServiceConfig.
  #app.run([
    #'$httpBackend'
    #'$rootScope'
    #'$location'
    #'messagesBusService'
    #messagesBusServiceConfig
  #])


  logProviderConfig = ($logProvider) ->
      ###
      @ngdoc config  
      @name interface.config:logProviderConfig  
      @function logjProviderConfig  
      @description

      Log provider exposure.

      ###

      $logProvider.debugEnabled true

  # Implement logProviderConfig.
  app.config([
      '$logProvider'
      logProviderConfig
  ])


  ariaProviderConfig = ($ariaProvider) ->
      ###
      @ngdoc config  
      @name interface.config:ariaProviderConfig  
      @function ariaProviderConfig  
      @description

      Accessibility implementation hooks.

      ###

      $ariaProvider.config(
        ariaInvalid : false
        tabindex    : true
      )

  # Implement ariaProviderConfig.
  app.config([
    '$ariaProvider'
    ariaProviderConfig
  ])


  #pusherConfig = (PusherServiceProvider) ->
    ####
    #@ngdoc config  
    #@name interface.config:pusherConfig  
    #@function pusherConfig  
    #@description

    #Pusher Config.

    ####

    #PusherServiceProvider
      #.setToken(environment.pusherServiceAppKey)
      #.setOptions({})

  ## Implement Pusher.
  #app.config([
    #'PusherServiceProvider'
    #pusherConfig
  #])


  urlIntercept = ($urlRouterProvider) ->
    ###
    @ngdoc config  
    @name interface.config:urlIntercept  
    @function urlIntercept  
    @description

    URL Intercept.

    ###

    $urlRouterProvider.deferIntercept()

  # Implement URL Intercept.
  app.config([
    '$urlRouterProvider'
    urlIntercept
  ])


  routeManager = ($rootScope, $stateParams, $urlRouter, $timeout) ->
    ###
    @ngdoc run  
    @name interface.run:routeManager  
    @function routeManager  
    @description

    Route manager.

    ###

    $rootScope.oldStampAlias = null
    $rootScope.$on('$locationChangeSuccess', (e, newUrl) ->
      profileRegions = newUrl and newUrl.indexOf('@') isnt -1 and newUrl.indexOf('profile') is -1
      stampsRegions = newUrl and newUrl.indexOf('@') isnt -1
      $timeout ->
        $rootScope.$apply () ->
          $rootScope.oldStampAlias = $stateParams.stampAlias
          return
        return
      , 0

      if profileRegions and stampsRegions
        if $rootScope.oldStampAlias isnt null
          e.preventDefault()
        else
          $urlRouter.sync()
    )
    $urlRouter.listen()

  # Implement Route Manager.
  app.run([
    '$rootScope'
    '$stateParams'
    '$urlRouter'
    '$timeout'
    routeManager
  ])


  #configCurrencyTranslations = ($translateProvider) ->

    ####
    #Using ``angular-translate`` as a basic implementation point for currency 
    #translation displays within the bits-display-board directive.
    ####

    #$window = window
    #_lang = $window.location.href.split('/')
    #lang = _.last(_lang)

    #$translateProvider.translations('en', {
      #bits: 'USD',
    #})

    #$translateProvider.translations('de', {
      #bits: 'Euro',
    #})

    #$translateProvider.translations('fr', {
      #bits: 'Euro',
    #})

    #$translateProvider.translations('es-sp', {
      #bits: 'Euro',
    #})

    #$translateProvider.translations('es-mx', {
      #bits: 'Peso',
    #})

    #$translateProvider.translations('mx', {
      #bits: 'Peso',
    #})

    #if lang is 'en' or lang is 'de' or lang is 'fr' or lang is 'es-mx' or lang is 'mx'
      #$translateProvider.preferredLanguage(lang)

  #app.config([
    #'$translateProvider'
    #configCurrencyTranslations
  #])

  #mdConfig = ($mdThemingProvider) ->

    #$mdThemingProvider
      #.definePalette('LoveStamp', {
        #'50'   : 'ffebee'
        #'100'  : 'ffcdd2'
        #'200'  : 'ef9a9a'
        #'300'  : 'e57373'
        #'400'  : 'ef5350'
        #'500'  : 'f44336'
        #'600'  : 'e53935'
        #'700'  : 'd32f2f'
        #'800'  : 'c62828'
        #'900'  : 'b71c1c'
        #'A100' : 'ff8a80'
        #'A200' : 'ff5252'
        #'A400' : 'ff1744'
        #'A700' : '91D936'
        #'contrastLightColors'  : undefined
        #'contrastDefaultColor' : 'light'
        #'contrastDarkColors'   : [
          #'50'
          #'100'
          #'200'
          #'300'
          #'400'
          #'A100'
        #]
      #})

    #$mdThemingProvider
      #.theme('default')
      #.primaryPalette('LoveStamp')

  #app.config [
    #'$mdThemingProvider'
    #mdConfig
  #]

  app.config([
    '$compileProvider'
    ($compileProvider) ->
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/)
  ])

  cordovaCheck = ($timeout, $q, $rootScope, $window, $ionicPlatform) ->
    ###
    @usage

      define ['interface'], (__interface__) ->
        controller = (cordovaCheck) ->
          cordovaCheck.ionicReady()
          cordovaCheck.documentReady().then () ->
            console.log 'Device is ready!'
            cordovaCheck.addCapability(...).then(...)
        __interface__.controller(['cordovaCheck', controller])
        # Or as module controller:
        ['cordovaCheck', controller]

    ###

    console.dir $ionicPlatform
    # Create check interface.
    checkInterface = {}
    checkInterface.capabilities = []


    checkInterface.addCapability = (config) ->
      ###
      Add Capability.

      @param {object} config
      @usage

        cordovaCheck.addCapability({
          featureName: 'StatusBar'
          featureCallback: () ->
            StatusBar.hide()
          timeout: 0
        }).then((status) ->
          console.log status
        , (status) ->
          console.log status
        )

      ###

      def = $q.defer()
      config.featureAdded = true
      config.featureFired = false
      _capabilities = _(@capabilities).push(config) # wrap capability
      _capabilities.commit()
      _capability = _capabilities.last()

      if $window[_capability.featureName] and _capability.featureFired == false

        callbackStatus = _capability.featureCallback()

        $timeout ->

          if callbackStatus != undefined
            _capability.featureFired = true
            def.resolve(callbackStatus)
          else
            _capability.featureFired = false
            console.log('A device capability should explicitly tell us its status.')
            def.reject('No status provided but fired (assumption).')
        , (_capability.timeout or 0)

      def.promise


    checkInterface.ionicReady = () ->
      ###
      Ionic Device Ready Check
      ###

      def = $q.defer()

      onDeviceReady = () ->
        $timeout ->
          $rootScope.$apply(def.resolve)
        , 0

      try
        $ionicPlatform.ready(onDeviceReady)
      catch e
        ionic.Platform.ready(onDeviceReady)

      def.promise


    checkInterface.ngDocumentReady = (useCapture, wantsUntrusted) ->
      ###
      Angularized Browser Document Device Ready Check

      @param {boolean} useCapture
        Event dispatch from `listener` before `EventTarget`.
      @param {boolean} wantsUntrusted
        Synthetic events for Gecko under "add-on" scenarios.
      ###

      def = $q.defer()
      _useCapture = useCapture or false
      _wantsUntrusted = wantsUntrusted or undefined

      onDeviceReady = () ->
        $timeout ->
          $rootScope.$apply(def.resolve)
        , 0

      ngDocument = angular.element(document)

      angular.element(ngDocument).ready(() ->
        ngDocument.on('deviceready', onDeviceReady)
        return
      )

      def.promise


    checkInterface.documentReady = (useCapture, wantsUntrusted) ->
      ###
      Browser Document Device Ready Check

      @param {boolean} useCapture
        Event dispatch from `listener` before `EventTarget`.
      @param {boolean} wantsUntrusted
        Synthetic events for Gecko under "add-on" scenarios.
      ###

      def = $q.defer()
      _useCapture = useCapture or false
      _wantsUntrusted = wantsUntrusted or undefined

      onDeviceReady = () ->
        $timeout ->
          $rootScope.$apply(def.resolve)
        , 0

      if _wantsUntrusted != undefined
        document.addEventListener('deviceready', onDeviceReady, _useCapture, _wantsUntrusted)
      else
        document.addEventListener('deviceready', onDeviceReady, _useCapture)

      def.promise


    # Export check interface.
    checkInterface


  app.factory('cordovaCheck', [
    '$timeout'
    '$q'
    '$rootScope'
    '$window'
    '$ionicPlatform'
    cordovaCheck
  ])

  app.run [
    '$ionicPlatform'
    '$rootScope'
    ($ionicPlatform, $rootScope) ->
      if $ionicPlatform and window.DEBUG
        $rootScope.$ionicPlatform = $ionicPlatform

      ###
          console.dir $ionicPlatform
          > Object
          $backButtonActions: Object
          _hasBackButtonHandler: true
          hardwareBackButtonClick: (e)
          is: (e)
          offHardwareBackButton: (e)
          on: (e,t)
          onHardwareBackButton: (e)
          ready: (t)
          registerBackButtonAction: (e,n,i)
          __proto__: Object
      ###
      ionic.Platform.fullScreen()
  ]

  document.addEventListener 'deviceready', () ->
    if window.is_android
      cordova.exec(null, null, 'SplashScreen', 'hide', [])
    return

  $body = angular.element(document)
  angularAMD.bootstrap(app, true, $body)
