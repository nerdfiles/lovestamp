###
# fileOverview

     _
    | |                 _
    | |__   ___   ___ _| |_
    |  _ \ / _ \ / _ (_   _)
    | |_) ) |_| | |_| || |_
    |____/ \___/ \___/  \__)

## description

@note Consider OAMD specifications instead of whatever native device 
APIs expect. Modernizr is useful 'modernizr/feature-detects/css-calc' 
modalities.

###

require.config

  baseUrl: '../www/scripts/'

  waitSeconds: 30

  #urlArgs: 'v=1&bust=' + (new Date()).getTime()
  #urlArgs: 'v=1'

  ###*
  Aliases
  ###
  paths:

    'ngIOS9UIWebViewPatch'      : 'patches/angular-ios9-uiwebview-patch'
    'androidLeafletTransition'  : 'patches/leaflet-android-transition'

    'async'                     : 'ext/async'
    'font'                      : 'ext/font'
    'goog'                      : 'ext/goog'
    'image'                     : 'ext/image'
    'json'                      : 'ext/json'
    'noext'                     : 'ext/noext'
    'mdown'                     : 'ext/mdown'
    'propertyParser'            : 'ext/propertyParser'
    'markdownConverter'         : 'ext/Markdown.Converter'

    'helpers/async'             : 'ext/utils/async'
    'utils/async'               : './utils/async'

    'leaflet-oms'               : 'ext/oms'
    'leaflet-osmbuildings'      : 'ext/OSMBuildings-Leaflet'
    'leaflet-markercluster'     : 'ext/leaflet.markercluster'

    'geojson-tools-src'         : 'ext/geojson-tools.amd'
    'geojson-tools'             : 'services/geoJson'

    'chance'                    : './ext/chance'
    'parse-address'             : './ext/addressit.min'

    ###*
    Core Application Materials
    ###
    angular                     : 'ext/angular'
    'loggly.tracker'            : 'ext/loggly.tracker'
    'loggly.src'                : 'ext/logglyService'
    'loggly.service'            : 'ext/logglyService.min'
    angularAMD                  : 'ext/angularAMD'
    ngload                      : 'ext/ngload'

    'elastic-search'            : 'ext/elasticsearch.min'

    ###*
    SnowShoe Implementation
    ###
    'sss-client'                : 'ext/sss.client'
    'sss-util'                  : 'ext/sss.util'
    touchy                      : 'ext/touchy'
    'jquery-json'               : 'ext/jquery.json-2.4.min'

    'graham-scan'               : 'ext/grahamScan-modified'

    'stroll'                    : 'ext/stroll.min'

    moment                      : 'ext/moment.min'
    numeral                     : 'ext/numeral.min'

    ###*
    Visualization Dependency
    ###
    d3                          : 'ext/d3.min'
    c3                          : 'ext/c3.min'

    ###*
    Interactive Maps Dependency
    ###
    #'leaflet'                  : 'ext/leaflet'
    #'leaflet'                  : 'ext/leaflet-src'
    #'leaflet'                     : 'ext/leaflet'
    'leaflet'                   : 'ext/leaflet-src'
    'leaflet-tooltip'           : 'ext/leaflet.tooltip'
    'leaflet-awesome-markers'   : 'ext/leaflet.awesome-markers' # @TODO Decorate?
    'leaflet-draw'              : 'ext/leaflet.draw'
    'leaflet-knn'               : 'ext/leaflet-knn.min'
    'leaflet-label'             : 'ext/leaflet.label'
    'jquery.debounce'           : 'ext/jquery.debounce'

    'angular-pusher'            : 'ext/angular-pusher.min'

    'angular-draggable'         : 'ext/ngDraggable'

    'tangle'                    : 'ext/Tangle'
    'tangle.mootools'           : 'ext/mootools'
    'tangle.sprintf'            : 'ext/sprintf'
    'tangle.BVTouchable'        : 'ext/BVTouchable'
    'tangleKit'                 : 'ext/TangleKit'

    ###*
    AngularJS Dependency
    ###
    'angular-route'             : 'ext/angular-route.min'
    'angular-audio'             : 'ext/angular.audio'
    'angular-ui-router'         : 'ext/angular-ui-router.min'
    'hammer'                    : 'ext/hammer.min'
    'angular-hammer'            : 'ext/angular-hammer'
    'angular-local-storage'     : 'ext/angular-local-storage.min'
    'angular-leaflet-directive' : 'ext/angular-leaflet-directive.min'
    'angular-animate'           : 'ext/angular-animate.min'
    #'angular-cookies'          : 'ext/angular-cookies'
    #'angular-resource'         : 'ext/angular-resource'
    'angular-sanitize'          : 'ext/angular-sanitize.min'
    'angular-touch'             : 'ext/angular-touch.min'
    'angular-material'          : 'ext/angular-material.min'
    'angular-aria'              : 'ext/angular-aria.min'
    #'angular-geolocation'       : 'ext/angularjs-geolocation.min'
    'angular-geolocation'       : 'ext/geolocation'
    'angular-messages'          : 'ext/angular-messages.min'
    'angular-messages-bus'      : 'ext/messagesBus.min'
    'angular-payments'          : 'ext/angular-payments.min'
    'angular-infinite-scroll'   : 'ext/ng-infinite-scroll.min'
    'angular-geotranslation'    : 'ext/ng-geotranslation'
    'angular-elastic-search'    : 'ext/elasticsearch.angular.min'
    'material-date-picker'      : 'ext/mbdatepicker'
    'angular-once'              : 'ext/once'
    'angular-translate'         : 'ext/angular-translate.min'
    'angular-qrcode'            : 'ext/qrcode'

    'bitcoin-prices'            : 'ext/bitcoinprices'

    'angular-chosen-localytics' : 'ext/chosen'
    'chosen'                    : 'ext/chosen.jquery.min'

    firebase                    : 'ext/firebase'
    'angular-fire'              : 'ext/angularfire.min'
    'angular-base64'            : 'ext/angular-base64.min'

    ###*
    Application Front End Environment Configurations
    ###
    config    : 'config/base'

    ###*
    Native Application URIs

    So there's a Web Dashboard for Users to hit. Any user can log in, create a 
    stamp request, etc. We're deploying to a select for OSes, like Android and 
    iOS, but in general the entire application's source code starts with Web 
    Technologies which are wrapped and deployed via PhoneGap. The User should be 
    able to access her dashboard so long as she has a stamp.
    ###
    'routes': './routes-device'

    ###*
    Filters
    ###

    'filters/date'                  : './filters/date'
    'filters/basic'                 : './filters/basic'
    'filters/search'                : './filters/search'

    ###*
    Use-case Directives
    ###

    'components/push/directive'          : './components/push/directive'
    'directives/title'                   : './directives/title'
    'directives/openWindow'              : './directives/openWindow'
    'directives/hoverClass'              : './directives/hoverClass'
    'directives/purse'                   : './directives/purse'
    'directives/preloader'               : './directives/preloader'
    'directives/search/simple'           : './modules/www/directives/simple-search' # Shared across www module controllers.
    'directives/stats/static'            : './modules/www/directives/static-stats' # Shared across www module controllers.
    'directives/stats/treemap-frequency' : './modules/www/directives/treemap-frequency-stats'
    'directives/authCheck'               : './directives/authCheck'
    'directives/site/navigation'         : './modules/www/directives/navigation'
    'directives/bitsDisplayCounter'      : './directives/bitsDisplayCounter'
    'directives/notification'            : './directives/notification'
    'directives/loader'                  : './modules/www/controllers/login/directives/loader'
    'directives/version'                 : './directives/appVersion'
    'directives/customEnter'             : './directives/customEnter'
    'directives/customLoader'            : './directives/customLoader'
    'directives/relExternal'             : './directives/relExternal'
    'directives/stroll'                  : './modules/www/directives/stroll'
    'directives/taskbar'                 : './modules/www/index/directives/taskbar'
    'index/directives/profile-orb'       : './modules/index/directives/profile-image-mask-content'
    'index/directives/merchant-sheet'    : './modules/index/directives/merchant-sheet'
    'index/directives/set-nav-item'      : './modules/index/directives/set-nav-item'
    'directives/stamp'                   : './components/snowshoestamp/directives/stamp'

    'ProductTourCtrl'              : './modules/index/controllers/productTour'
    'index/MerchantSheetCtrl'            : './modules/index/controllers/merchantSheet'

    ###*
    Subcontrollers

    @description Potential ui-router subroutes.
    ###

    'WpHeaderController'              : './modules/www/controllers/header/controllers/base'
    'IndexWpHeaderController'         : './modules/index/controllers/IndexWpHeaderController'
    'IndexController'                 : './modules/index/controllers/base'
    'ProfileController'               : './modules/profile/controllers/base'
    'AuthenticationController'        : './modules/authentication/controllers/base'
    'LoginController'                 : './modules/www/controllers/login/controllers/base'
    'LegalController'                 : './modules/www/controllers/legal/controllers/base'
    'AdminController'                 : './modules/www/controllers/admin/controllers/base'
    'DashboardController'             : './modules/www/controllers/dashboard/controllers/base'
    'PaymentsController'              : './modules/www/controllers/payments/controllers/base'
    'RequestsController'              : './modules/www/controllers/requests/controllers/base'
    'SocialController'                : './modules/www/controllers/social/controllers/base'
    'StampSignupController'           : './modules/www/controllers/stamp-signup/controllers/base'
    'StampsController'                : './modules/www/controllers/stamps/controllers/base'
    'StoreController'                 : './modules/www/controllers/store/controllers/base'
    'ModuleControllerCommon'          : './modules/www/base'
    'ComponentStampBaseController'    : './components/snowshoestamp/controllers/base'
    'ComponentStampErrorController'   : './components/snowshoestamp/controllers/error'
    'ComponentStampSuccessController' : './components/snowshoestamp/controllers/success'
    'ComponentStampHelpController'    : './components/snowshoestamp/controllers/help'
    'NotFoundController'              : './modules/notFound/controllers/base'
    'ContactController'               : './modules/contact/controllers/base'
    'HelpController'                  : './modules/help/controllers/base'

    ###*
    'Auth'

    Covers data marshalling and user authentication via Social Networks.
    ###

    'auth/user/roll-up'     : './modules/authentication/directives/userRollup'
    'auth/access/conductor' : './modules/authorization/directives/accessConductor'
    'auth/user/main'        : './modules/authentication/directives/main'

    ###*
    Configuration Decorators
    ###

    'config/exceptionHandler' : './config/exceptionHandler'
    'config/frameBuster'      : './config/frameBuster'
    'decorators/base'         : './decorators/base'
    'decorators/prev'         : './decorators/prev'

    ###*
    Client-side Services
    ###

    'service/crypto'          : './services/crypto'
    'services/stamp'          : './services/stamp' # @note Corresponds to rules.yaml #/definitions/stamp.
    'services/payments'       : './services/payments'
    'services/tip'            : './services/changeTip'
    'services/profile'        : './services/profile'
    'services/social'         : './services/social'
    'services/map'            : './services/map'
    'services/messaging'      : './services/messaging' # @note Contains inbox and outbox APIs.
    'services/reference'      : './services/reference'
    'services/notification'   : './services/notification'
    'services/authentication' : './services/authentication'
    'services/authorization'  : './services/authorization'
    'services/user/track'     : './services/track'
    'services/bank'           : './services/bank'
    'services/browser'        : './services/browser'
    'services/sound'          : './services/sound'

    'crypto'                  : './ext/Crypto'
    'crypto.BlockModes'       : './ext/BlockModes'
    'crypto.AES'              : './ext/AES'
    'crypto.PBKDF2'           : './ext/PBKDF2'
    'crypto.HMAC'             : './ext/HMAC'
    'crypto.SHA1'             : './ext/SHA1'
    'crypto.MD5'              : './ext/MD5'

    'lodash'                  : './ext/lodash.min'
    'one-color'               : './ext/one-color'
    'add-to-homescreen'       : './ext/addtohomescreen'
    #'add-to-homescreen'       : './ext/addtohomescreen.min'

    'interface'               : './interface'
    'bootstrap'               : './bootstrap/device-android'

    ###*
    @deprecated
    ###

    #'models/stamp'        : './models/stamp'
    'utils/hammer'         : './utils/hammer'
    'utils/tangle'         : './utils/tangle'
    'utils/addToHomescreen': './utils/addToHomescreen'
    'utils/firebase'       : './utils/firebase-generic'
    'utils/firebase/email' : './utils/firebase-email'
    'utils/dataDefender'   : './utils/dataDefender'
    'utils/modalWindow'    : './utils/modalWindow'
    'utils/googleMaps'     : './utils/googleMaps'
    'utils/debounce'       : './utils/debounce'

    'leaflet-touch'        : './utils/leaflet-touch'

    'components/sss/spin'     : './ext/spin'
    'components/sss/stampsdk' : './ext/stampsdk'

    'jquery'          : './ext/jquery.min'
    'jquery.easing'   : './ext/jquery.easing.min'

    'angular-c3'      : './ext/c3-angular.min'
    'angular-rickshaw': './ext/rickshaw'


    'cordova'         : '../cordova'
    'angular-cordova' : './ext/ng-cordova.min'
    'cordova-ios'     : './ext/cordova.ios'
    'cordova-android' : './ext/cordova.android'
    'cordova-browser' : './ext/cordova.browser'
    'ionic'           : './ext/ionic.min'
    'ionic-angular'   : './ext/ionic-angular.min'
    'qrcode'          : './ext/utils/qrcode'
    'qrcode_utf8'     : './ext/utils/qrcode_UTF8'
    'utils/angular-qr': './utils/angular-qr'
    'angular-tablesort': './ext/angular-tablesort'
    'loaders.css'       : './ext/loaders.css'
    'angular-ui-mask'   : './ext/mask.min'

  shim:
    'angular-once'         : ['angular']
    'angular-ui-mask'      : ['angular']
    'loaders.css'          : ['jquery']
    'qrcode_utf8'          : ['qrcode']
    'angular-qrcode'       : ['qrcode_utf8']
    'utils/angular-qr'     : ['angular-qrcode']
    'jquery.debounce'      : ['jquery']

    'angular-tablesort'    : ['angular']
    'angular-rickshaw'     : ['d3', 'angular']
    'angular-c3'           : ['d3', 'angular']
    'utils/debounce'       : ['jquery.debounce']
    'bitcoin-prices'       : ['jquery'] # @note Source bower_component files uses 'jQuery' for AMD alias. Be sure to correct for this.
    'loggly.service'       : ['angular']
    'bootstrap': [
        'angularAMD'
        'ionic'
    ]

    'lodash':
      exports: '_'

    'angular':
        'deps': ['jquery']
        'exports': 'angular'

    'angular-pusher'       : ['angular']
    'angular-messages-bus' : ['angular']
    'angular-route'        : ['angular']
    'material-date-picker' : ['angular']
    'angular-material'     : [
      'angular'
      'angular-aria'
      'angular-animate'
      'utils/hammer'
    ]

    'angular-ui-router'       : ['angular']
    'angular-animate'         : ['angular']
    'angular-cookies'         : ['angular']
    'angular-resource'        : ['angular']
    'angular-sanitize'        : ['angular']
    'angular-draggable'       : ['angular']
    'angular-messages'        : ['angular']
    'angular-touch'           : ['angular']
    'angular-elastic-search'  : [
        'angular'
        'elastic-search'
    ]

    'cordova'         : ['angular-cordova']
    'cordova-ios'     : ['angular-cordova']
    'cordova-android' : ['angular-cordova']
    'cordova-browser' : ['angular-cordova']
    'angular-cordova' : ['angular']
    'ionic':
        exports: 'ionic'
    'ionic-angular':
        deps: [
            'angular'
            'ionic'
        ]
        exports: 'ionic'

    'angularAMD' : ['angular']
    'ngload'     : ['angularAMD'] # namespaces for extensions; consider Modernizr.

    'utils/async': ['helpers/async']

    'hammer':
      exports: 'Hammer'
    'angular-hammer'       : ['hammer']
    'utils/hammer'         : ['angular-hammer']
    'utils/firebase'       : ['angular-fire']
    'utils/firebase/email' : ['angular-fire']

    'angular-fire': [
      'firebase'
      'angular'
    ]

    'angular-geolocation'       : ['angular']
    'angular-chosen-localytics' : ['chosen']
    'angular-payments'          : ['angular']

    'angular-infinite-scroll': [
        'jquery'
        'angular'
    ]

    'angular-base64'        : ['angular']
    'angular-aria'          : ['angular']
    'angular-local-storage' : ['angular']
    'leaflet' :
      deps: ['angular']
      exports: 'L'

    'angular-leaflet-directive' : [
      'angular'
      'androidLeafletTransition'
    ]

    'androidLeafletTransition'  : ['leaflet']

    'leaflet-touch'             : ['angular-leaflet-directive']
    'leaflet-gl'                : ['angular-leaflet-directive']
    'leaflet-gl-min'            : ['angular-leaflet-directive']
    'leaflet-osmbuildings'      : ['angular-leaflet-directive']
    'leaflet-markercluster'     : ['angular-leaflet-directive']
    'leaflet-oms'               : ['angular-leaflet-directive']
    'leaflet-label'             : ['angular-leaflet-directive']
    'leaflet-awesome-markers'   : ['angular-leaflet-directive']
    'leaflet-draw'              : ['angular-leaflet-directive']
    'leaflet-tooltip'           : ['angular-leaflet-directive']

    'leaflet-knn':
      deps: ['angular-leaflet-directive']
      exports: 'leafletKnn'

    'jquery':
      exports: '$'
    'jquery-json':
      deps: ['jquery']
    'jquery.easing':
      'deps': ['jquery']

    'chosen':
      'deps': [
          'jquery'
          'angular'
      ]

    'geojson-tools': ['lodash']

    'touchy':
      deps: [
        'sss-client'
        'sss-util'
      ]

    'tangle':
      exports: 'Tangle'

    'decorators/base'         : ['angular']
    'decorators/prev'         : ['angular']
    'config/exceptionHandler' : ['angular']

    'c3': ['d3']
    'crypto':
      exports: 'CryptoJS'
    'crypto.AES':
      exports: 'CryptoJS'
      deps: [
        'crypto'
        'crypto.BlockModes'
        'crypto.PBKDF2'
        'crypto.HMAC'
        'crypto.SHA1'
        'crypto.MD5'
      ]
    'crypto.MD5':
      exports: 'CryptoJS'
      deps: ['crypto']
    'crypto.BlockModes':
      exports: 'CryptoJS'
      deps: ['crypto']
    'crypto.PBKDF2':
      exports: 'CryptoJS'
      deps: ['crypto']
    'crypto.HMAC':
      exports: 'CryptoJS'
      deps: ['crypto']
    'crypto.SHA1':
      exports: 'CryptoJS'
      deps: ['crypto']

  deps: [
    'bootstrap' # @note Be unnecessary to differentiate here with a different bootstrap agent.
  ]

  #priority: [
      #'ionic'
      #'angular'
  #]


