
/*
 * fileOverview

     _                           ___
    (_)       _                 / __)
    _ ____ _| |_ _____  ____ _| |__ _____  ____ _____
    | |  _ (_   _) ___ |/ ___|_   __|____ |/ ___) ___ |
    | | | | || |_| ____| |     | |  / ___ ( (___| ____|
    |_|_| |_| \__)_____)_|     |_|  \_____|\____)_____)

@module interface

 *# description

  Application's central interface for modular inputs.

 *# note

jQuery can be loaded as needed within Module Controllers defined in our routes.
The basic idea is that Continuous Integration involves the dynamic configuration 
of payloads per unique URL, such that modules can specify their unique dependencies 
without weighing down the active performance of other pages within a continuous 
experience of the application. jQuery is ~90KB. We may not want to introduce this 
sizable payload "everywhere" given the applications URL namespace. CI enables micro-
regression testing and contractual cache invalidation, such that bookmarks, for example,
become stateless, idempotent representations of the application. Cool URLs are stateless, 
in that they express a configuration of the application without session or followed line 
of a use-case scenario, or E2E test. The anchors and UI functions of the application at 
that URL possess Hypermedia Controls which determine future view states. 

RequireJS makes it possible to decouple materials like jQuery from stateless representations, 
on top of AngularJS's dependency injection (DI) which makes statelessness fashionable as an 
interface grammar (decoupling "app modules" from routes such that memory allocation is rigid, 
following an organic shim in the code implementation). Angular ui-router[0] or Anguler detour[1] 
are good extensions to the dependency injection concept.

[0]: https://github.com/marcoslin/angularAMD/issues/113#issuecomment-63229298
[1]: https://github.com/afterglowtech/angular-detour/blob/master/samples/3-json-config/js/app.js
 */

(function() {
  define(['angularAMD', 'angular', 'config', 'routes', 'lodash', 'moment', 'ionic-angular', 'angular-cordova', 'leaflet-markercluster', 'leaflet-oms', 'angular-messages-bus', 'angular-ui-router', 'angular-once', 'angular-chosen-localytics', 'loggly.tracker', 'loggly.src', 'angular-route', 'ngIOS9UIWebViewPatch', 'angular-animate', 'angular-messages', 'material-date-picker', 'angular-local-storage', 'angular-geolocation', 'angular-leaflet-directive', 'leaflet-knn', 'leaflet-label', 'angular-sanitize', 'angular-draggable', 'angular-aria', 'angular-touch', 'angular-material', 'angular-fire', 'angular-aria', 'angular-payments', 'angular-elastic-search', 'angular-base64', 'angular-pusher', 'angular-translate', 'angular-audio', 'angular-infinite-scroll'], function(angularAMD, angular, environment, routes, _, moment) {
    var $body, app, appInit, applicationDependencies, ariaProviderConfig, baseUrl, base_applicationDependencies, basicAuthorization, cacheTemplates, clickMonad, cordovaCheck, fanIntercept, geolocationAvailable, ios_applicationDependencies, leafletExtended, localStorageConfig, logProviderConfig, logglyConfig, orbitalLoaderConfig, productTourStage, protocol, routeManager, urlIntercept;
    Math.log1p = Math.log1p || function(x) {
      return Math.log(1 + x);
    };
    Number.prototype.int = String.prototype.int = function() {
      return parseInt(this, 10);
    };
    Array.prototype.randomPrint = function() {
      var a, arr, len;
      arr = this;
      len = arr.length;
      a = Math.floor(Math.random() * len);
      return arr[a];
    };
    String.prototype.userTypeCheck = function(type) {

      /*
      @namespace userTypeCheck  
      @description  
      
      A function for normalizing users into an index based on the baptized social 
      network of their choosing.
      
      @TODO Move this into our client-side services layer.  
      @prototype String  
      @return {string}
       */
      var str, symbol;
      str = this;
      symbol = type === 'twitter' ? '@' : type === 'facebook' ? 'f' : '+';
      return symbol + str;
    };
    Array.prototype.keySort = function(keys) {

      /*
      @module utils  
      @namespace Array::keySort
       */
      var KL, k, keySort, obIx, obLen;
      keys = keys || {};
      obLen = function(obj) {
        var key, size;
        size = 0;
        key = void 0;
        for (key in obj) {
          if (obj.hasOwnProperty(key)) {
            size++;
          }
        }
        return size;
      };
      obIx = function(obj, ix) {
        var key, size;
        size = 0;
        key = void 0;
        for (key in obj) {
          if (obj.hasOwnProperty(key)) {
            if (size === ix) {
              return key;
            }
            size++;
          }
        }
        return false;
      };
      keySort = function(a, b, d) {
        d = (d !== null ? d : 1);
        if (a === b) {
          return 0;
        }
        if (a > b) {
          return 1 * d;
        } else {
          return -1 * d;
        }
      };
      KL = obLen(keys);
      if (!KL) {
        return this.sort(keySort);
      }
      for (k in keys) {
        keys[k] = (keys[k] === 'desc' || keys[k] === -1 ? -1 : (keys[k] === 'skip' || keys[k] === 0 ? 0 : 1));
      }
      this.sort(function(a, b) {
        var dir, ix, sorted;
        sorted = 0;
        ix = 0;
        while (sorted === 0 && ix < KL) {
          k = obIx(keys, ix);
          if (k) {
            dir = keys[k];
            sorted = keySort(a[k], b[k], dir);
            ix++;
          }
        }
        return sorted;
      });
      return this;
    };
    protocol = window.location.protocol ? window.location.protocol + '//' : 'https://';
    baseUrl = window.location.host ? window.location.host : 'lovestamp.io';
    appInit = function($rootScope, $stateParams, $location, $window, orbitalLoader) {

      /*
      @ngdoc run  
      @name interface.run:appInit  
      @function appInit  
      @description
      
      Initialization setup for the application.
       */
      var a, bodyClass, filterList, history, l, pageNameConstruct;
      $rootScope.userLanded = false;
      $rootScope.isActive = false;
      filterList = ['merchant/stamp/id', 'merchant/stamps', 'merchant/legal', 'merchant/dashboard', 'merchant/admin', 'merchant/store', 'merchant/social', 'merchant/requests', 'merchant/admin/payments'];
      history = [];
      bodyClass = [];
      bodyClass.push('js-enabled');
      bodyClass.push('js');
      bodyClass.push('lovestamp_io');
      bodyClass.push('base-template');
      pageNameConstruct = $location.path().replace(/\//g, '--');
      if (pageNameConstruct === '--') {
        bodyClass.push('index');
      } else {
        bodyClass.push(pageNameConstruct.substring(1).substring(1));
      }
      l = $location.path().replace($location.path().charAt(0), '');
      a = _.filter(filterList, function(_l) {
        return _l === l;
      });
      if (_.size(a)) {
        bodyClass.push('admin-like');
        bodyClass.push(l);
      }
      $rootScope.bodyClass = bodyClass.join(' ');
      $rootScope.$on('$locationChangeStart', function() {
        bodyClass = [];
        bodyClass.push('lovestamp_io');
        bodyClass.push('js-enabled');
        bodyClass.push('js');
        bodyClass.push('base-template');
        pageNameConstruct = $location.path().replace(/\//g, '--');
        if (pageNameConstruct === '--') {
          bodyClass.push('index');
        } else {
          bodyClass.push(pageNameConstruct.substring(1).substring(1));
        }
        l = $location.path().replace($location.path().charAt(0), '');
        a = _.filter(filterList, function(_l) {
          return _l === l;
        });
        if (_.size(a)) {
          bodyClass.push('admin-like');
          bodyClass.push(l);
        }
        $rootScope.bodyClass = bodyClass.join(' ');
      });
      $rootScope.domainUrl = protocol + baseUrl;
      $rootScope.siteUrl = protocol + baseUrl;

      /*
      To be used in setting the presentation on index controller's map view.
       */
      $rootScope.mapHeight = function() {
        return $window.innerHeight;
      };

      /*
      Main loading display.
       */
      $rootScope.$on('loading:show', function() {
        $rootScope.loadingSubpage = false;
        return orbitalLoader.show();
      });
      $rootScope.$on('loading:hide', function() {
        $rootScope.loadingSubpage = true;
        return orbitalLoader.hide();
      });
      $rootScope.$on('$locationChangeStart', function() {
        history.push($location.$$path);
      });
      $rootScope.$on('auth/user/roll-up', function(event, authData) {
        $rootScope.authData = authData;
        $rootScope.user = authData.authData;
        if (authData.authData) {
          $rootScope.isActive = true;
        }
      });
      $rootScope.back = function() {
        var prevUrl;
        prevUrl = (history.length > 1 ? history.splice(-2)[0] : '/');
        $location.path(prevUrl);
      };
      $rootScope.params = $stateParams;

      /**
      Wrapper for angular.isArray, isObject, etc checks for use in the view
      
      @inner
      @param {string} type The name of the check (casing sensitive).
      @param {string} value Value to check.
       */
      $rootScope.is = function(type, value) {
        return angular['is' + type](value);
      };

      /**
      Wrapper for $.isEmptyObject()
      
      @inner
      @param value {mixed} Value to be tested
      @return boolean
       */
      $rootScope.empty = function(value) {
        return angular.isEmptyObject(value);
      };
      $rootScope._ = _;
      if ($window.is_device && cordova.InAppBrowser) {
        $rootScope.InAppBrowserOpen = cordova.InAppBrowser.open;
      } else {
        $rootScope.InAppBrowserOpen = $window.open;
      }
      $rootScope.is_device = $window.is_device;
      if ($window.is_device) {
        $rootScope.image_path_prefix = './';
      } else {
        $rootScope.image_path_prefix = '/assets/';
      }
      $rootScope.moment = function(dateObject, formatSpecification) {

        /*
         */
        return moment(dateObject).format(formatSpecification);
      };
      $rootScope.iOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;

      /**
      Debugging Tools
      
      Allows you to execute debug functions from the view.
       */
      if (!window.DEBUG) {
        $rootScope.log = function(variable) {

          /**
          @inner
          @return undefined
           */
          console.log(variable);
        };
        $rootScope.alert = function(text) {

          /**
          @inner
          @return undefined
           */
          alert(text);
        };
      }
    };
    base_applicationDependencies = ['firebase', 'ionic', 'ngCordova', 'leaflet-directive', 'ui.router', 'ngAria', 'ngRoute', 'ngMaterial', 'ngAnimate', 'geolocation', 'LocalStorageModule', 'ngSanitize', 'ngTouch', 'ngMessages', 'angularPayments', 'elasticsearch', 'loggly', 'hmTouchEvents', 'ngDraggable', 'base64', 'messageBusModule', 'materialDatePicker', 'once', 'localytics.directives', 'doowb.angular-pusher', 'pascalprecht.translate', 'ngAudio', 'infinite-scroll'];
    ios_applicationDependencies = ['firebase', 'ionic', 'ngCordova', 'leaflet-directive', 'ui.router', 'ngAria', 'ngRoute', 'ngIOS9UIWebViewPatch', 'ngMaterial', 'ngAnimate', 'geolocation', 'LocalStorageModule', 'ngSanitize', 'ngTouch', 'ngMessages', 'angularPayments', 'elasticsearch', 'loggly', 'hmTouchEvents', 'ngDraggable', 'base64', 'messageBusModule', 'materialDatePicker', 'once', 'localytics.directives', 'doowb.angular-pusher', 'pascalprecht.translate', 'ngAudio'];
    applicationDependencies = window.is_ios ? ios_applicationDependencies : base_applicationDependencies;
    app = angular.module('lovestamp', applicationDependencies);
    app.constant('FIREBASE_URL', 'https://lovestamp.firebaseio.com');
    app.constant('loginRedirectPath', '/login');
    leafletExtended = function() {

      /*
      @ngdoc factory  
      @name interface.config:leafletExtended  
      @function leafletExtended  
      @description
      
        Extensions to Leaflet map UI.
       */
      var serviceInterface;
      serviceInterface = {};
      serviceInterface.OverlappingMarkerSpiderfier = window.OverlappingMarkerSpiderfier;
      return serviceInterface;
    };
    app.factory('leaflet.extended', [leafletExtended]);
    app.config(['$routeProvider', '$httpProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider', routes]);
    app.run(['$rootScope', '$stateParams', '$location', '$window', 'orbitalLoader', appInit]);
    geolocationAvailable = function($rootScope, $window, geolocation) {
      var serviceInterface, unitedStatesLocationConstruct, _getLocation;
      serviceInterface = {};
      serviceInterface.geolocationAvailable = false;
      unitedStatesLocationConstruct = {
        coords: {
          latitude: 37.6,
          longitude: -95.665
        }
      };
      _getLocation = geolocation.getLocation();
      _getLocation.then(function(d) {
        serviceInterface.geolocationAvailable = d;
        angular.element(document).ready(function() {
          $rootScope.$broadcast('geolocationAvailable', d);
        });
      }, function() {
        var d;
        d = unitedStatesLocationConstruct;
        serviceInterface.geolocationAvailable = d;
        angular.element(document).ready(function() {
          $rootScope.$broadcast('geolocationAvailable', d);
        });
      });
      return serviceInterface;
    };
    app.factory('geolocationAvailable', geolocationAvailable);
    cacheTemplates = function($templateCache, $stateProvider, $http, $timeout) {
      var t;
      if (window.is_device === false) {
        t = $timeout(function() {
          $http.get('/assets/scripts/modules/www/controllers/stamps/partials/page.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/store/partials/page.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/social/partials/page.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/dashboard/partials/page.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/admin/partials/base.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/requests/partials/base.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/payments/partials/base.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/payments/partials/alt-base.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/stamp-signup/partials/page.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/header/partials/nav.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/www/controllers/footer/partials/socket-region.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/index/partials/base.html', {
            cache: $templateCache
          });
          $http.get('/assets/scripts/modules/index/partials/bottom-sheet-grid-template.html', {
            cache: $templateCache
          });
          $timeout.cancel(t);
        }, 7000);
      }
    };
    fanIntercept = function($window, $location, $rootScope, $injector) {

      /*
      @ngdoc run  
      @name interface.config:fanInterceptConfig  
      @function fanInterceptConfig  
      @description
      
        Fan intercept URL interface.
       */
      $rootScope.$on('$locationChangeStart', function() {
        if ($location.path().indexOf('admin') !== -1 && environment.isFanSubdomain) {
          return $injector.get('$state').go('login-fan-auth');
        }
      });
      if ($location.host().indexOf('fan') !== -1 && environment.isFanSubdomain === true) {
        if ($window.is_ios) {
          $injector.get('$state').go('home-ios');
        } else {
          $injector.get('$state').go('home');
        }
      }
      if ($location.host().indexOf('merchant') !== -1 && environment.isFanSubdomain === false) {
        $injector.get('$state').go('dashboard');
      }
    };
    app.run(['$window', '$location', '$rootScope', '$injector', fanIntercept]);
    logglyConfig = function(loggly) {

      /*
      @ngdoc run  
      @name interface.config:logglyConfig  
      @function logglyConfig  
      @description
      
        Loggly client configuration.
       */
      loggly.setApiKey(environment.loggly.logglyApiKey);
    };
    app.run(['loggly', logglyConfig]);
    basicAuthorization = function($httpProvider) {

      /*
      @ngdoc config  
      @name interface.config:basicAuthorization  
      @function basicAuthorization  
      @description
      
        Basic route authorization decoration.
      
        Authorization for certain routes by stamp ownership or blessed profile 
        status.
       */
      return $httpProvider.interceptors.push([
        '$q', '$injector', '$rootScope', function($q, $injector, $rootScope) {
          return {
            'request': function(config) {
              $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                if (error.status === 401) {
                  return $injector.get('$state').go('login-fan-auth');
                }
              });
              $rootScope.$on('auth/user/roll-up', function(event, authData) {
                if (authData && authData.authData) {
                  $rootScope.$broadcast('map:ready');
                  $rootScope.$broadcast('auth:ready', authData);
                  $rootScope.userLanded = true;
                } else {
                  if (window.is_fan) {
                    $injector.get('$state').go('login-fan-auth');
                  } else if (window.is_merchant) {
                    $injector.get('$state').go('login-merchant-auth');
                  } else {
                    $injector.get('$state').go('login-fan-auth');
                  }
                }
              });
              $rootScope.$on('user:hasStampOrIsActive', function(event, userHasStampOrIsActive) {
                if (userHasStampOrIsActive !== true) {
                  $rootScope.loadingSubpage = true;
                  return $injector.get('$state').go('store-request');
                }
              });
              return config;
            },
            'response': function(response) {
              return $q.when(response);
            },
            'responseError': function(reason) {
              if (reason.status === 401) {
                if (window.is_fan) {
                  $injector.get('$state').go('login-fan-auth');
                } else if (window.is_merchant) {
                  $injector.get('$state').go('login-merchant-auth');
                } else {
                  $injector.get('$state').go('login-fan-auth');
                }
              }
              return $q.reject(reason);
            }
          };
        }
      ]);
    };
    app.config(['$httpProvider', basicAuthorization]);
    localStorageConfig = function(localStorageServiceProvider) {

      /*
      @ngdoc config  
      @name interface.config:localStorageConfig
      @function localStorageConfig
      @description
      
        localStorage configuration.
       */
      localStorageServiceProvider.setPrefix('LoveStamp').setStorageType('localStorage').setNotify(true, true);
    };
    app.config(['localStorageServiceProvider', localStorageConfig]);
    orbitalLoaderConfig = function($log, $rootScope) {

      /*
      @ngdoc factory  
      @name interface.factory:orbitalLoaderConfig  
      @function orbitalLoaderConfig  
      @description
      
        Orbital loader configuration.
       */
      var serviceInterface;
      serviceInterface = {};
      serviceInterface.show = function() {
        $rootScope.loader = true;
      };
      serviceInterface.hide = function() {
        $rootScope.loader = false;
      };
      return serviceInterface;
    };
    app.factory('orbitalLoader', ['$log', '$rootScope', orbitalLoaderConfig]);
    app.config(function($provide) {
      $provide.decorator('$q', function($delegate) {
        var allSettled;
        allSettled = function(promises) {
          var counter, deferred, results;
          deferred = $delegate.defer();
          counter = 0;
          results = angular.isArray(promises) ? [] : {};
          angular.forEach(promises, function(promise, key) {
            counter++;
            $delegate.when(promise).then(function(value) {
              if (results.hasOwnProperty(key)) {
                return;
              }
              results[key] = {
                status: 'fulfilled',
                value: value
              };
              if (!--counter) {
                deferred.resolve(results);
              }
            }, function(reason) {
              if (results.hasOwnProperty(key)) {
                return;
              }
              results[key] = {
                status: 'rejected',
                reason: reason
              };
              if (!--counter) {
                deferred.resolve(results);
              }
            });
          });
          if (counter === 0) {
            deferred.resolve(results);
          }
          return deferred.promise;
        };
        $delegate.allSettled = allSettled;
        return $delegate;
      });
    });
    app.config(function($provide) {
      $provide.decorator('$q', function($delegate) {
        var isPromiseLike, serial;
        isPromiseLike = function(obj) {
          return obj && angular.isFunction(obj.then);
        };
        serial = function(tasks) {
          var error, prevPromise;
          prevPromise = void 0;
          error = new Error();
          angular.forEach(tasks, function(task, key) {
            var fail, nextPromise, notify, success;
            success = task.success || task;
            fail = task.fail;
            notify = task.notify;
            nextPromise = void 0;
            if (!prevPromise) {
              nextPromise = success();
              if (!isPromiseLike(nextPromise)) {
                error.message = 'Task ' + key + ' did not return a promise.';
                throw error;
              }
            } else {
              nextPromise = prevPromise.then(function(data) {
                var ret;
                if (!success) {
                  return data;
                }
                ret = success(data);
                if (!isPromiseLike(ret)) {
                  error.message = 'Task ' + key + ' did not return a promise.';
                  throw error;
                }
                return ret;
              }, function(reason) {
                var ret;
                if (!fail) {
                  return $delegate.reject(reason);
                }
                ret = fail(reason);
                if (!isPromiseLike(ret)) {
                  error.message = 'Fail for task ' + key + ' did not return a promise.';
                  throw error;
                }
                return ret;
              }, notify);
            }
            prevPromise = nextPromise;
          });
          return prevPromise || $delegate.when();
        };
        $delegate.serial = serial;
        return $delegate;
      });
    });
    productTourStage = function($rootScope, leafletData, $timeout, $location) {

      /*
      @ngdoc directive  
      @name interface.directive:productTourStage  
      @element body  
      @function productTourStage  
      @description  
      
      A product tour of the LoveStamp application that uses image overlays 
      organized and managed by AngularJS directive.
      
      @example  
      
          <body
            class="{{ bodyClass }}"
            product-tour-stage
            itemscope
            itemtype="https://schema.org/WebPage"
          >
            <!-- a full on page -->
          </body>
       */
      return {
        restrict: 'A',
        link: function($scope, element) {
          var absent, ch;
          element.on('click', function(e) {
            if (e.target.tagName === 'svg' || e.target.tagName === 'img' || e.target.tagName === 'path') {
              return;
            }
            if (angular.element(e.target).hasClass('md-bottom-sheet-backdrop')) {
              element.removeClass('pt--undefined');
              element.removeClass('pt--hangout');
              element.removeClass('pt--mail');
              element.removeClass('pt--message');
              element.removeClass('pt--copy');
              element.removeClass('pt--facebook');
              element.removeClass('pt--twitter');
              element.removeClass('pt--google');
              $rootScope.productTourLoaded = true;
            }
            if ($rootScope.productTourLoaded) {
              return leafletData.getMap('index').then(function(map) {
                return map.invalidateSize(false);
              });
            }
          });
          ch = function(newValue, timeout, _switch) {
            var cl, _cl;
            cl = angular.element(element).attr('class');
            _cl = cl.split(' ');
            element.addClass('exiting');
            return $timeout(function() {
              _.forEach(_cl, function(na) {
                if (na.indexOf('pt--') !== -1) {
                  return element.removeClass(na);
                }
              });
              element.removeClass('exiting');
              element.addClass('pt--' + newValue);
              if (_switch || newValue) {
                element.removeClass('show--product-tour--');
                return element.addClass('show--product-tour');
              }
            }, timeout);
          };
          absent = -1;
          $scope.$on('hide:pt', function() {
            element.removeClass('show--product-tour--');
            return element.removeClass('show--product-tour');
          });
          $scope.$watch('icon', function(newValue) {
            var timeout, _switch;
            if (newValue) {
              timeout = 250;
              _switch = true;
              if ($location.path().indexOf('product-tour') !== absent) {
                return ch(newValue, timeout, 'undefined');
              }
            }
          });
          $rootScope.$on('$locationChangeSuccess', function() {
            var timeout;
            timeout = 0;
            if ($location.path().indexOf('product-tour') !== absent) {
              return ch('undefined', timeout);
            }
          });
        }
      };
    };
    app.directive('productTourStage', ['$rootScope', 'leafletData', '$timeout', '$location', productTourStage]);
    clickMonad = function($timeout) {

      /*
      @ngdoc directive  
      @name interface.directive:clickMonad  
      @element a  
      @function clickMonad  
      @description  
      
      Prevent double trigger on click events for ng-click directives, and possibly 
      other simple interactive captures.
      
      @example  
      
          <a
            module="interface"
            click-monad
            ng-click="someFunction()"
          >
            <span class="fa fa-flash"></span>
          </a>
       */
      var delay;
      delay = 500;
      return {
        restrict: 'A',
        priority: -1,
        link: function($scope, element) {
          var clickHandler, disabled;
          disabled = false;
          clickHandler = function(event) {
            if (disabled) {
              event.preventDefault();
              return event.stopImmediatePropagation();
            } else {
              disabled = true;
              return $timeout(function() {
                disabled = false;
              }, delay, false);
            }
          };
          $scope.$on('$destroy', function() {
            return element.off('click', clickHandler);
          });
          element.on('click', clickHandler);
        }
      };
    };
    app.directive('clickMonad', ['$timeout', clickMonad]);
    logProviderConfig = function($logProvider) {

      /*
      @ngdoc config  
      @name interface.config:logProviderConfig  
      @function logjProviderConfig  
      @description
      
      Log provider exposure.
       */
      return $logProvider.debugEnabled(true);
    };
    app.config(['$logProvider', logProviderConfig]);
    ariaProviderConfig = function($ariaProvider) {

      /*
      @ngdoc config  
      @name interface.config:ariaProviderConfig  
      @function ariaProviderConfig  
      @description
      
      Accessibility implementation hooks.
       */
      return $ariaProvider.config({
        ariaInvalid: false,
        tabindex: true
      });
    };
    app.config(['$ariaProvider', ariaProviderConfig]);
    urlIntercept = function($urlRouterProvider) {

      /*
      @ngdoc config  
      @name interface.config:urlIntercept  
      @function urlIntercept  
      @description
      
      URL Intercept.
       */
      return $urlRouterProvider.deferIntercept();
    };
    app.config(['$urlRouterProvider', urlIntercept]);
    routeManager = function($rootScope, $stateParams, $urlRouter, $timeout) {

      /*
      @ngdoc run  
      @name interface.run:routeManager  
      @function routeManager  
      @description
      
      Route manager.
       */
      $rootScope.oldStampAlias = null;
      $rootScope.$on('$locationChangeSuccess', function(e, newUrl) {
        var profileRegions, stampsRegions;
        profileRegions = newUrl && newUrl.indexOf('@') !== -1 && newUrl.indexOf('profile') === -1;
        stampsRegions = newUrl && newUrl.indexOf('@') !== -1;
        $timeout(function() {
          $rootScope.$apply(function() {
            $rootScope.oldStampAlias = $stateParams.stampAlias;
          });
        }, 0);
        if (profileRegions && stampsRegions) {
          if ($rootScope.oldStampAlias !== null) {
            return e.preventDefault();
          } else {
            return $urlRouter.sync();
          }
        }
      });
      return $urlRouter.listen();
    };
    app.run(['$rootScope', '$stateParams', '$urlRouter', '$timeout', routeManager]);
    app.config([
      '$compileProvider', function($compileProvider) {
        return $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
      }
    ]);
    cordovaCheck = function($timeout, $q, $rootScope, $window, $ionicPlatform) {

      /*
      @usage
      
        define ['interface'], (__interface__) ->
          controller = (cordovaCheck) ->
            cordovaCheck.ionicReady()
            cordovaCheck.documentReady().then () ->
              console.log 'Device is ready!'
              cordovaCheck.addCapability(...).then(...)
          __interface__.controller(['cordovaCheck', controller])
           * Or as module controller:
          ['cordovaCheck', controller]
       */
      var checkInterface;
      console.dir($ionicPlatform);
      checkInterface = {};
      checkInterface.capabilities = [];
      checkInterface.addCapability = function(config) {

        /*
        Add Capability.
        
        @param {object} config
        @usage
        
          cordovaCheck.addCapability({
            featureName: 'StatusBar'
            featureCallback: () ->
              StatusBar.hide()
            timeout: 0
          }).then((status) ->
            console.log status
          , (status) ->
            console.log status
          )
         */
        var callbackStatus, def, _capabilities, _capability;
        def = $q.defer();
        config.featureAdded = true;
        config.featureFired = false;
        _capabilities = _(this.capabilities).push(config);
        _capabilities.commit();
        _capability = _capabilities.last();
        if ($window[_capability.featureName] && _capability.featureFired === false) {
          callbackStatus = _capability.featureCallback();
          $timeout(function() {
            if (callbackStatus !== void 0) {
              _capability.featureFired = true;
              return def.resolve(callbackStatus);
            } else {
              _capability.featureFired = false;
              console.log('A device capability should explicitly tell us its status.');
              return def.reject('No status provided but fired (assumption).');
            }
          }, _capability.timeout || 0);
        }
        return def.promise;
      };
      checkInterface.ionicReady = function() {

        /*
        Ionic Device Ready Check
         */
        var def, e, onDeviceReady;
        def = $q.defer();
        onDeviceReady = function() {
          return $timeout(function() {
            return $rootScope.$apply(def.resolve);
          }, 0);
        };
        try {
          $ionicPlatform.ready(onDeviceReady);
        } catch (_error) {
          e = _error;
          ionic.Platform.ready(onDeviceReady);
        }
        return def.promise;
      };
      checkInterface.ngDocumentReady = function(useCapture, wantsUntrusted) {

        /*
        Angularized Browser Document Device Ready Check
        
        @param {boolean} useCapture
          Event dispatch from `listener` before `EventTarget`.
        @param {boolean} wantsUntrusted
          Synthetic events for Gecko under "add-on" scenarios.
         */
        var def, ngDocument, onDeviceReady, _useCapture, _wantsUntrusted;
        def = $q.defer();
        _useCapture = useCapture || false;
        _wantsUntrusted = wantsUntrusted || void 0;
        onDeviceReady = function() {
          return $timeout(function() {
            return $rootScope.$apply(def.resolve);
          }, 0);
        };
        ngDocument = angular.element(document);
        angular.element(ngDocument).ready(function() {
          ngDocument.on('deviceready', onDeviceReady);
        });
        return def.promise;
      };
      checkInterface.documentReady = function(useCapture, wantsUntrusted) {

        /*
        Browser Document Device Ready Check
        
        @param {boolean} useCapture
          Event dispatch from `listener` before `EventTarget`.
        @param {boolean} wantsUntrusted
          Synthetic events for Gecko under "add-on" scenarios.
         */
        var def, onDeviceReady, _useCapture, _wantsUntrusted;
        def = $q.defer();
        _useCapture = useCapture || false;
        _wantsUntrusted = wantsUntrusted || void 0;
        onDeviceReady = function() {
          return $timeout(function() {
            return $rootScope.$apply(def.resolve);
          }, 0);
        };
        if (_wantsUntrusted !== void 0) {
          document.addEventListener('deviceready', onDeviceReady, _useCapture, _wantsUntrusted);
        } else {
          document.addEventListener('deviceready', onDeviceReady, _useCapture);
        }
        return def.promise;
      };
      return checkInterface;
    };
    app.factory('cordovaCheck', ['$timeout', '$q', '$rootScope', '$window', '$ionicPlatform', cordovaCheck]);
    app.run([
      '$ionicPlatform', '$rootScope', function($ionicPlatform, $rootScope) {
        if ($ionicPlatform && window.DEBUG) {
          $rootScope.$ionicPlatform = $ionicPlatform;
        }

        /*
            console.dir $ionicPlatform
            > Object
            $backButtonActions: Object
            _hasBackButtonHandler: true
            hardwareBackButtonClick: (e)
            is: (e)
            offHardwareBackButton: (e)
            on: (e,t)
            onHardwareBackButton: (e)
            ready: (t)
            registerBackButtonAction: (e,n,i)
            __proto__: Object
         */
        return ionic.Platform.fullScreen();
      }
    ]);
    document.addEventListener('deviceready', function() {
      if (window.is_android) {
        cordova.exec(null, null, 'SplashScreen', 'hide', []);
      }
    });
    $body = angular.element(document);
    return angularAMD.bootstrap(app, true, $body);
  });

}).call(this);

//# sourceMappingURL=interface.js.map
