angular.module('partials').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/scripts/partials/heat-map.html',
    "<!-- Would be nice to use <meter>, but restyling it to what we want\n" +
    "     is a large pain in the sphincter.\n" +
    "-->\n" +
    "<div class=\"fa fa-pointer-down\"></div>\n" +
    "<ol>\n" +
    "    <li data-ng-repeat=\"scaleElement in scale\" data-ng-style=\"{\n" +
    "        width:                          scaleElement.width,\n" +
    "        'background-color':             scaleElement.color,\n" +
    "        'border-top-left-radius':       scaleElement.borderRadius.topLeft,\n" +
    "        'border-bottom-left-radius':    scaleElement.borderRadius.bottomLeft,\n" +
    "        'border-top-right-radius':      scaleElement.borderRadius.topRight,\n" +
    "        'border-bottom-right-radius':   scaleElement.borderRadius.bottomRight\n" +
    "    }\n" +
    "\"></li>\n" +
    "</ol>"
  );


  $templateCache.put('app/scripts/partials/loader.html',
    "<!-- Just another loader... -->"
  );


  $templateCache.put('app/scripts/partials/notification.html',
    "<md-toast>\n" +
    "  <span flex>{{currentStatus}}</span>\n" +
    "  <md-button ng-click=\"closeNotification()\">\n" +
    "    Close\n" +
    "  </md-button>\n" +
    "</md-toast>"
  );


  $templateCache.put('app/scripts/partials/payMaker.html',
    "<!--\n" +
    "# fileOverview\n" +
    "\n" +
    "                   _______       _\n" +
    "                  (_______)     | |\n" +
    " ____  _____ _   _ _  _  _ _____| |  _ _____  ____\n" +
    "|  _ \\(____ | | | | ||_|| (____ | |_/ ) ___ |/ ___)\n" +
    "| |_| / ___ | |_| | |   | / ___ |  _ (| ____| |\n" +
    "|  __/\\_____|\\__  |_|   |_\\_____|_| \\_)_____)_|\n" +
    "|_|         (____/\n" +
    "\n" +
    "## description\n" +
    "\n" +
    "A modal window implementation of Stripe's front end in AngularJS.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<md-dialog aria-label=\"LoveStamp Checkout\" ng-init=\"load()\">\n" +
    "\n" +
    "  <div class=\"md-dialog--wrapper\">\n" +
    "    <figure class=\"stripe-badge\">\n" +
    "      <img src=\"./assets/images/006--stripe.png\" alt=\"\">\n" +
    "    </figure>\n" +
    "\n" +
    "    <figure>\n" +
    "      <img src=\"./assets/images/logo--merchant-backend.png\">\n" +
    "    </figure>\n" +
    "\n" +
    "    <figure class=\"verified\">\n" +
    "      <img src=\"./assets/images/005--verified.png\" alt=\"\">\n" +
    "    </figure>\n" +
    "\n" +
    "    <form stripe-form=\"handleStripe\" name=\"payment\" class=\"forms--payment\">\n" +
    "\n" +
    "      <md-content>\n" +
    "          <md-subheader ng-class=\"{\n" +
    "              'animated': true,\n" +
    "              'fade-in': true\n" +
    "            }\" style=\"padding-left: 0\">\n" +
    "\n" +
    "            <span style=\"color: #111;\n" +
    "                display: block;\n" +
    "                margin-top: 2rem;\n" +
    "                font-size: 1.785rem;\n" +
    "                font-weight: 500;\n" +
    "                text-transform: uppercase;\n" +
    "                letter-spacing: 7px\">LoveStamp Checkout</span>\n" +
    "\n" +
    "            <aside ng-class=\"{\n" +
    "                'animated': true,\n" +
    "                'fade-in': true\n" +
    "              }\" ng-style=\"{\n" +
    "\n" +
    "                'padding-top': '1rem',\n" +
    "                'font-weight': '500',\n" +
    "                'color': '#111',\n" +
    "                'text-shadow': 'none'\n" +
    "\n" +
    "              }\">\n" +
    "              Your order is for <br>\n" +
    "              <span style=\"font-size: 2rem\n" +
    "                font-weight: 600;\n" +
    "                box-shadow: none !important;\n" +
    "                text-shadow: none !important;\n" +
    "                line-height: 0;\n" +
    "                font-variant: normal;\n" +
    "                font-size: 2rem;\n" +
    "                font-weight: 400;\n" +
    "                padding-bottom: 2rem\">{{customerItem.qty}} {{label}}</span>\n" +
    "            </aside>\n" +
    "\n" +
    "          </md-subheader>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "                                   _ _\n" +
    "                                  (_) |\n" +
    "           _____ _____ ____  _____ _| |\n" +
    "          | ___ (_____)    \\(____ | | |\n" +
    "          | ____|     | | | / ___ | | |\n" +
    "          |_____)     |_|_|_\\_____|_|\\_)\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <div class=\"field-row\">\n" +
    "            <md-input-container>\n" +
    "              <label for=\"payment.email\">\n" +
    "                <span class=\"fa fa-envelope-o\"></span>\n" +
    "                {{notificationError||'E-mail'}}\n" +
    "              </label>\n" +
    "              <md-input ng-attr-required=\"paymentConstruct.showRememberMeInput === true\" focus-me id=\"payment.email\" name=\"payment.email\" class=\"payment--email\" ng-model=\"paymentConstruct.email\"></md-input>\n" +
    "            </md-input-container>\n" +
    "          </div>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "               _       _  _\n" +
    "              | |     | |(_)\n" +
    "            __| |_____| | _ _   _ _____  ____ _   _\n" +
    "           / _  | ___ | || | | | | ___ |/ ___) | | |\n" +
    "          ( (_| | ____| || |\\ V /| ____| |   | |_| |\n" +
    "           \\____|_____)\\_)_| \\_/ |_____)_|    \\__  |\n" +
    "                                             (____/\n" +
    "                     _     _\n" +
    "                    | |   | |\n" +
    "           _____  __| | __| | ____ _____  ___  ___\n" +
    "          (____ |/ _  |/ _  |/ ___) ___ |/___)/___)\n" +
    "          / ___ ( (_| ( (_| | |   | ____|___ |___ |\n" +
    "          \\_____|\\____|\\____|_|   |_____|___/(___/\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <div class=\"field-row\">\n" +
    "\n" +
    "            <div class=\"field-label\">\n" +
    "              <div class=\"field-edit\">\n" +
    "\n" +
    "                <span ng-click=\"showSaveDelivery = true; fieldRowEditDeliveryAddress('address.shipping.content')\" ng-show=\"showSaveDelivery === false || showSaveDelivery === undefined\" class=\"field-sub-label\">\n" +
    "\n" +
    "                  <span class=\"hint--right\" data-hint=\"Click here to edit the content below\">\n" +
    "                    <span class=\"fa fa-edit animated tada\"></span>\n" +
    "                    Edit\n" +
    "                  </span>\n" +
    "\n" +
    "                </span>\n" +
    "\n" +
    "                <span ng-click=\"showSaveDelivery = false; fieldRowEditDeliveryAddress('address.shipping.content')\" ng-show=\"showSaveDelivery === true\" class=\"field-sub-label\">\n" +
    "                  <span class=\"fa fa-save animated tada\"></span>\n" +
    "                  Save\n" +
    "                </span>\n" +
    "\n" +
    "              </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"field-input\">\n" +
    "\n" +
    "              <md-input-container flex>\n" +
    "\n" +
    "                <label for=\"address.shipping\">\n" +
    "                  <span class=\"fa fa-home\"></span>\n" +
    "                  Delivery Address\n" +
    "                </label>\n" +
    "\n" +
    "                <textarea columns=\"1\" id=\"address.shipping\" name=\"address.shipping\" ng-readonly=\"showSaveDelivery === false || showSaveDelivery === undefined\" ng-class=\"{\n" +
    "                    'content-editable': address.shipping.editing,\n" +
    "                    'content-updated': address.shipping.contentUpdated\n" +
    "                  }\" ng-model=\"address.shipping.content\"></textarea>\n" +
    "\n" +
    "              </md-input-container>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "             _\n" +
    "           _| |_ _   _ ____  _____\n" +
    "          (_   _) | | |  _ \\| ___ |\n" +
    "            | |_| |_| | |_| | ____|\n" +
    "             \\__)\\__  |  __/|_____)\n" +
    "                (____/|_|\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <div class=\"field-row\">\n" +
    "\n" +
    "            <md-radio-group id=\"payment.type\" name=\"payment.type\" class=\"payment--type\" ng-required=\"true\" ng-model=\"paymentConstruct.type\" ng-init=\"paymentConstruct.type = '1';\" name=\"color\" aria-label=\"Card or Bitcoin\" style=\"margin-bottom: 0\">\n" +
    "              <md-radio-button aria-label=\"Choose to Pay with Credit or Debit Card\" value=\"1\">\n" +
    "                Card\n" +
    "              </md-radio-button>\n" +
    "              <md-radio-button aria-label=\"Choose to Pay with Bitcoin\" value=\"2\">\n" +
    "                Bitcoin\n" +
    "              </md-radio-button>\n" +
    "            </md-radio-group>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "\n" +
    "           _     _                 _       _______     _     _\n" +
    "          | |   (_)  _            (_)     (_______)   | |   | |\n" +
    "          | |__  _ _| |_ ____ ___  _ ____  _______  __| | __| |_   _\n" +
    "          |  _ \\| (_   _) ___) _ \\| |  _ \\|  ___  |/ _  |/ _  | | | |\n" +
    "          | |_) ) | | |( (__| |_| | | | | | |   | ( (_| ( (_| | |_| |\n" +
    "          |____/|_|  \\__)____)___/|_|_| |_|_|   |_|\\____|\\____|\\__  |\n" +
    "                                                              (____/\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <div ng-show=\"paymentConstruct.type === '2'\" class=\"field-row field-row--bitcoinAddy\">\n" +
    "\n" +
    "            <p>To complete your payment, please send bitcoins to the address \n" +
    "            below:</p>\n" +
    "\n" +
    "            <md-input-container>\n" +
    "              <label for=\"payment.bitcoinAddy\">\n" +
    "                <span class=\"fa fa-bitcoin\"></span>\n" +
    "              </label>\n" +
    "              <md-input id=\"payment.bitcoinAddy\" name=\"payment.bitcoinAddy\" ng-model=\"paymentConstruct.bitcoinAddy\" ng-readonly=\"true\"></md-input>\n" +
    "            </md-input-container>\n" +
    "          </div>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "                                _                      _\n" +
    "                               | |                    | |\n" +
    "            ____ _____  ____ __| |   ____  _   _ ____ | |__  _____  ____\n" +
    "           / ___|____ |/ ___) _  |  |  _ \\| | | |    \\|  _ \\| ___ |/ ___)\n" +
    "          ( (___/ ___ | |  ( (_| |  | | | | |_| | | | | |_) ) ____| |\n" +
    "           \\____)_____|_|   \\____|  |_| |_|____/|_|_|_|____/|_____)_|\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <div class=\"field-set--classicAddy\" ng-show=\"paymentConstruct.type === '1'\">\n" +
    "\n" +
    "          <div class=\"field-row field-row--number\">\n" +
    "            <md-input-container>\n" +
    "\n" +
    "              <label for=\"payment.cardNumber\">\n" +
    "                <span class=\"fa fa-credit-card\"></span>\n" +
    "                Card number\n" +
    "              </label>\n" +
    "\n" +
    "              <md-input id=\"payment.cardNumber\" name=\"payment.cardNumber\" ng-attr-required=\"paymentConstruct.type === '1'\" ng-model=\"number\" payments-validate=\"card\" payments-format=\"card\" payments-type-model=\"type\" ng-class=\"payment.number.$card.type\" aria-label=\"Card number\" placeholder=\"XXXX XXXX XXXX XXXX\"></md-input>\n" +
    "\n" +
    "            </md-input-container>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "                                _                      _                   _\n" +
    "                               | |                    (_)              _  (_)\n" +
    "            ____ _____  ____ __| |   _____ _   _ ____  _  ____ _____ _| |_ _  ___  ____\n" +
    "           / ___|____ |/ ___) _  |  | ___ ( \\ / )  _ \\| |/ ___|____ (_   _) |/ _ \\|  _ \\\n" +
    "          ( (___/ ___ | |  ( (_| |  | ____|) X (| |_| | | |   / ___ | | |_| | |_| | | | |\n" +
    "           \\____)_____|_|   \\____|  |_____|_/ \\_)  __/|_|_|   \\_____|  \\__)_|\\___/|_| |_|\n" +
    "                                                |_|\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <div class=\"field-row field-row--expiry\">\n" +
    "\n" +
    "            <md-input-container>\n" +
    "\n" +
    "              <label for=\"payment.cardExpiry\">\n" +
    "                <span class=\"fa fa-calendar-o\"></span>\n" +
    "                Card Expiration\n" +
    "              </label>\n" +
    "\n" +
    "              <md-input id=\"payment.cardExpiry\" name=\"payment.cardExpiry\" class=\"payment--cardExpiry\" aria-label=\"Card expiration month and year\" ng-attr-required=\"paymentConstruct.type === '1'\" ng-model=\"expiry\" payments-validate=\"expiry\" payments-format=\"expiry\" placeholder=\"MM / YYYY\"></md-input>\n" +
    "\n" +
    "            </md-input-container>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "            ____ _   _ ____\n" +
    "           / ___) | | / ___)\n" +
    "          ( (___ \\ V ( (___\n" +
    "           \\____) \\_/ \\____)\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "            <div class=\"field-row field-row--security\">\n" +
    "\n" +
    "              <md-input-container>\n" +
    "\n" +
    "                <label for=\"payment.cardCvc\">\n" +
    "                  <span class=\"fa fa-lock\"></span>\n" +
    "                  <span title=\"Card security code\">CVC</span>\n" +
    "                </label>\n" +
    "\n" +
    "                <md-input id=\"payment.cardCvc\" name=\"payment.cardCvc\" class=\"payment--cardCvc\" aria-label=\"Card security code\" ng-attr-required=\"paymentConstruct.type === '1'\" ng-model=\"cvc\" payments-validate=\"cvc\" payments-format=\"cvc\" payments-type-model=\"type\"></md-input>\n" +
    "\n" +
    "              </md-input-container>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "                                         _\n" +
    "                                        | |\n" +
    "            ____ _____ ____  _____ ____ | |__  _____  ____ _____ ____  _____\n" +
    "           / ___) ___ |    \\| ___ |    \\|  _ \\| ___ |/ ___|_____)    \\| ___ |\n" +
    "          | |   | ____| | | | ____| | | | |_) ) ____| |         | | | | ____|\n" +
    "          |_|   |_____)_|_|_|_____)_|_|_|____/|_____)_|         |_|_|_|_____)\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <!--div class=\"field-row\">\n" +
    "\n" +
    "            <md-checkbox\n" +
    "              id=\"payment.rememberMe\"\n" +
    "              name=\"payment.rememberMe\"\n" +
    "              class=\"payment--remember-me\"\n" +
    "              ng-model=\"showRememberMeInput\"\n" +
    "              aria-label=\"Remember Me?\"\n" +
    "            >\n" +
    "\n" +
    "              Remember Me?\n" +
    "\n" +
    "              <span class=\"sep\">|</span>\n" +
    "\n" +
    "              <span\n" +
    "                class=\"fa fa-info\"\n" +
    "              ></span>\n" +
    "\n" +
    "            </md-checkbox>\n" +
    "\n" +
    "            <div\n" +
    "              ng-show=\"showRememberMeInput && paymentConstruct.rememberMe != true\"\n" +
    "              class=\"remember-me--extra\"\n" +
    "            >\n" +
    "\n" +
    "              <md-divider></md-divider>\n" +
    "\n" +
    "              <br />\n" +
    "\n" +
    "              <p>For security, please enter your mobile phone number:</p>\n" +
    "\n" +
    "              <md-input-container\n" +
    "                ng-show=\"! c\"\n" +
    "              >\n" +
    "\n" +
    "                <label for=\"payment.phoneNumber\">\n" +
    "                  <span class=\"fa fa-phone\"></span>\n" +
    "                  <span\n" +
    "                    ng-show=\"paymentConstruct.rememberMe == true\"\n" +
    "                    class=\"fa fa-check\"\n" +
    "                  ></span>\n" +
    "                </label>\n" +
    "\n" +
    "                <md-input\n" +
    "                  id=\"payment.phoneNumber\"\n" +
    "                  name=\"payment.phoneNumber\"\n" +
    "                  class=\"payment--phoneNumber\"\n" +
    "                  ng-model=\"paymentConstruct.phoneNumber\"\n" +
    "                  placeholder=\"(555) 123-1234\"\n" +
    "                  ng-blur=\"validateAuth()\"\n" +
    "                ></md-input>\n" +
    "\n" +
    "                <md-tooltip md-autohide=\"true\">\n" +
    "                  We use Stripe to securely store your payment info for quick checkout on this site and others.\n" +
    "                </md-tooltip>\n" +
    "\n" +
    "              </md-input-container>\n" +
    "\n" +
    "              <md-input-container\n" +
    "                ng-show=\"c\"\n" +
    "              >\n" +
    "\n" +
    "                <label for=\"payment.authy\">\n" +
    "                  <span class=\"fa fa-unlock\"></span>\n" +
    "                </label>\n" +
    "\n" +
    "                <md-input\n" +
    "                  id=\"payment.authy\"\n" +
    "                  name=\"payment.authy\"\n" +
    "                  class=\"payment--authy\"\n" +
    "                  ng-model=\"paymentConstruct.authy\"\n" +
    "                  placeholder=\"XXXXXX\"\n" +
    "                  ng-blur=\"resolveAuth()\"\n" +
    "                ></md-input>\n" +
    "\n" +
    "              </md-input-container>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "          </div-->\n" +
    "\n" +
    "          <figure class=\"address-qr-code\" ng-if=\"qrBitcoinAddress\" ng-show=\"paymentConstruct.type === '2'\">\n" +
    "            <qrcode version=\"10\" error-correction-level=\"H\" size=\"200\" data=\"{{qrBitcoinAddressHref}}\" download></qrcode>\n" +
    "\n" +
    "            <figcaption ng-show=\"exchangeRateBasedAmount\">\n" +
    "              <p style=\"font-size: 2rem;\n" +
    "              line-height: 1;\n" +
    "              margin: 0\">{{exchangeRateBasedAmount}} BTC</p>\n" +
    "            </figcaption>\n" +
    "\n" +
    "            <!--\n" +
    "               -<figcaption class=\"open-wallet\" ng-if=\"qrBitcoinAddressHref\">\n" +
    "               -  <a href=\"{{qrBitcoinAddressHref}}\">Open Wallet</a>\n" +
    "               -</figcaption>\n" +
    "               -->\n" +
    "\n" +
    "          </figure>\n" +
    "\n" +
    "          <div class=\"md-actions\" layout=\"row\">\n" +
    "            <a class=\"cancel\" ng-click=\"cancel()\">\n" +
    "              <span class=\"fa fa-times-circle\"></span>\n" +
    "            </a>\n" +
    "\n" +
    "            <md-button ng-show=\"paymentConstruct.type == null || paymentConstruct.type === '1'\" ng-click=\"makePayment(payment)\" class=\"md-primary hint--top\" ng-disabled=\"paymentConstruct.type !== '1'\" data-hint=\"Securely Pay ${{price}} via Stripe Now\" aria-label=\"Securely Pay ${{price}} via Stripe Now\">\n" +
    "\n" +
    "              <img class=\"secure-pay--icon\" style=\"max-width      : 1rem;\n" +
    "                  margin         : .1rem .3rem .3rem 0;\n" +
    "                  -webkit-filter : invert(100%);\n" +
    "                  -moz-filter    : invert(100%);\n" +
    "                  -o-filter      : invert(100%);\n" +
    "                  filter         : invert(100%);\n" +
    "                  line-height    : 0\" src=\"./assets/images/005--verified.png\" alt=\"Secure Pay Now\" longdesc=\"Secure Pay Now\"> Pay Now: ${{price}}\n" +
    "\n" +
    "            </md-button>\n" +
    "\n" +
    "            <!--div class=\"badge-container\">\n" +
    "              <a\n" +
    "                class=\"badge\"\n" +
    "                ng-href=\"https://stripe.com/\"\n" +
    "                rel-external\n" +
    "                aria-label=\"Learn more about Stripe\"\n" +
    "              >\n" +
    "                <img\n" +
    "                  alt=\"Powered by Stripe\"\n" +
    "                  aria-label=\"Powered by Stripe\"\n" +
    "                  src=\"/images/powered-by-stripe-big@2x.png\"\n" +
    "                  longdesc=\"\n" +
    "                  # Stripe\n" +
    "\n" +
    "                  ## Transaction Fees\n" +
    "\n" +
    "                  Stripe takes a simple approach. They charge you a flat rate of 2.9% + 30¢ per \n" +
    "                  successful charge as long as you're doing under $1 million in volume per year. \n" +
    "                  This rate varies country to country, but it's always flat. They don't disclose \n" +
    "                  any special high volume rates.\n" +
    "\n" +
    "                  ## Security\n" +
    "\n" +
    "                  1. Automatically PCI compliant because you don't handle any sensitive credit card  \n" +
    "                  data on your servers.\n" +
    "                  2. More secure because a breach of your servers won't result in any stolen credit  \n" +
    "                  card data.\n" +
    "                  3. You're not tempted to store credit card data on your servers, which you really  \n" +
    "                  shouldn't be doing unless you're a big business and want to pay for PCI  \n" +
    "                  compliance.\"\n" +
    "                />\n" +
    "              </a>\n" +
    "            </div-->\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <footer>\n" +
    "            <nav>\n" +
    "              <ul>\n" +
    "              <li>Our <a ng-click=\"showTermsPage()\">Terms of Service</a></li>\n" +
    "              <li>Stripe's <a ng-click=\"showPrivacyPage()\">Privacy Policy</a></li>\n" +
    "              </ul>\n" +
    "            </nav>\n" +
    "          </footer>\n" +
    "\n" +
    "      </md-content>\n" +
    "\n" +
    "    </form>\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "</md-dialog>"
  );


  $templateCache.put('app/scripts/partials/sorter.html',
    "<form role=\"form\">\n" +
    "    <button class=\"btn dropdown-toggle\">\n" +
    "        <span class=\"icon icon-sort\"></span>\n" +
    "    </button>\n" +
    "    <ul class=\"dropdown-menu\">\n" +
    "        <li data-ng-model=\"sortType\" data-ng-repeat=\"type in sortTypes\">\n" +
    "            <a ng-click=\"sortThis(type)\">{{type.name}}</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</form>"
  );


  $templateCache.put('app/scripts/partials/title.html',
    "<!--\n" +
    "# fileOverview\n" +
    "\n" +
    "           _       _\n" +
    "       _  (_)  _  | |\n" +
    "     _| |_ _ _| |_| | _____ \n" +
    "    (_   _) (_   _) || ___ |\n" +
    "      | |_| | | |_| || ____|\n" +
    "       \\__)_|  \\__)\\_)_____)\n" +
    "\n" +
    "## description\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "  <meta itemprop itemtype itemscope name=\"title\" content>"
  );

}]);
