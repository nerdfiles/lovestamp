angular.module('controller_modules').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/scripts/modules/www/controllers/admin/partials/base.html',
    "<!--*\n" +
    "@fileOverview\n" +
    "\n" +
    " _\n" +
    "| |\n" +
    "| |__  _____  ___ _____\n" +
    "|  _ \\(____ |/___) ___ |\n" +
    "| |_) ) ___ |___ | ____|\n" +
    "|____/\\_____(___/|_____)\n" +
    "\n" +
    "@module www/controllers/admin/partials\n" +
    "@description\n" +
    "\n" +
    "  A basic set of interaction elements for administrators to work with, \n" +
    "  manipulate, and build a stampository.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--admin': true,\n" +
    "    'page--active': true\n" +
    "  }\" ng-init=\"loadStampList()\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1 ng-show=\"isAuthorized\">\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "\n" +
    "    <div class=\"page--unauthorized\" ng-show=\"!isAuthorized\">\n" +
    "      <h1>Unauthorized</h1>\n" +
    "    </div>\n" +
    "  </header>\n" +
    "\n" +
    "  <div itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"admin\" role=\"main\">\n" +
    "\n" +
    "   <div site-navigation></div>\n" +
    "\n" +
    "    <section class=\"command-center\" ng-show=\"isAuthorized\">\n" +
    "\n" +
    "      <header>\n" +
    "        <h1>Stampministry</h1>\n" +
    "      </header>\n" +
    "\n" +
    "      <nav>\n" +
    "        <ul>\n" +
    "\n" +
    "          <li class=\"stamp-figure\">\n" +
    "            <a class=\"hint--bottom\" ui-sref=\"stamp-id\" data-hint=\"Create a unique record for a stamp\">\n" +
    "              <span class=\"fa fa-plus-square\"></span> Id\n" +
    "            </a>\n" +
    "          </li>\n" +
    "\n" +
    "          <li class=\"budget-cron\">\n" +
    "            <a class=\"hint--bottom\" ng-click=\"buyInUsdOrBtc()\" data-hint=\"Buy in BTC or USD\">\n" +
    "              <!--\n" +
    "                 -<span class=\"fa fa-usd\"></span> Collect\n" +
    "                 -->\n" +
    "              <span class=\"fa fa-usd\"></span> Refuel\n" +
    "            </a>\n" +
    "          </li>\n" +
    "\n" +
    "          <li class=\"reset-budget\">\n" +
    "            <a class=\"hint--bottom\" ng-click=\"resetBudgetCounter()\" data-hint=\"Reset LoveStamp's budget counter\">\n" +
    "              <span class=\"fa fa-undo\"></span> Reset\n" +
    "            </a>\n" +
    "          </li>\n" +
    "\n" +
    "\n" +
    "          <li class=\"budget-control\">\n" +
    "\n" +
    "            <form name=\"forms.budgetControl\" class=\"budget-control\">\n" +
    "\n" +
    "              <div class=\"hint--bottom field-block\" data-hint=\"balances/payoutBudget\">\n" +
    "                <md-input-container>\n" +
    "                  <label for=\"previous-total\">\n" +
    "                    <span class=\"fa fa-bank\"></span> Actual\n" +
    "                  </label>\n" +
    "                  <md-input ng-init=\"loadActualTotal()\" readonly=\"readonly\" id=\"actual-total\" name=\"actual-total\" ng-model=\"forms.budgetControl.actualTotal\"></md-input>\n" +
    "                </md-input-container>\n" +
    "              </div>\n" +
    "\n" +
    "              <div class=\"field-block hint--bottom\" data-hint=\"balances/revenue\">\n" +
    "                <md-input-container>\n" +
    "                  <label for=\"revenue-total\">\n" +
    "                    <span class=\"fa fa-shopping-cart\"></span> Revenue\n" +
    "                  </label>\n" +
    "                  <md-input ng-init=\"loadRevenueTotal()\" readonly=\"readonly\" id=\"revenue-total\" name=\"revenue-total\" ng-model=\"forms.budgetControl.revenueTotal\"></md-input>\n" +
    "                </md-input-container>\n" +
    "              </div>\n" +
    "\n" +
    "              <div class=\"field-block hint--bottom\" data-hint=\"Manually set LoveStamp's budget\">\n" +
    "                <md-input-container>\n" +
    "                  <label for=\"set-budget\">\n" +
    "                    <span class=\"fa fa-asterisk\"></span> Redact\n" +
    "                  </label>\n" +
    "                  <md-input ng-init=\"getSetBudget()\" id=\"set-budget\" name=\"set-budget\" ng-model=\"forms.budgetControl.setBudget\"></md-input>\n" +
    "                </md-input-container>\n" +
    "              </div>\n" +
    "\n" +
    "              <div class=\"field-block field-submit\">\n" +
    "                <md-button ng-click=\"adjustBudget()\">\n" +
    "                  <span class=\"fa fa-pencil-square-o\"></span> Set Budget\n" +
    "                </md-button>\n" +
    "              </div>\n" +
    "\n" +
    "            </form>\n" +
    "          </li>\n" +
    "        </ul>\n" +
    "      </nav>\n" +
    "\n" +
    "      <style>\n" +
    "        .ball-pulse > div {\n" +
    "          background-color: #ccc !important;\n" +
    "        }\n" +
    "        .square-spin > div {\n" +
    "          background: #ccc;\n" +
    "          border: 1px solid #ccc;\n" +
    "        }\n" +
    "        .loader-inner {\n" +
    "          text-align: center;\n" +
    "        }\n" +
    "      </style>\n" +
    "      <div ng-show=\"!localStamps\">\n" +
    "        <div class=\"loader-inner ~ball-pulse square-spin\">\n" +
    "          <div></div>\n" +
    "          <!-- .ball-pulse\n" +
    "             -<div></div>\n" +
    "             -<div></div>\n" +
    "             -<div></div>\n" +
    "             -->\n" +
    "        </div>\n" +
    "      </div>\n" +
    "\n" +
    "      <ul name=\"forms.stampository\" deprecated--list-stroll class=\"stampository cards\" infinite-scroll=\"loadMoreStamps()\">\n" +
    "\n" +
    "        <li ng-repeat=\"stamp in localStamps | search:forms.stampository\">\n" +
    "\n" +
    "          <dl>\n" +
    "\n" +
    "            <dt data-hint=\"Merchant Stamp Alias: {{stamp.localStamp.merchantAlias || 'Unnamed'}}\" class=\"id hint--top\">\n" +
    "              <a ng-href=\"/users/{{stamp.user.$id}}\">{{stamp.user.$id}}</a>\n" +
    "            </dt>\n" +
    "\n" +
    "            <dd data-hint=\"SnowShoe Serial: {{stamp.localStamp.receipt.validation.stamp.serial||'Unknown'}}\" class=\"alias hint--top\">\n" +
    "              <span ng-show=\"stamp.localStamp.receipt.validation.stamp.serial\">\n" +
    "                <a ng-href=\"stamps/{{stamp.user.$id}}/{{stamp.localStamp.cleanedAlias}}\" once-text=\"stamp.localStamp.alias\"></a>\n" +
    "              </span>\n" +
    "              <span ng-show=\"!stamp.localStamp.receipt.validation.stamp.serial\">\n" +
    "                <span class=\"fa fa-exclamation-triangle\"></span>\n" +
    "                <br>\n" +
    "                {{stamp.localStamp.alias}}\n" +
    "              </span>\n" +
    "            </dd>\n" +
    "\n" +
    "            <dd data-hint=\"Stamp Signature Score\" class=\"score hint--top\">\n" +
    "              &#x0304;x<span once-text=\"stamp.score\"></span>\n" +
    "            </dd>\n" +
    "\n" +
    "            <dd data-hint=\"Stamp Activation Progress\" class=\"activation-progress hint--top\">\n" +
    "\n" +
    "              <ul class=\"field-slider field-slider--headers\">\n" +
    "                <li>\n" +
    "                  <span class=\"hint--left\" data-hint=\"Paid Invoice\">\n" +
    "                    <span class=\"fa fa-check-circle-o\"></span>\n" +
    "                  </span>\n" +
    "                </li>\n" +
    "\n" +
    "                <li>\n" +
    "                  <span class=\"hint--left\" data-hint=\"Product Shipped\">\n" +
    "                    <span class=\"fa fa-clock-o\"></span>\n" +
    "                  </span>\n" +
    "                </li>\n" +
    "\n" +
    "                <li>\n" +
    "                  <span class=\"hint--left\" data-hint=\"Stamp Activated\">\n" +
    "                    <span class=\"fa fa-play-circle-o\"></span>\n" +
    "                  </span>\n" +
    "                </li>\n" +
    "\n" +
    "              </ul>\n" +
    "\n" +
    "              <div class=\"inner\">\n" +
    "                <md-slider ng-model=\"stamp.activationProgressStatus\" ng-disabled=\"true\" step=\"1\" md-discrete min=\"1\" max=\"3\" aria-label=\"Stamp Activation Progress\" ng-change=\"activationProgressSlider(stamp)\"></md-slider>\n" +
    "              </div>\n" +
    "\n" +
    "            </dd>\n" +
    "\n" +
    "            <dd data-hint=\"Assign this stamp\" class=\"link hint--top\" select-box-click-overlay actionable=\"loadUserList()\">\n" +
    "              <select ng-model=\"handle\" data-placeholder=\"Assign to user\" ng-options=\"user.$id for user in users | filter: { $id: '!' + stamp.user.$id }\" ng-change=\"assignStamp(handle, stamp)\">\n" +
    "                <option value=\"\"></option>\n" +
    "              </select>\n" +
    "            </dd>\n" +
    "          </dl>\n" +
    "\n" +
    "        </li>\n" +
    "\n" +
    "      </ul>\n" +
    "\n" +
    "      <style>\n" +
    "        .select-box-click-overlay {\n" +
    "          height: 30px;\n" +
    "          position: absolute;\n" +
    "          background: orange;\n" +
    "          width: 100%;\n" +
    "          left: 0;\n" +
    "          opacity: 0;\n" +
    "        }\n" +
    "      </style>\n" +
    "\n" +
    "      <div class=\"search-control-stamp by-owner\">\n" +
    "        <form itemprop=\"potentialAction\" itemscope itemtype=\"http://schema.org/SearchAction\">\n" +
    "          <md-input-container>\n" +
    "            <meta itemprop=\"target\" content=\"{queryOwner}\">\n" +
    "            <label for=\"stampository.query-owner\">\n" +
    "              <span class=\"fa fa-child\"></span> Search the stampository by Owner (enter partial name)\n" +
    "            </label>\n" +
    "            <md-input id=\"stampository.query-owner\" name=\"stampository.query-owner\" class=\"stampository--query-owner\" ng-model=\"forms.stampository.queryOwner\" itemprop=\"query-input\"></md-input>\n" +
    "          </md-input-container>\n" +
    "        </form>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"search-control-stamp by-alias\">\n" +
    "        <form itemprop=\"potentialAction\" itemscope itemtype=\"http://schema.org/SearchAction\">\n" +
    "          <md-input-container>\n" +
    "            <meta itemprop=\"target\" content=\"{queryAlias}\">\n" +
    "            <label for=\"stampository.query-alias\">\n" +
    "              <span class=\"fa fa-at\"></span> Search the stampository by Alias (enter full name)\n" +
    "            </label>\n" +
    "            <md-input id=\"stampository.query-alias\" name=\"stampository.query-alias\" class=\"stampository--query-alias\" ng-model=\"forms.stampository.queryAlias\"></md-input>\n" +
    "          </md-input-container>\n" +
    "        </form>\n" +
    "      </div>\n" +
    "\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/dashboard/partials/page.html',
    "<!--\n" +
    "  @module www/controllers/dashboard\n" +
    "-->\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-show=\"loadingStoreOrDashboard\" ng-class=\"{\n" +
    "    'page--dash': true,\n" +
    "    'page--active': loadingSubpage\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "  </header>\n" +
    "\n" +
    "  <div itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"\" role=\"main\" ng-init=\"initializedUserbase()\">\n" +
    "\n" +
    "    <div site-navigation></div>\n" +
    "\n" +
    "    <section ng-show=\"noResults && loading == true\" ng-class=\"{\n" +
    "        'no-results': noResults,\n" +
    "        'no-results-yet': noResults\n" +
    "      }\">\n" +
    "      <p><span class=\"fa fa-th\"></span></p>\n" +
    "    </section>\n" +
    "\n" +
    "    <section class=\"stats--control\">\n" +
    "\n" +
    "      <header>\n" +
    "\n" +
    "        <h1>{{pageSectionTitle}}</h1>\n" +
    "\n" +
    "        <div class=\"intro\">\n" +
    "          <p>\n" +
    "          This is your customer engagement grid. Your top customers occupy the \n" +
    "          bigger area and the blue color intensity increases the more you \n" +
    "          stamp their friends. Click the handle to go to their social profile.\n" +
    "          </p>\n" +
    "        </div>\n" +
    "\n" +
    "        <md-divider></md-divider>\n" +
    "\n" +
    "        <div stamp-impressions-feedback>\n" +
    "          <dl>\n" +
    "\n" +
    "            <dt>\n" +
    "              <span class=\"fa fa-flash\"></span>\n" +
    "              Fuel Remaining\n" +
    "            </dt>\n" +
    "\n" +
    "            <dd ng-show=\"noResults\">Empty</dd>\n" +
    "            <dd ng-show=\"!noResults && expiredStatus\">Empty</dd>\n" +
    "            <dd ng-show=\"!noResults && !expiredStatus\">{{stampAnalytics.quantity}}</dd>\n" +
    "\n" +
    "            <dt>\n" +
    "              <span class=\"fa fa-history\"></span>\n" +
    "              Expiry Date\n" +
    "            </dt>\n" +
    "\n" +
    "            <dd ng-show=\"noResults\">Not started</dd>\n" +
    "            <dd ng-show=\"!noResults && expiredStatus\">Expired</dd>\n" +
    "            <dd ng-show=\"!noResults && !expiredStatus\">{{stampAnalytics.expiry}}</dd>\n" +
    "\n" +
    "          </dl>\n" +
    "        </div>\n" +
    "\n" +
    "        <md-divider></md-divider>\n" +
    "\n" +
    "      </header>\n" +
    "\n" +
    "      <ul ng-if=\"noResults == false\">\n" +
    "\n" +
    "        <li class=\"graph hint--top\" ng-repeat=\"item in graphsLabels.list\" aria-label=\"{{item.label}}\" data-hint=\"Tap to Zoom In/Out\">\n" +
    "          <!--\n" +
    "            @description\n" +
    "            A directive wrapper for C3 static snapshots generated from \n" +
    "            functions in Firebase/Blaze. Should wrap in authorization directive's\n" +
    "            scope.\n" +
    "            @namespace static-stats\n" +
    "          -->\n" +
    "          <figure>\n" +
    "\n" +
    "            <div static-stats=\"stamp--graph__{{item.id}}\" datasource=\"graphs[item.id]\">\n" +
    "\n" +
    "              <div class=\"default--loader animated fade-in\">\n" +
    "                <span class=\"lovestamp--loader\"></span>\n" +
    "              </div>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "            <figcaption aria-hidden=\"true\">\n" +
    "              {{item.label}}\n" +
    "            </figcaption>\n" +
    "\n" +
    "          </figure>\n" +
    "        </li>\n" +
    "\n" +
    "      </ul>\n" +
    "\n" +
    "    </section>\n" +
    "\n" +
    "    <section ng-if=\"noResults == true\" ng-class=\"{\n" +
    "        'no-results': noResults\n" +
    "      }\">\n" +
    "      <p>No graph data to show.</p>\n" +
    "      <p>Stamp a fan and this area will update with some juicy analytics.</p>\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/footer/partials/socket-region.html',
    "<!--\n" +
    "# fileOverview\n" +
    "\n" +
    "                      _                                        _\n" +
    "                     | |            _                         (_)\n" +
    "      ___  ___   ____| |  _ _____ _| |_ _____ ____ _____  ____ _  ___  ____\n" +
    "     /___)/ _ \\ / ___) |_/ ) ___ (_   _|_____) ___) ___ |/ _  | |/ _ \\|  _ \\\n" +
    "    |___ | |_| ( (___|  _ (| ____| | |_     | |   | ____( (_| | | |_| | | | |\n" +
    "    (___/ \\___/ \\____)_| \\_)_____)  \\__)    |_|   |_____)\\___ |_|\\___/|_| |_|\n" +
    "                                                        (_____|\n" +
    "\n" +
    "## description\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section ng-class=\"{\n" +
    "    'site-footer': true\n" +
    "  }\">\n" +
    "\n" +
    "  <!--\n" +
    "\n" +
    "         _                          ___\n" +
    "        | |            _           / __)            _\n" +
    "   ____ | |__  _____ _| |_ _____ _| |__ ___   ___ _| |_ _____  ____\n" +
    "  |  _ \\|  _ \\(____ (_   _|_____|_   __) _ \\ / _ (_   _) ___ |/ ___)\n" +
    "  | |_| | | | / ___ | | |_        | | | |_| | |_| || |_| ____| |\n" +
    "  |  __/|_| |_\\_____|  \\__)       |_|  \\___/ \\___/  \\__)_____)_|\n" +
    "  |_|\n" +
    "\n" +
    "  -->\n" +
    "\n" +
    "  <div class=\"phat-footer\">\n" +
    "\n" +
    "    <div class=\"col col-1\">\n" +
    "\n" +
    "      <figure>\n" +
    "\n" +
    "        <img src=\"/assets/images/logo--merchant-backend.png\" alt=\"LoveStamp\">\n" +
    "\n" +
    "      </figure>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"col col-2\">\n" +
    "\n" +
    "      <!--\n" +
    "\n" +
    "                        _       _\n" +
    "                       (_)     | |\n" +
    "        ___  ___   ____ _ _____| |\n" +
    "       /___)/ _ \\ / ___) (____ | |\n" +
    "      |___ | |_| ( (___| / ___ | |\n" +
    "      (___/ \\___/ \\____)_\\_____|\\_)\n" +
    "\n" +
    "      -->\n" +
    "\n" +
    "      <div class=\"social\">\n" +
    "\n" +
    "        <ul>\n" +
    "          <li>\n" +
    "            <a rel-external href=\"https://www.facebook.com/LoveStampio\">\n" +
    "              <span class=\"fa fa-facebook\"></span>\n" +
    "            </a>\n" +
    "          </li>\n" +
    "\n" +
    "          <li>\n" +
    "            <a rel-external href=\"https://plus.google.com/105216576270473890177\">\n" +
    "              <span class=\"fa fa-google-plus\"></span>\n" +
    "            </a>\n" +
    "          </li>\n" +
    "\n" +
    "          <li>\n" +
    "            <a rel-external href=\"https://twitter.com/LoveStamPR\">\n" +
    "              <span class=\"fa fa-twitter\"></span>\n" +
    "            </a>\n" +
    "          </li>\n" +
    "\n" +
    "<!--\n" +
    "   -          <li>\n" +
    "   -            <a\n" +
    "   -              rel-external\n" +
    "   -              href=\"https://lovestamp.slack.com\"\n" +
    "   -            >\n" +
    "   -              <span class=\"fa fa-slack\"></span>\n" +
    "   -            </a>\n" +
    "   -          </li>\n" +
    "   -\n" +
    "   -          <li>\n" +
    "   -            <a\n" +
    "   -              rel-external\n" +
    "   -              href=\"https://bitbucket.org/nerdfiles/lovestamp\"\n" +
    "   -            >\n" +
    "   -              <span class=\"fa fa-bitbucket\"></span>\n" +
    "   -            </a>\n" +
    "   -          </li>\n" +
    "   -->\n" +
    "        </ul>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "      <!--\n" +
    "\n" +
    "       ____  _____ _   _\n" +
    "      |  _ \\(____ | | | |\n" +
    "      | | | / ___ |\\ V /\n" +
    "      |_| |_\\_____| \\_/\n" +
    "\n" +
    "      -->\n" +
    "\n" +
    "      <nav>\n" +
    "\n" +
    "        <ul>\n" +
    "\n" +
    "          <li class=\"legal\">\n" +
    "            <a ui-sref=\"legal\" data-hint=\"Terms of Use, Privacy Policy, mumbo jumbo, etc.\">Legal Mumbo Jumbo</a>\n" +
    "          </li>\n" +
    "\n" +
    "          <li ng-if=\"isAuthorized\" class=\"admin\">\n" +
    "            <a ui-sref=\"admin\" data-hint=\"Our Command Center\">Administration</a>\n" +
    "          </li>\n" +
    "\n" +
    "\n" +
    "          <li ng-if=\"isAuthorized\" class=\"register\">\n" +
    "            <a data-hint=\"Create a new Stamp Id\" ui-sref=\"stamp-id\">Registration</a>\n" +
    "          </li>\n" +
    "\n" +
    "          <li ng-if=\"isAuthorized\" class=\"id\">\n" +
    "            <a data-hint=\"Create a new Stamp Id\" ui-sref=\"stamp-recall\">Identification</a>\n" +
    "          </li>\n" +
    "\n" +
    "          <li class=\"admin\" ng-show=\"isAuthorized\">\n" +
    "            <a data-hint=\"Track requests for stamps\" ui-sref=\"requests\">Requests</a>\n" +
    "          </li>\n" +
    "\n" +
    "          <li class=\"admin\" ng-show=\"isRegularUser\">\n" +
    "\n" +
    "            <a ng-if=\"!isAuthorized\" data-hint=\"Track payments for stamps and impressions\" ui-sref=\"payments-by-user\">\n" +
    "              Payments\n" +
    "            </a>\n" +
    "\n" +
    "            <a ng-if=\"isAuthorized\" data-hint=\"Track payments for stamps and impressions\" ui-sref=\"payments\">\n" +
    "              Payments\n" +
    "            </a>\n" +
    "\n" +
    "          </li>\n" +
    "\n" +
    "        </ul>\n" +
    "\n" +
    "      </nav>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "  <!--\n" +
    "\n" +
    "              _             _\n" +
    "             | |           | |\n" +
    "    ____ ___ | | ___  ____ | |__   ___  ____\n" +
    "   / ___) _ \\| |/ _ \\|  _ \\|  _ \\ / _ \\|  _ \\\n" +
    "  ( (__| |_| | | |_| | |_| | | | | |_| | | | |\n" +
    "   \\____)___/ \\_)___/|  __/|_| |_|\\___/|_| |_|\n" +
    "                     |_|\n" +
    "\n" +
    "  -->\n" +
    "\n" +
    "  <div class=\"address\" itemscope itemtype=\"http://schema.org/Organization\">\n" +
    "\n" +
    "    <div>\n" +
    "      &copy; 2015 LOVESTAMP. ALL RIGHTS RESERVED.\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"colophon\">\n" +
    "      LoveStamp, a product of <span itemprop=\"name\">Monarchy LLC</span>, is \n" +
    "      independently owned and operated and is not affiliated with \n" +
    "      ChangeTip or SnowShoe Stamp.\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"accessibly-hide\" itemprop=\"address\" itemscope itemtype=\"http://schema.org/PostalAddress\">\n" +
    "\n" +
    "      <div>\n" +
    "        <div itemprop=\"streetAddress\">Level 2, 2800 San Jacinto Street</div>\n" +
    "        <span itemprop=\"addressLocality\">Houston</span><span class=\"sep\">,</span>\n" +
    "        <span itemprop=\"addressRegion\">Texas</span>\n" +
    "        <span itemprop=\"postalCode\">77004</span>\n" +
    "        <span>E-mail:</span>\n" +
    "        <span itemprop=\"email\">support@lovestamp.io</span>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"organization--members\">\n" +
    "          <h2>Members</h2>\n" +
    "\n" +
    "          <div itemprop=\"member\" itemscope itemtype=\"http://schema.org/Person\">\n" +
    "            <h3>James Duchenne</h3>\n" +
    "            <h4 itemprop=\"jobTitle\">Management Lead</h4>\n" +
    "            <ul>\n" +
    "              <li><a href=\"https://twitter.com/jamesduchenne\">@jamesduchenne</a></li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "\n" +
    "          <div itemprop=\"member\" itemscope itemtype=\"http://schema.org/Person\">\n" +
    "            <h3>Adam Richard</h3>\n" +
    "            <h4 itemprop=\"jobTitle\">UX Lead</h4>\n" +
    "            <ul>\n" +
    "              <li><a href=\"https://twitter.com/adamwrichard\">@adamwrichard</a></li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "\n" +
    "          <div itemprop=\"member\" itemscope itemtype=\"http://schema.org/Person\">\n" +
    "            <h3>Kimberly Morgan</h3>\n" +
    "            <h4 itemprop=\"jobTitle\">PR Lead</h4>\n" +
    "            <ul>\n" +
    "              <li><a href=\"https://twitter.com/kymmer7691\">@kymmer7691</a></li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "\n" +
    "          <div itemprop=\"member\" itemscope itemtype=\"http://schema.org/Person\">\n" +
    "            <h3>Aha Hah</h3>\n" +
    "            <h4 itemprop=\"jobTitle\">Tech Lead</h4>\n" +
    "            <ul>\n" +
    "              <li><a href=\"https://twitter.com/filesofnerds\">@filesofnerds</a></li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/header/partials/nav.html',
    "<!--\n" +
    "# fileOverview\n" +
    "\n" +
    " ____  _____ _   _\n" +
    "|  _ \\(____ | | | |\n" +
    "| | | / ___ |\\ V /\n" +
    "|_| |_\\_____| \\_/\n" +
    "\n" +
    "## description\n" +
    "\n" +
    "  \n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<nav class=\"dash-nav\">\n" +
    "\n" +
    "  <ul class=\"nav mobile-menu--active\" mobile-menu-container>\n" +
    "\n" +
    "    <li mobile-menu-item mobile-menu-item-node=\"item\" ng-repeat=\"item in navigationLabels.list\" once-show=\"item.isAuthorized\">\n" +
    "\n" +
    "      <a aria-label=\"{{item.hint}}\" ng-href=\"{{item.uri}}\" data-hint=\"{{item.hint}}\" ng-attr-open-window=\"item.uri.indexOf('fan-view') !== -1\" open-window-uri=\"{{item.uri}}\" open-window-href=\"{{item.label}}\" open-window-site-url=\"{{siteUrl}}\" ng-class=\"{\n" +
    "          'no-hint'      : item.uri.indexOf('fan-view') !== -1 || item.uri.indexOf('logout') !== -1,\n" +
    "          'hint--pull-4' : true\n" +
    "        }\">\n" +
    "\n" +
    "        <span aria-hidden=\"true\" class=\"fa {{item.icon}}\"></span>\n" +
    "\n" +
    "        <span class=\"nav-label\" once-text=\"item.label\"></span>\n" +
    "\n" +
    "      </a>\n" +
    "    </li>\n" +
    "\n" +
    "  </ul>\n" +
    "\n" +
    "</nav>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/legal/partials/page.html',
    "<!--\n" +
    "@fileOverview\n" +
    "\n" +
    " ____  _____  ____ _____ \n" +
    "|  _ \\(____ |/ _  | ___ |\n" +
    "| |_| / ___ ( (_| | ____|\n" +
    "|  __/\\_____|\\___ |_____)\n" +
    "|_|         (_____|\n" +
    "\n" +
    "@module www/controllers/legal\n" +
    "@description\n" +
    "\n" +
    "  \n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"https://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--active' : true,\n" +
    "    'page--legal'  : true\n" +
    "  }\">\n" +
    "\n" +
    "  <!--\n" +
    "     -<div \n" +
    "     -  user-rollup\n" +
    "     -  auth-obj=\"authObj\"\n" +
    "     -  auth-data=\"authData\"\n" +
    "     -></div>\n" +
    "     -->\n" +
    "\n" +
    "  <!--\n" +
    "     -<header \n" +
    "     -  itemscope\n" +
    "     -  itemtype=\"https://schema.org/WebPageElement\" \n" +
    "     -  class=\"site-header\"\n" +
    "     ->\n" +
    "     -  <h1>\n" +
    "     -    <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "     -  </h1>\n" +
    "     -</header>\n" +
    "     -->\n" +
    "\n" +
    "  <div itemscope itemtype=\"https://schema.org/WebPageElement\" class=\"\" role=\"main\">\n" +
    "\n" +
    "    <!--<div site-navigation></div>-->\n" +
    "\n" +
    "    <style>\n" +
    "    p.p4 {\n" +
    "      text-align: center;\n" +
    "      font-size: 1.735rem;\n" +
    "    }\n" +
    "    </style>\n" +
    "\n" +
    "    <p style=\"text-align: center; margin: 10vh 0 0 0\">Return to <a ui-sref=\"login-auth\">LoveStamp Login</a></p>\n" +
    "\n" +
    "    <section class=\"legal-mumbo-jumbo\">\n" +
    "      <header><h1 class=\"s1\">LEGAL MUMBO JUMBO</h1></header>\n" +
    "      <p class=\"p2\">Lovestamp is a product of Monarchy LLC (‘Monarchy’, ‘We’ or ‘Us’ as the case may be). Monarchy operates and manages the LoveStamp-branded URL sites and applications including, but not limited to, any other features, content, software, codes, intellectual property or services in connection with the LoveStamp product under the terms and conditions set out herein (“Our Services”). This Agreement is between you (‘You’) and Us.</p>\n" +
    "      <br>\n" +
    "      <p class=\"p4\">RULES</p>\n" +
    "      <br>\n" +
    "      <p class=\"p5\"><b>General</b></p>\n" +
    "      <p class=\"p2\">Participation to LoveStamp is subject to Our Terms Of Use and Privacy Policy. We allow merchants to join the LoveStamp engagement platform by appending their businesses to Our Services. This allows them to perform check-in functions for individuals that are members of the LoveStamp ecosystem (together “Users”).</p>\n" +
    "      <p class=\"p7\"><b>Merchants</b></p>\n" +
    "      <p class=\"p2\">All legitimate businesses, charities, schools, non-profit organizations and associations (together, “Merchants”). You must sign up using the relevant social media account. To obtain a stamp, you must first be vetted by Us. We may verify the legitimacy of your application. We may invite You to join Our Services and get a stamp whether or not an invitation request has been submitted through Our Websites.</p>\n" +
    "      <p class=\"p2\">You agree that You may not utilize a username that portrays You, in a misleading manner, as belonging to another recognized business, individual or organization whether or not that name or username is a recognized trademark of that business, individual or organization or a where a third party has superior rights to You (through legal registration or otherwise) to utilize that name or username. In this event, We may suspend your account (or where this constitutes a serious offence, terminate your account) to Our Services, unless reliable evidence disputing that fact is obtained. In this event, You agree not to hold us liable for taking such an action against You.</p>\n" +
    "      <p class=\"p2\">All Merchant representatives of an Organization, represent that at the time of registration they have authority to enter into an agreement with Us to use Our Services on behalf of that Merchant.</p>\n" +
    "      <p class=\"p2\">Upon registration, We will provide You with access to Our Website solely for the purpose of enabling You to promote, recognize and check in your fans, supporters or customers (“Individual Users”).</p>\n" +
    "      <p class=\"p2\">By using Our Services, You warrant that:</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Your use of Our Services, performance of your obligations do not and will not violate any other agreement or legal obligation by which You are bound to; and</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You have the necessary authority, licenses, rights, consents and permissions to run any activity You offer to our Individual Users.</p>\n" +
    "      <p class=\"p2\">You acknowledge and agree that:</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We have no obligation to You or our Users in respect of the fulfillment of any specific type of rewards (for example, instead of rewards in bits, this could change to another rewards unit of account) or for any activity You offer or promises You make to them through Our Services, including but not limited to the sale of products or services or the collection of any monies payable to You;</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You are responsible for dealing with Users that interact with You regarding any queries, issues, complaints and the like for activities that You offer through Our Platform. Where We believe, acting reasonably, that You are not resolving a dispute in accordance with this Clause then We may, by notice to You either suspend your Account until such time as the dispute is resolved;</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>It is a breach of Our Terms of Use and Our Rules for You not to fulfill any offer or promises associated with an activity you advertise on Our Services; and</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You will take all necessary steps as may be reasonably required for the purposes of meeting the offer for your activity, resolving any disputes in good faith and in a timely manner.</p>\n" +
    "      <p class=\"p7\"><b>Individual Users</b></p>\n" +
    "      <p class=\"p2\">Participation to use Our Services is subject to Our Terms of Use and Privacy Policy. Our Services are free to You, subject to the following. You must:</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>be over the age of 13 years old;</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>have parental (or legal guardian) consent, if under the age of 18 years old;</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>not have had a LoveStamp account suspended or cancelled previously; or</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>not be precluded from joining Our Services under the laws of the relevant jurisdiction that applies to you.</p>\n" +
    "      <p class=\"p2\">You must sign up using the relevant social account. By using Our Services, You represent and warrant that You satisfy all of the requirements listed above.</p>\n" +
    "      <p class=\"p2\">If We believe that You incorrectly represented yourself as 13 years of age or older, your account may be suspended without warning. We may then either reinstate or terminate your account at our sole discretion. Any benefits associated with all accumulated reward tokens will be forfeited in this case.</p>\n" +
    "      <p class=\"p2\">We administer Our Services in order of priority, first to provide a stable system capable of being used by the majority of our Users then to seamlessly provide for functionalities that we deem will enhance the interactions we aim to promote. However, to the full extent permitted by the law, We accept no responsibility for the proper functioning of Our Services, especially during its ‘Beta’ or ‘Testing’ Version.</p>\n" +
    "      <p class=\"p2\">We may issue notices concerning any disruptions or delays in performing website updates or upgrades. Such disruption or delays shall not give rise to any legal actions by You or liabilities to Us.</p>\n" +
    "      <p class=\"p5\"><b>Posting of Content</b></p>\n" +
    "      <p class=\"p7\"><b>Merchants</b></p>\n" +
    "      <p class=\"p2\">You may post content (whether written or in video format, whether in the form or offers or not) on Our Services. Content You post:</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>must be your property or You must hold suitable use rights for same. You must not post material that is subject to copyright or proprietary rights of a third party without express consent from that third party. You warrant to Us at all times that you have the necessary licenses, rights, consents and permissions for the content You post on Our Services;</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>must not contain material that Infringes upon any law or regulation, or is likely to infringe the rights of any third party or is offensive, indecent, or otherwise inappropriate, regardless of whether such material or its dissemination is lawful. This includes (but is not limited to), obscene material and fraudulent or misleading or deceptive statements; and</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>must be in a format and posted in a manner that is allowed (as directed by the functionalities available offered via Our Services for that purpose).</p>\n" +
    "      <p class=\"p2\">You retain ownership, sole responsibility and accept sole liability for the content You post using Our Services.</p>\n" +
    "      <p class=\"p2\">By submitting your content for posting on Our website or applications, You grant Us:</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>a worldwide, non-exclusive, royalty-free, sub-licensable and transferable license to use, reproduce, distribute, display, publish, make available online or electronically transmit made in connection with Our Services, including without limitation for Us to promote part or all of Our Services in any media formats and through any media channels; and</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>a right to grant each User a non-exclusive license to access, view and share your content published on Our websites or applications.</p>\n" +
    "      <p class=\"p2\">We may at any time, without liability or notice to You, remove, alter or disable access to all or any of your Content published on Our website if:</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You breach Our Terms of Use or Privacy Policy;</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We are required to do so by any law, regulation or regulatory body; or</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We are required to do so to ensure compliance with a notification from a copyright owner or their agent.</p>\n" +
    "      <p class=\"p2\">Where We provide You our logo or other marketing material, You must not use this in conjunction with illegal, offensive, racist acts OR activities that may bring us into disrepute OR the provision of false and misleading information regarding your participation.</p>\n" +
    "      <p class=\"p5\"><b>Reward Tokens</b></p>\n" +
    "      <p class=\"p7\"><b>Merchants</b></p>\n" +
    "      <p class=\"p9\"><i>i. Rewards</i></p>\n" +
    "      <p class=\"p2\">You acknowledge that you do not reward Users with bits, our rewards token, but rather check them in for your particular activity. We, at our sole discretion, determine which Users to reward, the quantum and nature of those rewards following a notification from You that a particular User has been checked in.</p>\n" +
    "      <p class=\"p9\"><i>ii. Stamps</i></p>\n" +
    "      <p class=\"p2\">To use Our Services, you must first obtain a Stamp. This can only be obtained once you have been vetted, with the following details, which includes but is not limited to:</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Merchant name, address, telephone number and other appropriate contact details;</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>A contact person authorized by the Merchant to deal with us; and</p>\n" +
    "      <p class=\"p8\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Merchant good standing nature and appropriate business activity checks.</p>\n" +
    "      <p class=\"p2\">Stamps provide a unique profile that can identify a particular Merchant. It may, on rare occasions, however, produce data collisions where a Merchant can’t be identified. In this instance, you should reapply the stamp first THEN contact us if the problem persists. Note that while LoveStamp is in alpha 0r beta, we accept no responsibility or liability for a stamp malfunctioning, whether or not due to user error.</p>\n" +
    "      <p class=\"p2\">As each stamp is unique, once purchased the item is non-returnable unless there is a defect with the stamp that doesn’t allow You to use all or part of Our Services as provided. In this event, you may choose a replacement stamp or a refund.</p>\n" +
    "      <p class=\"p9\"><i>iii. Fuel</i></p>\n" +
    "      <p class=\"p2\">Once a stamp is obtained, You must purchase a plan, called “Fuel”. Fuel allows you to a maximum of 300 Stamp Impressions with an expiry date of 30 days. You can buy Fuel in allotments, with 1 allotment giving you 300 Stamp Impressions and an expiry date of 30 days, 2 allotments giving you 600 stamp impressions and an expiry date of 60 days, so on and so forth.</p>\n" +
    "      <p class=\"p2\">Your fuel plan starts on the date of your purchase (of fuel). Where you already have an expiry date from a prior purchase and the newly calculated expiry date is less than Your current expiry date, Your current expiry date will apply. Alternatively, Your current expiry date will be extended to a new expiry date relevant to Your purchase. For example, if Your current expiry date is 1 May 2015, and you made a purchase of 300 Stamp Impressions on 1 March 2015 (hence, giving you an expiry date of 30 March 2015), your expiry date will still be 1 May 2015. If, however, you made a purchase of 300 Stamp Impressions on 15 April 2015, your expiry date will be 15 May 2015 (15 April 2015 + 30 days).</p>\n" +
    "      <p class=\"p2\">The role of Fuel allows You to be advertised on LoveStamp as you check-in Individual Users. It also allows you to track your engagement activity through our metrics as provided via Our websites or applications. Without Fuel, you will not be able to participate on LoveStamp. We, therefore, ask you to consider the cost of Fuel prior to purchasing a stamp.</p>\n" +
    "      <p class=\"p2\">We reserve the right at any time and for any reason to modify, limit, suspend or discontinue Our websites or applications, in whole or in part, with or without notice, for the purpose of undertaking any maintenance, upgrading or similar processes in respect of the Website. As a result of an extended discontinuance of Our Services caused by Us, at your request We will refund the fuel fee equal to the pro rata of unused Stamp Impressions that You paid for according to the following:</p>\n" +
    "      <p class=\"p8\"><i><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Total Unused Stamp Impressions/Total Stamp Impressions Purchased x Fuel Cost Paid.</i></p>\n" +
    "      <p class=\"p7\"><b>Individual Users</b></p>\n" +
    "      <p class=\"p2\">You can collect bits, our reward tokens, from Us by being checked in at a Merchant’s location, provided that you’re at least within 1,000 feet from the nearest Merchant to you. LoveStamp will not work if you don’t allow us to locate your position. A Merchant has the absolute discretion in whom they choose to check-in. You acknowledge and agree that the reward tokens are not provided by the Merchant, but by Us once we receive check-in confirmation from a Merchant. As such, the Merchant has no control over whether a reward token is provided, the quantum and nature of that reward. For avoidance of doubt, You have no recourse to complain or cause a review to be made in relation to reward tokens.</p>\n" +
    "      <p class=\"p5\"><b>Miscellaneous Rules</b></p>\n" +
    "      <p class=\"p12\">Merchants</p>\n" +
    "      <p class=\"p13\">You agree that You cannot offer activities deemed to be a game of chance through Our Services without the proper licenses or approvals in force in the country the activity is held. We will not be responsible or liable to any third party for your improper use of Our Services in relation to games of chance. Any breach of this clause may give rise to suspension or termination of your account.</p>\n" +
    "      <p class=\"p13\">You agree not to provide for check-in activities to be advertised for activities that are illegal in nature in your jurisdiction, or that may cause an Individual User injury, death, addiction, or otherwise losses, whether or not of a monetary nature.</p>\n" +
    "      <p class=\"p12\">Users, Generally</p>\n" +
    "      <p class=\"p13\">The process of creating fake accounts and farming points to obtain benefits, whether or not through attempting to cause rewards to be obtained to legitimate accounts is expressly prohibited. Our system is designed to address suspicious transactions of that nature and We may be required to use your personal information to contact You and request that You provide proper identification, information for verification and the nature of the transactions. Failure to provide the relevant information to conduct our assessment may result in immediate termination of Your account or all accounts associated with the activity.</p>\n" +
    "      <p class=\"p13\">We will continue to provide You with access to Our Services up until such time as You terminate your account or We terminate your account for breach of Our Terms Of Use, Privacy Policy or Our Rules. Note that We reserve the right to remove your account where your account has not been used for a period of more than 6 months or where your business is subject to insolvency, liquidation or winding up (‘Abandonment of Account’). For cases of breach, Abandonment of Account or where You delete your account, You agree that you will not hold us liable to refund any unused Stamp Impressions and the refund clauses do not apply to You.</p>\n" +
    "      <p class=\"p13\">We may amend Our Rules from time to time.</p>\n" +
    "      <br>\n" +
    "      <p class=\"p4\">TERMS OF USE</p>\n" +
    "      <br>\n" +
    "      <p class=\"p5\"><b>Acceptance of Terms of Use</b></p>\n" +
    "      <p class=\"p14\">By using Our Services You agree to and accept our Terms of Use (thereafter “TOU”). You represent that You have the right and authority to use Our Services, and THAT you will use Our Services for purposes that are permitted by Our TOU, Our Privacy Policy and any rules set out hereunder, and THAT the performance of Your obligations under Our TOU doesn’t infringe the rights or any third party, and THAT You agree to abide by all applicable laws, including any laws regarding the export of data or software to and from the USA and Australia or other relevant country, whether in your jurisdiction or the jurisdictions where Our Services are provided.</p>\n" +
    "      <p class=\"p5\"><b>Variation of TOU</b></p>\n" +
    "      <p class=\"p14\">We reserve the right to vary Our TOU at any time. Each such variation will be effective immediately upon posting on our website, or via our public social media accounts as the case may be. Your continued use of Our Services following any variation constitutes your acceptance and agreement to be bound by Our TOU, as varied. It is therefore important, and we recommend, that You review Our TOU regularly.</p>\n" +
    "      <p class=\"p5\"><b>Form And Nature of Services Provided</b></p>\n" +
    "      <p class=\"p14\">By using Our Services, You acknowledge and agree that, the form and nature of Our Services may change from time to time without prior notice to You; we may, either permanently or temporarily, stop providing Our Services or any features associated with it to You at Our sole discretion, without prior notice; if we disable access to your account, You may be prevented from accessing Our Services, your account details, or any files or other content which is contained in Your account; and, while we may not currently have a limit on the extent to which You use Our Services or amount of data storage space You use, such limits may be varied at Our discretion without prior notice to You.</p>\n" +
    "      <p class=\"p5\"><b>Registration</b></p>\n" +
    "      <p class=\"p17\">You must be 13 years or older to participate.</p>\n" +
    "      <p class=\"p17\">You may become a registered User of Our Services by signing up up on the relevant area of our websites or applications. In order to register and access and use Our Services, You may be required to provide information about You (such as identification, contact details and account details for payments). By registering and using Our Platform, You represent and warrant that all registration information You submit is truthful and accurate, and THAT You will maintain the accuracy of such information at all times whilst You continue to be registered for, and use, Our Services.</p>\n" +
    "      <p class=\"p17\">Your registration may be cancelled if We, at our sole discretion, determine that You hold multiple accounts, have provided identification information that is false in nature or intended to mislead, or have acted in a way that is intended to or actually damages the reliability of Our Services to Our Users; and or, THAT You are in breach of Our TOU or Our TOU otherwise give Us the right to cancel Your registration.</p>\n" +
    "      <p class=\"p17\">You may not register for Our Services without our prior permission where Your registration was previously cancelled as a result of You being in breach of Our TOU (as they applied at the relevant time).</p>\n" +
    "      <p class=\"p5\"><b>Use of Our Services, Generally</b></p>\n" +
    "      <p class=\"p18\">By using Our Services, You agree:</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Not to post any Prohibited Content on Our websites or applications;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Not to bring Our Services into disrepute;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Not to engage in any prohibited activities as set out in Our TOU;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Not to access (or attempt to access) any of Our Services through any automated means (including use of scripts or web crawlers), unless You are specifically allowed to do so under the terms of a seperate agreement with Us;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>To comply with the instructions set out on our Websites or applications, or directions received from Us;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>That you are solely responsible for (and that We have no responsibility to You or any third party) for: any breach of your obligations or any representation or warranty You provide under Our TOU and for the consequences of any such breach (including any loss or damage howsoever caused which We or any third party may suffer); and, Your interactions with other users of Our Services (‘Our Users’).</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>That We give You a personal, worldwide, royalty-fee, non-assignable and non-exclusive license to use Our Services. This license is for the sole purpose of promoting your interactions with Us and Our Users and such other purposes permitted by Our TOU;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Not to assign (or grant a sub-license of) your rights to use Our Services, grant a security interest in or over your rights to use Our Services, or otherwise transfer any part of your rights to use Our Services, unless You are specifically allowed to do so under the terms of a separate agreement with Us;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Not to reproduce, duplicate, copy, sell, trade or resell any part of Our Websites, Our applications or Our Services for any purpose, unless You are specifically allowed to do so under the terms of a separate agreement with Us;Not (and will not permit anyone else) to copy, modify, adapt, create a derivative work of, reverse engineer, decompile or otherwise attempt to extract the source code of Our Services or any part thereof, unless You are specifically allowed to do so under the terms of a separate agreement with Us;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>That Our Services may provide a means to automatically download and install updates from time to time. These updates are designed to improve, enhance and further develop Our Services and may take the form of bug fixes, enhanced functions, new software modules and completely new versions. You agree to receive such updates (and permit Us to deliver these to You), as part of your use of Our Services;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>That We reserve the right, but have no obligation, to intervene as an arbitrator in any way with disputes between You and Our Users.</p>\n" +
    "      <p class=\"p5\"><b>Legal Responsibilities</b></p>\n" +
    "      <p class=\"p17\">You are solely responsible to understand and abide by any federal, state, local or foreign laws, including tax laws, that may apply. You acknowledge that We may be required to provide information, in compliance with applicable laws, to the relevant Tax Office or governmental agency that requests such information concerning You or its citizens.</p>\n" +
    "      <p class=\"p17\">By registering for Our Services, You, represent and warrant that your earning of any Reward Tokens or “bits” on or through Our Services, is either made in the course or furtherance of an activity done as a private recreational pursuit or hobby, or is wholly of a private or domestic nature, OR where You are registered as a merchant, made in the course of conducting a legitimate business or activity according to the laws in force in Your jurisdiction. Under no circumstances, should any consideration provided to merchants for which a reward is obtained via Our Services be solely made to obtain same (that is, the consideration provided must not be in form of a wager).</p>\n" +
    "      <p class=\"p5\"><b>Password And Account Security</b></p>\n" +
    "      <p class=\"p17\">You acknowledge and agree that You are entirely responsible for maintaining the confidentiality of passwords associated with your account, the unique identifying stamp that We provide, including passwords to any third party web service, to access Our Platform. You agree not to use the account, username, email address or password of any of Our Users at any time. Further, you are solely responsible for the use under your account, whether or not those activities are authorized by You.</p>\n" +
    "      <p class=\"p17\">If You become aware of any unauthorized use of your password or account, You must notify Us immediately at <span class=\"s3\">account@lovestamp.io</span>.</p>\n" +
    "      <p class=\"p5\"><b>Stamping Activity</b></p>\n" +
    "      <p class=\"p17\">You acknowledge and agree that merchants using Our Services append their businesses to Our Services for the sole purpose of being advertised on our websites or applications, or on the relevant third party applications, AND to use the stamp provided to check-in Users by making stamp impressions in the manner We indicate. At no point do merchants have the ability to control the quantum of reward tokens associated with their check-in activity AND the amount of the reward tokens given is solely at Our discretion. However, merchants have the absolute discretion with respect to which Users they stamp and are not obligated to stamp all Users that present their mobile devices to be stamped.</p>\n" +
    "      <p class=\"p17\">Where Our Services include hyperlinks to a merchant’s website, i.e. a third party site that we do not control, You acknowledge and agree that we assume no responsibility for the accessibility to that site NOR the content, rules, or practices set out on that website. Accordingly, we encourage You to be aware that when You leave Our Websites and to read the terms and conditions and policies of any third party website that You visit as a result of using Our Services. For avoidance of doubt, We are not responsible for any loss or damage which may be incurred by You as a result of any reliance on the completeness, accuracy or existence of the Content published on any third party sites and the administrator of those sites are solely responsible for the terms and conditions, which govern the sale of any product or activity.</p>\n" +
    "      <p class=\"p5\"><b>Content and Proprietary Rights</b></p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Content presented to or otherwise available for use by You as part of Our Services may be protected by intellectual property rights which We own. You may only use that content for the purposes and in the manner permitted by Our TOU and may not modify, adapt, rent, lease, loan, sell, distribute or create derivative works based on that Content (either in whole or in part), unless You are specifically allowed to do so under the terms of a separate agreement with Us.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>All third party content, including merchant information, which You may have access to as part of, or through your use of, Our Services is the sole responsibility of the person from which such content originated and We do not represent or warrant and are not responsible for ensuring the completeness and accuracy of that third party content.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>By using Our Services You may be exposed to content that You may find offensive, indecent or objectionable and that, in this respect, your use of Our Services is at your own risk.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You are solely responsible for (and acknowledge that to the extent permitted by law, We have no responsibility to You or to any third party for), any content that You create, transmit or display while using Our Services and for the consequences of your actions (including any loss or damage which We or any third party may suffer) by doing so.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We assume no responsibility for monitoring Our Services for any prohibited content or Prohibited Activity. If at any time We choose, in our sole discretion, to monitor Our Services, We nonetheless assume no responsibility for that content, no obligation to modify or remove any inappropriate content, and no responsibility for the conduct of any of Our Users submitting any such Content.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You acknowledge and agree that We own all legal right, title and interest in Our Services, including any intellectual property rights (which includes Our patent entitled An Automated Rewards System For Different Audiences Utilizing Self-Regulating Algorithm and Game Mechanics) which subsist in, and derives from Our Services (whether those rights happen to be registered or not, and wherever in the world those rights may exist). You further acknowledge that Our Services may contain information, which is confidential.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Unless You have agreed otherwise in writing with Us, nothing in Our TOU gives You a right to use any of Our trade names, trademarks, service marks, logos, domain names, and other distinctive brand features unless they are incorporated in the content and used without modification or adaptation as part of that content.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You agree that in using Our Services, You will not use any trademark, service mark, trade name or logo of any third party in a way that is likely or intended to cause confusion unless You have been expressly authorized to do so in writing.</p>\n" +
    "      <p class=\"p5\"><b>Prohibited Content and Prohibited Activities</b></p>\n" +
    "      <p class=\"p18\">The following are examples of the kind of Content that You may not post on Our websites or applications (‘Prohibited Content’). We reserve the right to investigate and take appropriate legal action against You if You breach this provision, including without limitation, removing the offending content from Our Platform, cancelling your registration as a User and terminating your access to Our Services. Prohibited Content includes, but is not limited to, content that:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>constitutes or promotes information or other material that is false or misleading or promotes illegal activities or conduct that is abusive, threatening, obscene, defamatory or libelous;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>constitutes or promotes an illegal or unauthorized copy of another person’s copyrighted work including, but not limited to, providing pirated computer programs or links to them, providing information to circumvent manufacturer-installed copy-protect devices, or providing pirated music or links to pirated music files;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>is patently offensive or promotes racism, bigotry, hatred or physical harm of any kind against any group or individual;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>harasses or advocates harassment of another person;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>exploits people in a sexual or violent manner;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>contains nudity, sexually suggestive imagery, excessive violence, or offensive subject matter or contains a link to an adult website;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>furthers or promotes any criminal activity or enterprise or provides instructional information about illegal activities including, but not limited to making or buying illegal weapons, violating someone’s privacy, or providing or creating computer viruses;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>contains restricted or password only access pages or hidden pages or images;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>solicits passwords or personal identifying information for commercial or unlawful purposes from Our Users;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>includes a photograph or video of another person that You have posted without that person’s consent;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>solicits personal information from anyone under the age of 18;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>publicly posts information that poses or creates a privacy or security risk to any person;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>violates the privacy rights, publicity rights, copyrights, trademark rights, contract rights or any other rights of any person;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>promotes propaganda or symbols of organizations which are illegal in your country or in any country in which Our Platform are used.</p>\n" +
    "      <p class=\"p18\">The following are examples of the kind of activity that is prohibited on Our websites or applications and through your use of Our Services. We reserve the right to investigate and take appropriate legal action against You if You breach this provision, including without limitation, reporting You to law enforcement authorities, cancelling your registration and terminating your access to Our Services. Prohibited activities include, but is not limited to:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>other than when displaying offers, advertising to, or soliciting any of Our Users to buy or sell any products or services through Our Services;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>sending any ‘chain letters’, unsolicited bulk email or ‘spam’, or any other unauthorized commercial communications of any kind through, or in relation to, Our Services;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>circumventing or modifying, attempting to circumvent or modify, or encouraging or assisting any other person in circumventing or modifying any security technology or code that is part of Our Services;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>activity that involves the use of viruses, bots, worms, or any other computer code, files or programs that interrupt, destroy or limit the functionality of any computer software or hardware, or otherwise permit the unauthorized use of or access to a computer or a computer network;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>covering or obscuring the banner advertisements on any of Our websites or applications or in relation to any of Our Platform used in any place in any media via HTML/CSS or any other means;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>any automated use of the system, including but not limited to, using scripts to send comments or messages;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>interfering with, disrupting, or creating an undue burden on Our Services or the networks or servers connected to Our Services;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>impersonating or attempting to impersonate or encouraging or assisting any other person to impersonate another user, person or entity;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>using the account, username, or password of another of Our User at any time;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>uploading, embedding, posting, emailing, transmitting or otherwise making available any material that infringes any copyright, patent, trademark, trade secret or other proprietary rights of any person or entity;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>selling or otherwise transferring the whole or any part of your profile;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>using or attempting to use or encouraging or assisting any other person in using any information obtained from Our Services in order to harass, abuse, or harm another person or entity;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>displaying an unauthorized commercial advertisement on or associated with the content on Our websites or applications in any place, or accepting payment or anything of value from a third person in exchange for your performing any commercial activity through the unauthorized use of Our Services on behalf of that person; or</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>using Our Services in a manner inconsistent with or otherwise in breach of any Applicable Laws.</p>\n" +
    "      <p class=\"p18\">The following provisions will apply where We, at our sole discretion, determine that You have engaged in any of the prohibited activities contained in Our TOU that may constitute a breach of applicable unsolicited or bulk communication laws:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>on the first occasion that We determine that You have engaged in any such activity, You may be issued with a warning depending on the severity of the activity; and</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>on the second occasion that We determine that You have engaged in any such Prohibited Activity, We will have the right to suspend your account for a period of 7 days or terminate your access to Our Services.</p>\n" +
    "      <p class=\"p18\">If You become aware of any of Our Users posting any Prohibited Content or engaging in any activities that have been prohibited, You may report the incident to Us by contacting us directly at <span class=\"s3\">support@lovestamp.io</span>.</p>\n" +
    "      <p class=\"p5\"><b>Limitation of Liability And Indemnity</b></p>\n" +
    "      <p class=\"p18\">We, Our subsidiaries, and Our directors, officers, employees, contractors, agents and licensors will not be liable to You:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>In tort, contract or otherwise, howsoever caused, even if such loss or damage was reasonably foreseeable, for any:</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>loss of profits, opportunity, revenue, data, goodwill, business or anticipated savings, pure economic loss or expectation loss; or</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>any indirect, consequential, special, punitive or exemplary loss or damage; or</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>any loss or damage which may be incurred by You, including but not limited to loss or damage arising out of:</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>any reliance placed by You upon the completeness, accuracy or existence of any Content, or as a result of any relationship or transaction between ourselves or any of Our Users;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>any changes that We make to Our Services;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Where we cease, either permanently or temporarily, to provide Our Services or any features within Our Services;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>the deletion or corruption of, or failure to store any content or other data maintained or transmitted by or through your use of Our Services;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Your failure to provide Us with accurate account information; or</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Your failure to keep your password or account details secure and confidential.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You will indemnify and hold Us, Our subsidiaries and Our directors, officers, employees, contractors, agents and licensors harmless from any loss, damage, proceeding and cost (including all reasonable legal costs), arising out of claim by a third party as a result of your breach of Our TOU or your violation of any Applicable Law or the rights of any third party.</p>\n" +
    "      <p class=\"p18\">This Clause shall survive the expiry or termination of Our TOU and or its applicability to You.</p>\n" +
    "      <p class=\"p18\">Nothing in Our TOU will exclude or limit Our warranties or liability for losses that may not be lawfully excluded or limited by Applicable Law. Some jurisdictions do not allow the exclusion of certain warranties or conditions or the limitation or exclusion of liability for loss or damage caused by negligence, breach of contract or breach of implied terms, or incidental or consequential damages. Accordingly, only the limitations, which are lawful in your jurisdiction will apply to You and Our liability will apply to the maximum extent permitted by law.</p>\n" +
    "      <p class=\"p18\">You acknowledge and agree that:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Your use of Our Services is at your own risk and that Our Services are provided ‘as is’ and ‘as available';</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We, Our subsidiaries, affiliates and licensors do not represent or warrant to You (especially during the period of release of Our Services that is advertised as ‘Alpha’, ‘Beta’ or ‘Beta Version’ on Our Website) that:</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Your use of Our Services will meet your requirements;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Your use of Our Services will be uninterrupted or free from error;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>any information obtained by You as a result of your use of Our Services will be complete, accurate or reliable; or</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>defects in the operation or functionality of any part of Our Services will be corrected;</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>any content downloaded or otherwise obtained by You through your use of Our Services is done at your own discretion and risk and You will be solely responsible for any damage to your computer system or other device or loss of data that results from the download of any such material; and</p>\n" +
    "      <p class=\"p19\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>to the extent permitted by law, no advice or information, whether oral or written, obtained by You from Us or through Our website, applications or Our Services will create any warranty or representation not expressly stated in Our TOU.</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We further expressly disclaim all warranties of any kind to the fullest extent permitted by laws in New South Wales, Australia or Texas, USA, whether express or implied, including, but not limited to warranties of merchantability, fitness for a particular purpose and non-infringement.</p>\n" +
    "      <p class=\"p5\"><b>General Terms (Miscellaneous)</b></p>\n" +
    "      <p class=\"p17\">Our TOU constitutes the whole agreement between Us and You.</p>\n" +
    "      <p class=\"p24\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You acknowledge that Our Services may not be free from fault, bugs or interruptions, which may affect the performance of Our Services; AND You agree to comply with all applicable laws, including (without limitation), the following items of legislation of the Commonwealth of Australia being the Privacy Act 1988 (Cth) and the Spam Act 2003 (Cth) and similar items of legislation in other jurisdictions; AND You agree to participate on Our Services at Your own risk and agree not to hold Us liable or responsible for any injury, damage, death or the like howsoever caused; AND You agree not to hold us responsible for any dissatisfaction or losses, to the full extent permitted by the law, regarding your displayed information online or in the media or for other matters relating to your participation on Our Services; AND You agree that You are not Our affiliate or agent and that We are independent parties and will not represent yourself as such.</p>\n" +
    "      <p class=\"p24\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We reserve the right at any time to modify, limit, suspend or discontinue Our website or applications, in whole or in part, with or without notice, for the purpose of undertaking any maintenance, upgrading or similar processes in respect of the Website, and We will not be liable to You or any other party for any such modification, limitation, suspension or discontinuance. We may provide You with notices, including those regarding changes to Our TOU, by email, regular mail, or postings on Our website or applications.</p>\n" +
    "      <p class=\"p24\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Where We do not exercise or enforce any legal right or remedy which is contained in Our TOU (or which We have the benefit of under any Applicable Law), this will not be taken to be a formal waiver of Our rights and that those rights or remedies will still be available to Us. If a part of Our TOU is held to be void, voidable or unenforceable or an invalid part severed, the remainder of Our TOU is not affected.</p>\n" +
    "      <p class=\"p24\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We may at any time assign the whole or any part of Our rights and obligations under this Agreement to a related body corporate or a third party without your consent.</p>\n" +
    "      <p class=\"p24\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>The laws of New South Wales in the Commonwealth of Australia or The Laws of Texas, USA govern Our TOU and You irrevocably submit to the exclusive jurisdiction of the courts in those jurisdictions. Notwithstanding this, We reserve the right to apply for an injunctive remedy or an equivalent type of urgent legal relief in any jurisdiction where we provide Our Services.</p>\n" +
    "      <p class=\"p5\"><b>ToU: Ending Your Relationship with Us</b></p>\n" +
    "      <p class=\"p18\">Our TOU will continue to apply until either You cease using Our Services or We cancel your access.</p>\n" +
    "      <p class=\"p18\">If You want to cease using Our Services, You may do so by requesting your access to Our Services to be deleted.</p>\n" +
    "      <p class=\"p18\">We may at any time, cancel your access to Our Services if:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You have breached any provision of Our TOU or have acted in a manner which clearly shows that You do not intend to, or are unable to comply with the provisions of Our TOU;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We are unable to verify or authenticate any information You have provided;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We are required to do so by law (for example, where the provision of Our Services to You is, or becomes, unlawful);</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We are transitioning to no longer provide Our Services in your country; or</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>the provision of Our Platform is no longer commercially viable.</p>\n" +
    "      <p class=\"p18\">Nothing in this clause will affect Our rights regarding the provision of Our Services under Our TOU.</p>\n" +
    "      <br>\n" +
    "      <p class=\"p4\">PRIVACY POLICY</p>\n" +
    "      <br>\n" +
    "      <p class=\"p5\"><b>Policy Details</b></p>\n" +
    "      <p class=\"p17\">This policy details how We use and protect personal information (PI) it receives, including information in relation to Our Services. We are committed to protecting your privacy. Information you (‘You’) provide while using Our Services will be used only in accordance with this Policy.</p>\n" +
    "      <p class=\"p17\">The terms of this Policy prevails and by interacting with Us, You agree to be bound by not just the contents of this Policy but Our Rules and Terms of Use. In order for Us to provide Our Services We must collect information from You. This Policy applies only to Our Services and to activities of Our officers and employees in carrying out Our business or providing these services. Our websites and applications do not extend to third-party sites and We are not responsible for the collection or use of PI collected on these third-party sites.</p>\n" +
    "      <p class=\"p17\">From time to time, We may conduct a promotion or other activity, which has its own specific Privacy Policy that will be provided at the time any information is requested. Any specific Privacy Policy will then apply to a specific promotion or activity and will override our standard Policy to the extent of any inconsistency.</p>\n" +
    "      <p class=\"p5\"><b>How We Collect Information</b></p>\n" +
    "      <p class=\"p17\">When You visit Our websites or applications You may provide Us with two types of information: Personal Information (‘PI’) You knowingly choose to disclose that is collected on an individual basis and ‘website use information’ collected on an aggregate basis as You and others use Our Services.</p>\n" +
    "      <p class=\"p17\">We may, as a result of your usage of Our Services, hold and process PI obtained about You when You access Our Services for the purposes of fraud prevention, to conduct our business and to provide You with better customer service and products, to evaluate the effectiveness of our marketing and for statistical analysis.</p>\n" +
    "      <p class=\"p5\"><b>Privacy: Personal Information</b></p>\n" +
    "      <p class=\"p17\">We identify You by your full name, email address, username, geographic location and password, or with your permission by reference to your identity on another web service such as Facebook, Twitter and Google+. You may also provide further identifying information such as a picture of yourself. If You connect another web service with your User Account, You allow Us to collect and store information about You made available by that web service.</p>\n" +
    "      <p class=\"p17\">During your interactions with Us, You may be invited to provide information about goods, services and other people, businesses, organizations or brands with which You wish to affiliate. In order to receive or provide payments from You, We may collect information and account details enabling such transactions, and details of those transactions, such as a record of products You have sold/purchased/used.</p>\n" +
    "      <p class=\"p5\"><b>How We Use Collected Information</b></p>\n" +
    "      <p class=\"p18\">We use the information You provide in order for Us to identify You, communicate with You, and provide personalized services. You can remain anonymous to other Users of Our Services by withholding identification through the use of usernames, opt in to remain anonymous refusing offers to connect to Our Platform with accounts on other web sites (such as Facebook or Twitter), and/or making use of a third-party email provider.</p>\n" +
    "      <p class=\"p18\">If You decline to provide certain required PI, in accordance with Our Terms of Use, We may not to be able to provide You access to Our Services. Our use of collected information may include:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Sharing information, including information that is not publicly published but excluding sensitive information, with other Users, including but not limited to the use of favorites, articles, blogs, videos, events and other goods/services or brands with which You wish to affiliate with;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Sharing aggregate information with third parties with your consent or which do not personally identify You;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Providing customized services to You, including selection of content and advertising;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Contacting You, including with promotional materials and notifications or to verify accuracy of details provided for your interaction with Our Services;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Billing and account management;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Research and development in improving Our Services and in developing new products and services; and</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Monitoring and maintaining Our computers and network.</p>\n" +
    "      <p class=\"p18\">We will not sell or otherwise provide your PI to a third party, or make any other use of your PI, for any purpose which you have not previously agreed to or that is not incidental to your use of Our Services. For the avoidance of doubt, PI will not be used for any purpose, which a reasonable person would not expect. Notwithstanding this, we cannot be held responsible for any misuse or unauthorized disclosure of your PI by third parties.</p>\n" +
    "      <p class=\"p18\">If You request for Us not to use PI in a particular manner or at all, We will adopt all reasonable measures to observe your request but We may still use or disclose that information if We:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>subsequently notify You of the intended use or disclosure and You do not object to that use or disclosure;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>believe that observing the request will legitimately inhibit your ability to provide Our Services;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>believe that the use or disclosure is reasonably necessary to assist a law enforcement agency; or</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>is required by law to disclose that information.</p>\n" +
    "      <p class=\"p18\">The PI We collect will be used only for purposes outlined in this policy. If We propose to use your information for any other purposes then You will be provided with a means to opt out of the use of your information for those purposes. You may opt out of receiving promotional materials from Us but cannot opt out of receiving certain communications from Us (such as service announcements and administrative messages) as it is instrumental to providing Our Services.</p>\n" +
    "      <p class=\"p18\">You can view and edit your account information at any time by visiting your profile settings page. You can request for Us to delete your account on Our websites or applications (in the relevant spaces) at any time, which will remove all personal and identifying information from Our service, except as required to be retained by law or for legitimate business purposes. Some information may reside on backup devices for a limited period of time after your account is deleted.</p>\n" +
    "      <p class=\"p5\"><b>Information Sharing</b></p>\n" +
    "      <p class=\"p18\">Our Services are designed to allow You to share information about your affiliations with brands to others. Some information You provide Us is made publicly available and may be accessed by anyone on the Internet including, but not limited to:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>your username</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>your profile</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>your profile picture (if any)</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>your approximate geographic locations</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>links to your accounts on third party websites</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>content You provide, including articles, blog posts or video files.</p>\n" +
    "      <p class=\"p18\">Your information (including name, email address, postal address, telephone number, preferences, sales or purchases) is not made available to anyone except for circumstances as per Our Terms and Conditions of Use, unless You provide express consent. We will not publish this information about You except where it may be collated to form anonymous analytics and presented under general indices and graphs.</p>\n" +
    "      <p class=\"p18\">Any material You post for advertising purposes on Our websites or applications will be publicly displayed and it may not be possible to subsequently delete or remove without prior permission from storage for the purposes of Our Services. Any material You may have contributed to Our websites or applications may remain in some form, on the site even after You cease to be a member.</p>\n" +
    "      <p class=\"p18\">We do not sell or share your PI with other people or companies except where:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>You have provided your express consent;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>It is made in connection with the provision of Our Services pursuant to Our Terms of Use;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We provide the information to trusted affiliates to process information on our behalf, or to billing, customer relations management, credit card, order fulfillment or shipping companies in order to provide goods or services to You. Such partners must process information under agreements to maintain this policy and with the appropriate security and confidentiality mechanisms;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>In response to a court order, legal process or enforceable government request;</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>We believe it is necessary to share information in order to investigate or take action in response to illegal activities; fraud or technical or security issues; threats against the rights; property or safety of 21 Capital, Monarchy, its employees, Users or the public; potential or actual violations of applicable terms of service; or</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>As otherwise required by law.</p>\n" +
    "      <p class=\"p18\">We may share non-personal, aggregate information with third parties, which do not personally identify You.</p>\n" +
    "      <p class=\"p18\">We may display advertisements that are targeted based on PI, including your interactions with this web site. We do not provide any PI to advertisers or advertisement serving companies, but advertisers may collect anonymous information and draw inferences about targeting criteria for a particular advertising campaign.</p>\n" +
    "      <p class=\"p18\">If You provide us with your mobile telephone number, email address, or other such PI, You accept and authorize us to send You messages related to Our Services (where You give Us express permission to do so).</p>\n" +
    "      <p class=\"p18\">We may disclose your PI solely if You authorize it, except where that disclosure is required to obtain advice, prepare legal proceedings, to investigate suspected improper conduct or wrong doing, to assist a lawful authority in the discharge of its duties and/or by law.</p>\n" +
    "      <p class=\"p5\"><b>Data Quality &amp; Security</b></p>\n" +
    "      <p class=\"p17\">We take reasonable steps to ensure that the PI we collect, use or disclose is accurate, complete and up to date. If your PI is incorrect or changes, please contact Us so that We can update your PI and continue providing You with Our Services.</p>\n" +
    "      <p class=\"p17\">We employ appropriate electronic and procedural measures to protect PI from misuse, loss and unauthorized access, modification or disclosure. Access to your PI is limited to employees We believe reasonably need to use that information in order to perform their roles in providing or developing Our Services.</p>\n" +
    "      <p class=\"p17\">We are committed to the protection of your privacy and meeting the standards set out in Australia’s Privacy Act 1988 (Commonwealth) and the National Privacy Principles (‘NPP’) and similar legislation in jurisdiction we service.</p>\n" +
    "      <p class=\"p17\">Once we no longer need your PI, we take reasonable steps to delete same.</p>\n" +
    "      <p class=\"p5\"><b>Sensitive Information</b></p>\n" +
    "      <p class=\"p18\">Where practical, You may deal with Us on an anonymous basis. However, because of the services We provide, if You do not provide Us with your PI, We may not be able to allow you to join or participate in utilising Our Services or provide You with the requested product or service.</p>\n" +
    "      <p class=\"p18\">We will not collect sensitive information except with your consent, and only if it’s necessary for some activity or function. For the purpose of this policy, ‘Sensitive Information’ means health information or information or an opinion about an individual’s:</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Racial or Ethnic origin</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Political opinions</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Membership of a Political Association</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Religious Beliefs or affiliations</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Philosophical beliefs</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Membership of a professional or trade association</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Membership of a trade union</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Sexual preferences or practices; or</p>\n" +
    "      <p class=\"p22\"><span class=\"Apple-tab-span\">\t</span>•<span class=\"Apple-tab-span\">\t</span>Criminal record</p>\n" +
    "      <p class=\"p18\">We will not require You to provide sensitive information unless you have consented; or collection of the information is specifically authorised or required by law.</p>\n" +
    "      <p class=\"p5\"><b>Anti-Money Laundering Policy</b></p>\n" +
    "      <p class=\"p17\">We are committed to full compliance with all applicable laws and regulations regarding Anti- Money Laundering (“AML”). Our policy is to prevent people engaged in money laundering, fraud, and other financial crimes, including terrorist financing, from using Our Services.</p>\n" +
    "      <p class=\"p17\">To comply with legislative requirements, and global sanctions, We may screen our Users accounts for suspicious activity. In addition, We may request that You provide Us with documentation to help prove your identity or for business verification purposes. We will report suspicious transactions to the financial intelligence unit in the respective country.</p>\n" +
    "      <p class=\"p17\">As part of our AML procedures, We may collect information from You to satisfy Our “know your member and customer” requirements (full name or business/organization name, officers (where applicable), email address, date of birth and address). This means that We may request information from You due to a specific identification requirement. We may ask You to provide documentation to help confirm your identity or provide additional information.</p>\n" +
    "      <p class=\"p5\"><b>Revisions</b></p>\n" +
    "      <p class=\"p14\">We may amend Our Privacy Policy from time to time. If We make a change to this Policy that, in Our sole discretion, is material, We will notify You via our platform news updates or our public forum pages. By continuing to access or Our Services after those changes become effective, You agree to be bound by the revised Policy.</p>\n" +
    "      <br>\n" +
    "      <p class=\"p4\">MORE INFORMATION</p>\n" +
    "      <br>\n" +
    "      <p class=\"p18\">For more information contact us:</p>\n" +
    "      <p class=\"p18\">support@lovestamp.io</p>\n" +
    "      <br>\n" +
    "      <p class=\"p28\"><b>Australia</b></p>\n" +
    "      <p class=\"p18\">21 Capital Pty Ltd</p>\n" +
    "      <p class=\"p18\">Address: Suite 109 (Rear Door), 26-32 Pirrama Road, Pyrmont NSW 2009 Australia</p>\n" +
    "      <p class=\"p28\"><b>United States</b></p>\n" +
    "      <p class=\"p18\">Monarchy LLC</p>\n" +
    "      <p class=\"p18\">Address: Level 2, 2800 San Jacinto Street, Houston, TX 77004, USA</p>\n" +
    "      <br>\n" +
    "      <p class=\"p18\"><em>Last updated: January 2015</em></p>\n" +
    "      <br>\n" +
    "      <br>\n" +
    "    </section>\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/login/partials/fan.html',
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-show=\"noMap === false\" ng-class=\"{\n" +
    "    'page--login': true,\n" +
    "    'page--active': true,\n" +
    "    'page--login__fan': true\n" +
    "  }\">\n" +
    "\n" +
    "  <div class=\"site-banner\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"login-auth\">LoveStamp Login</a>\n" +
    "\n" +
    "      <span class=\"terms\" ng-show=\"is_device\">\n" +
    "        You agree to our&nbsp;<a ui-sref=\"fan-legal\">Terms</a> by signing in.\n" +
    "      </span>\n" +
    "\n" +
    "      <span class=\"terms\" ng-show=\"!is_device\">\n" +
    "        You agree to our&nbsp;<a rel-external href=\"https://lovestamp.io/legal-mumbo-jumbo\">Terms</a> by signing in.\n" +
    "      </span>\n" +
    "\n" +
    "    </h1>\n" +
    "  </div>\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <div leaflet id=\"login\" defaults=\"defaults\" center=\"center\" ng-class=\"{\n" +
    "      'animated': authData && showMap,\n" +
    "      'fadeIn': authData && showMap\n" +
    "    }\" ng-show=\"map\" height=\"{{mapHeight()}}\" width=\"100%\"></div>\n" +
    "\n" +
    "  <div loader ng-class=\"{\n" +
    "      'loading': authObj && authData && showMap\n" +
    "    }\" ng-if=\"! showMap\">\n" +
    "  </div>\n" +
    "\n" +
    "  <ul>\n" +
    "\n" +
    "    <li class=\"auth-with-facebook\" ng-show=\"provider == 'facebook' || provider == null\">\n" +
    "      <a class=\"button\" click-monad ng-click=\"authWith('facebook')\">\n" +
    "        <span class=\"fa fa-facebook\"></span> Sign in with Facebook\n" +
    "      </a>\n" +
    "    </li>\n" +
    "\n" +
    "\n" +
    "    <li class=\"auth-with-twitter\" ng-show=\"provider == 'twitter' || provider == null\">\n" +
    "      <a class=\"button\" click-monad ng-click=\"authWith('twitter')\">\n" +
    "        <span class=\"fa fa-twitter\"></span>Sign in with Twitter\n" +
    "      </a>\n" +
    "    </li>\n" +
    "\n" +
    "\n" +
    "    <li class=\"auth-with-google\" ng-show=\"provider == 'google' || provider == null\">\n" +
    "      <a class=\"button\" click-monad ng-click=\"authWith('google')\">\n" +
    "        <span class=\"fa fa-google\"></span> Sign in with Google Plus\n" +
    "      </a>\n" +
    "    </li>\n" +
    "\n" +
    "  </ul>\n" +
    "\n" +
    "</section>\n" +
    "\n" +
    "<section class=\"page--no-map\">\n" +
    "\n" +
    "  <style>\n" +
    "    .no-map {\n" +
    "      text-align: center;\n" +
    "      display: none !important;\n" +
    "    }\n" +
    "    .no-map.show-this {\n" +
    "      display: block !important;\n" +
    "    }\n" +
    "    .no-map .fa {\n" +
    "      font-size: 3rem;\n" +
    "    }\n" +
    "\n" +
    "    .no-map--inner {\n" +
    "      margin: 0 2vh;\n" +
    "      position: absolute;\n" +
    "      top: 50%;\n" +
    "      margin-top: -60%;\n" +
    "    }\n" +
    "    .guide h2,\n" +
    "    .guide li,\n" +
    "    .guide {\n" +
    "      font-size: .8rem;\n" +
    "    }\n" +
    "    .guide h2 {\n" +
    "      margin-bottom: 1rem;\n" +
    "    }\n" +
    "    .guide {\n" +
    "      margin-top: 3rem;\n" +
    "    }\n" +
    "  </style>\n" +
    "\n" +
    "  <div class=\"no-map\" ng-class=\"{\n" +
    "      'show-this': noMap === true\n" +
    "    }\">\n" +
    "    <div class=\"no-map--inner\">\n" +
    "\n" +
    "      <span class=\"fa fa-spin fa-location-arrow\"></span>\n" +
    "\n" +
    "      <h1>Ain't nobody got time for dat!</h1>\n" +
    "      <p>LoveStamp is map-based, enable <i>Location Services</i> or \n" +
    "         it won't work!</p>\n" +
    "\n" +
    "      <div class=\"guide\">\n" +
    "        <h2>Learn how to enable location services:</h2>\n" +
    "        <ul>\n" +
    "          <li ng-show=\"is_device\"><a ng-click=\"InAppBrowserOpen('https://support.apple.com/en-us/HT201357', '_blank')\">iOS</a></li>\n" +
    "          <li ng-show=\"is_device\"><a ng-click=\"InAppBrowserOpen('https://support.google.com/gmm/answer/1646140?hl=en', '_blank')\">Android</a></li>\n" +
    "          <li ng-show=\"!is_device\"><a rel-external href=\"https://support.apple.com/en-us/HT201357\">iOS</a></li>\n" +
    "          <li ng-show=\"!is_device\"><a rel-external href=\"https://support.google.com/gmm/answer/1646140?hl=en\">Android</a></li>\n" +
    "        </ul>\n" +
    "      </div>\n" +
    "\n" +
    "    </div>\n" +
    "  </div>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/login/partials/loader.html',
    "<div class=\"loader\">\n" +
    "  <span class=\"lovestamp--loader\">\n" +
    "  <!--<span class=\"fa ~fa-spin ~fa-spinner ~fa-cog ~fa-spinner\"></span>-->\n" +
    "  </span>\n" +
    "</div>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/login/partials/merchant.html',
    "<div optimized-page-title></div>\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-show=\"noMap === false\" ng-class=\"{\n" +
    "    'page--login': true,\n" +
    "    'page--active': true,\n" +
    "    'page--login__merchant': true\n" +
    "  }\">\n" +
    "\n" +
    "  <nav class=\"back\">\n" +
    "    <ul>\n" +
    "      <li>\n" +
    "        <a click-monad href=\"http://lovestamp.io\">Back Home</a>\n" +
    "      </li>\n" +
    "    </ul>\n" +
    "  </nav>\n" +
    "\n" +
    "  <div class=\"site-banner\">\n" +
    "\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"login-auth\">LoveStamp Login</a>\n" +
    "      <i>For Merchants</i>\n" +
    "\n" +
    "      <span class=\"terms\" ng-show=\"is_device\">\n" +
    "        You agree to our  \n" +
    "          <a click-monad ng-click=\"InAppBrowserOpen('https://lovestamp.io/legal-mumbo-jumbo', '_blank');\">Terms</a> \n" +
    "        by signing in.\n" +
    "      </span>\n" +
    "\n" +
    "      <span class=\"terms\" ng-show=\"!is_device\">\n" +
    "        You agree to our  \n" +
    "          <a rel-external mobile-link ng-href=\"https://lovestamp.io/legal-mumbo-jumbo\">Terms</a> \n" +
    "        by signing in.\n" +
    "      </span>\n" +
    "\n" +
    "    </h1>\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <div leaflet id=\"login\" defaults=\"defaults\" center=\"center\" ng-class=\"{\n" +
    "      'animated': authData && showMap,\n" +
    "      'fadeIn': authData && showMap\n" +
    "    }\" ng-show=\"map\" height=\"{{mapHeight()}}\" width=\"100%\"></div>\n" +
    "\n" +
    "  <div loader ng-class=\"{\n" +
    "      'loading': authObj && authData && showMap\n" +
    "    }\" ng-if=\"! showMap\">\n" +
    "  </div>\n" +
    "\n" +
    "  <ul>\n" +
    "\n" +
    "    <li class=\"auth-with-facebook\" ng-show=\"provider == 'facebook' || provider == null\">\n" +
    "      <a class=\"button\" click-monad ng-click=\"authWith('facebook')\">\n" +
    "        <span class=\"fa fa-facebook\"></span> Sign in with Facebook\n" +
    "      </a>\n" +
    "    </li>\n" +
    "\n" +
    "\n" +
    "    <li class=\"auth-with-twitter\" ng-show=\"provider == 'twitter' || provider == null\">\n" +
    "      <a class=\"button\" click-monad ng-click=\"authWith('twitter')\">\n" +
    "        <span class=\"fa fa-twitter\"></span>Sign in with Twitter\n" +
    "      </a>\n" +
    "    </li>\n" +
    "\n" +
    "\n" +
    "    <li class=\"auth-with-google\" ng-show=\"provider == 'google' || provider == null\">\n" +
    "      <a class=\"button\" click-monad ng-click=\"authWith('google')\">\n" +
    "        <span class=\"fa fa-google\"></span> Sign in with Google Plus\n" +
    "      </a>\n" +
    "    </li>\n" +
    "\n" +
    "  </ul>\n" +
    "\n" +
    "</section>\n" +
    "\n" +
    "\n" +
    "<section class=\"page--no-map\">\n" +
    "\n" +
    "  <style>\n" +
    "    .no-map {\n" +
    "      text-align: center;\n" +
    "      display: none !important;\n" +
    "    }\n" +
    "    .no-map.show-this {\n" +
    "      display: block !important;\n" +
    "    }\n" +
    "    .no-map .fa {\n" +
    "      font-size: 3rem;\n" +
    "    }\n" +
    "\n" +
    "    .no-map--inner {\n" +
    "      margin: 0 2vh;\n" +
    "      position: absolute;\n" +
    "      top: 50%;\n" +
    "      margin-top: -60%;\n" +
    "    }\n" +
    "    .guide h2,\n" +
    "    .guide li,\n" +
    "    .guide {\n" +
    "      font-size: .8rem;\n" +
    "    }\n" +
    "    .guide h2 {\n" +
    "      margin-bottom: 1rem;\n" +
    "    }\n" +
    "    .guide {\n" +
    "      margin-top: 3rem;\n" +
    "    }\n" +
    "  </style>\n" +
    "\n" +
    "  <div class=\"no-map\" ng-class=\"{\n" +
    "      'show-this': noMap === true\n" +
    "    }\">\n" +
    "    <div class=\"no-map--inner\">\n" +
    "\n" +
    "      <span class=\"fa fa-spin fa-location-arrow\"></span>\n" +
    "\n" +
    "      <h1>Ain't nobody got time for dat!</h1>\n" +
    "      <p>LoveStamp is map-based, enable <i>Location Services</i> or \n" +
    "         it won't work!</p>\n" +
    "\n" +
    "      <div class=\"guide\">\n" +
    "        <h2>Learn how to enable location services:</h2>\n" +
    "        <ul>\n" +
    "          <li ng-show=\"is_device\"><a click-monad ng-click=\"InAppBrowserOpen('https://support.apple.com/en-us/HT201357', '_blank')\">iOS</a></li>\n" +
    "          <li ng-show=\"is_device\"><a click-monad ng-click=\"InAppBrowserOpen('https://support.google.com/gmm/answer/1646140?hl=en', '_blank')\">Android</a></li>\n" +
    "          <li ng-show=\"!is_device\"><a rel-external href=\"https://support.apple.com/en-us/HT201357\">iOS</a></li>\n" +
    "          <li ng-show=\"!is_device\"><a rel-external href=\"https://support.google.com/gmm/answer/1646140?hl=en\">Android</a></li>\n" +
    "        </ul>\n" +
    "      </div>\n" +
    "\n" +
    "    </div>\n" +
    "  </div>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/payments/partials/alt-base.html',
    "<!--*\n" +
    "@fileOverview\n" +
    "\n" +
    " _\n" +
    "| |\n" +
    "| |__  _____  ___ _____\n" +
    "|  _ \\(____ |/___) ___ |\n" +
    "| |_) ) ___ |___ | ____|\n" +
    "|____/\\_____(___/|_____)\n" +
    "\n" +
    "@module www/controllers/admin/partials\n" +
    "@description\n" +
    "\n" +
    "  A basic set of interaction elements for administrators to work with, \n" +
    "  manipulate, and build a stampository.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--payments': true,\n" +
    "    'page--active': true\n" +
    "  }\" ng-init=\"_loadPaymentsList()\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "  </header>\n" +
    "\n" +
    "  <div itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"admin\" role=\"main\">\n" +
    "\n" +
    "   <div site-navigation></div>\n" +
    "\n" +
    "    <section class=\"command-center command-center--payments\">\n" +
    "\n" +
    "      <header>\n" +
    "        <h1>Payments</h1>\n" +
    "      </header>\n" +
    "\n" +
    "      <aside style=\"text-align: center; margin: 0 5vh 0; margin-bottom: 3vh\">\n" +
    "        <p style=\"font-size: 16px\">Below is a record of all your payments made on LoveStamp, a product of Monarchy LLC.</p>\n" +
    "        <!--<p style=\"font-size: 16px;\"><small style=\"font-size: .9rem;\">If you have any queries about this, <a href=\"mailto:support@lovestamp.io\" style=\"text-decoration: underline;\">contact us</a>.</small></p>-->\n" +
    "      </aside>\n" +
    "\n" +
    "      <aside ng-show=\"paymentsList.length === 0\" style=\"text-align: center\">\n" +
    "        <p>You do not have a purchase history.</p>\n" +
    "      </aside>\n" +
    "\n" +
    "      <ul class=\"list--adaptive\" ng-show=\"paymentsList !== 0\">\n" +
    "        <li class=\"hint--top\" ng-repeat=\"type in paymentsList\" data-hint=\"Purchase summary\">\n" +
    "          <table class=\"table--adaptive alt\">\n" +
    "\n" +
    "            <tfoot id=\"{{type.$id}}\" once-class=\"{\n" +
    "                'active': type.$id === hashedType\n" +
    "              }\">\n" +
    "              <tr>\n" +
    "                <td colspan=\"4\">\n" +
    "                  <em><a href=\"merchant/payments{{type.$id}}\">\n" +
    "\n" +
    "                    <span style=\"text-transform: capitalize\" once-text=\"type.$id\"></span> purchased\n" +
    "\n" +
    "                  </a></em>.\n" +
    "                </td>\n" +
    "              </tr>\n" +
    "            </tfoot>\n" +
    "\n" +
    "            <thead>\n" +
    "              <tr>\n" +
    "                <th>Date</th>\n" +
    "                <th>Description</th>\n" +
    "                <th>Paid</th>\n" +
    "                <th>Status</th>\n" +
    "              </tr>\n" +
    "            </thead>\n" +
    "\n" +
    "            <tbody>\n" +
    "              <tr once-class=\"{\n" +
    "                  'stamps': (payment.description.indexOf('stamps') !== -1),\n" +
    "                  'impressions': (payment.description.indexOf('stamps') === -1)\n" +
    "                }\" ng-repeat=\"payment in type.payments\">\n" +
    "                <td data-label=\"{{payment.created_label}}\">{{payment.created}}</td>\n" +
    "                <td data-label=\"{{payment.short_description}}\">\n" +
    "                  <a once-href=\"(payment.url_prefix + payment.tail_description)\">\n" +
    "                    {{payment.short_description}}, {{payment._social_prefix}}{{payment.tail_description}}\n" +
    "                  </a>\n" +
    "                </td>\n" +
    "                <td data-label=\"{{payment.amount}}\">\n" +
    "                  <span once-show=\"payment.currency == 'usd'\">$</span>\n" +
    "                  <span once-text=\"(payment.amount + payment.currency.toUpperCase())\">\n" +
    "                  </span>\n" +
    "                </td>\n" +
    "\n" +
    "                <td ng-init=\"shippingLabel = (payment.shipped === true) ? 'Shipped' : 'Preparing to Ship'\" data-label=\"{{shippingLabel}}\">\n" +
    "\n" +
    "                  <span data-label=\"Preparing to Ship\" data-hint=\"Preparing to Ship\" class=\"hint--top\" once-show=\"(payment.description.indexOf('stamps') !== -1) && payment.shipped !== true\">\n" +
    "                    <span class=\"fa fa-clock-o\"></span>\n" +
    "                  </span>\n" +
    "\n" +
    "                  <span data-label=\"Shipped\" data-hint=\"Shipped\" class=\"hint--top\" once-show=\"(payment.description.indexOf('stamps') !== -1) && payment.shipped === true\">\n" +
    "                    <span class=\"fa fa-envelope-square\"></span>\n" +
    "                  </span>\n" +
    "\n" +
    "                  <span data-label=\"Shipped\" data-hint=\"Shipped\" class=\"hint--top\" once-show=\"(payment.description.indexOf('stamps') === -1)\">\n" +
    "                    <span style=\"font-size: 1rem\" class=\"fa fa-tachometer\"></span>\n" +
    "                  </span>\n" +
    "                </td>\n" +
    "\n" +
    "              </tr>\n" +
    "            </tbody>\n" +
    "          </table>\n" +
    "        </li>\n" +
    "      </ul>\n" +
    "\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/payments/partials/base.html',
    "<!--*\n" +
    "@fileOverview\n" +
    "\n" +
    " _\n" +
    "| |\n" +
    "| |__  _____  ___ _____\n" +
    "|  _ \\(____ |/___) ___ |\n" +
    "| |_) ) ___ |___ | ____|\n" +
    "|____/\\_____(___/|_____)\n" +
    "\n" +
    "@module www/controllers/admin/partials\n" +
    "@description\n" +
    "\n" +
    "  A basic set of interaction elements for administrators to work with, \n" +
    "  manipulate, and build a stampository.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--payments': true,\n" +
    "    'page--active': true\n" +
    "  }\" ng-init=\"loadPaymentsList()\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "  </header>\n" +
    "\n" +
    "  <div itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"admin\" role=\"main\">\n" +
    "\n" +
    "   <div site-navigation></div>\n" +
    "\n" +
    "    <section class=\"command-center command-center--payments\">\n" +
    "\n" +
    "      <header>\n" +
    "        <h1>Payments</h1>\n" +
    "      </header>\n" +
    "\n" +
    "      <aside style=\"text-align: center; margin: 0 5vh 0; margin-bottom: 3vh\">\n" +
    "        <p style=\"font-size: 16px\">Below is a record of all your payments made on LoveStamp, a product of Monarchy LLC.</p>\n" +
    "        <!--<p style=\"font-size: 16px;\"><small style=\"font-size: .9rem;\">If you have any queries about this, <a href=\"mailto:support@lovestamp.io\" style=\"text-decoration: underline;\">contact us</a>.</small></p>-->\n" +
    "      </aside>\n" +
    "\n" +
    "      <aside ng-show=\"!paymentsList\" style=\"text-align: center\">\n" +
    "        <p>You do not have a purchase history.</p>\n" +
    "      </aside>\n" +
    "\n" +
    "      <table class=\"table--adaptive base animated fade-in\" ng-if=\"paymentsList.length !== 0\">\n" +
    "\n" +
    "        <thead>\n" +
    "          <tr>\n" +
    "            <th>Date</th>\n" +
    "            <th>Description</th>\n" +
    "            <th>Paid</th>\n" +
    "            <th>Status</th>\n" +
    "          </tr>\n" +
    "        </thead>\n" +
    "\n" +
    "        <tbody>\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "                                                _        \n" +
    "           ____  _____ _   _ ____  _____ ____ _| |_  ___ \n" +
    "          |  _ \\(____ | | | |    \\| ___ |  _ (_   _)/___)\n" +
    "          | |_| / ___ | |_| | | | | ____| | | || |_|___ |\n" +
    "          |  __/\\_____|\\__  |_|_|_|_____)_| |_| \\__|___/ \n" +
    "          |_|         (____/                             \n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <tr ns:os=\"https://schema.org/Order\" itemscope itemtype=\"https://schema.org/Order\" ng-repeat=\"payment in paymentsList\" ng-class=\"{\n" +
    "                'stamps'      : (payment.description.indexOf('stamps') !== -1),\n" +
    "                'impressions' : (payment.description.indexOf('stamps') === -1)\n" +
    "            }\">\n" +
    "\n" +
    "            <td data-label=\"{{payment.created_label}}\" ng-bind=\"payment.created\"></td>\n" +
    "\n" +
    "            <td data-label=\"{{payment.short_description}}\">\n" +
    "              <a ng-href=\"(payment.url_prefix + payment.tail_description)\">{{payment.short_description}}, {{payment._social_prefix}}{{payment.tail_description}}</a>\n" +
    "            </td>\n" +
    "\n" +
    "            <td data-label=\"{{payment.amount}}\">\n" +
    "              <span ng-show=\"payment.currency == 'usd'\">$</span>\n" +
    "              {{payment.amount}}\n" +
    "              {{payment.currency.toUpperCase()}}\n" +
    "            </td>\n" +
    "\n" +
    "            <td ng-init=\"shippingLabel = (payment.shipped === true || payment.description.indexOf('stamps') === -1) ? 'Shipped' : 'Preparing to Ship'\" data-label=\"{{shippingLabel}}\">\n" +
    "\n" +
    "              <md-checkbox ng-model=\"payment.shipped\" class=\"data--hint\" data-hint=\"{{payment._status.title}}\" aria-label=\"{{payment._status.title}}\" ng-disabled=\"!isAuthorized\" ng-show=\"isAuthorized && payment._status.hidden === false && (payment.description.indexOf('stamps') !== -1)\" ng-click=\"setStatus(payment)\">\n" +
    "              </md-checkbox>\n" +
    "\n" +
    "              <span ng-show=\"isAuthorized && payment._status.hidden === false && (payment.description.indexOf('stamps') === -1)\" data-hint=\"Shipped\" style=\"display: block; width: 100%\">\n" +
    "                <span class=\"fa fa-tachometer\" style=\"font-size: 1rem; left: -6px; position: relative\"></span>\n" +
    "              </span>\n" +
    "\n" +
    "              <span data-hint=\"Preparing to Ship\" class=\"hint--top\" ng-show=\"(payment.description.indexOf('stamps') !== -1) && !isAuthorized && !payment.shipped\">\n" +
    "                <span class=\"fa fa-clock-o\"></span>\n" +
    "              </span>\n" +
    "\n" +
    "              <span data-hint=\"Shipped\" class=\"hint--top\" ng-show=\"(payment.description.indexOf('stamps') !== -1) && !isAuthorized && payment.shipped\">\n" +
    "                <span class=\"fa fa-envelope-square\"></span>\n" +
    "              </span>\n" +
    "\n" +
    "              <span data-label=\"Shipped\" data-hint=\"Shipped\" class=\"hint--top\" ng-show=\"(payment.description.indexOf('stamps') === -1) && !isAuthorized\">\n" +
    "                <span class=\"fa fa-tachometer\" style=\"font-size: 1rem; left: -6px; position: relative\"></span>\n" +
    "              </span>\n" +
    "\n" +
    "            </td>\n" +
    "          </tr>\n" +
    "        </tbody>\n" +
    "      </table>\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/requests/partials/base.html',
    "<!--*\n" +
    "@fileOverview\n" +
    "\n" +
    " _\n" +
    "| |\n" +
    "| |__  _____  ___ _____\n" +
    "|  _ \\(____ |/___) ___ |\n" +
    "| |_) ) ___ |___ | ____|\n" +
    "|____/\\_____(___/|_____)\n" +
    "\n" +
    "@module www/controllers/requests/partials\n" +
    "@description\n" +
    "\n" +
    "A page for tracking stamp requests.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--requests': true,\n" +
    "    'page--active': true\n" +
    "  }\" ng-init=\"loadRequestsList()\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "  </header>\n" +
    "\n" +
    "  <div itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"admin\" role=\"main\">\n" +
    "\n" +
    "   <div site-navigation></div>\n" +
    "\n" +
    "    <section class=\"command-center command-center--requests\">\n" +
    "\n" +
    "      <header>\n" +
    "        <h1>Requests</h1>\n" +
    "      </header>\n" +
    "\n" +
    "      <table class=\"table--adaptive base\">\n" +
    "        <tfoot>\n" +
    "          <tr>\n" +
    "            <td colspan=\"5\">\n" +
    "              All merchant requests for LoveStamps.\n" +
    "            </td>\n" +
    "          </tr>\n" +
    "        </tfoot>\n" +
    "\n" +
    "        <thead>\n" +
    "          <tr>\n" +
    "            <th>\n" +
    "              <!--\n" +
    "                 -<input\n" +
    "                 -  ng-model=\"searchByType\"\n" +
    "                 -  type=\"text\"\n" +
    "                 -  placeholder=\"Type\"\n" +
    "                 -  autofocus\n" +
    "                 -/>\n" +
    "                 -->\n" +
    "              Type\n" +
    "            </th>\n" +
    "            <th>Name</th>\n" +
    "            <th>Organization</th>\n" +
    "            <th>Phone Number</th>\n" +
    "            <th>Referrer</th>\n" +
    "          </tr>\n" +
    "        </thead>\n" +
    "\n" +
    "        <tbody ng-repeat=\"rOwner in requestsList\">\n" +
    "\n" +
    "          <tr ng-repeat=\"r in rOwner.requests | filter: { contactBusinessType: searchByType }\">\n" +
    "\n" +
    "            <td class=\"hint--top\" ng-if=\"r.contactAboutBusiness\" data-hint=\"{{r.contactAboutBusiness}}\">\n" +
    "              <span ng-if=\"(isAuthorized && r.contactAboutBusiness) && (r.contactBusinessType.indexOf('Test') !== -1 || r.contactAboutBusiness.indexOf('Test') !== -1)\" class=\"fa fa-eye-slash\"></span>\n" +
    "              <span ng-if=\"r.contactAboutBusiness && (r.contactBusinessType.indexOf('Shop') !== -1 || r.contactAboutBusiness.indexOf('Shop') !== -1)\" class=\"fa fa-home\"></span>\n" +
    "              <span ng-if=\"r.contactAboutBusiness && (r.contactBusinessType.indexOf('Bitcoin') !== -1 || r.contactAboutBusiness.indexOf('Bitcoin') !== -1)\" class=\"fa fa-bitcoin\"></span>\n" +
    "              {{r.contactBusinessType}}\n" +
    "            </td>\n" +
    "\n" +
    "            <td class=\"hint--top\" ng-if=\"!r.contactAboutBusiness\">\n" +
    "              <span ng-if=\"(isAuthorized && r.contactAboutBusiness) && (r.contactBusinessType.indexOf('Test') !== -1 || r.contactAboutBusiness.indexOf('Test') !== -1)\" class=\"fa fa-eye-slash\"></span>\n" +
    "              <span ng-if=\"r.contactAboutBusiness && (r.contactBusinessType.indexOf('Shop') !== -1 || r.contactAboutBusiness.indexOf('Shop') !== -1)\" class=\"fa fa-home\"></span>\n" +
    "              <span ng-if=\"r.contactAboutBusiness && (r.contactBusinessType.indexOf('Bitcoin') !== -1 || r.contactAboutBusiness.indexOf('Bitcoin') !== -1)\" class=\"fa fa-bitcoin\"></span>\n" +
    "              {{r.contactBusinessType}}\n" +
    "            </td>\n" +
    "\n" +
    "            <td class=\"hint--top\" data-hint=\"{{r.contactEmail}}\">\n" +
    "              <a href=\"mailto:{{r.contactEmail}}?subject=We+choose+you!\">{{r.contactName}}</a>\n" +
    "            </td>\n" +
    "\n" +
    "            <td>\n" +
    "              {{r.contactOrganizationName}}\n" +
    "            </td>\n" +
    "\n" +
    "            <style>\n" +
    "              .cursor--phone {\n" +
    "                cursor: pointer;\n" +
    "              }\n" +
    "            </style>\n" +
    "\n" +
    "            <td>\n" +
    "              <a href=\"tel:{{r.contactPhoneNumber}}\">\n" +
    "                <input type=\"text\" class=\"cursor--phone\" ng-model=\"r.contactPhoneNumber\" ui-mask=\"(999) 999-9999\">\n" +
    "              </a>\n" +
    "            </td>\n" +
    "\n" +
    "            <td ng-if=\"r.contactReferrer.indexOf('http://') !== -1\">\n" +
    "              <a href=\"{{r.contactReferrer}}\">{{r.contactReferrer}}</a>\n" +
    "            </td>\n" +
    "\n" +
    "            <td ng-if=\"r.contactReferrer.indexOf('http://') === -1\">\n" +
    "              {{r.contactReferrer}}\n" +
    "            </td>\n" +
    "\n" +
    "          </tr>\n" +
    "\n" +
    "        </tbody>\n" +
    "\n" +
    "      </table>\n" +
    "\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/social/partials/page.html',
    "<!--\n" +
    "@fileOverview\n" +
    "\n" +
    " ____  _____  ____ _____\n" +
    "|  _ \\(____ |/ _  | ___ |\n" +
    "| |_| / ___ ( (_| | ____|\n" +
    "|  __/\\_____|\\___ |_____)\n" +
    "|_|         (_____|\n" +
    "\n" +
    "@module www/controllers/social\n" +
    "@description\n" +
    "\n" +
    "  \n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"https://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--information': true,\n" +
    "    'page--active': true\n" +
    "  }\" ng-init=\"load()\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"https://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "  </header>\n" +
    "\n" +
    "  <div itemscope itemtype=\"https://schema.org/WebPageElement\" class=\"\" role=\"main\">\n" +
    "\n" +
    "    <div site-navigation></div>\n" +
    "\n" +
    "    <!--\n" +
    "\n" +
    "                      _       _\n" +
    "                     (_)     | |\n" +
    "      ___  ___   ____ _ _____| |\n" +
    "     /___)/ _ \\ / ___) (____ | |\n" +
    "    |___ | |_| ( (___| / ___ | |\n" +
    "    (___/ \\___/ \\____)_\\_____|\\_)\n" +
    "\n" +
    "    -->\n" +
    "\n" +
    "    <section class=\"social--control\">\n" +
    "\n" +
    "      <header>\n" +
    "        <h1>{{pageSectionTitle}}</h1>\n" +
    "      </header>\n" +
    "\n" +
    "      <section class=\"social--control__connect\" ng-init=\"loadSocialMediaProfiles()\">\n" +
    "\n" +
    "        <div class=\"intro\">\n" +
    "          <p>\n" +
    "          We recommend that you connect the following social media accounts \n" +
    "          to publish your engagement history with your fans to those sites.\n" +
    "          </p>\n" +
    "        </div>\n" +
    "\n" +
    "        <ul class=\"social-network--profile\">\n" +
    "\n" +
    "          <li>\n" +
    "            <span class=\"enabled--badge--facebook\" ng-show=\"enabledSocialProfile.facebook !== undefined || isCurrentSocial === 'facebook'\"></span>\n" +
    "            <md-button aria-label=\"Connect your Facebook profile\" ng-class=\"{\n" +
    "                'md-primary' : true,\n" +
    "                'inactive'   : enabledSocialProfile.facebook !== true,\n" +
    "                'enabled'    : enabledSocialProfile.facebook !== undefined || isCurrentSocial === 'facebook',\n" +
    "                'animated'   : facebookHover,\n" +
    "                'pulse'      : facebookHover\n" +
    "              }\" ng-mouseenter=\"facebookHover = true\" ng-mouseleave=\"facebookHover = false\" ng-click=\"!(enabledSocialProfile.facebook !== undefined || isCurrentSocial === 'facebook') && connectProfile('facebook')\">\n" +
    "              <span class=\"fa fa-facebook\"></span>\n" +
    "            </md-button>\n" +
    "          </li>\n" +
    "\n" +
    "          <li>\n" +
    "            <span class=\"enabled--badge--google\" ng-show=\"enabledSocialProfile.google !== undefined || isCurrentSocial === 'google'\"></span>\n" +
    "            <md-button aria-label=\"Connect your Google+ profile\" ng-class=\"{\n" +
    "                'md-primary' : true,\n" +
    "                'inactive'   : enabledSocialProfile.google !== true,\n" +
    "                'enabled'    : enabledSocialProfile.google !== undefined || isCurrentSocial === 'google',\n" +
    "                'animated'   : googleHover,\n" +
    "                'pulse'      : googleHover\n" +
    "              }\" ng-mouseenter=\"googleHover = true\" ng-mouseleave=\"googleHover = false\" ng-click=\"!(enabledSocialProfile.google !== undefined || isCurrentSocial === 'google') && connectProfile('google')\">\n" +
    "              <span class=\"fa fa-google-plus\"></span>\n" +
    "            </md-button>\n" +
    "          </li>\n" +
    "\n" +
    "          <li>\n" +
    "            <span class=\"enabled--badge--twitter\" ng-show=\"enabledSocialProfile.twitter !== undefined || isCurrentSocial === 'twitter'\"></span>\n" +
    "            <md-button aria-label=\"Connect your Twitter profile\" ng-class=\"{\n" +
    "                'md-primary' : true,\n" +
    "                'inactive'   : enabledSocialProfile.twitter !== true,\n" +
    "                'enabled'    : enabledSocialProfile.twitter !== undefined || isCurrentSocial === 'twitter',\n" +
    "                'animated'   : twitterHover,\n" +
    "                'pulse'      : twitterHover\n" +
    "              }\" ng-mouseenter=\"twitterHover = true\" ng-mouseleave=\"twitterHover = false\" ng-click=\"!(enabledSocialProfile.twitter !== undefined || isCurrentSocial === 'twitter') && connectProfile('twitter')\">\n" +
    "              <span class=\"fa fa-twitter\"></span>\n" +
    "            </md-button>\n" +
    "          </li>\n" +
    "\n" +
    "        </ul>\n" +
    "\n" +
    "      </section>\n" +
    "\n" +
    "      <section class=\"social--control__profile\">\n" +
    "        <form name=\"name.forms.profileForm\">\n" +
    "          <div class=\"field-row\">\n" +
    "\n" +
    "            <div class=\"field-input\"><md-input-container>\n" +
    "              <label>\n" +
    "                <span class=\"fa fa-quote-left\"></span>\n" +
    "                <span class=\"shimmy-label\">Merchant Name &bullet; <em>(Public)</em></span>\n" +
    "              </label>\n" +
    "              <input md-maxlength=\"30\" required name=\"profile.merchantPublicName\" aria-label=\"Merchant's street name or grapevine name\" ng-model=\"profile.merchantPublicName\">\n" +
    "              <div ng-messages=\"profile.merchantPublicName.$error\">\n" +
    "                <div ng-message=\"required\">This is required.</div>\n" +
    "                <div ng-message=\"md-maxlength\">The merchant's public name has to be less than 30 characters long.</div>\n" +
    "              </div>\n" +
    "            </md-input-container></div>\n" +
    "\n" +
    "            <div class=\"field-input\"><md-input-container>\n" +
    "              <label>\n" +
    "                <span class=\"fa fa-smile-o\"></span>\n" +
    "                <span class=\"shimmy-label\">Contact Person</span>\n" +
    "              </label>\n" +
    "              <input md-maxlength=\"70\" required name=\"profile.contactPerson\" aria-label=\"Número uno\" ng-model=\"profile.contactPerson\">\n" +
    "              <div ng-messages=\"profile.contactPerson.$error\">\n" +
    "                <div ng-message=\"required\">This is required.</div>\n" +
    "                <div ng-message=\"md-maxlength\">The contact person's name has to be less than 50 characters long.</div>\n" +
    "              </div>\n" +
    "            </md-input-container></div>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <div class=\"field-row\">\n" +
    "\n" +
    "            <div class=\"field-input\"><md-input-container>\n" +
    "              <label>\n" +
    "                <span class=\"fa fa-mail-forward\"></span>\n" +
    "                <span class=\"shimmy-label\">E-mail</span>\n" +
    "              </label>\n" +
    "              <input md-maxlength=\"100\" required name=\"profile.email\" ng-model=\"profile.email\" aria-label=\"Drop us a line !\">\n" +
    "              <div ng-messages=\"profile.email.$error\">\n" +
    "                <div ng-message=\"required\">This is required.</div>\n" +
    "                <div ng-message=\"md-maxlength\">The email has to be less than 100 characters long.</div>\n" +
    "              </div>\n" +
    "            </md-input-container></div>\n" +
    "\n" +
    "            <div class=\"field-input\"><md-input-container>\n" +
    "              <label>\n" +
    "                <span class=\"fa fa-phone\"></span>\n" +
    "                <span class=\"shimmy-label\">Telephone</span>\n" +
    "              </label>\n" +
    "              <input md-maxlength=\"15\" md-minlength=\"10\" required name=\"profile.primaryTelephone\" ng-model=\"profile.primaryTelephone\" aria-label=\"Mobile number?\">\n" +
    "              <div ng-messages=\"profile.primaryTelephone.$error\">\n" +
    "                <div ng-message=\"required\">This is required.</div>\n" +
    "                <div ng-message=\"md-maxlength\">The primary telephone has to be less than 15 characters long.</div>\n" +
    "              </div>\n" +
    "            </md-input-container></div>\n" +
    "\n" +
    "            <div class=\"field-input\"><md-input-container>\n" +
    "              <label>\n" +
    "                <span class=\"fa fa-link\"></span>\n" +
    "                <span class=\"shimmy-label\">Website &bullet; <em>(Public)</em></span>\n" +
    "              </label>\n" +
    "              <input md-maxlength=\"50\" required name=\"description\" ng-model=\"profile.primaryWebsite\">\n" +
    "              <div ng-messages=\"profile.primaryWebsite.$error\">\n" +
    "                <div ng-message=\"required\">This is required.</div>\n" +
    "                <div ng-message=\"md-maxlength\">The primary website has to be less than 100 characters long.</div>\n" +
    "              </div>\n" +
    "            </md-input-container></div>\n" +
    "\n" +
    "          </div>\n" +
    "        </form>\n" +
    "      </section>\n" +
    "\n" +
    "      <section class=\"social--control__details\">\n" +
    "\n" +
    "        <form name=\"forms.billingAddressForm\">\n" +
    "          <div class=\"field-row\">\n" +
    "            <div class=\"field-label\">\n" +
    "              <div class=\"field-edit\">\n" +
    "\n" +
    "                <span ng-click=\"showSaveBilling = true; fieldRowEditBillingAddress('address.billing.content')\" ng-show=\"showSaveBilling === false || showSaveBilling === undefined\" class=\"field-sub-label\">\n" +
    "                  <span class=\"hint--top hint--pull-4\" data-hint=\"Click here to edit the content below\">\n" +
    "                    <span class=\"fa fa-edit animated tada\"></span>\n" +
    "                    Edit\n" +
    "                  </span>\n" +
    "                </span>\n" +
    "\n" +
    "                <span ng-click=\"showSaveBilling = false; fieldRowEditBillingAddress('address.billing.content')\" ng-show=\"showSaveBilling === true\" class=\"field-sub-label\">\n" +
    "                  <span class=\"fa fa-save animated tada\"></span>\n" +
    "                  Save\n" +
    "                </span>\n" +
    "\n" +
    "              </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"field-input\">\n" +
    "\n" +
    "              <md-input-container flex>\n" +
    "\n" +
    "                <label for=\"address.billing\">Billing Address</label>\n" +
    "\n" +
    "                <textarea id=\"address.billing\" name=\"address.billing\" ng-readonly=\"showSaveBilling === false || showSaveBilling === undefined\" md-maxlength=\"95\" ng-class=\"{\n" +
    "                    'content-editable': address.billing.editing,\n" +
    "                    'content-updated': address.billing.contentUpdated\n" +
    "                  }\" ng-model=\"address.billing.content\"></textarea>\n" +
    "\n" +
    "              </md-input-container>\n" +
    "\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </form>\n" +
    "\n" +
    "        <form name=\"forms.shippingAddressForm\">\n" +
    "          <div class=\"field-row\">\n" +
    "            <div class=\"field-label\">\n" +
    "              <div class=\"field-edit\">\n" +
    "\n" +
    "                <span ng-show=\"showSaveShipping === false || showSaveShipping === undefined\" ng-click=\"showSaveShipping = true; fieldRowEditShippingAddress('address.shipping.content')\" class=\"field-sub-label\">\n" +
    "                  <span class=\"hint--top hint--pull-4\" data-hint=\"Click here to edit the content below\">\n" +
    "                    <span class=\"fa fa-edit animated tada\"></span>\n" +
    "                    Edit\n" +
    "                  </span>\n" +
    "                </span>\n" +
    "\n" +
    "                <span ng-click=\"showSaveShipping = false; fieldRowEditShippingAddress('address.shipping.content')\" ng-show=\"showSaveShipping === true\" class=\"field-sub-label\">\n" +
    "                  <span class=\"fa fa-save animated tada\"></span>\n" +
    "                  Save\n" +
    "                </span>\n" +
    "\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"field-input\">\n" +
    "\n" +
    "              <md-input-container flex>\n" +
    "\n" +
    "                <label for=\"address.shipping\">Shipping Address</label>\n" +
    "\n" +
    "                <textarea id=\"address.shipping\" name=\"address.shipping\" ng-readonly=\"showSaveShipping === false || showSaveShipping === undefined\" md-maxlength=\"95\" ng-class=\"{\n" +
    "                    'content-editable': address.shipping.editing,\n" +
    "                    'content-updated': address.shipping.contentUpdated\n" +
    "                  }\" ng-model=\"address.shipping.content\"></textarea>\n" +
    "\n" +
    "              </md-input-container>\n" +
    "\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </form>\n" +
    "\n" +
    "        <form name=\"forms.bioForm\">\n" +
    "          <div class=\"field-row\">\n" +
    "            <div class=\"field-label\">\n" +
    "              <div class=\"field-edit\">\n" +
    "\n" +
    "                <span ng-show=\"showSave === false || showSave === undefined\" ng-click=\"showSave = true; fieldRowEditBio('user.bio.content')\" class=\"field-sub-label\">\n" +
    "                  <span class=\"hint--top hint--pull-4\" data-hint=\"Click here to edit the content below\">\n" +
    "                    <span class=\"fa fa-edit animated tada\"></span>\n" +
    "                    Edit\n" +
    "                  </span>\n" +
    "                </span>\n" +
    "\n" +
    "                <span ng-click=\"showSave = false; fieldRowEditBio('user.bio.content')\" ng-show=\"showSave === true\" class=\"field-sub-label\">\n" +
    "                  <span class=\"fa fa-save animated tada\"></span>\n" +
    "                  Save\n" +
    "                </span>\n" +
    "\n" +
    "              </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"field-input\">\n" +
    "\n" +
    "              <md-input-container flex>\n" +
    "\n" +
    "                <label for=\"user.bio\">Bio &bullet; <span class=\"emphasis\">(Public)</span></label>\n" +
    "\n" +
    "                <textarea id=\"user.bio\" name=\"user.bio\" ng-readonly=\"showSave === false\" md-maxlength=\"140\" ng-class=\"{\n" +
    "                    'content-editable': user.bio.editing,\n" +
    "                    'content-updated': user.bio.contentUpdated\n" +
    "                  }\" ng-model=\"user.bio.content\"></textarea>\n" +
    "\n" +
    "              </md-input-container>\n" +
    "\n" +
    "            </div>\n" +
    "          </div>\n" +
    "\n" +
    "        </form>\n" +
    "\n" +
    "      </section>\n" +
    "\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/stamp-signup/partials/page.html',
    "<!--\n" +
    "  @module www/controllers/stamp-signup\n" +
    "-->\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--stamp-signup': true,\n" +
    "    'page--active': true\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "  </header>\n" +
    "\n" +
    "\n" +
    "  <div itemscope itemtype=\"http://schema.org/WebPageElement\" role=\"main\">\n" +
    "\n" +
    "    <div site-navigation></div>\n" +
    "\n" +
    "    <section ng-class=\"{\n" +
    "        'stamp-signup': true\n" +
    "      }\" ng-style=\"{\n" +
    "        'padding-top': '3vh'\n" +
    "      }\">\n" +
    "\n" +
    "      <div>\n" +
    "\n" +
    "        <form class=\"forms--baptize\" ng-submit=\"linkStampToUser(forms.baptize.alias)\">\n" +
    "\n" +
    "          <header>\n" +
    "            <h1>{{pageSectionTitle}}</h1>\n" +
    "          </header>\n" +
    "\n" +
    "          <div class=\"field-intro\">\n" +
    "\n" +
    "            <p>\n" +
    "            You must be verified before joining the LoveStamp family. If you \n" +
    "            have a Stamp serial number, enter it. Otherwise tell us why you'd \n" +
    "            like a Stamp, and we'll contact you within 48 hours.\n" +
    "            </p>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <div class=\"field-column field-column-1\">\n" +
    "\n" +
    "            <md-input-container>\n" +
    "\n" +
    "              <label for=\"stamp.alias\">\n" +
    "\n" +
    "                <span ng-if=\"!forms.baptize.alias\" class=\"fa fa-unlock-alt\"></span>\n" +
    "\n" +
    "                <span ng-if=\"forms.baptize.alias\" class=\"fa fa-lock\"></span>\n" +
    "\n" +
    "                Stamp Serial Number\n" +
    "              </label>\n" +
    "\n" +
    "              <input required id=\"baptize.alias\" name=\"baptize.alias\" class=\"baptize--alias\" ng-model=\"forms.baptize.alias\">\n" +
    "\n" +
    "              <div ng-messages=\"forms.baptize.stampAlias.$error\">\n" +
    "                <div ng-message=\"required\">Check underneath your hands.</div>\n" +
    "              </div>\n" +
    "\n" +
    "            </md-input-container>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <div class=\"field-column field-submit\">\n" +
    "            <div class=\"field-input\">\n" +
    "              <md-button>\n" +
    "                Lock and Load\n" +
    "\n" +
    "                <span class=\"fa fa-bullseye\"></span>\n" +
    "              </md-button>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "\n" +
    "        </form>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "      <form class=\"field-intro forms--signup\" ng-if=\"requested || requestTimeout\" ng-style=\"{\n" +
    "          'margin-top': '2vh'\n" +
    "        }\">\n" +
    "\n" +
    "        <header>\n" +
    "          <h1>You'll hear from us soon!</h1>\n" +
    "        </header>\n" +
    "\n" +
    "        <!--\n" +
    "           -<p>Thank you for your request. We'll contact by the time the countdown \n" +
    "           -reaches 0. If you're approved, the next time you log in you'll have \n" +
    "           -access to the full LoveStamp site and you'll be able to purchase and \n" +
    "           -fuel a stamp.</p>\n" +
    "           -->\n" +
    "\n" +
    "        <br>\n" +
    "\n" +
    "        <div count-down requested=\"requested\" request-timeout=\"requestTimeout\">\n" +
    "          <div ng-class=\"{\n" +
    "              'counter': true,\n" +
    "              'animated': true,\n" +
    "              'fade-in': true\n" +
    "            }\"></div>\n" +
    "        </div>\n" +
    "\n" +
    "      </form>\n" +
    "\n" +
    "      <form class=\"forms--signup\" ng-submit=\"createSignupForUser()\" ng-if=\"!(requested || requestTimeout)\">\n" +
    "\n" +
    "        <header>\n" +
    "          <h1>I don't have a Stamp</h1>\n" +
    "        </header>\n" +
    "\n" +
    "        <section>\n" +
    "\n" +
    "          <div class=\"field-column field-column-1\">\n" +
    "            <div class=\"field-input\">\n" +
    "\n" +
    "              <md-input-container>\n" +
    "                <label for=\"signup.contactOrganizationName\">\n" +
    "                  <span class=\"fa fa-at\"></span>\n" +
    "                  <span class=\"shimmy-label\">Merchant Name</span>\n" +
    "                </label>\n" +
    "                <md-input required id=\"signup.contactOrganizationName\" name=\"signup.contactOrganizationName\" class=\"signup--contactOrganizationName\" ng-model=\"forms.signup.contactOrganizationName\"></md-input>\n" +
    "              </md-input-container>\n" +
    "\n" +
    "              <md-input-container>\n" +
    "                <label for=\"signup.contactName\">\n" +
    "                   <span class=\"fa fa-comments-o\"></span>\n" +
    "                   <span class=\"shimmy-label\">Contact Person</span>\n" +
    "                </label>\n" +
    "                <md-input required id=\"signup.contactName\" name=\"signup.contactName\" class=\"signup--contactName\" ng-model=\"forms.signup.contactName\"></md-input>\n" +
    "              </md-input-container>\n" +
    "\n" +
    "              <md-input-container>\n" +
    "                <label for=\"signup.contactEmail\">\n" +
    "                  <span class=\"fa fa-mail-reply\"></span>\n" +
    "                  <span class=\"shimmy-label\">E-mail</span>\n" +
    "                </label>\n" +
    "                <md-input required id=\"signup.contactEmail\" name=\"signup.contactEmail\" class=\"signup--contactEmail\" ng-model=\"forms.signup.contactEmail\"></md-input>\n" +
    "              </md-input-container>\n" +
    "\n" +
    "              <md-input-container>\n" +
    "                <label for=\"signup.contactPhoneNumber\">\n" +
    "                  <span class=\"fa fa-phone\"></span>\n" +
    "                  <span class=\"shimmy-label\">Telephone</span>\n" +
    "                </label>\n" +
    "                <md-input required id=\"signup.contactPhoneNumber\" name=\"signup.contactPhoneNumber\" class=\"signup--contactPhoneNumber\" ng-model=\"forms.signup.contactPhoneNumber\"></md-input>\n" +
    "              </md-input-container>\n" +
    "\n" +
    "              <md-input-container>\n" +
    "                <label for=\"signup.contactBusinessType\">\n" +
    "                  <span class=\"fa fa-tag\"></span>\n" +
    "                  <span class=\"shimmy-label\">Type of Business</span>\n" +
    "                </label>\n" +
    "                <md-input required id=\"signup.contactBusinessType\" name=\"signup.contactBusinessType\" class=\"signup--contactBusinessType\" ng-model=\"forms.signup.contactBusinessType\"></md-input>\n" +
    "              </md-input-container>\n" +
    "\n" +
    "              <md-input-container>\n" +
    "                <label for=\"signup.contactReferrer\">\n" +
    "                  <span class=\"fa fa-exchange\"></span>\n" +
    "                  <span class=\"shimmy-label\">How did you hear about us?</span>\n" +
    "                </label>\n" +
    "                <md-input required id=\"signup.contactReferrer\" name=\"signup.contactReferrer\" class=\"signup--contactReferrer\" ng-model=\"forms.signup.contactReferrer\"></md-input>\n" +
    "              </md-input-container>\n" +
    "\n" +
    "            </div>\n" +
    "          </div>\n" +
    "\n" +
    "          <div class=\"field-column field-column-2\">\n" +
    "            <div class=\"field-input\">\n" +
    "              <textarea ng-required=\"forms.signup.contactOrganizationName\" id=\"signup--aboutBusiness\" name=\"signup--aboutBusiness\" class=\"signup--aboutBusiness\" ng-model=\"forms.signup.contactAboutBusiness\" cols=\"30\" rows=\"10\" placeholder=\"Tell us why you want to join our family\"></textarea>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "\n" +
    "          <md-divider></md-divider>\n" +
    "\n" +
    "          <br>\n" +
    "\n" +
    "          <div class=\"field-row field-note\">\n" +
    "            <!--<p>We’ll give you a call to verify your details in the 24 business hours.</p>-->\n" +
    "          </div>\n" +
    "\n" +
    "          <div class=\"field-row field-submit\">\n" +
    "            <div class=\"field-input\">\n" +
    "              <md-button ng-disabled=\"formname.$invalid\">\n" +
    "                Send Request\n" +
    "\n" +
    "                <span class=\"fa fa-paper-plane-o\"></span>\n" +
    "              </md-button>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "\n" +
    "        </section>\n" +
    "\n" +
    "      </form>\n" +
    "\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/stamps/partials/page.html',
    "<!--\n" +
    "# fileOverview\n" +
    "\n" +
    " ____  _____  ____ _____\n" +
    "|  _ \\(____ |/ _  | ___ |\n" +
    "| |_| / ___ ( (_| | ____|\n" +
    "|  __/\\_____|\\___ |_____)\n" +
    "|_|         (_____|\n" +
    "\n" +
    "@module www/controllers/stamps\n" +
    "\n" +
    "## description\n" +
    "\n" +
    "  Merchant subadministration command center for stampory subfaction.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--stamp-control': true,\n" +
    "    'page--active': true\n" +
    "  }\" ng-init=\"loadStampList()\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "  </header>\n" +
    "\n" +
    "  <div itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"\" role=\"main\">\n" +
    "\n" +
    "    <div site-navigation></div>\n" +
    "\n" +
    "    <section class=\"stamp--control\">\n" +
    "\n" +
    "      <header>\n" +
    "        <h1>{{pageSectionTitle}}</h1>\n" +
    "      </header>\n" +
    "\n" +
    "      <p class=\"intro\">\n" +
    "        Stamps must have a <label for=\"location\"><em>location</em></label> and \n" +
    "        <label for=\"hours\"><em>operating hours</em></label>. Give your stamp an \n" +
    "        <label for=\"\"><em>alias</em></label> to identify them easily, and tell \n" +
    "        your fans what they can do to get stamped.\n" +
    "      </p>\n" +
    "\n" +
    "      <aside class=\"stampdex\">\n" +
    "\n" +
    "        <header>\n" +
    "          <h1>\n" +
    "            <span class=\"fa fa-list-alt\"></span> \n" +
    "            Stampdex\n" +
    "          </h1>\n" +
    "        </header>\n" +
    "\n" +
    "        <div class=\"stamp-list\">\n" +
    "\n" +
    "          <!--\n" +
    "\n" +
    "                                            _\n" +
    "                  _                        | |\n" +
    "            ___ _| |_ _____ ____  ____   __| |_____ _   _\n" +
    "           /___|_   _|____ |    \\|  _ \\ / _  | ___ ( \\ / )\n" +
    "          |___ | | |_/ ___ | | | | |_| ( (_| | ____|) X (\n" +
    "          (___/   \\__)_____|_|_|_|  __/ \\____|_____|_/ \\_)\n" +
    "                                 |_|\n" +
    "\n" +
    "          -->\n" +
    "\n" +
    "          <form name=\"forms.stampdex\" class=\"stampdex--list-search\">\n" +
    "\n" +
    "            <!--\n" +
    "\n" +
    "             _  _\n" +
    "            | |(_)       _\n" +
    "            | | _  ___ _| |_\n" +
    "            | || |/___|_   _)\n" +
    "            | || |___ | | |_\n" +
    "             \\_)_(___/   \\__)\n" +
    "\n" +
    "            -->\n" +
    "\n" +
    "            <ul deprecated--list-stroll class=\"field-row wave\">\n" +
    "\n" +
    "              <li ng-show=\"stampList.length === 0\">\n" +
    "                You do not own any stamps.\n" +
    "              </li>\n" +
    "\n" +
    "              <li ng-repeat=\"stamp in stampList | filter:forms.stampdex.query | orderBy: 'alias'\" ng-click=\"loadDetail(stamp)\" ng-init=\"merchantAliasHref = ('/merchant/stamps/' + user.handle + '/' + stamp.merchantAlias)\">\n" +
    "\n" +
    "                <a class=\"stamp--merchantAlias\" set-url merchant-alias=\"stamp.merchantAlias\" once-href=\"merchantAliasHref\" ng-show=\"! stamp.hours.alwaysOpen && ! stamp.hours\">\n" +
    "                  {{stamp.merchantAlias||stamp.alias}}\n" +
    "                </a>\n" +
    "\n" +
    "                <a class=\"hint--bottom hint--pull-3 stamp--merchantAlias\" data-hint=\"24 hrs/day\" set-url merchant-alias=\"stamp.merchantAlias\" once-href=\"merchantAliasHref\" ng-show=\"stamp.hours.alwaysOpen\" ng-bind=\"stamp.merchantAlias||stamp.alias\"></a>\n" +
    "\n" +
    "                <a class=\"hint--bottom hint--pull-5 stamp--merchantAlias\" data-hint=\"Stamps between {{stamp.hours.start}}-{{stamp.hours.end}}\" set-url merchant-alias=\"stamp.merchantAlias\" once-href=\"merchantAliasHref\" ng-show=\"stamp.hours && stamp.hours.start && stamp.hours.end && ! stamp.hours.alwaysOpen\" ng-bind=\"stamp.merchantAlias||stamp.alias\"></a>\n" +
    "\n" +
    "                <span class=\"fa fa-square\" ng-if=\"stamp.active\"></span>\n" +
    "                <span class=\"fa fa-square-o\" ng-if=\"!stamp.active\"></span>\n" +
    "\n" +
    "              </li>\n" +
    "\n" +
    "            </ul>\n" +
    "\n" +
    "            <!--\n" +
    "\n" +
    "                                         _\n" +
    "                                        | |\n" +
    "              ___ _____ _____  ____ ____| |__\n" +
    "             /___) ___ (____ |/ ___) ___)  _ \\\n" +
    "            |___ | ____/ ___ | |  ( (___| | | |\n" +
    "            (___/|_____)_____|_|   \\____)_| |_|\n" +
    "\n" +
    "            -->\n" +
    "\n" +
    "            <div class=\"field-row\">\n" +
    "\n" +
    "              <md-input-container>\n" +
    "                <label for=\"stampdex.query\">\n" +
    "                  <span class=\"fa fa-search\"></span>\n" +
    "                  <span class=\"shimmy-label\">Search your stamps</span>\n" +
    "                </label>\n" +
    "                <md-input id=\"stampdex.query\" name=\"stampdex.query\" class=\"stampdex--query\" ng-model=\"forms.stampdex.query\" aria-label=\"Try using syntactical gestures with –s or /s and symbols to organize your Stampdex.\" unused--autofocus></md-input>\n" +
    "              </md-input-container>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "          </form>\n" +
    "        </div>\n" +
    "\n" +
    "        <!--\n" +
    "\n" +
    "               _       _\n" +
    "              (_)     (_)       _\n" +
    "         ____  _ ____  _  ___ _| |_ ___   ____ _____\n" +
    "        |    \\| |  _ \\| |/___|_   _) _ \\ / ___) ___ |\n" +
    "        | | | | | | | | |___ | | || |_| | |   | ____|\n" +
    "        |_|_|_|_|_| |_|_(___/   \\__)___/|_|   |_____)\n" +
    "\n" +
    "        -->\n" +
    "\n" +
    "        <nav mini-store class=\"additional-actions\">\n" +
    "          <ul current-stamp>\n" +
    "            <li>\n" +
    "              <form name=\"miniStore\" aria-label=\"We like to leave aliases in fortune cookies\">\n" +
    "                <md-input-container>\n" +
    "                  <md-input required id=\"stampdex--current-stamp\" name=\"stampdex--current-stamp\" ng-model=\"getCurrentStamp\" ng-blur=\"miniStore.$activated = false\" aria-label=\"Add your new alias to register your stamp\" custom-enter=\"stampAdd()\"></md-input>\n" +
    "                </md-input-container>\n" +
    "              </form>\n" +
    "            </li>\n" +
    "            <li>\n" +
    "              <md-button class=\"md-primary stampdex--button__add\" aria-label=\"Lock and load another Stamp\" ng-click=\"miniStore.$activated = true\">\n" +
    "                <span class=\"fa fa-thumbs-o-up is-dirty\"></span>\n" +
    "                <span class=\"fa fa-plus\"></span>\n" +
    "                <span ng-show=\"miniStore.$activated === undefined || miniStore.$activated === false\">Register a Stamp</span>\n" +
    "                <span ng-show=\"miniStore.$activated === true\">Enter Serial Number</span>\n" +
    "              </md-button>\n" +
    "            </li>\n" +
    "          </ul>\n" +
    "        </nav>\n" +
    "\n" +
    "      </aside>\n" +
    "\n" +
    "      <!--\n" +
    "\n" +
    "       _           _          ___\n" +
    "      | |         (_)        / __)\n" +
    "      | |__   ____ _ _____ _| |__\n" +
    "      |  _ \\ / ___) | ___ (_   __)\n" +
    "      | |_) ) |   | | ____| | |\n" +
    "      |____/|_|   |_|_____) |_|\n" +
    "\n" +
    "      -->\n" +
    "\n" +
    "      <section class=\"stamp-detail stamp-detail--unloaded\" ng-if=\"! stampControlList.length\" aria-label=\"No stamp loaded\">\n" +
    "        <md-divider></md-divider>\n" +
    "        <br>\n" +
    "        <br>\n" +
    "        <br>\n" +
    "        <span class=\"fa fa-minus-square-o\" aria-hidden=\"true\"></span>\n" +
    "        <br>\n" +
    "        <br>\n" +
    "        <br>\n" +
    "        <md-divider></md-divider>\n" +
    "      </section>\n" +
    "\n" +
    "      <section product-brief ng-if=\"stampControlList.length\" ng-class=\"{\n" +
    "          'stamp-detail': true\n" +
    "        }\">\n" +
    "        <md-divider></md-divider>\n" +
    "        <ul class=\"stamp-detail--container\">\n" +
    "\n" +
    "          <li ng-repeat=\"stamp in stampControlList\" class=\"stamp-detail--listing\">\n" +
    "            <form ng-submit=\"updateStamp(stamp)\">\n" +
    "\n" +
    "              <dl class=\"stamp-detail--control-stamp\">\n" +
    "\n" +
    "                <dt class=\"stamp--alias\">\n" +
    "                  <span class=\"stamp--alias__label\">Current Stamp</span>\n" +
    "                  <span class=\"sep fa fa-long-arrow-right\"></span>\n" +
    "                  <span class=\"stamp--alias__value\" ng-bind=\"(stamp.merchantAlias||stamp.alias)\"></span>\n" +
    "                  <hr>\n" +
    "                </dt>\n" +
    "\n" +
    "                <dd class=\"stamp--status\">\n" +
    "                  <div class=\"field-block\">\n" +
    "                    <label>Status</label>\n" +
    "\n" +
    "                    <md-switch aria-label=\"On / Off\" ng-init=\"stampId = ('stamp--live__' + stamp.$id)\" once-id=\"stampId\" ng-model=\"stamp.active\"></md-switch>\n" +
    "\n" +
    "                    <p ng-show=\"stamp.active\" class=\"field-annotation\">\n" +
    "                      <em>Slide left to deactivate your stamp.</em>\n" +
    "                    </p>\n" +
    "\n" +
    "                    <p ng-show=\"!stamp.active\" class=\"field-annotation\">\n" +
    "                      <em>Slide right to activate your stamp.</em>\n" +
    "                    </p>\n" +
    "                  </div>\n" +
    "\n" +
    "                  <div class=\"field-block\">\n" +
    "\n" +
    "                    <ul class=\"field-slider field-slider--headers\">\n" +
    "                      <li ng-style=\"{\n" +
    "                            'text-indent': '-3rem'\n" +
    "                        }\">Birth</li>\n" +
    "                      <li>Registered</li>\n" +
    "                      <li ng-style=\"{\n" +
    "                            'text-indent': '3rem'\n" +
    "                        }\">1<sup>st</sup> Impression</li>\n" +
    "                    </ul>\n" +
    "\n" +
    "                    <md-slider ng-model=\"stamp.statusSaturation\" ng-disabled=\"true\" aria-disabled=\"true\" step=\"1\" md-discrete min=\"1\" max=\"3\" aria-label=\"Stamp Activation Progress\"></md-slider>\n" +
    "\n" +
    "                    <ul class=\"field-slider field-slider--captions\">\n" +
    "                      <li>\n" +
    "                        <span aria-hidden=\"true\" class=\"fa fa-arrow-circle-o-right\">\n" +
    "                          <br>\n" +
    "                        </span>\n" +
    "                        <div style=\"text-indent: -4ch\">{{stamp.birthDate}}</div>\n" +
    "                      </li>\n" +
    "                      <li>\n" +
    "                        <span aria-hidden=\"true\" class=\"fa fa-send\"></span>\n" +
    "                        <div style=\"text-indent: 0\">{{stamp.registeredDate}}</div>\n" +
    "                      </li>\n" +
    "                      <li>\n" +
    "                        <span aria-hidden=\"true\" class=\"fa fa-toggle-right\"></span>\n" +
    "                      <div style=\"text-indent: 5rem\">{{stamp.firstImpressionDate}}</div>\n" +
    "                      </li>\n" +
    "                    </ul>\n" +
    "\n" +
    "                  </div>\n" +
    "\n" +
    "                </dd>\n" +
    "\n" +
    "                <!--\n" +
    "\n" +
    "                 _     _\n" +
    "                (_)   | |\n" +
    "                 _  __| |\n" +
    "                | |/ _  |\n" +
    "                | ( (_| |\n" +
    "                |_|\\____|\n" +
    "\n" +
    "                -->\n" +
    "\n" +
    "                <dd class=\"stamp--id\" id=\"stamp--id__{{stamp.$id}}\">\n" +
    "                  <div class=\"field-block\">\n" +
    "                    <div class=\"field-row\">\n" +
    "                      <div class=\"field-input\">\n" +
    "                        <md-input-container>\n" +
    "                          <label for=\"stamp--id__{{stamp.$id}}\">\n" +
    "                            <span class=\"fa fa-lock\"> </span>\n" +
    "                            <span class=\"shimmy-label\">ID</span>\n" +
    "                          </label>\n" +
    "                          <md-input required id=\"stamp--id__{{stamp.$id}}\" name=\"stamp--id__{{stamp.$id}}\" class=\"stamp--id\" ng-readonly=\"true\" aria-label=\"Our in house signature\" ng-model=\"stamp.alias\"></md-input>\n" +
    "                        </md-input-container>\n" +
    "                      </div>\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "                </dd>\n" +
    "\n" +
    "                <!--\n" +
    "                       _  _                           _\n" +
    "                      | |(_)                         (_)                    _\n" +
    "                 _____| | _ _____  ___                _ ____  ____  _   _ _| |_\n" +
    "                (____ | || (____ |/___)              | |  _ \\|  _ \\| | | (_   _)\n" +
    "                / ___ | || / ___ |___ |______ _______| | | | | |_| | |_| | | |_\n" +
    "                \\_____|\\_)_\\_____(___(_______|_______)_|_| |_|  __/|____/   \\__)\n" +
    "                                                             |_|\n" +
    "                -->\n" +
    "\n" +
    "                <dd class=\"stamp--alias__input\">\n" +
    "\n" +
    "                  <div class=\"field-block\">\n" +
    "                    <div class=\"field-row\">\n" +
    "                      <div class=\"field-input\">\n" +
    "                        <md-input-container>\n" +
    "                          <label for=\"stamp--alias__{{stamp.$id}}\">\n" +
    "                            <span class=\"fa fa-bookmark\"> </span>\n" +
    "                            <span class=\"shimmy-label\">Stamp Alias</span>\n" +
    "                          </label>\n" +
    "                          <md-input id=\"stamp--alias__{{stamp.$id}}\" name=\"stamp--alias__{{stamp.$id}}\" class=\"stamp--alias__input\" ng-model=\"stamp.merchantAlias\" aria-label=\"Choose an alias for this Stamp\" placeholder=\"Suggestions: &quot;Stampy&quot;, &quot;Shop1&quot;, &quot;Carl&quot;\"></md-input>\n" +
    "                        </md-input-container>\n" +
    "                      </div>\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "\n" +
    "                </dd>\n" +
    "\n" +
    "                <!--\n" +
    "\n" +
    "                    ___\n" +
    "                   / __)\n" +
    "                 _| |__ ___   ____ ____\n" +
    "                (_   __) _ \\ / ___)    \\\n" +
    "                  | | | |_| | |   | | | |\n" +
    "                  |_|  \\___/|_|   |_|_|_|\n" +
    "\n" +
    "                -->\n" +
    "\n" +
    "                <dd class=\"stamp--form\" id=\"stamp--details__{{stamp.$id}}\">\n" +
    "\n" +
    "                  <div class=\"field-block field-block--address\">\n" +
    "\n" +
    "                    <!--\n" +
    "\n" +
    "                         _          ___             _\n" +
    "                        | |        / __)           | |  _\n" +
    "                      __| |_____ _| |__ _____ _   _| |_| |_\n" +
    "                     / _  | ___ (_   __|____ | | | | (_   _)\n" +
    "                    ( (_| | ____| | |  / ___ | |_| | | | |_\n" +
    "                     \\____|_____) |_|  \\_____|____/ \\_) \\__)\n" +
    "\n" +
    "                               _     _\n" +
    "                              | |   | |\n" +
    "                     _____  __| | __| | ____ _____  ___  ___\n" +
    "                    (____ |/ _  |/ _  |/ ___) ___ |/___)/___)\n" +
    "                    / ___ ( (_| ( (_| | |   | ____|___ |___ |\n" +
    "                    \\_____|\\____|\\____|_|   |_____|___/(___/\n" +
    "\n" +
    "                    -->\n" +
    "\n" +
    "                    <div class=\"field-row\">\n" +
    "                      <md-input-container>\n" +
    "\n" +
    "                        <label for=\"stamp-default-address\">\n" +
    "                          <span class=\"fa fa-map-marker\"></span>\n" +
    "                          <span class=\"shimmy-label\">\n" +
    "                              Use \n" +
    "                              <em>Same Details As</em> &mdash;\n" +
    "                              Location / Activity / Opening Hours (Enter Stamp Alias)\n" +
    "                          </span>\n" +
    "                        </label>\n" +
    "\n" +
    "                        <md-input id=\"stamp-default-address\" name=\"stamp-defaultAddress\" ng-model=\"stamp.defaultAddress\" ng-change=\"addressLookup()\" placeholder=\"Link this Stamp's address to another's current address\" aria-label=\"Link this Stamp's address to another's current address\"></md-input>\n" +
    "\n" +
    "                      </md-input-container>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <!--\n" +
    "\n" +
    "                     _  _                ___\n" +
    "                    | |(_)              (___)\n" +
    "                    | | _ ____  _____      _\n" +
    "                    | || |  _ \\| ___ |    | |\n" +
    "                    | || | | | | ____|   _| |_\n" +
    "                     \\_)_|_| |_|_____)  (_____)\n" +
    "\n" +
    "                    -->\n" +
    "\n" +
    "                    <div class=\"field-row field-address--line-1\">\n" +
    "                      <div class=\"field-input\">\n" +
    "                        <md-input-container>\n" +
    "                          <label for=\"stamp-address--line-1__{{stamp.$id}}\">\n" +
    "                            <span class=\"fa fa-home\"> </span>\n" +
    "                            <span class=\"shimmy-label\">Address Line 1</span>\n" +
    "                          </label>\n" +
    "                          <md-input ng-attr-required=\"!stamp.defaultAddress\" id=\"stamp--address--line-1__{{stamp.$id}}\" name=\"stamp--address--line-1__{{stamp.$id}}\" ng-readonly=\"stamp.defaultAddress && updatedStampDefault\" aria-label=\"Primary address line\" ng-model=\"stamp.address.line1\">\n" +
    "                        </md-input-container>\n" +
    "                      </div>\n" +
    "                    </div><!-- .field-row -->\n" +
    "\n" +
    "                    <!--\n" +
    "\n" +
    "                           _\n" +
    "                          (_)  _\n" +
    "                      ____ _ _| |_ _   _\n" +
    "                     / ___) (_   _) | | |\n" +
    "                    ( (___| | | |_| |_| |\n" +
    "                     \\____)_|  \\__)\\__  |\n" +
    "                                  (____/\n" +
    "\n" +
    "                    -->\n" +
    "\n" +
    "                    <div class=\"field-row field-city\">\n" +
    "                      <div class=\"field-input\">\n" +
    "                        <md-input-container>\n" +
    "                          <label for=\"stamp-address--city__{{stamp.$id}}\">\n" +
    "                            <span class=\"fa fa-puzzle-piece\"> </span>\n" +
    "                            <span class=\"shimmy-label\">Choose a city</span>\n" +
    "                          </label>\n" +
    "                          <md-input ng-attr-required=\"!stamp.defaultAddress\" id=\"stamp-address--city__{{stamp.$id}}\" name=\"stamp-address--city__{{stamp.$id}}\" ng-model=\"stamp.address.cityLabel\" aria-label=\"Urban or Rural Stamp?\" ng-readonly=\"stamp.defaultAddress && updatedStampDefault\"></md-input>\n" +
    "                        </md-input-container>\n" +
    "                      </div>\n" +
    "                    </div><!-- .field-row -->\n" +
    "\n" +
    "                    <!--\n" +
    "\n" +
    "                            _           _\n" +
    "                      ___ _| |_ _____ _| |_ _____\n" +
    "                     /___|_   _|____ (_   _) ___ |\n" +
    "                    |___ | | |_/ ___ | | |_| ____|\n" +
    "                    (___/   \\__)_____|  \\__)_____)\n" +
    "\n" +
    "                    -->\n" +
    "\n" +
    "                    <div class=\"field-row field-state\">\n" +
    "                      <div class=\"field-input\">\n" +
    "\n" +
    "                        <md-input-container>\n" +
    "                          <label for=\"stamp-address--state__{{stamp.$id}}\">\n" +
    "                            <span class=\"fa fa-puzzle-piece\"> </span>\n" +
    "                            <span class=\"shimmy-label\">Choose a state</span>\n" +
    "                          </label>\n" +
    "                          <md-input ng-attr-required=\"!stamp.defaultAddress\" id=\"stamp-address--state__{{stamp.$id}}\" name=\"stamp-address--state__{{stamp.$id}}\" ng-model=\"stamp.address.stateLabel\" aria-label=\"TransStamp?\" ng-readonly=\"stamp.defaultAddress && updatedStampDefault\"></md-input>\n" +
    "                        </md-input-container>\n" +
    "\n" +
    "                      </div>\n" +
    "\n" +
    "                    </div>\n" +
    "\n" +
    "                    <!--\n" +
    "\n" +
    "                           _       _______          _\n" +
    "                          (_)     (_______)        | |\n" +
    "                     _____ _ ____  _       ___   __| |_____\n" +
    "                    (___  ) |  _ \\| |     / _ \\ / _  | ___ |\n" +
    "                     / __/| | |_| | |____| |_| ( (_| | ____|\n" +
    "                    (_____)_|  __/ \\______)___/ \\____|_____)\n" +
    "                            |_|\n" +
    "\n" +
    "                    -->\n" +
    "\n" +
    "                    <div class=\"field-row field-zip\">\n" +
    "                      <div class=\"field-input\">\n" +
    "                        <md-input-container>\n" +
    "                          <label for=\"stamp-address--zipCode__{{stamp.$id}}\">\n" +
    "                            <span class=\"fa fa-puzzle-piece\"> </span>\n" +
    "                            <span class=\"shimmy-label\">Zip Code</span>\n" +
    "                          </label>\n" +
    "\n" +
    "                          <md-input ng-attr-required=\"!stamp.defaultAddress\" id=\"stamp-address--zipCode__{{stamp.$id}}\" name=\"stamp-address--zipCode__{{stamp.$id}}\" ng-model=\"stamp.address.zipCode\" holdng-blur=\"loadZip(stamp.address.zipCode)\" aria-label=\"Stamp from this zip code\" ng-readonly=\"stamp.defaultAddress && updatedStampDefault\"></md-input>\n" +
    "\n" +
    "                        </md-input-container>\n" +
    "                      </div>\n" +
    "                    </div><!-- .field-row -->\n" +
    "\n" +
    "                    <!--\n" +
    "\n" +
    "                                             _\n" +
    "                      ____ ___  _   _ ____ _| |_  ____ _   _\n" +
    "                     / ___) _ \\| | | |  _ (_   _)/ ___) | | |\n" +
    "                    ( (__| |_| | |_| | | | || |_| |   | |_| |\n" +
    "                     \\____)___/|____/|_| |_| \\__)_|    \\__  |\n" +
    "                                                      (____/\n" +
    "\n" +
    "                    -->\n" +
    "\n" +
    "                    <div class=\"field-row field-country\">\n" +
    "                      <div class=\"field-input\">\n" +
    "\n" +
    "                        <md-input-container>\n" +
    "                          <label for=\"stamp-address--country__{{stamp.$id}}\">\n" +
    "                            <span class=\"fa fa-puzzle-piece\"></span>\n" +
    "                            <span class=\"shimmy-label\">Choose a country</span>\n" +
    "                          </label>\n" +
    "\n" +
    "                          <md-input ng-attr-required=\"!stamp.defaultAddress\" id=\"stamp-address--country__{{stamp.$id}}\" ng-model=\"stamp.address.countryLabel\" aria-label=\"I'ntl. Stamp!\" ng-readonly=\"stamp.defaultAddress && updatedStampDefault\"></md-input>\n" +
    "\n" +
    "                        </md-input-container>\n" +
    "\n" +
    "                      </div>\n" +
    "                    </div><!-- .field-row -->\n" +
    "\n" +
    "                  </div><!-- .field-block -->\n" +
    "\n" +
    "                  <!--\n" +
    "\n" +
    "                   _                 ___ _              \n" +
    "                  | |               / __) |         _   \n" +
    "                  | | _____ _____ _| |__| | _____ _| |_ \n" +
    "                  | || ___ (____ (_   __) || ___ (_   _)\n" +
    "                  | || ____/ ___ | | |  | || ____| | |_ \n" +
    "                   \\_)_____)_____| |_|   \\_)_____)  \\__)\n" +
    "                                                        \n" +
    "\n" +
    "                  -->\n" +
    "\n" +
    "                  <div class=\"field-block field-block--address__latLng\">\n" +
    "\n" +
    "                    <!--\n" +
    "                       -<div\n" +
    "                       -  leaflet\n" +
    "                       -  data-tap-disabled=\"true\"\n" +
    "                       -  id       = \"stamp\"\n" +
    "                       -  defaults = \"defaults\"\n" +
    "                       -  center   = \"center\"\n" +
    "                       -  height   = \"320px\"\n" +
    "                       -  width    = \"100%\"\n" +
    "                       -></div>\n" +
    "                       -->\n" +
    "\n" +
    "                  </div>\n" +
    "\n" +
    "                  <!--\n" +
    "                     -<br />\n" +
    "                     -<hr style=\"border-color: #555;\" />\n" +
    "                     -<br />\n" +
    "                     -->\n" +
    "\n" +
    "                  <!--\n" +
    "\n" +
    "                                     _       _             \n" +
    "                                 _  (_)     (_)  _         \n" +
    "                   _____  ____ _| |_ _ _   _ _ _| |_ _   _ \n" +
    "                  (____ |/ ___|_   _) | | | | (_   _) | | |\n" +
    "                  / ___ ( (___  | |_| |\\ V /| | | |_| |_| |\n" +
    "                  \\_____|\\____)  \\__)_| \\_/ |_|  \\__)\\__  |\n" +
    "                                                    (____/ \n" +
    "\n" +
    "                  -->\n" +
    "\n" +
    "                  <div class=\"field-block field-block--activity\">\n" +
    "                    <div class=\"field-row\">\n" +
    "\n" +
    "                      <md-input-container flex>\n" +
    "                        <label for=\"stamp-activity\">\n" +
    "                          <span class=\"fa fa-question-circle\"> </span>\n" +
    "                          <span class=\"shimmy-label\">Activity You'd Like Your Fans To Do To Get Stamped (140 max)</span>\n" +
    "                        </label>\n" +
    "\n" +
    "                        <textarea id=\"stamp-activity\" name=\"stamp-activity\" ng-model=\"stamp.activity\" aria-label=\"Secret hand shake?\" required md-maxlength=\"140\"></textarea>\n" +
    "\n" +
    "                      </md-input-container>\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "\n" +
    "                  <!--\n" +
    "\n" +
    "                   _     _\n" +
    "                  (_)   (_)\n" +
    "                   _______  ___  _   _  ____ ___\n" +
    "                  |  ___  |/ _ \\| | | |/ ___)___)\n" +
    "                  | |   | | |_| | |_| | |  |___ |\n" +
    "                  |_|   |_|\\___/|____/|_|  (___/\n" +
    "\n" +
    "                  -->\n" +
    "\n" +
    "                  <div class=\"field-block field-block--hours\">\n" +
    "                    <div class=\"field-row\">\n" +
    "\n" +
    "                      <div class=\"field-label\" style=\"text-align: center\">\n" +
    "                        <label for=\"stamp-hours--start__{{stamp.$id}}\">\n" +
    "                          <b>Stamp active hours</b>\n" +
    "                        </label>\n" +
    "                      </div>\n" +
    "\n" +
    "                      <div class=\"field-input field-type-inline field-type-inline-3\">\n" +
    "\n" +
    "                        <div class=\"field-wrap field-wrap--start-time\">\n" +
    "                          <md-input-container>\n" +
    "                            <label for=\"stamp-hours--start__{{stamp.$id}}\">\n" +
    "                              <span class=\"fa fa-clock-o\"> </span>\n" +
    "                              <span class=\"shimmy-label\">Start Time</span>\n" +
    "                            </label>\n" +
    "\n" +
    "                            <md-input id=\"stamp-hours--start__{{stamp.$id}}\" name=\"stamp-hours--start__{{stamp.$id}}\" type=\"date_time\" ng-attr-required=\"!stamp.hours.alwaysOpen\" ng-model=\"stamp.hours.start\" ng-disabled=\"stamp.hours.alwaysOpen\" aria-disabled=\"stamp.hours.alwaysOpen\" aria-label=\"When is this stamp on the prowl?\" placeholder=\"00:00\" ng-pattern=\"/^\\d{2}:\\d{2}$/\"></md-input>\n" +
    "\n" +
    "                          </md-input-container>\n" +
    "\n" +
    "                        </div>\n" +
    "\n" +
    "                        <div class=\"field-wrap--sep\">&amp;</div>\n" +
    "\n" +
    "                        <div class=\"field-wrap field-wrap--end-time\">\n" +
    "                          <md-input-container>\n" +
    "                            <label for=\"stamp-hours--end__{{stamp.$id}}\">\n" +
    "                              <span class=\"fa fa-history\"> </span>\n" +
    "                              <span class=\"shimmy-label\">End Time</span>\n" +
    "                            </label>\n" +
    "\n" +
    "                            <md-input id=\"stamp-hours--end__{{stamp.$id}}\" name=\"stamp-hours--end__{{stamp.$id}}\" type=\"date_time\" ng-model=\"stamp.hours.end\" ng-attr-required=\"!stamp.hours.alwaysOpen\" ng-disabled=\"stamp.hours.alwaysOpen\" aria-disabled=\"stamp.hours.alwaysOpen\" aria-label=\"Putting the stamp to bed\" placeholder=\"00:00\" ng-pattern=\"/^\\d{2}:\\d{2}$/\"></md-input>\n" +
    "\n" +
    "                          </md-input-container>\n" +
    "\n" +
    "                        </div>\n" +
    "\n" +
    "                        <div class=\"field-wrap field-wrap--timezone\">\n" +
    "\n" +
    "                          <md-select id=\"stamp-hours--time-zone__{{stamp.$id}}\" name=\"stamp-hours--time-zone__{{stamp.$id}}\" ng-attr-required=\"!stamp.hours.alwaysOpen\" ng-model=\"stamp.hours.timeZone\" role=\"zone\" ng-disabled=\"stamp.hours.alwaysOpen\" aria-disabled=\"stamp.hours.alwaysOpen\" aria-label=\"Pick a timezone\" placeholder=\"Timezone\">\n" +
    "\n" +
    "                            <md-option value=\"Central\">CST</md-option>\n" +
    "\n" +
    "                            <md-option value=\"Eastern\">EST</md-option>\n" +
    "\n" +
    "                            <md-option value=\"Mountain\">MT</md-option>\n" +
    "\n" +
    "                            <md-option value=\"Pacific\">PST</md-option>\n" +
    "\n" +
    "                          </md-select>\n" +
    "                        </div>\n" +
    "\n" +
    "                        <div class=\"field-wrap field-wrap--always\">\n" +
    "                          <div class=\"field-input\">\n" +
    "\n" +
    "                              <md-checkbox id=\"stamp--open-hours__always\" aria-label=\"Always Open\" ng-required=\"stamp.hours.end == '' && stamp.hours.start == ''\" ng-disabled=\"stamp.hours.start || stamp.hours.end\" ng-model=\"stamp.hours.alwaysOpen\"></md-checkbox>\n" +
    "\n" +
    "                          </div>\n" +
    "                          <div class=\"field-label\">\n" +
    "                            <label for=\"stamp--open-hours__always\">Or 24 hrs/day</label>\n" +
    "                          </div>\n" +
    "                        </div>\n" +
    "\n" +
    "                      </div>\n" +
    "                    </div><!-- .field-row -->\n" +
    "\n" +
    "                    <div class=\"field-row field-row--days\">\n" +
    "                      <ul>\n" +
    "                        <li>\n" +
    "                          <md-checkbox id=\"stamp--open-days__sunday\" name=\"stamp--open-days__sunday\" aria-label=\"Sunday\" ng-model=\"stamp.days.sunday\"></md-checkbox>\n" +
    "                          Sun\n" +
    "                        </li>\n" +
    "                        <li>\n" +
    "                          <md-checkbox id=\"stamp--open-days__monday\" name=\"stamp--open-days__monday\" aria-label=\"Monday\" ng-model=\"stamp.days.monday\"></md-checkbox>\n" +
    "                          Mon\n" +
    "                        </li>\n" +
    "                        <li>\n" +
    "                          <md-checkbox id=\"stamp--open-days__tuesday\" name=\"stamp--open-days__tuesday\" aria-label=\"Tuesday\" ng-model=\"stamp.days.tuesday\"></md-checkbox>\n" +
    "                          Tue\n" +
    "                        </li>\n" +
    "                        <li>\n" +
    "                          <md-checkbox id=\"stamp--open-days__wednesday\" name=\"stamp--open-days__wednesday\" aria-label=\"Wednesday\" ng-model=\"stamp.days.wednesday\"></md-checkbox>\n" +
    "                          Wed\n" +
    "                        </li>\n" +
    "                        <li>\n" +
    "                          <md-checkbox id=\"stamp--open-days__thursday\" name=\"stamp--open-days__thursday\" aria-label=\"Thursday\" ng-model=\"stamp.days.thursday\"></md-checkbox>\n" +
    "                          Thu\n" +
    "                        </li>\n" +
    "                        <li>\n" +
    "                          <md-checkbox id=\"stamp--open-days__friday\" name=\"stamp--open-days__friday\" aria-label=\"Friday\" ng-model=\"stamp.days.friday\"></md-checkbox>\n" +
    "                          Fri\n" +
    "                        </li>\n" +
    "\n" +
    "                        <li>\n" +
    "                          <md-checkbox id=\"stamp--open-days__saturday\" name=\"stamp--open-days__saturday\" aria-label=\"Saturday\" ng-model=\"stamp.days.saturday\"></md-checkbox>\n" +
    "                          Sat\n" +
    "                        </li>\n" +
    "\n" +
    "                      </ul>\n" +
    "                    </div>\n" +
    "\n" +
    "                  </div><!-- .field-block -->\n" +
    "\n" +
    "                </dd>\n" +
    "              </dl>\n" +
    "\n" +
    "              <br>\n" +
    "              <hr>\n" +
    "              <br>\n" +
    "\n" +
    "              <div ng-class=\"{\n" +
    "                  'field-block'  : true,\n" +
    "                  'field-submit' : true\n" +
    "                }\">\n" +
    "                <div data-hint=\"Update this Stamp's metadata\" class=\"hint--top\">\n" +
    "                  <md-button class=\"md-primary stamp-control--save\">\n" +
    "                    <span class=\"fa fa-magic\"></span>\n" +
    "                    Save Stamp [\n" +
    "                    <span class=\"shimmy-label\">{{stamp.merchantAlias || stamp.alias || stamp.$id}}</span>\n" +
    "                    ]\n" +
    "                  </md-button>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </form>\n" +
    "\n" +
    "          </li>\n" +
    "\n" +
    "        </ul>\n" +
    "      </section>\n" +
    "\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/modules/www/controllers/store/partials/page.html',
    "<!--\n" +
    "# fileOverview\n" +
    "\n" +
    "            _\n" +
    "      ___ _| |_ ___   ____ _____\n" +
    "     /___|_   _) _ \\ / ___) ___ |\n" +
    "    |___ | | || |_| | |   | ____|\n" +
    "    (___/   \\__)___/|_|   |_____)\n" +
    "\n" +
    "@module www/controllers/store  \n" +
    "@depends authentication/directives/userRollup  \n" +
    "\n" +
    "## @description\n" +
    "\n" +
    "Merchants are authorized to view the store after an initial request is made.  \n" +
    "Merchants are then able to purchase Stamps or Impressions.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section id=\"top\" itemscope itemtype=\"http://schema.org/WebPage\" ng-show=\"loadingStoreOrDashboard\" ng-class=\"{\n" +
    "    'page--get-stamp': true,\n" +
    "    'page--active': loadingSubpage\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "    <h1>\n" +
    "      <a ui-sref=\"dashboard\">LoveStamp</a>\n" +
    "    </h1>\n" +
    "  </header>\n" +
    "\n" +
    "  <div itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"\" role=\"main\">\n" +
    "\n" +
    "    <div site-navigation></div>\n" +
    "\n" +
    "    <!--\n" +
    "\n" +
    "             ___    ___\n" +
    "            / __)  / __)\n" +
    "      ___ _| |__ _| |__ _____  ____ ___\n" +
    "     / _ (_   __|_   __) ___ |/ ___)___)\n" +
    "    | |_| || |    | |  | ____| |  |___ |\n" +
    "     \\___/ |_|    |_|  |_____)_|  (___/\n" +
    "\n" +
    "    -->\n" +
    "\n" +
    "    <section class=\"get-a-stamp--plans\">\n" +
    "\n" +
    "      <header>\n" +
    "        <h1>{{stampPlanTitle}}</h1>\n" +
    "      </header>\n" +
    "\n" +
    "      <p class=\"intro\">\n" +
    "        Allow 2 weeks for stamp delivery. Fuel, a.k.a. stamp impressions, is \n" +
    "        available as soon as you buy it. Only fuel up if you have a stamp, \n" +
    "        because the expiry date is calculated from the purchase date.\n" +
    "      </p>\n" +
    "\n" +
    "      <ul ng-init=\"initExchangePrices()\">\n" +
    "\n" +
    "        <li ng-repeat=\"stampPlan in stampPlansList\">\n" +
    "\n" +
    "          <dl>\n" +
    "\n" +
    "            <dt>\n" +
    "              <h3>{{stampPlan.type}} {{stampPlan.impressionsLabel}}</h3>\n" +
    "            </dt>\n" +
    "\n" +
    "            <dd class=\"corollary--price-point\">\n" +
    "              <div class=\"stamp-plan--price-point ~hint--bottom\">\n" +
    "                <ul ng-init=\"loadExchangeRateData()\">\n" +
    "                  <li ng-repeat=\"quantity in stampPlan.addToCart.options\" ng-show=\"stampPlan.show == quantity\">\n" +
    "                    <span usd-price-convert class=\"stamp-plan--price-point__cost\" quantity=\"quantity\" stamp-plan=\"stampPlan\" stamp-plans=\"stampPlans\" usd-currency=\"stampPlan.currency\" current-stamp-plan=\"currentStampPlan\" store-house-stamp=\"storeHouseStamp\">\n" +
    "                    </span>\n" +
    "                  </li>\n" +
    "                </ul>\n" +
    "              </div>\n" +
    "            </dd>\n" +
    "\n" +
    "            <dd class=\"corollary--notes\">\n" +
    "              <div class=\"stamp-plan--notes\">\n" +
    "                <ul>\n" +
    "                  <li ng-repeat=\"note in stampPlan.notes\">\n" +
    "                    <p>{{note}}</p>\n" +
    "                  </li>\n" +
    "                </ul>\n" +
    "              </div>\n" +
    "            </dd>\n" +
    "\n" +
    "            <dd class=\"corollary--cart\">\n" +
    "              <div class=\"stamp-plan--add-to-cart\">\n" +
    "\n" +
    "                <md-select ng-model=\"forms.stampAsPlanForm[stampPlan.label]\" role=\"cart\" placeholder=\"Buy Now\" id=\"stamp-plan--add-to-cart-quantity\" name=\"stamp-plan--add-to-cart-quantity\" class=\"stamp-plan--add-to-cart-quantity\">\n" +
    "\n" +
    "                  <md-optgroup label=\"{{stampPlan.label}}\">\n" +
    "                    <md-option ng-value=\"quantity\" ng-repeat=\"quantity in stampPlan.addToCart.options\" click-monad ng-click=\"initializedQuantity(stampPlan, quantity); showStripePaymentForm($event, stampPlan, quantity)\">\n" +
    "                      {{quantity}}\n" +
    "                    </md-option>\n" +
    "                  \n" +
    "\n" +
    "                </md-optgroup></md-select>\n" +
    "\n" +
    "              </div>\n" +
    "            </dd>\n" +
    "\n" +
    "          </dl>\n" +
    "\n" +
    "        </li>\n" +
    "      </ul>\n" +
    "    </section>\n" +
    "\n" +
    "  </div><!-- [role=\"main\"] -->\n" +
    "\n" +
    "  <footer>\n" +
    "    <div ng-include=\"layoutSections.footerUrl\"></div>\n" +
    "  </footer>\n" +
    "\n" +
    "</section>"
  );

}]);
