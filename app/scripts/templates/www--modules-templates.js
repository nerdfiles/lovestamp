angular.module('www_modules').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/scripts/modules/www/partials/static-stats.html',
    "<div class=\"monad--graph graph--static-stats\"></div>"
  );


  $templateCache.put('app/scripts/modules/www/partials/treemap-frequency-stats.html',
    "<div ng-class=\"{\n" +
    "    'monad--graph'                   : true,\n" +
    "    'graph--treemap-frequency-graph' : true,\n" +
    "    'animated'                       : true,\n" +
    "    'fade-in'                        : true\n" +
    "  }\"></div>"
  );

}]);
