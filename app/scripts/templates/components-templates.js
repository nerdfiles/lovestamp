angular.module('components').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/scripts/components/snowshoestamp/partials/base.html',
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--active': true,\n" +
    "    'page--stamp': true\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "  </header>\n" +
    "\n" +
    "  <style>\n" +
    "  footer a:before { border-width: 2px !important; }\n" +
    "  </style>\n" +
    "  <footer>\n" +
    "    <a click-monad ng-click=\"loadHome()\" class=\"button md-primary\" style=\"margin-top: 1.75ch; margin-left: -0.15%\">\n" +
    "      <span class=\"fa fa-close\"></span>\n" +
    "    </a>\n" +
    "  </footer>\n" +
    "\n" +
    "  <h1>Stamp Here</h1>\n" +
    "\n" +
    "  <div ng-stamp ng-class=\"{\n" +
    "      'animated'       : true,\n" +
    "      'bounce-in-left' : !stampIdent,\n" +
    "      'stamp--touched' : stamped\n" +
    "    }\" appkey=\"appKey\" stampident=\"false\" username=\"username\" stamp-passes=\"stampPasses\" stamp-receipt=\"stampReceipt\">\n" +
    "    <span class=\"stamp--loader\">\n" +
    "      <span class=\"fa fa-spinner fa-spin\"></span>\n" +
    "    </span>\n" +
    "  </div>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/components/snowshoestamp/partials/error.html',
    "<!--*\n" +
    "# fileOverview\n" +
    "\n" +
    " _____  ____ ____ ___   ____\n" +
    "| ___ |/ ___) ___) _ \\ / ___)\n" +
    "| ____| |  | |  | |_| | |\n" +
    "|_____)_|  |_|   \\___/|_|\n" +
    "\n" +
    "@module plugins/snowshoestamp/partials  \n" +
    "\n" +
    "## description\n" +
    "\n" +
    "Error page for stampening validation.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--active': isActive,\n" +
    "    '~page--inactive': ! isActive,\n" +
    "    'page--stamp__error': true\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <div class=\"page-container\">\n" +
    "\n" +
    "    <div class=\"page-divider\">\n" +
    "\n" +
    "      <ul>\n" +
    "\n" +
    "        <li class=\"changetip--default-header\">\n" +
    "\n" +
    "          <div class=\"page-anchor\">\n" +
    "\n" +
    "            <md-button class=\"back\" aria-label=\"Back to Map\" click-monad ng-click=\"loadHome()\">\n" +
    "              <div class=\"inner\">\n" +
    "                <span class=\"fa fa-close\"></span>\n" +
    "              </div>\n" +
    "            </md-button>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "          <div class=\"inner\">\n" +
    "            <h1 style=\"font-size: 3rem;\n" +
    "                       line-height: 1;\n" +
    "                       font-weight: 600;\n" +
    "                       margin: 0 0 .67em 0 !important\">Oops!</h1>\n" +
    "            <p style=\"font-size: 1rem; font-weight: 600\">There was a problem.</p>\n" +
    "          </div>\n" +
    "\n" +
    "        </li>\n" +
    "\n" +
    "        <li class=\"changetip--guide\">\n" +
    "\n" +
    "          <div class=\"inner\">\n" +
    "            <figure class=\"alert\">\n" +
    "              <span></span>\n" +
    "            </figure>\n" +
    "\n" +
    "            <div style=\"position: relative;\n" +
    "              top: 2rem;\n" +
    "              width: 120%;\n" +
    "              left: -10%\">\n" +
    "            <p>The stamp may be  unregistered, out of hours, unrecognized, or de-activated.</p>\n" +
    "            <br>\n" +
    "            <p>If the problem persists, please <a ng-click=\"openContact()\">contact us</a>.</p>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "\n" +
    "        </li>\n" +
    "\n" +
    "        <li class=\"redirect\">\n" +
    "\n" +
    "          <md-button ng-click=\"redirect('/stamp')\">\n" +
    "            <div class=\"field-column\">\n" +
    "              <span class=\"label\">Try Again</span>\n" +
    "            </div>\n" +
    "          </md-button>\n" +
    "\n" +
    "        </li>\n" +
    "\n" +
    "      </ul>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "  <!--\n" +
    "     -<footer>\n" +
    "     -</footer>\n" +
    "     -->\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/components/snowshoestamp/partials/help.html',
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--active': isActive,\n" +
    "    '~page--inactive': ! isActive,\n" +
    "    'page--stamp__help': true\n" +
    "  }\">\n" +
    "\n" +
    "  <!--*\n" +
    "  @fileOverview\n" +
    "\n" +
    "   _           _\n" +
    "  | |         | |\n" +
    "  | |__  _____| | ____\n" +
    "  |  _ \\| ___ | ||  _ \\\n" +
    "  | | | | ____| || |_| |\n" +
    "  |_| |_|_____)\\_)  __/\n" +
    "                 |_|\n" +
    "\n" +
    "  @module plugins/snowshoestamp/partials\n" +
    "\n" +
    "  @description\n" +
    "\n" +
    "    Help page for SSS.\n" +
    "\n" +
    "  -->\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <div class=\"page-container\">\n" +
    "\n" +
    "    <div class=\"page-divider\">\n" +
    "\n" +
    "      <div class=\"page-column page-column-1\">\n" +
    "        <md-button class=\"back md-button md-primary\" ui-sref=\"home\">\n" +
    "          <div class=\"inner\">\n" +
    "            <span class=\"fa fa-angle-left\"></span>\n" +
    "            <span class=\"label\">Home</span>\n" +
    "          </div>\n" +
    "        </md-button>\n" +
    "      </div><!-- .page-column-1 -->\n" +
    "\n" +
    "      <div class=\"page-column page-column-2\">\n" +
    "        <ul>\n" +
    "\n" +
    "          <li class=\"push-notifications\">\n" +
    "            <h1>\n" +
    "              <span class=\"fa fa-question\"></span>\n" +
    "            </h1>\n" +
    "          </li>\n" +
    "\n" +
    "          <li class=\"product-tour\">\n" +
    "            <h2>How does LoveStamp work?</h2>\n" +
    "            <p>\n" +
    "              LoveStamp rewards you with random bits when your mobile device is stamped by \n" +
    "              a participating merchant. A \"bit\" is a fraction of a bitcoin that LoveStamp gives \n" +
    "              you via changetip.com. After stamping, we'll post on our social media page \n" +
    "              mentioning <span class=\"social-callout\">@you</span> and <span class=\"social-callout\">@stamper</span>.\n" +
    "            </p>\n" +
    "          </li>\n" +
    "\n" +
    "          <li class=\"log-out\">\n" +
    "            <h2>What can I do with bits?</h2>\n" +
    "            <p>\n" +
    "              You can read all about it here. In brief, you can tip people over social media \n" +
    "              by e-mail, text message, and more to show your appreciation or pay for something. \n" +
    "              If you've accumulated enough, you can redeem them for items from over 100k+ \n" +
    "              merchants.\n" +
    "            </p>\n" +
    "          </li>\n" +
    "\n" +
    "        </ul>\n" +
    "      </div><!-- .page-column-2 -->\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "  <!--\n" +
    "     -<footer>\n" +
    "     -</footer>\n" +
    "     -->\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/components/snowshoestamp/partials/id.html',
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--active': true,\n" +
    "    'page--stamp__id': true\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "  </header>\n" +
    "\n" +
    "  <footer>\n" +
    "    <a ng-click=\"addStamp()\" class=\"button md-primary\">\n" +
    "      <span class=\"fa fa-close\"></span>\n" +
    "      <!--<span class=\"label\">Close</span>-->\n" +
    "    </a>\n" +
    "  </footer>\n" +
    "\n" +
    "  <h1>Stamp Here</h1>\n" +
    "\n" +
    "  <div ng-stamp ng-class=\"{\n" +
    "      'animated'          : true,\n" +
    "      'bounce-in-left'    : !stampIdent,\n" +
    "      'stamp--touched'    : stampIdent\n" +
    "    }\" appkey=\"appKey\" stampident=\"true\" username=\"username\" stamp-passes=\"stampPasses\" stamp-receipt=\"stampReceipt\">\n" +
    "  </div>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/components/snowshoestamp/partials/pending.html',
    "<!--\n" +
    "# fileOverview\n" +
    "\n" +
    "                           _ _\n" +
    "                          | (_)\n" +
    "     ____  _____ ____   __| |_ ____   ____\n" +
    "    |  _ \\| ___ |  _ \\ / _  | |  _ \\ / _  |\n" +
    "    | |_| | ____| | | ( (_| | | | | ( (_| |\n" +
    "    |  __/|_____)_| |_|\\____|_|_| |_|\\___ |\n" +
    "    |_|                             (_____|\n" +
    "\n" +
    "@module plugins/snowshoestamp/partials  \n" +
    "\n" +
    "## description\n" +
    "\n" +
    "Pending page for stampening validation.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--active'         : isActive,\n" +
    "    '~page--inactive'      : ! isActive,\n" +
    "    'page--stamp__pending' : true\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <div class=\"page-container\">\n" +
    "\n" +
    "    <div class=\"page-divider\">\n" +
    "\n" +
    "      <ul>\n" +
    "\n" +
    "        <li class=\"bits-display-container\">\n" +
    "\n" +
    "          <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "\n" +
    "            <div class=\"page-anchor\">\n" +
    "\n" +
    "              <md-button class=\"back md-button md-primary\" aria-label=\"Back to Map\" ui-sref=\"home\">\n" +
    "                <div class=\"inner\">\n" +
    "                  <span class=\"fa fa-close\" style=\"left: .095ch;\n" +
    "                      position: relative;\n" +
    "                      top: -.05ch\"></span>\n" +
    "                </div>\n" +
    "              </md-button>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "            <div bits-display-board class=\"\" itemscope itemtype=\"http://schema.org/Event\">\n" +
    "\n" +
    "              <meta itemprop=\"validFrom\" content=\"now\">\n" +
    "              <meta itemprop=\"validThrough\" content=\"+7 days\">\n" +
    "\n" +
    "              <ul>\n" +
    "                <li class=\"display--value\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">\n" +
    "\n" +
    "                  <div bits-display-counter bits-collected-value=\"bitsCollectedValue\" itemprop=\"price\" content=\"{{bitsCollectedValue||0}}\">{{bitsCollectedValue||0}}</div>\n" +
    "\n" +
    "                  <div class=\"bits-collected-label\" style=\"font-weight: 300; font-size: 1.5rem\" itemprop=\"priceCurrency\" content=\"{{collectedCurrency}}\">{{collectedCurrency}} received</div>\n" +
    "\n" +
    "                </li>\n" +
    "              </ul>\n" +
    "            </div>\n" +
    "          </header>\n" +
    "\n" +
    "        </li>\n" +
    "\n" +
    "        <li class=\"product-tour\">\n" +
    "\n" +
    "          <h2>Hey LoveStampee!</h2>\n" +
    "\n" +
    "          <p>We noticed that you're not a ChangeTip user! Your bits are waiting for you there.</p>\n" +
    "\n" +
    "          <figure>\n" +
    "            <div class=\"monad\">\n" +
    "              <a ng-href=\"https://changetip.com\" rel-external>\n" +
    "                <img src=\"/images/300--changetip--default.png\" alt=\"Callout to sign up and claim bitcoins at ChangeTip.com\">\n" +
    "              </a>\n" +
    "            </div>\n" +
    "            <figcaption>\n" +
    "              <p class=\"callout-standard\">\n" +
    "                <a ng-href=\"https://changetip.com\" rel-external>\n" +
    "                  Sign Up and Claim them.\n" +
    "                </a>\n" +
    "              </p>\n" +
    "            </figcaption>\n" +
    "          </figure>\n" +
    "\n" +
    "        </li>\n" +
    "\n" +
    "        <li class=\"log-out\">\n" +
    "          <div class=\"inner\">\n" +
    "            <p>\n" +
    "              We chose ChangeTip to store your bits because they're the best at securing them and providing a variety of ways for you to use them. Sign up <span class=\"emphasis-time\">within 7 days</span> or they'll autodestruct.\n" +
    "            </p>\n" +
    "          </div>\n" +
    "        </li>\n" +
    "\n" +
    "      </ul>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/components/snowshoestamp/partials/recall.html',
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--active': true,\n" +
    "    'page--stamp__id': true\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "  </header>\n" +
    "\n" +
    "  <footer>\n" +
    "    <a ng-click=\"goToAdmin()\" class=\"button md-primary\">\n" +
    "      <span class=\"fa fa-close\"></span>\n" +
    "      <!--<span class=\"label\">Close</span>-->\n" +
    "    </a>\n" +
    "  </footer>\n" +
    "\n" +
    "  <h1>Stamp Here</h1>\n" +
    "\n" +
    "  <div ng-stamp ng-class=\"{\n" +
    "      'animated'          : true,\n" +
    "      'bounce-in-left'    : !stampIdent,\n" +
    "      'stamp--touched'    : stampIdent\n" +
    "    }\" appkey=\"appKey\" stampident=\"true\" username=\"username\" stamp-passes=\"stampPasses\" stamp-receipt=\"stampReceipt\">\n" +
    "  </div>\n" +
    "\n" +
    "</section>"
  );


  $templateCache.put('app/scripts/components/snowshoestamp/partials/success.html',
    "<!--\n" +
    "# fileOverview\n" +
    "\n" +
    "      ___ _   _  ____ ____ _____  ___  ___\n" +
    "     /___) | | |/ ___) ___) ___ |/___)/___)\n" +
    "    |___ | |_| ( (__( (___| ____|___ |___ |\n" +
    "    (___/|____/ \\____)____)_____|___/(___/\n" +
    "\n" +
    "@module plugins/snowshoestamp/partials  \n" +
    "\n" +
    "## description\n" +
    "\n" +
    "Success page for stampening validation.\n" +
    "\n" +
    "-->\n" +
    "\n" +
    "<section itemscope itemtype=\"http://schema.org/WebPage\" ng-class=\"{\n" +
    "    'page--active'         : isActive,\n" +
    "    '~page--inactive'      : ! isActive,\n" +
    "    'page--stamp__success' : true\n" +
    "  }\">\n" +
    "\n" +
    "  <div user-rollup auth-obj=\"authObj\" auth-data=\"authData\"></div>\n" +
    "\n" +
    "  <div class=\"page-container\">\n" +
    "\n" +
    "    <div class=\"page-divider\">\n" +
    "\n" +
    "      <ul>\n" +
    "\n" +
    "        <li class=\"bits-display-container\">\n" +
    "\n" +
    "          <header itemscope itemtype=\"http://schema.org/WebPageElement\" class=\"site-header\">\n" +
    "\n" +
    "            <div class=\"page-anchor\">\n" +
    "\n" +
    "              <md-button class=\"back\" aria-label=\"Back to Map\" click-monad ng-click=\"loadHome()\">\n" +
    "                <div class=\"inner\">\n" +
    "                  <span class=\"fa fa-close\" style=\"left: .095ch;\n" +
    "                      position: relative;\n" +
    "                      top: -.05ch\"></span>\n" +
    "                </div>\n" +
    "              </md-button>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "            <div bits-display-board class=\"\" itemscope itemtype=\"http://schema.org/Event\">\n" +
    "              <meta itemprop=\"validFrom\" content=\"now\">\n" +
    "              <meta itemprop=\"validThrough\" content=\"+7 days\">\n" +
    "              <ul>\n" +
    "                <li class=\"display--value\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">\n" +
    "\n" +
    "                  <div bits-display-counter bits-collected-value=\"bitsCollectedValue\" itemprop=\"price\" content=\"{{bitsCollectedValue||0}}\">{{bitsCollectedValue||0}}</div>\n" +
    "\n" +
    "                  <div class=\"bits-collected-label\" style=\"font-weight: 300; font-size: 1.5rem\" itemprop=\"priceCurrency\" content=\"{{collectedCurrency}}\">{{collectedCurrency}} received</div>\n" +
    "\n" +
    "                </li>\n" +
    "              </ul>\n" +
    "            </div>\n" +
    "          </header>\n" +
    "        </li>\n" +
    "\n" +
    "        <li class=\"product-tour\">\n" +
    "\n" +
    "          <figure>\n" +
    "            <div class=\"monad updated--pending-pool\">\n" +
    "              <a ui-sref=\"users-handle-profile\" title=\"Go back to Profile\">\n" +
    "                <span class=\"fa\"></span>\n" +
    "                <img src=\"{{image_path_prefix}}images/draggable--purse.png\" alt=\"Go to Profile\">\n" +
    "              </a>\n" +
    "            </div>\n" +
    "            <figcaption>\n" +
    "              <div class=\"bits-collected-value\">\n" +
    "                {{updatedPurse||\"0.00\"}}\n" +
    "              </div>\n" +
    "              <div class=\"bits-collected-label\">\n" +
    "                bits in stash\n" +
    "              </div>\n" +
    "            </figcaption>\n" +
    "          </figure>\n" +
    "\n" +
    "          <br>\n" +
    "\n" +
    "          <footer ng-click=\"loadProfilePage()\">\n" +
    "            <p>Use your bits now.</p>\n" +
    "          </footer>\n" +
    "\n" +
    "        </li>\n" +
    "\n" +
    "      </ul>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "</section>"
  );

}]);
