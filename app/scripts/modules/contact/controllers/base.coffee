###*
@fileOverview
@module contact
@description

  Contact.

###

define [
  "interface"
  "services/messaging"
], (__interface__) ->
  ContactController = ($scope, messagingService) ->

    ###*
    Form Model
    ###
    $scope.forms.contact = {}

    ###*
    Form Interaction Model
    ###
    $scope.inboxMessage = (contact) ->
      return

    return

  [
    "$scope"
    "messaging.service"
    ContactController
  ]

