
/**
@fileOverview
@module contact
@description

  Contact.
 */

(function() {
  define(["interface", "services/messaging"], function(__interface__) {
    var ContactController;
    ContactController = function($scope, messagingService) {

      /**
      Form Model
       */
      $scope.forms.contact = {};

      /**
      Form Interaction Model
       */
      $scope.inboxMessage = function(contact) {};
    };
    return ["$scope", "messaging.service", ContactController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
