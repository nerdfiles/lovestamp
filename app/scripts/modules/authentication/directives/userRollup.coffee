###
# fileOverview

                            ______       _ _
                           (_____ \     | | |
     _   _  ___ _____  ____ _____) )___ | | | _   _ ____
    | | | |/___) ___ |/ ___)  __  // _ \| | || | | |  _ \
    | |_| |___ | ____| |   | |  \ \ |_| | | || |_| | |_| |
    |____/(___/|_____)_|   |_|   |_\___/ \_)_)____/|  __/
                                                   |_|

@module authentication  

## description

Check user just got a whole lot bigger, with a bit more single responsibility 
and DOM communication.

A hypermedia UI control to expose a failover OAuth lib rendered in AngularJS 
services.

###

define [
  "interface"
  "config"
  "services/authentication"
  "services/user/track"
  "services/profile"
], (__interface__, environment) ->

  userRollup = ($rootScope, $timeout, $log, FIREBASE_URL, $firebaseAuth, $location, loginSimple, userTrack, profileService, localStorageService, $state, $window) ->
    restrict : "A"
    scope    :
      authObj  : "="
      authData : "="

    link: ($scope, element, attr) ->

      # Needful local vars.
      socialPrefix = profileService.socialPrefixesEnabled
      ref = new Firebase(FIREBASE_URL)
      authObj = $scope.authObj = $firebaseAuth(ref)
      authData = $scope.authObj.$getAuth()
      $scope.loggingIn = false
      absent = -1


      # Communicate to Concerned Controllers that we have authentication data.
      $rootScope.$emit "auth/user/roll-up",
        authObj  : authObj
        authData : authData

      $rootScope.$on("$stateChangeError", (event, toState, toParams, fromState, fromParams, error) ->
        if (error == "AUTH_REQUIRED")
          if $window.is_fan
            $location.path("fan/login")
          else if $window.is_merchant
            $location.path("merchant/login")
          else
            $location.path("admin/login")
      )

      $rootScope.authWith = (provider, captureOnly) ->
        ###
        A generic API utility to be used across all authenticating conditions.

        @param {object} provider
        @return {Promise} A Promise serviced via our authentication lib.
        ###

        loginSimple.providerLogin(provider, captureOnly).then((providerData) ->

          $scope.prefix = profileService.socialPrefixActive = socialPrefix[provider]
          $scope.provider = profileService.socialProviderActive = provider
          $scope._handle = handle = providerData.handle
          $scope.loggingIn = true

          return providerData  if captureOnly

          _provider = providerData.provider

          __prefix = if _provider == 'twitter' then '@' else if _provider == 'facebook' then 'ᶠ' else '✚'
          ___pp = __prefix + providerData[_provider].username

          profileService.getUserStatus(___pp).$loaded().then (profileData) ->

            productTourStatus = false

            if profileData and profileData.profile and profileData.profile.productTour and profileData.profile.productTour.seen
              productTourStatus = profileData.profile.productTour.seen
              isActive = profileData.active

            # Must capture last network if it exists in another profile and reorder
            if profileData.profile and !profileData.profile.networks

              _cleanedId = providerData.uid.split(':')
              cleanedId = _cleanedId[1]

              profileService.getNetworksByUserId(providerData.provider, providerData.uid).then (networks) ->
                profileService.updateProfile providerData.handle, networks

            if environment.isSubtype isnt 'fan' and !isActive and $window.is_device != true
              $state.go('store-request')
              return

            if $window.is_device
              if productTourStatus
                $timeout ->
                  if $window.is_ios
                    $state.go('home-ios')
                  else
                    $state.go('home')
                , 300
              else
                $timeout ->
                  $location.path 'fan/show/product-tour'
                , 300

            else

              if environment.isSubtype isnt 'fan'
                $state.go("dashboard")
              else

                if productTourStatus
                  $timeout ->
                    if $window.is_fan
                      if $window.is_ios
                        $state.go('home-ios')
                      else
                        $state.go('home')
                    else
                      $state.go("dashboard")
                  , 300

                else
                  $timeout ->
                    $location.path 'fan/show/product-tour'
                  , 300

          return providerData

        , (error) ->
          $scope.loggingIn = false
        )


      $rootScope.$on "auth/user/roll-up", (event, authConstruct) ->
        ###
        Everywhere this directive, authorized services will expose hypermedia 
        UI controls.

        @TODO Move to authentication service.
        @namespace auth/user/roll-up
        ###

        if !authConstruct
          return
        authData = authConstruct.authData

        if !authData
          return
        provider = $scope.provider = authData.provider

        handle = authData.handle
        profileService.socialAspectsAvailable handle

        #messagesBusService.publish 'csrfTemplateTokenizer', {
          #decorateTemplate: () ->
            #((authData) ->
              #_authData.$id
              #)(authData)
        #}

        if authData
          handle = authData[provider].username
          bio    = if profileService.socialProviderActive is 'twitter' or profileService.socialProviderActive is 'facebook' then authData[provider].cachedUserProfile.description else authData[provider].username
          #createSocialProfile = profileService(handle, bio)
        return


      $scope.$on '$stateChangeSuccess', (event, views) ->
        ###
        @namespace $stateChangeStart
        ###

        $rootScope.loadingStoreOrDashboard = true
        return


      $scope.$on '$stateChangeStart', (event, views) ->
        ###
        @namespace $stateChangeStart
        ###

        $rootScope.loadingStoreOrDashboard = false
        return


      $rootScope.authenticating = () ->
        ###
        Process Authentication State
        ###

        if ! $rootScope.user
          $rootScope.user = loginSimple.requireAuth()
          $rootScope.user.then((requiredAuthData) ->

            userTrack requiredAuthData[requiredAuthData.provider].username
          , () ->

            if $window.is_fan
              $state.go "login-fan-auth"

            if $window.is_merchant
              $state.go "login-merchant-auth"

            if $window.is_device
              $state.go "login-fan-auth"

          )

      $window.addEventListener('beforeunload', (e) ->
        localStorageService.set('provider', $scope.provider)
      )

      $window.addEventListener('unload', (e) ->
        localStorageService.set('provider', $scope.provider)
      )

      document.addEventListener('pause', () ->
        localStorageService.set('provider', $scope.provider)
      )

      $scope.$on "$locationChangeStart", (e, newVal, oldVal) ->
        ###
        Events should bubble up to tell us about our given user.
        ###

        if newVal != oldVal
          locationPath = $location.path()
          if locationPath.indexOf("logout") isnt absent

            $rootScope.authenticating()
            #if $window.is_fan
              #$state.go "login-fan-auth"

            #if $window.is_merchant
              #$state.go "login-merchant-auth"

            #if $window.is_device
              #$state.go "login-fan-auth"

        return

      $rootScope.authenticating()

      return

  deps = [
    "$rootScope"
    "$timeout"
    "$log"
    "FIREBASE_URL"
    "$firebaseAuth"
    "$location"
    "login.simple"
    "user.track"
    "profile.service"
    #"messagesBusService"
    "localStorageService"
    "$state"
    '$window'
    userRollup
  ]

  __interface__.
    directive("userRollup", deps)

  return
