###*

             _
            (_)
 ____  _____ _ ____
|    \(____ | |  _ \
| | | / ___ | | | | |
|_|_|_\_____|_|_| |_|

@legacy
@description

  Inherited from seed.

###

define [
  "interface"
  "services/authentication"
], (__interface__) ->

  ###*
  A directive that shows elements only when user is logged in.
  ###
  # hide until we process it

  # sometimes if ngCloak exists on same element, they argue, so make sure that
  # this one always runs last for reliability

  ###*
  A complex directive namespace that shows elements only when user is logged out.
  ###
  __interface__
    .directive("ngShowAuth", [
      "login.simple"
      "$timeout"

      # Register the directive to AngularJS's $compileProvider on-demand or within 
      # R.js build schema.
      (loginSimple, $timeout) ->
        isLoggedIn = undefined
        loginSimple.watch (user) ->
          isLoggedIn = !!user
          return

        return (
          restrict: "A"
          link: (scope, element) ->
            update = ->
              $timeout (->
                element.toggleClass "ng-cloak", not isLoggedIn
                return
              ), 0
              return
            element.addClass "ng-cloak"
            update()
            loginSimple.watch update, scope
            return
        )
    ])
    .directive "ngHideAuth", [
      "login.simple"
      "$timeout"

      # Register the directive to AngularJS's $compileProvider on-demand or within 
      # R.js build schema.
      (loginSimple, $timeout) ->
        isLoggedIn = undefined
        console.dir isLoggedIn
        loginSimple.watch (user) ->
          isLoggedIn = !!user
          return

        return (
          restrict: "A"
          link: ($scope, element) ->
            update = ->
              element.addClass "ng-cloak" # hide until we process it
              # sometimes if ngCloak exists on same element, they argue, so make sure that
              # this one always runs last for reliability
              $timeout (->
                element.toggleClass "ng-cloak", isLoggedIn isnt false
                return
              ), 0
              return
            update()
            loginSimple.watch update, scope
            return
        )
    ]
  return

