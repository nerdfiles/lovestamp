
/**

             _
            (_)
 ____  _____ _ ____
|    \(____ | |  _ \
| | | / ___ | | | | |
|_|_|_\_____|_|_| |_|

@legacy
@description

  Inherited from seed.
 */

(function() {
  define(["interface", "services/authentication"], function(__interface__) {

    /**
    A directive that shows elements only when user is logged in.
     */

    /**
    A complex directive namespace that shows elements only when user is logged out.
     */
    __interface__.directive("ngShowAuth", [
      "login.simple", "$timeout", function(loginSimple, $timeout) {
        var isLoggedIn;
        isLoggedIn = void 0;
        loginSimple.watch(function(user) {
          isLoggedIn = !!user;
        });
        return {
          restrict: "A",
          link: function(scope, element) {
            var update;
            update = function() {
              $timeout((function() {
                element.toggleClass("ng-cloak", !isLoggedIn);
              }), 0);
            };
            element.addClass("ng-cloak");
            update();
            loginSimple.watch(update, scope);
          }
        };
      }
    ]).directive("ngHideAuth", [
      "login.simple", "$timeout", function(loginSimple, $timeout) {
        var isLoggedIn;
        isLoggedIn = void 0;
        console.dir(isLoggedIn);
        loginSimple.watch(function(user) {
          isLoggedIn = !!user;
        });
        return {
          restrict: "A",
          link: function($scope, element) {
            var update;
            update = function() {
              element.addClass("ng-cloak");
              $timeout((function() {
                element.toggleClass("ng-cloak", isLoggedIn !== false);
              }), 0);
            };
            update();
            loginSimple.watch(update, scope);
          }
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=main.js.map
