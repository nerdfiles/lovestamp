
/*
 * fileOverview

                            ______       _ _
                           (_____ \     | | |
     _   _  ___ _____  ____ _____) )___ | | | _   _ ____
    | | | |/___) ___ |/ ___)  __  // _ \| | || | | |  _ \
    | |_| |___ | ____| |   | |  \ \ |_| | | || |_| | |_| |
    |____/(___/|_____)_|   |_|   |_\___/ \_)_)____/|  __/
                                                   |_|

@module authentication  

 *# description

Check user just got a whole lot bigger, with a bit more single responsibility 
and DOM communication.

A hypermedia UI control to expose a failover OAuth lib rendered in AngularJS 
services.
 */

(function() {
  define(["interface", "config", "services/authentication", "services/user/track", "services/profile"], function(__interface__, environment) {
    var deps, userRollup;
    userRollup = function($rootScope, $timeout, $log, FIREBASE_URL, $firebaseAuth, $location, loginSimple, userTrack, profileService, localStorageService, $state, $window) {
      return {
        restrict: "A",
        scope: {
          authObj: "=",
          authData: "="
        },
        link: function($scope, element, attr) {
          var absent, authData, authObj, ref, socialPrefix;
          socialPrefix = profileService.socialPrefixesEnabled;
          ref = new Firebase(FIREBASE_URL);
          authObj = $scope.authObj = $firebaseAuth(ref);
          authData = $scope.authObj.$getAuth();
          $scope.loggingIn = false;
          absent = -1;
          $rootScope.$emit("auth/user/roll-up", {
            authObj: authObj,
            authData: authData
          });
          $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
            if (error === "AUTH_REQUIRED") {
              if ($window.is_fan) {
                return $location.path("fan/login");
              } else if ($window.is_merchant) {
                return $location.path("merchant/login");
              } else {
                return $location.path("admin/login");
              }
            }
          });
          $rootScope.authWith = function(provider, captureOnly) {

            /*
            A generic API utility to be used across all authenticating conditions.
            
            @param {object} provider
            @return {Promise} A Promise serviced via our authentication lib.
             */
            return loginSimple.providerLogin(provider, captureOnly).then(function(providerData) {
              var handle, ___pp, __prefix, _provider;
              $scope.prefix = profileService.socialPrefixActive = socialPrefix[provider];
              $scope.provider = profileService.socialProviderActive = provider;
              $scope._handle = handle = providerData.handle;
              $scope.loggingIn = true;
              if (captureOnly) {
                return providerData;
              }
              _provider = providerData.provider;
              __prefix = _provider === 'twitter' ? '@' : _provider === 'facebook' ? 'ᶠ' : '✚';
              ___pp = __prefix + providerData[_provider].username;
              profileService.getUserStatus(___pp).$loaded().then(function(profileData) {
                var cleanedId, isActive, productTourStatus, _cleanedId;
                productTourStatus = false;
                if (profileData && profileData.profile && profileData.profile.productTour && profileData.profile.productTour.seen) {
                  productTourStatus = profileData.profile.productTour.seen;
                  isActive = profileData.active;
                }
                if (profileData.profile && !profileData.profile.networks) {
                  _cleanedId = providerData.uid.split(':');
                  cleanedId = _cleanedId[1];
                  profileService.getNetworksByUserId(providerData.provider, providerData.uid).then(function(networks) {
                    return profileService.updateProfile(providerData.handle, networks);
                  });
                }
                if (environment.isSubtype !== 'fan' && !isActive && $window.is_device !== true) {
                  $state.go('store-request');
                  return;
                }
                if ($window.is_device) {
                  if (productTourStatus) {
                    return $timeout(function() {
                      if ($window.is_ios) {
                        return $state.go('home-ios');
                      } else {
                        return $state.go('home');
                      }
                    }, 300);
                  } else {
                    return $timeout(function() {
                      return $location.path('fan/show/product-tour');
                    }, 300);
                  }
                } else {
                  if (environment.isSubtype !== 'fan') {
                    return $state.go("dashboard");
                  } else {
                    if (productTourStatus) {
                      return $timeout(function() {
                        if ($window.is_fan) {
                          if ($window.is_ios) {
                            return $state.go('home-ios');
                          } else {
                            return $state.go('home');
                          }
                        } else {
                          return $state.go("dashboard");
                        }
                      }, 300);
                    } else {
                      return $timeout(function() {
                        return $location.path('fan/show/product-tour');
                      }, 300);
                    }
                  }
                }
              });
              return providerData;
            }, function(error) {
              return $scope.loggingIn = false;
            });
          };
          $rootScope.$on("auth/user/roll-up", function(event, authConstruct) {

            /*
            Everywhere this directive, authorized services will expose hypermedia 
            UI controls.
            
            @TODO Move to authentication service.
            @namespace auth/user/roll-up
             */
            var bio, handle, provider;
            if (!authConstruct) {
              return;
            }
            authData = authConstruct.authData;
            if (!authData) {
              return;
            }
            provider = $scope.provider = authData.provider;
            handle = authData.handle;
            profileService.socialAspectsAvailable(handle);
            if (authData) {
              handle = authData[provider].username;
              bio = profileService.socialProviderActive === 'twitter' || profileService.socialProviderActive === 'facebook' ? authData[provider].cachedUserProfile.description : authData[provider].username;
            }
          });
          $scope.$on('$stateChangeSuccess', function(event, views) {

            /*
            @namespace $stateChangeStart
             */
            $rootScope.loadingStoreOrDashboard = true;
          });
          $scope.$on('$stateChangeStart', function(event, views) {

            /*
            @namespace $stateChangeStart
             */
            $rootScope.loadingStoreOrDashboard = false;
          });
          $rootScope.authenticating = function() {

            /*
            Process Authentication State
             */
            if (!$rootScope.user) {
              $rootScope.user = loginSimple.requireAuth();
              return $rootScope.user.then(function(requiredAuthData) {
                return userTrack(requiredAuthData[requiredAuthData.provider].username);
              }, function() {
                if ($window.is_fan) {
                  $state.go("login-fan-auth");
                }
                if ($window.is_merchant) {
                  $state.go("login-merchant-auth");
                }
                if ($window.is_device) {
                  return $state.go("login-fan-auth");
                }
              });
            }
          };
          $window.addEventListener('beforeunload', function(e) {
            return localStorageService.set('provider', $scope.provider);
          });
          $window.addEventListener('unload', function(e) {
            return localStorageService.set('provider', $scope.provider);
          });
          document.addEventListener('pause', function() {
            return localStorageService.set('provider', $scope.provider);
          });
          $scope.$on("$locationChangeStart", function(e, newVal, oldVal) {

            /*
            Events should bubble up to tell us about our given user.
             */
            var locationPath;
            if (newVal !== oldVal) {
              locationPath = $location.path();
              if (locationPath.indexOf("logout") !== absent) {
                $rootScope.authenticating();
              }
            }
          });
          $rootScope.authenticating();
        }
      };
    };
    deps = ["$rootScope", "$timeout", "$log", "FIREBASE_URL", "$firebaseAuth", "$location", "login.simple", "user.track", "profile.service", "localStorageService", "$state", '$window', userRollup];
    __interface__.directive("userRollup", deps);
  });

}).call(this);

//# sourceMappingURL=userRollup.js.map
