###*
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module authentication/controllers
@depends services/authentication

# description

  Basic authentication glue controller.

  Register the directive to AngularJS's $compileProvider on-demand or within 
  R.js build schema.

# note

  Essentially glue for the checkUser directive, who's doing way too much 
  work with the interface. Phatty directive?

###

define [
  "interface"
  "lodash"
  "auth/user/roll-up"
  "directives/notification"
  "directives/loader"
  "services/profile"
  "services/stamp"
  "directives/loader"
  "directives/relExternal"
  "directives/title"
  'directives/mobileLink'
], (__interface__, _) ->

  deviceGeoSettings = {}
  zoomSetting = 3

  LoginController = ($scope, $rootScope, geolocationAvailable, $timeout, $location, profileService, $window, leafletData, geolocation, localStorageService) ->

    $scope.pageSectionTitle = 'Login'
    deviceGeoSettings ?= {}

    $rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) ->
      if toState
        $window.dispatchEvent(new Event('resize'))
    )

    _.extend $scope, {
      defaults:
        #tileLayer       : 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png'
        #tileLayer       : 'https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png'
        tileLayer        : if window.is_device != true then 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png' else 'http://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}@2x.png'
        tileLayerOptions:
          opacity      : 1
          detectRetina : true
          reuseTiles   : true
        #tileLayer       : "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        keyboard        : false
        dragging        : false
        doubleClickZoom : false
        tap             : false
        zoomControl     : false
        minZoom         : zoomSetting
        maxZoom         : zoomSetting
    }

    $rootScope.$on 'geolocationAvailable', (e, geolocationAvailable) ->

      $timeout ->
        $scope.$apply () ->
          $scope.noMap = false
          $scope.showMap = true
          $window.dispatchEvent(new Event('resize'))
      , 0

      _.extend deviceGeoSettings,
        lat: geolocationAvailable.coords.latitude
        lng: geolocationAvailable.coords.longitude
        zoom: zoomSetting

    # Last Login Provider
    $scope.provider = localStorageService.get('provider') or null

    $scope.$on('$destroy', () ->
      leafletData.unresolveMap('login')
    )

    _getMap = leafletData.getMap('login')

    _getMap.then (map) ->

      $timeout ->
        $scope.$apply () ->
          $scope.map = map
      , 0

    angular.extend $scope,
      center: deviceGeoSettings

    $rootScope.userLanded = true

    # Full height maps, huzzah.
    $rootScope.mapHeight = ->
      $window.innerHeight

    return

  [
    "$scope"
    "$rootScope"
    "geolocationAvailable"
    "$timeout"
    "$location"
    "profile.service"
    "$window"
    "leafletData"
    "geolocation"
    "localStorageService"
    "notification.service"
    LoginController
  ]
