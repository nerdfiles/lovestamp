
/**
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module authentication/controllers
@depends services/authentication

 * description

  Basic authentication glue controller.

  Register the directive to AngularJS's $compileProvider on-demand or within 
  R.js build schema.

 * note

  Essentially glue for the checkUser directive, who's doing way too much 
  work with the interface. Phatty directive?
 */

(function() {
  define(["interface", "lodash", "auth/user/roll-up", "directives/notification", "directives/loader", "services/profile", "services/stamp", "directives/loader", "directives/relExternal", "directives/title", 'directives/mobileLink'], function(__interface__, _) {
    var LoginController, deviceGeoSettings, zoomSetting;
    deviceGeoSettings = {};
    zoomSetting = 3;
    LoginController = function($scope, $rootScope, geolocationAvailable, $timeout, $location, profileService, $window, leafletData, geolocation, localStorageService) {
      var _getMap;
      $scope.pageSectionTitle = 'Login';
      if (deviceGeoSettings == null) {
        deviceGeoSettings = {};
      }
      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (toState) {
          return $window.dispatchEvent(new Event('resize'));
        }
      });
      _.extend($scope, {
        defaults: {
          tileLayer: window.is_device !== true ? 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png' : 'http://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}@2x.png',
          tileLayerOptions: {
            opacity: 1,
            detectRetina: true,
            reuseTiles: true
          },
          keyboard: false,
          dragging: false,
          doubleClickZoom: false,
          tap: false,
          zoomControl: false,
          minZoom: zoomSetting,
          maxZoom: zoomSetting
        }
      });
      $rootScope.$on('geolocationAvailable', function(e, geolocationAvailable) {
        $timeout(function() {
          return $scope.$apply(function() {
            $scope.noMap = false;
            $scope.showMap = true;
            return $window.dispatchEvent(new Event('resize'));
          });
        }, 0);
        return _.extend(deviceGeoSettings, {
          lat: geolocationAvailable.coords.latitude,
          lng: geolocationAvailable.coords.longitude,
          zoom: zoomSetting
        });
      });
      $scope.provider = localStorageService.get('provider') || null;
      $scope.$on('$destroy', function() {
        return leafletData.unresolveMap('login');
      });
      _getMap = leafletData.getMap('login');
      _getMap.then(function(map) {
        return $timeout(function() {
          return $scope.$apply(function() {
            return $scope.map = map;
          });
        }, 0);
      });
      angular.extend($scope, {
        center: deviceGeoSettings
      });
      $rootScope.userLanded = true;
      $rootScope.mapHeight = function() {
        return $window.innerHeight;
      };
    };
    return ["$scope", "$rootScope", "geolocationAvailable", "$timeout", "$location", "profile.service", "$window", "leafletData", "geolocation", "localStorageService", "notification.service", LoginController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
