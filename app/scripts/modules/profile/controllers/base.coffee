###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module profile/controllers  

## description

  A module for presenting profile settings to customers/fans.

###

define [
  "interface"
  "numeral"
  'jquery'
  "bitcoin-prices"
  "auth/user/roll-up"
  "services/notification"
  "services/profile"
  "services/stamp"
  "services/bank"
  'directives/title'
  'services/sound'
  'services/browser'
], (__interface__, numeral, $, bitcoinprices) ->

  dragPlatform = ($q, $timeout) ->
    linker = ($scope, element) ->
      $timeout ->
        $scope.$emit 'vendor:ready', element
      , 750
      return
    restrict: 'A'
    link: linker

  __interface__.directive 'dragPlatform', [
    '$q'
    '$timeout'
    dragPlatform
  ]

  ProfileController = ($scope, $stateParams, $rootScope, stampService, profileService, bankService, notificationService, userRequire, $location, $timeout, $window, $q, $compile, soundService, browserService, $state, loginSimple) ->

    _userRequire = userRequire()

    # For currency display labeling.
    $scope.bitsCollectedValue = 0
    $scope.bitsCollectedCurrency = 'BTC'

    # For form configuration.
    $scope.forms ?= {}
    $scope.forms.profile =
      productTour:
        seen: null
      pushNotifications:
        status: false

    # For ARIA labeling.
    $scope.pushNotifications =
      status: null
    $scope.productTour =
      status: null

    # Language map for currency exchange rate display.
    $scope.langMap =
      'de'    : 'EUR'
      'fr'    : 'EUR'
      'en'    : 'USD'
      'es-sp' : 'EUR'
      'es-mx' : 'PES'

    $scope.lang = if $scope.langMap[$stateParams.lang] then $scope.langMap[$stateParams.lang] else $scope.bitsCollectedCurrency

    $scope.pendingPoolBitsCollected = 0

    formsProfileProductTour = (newVal, oldVal) ->
      ###
      ARIA labels for Product Tour logic.
      ###

      labelStatus = null
      ySeen = "You've seen the product tour"
      nSeen = "You have not seen the product tour"

      if newVal isnt undefined or newVal isnt null
        labelStatus = if newVal.seen then ySeen else nSeen
        $timeout ->
          $scope.$apply () ->
            $scope.productTour.seen = newVal.seen
        , 0

    # Implement Product Tour form label logic.
    $scope.$watchCollection 'forms.profile.productTour', formsProfileProductTour


    formsProfilePushNotifications = (newVal, oldVal) ->
      ###
      ARIA labels for Push Notifications logic.
      ###

      labelStatus = null

      if newVal isnt undefined or newVal isnt null
        labelStatus = if newVal.status then 'On' else 'Off'
        $timeout ->
          $scope.$apply () ->
            $scope.pushNotifications.status = labelStatus
            return
          return
        , 0
      return

    # Implement Push Notifications form label logic.
    $scope.$watchCollection 'forms.profile.pushNotifications', formsProfilePushNotifications

    $scope.centerAnchor = true

    device_image_path = if window.is_device then '.' else '/assets'

    $scope.loadHome = () ->
      t = $timeout ->
        if $window.is_ios
          $location.path('/fan-ios')
        else
          $location.path('/fan')
        $timeout.cancel t
        return
      , 250
      return


    $timeout ->
      $scope.$apply () ->
        $scope.droppableVendors = [
          {
            name     : 'BitReserve'
            id       : 'bitreserve'
            imageSrc : device_image_path + '/images/exchange--bitreserve-logo.png'
            inactive : true
            url      : 'http://google.com'
          }

          {
            name     : 'Gyft'
            id       : 'gyft'
            imageSrc : device_image_path + '/images/exchange--gyft-logo.png'
            inactive : true
            url      : 'http://google.com'
          }

          {
            name     : 'ChangeTip'
            id       : 'changetip'
            imageSrc : device_image_path + '/images/exchange--changetip-logo.png'
            inactive : false
            url      : 'http://google.com'
          }

          {
            name     : 'Points.com'
            id       : 'points-com'
            imageSrc : device_image_path + '/images/exchange--points-com-logo.png'
            inactive : true
            url      : 'http://google.com'
          }

          {
            name     : 'WoW Gold'
            id       : 'wow'
            imageSrc : device_image_path + '/images/exchange--wow-logo.png'
            inactive : true
            url      : 'http://google.com'
          }
        ]
    , 0

    $scope.draggableObjects = [
    ]

    $scope.droppedObjects1 = []

    _userRequire.then (user) ->
      return  if not user
      return  if not user and not user.handle
      _username = $scope.username = user.handle
      bankService.getPurse(_username).then (purseData) ->

        $timeout ->

          $scope.$apply () ->

            $scope.pendingPoolBitsCollected = purseData.value
            $scope.draggableObjects.push {
              name     : 'purse'
              imageSrc : device_image_path + '/images/draggable--purse.png'
              value    : parseFloat($scope.pendingPoolBitsCollected)
            }
        , 0

    $scope.cancelTransfer = () ->
      notificationService.cancelTransfer = true
      notificationService.displayNote 'Transfer canceled!'
      $scope.droppedObjects1 = []

    $scope.onPendingPoolDragComplete = (data, event) ->

      index = $scope.droppedObjects1.indexOf(data)
      if index > -1
          $scope.droppedObjects1.splice(index, 1)


    compilerCreator = (compiledHtml) ->
      def = $q.defer()
      $timeout ->

        returnString = ''
        i = 0
        while i < compiledHtml.length
          returnString += compiledHtml[i].outerHTML
          i++

        def.resolve returnString

        return
      , 0

      def.promise

    $scope.onPendingPoolDropComplete = (data, eventContainer, vendorObject) ->

      index = $scope.droppedObjects1.indexOf(data)
      notificationService.cancelTransfer = false
      absent = -1

      if vendorObject.inactive
          return notificationService.displayError 'Vendor currently unavailable!'

      if !(parseFloat($scope.pendingPoolBitsCollected) > 0)
          return notificationService.displayError 'Your purse is empty!'

      $timeout ->
        $scope.$apply ->
          $scope.droppedObjects1.push(data)

          countdownHtml = '<div class="pending-pool-count-down"><div class="countdown"></div><div class="countdown-cancel" click-monad ng-click="cancelTransfer()">Cancel</div></div>'
          $countdownHtml = $compile(countdownHtml)($scope)

          compilerCreator($countdownHtml).then((_$countdownHtml) ->

            notificationService.displayNote(_$countdownHtml, {
              hideDelay  : 3000
              scope      : $scope
              onComplete : () ->

                def = $q.defer()

                if notificationService.cancelTransfer == true
                    def.reject(null)
                    return def.promise

                if index == absent

                    if notificationService.cancelTransfer == true
                        def.reject(null)
                        return def.promise

                    _.extend data, vendorId: vendorObject.id

                    if !(parseFloat($scope.pendingPoolBitsCollected) > 0)
                        notificationService.displayError 'Your purse is empty. Please get stamped to earn bits.'
                        def.reject false
                        return

                    bankService.draggedPurse($scope.username, data).then((responseData) ->

                        if responseData.body == null
                            notificationService.displayNote 'Please try again later!'
                            def.reject false
                            return

                        _responseData = responseData.body
                        magic_url = _responseData.magic_url

                        if notificationService.cancelTransfer == true
                            $scope.droppedObjects1.push(data)
                            return def.reject(null)

                        responseBody = _responseData

                        detail = responseBody.detail

                        if detail and detail.indexOf('expired') isnt -1
                            notificationService.displayNote detail
                            def.reject false
                            return

                        if responseBody.status and responseBody.status.indexOf('Out') isnt -1
                            # In case of direct transfer
                            soundService.loadMedia($rootScope.image_path_prefix + 'sounds/gold').then((goldDrop) ->
                              goldDrop.play()
                            )
                            inAppBrowserLink = "https://www.changetip.com/login/#{provider}/"
                            $scope.pendingPoolBitsCollected = '0.00'
                            config =
                              url     : inAppBrowserLink
                              target  : "_blank"
                              options : []
                            browserService.open(config)
                            $scope.droppedObjects1.push(data)
                            def.resolve true
                            return

                        if magic_url
                            soundService.loadMedia($rootScope.image_path_prefix + 'sounds/gold').then((goldDrop) ->
                              goldDrop.play()
                            )
                            $scope.pendingPoolBitsCollected = '0.00'
                            inAppBrowserLink = magic_url
                            config =
                              url     : inAppBrowserLink
                              target  : "_blank"
                              options : []
                            browserService.open(config)
                            $scope.droppedObjects1.push(data)
                            def.resolve true
                        return

                    , (errorData) ->

                        notificationService.displayNote 'Whoops! Try again later!'
                        def.reject false

                    )

                def.promise

            }).then (transferStatus) ->
                console.log transferStatus
          )
      , 0


    $scope.getProfile = () ->
      ###
      Get profile details.
      ###

      _userRequire.then (user) ->
        return  if not user
        return  if not user and not user.handle

        username = user.handle
        profileService.getProfile(username).then (_userProfileData) ->

          _.forEach _userProfileData, (dataPoint, key) ->

            angular.extend $scope.forms.profile, dataPoint

          $timeout ->

            if parseFloat($scope.forms.profile.bitsCollected) > 0
              $scope.$apply () ->

                $scope.bitsCollectedValue = numeral(parseFloat($scope.forms.profile.bitsCollected)).format(bankService.currencyFormat)
                $scope.bitsCollectedAvailable =
                  'USD': parseFloat($scope.forms.profile.bitsCollected) / 1000000
                  'BTC': parseFloat($scope.forms.profile.bitsCollected) / 1000000
                  #'EUR': parseFloat($scope.forms.profile.bitsCollected) / 1000000

            $scope.$emit 'value', { finished: true }
          , 0
        return
      return


    loadedBtcCurrencyValue = (event, f) ->
      ###
      Use bitcoin-prices module to pull exchange rate metadata on BTC, USD, 
      EUR, and other currencies relevant to fans.
      ###

      $timeout ->
        $scope.$apply () ->
          bitcoinprices.init({

              url                  : "https://api.bitcoinaverage.com/ticker/all"
              marketRateVariable   : "24h_avg"
              defaultCurrency      : $scope.lang
              jQuery               : $
              priceAttribute       : "data-btc-price"
              priceOrignalCurrency : $scope.bitsCollectedCurrency # Converted to bits.
              symbols:
                "BTC"   : "<span class='hint--bottom' data-hint='Tap to switch currencies'><i class='fa fa-btc animated flip-in-y'></i></span>"
                "USD"   : "<span class='hint--bottom' data-hint='Tap to switch currencies'><i class='fa fa-usd animated flip-in-y'></i></span>"
                #"EUR"   : "<span class='hint--bottom' data-hint='Tap to switch currencies'><i class='fa fa-eur animated flip-in-y'></i></span>"
              currencies: [
                $scope.bitsCollectedCurrency
                "USD"
                #"EUR"
              ]
              ux:
                clickPrices             : true
                menu                    : false
                clickableCurrencySymbol : true

          })

          $scope.finishedConversionCurrency = f.finished

      , 100

    # Implement bitcoin-prices.
    $scope.$on 'value', loadedBtcCurrencyValue

    t = null

    $scope.logout = (redirectUrl) ->
      ###
      @namespace logout
      @description

        Log out.

      ###
      profileService.clearValidations()
      loginSimple.logout()
      $state.go(redirectUrl)
      return


    $scope.updateProfile = () ->
      ###
      @namespace updateProfile
      @description

        Update profile details.

      ###

      #if window.is_device and ($scope.granted == false or $scope.granted == null or $scope.forms.profile.pushNotifications.status != true)

      _userRequire.then (user) ->
        username = user.handle

        profileService.updateProfile(username, $scope.forms.profile)

        if $scope.forms.profile.pushNotifications.status
          $scope.pushNotifications.status = 'On'
        else
          $scope.pushNotifications.status = 'Off'
        #)
        return

      return


    _t = null
    $scope.showProductTour = () ->
      ###
      @namespace showProductTour
      @description

        Really a service level function. Leave it for now.

      ###

      redirectUrl = 'fan/show/product-tour'
      productTourSeen = if $scope.forms.profile.productTour.seen is null then true

      $scope.forms.profile.productTour.seen = true

      u = $scope.user
      username = u.handle

      profileService.updateProfile(username, $scope.forms.profile)
      _t = $timeout ->
        $location.path(redirectUrl)
        $timeout.cancel _t
        return
      , 250
      return

    return

  [
    "$scope"
    "$stateParams"
    "$rootScope"
    "stamp.service"
    "profile.service"
    "bank.service"
    "notification.service"
    "user.require"
    "$location"
    "$timeout"
    "$window"
    "$q"
    "$compile"
    "sound.service"
    'browser.service'
    '$state'
    'login.simple'
    ProfileController
  ]
