
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module profile/controllers  

 *# description

  A module for presenting profile settings to customers/fans.
 */

(function() {
  define(["interface", "numeral", 'jquery', "bitcoin-prices", "auth/user/roll-up", "services/notification", "services/profile", "services/stamp", "services/bank", 'directives/title', 'services/sound', 'services/browser'], function(__interface__, numeral, $, bitcoinprices) {
    var ProfileController, dragPlatform;
    dragPlatform = function($q, $timeout) {
      var linker;
      linker = function($scope, element) {
        $timeout(function() {
          return $scope.$emit('vendor:ready', element);
        }, 750);
      };
      return {
        restrict: 'A',
        link: linker
      };
    };
    __interface__.directive('dragPlatform', ['$q', '$timeout', dragPlatform]);
    ProfileController = function($scope, $stateParams, $rootScope, stampService, profileService, bankService, notificationService, userRequire, $location, $timeout, $window, $q, $compile, soundService, browserService, $state, loginSimple) {
      var compilerCreator, device_image_path, formsProfileProductTour, formsProfilePushNotifications, loadedBtcCurrencyValue, t, _t, _userRequire;
      _userRequire = userRequire();
      $scope.bitsCollectedValue = 0;
      $scope.bitsCollectedCurrency = 'BTC';
      if ($scope.forms == null) {
        $scope.forms = {};
      }
      $scope.forms.profile = {
        productTour: {
          seen: null
        },
        pushNotifications: {
          status: false
        }
      };
      $scope.pushNotifications = {
        status: null
      };
      $scope.productTour = {
        status: null
      };
      $scope.langMap = {
        'de': 'EUR',
        'fr': 'EUR',
        'en': 'USD',
        'es-sp': 'EUR',
        'es-mx': 'PES'
      };
      $scope.lang = $scope.langMap[$stateParams.lang] ? $scope.langMap[$stateParams.lang] : $scope.bitsCollectedCurrency;
      $scope.pendingPoolBitsCollected = 0;
      formsProfileProductTour = function(newVal, oldVal) {

        /*
        ARIA labels for Product Tour logic.
         */
        var labelStatus, nSeen, ySeen;
        labelStatus = null;
        ySeen = "You've seen the product tour";
        nSeen = "You have not seen the product tour";
        if (newVal !== void 0 || newVal !== null) {
          labelStatus = newVal.seen ? ySeen : nSeen;
          return $timeout(function() {
            return $scope.$apply(function() {
              return $scope.productTour.seen = newVal.seen;
            });
          }, 0);
        }
      };
      $scope.$watchCollection('forms.profile.productTour', formsProfileProductTour);
      formsProfilePushNotifications = function(newVal, oldVal) {

        /*
        ARIA labels for Push Notifications logic.
         */
        var labelStatus;
        labelStatus = null;
        if (newVal !== void 0 || newVal !== null) {
          labelStatus = newVal.status ? 'On' : 'Off';
          $timeout(function() {
            $scope.$apply(function() {
              $scope.pushNotifications.status = labelStatus;
            });
          }, 0);
        }
      };
      $scope.$watchCollection('forms.profile.pushNotifications', formsProfilePushNotifications);
      $scope.centerAnchor = true;
      device_image_path = window.is_device ? '.' : '/assets';
      $scope.loadHome = function() {
        var t;
        t = $timeout(function() {
          if ($window.is_ios) {
            $location.path('/fan-ios');
          } else {
            $location.path('/fan');
          }
          $timeout.cancel(t);
        }, 250);
      };
      $timeout(function() {
        return $scope.$apply(function() {
          return $scope.droppableVendors = [
            {
              name: 'BitReserve',
              id: 'bitreserve',
              imageSrc: device_image_path + '/images/exchange--bitreserve-logo.png',
              inactive: true,
              url: 'http://google.com'
            }, {
              name: 'Gyft',
              id: 'gyft',
              imageSrc: device_image_path + '/images/exchange--gyft-logo.png',
              inactive: true,
              url: 'http://google.com'
            }, {
              name: 'ChangeTip',
              id: 'changetip',
              imageSrc: device_image_path + '/images/exchange--changetip-logo.png',
              inactive: false,
              url: 'http://google.com'
            }, {
              name: 'Points.com',
              id: 'points-com',
              imageSrc: device_image_path + '/images/exchange--points-com-logo.png',
              inactive: true,
              url: 'http://google.com'
            }, {
              name: 'WoW Gold',
              id: 'wow',
              imageSrc: device_image_path + '/images/exchange--wow-logo.png',
              inactive: true,
              url: 'http://google.com'
            }
          ];
        });
      }, 0);
      $scope.draggableObjects = [];
      $scope.droppedObjects1 = [];
      _userRequire.then(function(user) {
        var _username;
        if (!user) {
          return;
        }
        if (!user && !user.handle) {
          return;
        }
        _username = $scope.username = user.handle;
        return bankService.getPurse(_username).then(function(purseData) {
          return $timeout(function() {
            return $scope.$apply(function() {
              $scope.pendingPoolBitsCollected = purseData.value;
              return $scope.draggableObjects.push({
                name: 'purse',
                imageSrc: device_image_path + '/images/draggable--purse.png',
                value: parseFloat($scope.pendingPoolBitsCollected)
              });
            });
          }, 0);
        });
      });
      $scope.cancelTransfer = function() {
        notificationService.cancelTransfer = true;
        notificationService.displayNote('Transfer canceled!');
        return $scope.droppedObjects1 = [];
      };
      $scope.onPendingPoolDragComplete = function(data, event) {
        var index;
        index = $scope.droppedObjects1.indexOf(data);
        if (index > -1) {
          return $scope.droppedObjects1.splice(index, 1);
        }
      };
      compilerCreator = function(compiledHtml) {
        var def;
        def = $q.defer();
        $timeout(function() {
          var i, returnString;
          returnString = '';
          i = 0;
          while (i < compiledHtml.length) {
            returnString += compiledHtml[i].outerHTML;
            i++;
          }
          def.resolve(returnString);
        }, 0);
        return def.promise;
      };
      $scope.onPendingPoolDropComplete = function(data, eventContainer, vendorObject) {
        var absent, index;
        index = $scope.droppedObjects1.indexOf(data);
        notificationService.cancelTransfer = false;
        absent = -1;
        if (vendorObject.inactive) {
          return notificationService.displayError('Vendor currently unavailable!');
        }
        if (!(parseFloat($scope.pendingPoolBitsCollected) > 0)) {
          return notificationService.displayError('Your purse is empty!');
        }
        return $timeout(function() {
          return $scope.$apply(function() {
            var $countdownHtml, countdownHtml;
            $scope.droppedObjects1.push(data);
            countdownHtml = '<div class="pending-pool-count-down"><div class="countdown"></div><div class="countdown-cancel" click-monad ng-click="cancelTransfer()">Cancel</div></div>';
            $countdownHtml = $compile(countdownHtml)($scope);
            return compilerCreator($countdownHtml).then(function(_$countdownHtml) {
              return notificationService.displayNote(_$countdownHtml, {
                hideDelay: 3000,
                scope: $scope,
                onComplete: function() {
                  var def;
                  def = $q.defer();
                  if (notificationService.cancelTransfer === true) {
                    def.reject(null);
                    return def.promise;
                  }
                  if (index === absent) {
                    if (notificationService.cancelTransfer === true) {
                      def.reject(null);
                      return def.promise;
                    }
                    _.extend(data, {
                      vendorId: vendorObject.id
                    });
                    if (!(parseFloat($scope.pendingPoolBitsCollected) > 0)) {
                      notificationService.displayError('Your purse is empty. Please get stamped to earn bits.');
                      def.reject(false);
                      return;
                    }
                    bankService.draggedPurse($scope.username, data).then(function(responseData) {
                      var config, detail, inAppBrowserLink, magic_url, responseBody, _responseData;
                      if (responseData.body === null) {
                        notificationService.displayNote('Please try again later!');
                        def.reject(false);
                        return;
                      }
                      _responseData = responseData.body;
                      magic_url = _responseData.magic_url;
                      if (notificationService.cancelTransfer === true) {
                        $scope.droppedObjects1.push(data);
                        return def.reject(null);
                      }
                      responseBody = _responseData;
                      detail = responseBody.detail;
                      if (detail && detail.indexOf('expired') !== -1) {
                        notificationService.displayNote(detail);
                        def.reject(false);
                        return;
                      }
                      if (responseBody.status && responseBody.status.indexOf('Out') !== -1) {
                        soundService.loadMedia($rootScope.image_path_prefix + 'sounds/gold').then(function(goldDrop) {
                          return goldDrop.play();
                        });
                        inAppBrowserLink = "https://www.changetip.com/login/" + provider + "/";
                        $scope.pendingPoolBitsCollected = '0.00';
                        config = {
                          url: inAppBrowserLink,
                          target: "_blank",
                          options: []
                        };
                        browserService.open(config);
                        $scope.droppedObjects1.push(data);
                        def.resolve(true);
                        return;
                      }
                      if (magic_url) {
                        soundService.loadMedia($rootScope.image_path_prefix + 'sounds/gold').then(function(goldDrop) {
                          return goldDrop.play();
                        });
                        $scope.pendingPoolBitsCollected = '0.00';
                        inAppBrowserLink = magic_url;
                        config = {
                          url: inAppBrowserLink,
                          target: "_blank",
                          options: []
                        };
                        browserService.open(config);
                        $scope.droppedObjects1.push(data);
                        def.resolve(true);
                      }
                    }, function(errorData) {
                      notificationService.displayNote('Whoops! Try again later!');
                      return def.reject(false);
                    });
                  }
                  return def.promise;
                }
              }).then(function(transferStatus) {
                return console.log(transferStatus);
              });
            });
          });
        }, 0);
      };
      $scope.getProfile = function() {

        /*
        Get profile details.
         */
        _userRequire.then(function(user) {
          var username;
          if (!user) {
            return;
          }
          if (!user && !user.handle) {
            return;
          }
          username = user.handle;
          profileService.getProfile(username).then(function(_userProfileData) {
            _.forEach(_userProfileData, function(dataPoint, key) {
              return angular.extend($scope.forms.profile, dataPoint);
            });
            return $timeout(function() {
              if (parseFloat($scope.forms.profile.bitsCollected) > 0) {
                $scope.$apply(function() {
                  $scope.bitsCollectedValue = numeral(parseFloat($scope.forms.profile.bitsCollected)).format(bankService.currencyFormat);
                  return $scope.bitsCollectedAvailable = {
                    'USD': parseFloat($scope.forms.profile.bitsCollected) / 1000000,
                    'BTC': parseFloat($scope.forms.profile.bitsCollected) / 1000000
                  };
                });
              }
              return $scope.$emit('value', {
                finished: true
              });
            }, 0);
          });
        });
      };
      loadedBtcCurrencyValue = function(event, f) {

        /*
        Use bitcoin-prices module to pull exchange rate metadata on BTC, USD, 
        EUR, and other currencies relevant to fans.
         */
        return $timeout(function() {
          return $scope.$apply(function() {
            bitcoinprices.init({
              url: "https://api.bitcoinaverage.com/ticker/all",
              marketRateVariable: "24h_avg",
              defaultCurrency: $scope.lang,
              jQuery: $,
              priceAttribute: "data-btc-price",
              priceOrignalCurrency: $scope.bitsCollectedCurrency,
              symbols: {
                "BTC": "<span class='hint--bottom' data-hint='Tap to switch currencies'><i class='fa fa-btc animated flip-in-y'></i></span>",
                "USD": "<span class='hint--bottom' data-hint='Tap to switch currencies'><i class='fa fa-usd animated flip-in-y'></i></span>"
              },
              currencies: [$scope.bitsCollectedCurrency, "USD"],
              ux: {
                clickPrices: true,
                menu: false,
                clickableCurrencySymbol: true
              }
            });
            return $scope.finishedConversionCurrency = f.finished;
          });
        }, 100);
      };
      $scope.$on('value', loadedBtcCurrencyValue);
      t = null;
      $scope.logout = function(redirectUrl) {

        /*
        @namespace logout
        @description
        
          Log out.
         */
        profileService.clearValidations();
        loginSimple.logout();
        $state.go(redirectUrl);
      };
      $scope.updateProfile = function() {

        /*
        @namespace updateProfile
        @description
        
          Update profile details.
         */
        _userRequire.then(function(user) {
          var username;
          username = user.handle;
          profileService.updateProfile(username, $scope.forms.profile);
          if ($scope.forms.profile.pushNotifications.status) {
            $scope.pushNotifications.status = 'On';
          } else {
            $scope.pushNotifications.status = 'Off';
          }
        });
      };
      _t = null;
      $scope.showProductTour = function() {

        /*
        @namespace showProductTour
        @description
        
          Really a service level function. Leave it for now.
         */
        var productTourSeen, redirectUrl, u, username;
        redirectUrl = 'fan/show/product-tour';
        productTourSeen = $scope.forms.profile.productTour.seen === null ? true : void 0;
        $scope.forms.profile.productTour.seen = true;
        u = $scope.user;
        username = u.handle;
        profileService.updateProfile(username, $scope.forms.profile);
        _t = $timeout(function() {
          $location.path(redirectUrl);
          $timeout.cancel(_t);
        }, 250);
      };
    };
    return ["$scope", "$stateParams", "$rootScope", "stamp.service", "profile.service", "bank.service", "notification.service", "user.require", "$location", "$timeout", "$window", "$q", "$compile", "sound.service", 'browser.service', '$state', 'login.simple', ProfileController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
