###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

## description

A basic search module controller which will perform Elasticsearch queries.

###

define [
    'interface'
    'config'
    'lodash'
    'auth/user/roll-up'
], (__interface__, environment, _) ->

    SearchController = ($scope) ->
        ###
        Search Module Controller
        ###

        return

    ### Hookup ###
    [
        "$scope"
        SearchController
    ]
