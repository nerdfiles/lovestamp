
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

 *# description

A basic search module controller which will perform Elasticsearch queries.
 */

(function() {
  define(['interface', 'config', 'lodash', 'auth/user/roll-up'], function(__interface__, environment, _) {
    var SearchController;
    SearchController = function($scope) {

      /*
      Search Module Controller
       */
    };

    /* Hookup */
    return ["$scope", SearchController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
