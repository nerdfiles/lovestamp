
/**
Home Module Controller
 */

(function() {
  define(function() {
    var NotFoundController;
    NotFoundController = function($scope) {
      $scope.message = "Quote the server, 404";
    };
    return ["$scope", NotFoundController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
