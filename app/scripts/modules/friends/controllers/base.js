
/**
@fileOverview

 _
| |
| |__  _____  ___ _____
|  _ \(____ |/___) ___ |
| |_) ) ___ |___ | ____|
|____/\_____(___/|_____)

@module friends
@description

  Friends front end from friendBatch functions across social networks.
 */

(function() {
  define(["interface", "auth/user/roll-up"], function(__interface__) {
    var FriendsController;
    FriendsController = function($scope) {
      console.log($scope);
    };
    return ["$scope", FriendsController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
