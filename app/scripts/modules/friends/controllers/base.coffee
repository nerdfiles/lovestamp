###*
@fileOverview

 _
| |
| |__  _____  ___ _____
|  _ \(____ |/___) ___ |
| |_) ) ___ |___ | ____|
|____/\_____(___/|_____)

@module friends
@description

  Friends front end from friendBatch functions across social networks.

###

define [
  "interface"
  "auth/user/roll-up"
], (__interface__) ->

  FriendsController = ($scope) ->
    console.log $scope
    return
  [
    "$scope"
    FriendsController
  ]
