
/**
 * fileOverview

     _
    | |
    | |__  _____  ___ _____ 
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/legal  

 *# description

Legal Module Controller exists to supply Terms of Service to In-App Users on 
iOS and Android because the extant Web-based InAppBrowser strategy does not 
functional properly with the given (Jupiter theme as of 10/2015) WordPress 
theme's behavior layer. i.e., Tabs on the Legal Mumbo Jumbo Page do not with 
in the InAppBrowser context.
 */

(function() {
  define(["interface", "directives/site/navigation"], function(__interface__) {
    var LegalController;
    LegalController = function($scope) {
      $scope.loadingStoreOrDashboard = true;
    };
    return ["$scope", LegalController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
