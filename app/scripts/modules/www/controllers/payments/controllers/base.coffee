###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/payments  

## description

A glue controller for our Payments panel.

###

define [
  "interface"
  "lodash"
  "numeral"
  "moment"
  "angular-infinite-scroll" # @dependency angular.module 'admin/controllers', ['infinite-scroll']
  "angular-chosen-localytics" # @dependency angular.module 'admin/controllers', ['localytics.directives']
  "directives/stats/static"
  "directives/site/navigation"
  "directives/stroll"
  "auth/user/roll-up"
  "services/profile"
  "services/stamp"
  "services/bank"
  "services/payments"
  "filters/search"
  'angular-tablesort'
  'services/crypto'
], (__interface__, _, numeral, moment) ->

  PaymentsController = ($scope, $http, profileService, stampService, bankService, $timeout, $base64, paymentsService, userRequire, $location, $q, cryptoService, $rootScope) ->

    $scope.paymentsList = []

    if $location.absUrl().indexOf('payments') != -1 and $location.absUrl().indexOf('basic') == -1
      setPrimaryPurchases = true

    $scope.hashedType = $location.hash()

    $scope.$on '$locationChangeStart', () ->
      $scope.hashedType = $location.hash()

    _userRequire = userRequire()

    $scope.setStatus = (paymentConstruct) ->

      $timeout ->
        $scope.$apply () ->

          _handle = paymentConstruct.description.split(' ')
          handle = _.last _handle
          $id = paymentConstruct.$id
          $type = if paymentConstruct.description.indexOf('stamps for') != -1 then 'stamps' else 'impressions'
          formObject =
            created : paymentConstruct.created
            shipped  : paymentConstruct.shipped

          paymentsService.update('stripe').receipt(handle, $type, $id, formObject)

      , 0


    $scope.config_loadPaymentsList = (handle) ->
      ###
      @yields [$id: ..., payments: Array, $id: ..., payments: Array]
      ###

      callee = (paymentConstruct) ->
        def = $q.defer()

        paymentConstruct.$loaded().then (a) ->

          _.forEach a, (paymentList, handle) ->

            if _.has(paymentList, 'stamps') or _.has(paymentList, 'impressions')
              _.forEach paymentList, (paymentData, listType) ->

                _.forEach paymentData, (order, $id) ->

                  if _.has(paymentList, 'stamps')
                    console.log order

                    order.$id = $id

                    order.$statusSaturationList = [
                      {
                        $id      : $id
                        value    : 1
                        title    : 'Received'
                        disabled : true
                        hidden   : true
                      }
                      {
                        $id      : $id
                        value    : 2
                        title    : 'Shipped?'
                        disabled : true
                        hidden   : false
                      }
                    ]

                    order._status = _.extend {}, _.last(order.$statusSaturationList)

          def.resolve a
        def.promise

      loadedHandled = if handle then handle else null
      _callee = if handle then undefined else callee

      paymentsService.getPayments(loadedHandled, callee: _callee).then (cleanedPaymentsData) ->

        if !handle
          _paymentsList = _.toArray(_.map(_.extend.apply(_, cleanedPaymentsData), (d, k) ->
            $id: k
            payments: _.toArray(d.stamps).concat(_.toArray(d.impressions))
          ))
        else
          _paymentsList = _.toArray(_.map(_.extend.apply(_, cleanedPaymentsData), (d, k) ->
            $id: k
            payments: _.toArray(d)
          ))

        paymentsList = _.map(_paymentsList, (paymentData) ->

          paymentData.payments = _.map(paymentData.payments, (p) ->

            _p = _.extend {}, p

            _p.__status__ = () ->

              def = $q.defer()
              $timeout ->
                # Cached
                def.resolve p
              , 0
              def.promise

            _p.__status__().then (paymentData) ->
              _p.hashed = cryptoService.hash JSON.stringify(paymentData)

            _p._short_description = _p.description.split(' ')
            _p.short_description = _p._short_description[0] + ' ' + _p._short_description[1]
            _p.tail_description = _p._short_description[3].replace(_p._short_description[3].charAt(0), '')

            _p.url_prefix = ''
            _p._social_prefix = _p._short_description[3].charAt(0)

            if _p._social_prefix == '@'
              _p.url_prefix = 'https://twitter.com/'

            if _p._social_prefix == '✚'
              _p.url_prefix = 'https://plus.google.com/'

            if _p._social_prefix == 'ᶠ'
              _p.url_prefix = 'https://facebook.com/'

            if _.isString(_p.created)
              _p.created = moment(_p.created).format("MM/DD/YYYY HH:mm:ss")
              _p.created_label = moment(_p.created).format("MM/DD/YYYY")
            else
              _p.created = moment(new Date(parseInt(_p.created) * 1000)).format("MM/DD/YYYY HH:mm:ss")
              _p.created_label = moment(new Date(parseInt(_p.created) * 1000)).format("MM/DD/YYYY")

            _p

          )

          paymentData.payments

        )

        $scope.paymentsList = _.flattenDeep(paymentsList)
        return


    $scope.loadPaymentsList = () ->
      $scope.$on 'isAuthorized', (event, authorization) ->
        $scope.isAuthorized = authorization
        if $scope.isAuthorized == true
          $scope.config_loadPaymentsList(false)
        else
          _userRequire.then (authData) ->
            handle = authData.handle
            $scope.config_loadPaymentsList(handle)


    # @alt
    $scope._loadPaymentsList = () ->

      _userRequire.then (authData) ->
        handle = authData.handle
        paymentsService.getPayments(handle).then (cleanedPaymentsData) ->

          # $id: ..., payments: Array
          $scope.paymentsList = _paymentsList = _.toArray(_.map(_.extend.apply(_, cleanedPaymentsData), (d, k) ->
            p = _.map(d, (q) ->
              _p = _.extend {}, q

              _p._short_description = _p.description.split(' ')
              _p.short_description = _p._short_description[0] + ' ' + _p._short_description[1]
              _p.tail_description = _p._short_description[3].replace(_p._short_description[3].charAt(0), '')

              _p.url_prefix = ''
              _p._social_prefix = _p._short_description[3].charAt(0)

              if _p._social_prefix == '@'
                _p.url_prefix = 'https://twitter.com/'

              if _p._social_prefix == '✚'
                _p.url_prefix = 'https://plus.google.com/'

              if _p._social_prefix == 'ᶠ'
                _p.url_prefix = 'https://facebook.com/'

              if _.isString(_p.created)
                _p.created = moment(_p.created).format("MM/DD/YYYY HH:mm:ss")
                _p.created_label = moment(_p.created).format("MM/DD/YYYY")
              else
                _p.created = moment(new Date(parseInt(_p.created) * 1000)).format("MM/DD/YYYY HH:mm:ss")
                _p.created_label = moment(new Date(parseInt(_p.created) * 1000)).format("MM/DD/YYYY")

              _p
            )

            if k == 'stamps' and setPrimaryPurchases and !$scope.hashedType
              $scope.hashedType = 'stamps'

            $id: k
            payments: p
          ))
          return

    return

  moduleController = [
    '$scope'
    '$http'
    'profile.service'
    'stamp.service'
    'bank.service'
    "$timeout"
    "$base64"
    "payments.service"
    "user.require"
    "$location"
    "$q"
    "crypto.service"
    "$rootScope"
    PaymentsController
  ]
