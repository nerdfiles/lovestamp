
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/payments  

 *# description

A glue controller for our Payments panel.
 */

(function() {
  define(["interface", "lodash", "numeral", "moment", "angular-infinite-scroll", "angular-chosen-localytics", "directives/stats/static", "directives/site/navigation", "directives/stroll", "auth/user/roll-up", "services/profile", "services/stamp", "services/bank", "services/payments", "filters/search", 'angular-tablesort', 'services/crypto'], function(__interface__, _, numeral, moment) {
    var PaymentsController, moduleController;
    PaymentsController = function($scope, $http, profileService, stampService, bankService, $timeout, $base64, paymentsService, userRequire, $location, $q, cryptoService, $rootScope) {
      var setPrimaryPurchases, _userRequire;
      $scope.paymentsList = [];
      if ($location.absUrl().indexOf('payments') !== -1 && $location.absUrl().indexOf('basic') === -1) {
        setPrimaryPurchases = true;
      }
      $scope.hashedType = $location.hash();
      $scope.$on('$locationChangeStart', function() {
        return $scope.hashedType = $location.hash();
      });
      _userRequire = userRequire();
      $scope.setStatus = function(paymentConstruct) {
        return $timeout(function() {
          return $scope.$apply(function() {
            var $id, $type, formObject, handle, _handle;
            _handle = paymentConstruct.description.split(' ');
            handle = _.last(_handle);
            $id = paymentConstruct.$id;
            $type = paymentConstruct.description.indexOf('stamps for') !== -1 ? 'stamps' : 'impressions';
            formObject = {
              created: paymentConstruct.created,
              shipped: paymentConstruct.shipped
            };
            return paymentsService.update('stripe').receipt(handle, $type, $id, formObject);
          });
        }, 0);
      };
      $scope.config_loadPaymentsList = function(handle) {

        /*
        @yields [$id: ..., payments: Array, $id: ..., payments: Array]
         */
        var callee, loadedHandled, _callee;
        callee = function(paymentConstruct) {
          var def;
          def = $q.defer();
          paymentConstruct.$loaded().then(function(a) {
            _.forEach(a, function(paymentList, handle) {
              if (_.has(paymentList, 'stamps') || _.has(paymentList, 'impressions')) {
                return _.forEach(paymentList, function(paymentData, listType) {
                  return _.forEach(paymentData, function(order, $id) {
                    if (_.has(paymentList, 'stamps')) {
                      console.log(order);
                      order.$id = $id;
                      order.$statusSaturationList = [
                        {
                          $id: $id,
                          value: 1,
                          title: 'Received',
                          disabled: true,
                          hidden: true
                        }, {
                          $id: $id,
                          value: 2,
                          title: 'Shipped?',
                          disabled: true,
                          hidden: false
                        }
                      ];
                      return order._status = _.extend({}, _.last(order.$statusSaturationList));
                    }
                  });
                });
              }
            });
            return def.resolve(a);
          });
          return def.promise;
        };
        loadedHandled = handle ? handle : null;
        _callee = handle ? void 0 : callee;
        return paymentsService.getPayments(loadedHandled, {
          callee: _callee
        }).then(function(cleanedPaymentsData) {
          var paymentsList, _paymentsList;
          if (!handle) {
            _paymentsList = _.toArray(_.map(_.extend.apply(_, cleanedPaymentsData), function(d, k) {
              return {
                $id: k,
                payments: _.toArray(d.stamps).concat(_.toArray(d.impressions))
              };
            }));
          } else {
            _paymentsList = _.toArray(_.map(_.extend.apply(_, cleanedPaymentsData), function(d, k) {
              return {
                $id: k,
                payments: _.toArray(d)
              };
            }));
          }
          paymentsList = _.map(_paymentsList, function(paymentData) {
            paymentData.payments = _.map(paymentData.payments, function(p) {
              var _p;
              _p = _.extend({}, p);
              _p.__status__ = function() {
                var def;
                def = $q.defer();
                $timeout(function() {
                  return def.resolve(p);
                }, 0);
                return def.promise;
              };
              _p.__status__().then(function(paymentData) {
                return _p.hashed = cryptoService.hash(JSON.stringify(paymentData));
              });
              _p._short_description = _p.description.split(' ');
              _p.short_description = _p._short_description[0] + ' ' + _p._short_description[1];
              _p.tail_description = _p._short_description[3].replace(_p._short_description[3].charAt(0), '');
              _p.url_prefix = '';
              _p._social_prefix = _p._short_description[3].charAt(0);
              if (_p._social_prefix === '@') {
                _p.url_prefix = 'https://twitter.com/';
              }
              if (_p._social_prefix === '✚') {
                _p.url_prefix = 'https://plus.google.com/';
              }
              if (_p._social_prefix === 'ᶠ') {
                _p.url_prefix = 'https://facebook.com/';
              }
              if (_.isString(_p.created)) {
                _p.created = moment(_p.created).format("MM/DD/YYYY HH:mm:ss");
                _p.created_label = moment(_p.created).format("MM/DD/YYYY");
              } else {
                _p.created = moment(new Date(parseInt(_p.created) * 1000)).format("MM/DD/YYYY HH:mm:ss");
                _p.created_label = moment(new Date(parseInt(_p.created) * 1000)).format("MM/DD/YYYY");
              }
              return _p;
            });
            return paymentData.payments;
          });
          $scope.paymentsList = _.flattenDeep(paymentsList);
        });
      };
      $scope.loadPaymentsList = function() {
        return $scope.$on('isAuthorized', function(event, authorization) {
          $scope.isAuthorized = authorization;
          if ($scope.isAuthorized === true) {
            return $scope.config_loadPaymentsList(false);
          } else {
            return _userRequire.then(function(authData) {
              var handle;
              handle = authData.handle;
              return $scope.config_loadPaymentsList(handle);
            });
          }
        });
      };
      $scope._loadPaymentsList = function() {
        return _userRequire.then(function(authData) {
          var handle;
          handle = authData.handle;
          return paymentsService.getPayments(handle).then(function(cleanedPaymentsData) {
            var _paymentsList;
            $scope.paymentsList = _paymentsList = _.toArray(_.map(_.extend.apply(_, cleanedPaymentsData), function(d, k) {
              var p;
              p = _.map(d, function(q) {
                var _p;
                _p = _.extend({}, q);
                _p._short_description = _p.description.split(' ');
                _p.short_description = _p._short_description[0] + ' ' + _p._short_description[1];
                _p.tail_description = _p._short_description[3].replace(_p._short_description[3].charAt(0), '');
                _p.url_prefix = '';
                _p._social_prefix = _p._short_description[3].charAt(0);
                if (_p._social_prefix === '@') {
                  _p.url_prefix = 'https://twitter.com/';
                }
                if (_p._social_prefix === '✚') {
                  _p.url_prefix = 'https://plus.google.com/';
                }
                if (_p._social_prefix === 'ᶠ') {
                  _p.url_prefix = 'https://facebook.com/';
                }
                if (_.isString(_p.created)) {
                  _p.created = moment(_p.created).format("MM/DD/YYYY HH:mm:ss");
                  _p.created_label = moment(_p.created).format("MM/DD/YYYY");
                } else {
                  _p.created = moment(new Date(parseInt(_p.created) * 1000)).format("MM/DD/YYYY HH:mm:ss");
                  _p.created_label = moment(new Date(parseInt(_p.created) * 1000)).format("MM/DD/YYYY");
                }
                return _p;
              });
              if (k === 'stamps' && setPrimaryPurchases && !$scope.hashedType) {
                $scope.hashedType = 'stamps';
              }
              return {
                $id: k,
                payments: p
              };
            }));
          });
        });
      };
    };
    return moduleController = ['$scope', '$http', 'profile.service', 'stamp.service', 'bank.service', "$timeout", "$base64", "payments.service", "user.require", "$location", "$q", "crypto.service", "$rootScope", PaymentsController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
