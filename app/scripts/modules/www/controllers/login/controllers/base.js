
/**
 * fileOverview

     _
    | |                     
    | |__  _____  ___ _____ 
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)


 *# module

    www/controllers/login

 *# description

Login Module Controller. For Application-wide authentication modals windows.
 */

(function() {
  define(['AuthenticationController'], function(AuthenticationController) {
    var BaseController;
    return BaseController = AuthenticationController;
  });

}).call(this);

//# sourceMappingURL=base.js.map
