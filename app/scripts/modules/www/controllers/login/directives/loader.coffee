###

@fileOverview

 _                 _
| |               | |
| | ___  _____  __| |_____  ____
| |/ _ \(____ |/ _  | ___ |/ ___)
| | |_| / ___ ( (_| | ____| |
 \_)___/\_____|\____|_____)_|

@description

    A loader.

###

define [
  "interface"
  "config"
  #"hammer" # @supports angular-material
  #"stroll" # @dependencies angular
], (__interface__, environment) ->

  ###*
  @namespace leaflet/loader
  @description A leaflet page loader, since all pages include maps, and 
               AngularJS supplies those maps.

  @params {$internal} timeout
  @params {$internal} log
  ###

  _t = if window.is_device then './scripts/' else (environment.baseTemplateUrl)
  loader = ($timeout, $log, $rootScope) ->
    restrict: "A"
    templateUrl: _t + 'modules/www/controllers/login/partials/loader.html'
    scope:
        showMap: '=showMap'
    link: ($scope, element, attr) ->
        $timeout ->
            $rootScope.showMap = false
            $scope.$apply()
        , 0
        $scope.$on('$locationChangeStart', () ->
            $timeout ->
                $rootScope.showMap = false
                $scope.$apply()
            , 0
        )
        return

  deps = [
    "$timeout"
    "$log"
    "$rootScope"
    loader
  ]

  __interface__
    .directive "loader", deps
  return
