
/*

@fileOverview

 _                 _
| |               | |
| | ___  _____  __| |_____  ____
| |/ _ \(____ |/ _  | ___ |/ ___)
| | |_| / ___ ( (_| | ____| |
 \_)___/\_____|\____|_____)_|

@description

    A loader.
 */

(function() {
  define(["interface", "config"], function(__interface__, environment) {

    /**
    @namespace leaflet/loader
    @description A leaflet page loader, since all pages include maps, and 
                 AngularJS supplies those maps.
    
    @params {$internal} timeout
    @params {$internal} log
     */
    var deps, loader, _t;
    _t = window.is_device ? './scripts/' : environment.baseTemplateUrl;
    loader = function($timeout, $log, $rootScope) {
      return {
        restrict: "A",
        templateUrl: _t + 'modules/www/controllers/login/partials/loader.html',
        scope: {
          showMap: '=showMap'
        },
        link: function($scope, element, attr) {
          $timeout(function() {
            $rootScope.showMap = false;
            return $scope.$apply();
          }, 0);
          $scope.$on('$locationChangeStart', function() {
            return $timeout(function() {
              $rootScope.showMap = false;
              return $scope.$apply();
            }, 0);
          });
        }
      };
    };
    deps = ["$timeout", "$log", "$rootScope", loader];
    __interface__.directive("loader", deps);
  });

}).call(this);

//# sourceMappingURL=loader.js.map
