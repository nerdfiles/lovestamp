
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/social

 *# description

Information Module Controller.
 */

(function() {
  define(["interface", "lodash", "directives/site/navigation", "auth/user/roll-up", "services/profile", "services/notification", "services/map", "services/authentication"], function(__interface__, _) {
    var SocialController;
    SocialController = function($scope, $q, $window, profileService, userRequire, mapService, $timeout, $rootScope, localStorageService, notificationService, loginSimple) {
      var _cachedHandle, _l, _type, _userRequire;
      $scope.pageSectionTitle = 'Social';
      $scope.forms = {
        billingAddressForm: {},
        shippingAddressForm: {},
        bioForm: {}
      };
      $scope.user = {
        bio: {
          contentEditable: void 0,
          contentUpdated: void 0,
          content: '',
          editing: false
        },
        provider: {
          twitter: false,
          google: false,
          facebook: false
        }
      };
      $scope.address = {
        billing: {
          contentEditable: void 0,
          contentUpdated: void 0,
          content: '',
          editing: false
        },
        shipping: {
          contentEditable: void 0,
          contentUpdated: void 0,
          content: '',
          editing: false
        }
      };
      $scope.originUserId = null;
      $scope.load = function() {

        /*
        load
         */
        var _userRequire;
        _userRequire = userRequire();
        return _userRequire.then(function(authData) {
          var handle;
          handle = authData.handle;
          return profileService.getProfile(handle).then(function(profileData) {
            var networks, networksList, originUserId, u, _authData, _networks;
            _networks = _.last(_.filter(_.map(profileData, function(d, k) {
              if (d.networks) {
                return d;
              }
            })));
            networks = _networks.networks;
            networksList = _.toArray(networks);
            networksList = _.filter(networksList, function(a) {
              return a.indexOf('[') !== -1;
            });
            _authData = _.last(networksList);
            u = _authData.split(':');
            originUserId = $scope.originUserId = u[1];
            originUserId = originUserId.replace(']', '');
            return profileService.getProfileById(originUserId).then(function(cleanedProfileData) {
              return $timeout(function() {
                return $scope.$apply(function() {
                  var addressBillingObject, addressShippingObject, bioObject, validObject;
                  $scope._profile = cleanedProfileData;
                  addressBillingObject = $scope._profile.addressBilling;
                  addressShippingObject = $scope._profile.addressShipping;
                  bioObject = $scope._profile.bio;
                  if (addressBillingObject) {
                    $scope.address.billing.content = [addressBillingObject.street_number, " ", addressBillingObject.route, "\n", addressBillingObject.locality, ", ", addressBillingObject.administrative_area_level_1, " ", addressBillingObject.postal_code].join('');
                  }
                  if (addressShippingObject) {
                    $scope.address.shipping.content = [addressShippingObject.street_number, " ", addressShippingObject.route, "\n", addressShippingObject.locality, ", ", addressShippingObject.administrative_area_level_1, " ", addressShippingObject.postal_code].join('');
                  }
                  if (bioObject) {
                    $scope.user.bio.content = bioObject;
                  }
                  $scope.$watchCollection('user.bio', function(newValue, oldValue) {

                    /*
                    @namespace user.bio.content
                    @description
                    
                    Watching for generic updates from ``connectProfile``.
                     */
                    if (newValue !== '' && $scope.user.bio.contentUpdated) {
                      return $scope.user.bio.editing = true;
                    }
                  });
                  validObject = $scope._profile.overview;
                  if (!validObject) {
                    return;
                  }
                  $scope.overview = validObject;
                  return $scope.profile = $scope.overview;
                });
              }, 0);
            });
          });
        });
      };
      $scope.$watchCollection('profile', function(newValue, oldValue) {
        var foundOriginUser, isDirty, originUserId, _userRequire;
        isDirty = false;
        _.forEach(newValue, function(prop) {
          if (prop !== null) {
            return isDirty = true;
          }
        });
        if (JSON.stringify(newValue) !== JSON.stringify(oldValue)) {
          foundOriginUser = false;
          originUserId = null;
          _userRequire = userRequire();
          return _userRequire.then(function(authData) {
            var _handle;
            _handle = authData.handle;
            return profileService.getUserStatus(_handle).$loaded().then(function(d) {
              var content, __handle;
              __handle = null;
              _.forEach(d.profile.networks, function(userInNetwork) {
                var e, __originUserId, _originUserId;
                if (userInNetwork.indexOf('[') !== -1) {
                  foundOriginUser = true;
                  try {
                    originUserId = userInNetwork.split(':');
                    _originUserId = originUserId[1].split(']');
                    __originUserId = _originUserId[0];
                    return __handle = __originUserId;
                  } catch (_error) {
                    e = _error;
                    return __handle = _handle;
                  }
                }
              });
              if (foundOriginUser !== true) {
                __handle = _handle;
              }
              content = 'overview';
              if (foundOriginUser) {
                return profileService.getUserById(_handle).then(function(__handle) {
                  return profileService.updateProfileBranch(givenUser.$id, content, newValue);
                });
              } else {
                return profileService.updateProfileBranch(__handle, content, newValue);
              }
            });
          });
        }
      });
      $scope.fieldRowEditBillingAddress = function(content) {

        /*
         * fieldRowEditBillingAddress
         */
        var _userRequire;
        _userRequire = userRequire();
        _userRequire.then(function(authData) {
          var handle, verifiedAddress;
          handle = authData.handle;
          if (!$scope.address.billing.editing) {
            $scope.address.billing.editing = true;
            return $scope.address.billing.contentEditable = $scope.$watch(content, function(newValue, oldValue) {
              if (newValue !== '' && newValue !== oldValue) {
                return $scope.address.billing.contentUpdated = true;
              } else {
                return $scope.address.billing.contentUpdated = false;
              }
            });
          } else {
            return verifiedAddress = mapService.Owlish.verifyAddress($scope.address.billing.content).then(function(updatedProfile) {
              var foundOriginUser, originUserId, __handle;
              __handle = null;
              foundOriginUser = false;
              originUserId = null;
              profileService.getUserStatus(handle).$loaded().then(function(d) {
                _.forEach(d.profile.networks, function(userInNetwork) {
                  var e, __originUserId, _originUserId;
                  if (userInNetwork.indexOf('[') !== -1) {
                    foundOriginUser = true;
                    try {
                      originUserId = userInNetwork.split(':');
                      _originUserId = originUserId[1].split(']');
                      __originUserId = _originUserId[0];
                      return __handle = __originUserId;
                    } catch (_error) {
                      e = _error;
                      return __handle = _handle;
                    }
                  }
                });
                if (foundOriginUser !== true) {
                  __handle = _handle;
                }
                if (foundOriginUser) {
                  return profileService.getUserById(__handle).then(function(givenUser) {
                    return profileService.updateProfileBranch(givenUser.$id, content, updatedProfile.data);
                  });
                } else {
                  return profileService.updateProfileBranch(__handle, content, updatedProfile.data);
                }
              });
              $scope.address.billing.editing = false;
              return $scope.address.billing.contentEditable();
            });
          }
        });
      };
      $scope.fieldRowEditShippingAddress = function(content) {

        /*
         * fieldRowEditShippingAddress
         */
        var _userRequire;
        _userRequire = userRequire();
        _userRequire.then(function(authData) {
          var handle, verifiedAddress;
          handle = authData.handle;
          if (!$scope.address.shipping.editing) {
            $scope.address.shipping.editing = true;
            return $scope.address.shipping.contentEditable = $scope.$watch(content, function(newValue, oldValue) {
              if (newValue !== '' && newValue !== oldValue) {
                return $scope.address.shipping.contentUpdated = true;
              } else {
                return $scope.address.shipping.contentUpdated = false;
              }
            });
          } else {
            return verifiedAddress = mapService.Owlish.verifyAddress($scope.address.shipping.content).then(function(updatedProfile) {
              var foundOriginUser, originUserId, __handle;
              __handle = null;
              foundOriginUser = false;
              originUserId = null;
              profileService.getUserStatus(handle).$loaded().then(function(d) {
                _.forEach(d.profile.networks, function(userInNetwork) {
                  var e, __originUserId, _originUserId;
                  if (userInNetwork.indexOf('[') !== -1) {
                    foundOriginUser = true;
                    try {
                      originUserId = userInNetwork.split(':');
                      _originUserId = originUserId[1].split(']');
                      __originUserId = _originUserId[0];
                      return __handle = __originUserId;
                    } catch (_error) {
                      e = _error;
                      return __handle = _handle;
                    }
                  }
                });
                if (foundOriginUser !== true) {
                  __handle = _handle;
                }
                return profileService.updateProfileBranch(__handle, content, updatedProfile.data);
              });
              $scope.address.shipping.editing = false;
              return $scope.address.shipping.contentEditable();
            });
          }
        });
      };
      $scope.fieldRowEditBio = function(content) {

        /*
        @namespace fieldRowEditBio
        @description
        
        Should listen for ``connectProfile(socialPrefix)``.
         */
        var _userRequire;
        _userRequire = userRequire();
        _userRequire.then(function(authData) {
          var foundOriginUser, handle, originUserId, updatedProfile;
          handle = authData.handle;
          if (!$scope.user.bio.editing) {
            $scope.user.bio.editing = true;
            return $scope.user.bio.contentEditable = $scope.$watch(content, function(newValue, oldValue) {
              if (newValue !== '' && newValue !== oldValue || $scope.user.bio.content !== '') {
                $scope.user.bio.contentUpdated = true;
              } else {
                $scope.user.bio.contentUpdated = false;
              }
            });
          } else {
            if ($scope.user.bio.content !== '' || $scope.user.bio.contentEditable) {
              updatedProfile = $scope.user.bio.content;
            }
            foundOriginUser = false;
            originUserId = null;
            profileService.getUserStatus(handle).$loaded().then(function(d) {
              var __handle;
              __handle = null;
              _.forEach(d.profile.networks, function(userInNetwork) {
                var e, __originUserId, _originUserId;
                if (userInNetwork.indexOf('[') !== -1) {
                  foundOriginUser = true;
                  try {
                    originUserId = userInNetwork.split(':');
                    _originUserId = originUserId[1].split(']');
                    __originUserId = _originUserId[0];
                    return __handle = __originUserId;
                  } catch (_error) {
                    e = _error;
                    return __handle = _handle;
                  }
                }
              });
              if (foundOriginUser !== true) {
                __handle = _handle;
              }
              if (foundOriginUser) {
                profileService.getUserById(__handle).then(function(givenUser) {
                  return profileService.updateProfileBranch(givenUser.$id, content, updatedProfile);
                });
              } else {
                profileService.updateProfileBranch(__handle, content, updatedProfile);
              }
              $scope.user.bio.editing = false;
              return $scope.user.bio.contentEditable();
            });
          }
        });
      };
      $scope.loadSocialMediaProfiles = function() {

        /*
        Load Social Media Profile
         */
        var _userRequire;
        _userRequire = userRequire();
        return _userRequire.then(function(authData) {
          var handle;
          handle = authData.handle;
          $scope.isCurrentSocial = handle.charAt(0) === '@' ? 'twitter' : handle.charAt(0) === '+' ? 'google' : 'facebook';
          return $timeout(function() {
            return $scope.$apply(function() {
              var userProfile;
              userProfile = profileService.getProfile(handle);
              return userProfile.then(function(_profileData) {
                var filteredCheckProfiles, networksEnabled, profileData;
                profileData = _.extend.apply(_, _profileData);
                networksEnabled = profileData.networks;
                $scope.availableSocialProfiles = _.extend({}, networksEnabled);
                $scope.enabledSocialProfile = {
                  'twitter': $scope.availableSocialProfiles['twitter'],
                  'google': $scope.availableSocialProfiles['google'],
                  'facebook': $scope.availableSocialProfiles['facebook']
                };
                $scope.checkProfiles = _.toArray($scope.enabledSocialProfile);
                filteredCheckProfiles = _.filter($scope.checkProfiles);
                if ($scope.checkProfiles.length === 1) {
                  return $scope.enabledSocialProfile[$scope.isCurrentSocial] = "[" + $scope.enabledSocialProfile[$scope.isCurrentSocial] + "]";
                }
              });
            });
          }, 2000);
        });
      };
      _cachedHandle = null;
      _userRequire = userRequire();
      _userRequire.then(function(authData) {
        return _cachedHandle = authData.handle;
      });
      $scope.updateSocialMediaProfiles = function() {

        /*
         */
        var availableSocialProfiles, e, enabledSocialProfile, handle, networks, profileIds, social_facebook, social_google, social_twitter;
        handle = _cachedHandle;
        availableSocialProfiles = {
          'twitter': localStorageService.get('twitter'),
          'google': localStorageService.get('google'),
          'facebook': localStorageService.get('facebook')
        };
        if ($scope.checkProfiles.length === 1) {
          availableSocialProfiles[$scope.isCurrentSocial] = "[" + $scope.enabledSocialProfile[$scope.isCurrentSocial] + "]";
        }
        try {
          social_twitter = {
            uid: $scope.enabledSocialProfile.twitter
          };
        } catch (_error) {
          e = _error;
          console.log('Twitter not lodged.');
        }
        try {
          social_facebook = {
            uid: $scope.enabledSocialProfile.facebook
          };
        } catch (_error) {
          e = _error;
          console.log('Facebook not lodged.');
        }
        try {
          social_google = {
            uid: $scope.enabledSocialProfile.google
          };
        } catch (_error) {
          e = _error;
          console.log('Google not lodged.');
        }
        enabledSocialProfile = {
          'twitter': social_twitter ? social_twitter : availableSocialProfiles['twitter'],
          'google': social_google ? social_google : availableSocialProfiles['google'],
          'facebook': social_facebook ? social_facebook : availableSocialProfiles['facebook']
        };
        profileIds = {};
        if (enabledSocialProfile['twitter'] && enabledSocialProfile['twitter'].uid) {
          _.extend(profileIds, {
            twitter: enabledSocialProfile['twitter'].uid
          });
        }
        if (enabledSocialProfile['facebook'] && enabledSocialProfile['facebook'].uid) {
          _.extend(profileIds, {
            facebook: enabledSocialProfile['facebook'].uid
          });
        }
        if (enabledSocialProfile['google'] && enabledSocialProfile['google'].uid) {
          _.extend(profileIds, {
            google: enabledSocialProfile['google'].uid
          });
        }
        networks = {};
        _.extend(networks, {
          'networks': profileIds
        });
        profileService.updateProfile(handle, networks);
        return $timeout(function() {
          return $scope.$apply(function() {
            return $scope.loadSocialMediaProfiles();
          });
        }, 1000);
      };
      _type = null;
      _l = null;
      $scope.connectProfile = function(type) {

        /*
        Update Profile (Rich API Control)
        
        @param {string} type
          A string of the social network provider. Currently:
            1. twitter
            2. google
            3. facebook
        @return {undefined}
         */
        var checkUpNote;
        checkUpNote = {
          type: type
        };
        $scope.social.type = checkUpNote.type;
        $scope.$broadcast("auth/user/check-up", checkUpNote);
        _userRequire = userRequire();
        _userRequire.then(function(authData) {
          var _cachedUserProfile;
          if (authData[type]) {
            $scope.cachedUserProfile = _cachedUserProfile = authData[type] && authData[type].cachedUserProfile ? authData[type].cachedUserProfile : null;
            if ($scope.user.bio.content === '') {
              return $scope.user.bio.content = $scope.cachedUserProfile.description || $scope.cachedUserProfile.name;
            }
          } else {
            if (_type === null) {
              _type = authData.provider;
            }
            if (_l === null) {
              _l = sessionStorage.getItem('firebase:session::lovestamp');
              localStorageService.set(_type, _l);
            }
            return $rootScope.authWith(type, true).then(function(_authData) {
              var a, accessId, accessToken, accessTokenSecret, parsedOldSession, provider;
              a = _authData;
              if (_l !== null) {
                localStorageService.set(type, JSON.stringify(a));
                parsedOldSession = JSON.parse(_l);
                provider = parsedOldSession.provider;
                if (provider === 'twitter') {
                  accessId = parsedOldSession[parsedOldSession.provider].id;
                  accessTokenSecret = parsedOldSession[parsedOldSession.provider].accessTokenSecret;
                  accessToken = parsedOldSession[parsedOldSession.provider].accessToken;
                  loginSimple.relogin(provider, accessId, accessToken, accessTokenSecret);
                } else {
                  accessToken = parsedOldSession[parsedOldSession.provider].accessToken;
                  loginSimple.relogin(provider, accessId, accessToken, accessTokenSecret);
                }
                return $scope.updateSocialMediaProfiles();
              } else {
                return notificationService.displayNote('Profile not set.');
              }
            });
          }
        });
      };
    };
    return ["$scope", "$q", "$window", "profile.service", "user.require", "map.service", "$timeout", "$rootScope", "localStorageService", "notification.service", "login.simple", SocialController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
