###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/social

## description

Information Module Controller.

###

define [
  "interface"
  "lodash"
  "directives/site/navigation"
  "auth/user/roll-up"
  "services/profile"
  "services/notification"
  "services/map"
  "services/authentication"
], (__interface__, _) ->

  SocialController = ($scope, $q, $window, profileService, userRequire, mapService, $timeout, $rootScope, localStorageService, notificationService, loginSimple) ->

    $scope.pageSectionTitle = 'Social'

    $scope.forms =
      billingAddressForm: {}
      shippingAddressForm: {}
      bioForm: {}

    $scope.user =
      bio:
        contentEditable : undefined
        contentUpdated  : undefined
        content         : ''
        editing         : false
      provider:
        twitter  : false
        google   : false
        facebook : false

    $scope.address =
      billing:
        contentEditable : undefined
        contentUpdated  : undefined
        content         : ''
        editing         : false
      shipping:
        contentEditable : undefined
        contentUpdated  : undefined
        content         : ''
        editing         : false

    $scope.originUserId = null
    $scope.load = () ->
      ###
      load
      ###

      _userRequire = userRequire()

      _userRequire.then((authData) ->

        handle = authData.handle

        profileService.getProfile(handle).then (profileData) ->

          _networks = _.last(_.filter(_.map(profileData, (d, k) ->
            if d.networks
              return d
          )))

          networks = _networks.networks
          networksList = _.toArray(networks)
          networksList = _.filter(networksList, (a) ->
            a.indexOf('[') isnt -1
          )
          _authData = _.last(networksList)

          u = _authData.split(':')
          originUserId = $scope.originUserId = u[1]
          originUserId = originUserId.replace(']', '')

          profileService.getProfileById(originUserId).then (cleanedProfileData) ->

            $timeout ->
              $scope.$apply () ->
                $scope._profile = cleanedProfileData
                addressBillingObject = $scope._profile.addressBilling
                addressShippingObject = $scope._profile.addressShipping
                bioObject = $scope._profile.bio

                if addressBillingObject
                  $scope.address.billing.content = [
                    addressBillingObject.street_number
                    " "
                    addressBillingObject.route
                    "\n"
                    addressBillingObject.locality
                    ", "
                    addressBillingObject.administrative_area_level_1
                    " "
                    addressBillingObject.postal_code
                  ].join ''

                if addressShippingObject
                  $scope.address.shipping.content = [
                    addressShippingObject.street_number
                    " "
                    addressShippingObject.route
                    "\n"
                    addressShippingObject.locality
                    ", "
                    addressShippingObject.administrative_area_level_1
                    " "
                    addressShippingObject.postal_code
                  ].join ''

                if bioObject
                  $scope.user.bio.content = bioObject

                $scope.$watchCollection 'user.bio', (newValue, oldValue) ->
                  ###
                  @namespace user.bio.content
                  @description

                  Watching for generic updates from ``connectProfile``.

                  ###

                  if newValue isnt '' and $scope.user.bio.contentUpdated
                    $scope.user.bio.editing = true
                    #$scope.user.bio.placeholder = null

                validObject = $scope._profile.overview

                return  if not validObject
                #_overview = _.last validObject
                $scope.overview = validObject
                $scope.profile = $scope.overview

            , 0
      )


    $scope.$watchCollection('profile', (newValue, oldValue) ->
      isDirty = false
      _.forEach newValue, (prop) ->
        if prop != null
          isDirty = true

      if JSON.stringify(newValue) != JSON.stringify(oldValue)

        foundOriginUser = false
        originUserId = null

        _userRequire = userRequire()
        _userRequire.then (authData) ->

          _handle = authData.handle

          profileService.getUserStatus(_handle).$loaded().then (d) ->
            __handle = null
            _.forEach d.profile.networks, (userInNetwork) ->
              if userInNetwork.indexOf('[') isnt -1
                foundOriginUser = true
                try
                  originUserId = userInNetwork.split(':')
                  _originUserId = originUserId[1].split(']')
                  __originUserId = _originUserId[0]
                  __handle = __originUserId
                catch e
                  __handle = _handle

            if foundOriginUser != true
              __handle = _handle

            content = 'overview'
            if foundOriginUser
              profileService.getUserById(_handle).then (__handle) ->
                profileService.updateProfileBranch(givenUser.$id, content, newValue)
            else
              profileService.updateProfileBranch(__handle, content, newValue)

    )


    $scope.fieldRowEditBillingAddress = (content) ->
      ###
      # fieldRowEditBillingAddress
      ###

      _userRequire = userRequire()
      _userRequire.then (authData) ->
        handle = authData.handle

        if ! $scope.address.billing.editing

          $scope.address.billing.editing = true
          $scope.address.billing.contentEditable = $scope.$watch(content, (newValue, oldValue) ->
            if newValue isnt '' and newValue isnt oldValue
              $scope.address.billing.contentUpdated = true
            else
              $scope.address.billing.contentUpdated = false
          )

        else

          verifiedAddress = mapService.Owlish.verifyAddress($scope.address.billing.content).then (updatedProfile) ->

            __handle = null

            foundOriginUser = false
            originUserId = null
            profileService.getUserStatus(handle).$loaded().then (d) ->
              _.forEach d.profile.networks, (userInNetwork) ->
                if userInNetwork.indexOf('[') isnt -1
                  foundOriginUser = true
                  try
                    originUserId = userInNetwork.split(':')
                    _originUserId = originUserId[1].split(']')
                    __originUserId = _originUserId[0]
                    __handle = __originUserId
                  catch e
                    __handle = _handle

              if foundOriginUser != true
                __handle = _handle

              if foundOriginUser
                profileService.getUserById(__handle).then (givenUser) ->
                  profileService.updateProfileBranch(givenUser.$id, content, updatedProfile.data)
              else
                # Store along a branched social microdata schema
                profileService.updateProfileBranch(__handle, content, updatedProfile.data)

            # Reset.
            $scope.address.billing.editing = false

            # Reset our elemental model.
            $scope.address.billing.contentEditable()

      return


    $scope.fieldRowEditShippingAddress = (content) ->
      ###
      # fieldRowEditShippingAddress
      ###

      # @TODO Contains incorrect data.
      _userRequire = userRequire()
      _userRequire.then (authData) ->
        handle = authData.handle

        if ! $scope.address.shipping.editing

          $scope.address.shipping.editing = true
          $scope.address.shipping.contentEditable = $scope.$watch(content, (newValue, oldValue) ->
            if newValue isnt '' and newValue isnt oldValue
              $scope.address.shipping.contentUpdated = true
            else
              $scope.address.shipping.contentUpdated = false
          )

        else

          verifiedAddress = mapService.Owlish.verifyAddress($scope.address.shipping.content).then (updatedProfile) ->

            __handle = null
            foundOriginUser = false
            originUserId = null

            profileService.getUserStatus(handle).$loaded().then (d) ->

              _.forEach d.profile.networks, (userInNetwork) ->
                if userInNetwork.indexOf('[') isnt -1
                  foundOriginUser = true
                  try
                    originUserId = userInNetwork.split(':')
                    _originUserId = originUserId[1].split(']')
                    __originUserId = _originUserId[0]
                    __handle = __originUserId
                  catch e
                    __handle = _handle

              if foundOriginUser != true
                __handle = _handle

              # Store along a branched social microdata schema
              profileService.updateProfileBranch(__handle, content, updatedProfile.data)

            # Reset
            $scope.address.shipping.editing = false

            # Reset our elemental model.
            $scope.address.shipping.contentEditable()

      return


    $scope.fieldRowEditBio = (content) ->
      ###
      @namespace fieldRowEditBio
      @description

      Should listen for ``connectProfile(socialPrefix)``.

      ###

      # @TODO Missing children.
      _userRequire = userRequire()
      _userRequire.then (authData) ->
        handle = authData.handle

        if ! $scope.user.bio.editing

          $scope.user.bio.editing = true
          $scope.user.bio.contentEditable = $scope.$watch(content, (newValue, oldValue) ->
            if newValue isnt '' and newValue isnt oldValue or $scope.user.bio.content isnt ''
              $scope.user.bio.contentUpdated = true
            else
              $scope.user.bio.contentUpdated = false
            return
          )

        else

          if $scope.user.bio.content isnt '' or $scope.user.bio.contentEditable

            updatedProfile = $scope.user.bio.content

          foundOriginUser = false
          originUserId = null

          profileService.getUserStatus(handle).$loaded().then (d) ->
            __handle = null

            _.forEach d.profile.networks, (userInNetwork) ->
              if userInNetwork.indexOf('[') isnt -1
                foundOriginUser = true
                try
                  originUserId = userInNetwork.split(':')
                  _originUserId = originUserId[1].split(']')
                  __originUserId = _originUserId[0]
                  __handle = __originUserId
                catch e
                  __handle = _handle

            if foundOriginUser != true
              __handle = _handle

            # Store along a branched social microdata schema
            if foundOriginUser
              profileService.getUserById(__handle).then (givenUser) ->
                profileService.updateProfileBranch(givenUser.$id, content, updatedProfile)
            else
              profileService.updateProfileBranch(__handle, content, updatedProfile)

            $scope.user.bio.editing = false

            # Reset our elemental model.
            $scope.user.bio.contentEditable()

          return
      return


    $scope.loadSocialMediaProfiles = () ->
      ###
      Load Social Media Profile
      ###

      _userRequire = userRequire()

      _userRequire.then (authData) ->
        handle = authData.handle
        $scope.isCurrentSocial = if handle.charAt(0) == '@' then 'twitter' else if handle.charAt(0) == '+' then 'google' else 'facebook'

        $timeout ->
          $scope.$apply () ->
            userProfile = profileService.getProfile handle

            userProfile.then (_profileData) ->
              profileData = _.extend.apply(_, _profileData)
              networksEnabled = profileData.networks

              $scope.availableSocialProfiles = _.extend {}, networksEnabled

              $scope.enabledSocialProfile =
                'twitter'  : $scope.availableSocialProfiles['twitter']
                'google'   : $scope.availableSocialProfiles['google']
                'facebook' : $scope.availableSocialProfiles['facebook']

              $scope.checkProfiles = _.toArray($scope.enabledSocialProfile)
              filteredCheckProfiles = _.filter $scope.checkProfiles
              if $scope.checkProfiles.length == 1
                $scope.enabledSocialProfile[$scope.isCurrentSocial] = "[#{$scope.enabledSocialProfile[$scope.isCurrentSocial]}]"
                #originCheckProfiles = _.last(filteredCheckProfiles)
                #_originCheckProfiles = originCheckProfiles.split(':')
                #_originCheckProfilesProvider = _originCheckProfiles[0]
                #_originCheckProfilesHandle = _originCheckProfiles[1]
                #$scope.enabledSocialProfile[_originCheckProfilesProvider] = _originCheckProfilesHandle + ':[' + _originCheckProfilesHandle + ']'
        , 2000

    _cachedHandle = null

    _userRequire = userRequire()

    _userRequire.then (authData) ->
      _cachedHandle = authData.handle

    $scope.updateSocialMediaProfiles = () ->
      ###
      ###

      handle = _cachedHandle

      availableSocialProfiles =
        'twitter'  : localStorageService.get('twitter')
        'google'   : localStorageService.get('google')
        'facebook' : localStorageService.get('facebook')

      if $scope.checkProfiles.length == 1
        availableSocialProfiles[$scope.isCurrentSocial] = "[#{$scope.enabledSocialProfile[$scope.isCurrentSocial]}]"

      try
        social_twitter = uid: $scope.enabledSocialProfile.twitter
      catch e
        console.log 'Twitter not lodged.'

      try
        social_facebook = uid: $scope.enabledSocialProfile.facebook
      catch e
        console.log 'Facebook not lodged.'

      try
        social_google = uid: $scope.enabledSocialProfile.google
      catch e
        console.log 'Google not lodged.'

      enabledSocialProfile =
        'twitter'  : if social_twitter then social_twitter else availableSocialProfiles['twitter']
        'google'   : if social_google then social_google else availableSocialProfiles['google']
        'facebook' : if social_facebook then social_facebook else availableSocialProfiles['facebook']

      profileIds = {}

      if enabledSocialProfile['twitter'] and enabledSocialProfile['twitter'].uid
        _.extend profileIds, twitter: enabledSocialProfile['twitter'].uid

      if enabledSocialProfile['facebook'] and enabledSocialProfile['facebook'].uid
        _.extend profileIds, facebook: enabledSocialProfile['facebook'].uid

      if enabledSocialProfile['google'] and enabledSocialProfile['google'].uid
        _.extend profileIds, google: enabledSocialProfile['google'].uid

      networks = {}

      _.extend networks, { 'networks': profileIds }

      profileService.updateProfile handle, networks

      $timeout ->
        $scope.$apply () ->
          $scope.loadSocialMediaProfiles()
      , 1000

    _type = null
    _l = null

    $scope.connectProfile = (type) ->
      ###
      Update Profile (Rich API Control)

      @param {string} type
        A string of the social network provider. Currently:
          1. twitter
          2. google
          3. facebook
      @return {undefined}
      ###

      checkUpNote =
        type: type

      $scope.social.type = checkUpNote.type

      $scope.$broadcast "auth/user/check-up", checkUpNote

      _userRequire = userRequire()

      _userRequire.then (authData) ->

        # Implied Condition: Current auth data of type is already set
        if authData[type]
          $scope.cachedUserProfile = _cachedUserProfile = if authData[type] and authData[type].cachedUserProfile then authData[type].cachedUserProfile else null

          if $scope.user.bio.content == ''
            $scope.user.bio.content = $scope.cachedUserProfile.description or $scope.cachedUserProfile.name

        else

          # Securing another social network's auth data
          # Get current authData type
          if _type is null
            _type = authData.provider

          # Retrieve current social network auth data by type from client storage
          if _l is null
            _l = sessionStorage.getItem('firebase:session::lovestamp')

            # Set cached social network auth data for re-use
            localStorageService.set _type, _l

          $rootScope.authWith(type, true).then (_authData) ->

            a = _authData

            if _l isnt null

              # Set new social auth data by type
              localStorageService.set type, JSON.stringify(a)

              # Re-establish cached provider from auth data
              #sessionStorage.setItem('firebase:session::lovestamp', _l)

              parsedOldSession = JSON.parse(_l)
              provider = parsedOldSession.provider

              if provider == 'twitter'
                accessId = parsedOldSession[parsedOldSession.provider].id
                accessTokenSecret = parsedOldSession[parsedOldSession.provider].accessTokenSecret
                accessToken = parsedOldSession[parsedOldSession.provider].accessToken
                loginSimple.relogin(provider, accessId, accessToken, accessTokenSecret)
              else
                accessToken = parsedOldSession[parsedOldSession.provider].accessToken
                loginSimple.relogin(provider, accessId, accessToken, accessTokenSecret)

              $scope.updateSocialMediaProfiles()
            else
              notificationService.displayNote 'Profile not set.'
      return

    return

  [
    "$scope"
    "$q"
    "$window"
    "profile.service"
    "user.require"
    "map.service"
    "$timeout"
    "$rootScope"
    "localStorageService"
    "notification.service"
    "login.simple"
    SocialController
  ]
