###*
@fileOverview

     _                     _
    | |                   | |
    | |__  _____ _____  __| |_____  ____
    |  _ \| ___ (____ |/ _  | ___ |/ ___)
    | | | | ____/ ___ ( (_| | ____| |
    |_| |_|_____)_____|\____|_____)_|

@module www/controllers/header

@description

WpHeaderController Module Controller.

###

define [
  "interface"
  "lodash"
  "jquery"
  "config"
  "services/authentication"
  "services/profile"
  'directives/openWindow'
  'directives/relExternal'
], (__interface__, _, $, environment) ->

  mobileMenuContainer = ($timeout, $q, profileService, $compile, userRequire) ->
    ###
    @usage profile personalization of client-based menu capability detection.
    ###

    linker = ($scope, _element) ->
      element = $(_element)

      $scope.mobileForms =
        personalizedMenuExpanded : true
        personalizedMenuOption   : 'desktop'


      $scope.determinedMenuStyle = () ->
        ### Determined Menu Styler ###

        profileUpdateObject = _.extend {}, $scope.mobileForms

        profileService.updateObject handle, profileUpdateObject
        return


      $scope.setPersonalizedMenuConfig = () ->
        ### Set Personalized Menu Config for client. ###
        $scope.mobileForms.personalizedMenuExpanded = ! element.hasClass 'mobile-menu--active'
        element.toggleClass 'mobile-menu--active'
        return


      $scope.getPersonalizedMenuConfig = () ->
        ### Get Personalized Menu Config from both DB store and localStorage and responsive logic. ###
        return


      element.wrap () ->
        "<form></form>"

      toggleForm = """
        <div
          ng-init="getPersonalizedMenuConfig()"
          class="mobile-menu mobile-menu--toggle"
        >

          <span
            ng-click="setPersonalizedMenuConfig()"
            class="fa fa-navicon"
          ></span>

          <mg-checkbox
            id="personalized-menu-option--menu-style"
            name="personalized-menu-option--menu-style"
            ng-model="mobileForms.personalizedMenuExpanded"
            aria-label="Expanded?"
          ></md-checkbox>

        </div>
      """

      configuredTemplate = $compile(toggleForm)($scope)
      element.parent().prepend(configuredTemplate)
      return

    restrict : 'A'
    link     : linker

  mobileMenuItem = ($timeout, $q, profileService) ->
    ###
    @inner
    @param {object::$internal} $timeout
    @param {object::$internal} $q
    @param {object:Factory} profileService
    @usage debounced menu items with throttling.
    ###

    linker = ($scope, element) ->

      $scope.determinedMenuStyle = () ->
        ### Determined Menu Style ###

        profileUpdateObject =
          menuOption: 'mobile'

        profileService.updateObject handle, profileUpdateObject
        return

      #element.removeClass('pt--undefined')
      #element.removeClass('pt--hangout')
      #element.removeClass('pt--mail')
      #element.removeClass('pt--message')
      #element.removeClass('pt--copy')
      #element.removeClass('pt--facebook')
      #element.removeClass('pt--twitter')
      #element.removeClass('pt--google')

      return

    restrict: 'A'
    link: linker

  WpHeaderController = ($scope, userRequire, profileService, $timeout, $compile) ->
    ###
    Webpage Header Controller.

    @inner
    @param {object} $scope
    @param {object:Promise} userRequire
    @param {object:Factory} profileService
    @param {object::$internal} $timeout
    @return {undefined}
    ###

    $scope.module = wpheader: true

    _userRequire = userRequire()

    # Loader
    $scope.loader = $scope.showMap = null

    _userRequire.then (authData) ->
      return  if !authData
      handle = authData.handle
      _profileData = profileService.getUserStatus(handle).$loaded().then (profileData) ->
        _size = undefined

        if profileData.stamps and profileData.stamps.local
          _size = _.size profileData.stamps.local

        $scope.authCheck = profileData.active and _size
        $scope.isRegularUser = profileData.active or _size

        $timeout ->
          $scope.$apply () ->
            $scope.isAuthorized = profileData.role and profileData.role.default and profileData.role.default is 'admin'
            $scope.$broadcast 'isAuthorized', $scope.isAuthorized
        , 0

        # Navigation Labels
        navigationLabels.list = []
        navList = [
          {
            label        : "Dashboard"
            uri          : "dashboard"
            hint         : "Learn about your customers"
            icon         : 'fa-th-large'
            isAuthorized : $scope.authCheck
          }
          {
            label        : "Fan View"
            uri          : "fan-view"
            hint         : "Fan View"
            icon         : 'fa-map-marker'
            isAuthorized : $scope.authCheck
          }
          {
            label        : "Stamps"
            uri          : "stamps"
            hint         : "Review your LoveStamps"
            icon         : "fa-list-ol"
            isAuthorized : $scope.authCheck
          }
          {
            label        : "Social"
            uri          : "social"
            hint         : "Manage your social presence"
            icon         : "fa-pencil-square-o"
            isAuthorized : $scope.authCheck
          }
          {
            label        : "Store"
            uri          : "store"
            hint         : "Lock and load your next LoveStamp"
            icon         : "fa-shopping-cart"
            isAuthorized : true
          }
          {
            label        : "Log Out"
            uri          : "logout"
            hint         : "Shop offline — we dare ya!"
            icon         : "fa-sign-out"
            isAuthorized : true
          }
        ]

        _.forEach navList, (l) ->
          prefix = if window.is_fan then 'fan' else if window.is_merchant then 'merchant' else 'merchant'
          sep = '/'
          l.uri = prefix + sep + l.uri

        navigationLabels.list = navList
        $scope.navigationLabels = navigationLabels

        $timeout ->
          $scope.$apply()
        , 0

    ###*
    Branding Swag
    ###
    navigationLabels = {}
    subNavigationLabels = {}

    ###*
    Model Interfaces
    ###
    $scope.social = {}
    $scope.tmpSocial = {}
    $scope.authObj = {}

    $scope.baseUrl = if window.is_device then ("scripts/") else environment.baseTemplateUrl
    $scope.navigationLabels = $scope.subNavigationLabels = {}

    ###*
    ###
    $scope.layoutSections = {}
    $scope.layoutSections.footerUrl = $scope.baseUrl + "modules/www/controllers/footer/partials/socket-region.html"

    return


  ###
  @description Provide dependencies list for module loader.
  ###
  moduleControllerDeps = [
    "$scope"
    "user.require"
    "profile.service"
    "$timeout"
    "$compile"
    WpHeaderController
  ]

  mobileMenuContainerDeps = [
    '$timeout'
    '$q'
    'profile.service'
    '$compile'
    'user.require'
    mobileMenuContainer
  ]

  mobileMenuItemDeps = [
    '$timeout'
    '$q'
    'profile.service'
    mobileMenuItem
  ]


  ###
  @description Implement
  ###
  __interface__.

    directive('mobileMenuContainer', mobileMenuContainerDeps).

    directive('mobileMenuItem', mobileMenuItemDeps).

    controller("SiteNavController", moduleControllerDeps)

  return

