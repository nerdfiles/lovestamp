
/**
@fileOverview

     _                     _
    | |                   | |
    | |__  _____ _____  __| |_____  ____
    |  _ \| ___ (____ |/ _  | ___ |/ ___)
    | | | | ____/ ___ ( (_| | ____| |
    |_| |_|_____)_____|\____|_____)_|

@module www/controllers/header

@description

WpHeaderController Module Controller.
 */

(function() {
  define(["interface", "lodash", "jquery", "config", "services/authentication", "services/profile", 'directives/openWindow', 'directives/relExternal'], function(__interface__, _, $, environment) {
    var WpHeaderController, mobileMenuContainer, mobileMenuContainerDeps, mobileMenuItem, mobileMenuItemDeps, moduleControllerDeps;
    mobileMenuContainer = function($timeout, $q, profileService, $compile, userRequire) {

      /*
      @usage profile personalization of client-based menu capability detection.
       */
      var linker;
      linker = function($scope, _element) {
        var configuredTemplate, element, toggleForm;
        element = $(_element);
        $scope.mobileForms = {
          personalizedMenuExpanded: true,
          personalizedMenuOption: 'desktop'
        };
        $scope.determinedMenuStyle = function() {

          /* Determined Menu Styler */
          var profileUpdateObject;
          profileUpdateObject = _.extend({}, $scope.mobileForms);
          profileService.updateObject(handle, profileUpdateObject);
        };
        $scope.setPersonalizedMenuConfig = function() {

          /* Set Personalized Menu Config for client. */
          $scope.mobileForms.personalizedMenuExpanded = !element.hasClass('mobile-menu--active');
          element.toggleClass('mobile-menu--active');
        };
        $scope.getPersonalizedMenuConfig = function() {

          /* Get Personalized Menu Config from both DB store and localStorage and responsive logic. */
        };
        element.wrap(function() {
          return "<form></form>";
        });
        toggleForm = "<div\n  ng-init=\"getPersonalizedMenuConfig()\"\n  class=\"mobile-menu mobile-menu--toggle\"\n>\n\n  <span\n    ng-click=\"setPersonalizedMenuConfig()\"\n    class=\"fa fa-navicon\"\n  ></span>\n\n  <mg-checkbox\n    id=\"personalized-menu-option--menu-style\"\n    name=\"personalized-menu-option--menu-style\"\n    ng-model=\"mobileForms.personalizedMenuExpanded\"\n    aria-label=\"Expanded?\"\n  ></md-checkbox>\n\n</div>";
        configuredTemplate = $compile(toggleForm)($scope);
        element.parent().prepend(configuredTemplate);
      };
      return {
        restrict: 'A',
        link: linker
      };
    };
    mobileMenuItem = function($timeout, $q, profileService) {

      /*
      @inner
      @param {object::$internal} $timeout
      @param {object::$internal} $q
      @param {object:Factory} profileService
      @usage debounced menu items with throttling.
       */
      var linker;
      linker = function($scope, element) {
        $scope.determinedMenuStyle = function() {

          /* Determined Menu Style */
          var profileUpdateObject;
          profileUpdateObject = {
            menuOption: 'mobile'
          };
          profileService.updateObject(handle, profileUpdateObject);
        };
      };
      return {
        restrict: 'A',
        link: linker
      };
    };
    WpHeaderController = function($scope, userRequire, profileService, $timeout, $compile) {

      /*
      Webpage Header Controller.
      
      @inner
      @param {object} $scope
      @param {object:Promise} userRequire
      @param {object:Factory} profileService
      @param {object::$internal} $timeout
      @return {undefined}
       */
      var navigationLabels, subNavigationLabels, _userRequire;
      $scope.module = {
        wpheader: true
      };
      _userRequire = userRequire();
      $scope.loader = $scope.showMap = null;
      _userRequire.then(function(authData) {
        var handle, _profileData;
        if (!authData) {
          return;
        }
        handle = authData.handle;
        return _profileData = profileService.getUserStatus(handle).$loaded().then(function(profileData) {
          var navList, _size;
          _size = void 0;
          if (profileData.stamps && profileData.stamps.local) {
            _size = _.size(profileData.stamps.local);
          }
          $scope.authCheck = profileData.active && _size;
          $scope.isRegularUser = profileData.active || _size;
          $timeout(function() {
            return $scope.$apply(function() {
              $scope.isAuthorized = profileData.role && profileData.role["default"] && profileData.role["default"] === 'admin';
              return $scope.$broadcast('isAuthorized', $scope.isAuthorized);
            });
          }, 0);
          navigationLabels.list = [];
          navList = [
            {
              label: "Dashboard",
              uri: "dashboard",
              hint: "Learn about your customers",
              icon: 'fa-th-large',
              isAuthorized: $scope.authCheck
            }, {
              label: "Fan View",
              uri: "fan-view",
              hint: "Fan View",
              icon: 'fa-map-marker',
              isAuthorized: $scope.authCheck
            }, {
              label: "Stamps",
              uri: "stamps",
              hint: "Review your LoveStamps",
              icon: "fa-list-ol",
              isAuthorized: $scope.authCheck
            }, {
              label: "Social",
              uri: "social",
              hint: "Manage your social presence",
              icon: "fa-pencil-square-o",
              isAuthorized: $scope.authCheck
            }, {
              label: "Store",
              uri: "store",
              hint: "Lock and load your next LoveStamp",
              icon: "fa-shopping-cart",
              isAuthorized: true
            }, {
              label: "Log Out",
              uri: "logout",
              hint: "Shop offline — we dare ya!",
              icon: "fa-sign-out",
              isAuthorized: true
            }
          ];
          _.forEach(navList, function(l) {
            var prefix, sep;
            prefix = window.is_fan ? 'fan' : window.is_merchant ? 'merchant' : 'merchant';
            sep = '/';
            return l.uri = prefix + sep + l.uri;
          });
          navigationLabels.list = navList;
          $scope.navigationLabels = navigationLabels;
          return $timeout(function() {
            return $scope.$apply();
          }, 0);
        });
      });

      /**
      Branding Swag
       */
      navigationLabels = {};
      subNavigationLabels = {};

      /**
      Model Interfaces
       */
      $scope.social = {};
      $scope.tmpSocial = {};
      $scope.authObj = {};
      $scope.baseUrl = window.is_device ? "scripts/" : environment.baseTemplateUrl;
      $scope.navigationLabels = $scope.subNavigationLabels = {};

      /**
       */
      $scope.layoutSections = {};
      $scope.layoutSections.footerUrl = $scope.baseUrl + "modules/www/controllers/footer/partials/socket-region.html";
    };

    /*
    @description Provide dependencies list for module loader.
     */
    moduleControllerDeps = ["$scope", "user.require", "profile.service", "$timeout", "$compile", WpHeaderController];
    mobileMenuContainerDeps = ['$timeout', '$q', 'profile.service', '$compile', 'user.require', mobileMenuContainer];
    mobileMenuItemDeps = ['$timeout', '$q', 'profile.service', mobileMenuItem];

    /*
    @description Implement
     */
    __interface__.directive('mobileMenuContainer', mobileMenuContainerDeps).directive('mobileMenuItem', mobileMenuItemDeps).controller("SiteNavController", moduleControllerDeps);
  });

}).call(this);

//# sourceMappingURL=base.js.map
