
/**
@fileOverview

 _
| |
| |__  _____  ___ _____
|  _ \(____ |/___) ___ |
| |_) ) ___ |___ | ____|
|____/\_____(___/|_____)

@module www/controllers/admin
@description

  A glue controller for our admin panel.
 */

(function() {
  define(["interface", "lodash", "numeral", "angular-infinite-scroll", "angular-chosen-localytics", "directives/stats/static", "directives/site/navigation", "directives/stroll", "auth/user/roll-up", "services/profile", "services/stamp", "services/bank", "filters/search"], function(__interface__, _, numeral) {
    var AdminController, moduleController;
    __interface__.directive('selectBoxClickOverlay', [
      '$timeout', function($timeout) {
        var is_loaded, is_visible;
        is_visible = false;
        is_loaded = false;
        return {
          restrict: 'A',
          scope: {
            actionable: '&'
          },
          link: function($scope, element) {
            var a;
            element.prepend('<div class="select-box-click-overlay"></div>');
            a = function(e) {
              var event, s;
              event = document.createEvent('MouseEvents');
              event.initMouseEvent('mousedown', true, true, window);
              s = element.find('select')[0];
              $scope.actionable().then(function() {
                if (is_visible) {
                  is_visible = false;
                  return;
                }
                is_visible = true;
                if (is_loaded) {
                  s.dispatchEvent(event);
                  return;
                }
                return $timeout(function() {
                  s.dispatchEvent(event);
                  return is_loaded = true;
                }, 750);
              });
              e.stopPropagation();
            };
            element.find('.select-box-click-overlay').on('click', a);
            angular.element(document).on('click', function() {
              is_visible = false;
            });
          }
        };
      }
    ]);
    AdminController = function($scope, $http, profileService, stampService, bankService, $timeout, $base64, $location, $q) {
      $scope.stampList = [];
      $scope.localStamps = [];
      $scope.forms = {};
      $scope.forms.stampository = {};
      $scope.forms.budgetControl = {};
      $scope.forms.budgetControl.actualTotal = null;
      $scope.forms.budgetControl.revenueTotal = null;
      $scope.forms.budgetControl.setBudget = null;

      /*
      @namespace getSetBudget
      @return {undefined}
       */
      $scope.getSetBudget = function() {
        var setBudget;
        setBudget = bankService.getSetBudget();
        setBudget.$loaded().then(function(t) {
          return $scope.forms.budgetControl.setBudget = t.$value;
        });
      };

      /*
      @namespace resetBudgetCounter
      @return {undefined}
       */
      $scope.resetBudgetCounter = function() {
        bankService.resetBudget();
      };

      /*
      @namespace startBudgetCron
      @return {undefined}
       */
      $scope.startBudgetCron = function() {
        bankService.startBudgetCron();
      };

      /*
      @namespace buyinusdorbtc
      @retrun {undefined}
       */
      $scope.buyInUsdOrBtc = function() {
        $location.path('https://utxo.us');
      };

      /**
      @namespace adjustBudget
      @description
      
        Redact LoveStamp budget.
       */
      $scope.adjustBudget = function() {
        bankService.updateSetBudget($scope.forms.budgetControl.setBudget);
      };

      /*
      @namespace loadActualTotal
      @description
      
        Load actual total.
      
      @return {undefined}
       */
      $scope.loadActualTotal = function() {
        var actualTotal;
        actualTotal = bankService.getPayoutBudget();
        actualTotal.$loaded().then(function(t) {
          $scope.forms.budgetControl.actualTotal = numeral(t.$value).format(bankService.bitsFormat);
        });
      };

      /*
      @namespace loadRevenueTotal
      @return {undefined}
       */
      $scope.loadRevenueTotal = function() {
        var revenueTotal;
        revenueTotal = bankService.getRevenueTotal();
        revenueTotal.$loaded().then(function(t) {
          $scope.forms.budgetControl.revenueTotal = numeral(t.$value).format(bankService.bitsFormat);
        });
      };

      /*
      @namespace assignStamp
      @description
      
        Assign stamp to arbitrary user.
      
      @return {undefined}
       */
      $scope.assignStamp = function(handle, stamp) {
        stampService.assignStamp(handle, stamp);
      };
      $scope.stampActivationProgressUpdate = function() {};

      /*
      @namespace loadUserList
      @description
      
        This list includes users who do not own stamps, for general assignment 
        of stamps.
      
      @return {undefined}
       */
      $scope.loadUserList = function() {
        var def, userList;
        def = $q.defer();
        userList = profileService.getAllUsers();
        userList.$loaded().then(function(_userList) {
          console.log(_userList);
          $timeout(function() {
            $scope.$apply(function() {
              def.resolve(_userList);
              $scope.users = _userList;
            });
          }, 0);
        });
        return def.promise;
      };

      /*
      @namespace loadStampList
      @description
      
        This list should load all stamps with available ownerships.
      
      @return {undefined}
       */
      $scope.loadStampList = function() {
        var stampList;
        stampList = stampService.getAllStamps();
        stampList.$loaded().then(function(_userList) {
          var user, _i, _len;
          for (_i = 0, _len = _userList.length; _i < _len; _i++) {
            user = _userList[_i];
            if (user.stamps && user.stamps.local) {
              _.forEach(user.stamps.local, function(localStamp, keyName) {
                var avgScore, formattedAvgScore, l, _activationProgressStatus;
                _activationProgressStatus = localStamp.activationProgressStatus || localStamp.statusSaturation;
                avgScore = 0;
                l = localStamp.passes.length;
                _.forEach(localStamp.passes, function(pass) {
                  var score;
                  score = _.result(pass, 'runTotal');
                  if (_.isNumber(score)) {
                    return avgScore += score;
                  }
                });
                avgScore = avgScore / l;
                formattedAvgScore = numeral(avgScore).format('0,0.00');
                localStamp.cleanedAlias = encodeURIComponent($base64.encode(localStamp.alias));
                $scope.newStamp = angular.extend({}, {
                  id: keyName,
                  $id: keyName,
                  user: user,
                  localStamp: localStamp,
                  score: formattedAvgScore,
                  activationProgressStatus: _activationProgressStatus
                });
                return $scope.localStamps.push($scope.newStamp);
              });
            }
          }
        });
      };

      /*
      @namespace search
      @return {Promise} cluster An Elasticsearch cluster.
       */

      /*
      Activation Progress Slider Checker
       */
      $scope.activationProgressSlider = function(currentStamp) {
        currentStamp.localStamp.activationProgressStatus = currentStamp.activationProgressStatus;
        stampService.updateStamp(currentStamp.user.$id, currentStamp);
      };
      $scope.loadMoreStamps = function() {};
    };
    return moduleController = ['$scope', '$http', 'profile.service', 'stamp.service', 'bank.service', "$timeout", "$base64", "$location", "$q", AdminController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
