
/**
@fileOverview

                   _
                  (_)       _
  ____ _____  ____ _  ___ _| |_ _____  ____
 / ___) ___ |/ _  | |/___|_   _) ___ |/ ___)
| |   | ____( (_| | |___ | | |_| ____| |
|_|   |_____)\___ |_(___/   \__)_____)_|
            (_____|

@module www/controllers/admin
@description

  "Register" Module Controller.
 */

(function() {
  define(["interface", "lodash", "numeral", "directives/stats/static", "directives/site/navigation", "auth/user/roll-up", "services/profile", "services/stamp", "filters/search"], function(__interface__, _, numeral) {
    var RegisterController;
    RegisterController = function($scope, $stateParams, profileService, stampService) {
      $scope.updateCredentials = {
        active: true
      };
      $scope.stampList = [];
      $scope.localStamps = [];
      profileService.updateUser($stateParams.username, $scope.updateCredentials);

      /*
      @namespace loadUserList
       */
      $scope.loadUserList = function() {
        var userList;
        userList = profileService.getAllUsers();
        return userList.$loaded().then(function(_userList) {
          return $scope.users = _userList;
        });
      };

      /*
      @namespace loadStampList
       */
      $scope.loadStampList = function() {
        var stampList;
        stampList = stampService.getAllStamps();
        return stampList.$loaded().then(function(_userList) {
          var user, _i, _len, _results;
          _results = [];
          for (_i = 0, _len = _userList.length; _i < _len; _i++) {
            user = _userList[_i];
            if (user.stamps && user.stamps.local) {
              _results.push(_.forEach(user.stamps.local, function(localStamp) {
                var avgScore, formattedAvgScore, l;
                avgScore = 0;
                l = localStamp.passes.length;
                _.forEach(localStamp.passes, function(pass) {
                  var score;
                  score = _.result(pass, 'runTotal');
                  if (_.isNumber(score)) {
                    return avgScore += score;
                  }
                });
                avgScore = avgScore / l;
                formattedAvgScore = numeral(avgScore).format('0,0.00');
                return $scope.localStamps.push({
                  user: user,
                  localStamp: localStamp,
                  score: formattedAvgScore
                });
              }));
            } else {
              _results.push(void 0);
            }
          }
          return _results;
        });
      };
    };
    return ["$scope", "$stateParams", "profile.service", "stamp.service", RegisterController];
  });

}).call(this);

//# sourceMappingURL=register.js.map
