###*
@fileOverview

                   _
                  (_)       _
  ____ _____  ____ _  ___ _| |_ _____  ____
 / ___) ___ |/ _  | |/___|_   _) ___ |/ ___)
| |   | ____( (_| | |___ | | |_| ____| |
|_|   |_____)\___ |_(___/   \__)_____)_|
            (_____|

@module www/controllers/admin
@description

  "Register" Module Controller.

###

define [
  "interface"
  "lodash"
  "numeral"
  "directives/stats/static"
  "directives/site/navigation"
  "auth/user/roll-up"
  "services/profile"
  "services/stamp"
  "filters/search"
], (__interface__, _, numeral) ->

  RegisterController = ($scope, $stateParams, profileService, stampService) ->

    $scope.updateCredentials =
      active: true
    $scope.stampList = []
    $scope.localStamps = []

    profileService.updateUser($stateParams.username, $scope.updateCredentials)

    ###
    @namespace loadUserList
    ###

    $scope.loadUserList = () ->
      userList = profileService.getAllUsers()
      userList.$loaded().then (_userList) ->
        $scope.users = _userList

    ###
    @namespace loadStampList
    ###

    $scope.loadStampList = () ->

      stampList = stampService.getAllStamps()

      stampList.$loaded().then (_userList) ->

        # We're pulling all users first. Not scalable.
        for user in _userList
          if user.stamps and user.stamps.local
            _.forEach user.stamps.local, (localStamp) ->

              avgScore = 0
              l = localStamp.passes.length

              _.forEach localStamp.passes, (pass) ->
                score = _.result pass, 'runTotal'
                if _.isNumber(score)
                  avgScore += score

              avgScore = avgScore / l
              formattedAvgScore = numeral(avgScore).format '0,0.00'

              $scope.localStamps.push
                user: user
                localStamp: localStamp
                score: formattedAvgScore

    return

  [
    "$scope"
    "$stateParams"
    "profile.service"
    "stamp.service"
    RegisterController
  ]
