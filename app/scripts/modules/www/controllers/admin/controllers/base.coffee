###*
@fileOverview

 _
| |
| |__  _____  ___ _____
|  _ \(____ |/___) ___ |
| |_) ) ___ |___ | ____|
|____/\_____(___/|_____)

@module www/controllers/admin
@description

  A glue controller for our admin panel.

###

define [
  "interface"
  "lodash"
  "numeral"
  "angular-infinite-scroll" # @dependency angular.module 'admin/controllers', ['infinite-scroll']
  "angular-chosen-localytics" # @dependency angular.module 'admin/controllers', ['localytics.directives']
  "directives/stats/static"
  "directives/site/navigation"
  "directives/stroll"
  "auth/user/roll-up"
  "services/profile"
  "services/stamp"
  "services/bank"
  "filters/search"
], (__interface__, _, numeral) ->

  __interface__.directive 'selectBoxClickOverlay', [
    '$timeout'
    ($timeout) ->
      is_visible = false
      is_loaded = false
      restrict: 'A'
      scope:
        actionable: '&'
      link: ($scope, element) ->

        element.prepend('<div class="select-box-click-overlay"></div>')

        a = (e) ->

          event = document.createEvent('MouseEvents')
          event.initMouseEvent('mousedown', true, true, window)
          s = element.find('select')[0]

          $scope.actionable().then () ->

            if is_visible
              is_visible = false
              return
            is_visible = true

            if is_loaded
              s.dispatchEvent(event)
              return

            $timeout ->
              s.dispatchEvent(event)
              is_loaded = true
            , 750

          e.stopPropagation()

          return

        element.find('.select-box-click-overlay').on 'click', a

        angular.element(document).on('click', () ->
          is_visible = false
          return
        )
        return
  ]

  #AdminController = ($scope, $http, profileService, stampService, bankService, $timeout, es, esFactory, $base64) ->
  AdminController = ($scope, $http, profileService, stampService, bankService, $timeout, $base64, $location, $q) ->

    $scope.stampList = []
    $scope.localStamps = []
    $scope.forms = {}
    $scope.forms.stampository = {}
    $scope.forms.budgetControl = {}
    $scope.forms.budgetControl.actualTotal = null
    $scope.forms.budgetControl.revenueTotal = null
    $scope.forms.budgetControl.setBudget = null

    ###
    @namespace getSetBudget
    @return {undefined}
    ###

    $scope.getSetBudget = () ->
      setBudget = bankService.getSetBudget()
      setBudget.$loaded().then (t) ->
        $scope.forms.budgetControl.setBudget = t.$value
      return


    ###
    @namespace resetBudgetCounter
    @return {undefined}
    ###

    $scope.resetBudgetCounter = () ->
      bankService.resetBudget()
      return


    ###
    @namespace startBudgetCron
    @return {undefined}
    ###

    $scope.startBudgetCron = () ->
      bankService.startBudgetCron()
      return


    ###
    @namespace buyinusdorbtc
    @retrun {undefined}
    ###

    $scope.buyInUsdOrBtc = () ->
      $location.path('https://utxo.us')
      return


    ###*
    @namespace adjustBudget
    @description

      Redact LoveStamp budget.

    ###

    $scope.adjustBudget = () ->
      bankService.updateSetBudget $scope.forms.budgetControl.setBudget
      return


    ###
    @namespace loadActualTotal
    @description

      Load actual total.

    @return {undefined}
    ###

    $scope.loadActualTotal = () ->
      actualTotal = bankService.getPayoutBudget()
      actualTotal.$loaded().then (t) ->
        $scope.forms.budgetControl.actualTotal = numeral(t.$value).format bankService.bitsFormat
        return
      return

    ###
    @namespace loadRevenueTotal
    @return {undefined}
    ###

    $scope.loadRevenueTotal = () ->
      revenueTotal = bankService.getRevenueTotal()
      revenueTotal.$loaded().then (t) ->
        $scope.forms.budgetControl.revenueTotal = numeral(t.$value).format bankService.bitsFormat
        return
      return

    ###
    @namespace assignStamp
    @description

      Assign stamp to arbitrary user.

    @return {undefined}
    ###

    $scope.assignStamp = (handle, stamp) ->
      stampService.assignStamp(handle, stamp)
      return

    $scope.stampActivationProgressUpdate = () ->
      return

    ###
    @namespace loadUserList
    @description

      This list includes users who do not own stamps, for general assignment 
      of stamps.

    @return {undefined}
    ###

    $scope.loadUserList = () ->
      def = $q.defer()
      userList = profileService.getAllUsers()
      userList.$loaded().then (_userList) ->
        console.log _userList
        $timeout ->
          $scope.$apply () ->
            def.resolve _userList
            $scope.users = _userList
            return
          return
        , 0
        return
      def.promise

    ###
    @namespace loadStampList
    @description

      This list should load all stamps with available ownerships.

    @return {undefined}
    ###

    $scope.loadStampList = () ->

      stampList = stampService.getAllStamps()

      stampList.$loaded().then (_userList) ->

        # We're pulling all users first. Not scalable.
        for user in _userList
          if user.stamps and user.stamps.local

            # Collect stamp passes from Control Stamp pressings.
            _.forEach user.stamps.local, (localStamp, keyName) ->

              _activationProgressStatus = localStamp.activationProgressStatus or localStamp.statusSaturation

              avgScore = 0
              l = localStamp.passes.length

              # Calculate target average.
              _.forEach localStamp.passes, (pass) ->
                score = _.result pass, 'runTotal'
                if _.isNumber(score)
                  avgScore += score

              avgScore = avgScore / l
              formattedAvgScore = numeral(avgScore).format '0,0.00'

              localStamp.cleanedAlias = encodeURIComponent($base64.encode(localStamp.alias))

              $scope.newStamp = angular.extend {},
                id                       : keyName
                $id                      : keyName
                user                     : user
                localStamp               : localStamp
                score                    : formattedAvgScore
                activationProgressStatus : _activationProgressStatus

              $scope.localStamps.push $scope.newStamp

        return

      return


    ###
    @namespace search
    @return {Promise} cluster An Elasticsearch cluster.
    ###

    #$scope.search = () ->
      #es.cluster.state(metric: [
        #'cluster_name'
        #'nodes'
        #'master_node'
        #'version'
      #]).then((resp) ->
        #$scope.clusterState = resp
        #$scope.error = null
        #return
      #).catch (err) ->
        #$scope.clusterState = null
        #$scope.error = err
        ## if the err is a NoConnections error, then the client was not able to
        ## connect to elasticsearch. In that case, create a more detailed error
        ## message
        #if err instanceof esFactory.errors.NoConnections
          #$scope.error = new Error('Unable to connect to elasticsearch. ' + 'Make sure that it is running and listening at http://localhost:9200')
        #return


    ###
    Activation Progress Slider Checker
    ###
    $scope.activationProgressSlider = (currentStamp) ->
      currentStamp.localStamp.activationProgressStatus = currentStamp.activationProgressStatus
      stampService.updateStamp currentStamp.user.$id, currentStamp
      return

    $scope.loadMoreStamps = () ->
      return

    return

  moduleController = [
    '$scope'
    '$http'
    'profile.service'
    'stamp.service'
    'bank.service'
    "$timeout"
    #"es"
    #"esFactory"
    "$base64"
    "$location"
    "$q"
    AdminController
  ]
