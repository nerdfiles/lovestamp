
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/store  

 *# description

Store Module Controller.
 */

(function() {
  define(["interface", "lodash", "jquery", "bitcoin-prices", "chance", "config", "directives/site/navigation", "auth/user/roll-up", "services/profile", "services/stamp", "services/map", "services/payments", "services/notification", "services/bank", 'utils/angular-qr', "services/browser"], function(__interface__, _, $, bitcoinprices, Chance, environment) {
    var DialogController, StoreController, _formType, _quantity, _token;
    _token = void 0;
    _formType = void 0;
    _quantity = void 0;
    __interface__.directive('focusMe', function($timeout, $parse) {
      return {
        link: function(scope, element, attrs) {
          var model;
          model = $parse(attrs.focusMe);
          scope.$watch(model, function(value) {
            if (value === true) {
              return $timeout(function() {
                return element[0].focus();
              });
            }
          });
          return element.bind('blur', function() {
            return scope.$apply(model.assign(scope, false));
          });
        }
      };
    });
    __interface__.directive('usdPriceConvert', [
      '$compile', function($compile) {
        return {

          /*
          USD Price Conversion Directive.
           */
          restrict: "A",
          priority: -1,
          transclude: true,
          template: "<span data-usd-price='{{quantity}}'>{{quantity}} {{usdCurrency}}</span>",
          scope: {
            quantity: '=quantity',
            stampPlans: '=stampPlans',
            stampPlan: '=stampPlan',
            usdCurrency: '=usdCurrency',
            currentStampPlan: '=currentStampPlan',
            storeHouseStamp: '=storeHouseStamp',
            showStamps: '=showStamps'
          },
          link: function($scope, element, attrs) {
            if ($scope.stampPlan.impressions) {
              $scope.quantity = $scope.stampPlan.cost;
              $scope.stampPlan.show = $scope.quantity;
              if ($scope.storeHouseStamp['Fuel']) {
                $scope.storeHouseStamp['Fuel'] === false;
              } else {
                $scope.storeHouseStamp['Fuel'] === true;
              }
            } else {
              $scope.quantity = $scope.stampPlan.cost;
              $scope.stampPlan.show = $scope.quantity;
              if ($scope.storeHouseStamp['Stamps']) {
                $scope.storeHouseStamp['Stamps'] === false;
              } else {
                $scope.storeHouseStamp['Stamps'] === true;
              }
            }
            $scope._stampPlan = _.extend({}, $scope.stampPlan);
            $scope.$on("planUpdated:" + $scope.stampPlan.label, function(event, object) {
              if ($scope.stampPlan.impressions) {
                $scope.quantity = (object.updatedQuantity / 300) * $scope.stampPlan.cost;
                $scope.stampPlan.show = (object.updatedQuantity / 300) * $scope.stampPlan.cost;
              } else {
                $scope.quantity = object.updatedQuantity * $scope.stampPlan.cost;
                $scope.stampPlan.show = object.updatedQuantity * $scope.stampPlan.cost;
              }
            });
          }
        };
      }
    ]).controller('DialogController', [
      '$scope', 'user', '$http', '$mdDialog', 'user.require', 'profile.service', 'payments.service', '$timeout', 'map.service', 'customerItem', 'bank.service', 'notification.service', '$location', '$window', "browser.service", DialogController = function($scope, user, $http, $mdDialog, userRequire, profileService, paymentsService, $timeout, mapService, customerItem, bankService, notificationService, $location, $window, browserService) {

        /*
        Dialog Roaming Controller.
         */
        var pn, populateBitcoinCheckout, _userRequire;
        $scope.customerItem = customerItem;
        console.log(customerItem);
        $scope._price = $scope.customerItem.label === 'Impressions' ? ($scope.customerItem.qty / 300) * 30 : $scope.customerItem.qty * 50;
        $scope.price = numeral($scope._price).format(bankService.currencyFormat);
        $scope.label = $scope.customerItem.label === 'Stamps' && $scope.customerItem.qty === 1 ? 'Stamp' : $scope.customerItem.label;
        _userRequire = userRequire();
        $scope.showTermsPage = function() {

          /*
          Show Terms Page
           */
          var config;
          config = {
            url: 'http://lovestamp.io/legal-mumbo-jumbo',
            target: '_blank',
            options: []
          };
          browserService.open(config);
        };
        $scope.showPrivacyPage = function() {

          /*
          Show Privacy Page
           */
          var config;
          config = {
            url: 'https://stripe.com/us/privacy',
            target: '_blank',
            options: []
          };
          browserService.open(config);
        };
        $scope.paymentConstruct = {
          type: null,
          address: null,
          bitcoinAddy: null,
          email: null,
          expiry: null,
          number: null,
          security: null,
          rememberMe: null,
          cardPhone: null
        };
        $scope.address = {
          shipping: {
            contentEditable: void 0,
            contentUpdated: void 0,
            content: '',
            editing: false
          }
        };
        $scope.load = function() {

          /*
          load
           */
          _userRequire = userRequire();
          return _userRequire.then(function(authData) {
            var handle;
            handle = authData.handle;
            return profileService.getProfile(handle).then(function(profileData) {
              var networks, networksList, originUserId, u, _authData, _networks;
              _networks = _.last(_.filter(_.map(profileData, function(d, k) {
                if (d.networks) {
                  return d;
                }
              })));
              networks = _networks.networks;
              networksList = _.toArray(networks);
              networksList = _.filter(networksList, function(a) {
                return a.indexOf('[') !== -1;
              });
              _authData = _.last(networksList);
              u = _authData.split(':');
              originUserId = $scope.originUserId = u[1];
              originUserId = originUserId.replace(']', '');
              return profileService.getProfileById(originUserId).then(function(cleanedProfileData) {
                return $timeout(function() {
                  return $scope.$apply(function() {
                    var addressDeliveryObject;
                    $scope._profile = cleanedProfileData;
                    addressDeliveryObject = $scope._profile.addressShipping;
                    if (addressDeliveryObject) {
                      return $timeout(function() {
                        return $scope.$apply(function() {
                          return $scope.address.shipping.content = $scope.paymentConstruct.address = [addressDeliveryObject.street_number, " ", addressDeliveryObject.route, "\n", addressDeliveryObject.locality, ", ", addressDeliveryObject.administrative_area_level_1, " ", addressDeliveryObject.postal_code].join('');
                        });
                      }, 0);
                    }
                  });
                });
              });
            });
          });
        };
        $scope.fieldRowEditDeliveryAddress = function(content) {

          /*
           * fieldRowEditDeliveryAddress
           */
          _userRequire = userRequire();
          _userRequire.then(function(authData) {
            var handle, verifiedAddress;
            handle = authData.handle;
            if (!$scope.address.shipping.editing) {
              $scope.address.shipping.editing = true;
              return $scope.address.shipping.contentEditable = $scope.$watch(content, function(newValue, oldValue) {
                if (newValue !== '' && newValue !== oldValue) {
                  return $scope.address.shipping.contentUpdated = true;
                } else {
                  return $scope.address.shipping.contentUpdated = false;
                }
              });
            } else {
              return verifiedAddress = mapService.Owlish.verifyAddress($scope.address.shipping.content).then(function(updatedProfile) {
                profileService.updateProfileBranch(handle, content, updatedProfile.data);
                $scope.address.shipping.editing = false;
                $scope.address.shipping.contentEditable();
                _userRequire = userRequire();
                return _userRequire.then(function(authData) {
                  handle = authData.handle;
                  return profileService.getProfile(handle).then(function(profileData) {
                    var networks, networksList, originUserId, u, _authData, _networks;
                    _networks = _.last(_.filter(_.map(profileData, function(d, k) {
                      if (d.networks) {
                        return d;
                      }
                    })));
                    networks = _networks.networks;
                    networksList = _.toArray(networks);
                    networksList = _.filter(networksList, function(a) {
                      return a.indexOf('[') !== -1;
                    });
                    _authData = _.last(networksList);
                    u = _authData.split(':');
                    originUserId = $scope.originUserId = u[1];
                    originUserId = originUserId.replace(']', '');
                    return profileService.getProfileById(originUserId).then(function(cleanedProfileData) {
                      var addressDeliveryObject;
                      $scope._profile = cleanedProfileData;
                      addressDeliveryObject = $scope._profile.addressShipping;
                      if (addressDeliveryObject) {
                        return $timeout(function() {
                          return $scope.$apply(function() {
                            return $scope.address.shipping.content = $scope.paymentConstruct.address = [addressDeliveryObject.street_number, " ", addressDeliveryObject.route, "\n", addressDeliveryObject.locality, ", ", addressDeliveryObject.administrative_area_level_1, " ", addressDeliveryObject.postal_code].join('');
                          });
                        }, 0);
                      }
                    });
                  });
                });
              });
            }
          });
        };
        populateBitcoinCheckout = function(status, response) {
          if (status === 200) {
            return $timeout(function() {
              return $scope.$apply(function() {
                $scope.exchangeRateBasedAmount = response.bitcoin_amount / 100000000;
                $scope.bitcoin_address = $scope.paymentConstruct.bitcoinAddy = response.inbound_address;
                $scope.qrBitcoinAddressHref = response.bitcoin_uri;
                $scope.qrBitcoinAddress = $scope.bitcoin_address;
                return $scope.poll_response_id = response.id;
              });
            }, 0);
          } else {
            return notificationService.displayError(response.error.message);
          }
        };
        $scope.$watch('poll_response_id', function(newVal) {
          if (newVal) {
            return Stripe.bitcoinReceiver.pollReceiver(newVal, function() {
              var postData, postUrl;
              postUrl = 'payment/bitcoin/' + $scope.authData.handle + '/' + _formType;
              postData = {
                quantity: $scope.customerItem.qty,
                currency: 'usd',
                response_id: newVal,
                description: $scope.customerItem._qty + ' ' + $scope.label + ' for ' + $scope.paymentConstruct.email,
                $meta: $scope.paymentConstruct,
                email: $scope.paymentConstruct.email,
                metadata: $scope.paymentConstruct,
                rememberMe: $scope.paymentConstruct.rememberMe
              };
              return $http.post(postUrl, postData).success(function(successResponse) {
                return console.log(successResponse);
              });
            });
          }
        });
        pn = null;
        $scope.validateAuth = function() {
          var postData, postUrl;
          if ($scope.paymentConstruct.email === null) {
            notificationService.displayError('Please provide an e-mail');
            $scope.notificationError = 'Please provide an e-mail';
            return;
          }
          pn = $scope.paymentConstruct.phoneNumber;
          if (pn) {
            if (parseInt(pn.replace('(', '').replace(')', '').replace(/-/g, ''), 10) > 0) {
              postUrl = '/authorize';
              postData = {
                number: $scope.paymentConstruct.phoneNumber
              };
              return $http.post(postUrl, postData).success(function(authData) {
                $scope.c = authData;
                return notificationService.displayNote('Authorization successful!');
              });
            }
          }
        };
        $scope.resolveAuth = function() {

          /*
          Resolve Authentication via SMS through Twilio.
           */
          if ($scope.paymentConstruct.email === null) {
            notificationService.displayError('Please provide an e-mail');
            $scope.notificationError = 'Please provide an e-mail';
            delete $scope.c;
            return;
          }
          if ($scope.paymentConstruct.authy === $scope.c) {
            $scope.paymentConstruct.rememberMe = true;
            $scope.showRememberMeInput = true;
          } else {
            $scope.paymentConstruct.rememberMe = false;
            $scope.showRememberMeInput = false;
            notificationService.displayError('Try again!');
          }
          return delete $scope.c;
        };
        $scope.$watch('showRememberMeInput', function(newVal) {
          if (newVal === true && $scope.paymentConstruct.email === null) {
            notificationService.displayError('Please provide an e-mail');
            return $scope.notificationError = 'Please provide an e-mail';
          }
        });
        $scope.$watch('paymentConstruct.type', function(newVal, oldVal) {
          if (newVal && newVal === '2') {
            return Stripe.bitcoinReceiver.createReceiver({
              amount: $scope._price * 100,
              currency: 'usd',
              description: $scope.customerItem.qty + ' ' + $scope.label + ' for ' + $scope.paymentConstruct.email,
              email: $scope.paymentConstruct.email,
              metadata: $scope.paymentConstruct
            }, populateBitcoinCheckout);
          }
        });
        $scope.handleStripe = function(status, response) {

          /*
          @param {string} status
          @param {object} response
          @return {Promise:object} Returns the status of the current transaction.
           */
          var postData, postUrl, token;
          if (response.error) {
            notificationService.displayError(response.error.message);
          } else {
            token = response.id;
            _token = token;
            postUrl = 'payment/' + $scope.authData.handle + '/' + _formType;
            postData = {
              quantity: $scope.customerItem._qty,
              currency: 'usd',
              token: _token,
              description: $scope.customerItem._qty + ' ' + $scope.label + ' for ' + $scope.paymentConstruct.email,
              email: $scope.paymentConstruct.email,
              $meta: $scope.paymentConstruct,
              metadata: $scope.paymentConstruct,
              rememberMe: $scope.paymentConstruct.rememberMe
            };
            _userRequire.then(function(authData) {
              var handle;
              handle = authData.handle;
              return profileService.updateUserData(handle, {
                purchaseEmail: $scope.paymentConstruct.mail
              });
            });
            $http.post(postUrl, postData).success(function(paymentData) {
              if (!paymentData) {
                return notificationService.displayNote('Please try again!');
              }
              return notificationService.displayNote('Payment received!');
            }).error(function(error) {
              return notificationService.displayError(error);
            });
          }
        };
        $scope.hide = function() {

          /*
          Hide
           */
          $mdDialog.hide();
        };
        $scope.cancel = function() {

          /*
          Cancel
           */
          $mdDialog.cancel();
        };
        return $scope.makePayment = function($form) {

          /*
          Make Payment
           */
          $mdDialog.hide($form);
        };
      }
    ]);
    StoreController = function($scope, user, $http, $mdDialog, $rootScope, $q, $window, profileService, stampService, userRequire, $timeout, notificationService, $location, $anchorScroll) {

      /*
      Store Module Controller.
       */
      var multiplier, userAccount;
      multiplier = 300;
      $scope.currentStampPlan = {
        "Stamps": 1,
        "Impressions": 300
      };
      $scope.storeHouseStamp = {};
      $scope.storeHouseStamp['Fuel'] = true;
      $scope.storeHouseStamp['Stamps'] = true;
      $scope.loadExchangeRateData = function() {

        /*
        Load Exchange Rate Data.
         */
        return $timeout(function() {
          return $scope.initExchangePrices();
        }, 2000);
      };
      $scope.showStripePaymentForm = function(event, stampPlan, quantity) {

        /*
        Show Microtransaction Payment Form.
         */
        var $body, preparedIndividualItem, stampObject, type, typeString;
        $scope.currentStampPlan[stampPlan.label] = quantity;
        typeString = event.target.parentElement.parentElement.parentElement.textContent;
        type = typeString.indexOf('Stamps') !== -1 ? 'stamps' : 'impressions';
        _formType = type;
        $scope.selectedValue = _quantity = event.target.getAttribute('value');
        stampObject = {
          qty: quantity,
          _qty: _quantity,
          name: $scope.currentStampPlan,
          label: stampPlan.label,
          $meta: stampPlan
        };
        preparedIndividualItem = _.extend({}, stampObject);
        $body = document.querySelector('body');
        $location.hash('top');
        $anchorScroll();
        angular.element($body).addClass('extended-background');
        return $mdDialog.show({
          disableParentScroll: false,
          hasBackdrop: true,
          controller: 'DialogController',
          templateUrl: environment.baseTemplateUrl + 'partials/payMaker.html',
          targetEvent: event,
          preserveScope: true,
          focusOnOpen: false,
          locals: {
            customerItem: preparedIndividualItem
          }
        }).then(function($form) {
          if ($form && $form.$valid) {
            return console.log($form);
          }
        }, function() {
          notificationService.displayError('Are you sure?');
          return angular.element($body).removeClass('extended-background');
        });
      };
      userAccount = void 0;
      user.then(function(authData) {
        if (authData) {
          userAccount = '@' + authData[authData.provider].username;
          $scope.userActiveStatus = profileService.getUserStatus(userAccount);
          return $scope.userActiveStatus.$loaded().then(function(profileData) {
            var hasStamps, totalStamps;
            hasStamps = profileData.stamps && profileData.stamps.local;
            totalStamps = 0;
            if (hasStamps) {
              totalStamps = _.size(profileData.stamps.local);
            }
            if (totalStamps > 0) {
              return $rootScope.$emit('user:hasStampOrIsActive', true);
            } else {
              return $rootScope.$emit('user:hasStampOrIsActive', false);
            }
          });
        }
      });
      $scope.forms = {
        stampAsItemForm: {},
        stampAsPlanForm: {}
      };
      $scope.pageSectionTitle = 'Store';
      $scope.stampList = [];
      $scope.stampPlans = {
        Stamps: 1,
        Impressions: 300
      };
      $scope.initializedQuantity = function(stampPlan, updatedQuantity) {

        /*
        Initialize the property for dynamic Store drop down select.
         */
        var qty;
        if (stampPlan.impressions) {
          qty = updatedQuantity / 10;
        } else {
          qty = stampPlan.cost * updatedQuantity;
        }
        $scope.stampPlans[stampPlan.label] = qty;
        $scope.initExchangePrices(updatedQuantity);
        $scope.$broadcast("planUpdated:" + stampPlan.label, {
          stampPlan: stampPlan,
          updatedQuantity: updatedQuantity
        });
        return qty;
      };
      $scope.stampPlanTitle = 'Store';
      $scope.stampPlansList = [
        {
          label: 'Stamps',
          count: 1,
          type: 'The LoveStamp Pack',
          currency: 'USD',
          cost: 50,
          notes: ['1 Stamp & Registration Code', '1 Counter Display', 'Welcome Card & Instructions', 'Advertising on the LoveStamp Map'],
          addToCart: {
            buttonLabel: 'Buy Now',
            popoutTitle: 'Quantity',
            options: [1, 2, 3, 4, 5]
          }
        }, {
          label: 'Impressions',
          impressions: 300,
          impressionsLabel: 'Fuel',
          currency: 'USD',
          cost: 30,
          notes: ['300 Stamp Impressions', 'Valid for 30 Days', 'Unused Stamp Impressions Don\'t Roll Over', 'Stamp Impressions are Distributed to All Stamps'],
          expiryLabel: 'Expiry',
          expiryLimit: 2,
          expiryUnit: 'months',
          expiryHelpNote: 'after 1st stamp impression',
          addToCart: {
            buttonLabel: 'Buy Now',
            popoutTitle: 'Quantity',
            options: [1 * multiplier, 2 * multiplier, 3 * multiplier, 4 * multiplier, 5 * multiplier, 6 * multiplier, 7 * multiplier, 8 * multiplier, 9 * multiplier, 10 * multiplier, 11 * multiplier, 12 * multiplier]
          }
        }
      ];
      $scope.initExchangePrices = function() {
        return $timeout(function() {
          return $scope.$apply(function() {
            return bitcoinprices.init({
              url: "https://api.bitcoinaverage.com/ticker/all",
              marketRateVariable: "24h_avg",
              defaultCurrency: 'USD',
              jQuery: $,
              priceAttribute: "data-usd-price",
              priceOrignalCurrency: 'USD',
              symbols: {
                "USD": "<i class='fa fa-usd'></i>"
              },
              currencies: ["USD"],
              ux: {
                clickPrices: true,
                menu: false,
                clickableCurrencySymbol: true
              }
            });
          });
        }, 50);
      };
    };
    return ["$scope", "user", "$http", "$mdDialog", "$rootScope", "$q", "$window", "profile.service", "stamp.service", "user.require", "$timeout", "notification.service", "$location", "$anchorScroll", "browser.service", StoreController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
