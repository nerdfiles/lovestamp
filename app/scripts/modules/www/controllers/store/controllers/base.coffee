###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/store  

## description

Store Module Controller.

###

define [
  "interface"
  "lodash"
  "jquery"
  "bitcoin-prices"
  "chance"
  "config"
  "directives/site/navigation"
  "auth/user/roll-up"
  "services/profile"
  "services/stamp"
  "services/map"
  "services/payments"
  "services/notification"
  "services/bank"
  'utils/angular-qr'
  "services/browser"
], (__interface__, _, $, bitcoinprices, Chance, environment) ->

  _token = undefined
  _formType = undefined
  _quantity = undefined

  __interface__.directive('focusMe', ($timeout, $parse) ->
    return {
      link: (scope, element, attrs) ->
        model = $parse(attrs.focusMe)
        scope.$watch(model, (value) ->
          if(value == true) 
            $timeout(() ->
              element[0].focus()
            )
        )
        element.bind('blur', () ->
           scope.$apply(model.assign(scope, false))
        )
    }
  )

  __interface__.

      directive('usdPriceConvert', [
        '$compile'
        ($compile) ->
          ###
          USD Price Conversion Directive.
          ###

          restrict: "A"
          priority: -1
          transclude: true
          template: "<span data-usd-price='{{quantity}}'>{{quantity}} {{usdCurrency}}</span>"
          scope:
            quantity         : '=quantity'
            stampPlans       : '=stampPlans'
            stampPlan        : '=stampPlan'
            usdCurrency      : '=usdCurrency'
            currentStampPlan : '=currentStampPlan'
            storeHouseStamp  : '=storeHouseStamp'
            showStamps       : '=showStamps'
          link: ($scope, element, attrs) ->

            if $scope.stampPlan.impressions
                $scope.quantity = $scope.stampPlan.cost
                $scope.stampPlan.show = $scope.quantity

                if $scope.storeHouseStamp['Fuel']
                    $scope.storeHouseStamp['Fuel'] == false
                else
                    $scope.storeHouseStamp['Fuel'] == true
            else
                $scope.quantity = $scope.stampPlan.cost
                $scope.stampPlan.show = $scope.quantity

                if $scope.storeHouseStamp['Stamps']
                    $scope.storeHouseStamp['Stamps'] == false
                else
                    $scope.storeHouseStamp['Stamps'] == true


            $scope._stampPlan = _.extend {}, $scope.stampPlan

            $scope.$on "planUpdated:#{$scope.stampPlan.label}", (event, object) ->

                if $scope.stampPlan.impressions
                    $scope.quantity = (object.updatedQuantity / 300) * $scope.stampPlan.cost
                    $scope.stampPlan.show = (object.updatedQuantity / 300) * $scope.stampPlan.cost
                    #if $scope.storeHouseStamp['Fuel']
                        #$scope.storeHouseStamp['Fuel'] == false
                    #else
                        #$scope.storeHouseStamp['Fuel'] == true
                else
                    $scope.quantity = object.updatedQuantity * $scope.stampPlan.cost
                    $scope.stampPlan.show = object.updatedQuantity * $scope.stampPlan.cost
                    #if $scope.storeHouseStamp['Stamps']
                        #$scope.storeHouseStamp['Stamps'] == false
                    #else
                        #$scope.storeHouseStamp['Stamps'] == true

                return

            return
      ]).

      controller 'DialogController', [
          '$scope'
          'user'
          '$http'
          '$mdDialog'
          'user.require'
          'profile.service'
          'payments.service'
          '$timeout'
          'map.service'
          'customerItem'
          'bank.service'
          'notification.service'
          '$location'
          '$window'
          "browser.service"
          DialogController = ($scope, user, $http, $mdDialog, userRequire, profileService, paymentsService, $timeout, mapService, customerItem, bankService, notificationService, $location, $window, browserService) ->
            ###
            Dialog Roaming Controller.
            ###

            $scope.customerItem = customerItem
            console.log customerItem
            $scope._price = if $scope.customerItem.label is 'Impressions' then (($scope.customerItem.qty / 300) * 30) else ($scope.customerItem.qty * 50)
            $scope.price = numeral($scope._price).format(bankService.currencyFormat)
            $scope.label = if $scope.customerItem.label is 'Stamps' and $scope.customerItem.qty == 1 then 'Stamp' else $scope.customerItem.label

            _userRequire = userRequire()

            $scope.showTermsPage = () ->
              ###
              Show Terms Page
              ###

              config =
                url     : 'http://lovestamp.io/legal-mumbo-jumbo'
                target  : '_blank'
                options : []
              browserService.open config

              return

            $scope.showPrivacyPage = () ->
              ###
              Show Privacy Page
              ###

              config =
                url     : 'https://stripe.com/us/privacy'
                target  : '_blank'
                options : []
              browserService.open config

              return

            $scope.paymentConstruct =
              type         : null
              address      : null
              bitcoinAddy  : null
              email        : null
              expiry       : null
              number       : null
              security     : null
              rememberMe   : null
              cardPhone    : null


            $scope.address =
              shipping:
                contentEditable : undefined
                contentUpdated  : undefined
                content         : ''
                editing         : false

            $scope.load = () ->
              ###
              load
              ###

              _userRequire = userRequire()

              _userRequire.then((authData) ->

                handle = authData.handle

                profileService.getProfile(handle).then (profileData) ->

                  _networks = _.last(_.filter(_.map(profileData, (d, k) ->
                    if d.networks
                      return d
                  )))

                  networks = _networks.networks
                  networksList = _.toArray(networks)
                  networksList = _.filter(networksList, (a) ->
                    a.indexOf('[') isnt -1
                  )
                  _authData = _.last(networksList)

                  u = _authData.split(':')
                  originUserId = $scope.originUserId = u[1]
                  originUserId = originUserId.replace(']', '')

                  profileService.getProfileById(originUserId).then (cleanedProfileData) ->

                    $timeout ->
                      $scope.$apply () ->
                        $scope._profile = cleanedProfileData
                        addressDeliveryObject = $scope._profile.addressShipping

                        if addressDeliveryObject
                          $timeout ->
                            $scope.$apply () ->
                              $scope.address.shipping.content = $scope.paymentConstruct.address = [
                                addressDeliveryObject.street_number
                                " "
                                addressDeliveryObject.route
                                "\n"
                                addressDeliveryObject.locality
                                ", "
                                addressDeliveryObject.administrative_area_level_1
                                " "
                                addressDeliveryObject.postal_code
                              ].join ''
                          , 0

              )


            $scope.fieldRowEditDeliveryAddress = (content) ->
              ###
              # fieldRowEditDeliveryAddress
              ###

              # @TODO Contains incorrect data.
              _userRequire = userRequire()
              _userRequire.then (authData) ->

                handle = authData.handle

                if ! $scope.address.shipping.editing

                  $scope.address.shipping.editing = true
                  $scope.address.shipping.contentEditable = $scope.$watch(content, (newValue, oldValue) ->
                    if newValue isnt '' and newValue isnt oldValue
                      $scope.address.shipping.contentUpdated = true
                    else
                      $scope.address.shipping.contentUpdated = false
                  )

                else

                  verifiedAddress = mapService.Owlish.verifyAddress($scope.address.shipping.content).then (updatedProfile) ->

                    # Store along a branched social microdata schema
                    profileService.updateProfileBranch(handle, content, updatedProfile.data)

                    # Reset
                    $scope.address.shipping.editing = false

                    # Reset our elemental model.
                    $scope.address.shipping.contentEditable()

                    _userRequire = userRequire()

                    _userRequire.then((authData) ->
                      handle = authData.handle

                      #profileService.getProfile(handle).then (cleanedProfileData) ->
                        #$scope._profile = cleanedProfileData

                      profileService.getProfile(handle).then (profileData) ->

                        _networks = _.last(_.filter(_.map(profileData, (d, k) ->
                          if d.networks
                            return d
                        )))

                        networks = _networks.networks
                        networksList = _.toArray(networks)
                        networksList = _.filter(networksList, (a) ->
                          a.indexOf('[') isnt -1
                        )
                        _authData = _.last(networksList)

                        u = _authData.split(':')
                        originUserId = $scope.originUserId = u[1]
                        originUserId = originUserId.replace(']', '')

                        profileService.getProfileById(originUserId).then (cleanedProfileData) ->

                          $scope._profile = cleanedProfileData

                          addressDeliveryObject = $scope._profile.addressShipping

                          if addressDeliveryObject
                            $timeout ->
                              $scope.$apply () ->
                                $scope.address.shipping.content = $scope.paymentConstruct.address = [
                                  addressDeliveryObject.street_number
                                  " "
                                  addressDeliveryObject.route
                                  "\n"
                                  addressDeliveryObject.locality
                                  ", "
                                  addressDeliveryObject.administrative_area_level_1
                                  " "
                                  addressDeliveryObject.postal_code
                                ].join ''
                            , 0

                    )

              return

            populateBitcoinCheckout = (status, response) ->
              if status == 200
                $timeout ->
                  $scope.$apply () ->
                    $scope.exchangeRateBasedAmount = response.bitcoin_amount / 100000000
                    $scope.bitcoin_address = $scope.paymentConstruct.bitcoinAddy = response.inbound_address
                    $scope.qrBitcoinAddressHref = response.bitcoin_uri
                    $scope.qrBitcoinAddress = $scope.bitcoin_address
                    $scope.poll_response_id = response.id
                , 0
              else
                notificationService.displayError response.error.message

            $scope.$watch('poll_response_id', (newVal) ->
              if newVal

                Stripe.bitcoinReceiver.pollReceiver(newVal, () ->
                  postUrl = ('payment/bitcoin/' + $scope.authData.handle + '/' + _formType)

                  postData =
                    quantity    : $scope.customerItem.qty
                    currency    : 'usd' # e.g., 'usd'
                    response_id : newVal
                    description : $scope.customerItem._qty + ' ' + $scope.label + ' for ' + $scope.paymentConstruct.email
                    $meta       : $scope.paymentConstruct
                    email       : $scope.paymentConstruct.email
                    metadata    : $scope.paymentConstruct
                    rememberMe  : $scope.paymentConstruct.rememberMe

                  $http.post(postUrl, postData).success (successResponse) ->
                    console.log successResponse
                    #multiplier = 1
                    #timeoutDelay = multiplier * 1000

                    #$timeout(() ->
                      #Stripe.bitcoinReceiver.cancelReceiverPoll(newVal)
                      #$mdDialog.hide()
                      #notificationService.displayNote 'Time\'s up!'
                    #, timeoutDelay) # 60 secs

                )

            )

            pn = null
            $scope.validateAuth = () ->
              if $scope.paymentConstruct.email == null
                notificationService.displayError 'Please provide an e-mail'
                $scope.notificationError = 'Please provide an e-mail'
                return
              pn = $scope.paymentConstruct.phoneNumber
              if pn
                if parseInt(pn.replace('(', '').replace(')', '').replace(/-/g, ''), 10) > 0
                  postUrl = '/authorize'
                  postData =
                    number: $scope.paymentConstruct.phoneNumber

                  $http.post(postUrl, postData).
                    success((authData) ->
                      $scope.c = authData
                      notificationService.displayNote 'Authorization successful!'
                    )

            $scope.resolveAuth = () ->
              ###
              Resolve Authentication via SMS through Twilio.
              ###

              if $scope.paymentConstruct.email == null
                notificationService.displayError 'Please provide an e-mail'
                $scope.notificationError = 'Please provide an e-mail'
                delete $scope.c
                return
              if $scope.paymentConstruct.authy == $scope.c
                $scope.paymentConstruct.rememberMe = true
                $scope.showRememberMeInput = true
              else
                $scope.paymentConstruct.rememberMe = false
                $scope.showRememberMeInput = false
                notificationService.displayError 'Try again!'
              delete $scope.c


            $scope.$watch 'showRememberMeInput', (newVal) ->
              if newVal == true and $scope.paymentConstruct.email == null
                notificationService.displayError 'Please provide an e-mail'
                $scope.notificationError = 'Please provide an e-mail'


            $scope.$watch 'paymentConstruct.type', (newVal, oldVal) ->
              if newVal and newVal == '2'

                Stripe.bitcoinReceiver.createReceiver({
                  amount      : $scope._price * 100
                  currency    : 'usd'
                  description : $scope.customerItem.qty + ' ' + $scope.label + ' for ' + $scope.paymentConstruct.email
                  email       : $scope.paymentConstruct.email
                  metadata    : $scope.paymentConstruct
                }, populateBitcoinCheckout)


            $scope.handleStripe = (status, response) ->
              ###
              @param {string} status
              @param {object} response
              @return {Promise:object} Returns the status of the current transaction.
              ###

              if response.error
                notificationService.displayError response.error.message
              else
                token = response.id
                _token = token

                postUrl = ('payment/' + $scope.authData.handle + '/' + _formType)

                postData =
                  quantity    : $scope.customerItem._qty
                  currency    : 'usd' # e.g., 'usd'
                  token       : _token
                  description : $scope.customerItem._qty + ' ' + $scope.label + ' for ' + $scope.paymentConstruct.email
                  email       : $scope.paymentConstruct.email
                  $meta       : $scope.paymentConstruct
                  metadata    : $scope.paymentConstruct
                  rememberMe  : $scope.paymentConstruct.rememberMe

                _userRequire.then (authData) ->
                  handle = authData.handle
                  profileService.updateUserData(handle, { purchaseEmail: $scope.paymentConstruct.mail })

                $http.post(postUrl, postData).
                  success((paymentData) ->
                    if !paymentData
                      return notificationService.displayNote 'Please try again!'

                    notificationService.displayNote 'Payment received!'
                  ).
                  error((error) ->
                    notificationService.displayError error
                  )

              return

            $scope.hide = () ->
              ###
              Hide
              ###

              $mdDialog.hide()
              return

            $scope.cancel = () ->
              ###
              Cancel
              ###

              $mdDialog.cancel()
              return

            $scope.makePayment = ($form) ->
              ###
              Make Payment
              ###

              $mdDialog.hide($form)
              return

      ]


  StoreController = ($scope, user, $http, $mdDialog, $rootScope, $q, $window, profileService, stampService, userRequire, $timeout, notificationService, $location, $anchorScroll) ->
    ###
    Store Module Controller.
    ###

    multiplier = 300

    $scope.currentStampPlan =
        "Stamps"      : 1
        "Impressions" : 300

    $scope.storeHouseStamp = {}
    $scope.storeHouseStamp['Fuel'] = true
    $scope.storeHouseStamp['Stamps'] = true


    $scope.loadExchangeRateData = () ->
      ###
      Load Exchange Rate Data.
      ###
      $timeout ->
          $scope.initExchangePrices()
      , 2000


    $scope.showStripePaymentForm = (event, stampPlan, quantity) ->
      ###
      Show Microtransaction Payment Form.
      ###

      $scope.currentStampPlan[stampPlan.label] = quantity
      # Get the form type based on user's selection from the DOM
      typeString = event.target.parentElement.
        parentElement.
        parentElement.
        textContent

      # Set the description form type name based on user's selection
      type = if typeString.indexOf('Stamps') != -1 then 'stamps' else 'impressions'
      _formType = type
      $scope.selectedValue = _quantity = event.target.getAttribute 'value'

      stampObject =
        qty   : quantity
        _qty  : _quantity
        name  : $scope.currentStampPlan
        label : stampPlan.label
        $meta : stampPlan

      preparedIndividualItem = _.extend {}, stampObject

      $body = document.querySelector('body')

      $location.hash('top')
      $anchorScroll()
      angular.element($body).addClass 'extended-background'

      $mdDialog.show({
        disableParentScroll : false
        hasBackdrop         : true
        controller          : 'DialogController'
        templateUrl         : environment.baseTemplateUrl + 'partials/payMaker.html'
        targetEvent         : event
        preserveScope       : true
        focusOnOpen         : false
        locals :
          customerItem : preparedIndividualItem
      })
        .then(($form) ->
          if $form and $form.$valid
            console.log $form
        , () ->
          notificationService.displayError 'Are you sure?'
          angular.element($body).removeClass 'extended-background'
        )


    # @inprogress
    userAccount = undefined
    user.then (authData) ->

      if authData
        userAccount = '@' + authData[authData.provider].username
        $scope.userActiveStatus = profileService.getUserStatus(userAccount)
        $scope.userActiveStatus.$loaded().then (profileData) ->
          hasStamps = profileData.stamps and profileData.stamps.local
          totalStamps = 0
          if hasStamps
              totalStamps = _.size profileData.stamps.local
          if totalStamps > 0
            $rootScope.$emit('user:hasStampOrIsActive', true)
          else
            $rootScope.$emit('user:hasStampOrIsActive', false)

    $scope.forms =
      stampAsItemForm: {}
      stampAsPlanForm: {}

    $scope.pageSectionTitle = 'Store'
    $scope.stampList = [

    ]

    $scope.stampPlans =
        Stamps      : 1
        Impressions : 300

    $scope.initializedQuantity = (stampPlan, updatedQuantity) ->
        ###
        Initialize the property for dynamic Store drop down select.
        ###


        if stampPlan.impressions
            qty = updatedQuantity  / 10
        else
            qty = stampPlan.cost * updatedQuantity
        $scope.stampPlans[stampPlan.label] = qty
        $scope.initExchangePrices(updatedQuantity)
        $scope.$broadcast "planUpdated:#{stampPlan.label}", { stampPlan: stampPlan, updatedQuantity: updatedQuantity }
        qty

    $scope.stampPlanTitle = 'Store'
    $scope.stampPlansList = [
      {
        label: 'Stamps'
        count: 1
        type: 'The LoveStamp Pack'
        currency: 'USD'
        cost: 50
        notes: [
          '1 Stamp & Registration Code'
          '1 Counter Display'
          'Welcome Card & Instructions'
          'Advertising on the LoveStamp Map'
        ]
        addToCart:
          buttonLabel: 'Buy Now'
          popoutTitle: 'Quantity'
          options: [
            1
            2
            3
            4
            5
          ]
      }
      {
        label: 'Impressions'
        impressions: 300
        impressionsLabel: 'Fuel'
        currency: 'USD'
        cost: 30
        notes: [
          '300 Stamp Impressions'
          'Valid for 30 Days'
          'Unused Stamp Impressions Don\'t Roll Over'
          'Stamp Impressions are Distributed to All Stamps'
        ]
        expiryLabel: 'Expiry'
        expiryLimit: 2
        expiryUnit: 'months'
        expiryHelpNote: 'after 1st stamp impression'
        addToCart:
          buttonLabel: 'Buy Now'
          popoutTitle: 'Quantity'
          options: [
            1 * multiplier
            2 * multiplier
            3 * multiplier
            4 * multiplier
            5 * multiplier
            6 * multiplier
            7 * multiplier
            8 * multiplier
            9 * multiplier
            10 * multiplier
            11 * multiplier
            12 * multiplier
          ]
      }
    ]


    $scope.initExchangePrices = () ->
        $timeout ->
          $scope.$apply () ->
            bitcoinprices.init({

                url                  : "https://api.bitcoinaverage.com/ticker/all"
                marketRateVariable   : "24h_avg"
                defaultCurrency      : 'USD'
                jQuery               : $
                priceAttribute       : "data-usd-price"
                priceOrignalCurrency : 'USD' # Converted to bits.
                symbols:
                  "USD"   : "<i class='fa fa-usd'></i>"
                currencies: [
                  "USD"
                ]
                ux:
                  clickPrices             : true
                  menu                    : false
                  clickableCurrencySymbol : true

            })
        , 50

    return


  [
    "$scope"
    "user"
    "$http"
    "$mdDialog"
    "$rootScope"
    "$q"
    "$window"
    "profile.service"
    "stamp.service"
    "user.require"
    "$timeout"
    "notification.service"
    "$location"
    "$anchorScroll"
    "browser.service"
    StoreController
  ]
