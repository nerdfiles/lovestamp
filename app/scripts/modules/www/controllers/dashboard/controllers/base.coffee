###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/dashboard

## description

Dashboard Module Controller.

###

define [
  "interface"
  "d3"
  "jquery"
  'lodash'
  'moment'
  'numeral'
  #'directives/stats/static'
  'directives/stats/treemap-frequency'
  'directives/site/navigation'
  'auth/user/roll-up'
  'services/stamp'
  'services/profile'
], (__interface__, d3, $, _, moment, numeral) ->

  DashboardController = ($scope, $rootScope, $q, stampService, userRequire, $timeout, messagesBusService, user, profileService) ->

    _userRequire = userRequire()

    $scope.expiredStatus = undefined
    $scope.noResults = false

    _userRequire.then (authData) ->
      userAccount = '@' + authData[authData.provider].username
      $scope.userActiveStatus = profileService.getUserStatus(userAccount)
      $scope.userActiveStatus.$loaded().then (profileData) ->
        _impressions = profileData.impressions
        impressionsData = _.last(_.toArray(_impressions))

        #if !impressionsData
          #$rootScope.$emit('user:hasStampOrIsActive', false)

        #return  if !impressionsData
        #return  if !impressionsData.quantity
        #return  if !impressionsData.expiry

        _impression = _.last(_.toArray(profileData.impressions))
        if _impression
          impression = _impression.quantity
          expiry = _impression.expiry

          currentDateTime = moment()
          expiryDateTime = moment(expiry)

          if currentDateTime.isAfter( expiryDateTime ) or currentDateTime.isSame( expiryDateTime )
            $scope.expiredStatus = true

          if impression is 0
            $scope.expiredStatus = true

          hasStamps = profileData.stamps and profileData.stamps.local
          totalStamps = 0
          if hasStamps
              totalStamps = _.size profileData.stamps.local
          if totalStamps > 0
            $rootScope.$emit('user:hasStampOrIsActive', true)
          else
            $rootScope.$emit('user:hasStampOrIsActive', false)
        else
          $scope.noResults = true
          $scope.loading = false

    $scope.pageSectionTitle = 'Dashboard'
    $scope.graphs = {}
    $scope.isLoading = true
    $scope.stampAnalytics =
      quantity : null
      expiry   : null
    $scope.graphsLabels = {}
    $scope.graphsLabels.list = [
      {
        label : 'Engagement Grid'
        id    : 'grid'
      }
    ]

    loadTimeout = null
    t = null

    _stamps = stampService.getAllStamps().$loaded()

    $scope.$on '$destroy', () ->
      if t
        $timeout.cancel t
      if loadTimeout
        $timeout.cancel loadTimeout
      return

    _stamps.then (stamps) ->

      _userRequire.then (user) ->
        t = $timeout ->
          $scope.$apply () ->
            $scope.user = user

            $scope.profile = _.find stamps, (stampProfile) ->
              stampProfile.$id == $scope.user.handle

            if $scope.profile and $scope.profile.impressions

              i = _.toArray($scope.profile.impressions)
              $scope.impressions = _.last i

              _.extend $scope.stampAnalytics, $scope.impressions

              dateObject = $scope.stampAnalytics.expiry # @example 10-24-2015 19:00:33

              momentObject = moment(dateObject)

              if !momentObject._isAMomentObject
                y = dateObject.split('-')[2].split(' ')[0]
                m = dateObject.split('-')[0]
                d = dateObject.split('-')[1]

                dateConstruct = y + '-' + m + '-' + d
              else
                dateConstruct = dateObject

              $scope.stampAnalytics.expiry = moment(dateConstruct).format("dddd, MMMM Do YYYY")
              $scope.stampAnalytics.quantity = numeral($scope.stampAnalytics.quantity).format('0,0')

            return
          return
        , 0
        return
      return


    cachedStamps = []
    cachedStampsLength = null


    merchantStamps = (handle) ->
      ###
      Collect the stamps from a logged in merchant.
      @param {string} handle
      ###

      def = $q.defer()

      if cachedStampsLength
        def.resolve cachedStamps
      else
        stampService.
          getStampList(handle).$loaded().then (localStamps) ->
            cachedStamps = localStamps
            cachedStampsLength = cachedStamps.length
            def.resolve localStamps

      def.promise

    $scope.loading = true
    $scope.initializedUserbase = () ->
      ###
      initializeduserbase
      ###

      foundOriginUser = false
      originUserId = null

      _stamps.then (userBase) ->

        _userRequire.then (_user) ->
          handle = _user.handle
          merchantHandle = null

          profileService.getUserStatus(handle).$loaded().then (d) ->
            _.forEach d.profile.networks, (userInNetwork) ->
              if userInNetwork.indexOf('[') isnt -1
                foundOriginUser = true
                try
                  originUserId = userInNetwork.split(':')
                  _originUserId = originUserId[1].split(']')
                  __originUserId = _originUserId[0]
                  merchantHandle = __originUserId
                catch e
                  merchantHandle = handle

            if foundOriginUser
              profileService.getUserById(merchantHandle).then (givenUser) ->
                developDashboard(givenUser.$id)
            else
              developDashboard(handle)

          developDashboard = (_handle) ->

            merchantStamps(_handle).then (stampList) ->

              fans = _.filter(_.map userBase, (user) ->

                  _friends = []

                  if user and user.stamps and user.stamps.stats and user.stamps.stats.success

                    ss = user.stamps.stats.success

                    taggedStamps = _.filter ss, (stampenings, stampId) ->
                      flaggedFan = _.find stampList, { $id: stampId }

                    if taggedStamps.length

                      # Number of different stamps at fan's success history
                      return {
                        'id'        : user.$id
                        'prefix'    : user.$id.charAt(0)
                        'name'      : user.$id.charAt(0) + (if user.displayName then user.displayName else user.$id.replace(user.$id.charAt(0), ''))
                        'size'      : _.reduce(_.filter(_.map(taggedStamps, (t) ->

                            if t.stampImpressionsYellow
                              return t.stampImpressionsYellow
                          ), (_t) -> _t), (sum, n) -> sum + n) or 1

                        'intensity' : (() ->

                          userIntensityAggregate = aggregateBlueFriendImpressions = _.reduce(_.filter(_.map(taggedStamps, (t) ->

                              # Must identify that the user has at least an equal 
                              # (and not less than or 0) number of stamp impressions 
                              # blue than its total aggregate stamp impressions yellow 
                              # of all friends, then update assuming that the user was 
                              # always existing within the userbase.
                              if t.stampImpressionsBlue
                                return t.stampImpressionsBlue
                            ), (_t) -> _t), (sum, n) -> sum + n) or 0

                        )()
                      }

              , (d) ->

                # Filter out fans who have not bee stamped by logged in merchant.
                d
              )

              c = 0

              count = 0

              aggregateStampImpressionBlue = _.filter(_.map userBase, (user, userKeyname) ->

                  if user and user.stamps and user.stamps.stats and user.stamps.stats.success
                    ss = user.stamps.stats.success

                    _.forEach(ss, (stampenings, stampId) ->

                      _flaggedFan = _.find stampList, $id: stampId

                      if _flaggedFan
                        if stampenings.stampImpressionsBlue
                          count += ((parseInt(stampenings.stampImpressionsBlue, 10) or 0) + 1)
                    )

                    if count

                      # Number of different stamps at fan's success history
                      return totalColorSpace: count

              , (d) ->
                c += d.totalColorSpace
              )

              loadTimeout = $timeout ->

                $scope.$apply () ->

                  l = _.last(aggregateStampImpressionBlue)
                  _l = if l then l.totalColorSpace else 1

                  $scope.graphs.grid =
                    'name'            : merchantHandle
                    'size'            : 1
                    'children'        : fans.reverse()
                    'totalColorSpace' : _l

                  if _l is 1

                    $scope.noResults = true

                  $scope.loading = false

              , 0

    return

  moduleController = [
    '$scope'
    '$rootScope'
    '$q'
    'stamp.service'
    'user.require'
    '$timeout'
    'messagesBusService'
    'user'
    'profile.service'
    DashboardController
  ]
