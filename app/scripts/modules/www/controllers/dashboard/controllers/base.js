
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/dashboard

 *# description

Dashboard Module Controller.
 */

(function() {
  define(["interface", "d3", "jquery", 'lodash', 'moment', 'numeral', 'directives/stats/treemap-frequency', 'directives/site/navigation', 'auth/user/roll-up', 'services/stamp', 'services/profile'], function(__interface__, d3, $, _, moment, numeral) {
    var DashboardController, moduleController;
    DashboardController = function($scope, $rootScope, $q, stampService, userRequire, $timeout, messagesBusService, user, profileService) {
      var cachedStamps, cachedStampsLength, loadTimeout, merchantStamps, t, _stamps, _userRequire;
      _userRequire = userRequire();
      $scope.expiredStatus = void 0;
      $scope.noResults = false;
      _userRequire.then(function(authData) {
        var userAccount;
        userAccount = '@' + authData[authData.provider].username;
        $scope.userActiveStatus = profileService.getUserStatus(userAccount);
        return $scope.userActiveStatus.$loaded().then(function(profileData) {
          var currentDateTime, expiry, expiryDateTime, hasStamps, impression, impressionsData, totalStamps, _impression, _impressions;
          _impressions = profileData.impressions;
          impressionsData = _.last(_.toArray(_impressions));
          _impression = _.last(_.toArray(profileData.impressions));
          if (_impression) {
            impression = _impression.quantity;
            expiry = _impression.expiry;
            currentDateTime = moment();
            expiryDateTime = moment(expiry);
            if (currentDateTime.isAfter(expiryDateTime) || currentDateTime.isSame(expiryDateTime)) {
              $scope.expiredStatus = true;
            }
            if (impression === 0) {
              $scope.expiredStatus = true;
            }
            hasStamps = profileData.stamps && profileData.stamps.local;
            totalStamps = 0;
            if (hasStamps) {
              totalStamps = _.size(profileData.stamps.local);
            }
            if (totalStamps > 0) {
              return $rootScope.$emit('user:hasStampOrIsActive', true);
            } else {
              return $rootScope.$emit('user:hasStampOrIsActive', false);
            }
          } else {
            $scope.noResults = true;
            return $scope.loading = false;
          }
        });
      });
      $scope.pageSectionTitle = 'Dashboard';
      $scope.graphs = {};
      $scope.isLoading = true;
      $scope.stampAnalytics = {
        quantity: null,
        expiry: null
      };
      $scope.graphsLabels = {};
      $scope.graphsLabels.list = [
        {
          label: 'Engagement Grid',
          id: 'grid'
        }
      ];
      loadTimeout = null;
      t = null;
      _stamps = stampService.getAllStamps().$loaded();
      $scope.$on('$destroy', function() {
        if (t) {
          $timeout.cancel(t);
        }
        if (loadTimeout) {
          $timeout.cancel(loadTimeout);
        }
      });
      _stamps.then(function(stamps) {
        _userRequire.then(function(user) {
          t = $timeout(function() {
            $scope.$apply(function() {
              var d, dateConstruct, dateObject, i, m, momentObject, y;
              $scope.user = user;
              $scope.profile = _.find(stamps, function(stampProfile) {
                return stampProfile.$id === $scope.user.handle;
              });
              if ($scope.profile && $scope.profile.impressions) {
                i = _.toArray($scope.profile.impressions);
                $scope.impressions = _.last(i);
                _.extend($scope.stampAnalytics, $scope.impressions);
                dateObject = $scope.stampAnalytics.expiry;
                momentObject = moment(dateObject);
                if (!momentObject._isAMomentObject) {
                  y = dateObject.split('-')[2].split(' ')[0];
                  m = dateObject.split('-')[0];
                  d = dateObject.split('-')[1];
                  dateConstruct = y + '-' + m + '-' + d;
                } else {
                  dateConstruct = dateObject;
                }
                $scope.stampAnalytics.expiry = moment(dateConstruct).format("dddd, MMMM Do YYYY");
                $scope.stampAnalytics.quantity = numeral($scope.stampAnalytics.quantity).format('0,0');
              }
            });
          }, 0);
        });
      });
      cachedStamps = [];
      cachedStampsLength = null;
      merchantStamps = function(handle) {

        /*
        Collect the stamps from a logged in merchant.
        @param {string} handle
         */
        var def;
        def = $q.defer();
        if (cachedStampsLength) {
          def.resolve(cachedStamps);
        } else {
          stampService.getStampList(handle).$loaded().then(function(localStamps) {
            cachedStamps = localStamps;
            cachedStampsLength = cachedStamps.length;
            return def.resolve(localStamps);
          });
        }
        return def.promise;
      };
      $scope.loading = true;
      $scope.initializedUserbase = function() {

        /*
        initializeduserbase
         */
        var foundOriginUser, originUserId;
        foundOriginUser = false;
        originUserId = null;
        return _stamps.then(function(userBase) {
          return _userRequire.then(function(_user) {
            var developDashboard, handle, merchantHandle;
            handle = _user.handle;
            merchantHandle = null;
            profileService.getUserStatus(handle).$loaded().then(function(d) {
              _.forEach(d.profile.networks, function(userInNetwork) {
                var e, __originUserId, _originUserId;
                if (userInNetwork.indexOf('[') !== -1) {
                  foundOriginUser = true;
                  try {
                    originUserId = userInNetwork.split(':');
                    _originUserId = originUserId[1].split(']');
                    __originUserId = _originUserId[0];
                    return merchantHandle = __originUserId;
                  } catch (_error) {
                    e = _error;
                    return merchantHandle = handle;
                  }
                }
              });
              if (foundOriginUser) {
                return profileService.getUserById(merchantHandle).then(function(givenUser) {
                  return developDashboard(givenUser.$id);
                });
              } else {
                return developDashboard(handle);
              }
            });
            return developDashboard = function(_handle) {
              return merchantStamps(_handle).then(function(stampList) {
                var aggregateStampImpressionBlue, c, count, fans;
                fans = _.filter(_.map(userBase, function(user) {
                  var ss, taggedStamps, _friends;
                  _friends = [];
                  if (user && user.stamps && user.stamps.stats && user.stamps.stats.success) {
                    ss = user.stamps.stats.success;
                    taggedStamps = _.filter(ss, function(stampenings, stampId) {
                      var flaggedFan;
                      return flaggedFan = _.find(stampList, {
                        $id: stampId
                      });
                    });
                    if (taggedStamps.length) {
                      return {
                        'id': user.$id,
                        'prefix': user.$id.charAt(0),
                        'name': user.$id.charAt(0) + (user.displayName ? user.displayName : user.$id.replace(user.$id.charAt(0), '')),
                        'size': _.reduce(_.filter(_.map(taggedStamps, function(t) {
                          if (t.stampImpressionsYellow) {
                            return t.stampImpressionsYellow;
                          }
                        }), function(_t) {
                          return _t;
                        }), function(sum, n) {
                          return sum + n;
                        }) || 1,
                        'intensity': (function() {
                          var aggregateBlueFriendImpressions, userIntensityAggregate;
                          return userIntensityAggregate = aggregateBlueFriendImpressions = _.reduce(_.filter(_.map(taggedStamps, function(t) {
                            if (t.stampImpressionsBlue) {
                              return t.stampImpressionsBlue;
                            }
                          }), function(_t) {
                            return _t;
                          }), function(sum, n) {
                            return sum + n;
                          }) || 0;
                        })()
                      };
                    }
                  }
                }, function(d) {
                  return d;
                }));
                c = 0;
                count = 0;
                aggregateStampImpressionBlue = _.filter(_.map(userBase, function(user, userKeyname) {
                  var ss;
                  if (user && user.stamps && user.stamps.stats && user.stamps.stats.success) {
                    ss = user.stamps.stats.success;
                    _.forEach(ss, function(stampenings, stampId) {
                      var _flaggedFan;
                      _flaggedFan = _.find(stampList, {
                        $id: stampId
                      });
                      if (_flaggedFan) {
                        if (stampenings.stampImpressionsBlue) {
                          return count += (parseInt(stampenings.stampImpressionsBlue, 10) || 0) + 1;
                        }
                      }
                    });
                    if (count) {
                      return {
                        totalColorSpace: count
                      };
                    }
                  }
                }, function(d) {
                  return c += d.totalColorSpace;
                }));
                return loadTimeout = $timeout(function() {
                  return $scope.$apply(function() {
                    var l, _l;
                    l = _.last(aggregateStampImpressionBlue);
                    _l = l ? l.totalColorSpace : 1;
                    $scope.graphs.grid = {
                      'name': merchantHandle,
                      'size': 1,
                      'children': fans.reverse(),
                      'totalColorSpace': _l
                    };
                    if (_l === 1) {
                      $scope.noResults = true;
                    }
                    return $scope.loading = false;
                  });
                }, 0);
              });
            };
          });
        });
      };
    };
    return moduleController = ['$scope', '$rootScope', '$q', 'stamp.service', 'user.require', '$timeout', 'messagesBusService', 'user', 'profile.service', DashboardController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
