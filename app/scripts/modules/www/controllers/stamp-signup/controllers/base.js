
/**
@fileOverview

      _
     (_)
  ___ _  ____ ____  _   _ ____
 /___) |/ _  |  _ \| | | |  _ \
|___ | ( (_| | | | | |_| | |_| |
(___/|_|\___ |_| |_|____/|  __/
       (_____|           |_|

@module www/controllers/stamp-signup
@description

  Stamp Signup Module Controller
 */

(function() {
  define(["interface", "lodash", "moment", "directives/site/navigation", "auth/user/roll-up", "services/authentication", "services/profile", "services/stamp", "services/notification"], function(__interface__, _, moment) {
    var Countdown, StampSignupController;
    Countdown = function(options) {
      var counterEnd, decrementCounter, instance, seconds, timer, updateStatus;
      timer = void 0;
      instance = this;
      seconds = options.seconds || 10;
      updateStatus = options.onUpdateStatus || function() {};
      counterEnd = options.onCounterEnd || function() {};
      decrementCounter = function() {
        updateStatus(seconds);
        if (seconds === 0) {
          counterEnd();
          instance.stop();
        }
        seconds--;
      };
      this.start = function() {
        clearInterval(timer);
        timer = 0;
        seconds = options.seconds;
        timer = setInterval(decrementCounter, 1000);
      };
      this.stop = function() {
        clearInterval(timer);
      };
    };
    __interface__.directive('countDown', [
      '$rootScope', 'profile.service', 'user.require', "$window", function($rootScope, profileService, userRequire, $window) {
        return {
          restrict: 'A',
          scope: {
            'requested': '=',
            'requestTimeout': '='
          },
          link: function($scope, element) {
            var _userRequire;
            $scope.timeSet = false;
            _userRequire = userRequire();
            return _userRequire.then(function(authData) {
              var div, handle, newTime, now, stampRequestTimer, startTime, userRequestNotExpired;
              handle = authData.handle;
              startTime = $scope.requestTimeout ? $scope.requestTimeout : moment().add(172800, 'seconds');
              newTime = moment(startTime, "MM/DD/YYYY HH:mm:ss");
              userRequestNotExpired = true;
              now = moment(new Date());
              if (!$scope.requestTimeout) {
                profileService.updateProfile(handle, {
                  requestTimeout: newTime.format("MM/DD/YYYY HH:mm:ss")
                });
              }
              div = angular.element(element).find('div');
              stampRequestTimer = new Countdown({
                seconds: startTime,
                onUpdateStatus: function(sec) {
                  var d, datethen, ms, s;
                  datethen = newTime.subtract(1, 'seconds');
                  ms = moment(datethen, "MM/DD/YYYY HH:mm:ss").diff(moment(now, "MM/DD/YYYY HH:mm:ss"));
                  d = moment.duration(ms);
                  $scope.durationPassed = d.asHours();
                  s = Math.floor(d.asHours()) + moment(ms).format(":mm:ss");
                  div.html('<span>' + s + '</span>');
                },
                onCounterEnd: function() {}
              });
              return stampRequestTimer.start();

              /*
              $window.onbeforeunload = (event) ->
                _userRequire.then (authData) ->
              
                  handle = authData.handle
                  if userRequestNotExpired != false
                    profileService.updateProfile handle, {
                      requestTimeout: Math.abs($scope.durationPassed)
                    }
              
                  return
                return
              
              $scope.$on '$locationChangeStart', (event, next, current) ->
                _userRequire.then (authData) ->
              
                  handle = authData.handle
                  if userRequestNotExpired != false
                    profileService.updateProfile handle, {
                      requestTimeout: Math.abs($scope.durationPassed)
                    }
              
                  return
                return
               */
            });
          }
        };
      }
    ]);
    StampSignupController = function($scope, user, $q, $window, $location, profileService, stampService, userRequire, $stateParams, $timeout, notificationService) {

      /*
      @param {object} $scope
      @param {object} user
      @param {object} $q
      @param {object} $window
      @param {object} $location
      @param {object} profileService
      @param {object} stampService
      @param {object} userRequire
      @param {object} $stateParams
       */
      var _userRequire;
      $scope.requestConstruct = {};
      $scope.requestTimeout = false;
      _userRequire = userRequire();
      _userRequire.then(function(authData) {
        var handle;
        handle = authData.handle;
        $scope.username = handle;
        return profileService.getUserStatus(handle).$loaded().then(function(profileData) {
          return $timeout(function() {
            return $scope.$apply(function() {
              return $scope.requestTimeout = profileData.profile.requestTimeout;
            });
          }, 0);
        });
      });
      $scope.forms = {
        baptize: {},
        signup: {}
      };
      if ($stateParams.signedAlias) {
        $scope.locked = true;
      }
      $scope.pageSectionTitle = 'Hello!';
      $scope.requested = false;

      /*
      Link Stamp to User
       */
      $scope.linkStampToUser = function(alias) {
        _userRequire.then(function(authData) {
          var e;
          try {
            $scope.linking = stampService.lock(alias, authData.handle);
            $location.path('/stamps');
          } catch (_error) {
            e = _error;
            notificationService.displayError(e);
          }
        });
      };
      $scope.createSignupForUser = function() {

        /*
        Create Signup for User
        
        @unit requestConstruct
         */
        _userRequire.then(function(authData) {
          var handle;
          handle = authData.handle;
          $scope.requested = true;
          _.extend($scope.requestConstruct, $scope.forms.signup);
          stampService.requestForUser(handle, $scope.requestConstruct);
        });
      };
    };
    return ["$scope", "user", "$q", "$window", "$location", "profile.service", "stamp.service", "user.require", "$stateParams", "$timeout", "notification.service", StampSignupController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
