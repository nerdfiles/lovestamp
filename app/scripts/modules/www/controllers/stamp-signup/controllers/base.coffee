###*
@fileOverview

      _
     (_)
  ___ _  ____ ____  _   _ ____
 /___) |/ _  |  _ \| | | |  _ \
|___ | ( (_| | | | | |_| | |_| |
(___/|_|\___ |_| |_|____/|  __/
       (_____|           |_|

@module www/controllers/stamp-signup
@description

  Stamp Signup Module Controller

###

define [
  "interface"
  "lodash"
  "moment"
  "directives/site/navigation"
  "auth/user/roll-up"
  "services/authentication"
  "services/profile"
  "services/stamp"
  "services/notification"
], (__interface__, _, moment) ->

  Countdown = (options) ->

    timer = undefined
    instance = this
    seconds = options.seconds or 10
    updateStatus = options.onUpdateStatus or ->
    counterEnd = options.onCounterEnd or ->

    decrementCounter = ->
      updateStatus seconds
      if seconds == 0
        counterEnd()
        instance.stop()
      seconds--
      return

    @start = ->
      clearInterval timer
      timer = 0
      seconds = options.seconds
      timer = setInterval(decrementCounter, 1000)
      return

    @stop = ->
      clearInterval timer
      return

    return

  __interface__.
    directive 'countDown', [
      '$rootScope'
      'profile.service'
      'user.require'
      "$window"
      ($rootScope, profileService, userRequire, $window) ->
        restrict: 'A'
        scope:
          'requested': '='
          'requestTimeout': '='
        link: ($scope, element) ->

          $scope.timeSet = false
          _userRequire = userRequire()
          _userRequire.then (authData) ->

            handle = authData.handle

            startTime = if $scope.requestTimeout then $scope.requestTimeout else moment().add(172800, 'seconds')

            newTime = moment(startTime, "MM/DD/YYYY HH:mm:ss")
            userRequestNotExpired = true
            now = moment(new Date())

            if !$scope.requestTimeout

              profileService.updateProfile handle, {
                requestTimeout: newTime.format("MM/DD/YYYY HH:mm:ss")
              }

            div = angular.element(element).find('div')

            stampRequestTimer = new Countdown(
              seconds: startTime
              onUpdateStatus: (sec) ->

                datethen = newTime.subtract(1, 'seconds')

                ms = moment(datethen,"MM/DD/YYYY HH:mm:ss").diff(moment(now,"MM/DD/YYYY HH:mm:ss"))

                d = moment.duration(ms)

                $scope.durationPassed = d.asHours()

                s = Math.floor(d.asHours()) + moment(ms).format(":mm:ss")

                div.html('<span>' + s + '</span>')

                return

              onCounterEnd: ->
                #userRequestNotExpired = false

                #profileService.updateProfile handle, {
                  #requestTimeout: userRequestNotExpired
                #}

                return
            )

            stampRequestTimer.start()

            ###
            $window.onbeforeunload = (event) ->
              _userRequire.then (authData) ->

                handle = authData.handle
                if userRequestNotExpired != false
                  profileService.updateProfile handle, {
                    requestTimeout: Math.abs($scope.durationPassed)
                  }

                return
              return

            $scope.$on '$locationChangeStart', (event, next, current) ->
              _userRequire.then (authData) ->

                handle = authData.handle
                if userRequestNotExpired != false
                  profileService.updateProfile handle, {
                    requestTimeout: Math.abs($scope.durationPassed)
                  }

                return
              return
            ###

    ]


  StampSignupController = ($scope, user, $q, $window, $location, profileService, stampService, userRequire, $stateParams, $timeout, notificationService) ->
    ###
    @param {object} $scope
    @param {object} user
    @param {object} $q
    @param {object} $window
    @param {object} $location
    @param {object} profileService
    @param {object} stampService
    @param {object} userRequire
    @param {object} $stateParams
    ###

    $scope.requestConstruct = {}

    $scope.requestTimeout = false

    _userRequire = userRequire()

    _userRequire.then (authData) ->
      handle = authData.handle

      $scope.username = handle
      profileService.getUserStatus(handle).$loaded().then (profileData) ->
        $timeout ->
          $scope.$apply ->
            $scope.requestTimeout = profileData.profile.requestTimeout
        , 0

    $scope.forms =
      baptize: {}
      signup: {}

    if $stateParams.signedAlias
      $scope.locked = true

    $scope.pageSectionTitle = 'Hello!'

    $scope.requested = false


    ###
    Link Stamp to User
    ###
    $scope.linkStampToUser = (alias) ->
      _userRequire.then (authData) ->
        try
          $scope.linking = stampService.lock alias, authData.handle
          $location.path('/stamps')
        catch e
          notificationService.displayError e
        return
      return


    $scope.createSignupForUser = () ->
      ###
      Create Signup for User

      @unit requestConstruct
      ###
      _userRequire.then (authData) ->
        handle = authData.handle
        $scope.requested = true
        _.extend $scope.requestConstruct, $scope.forms.signup
        stampService.requestForUser handle, $scope.requestConstruct
        return
      return

    return


  [
    "$scope"
    "user"
    "$q"
    "$window"
    "$location"
    "profile.service"
    "stamp.service"
    "user.require"
    "$stateParams"
    "$timeout"
    "notification.service"
    StampSignupController
  ]
