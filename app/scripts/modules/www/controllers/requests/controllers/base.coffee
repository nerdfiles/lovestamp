###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/requests  

## description

A glue controller for our Requests panel.

###

define [
  "interface"
  "lodash"
  "numeral"
  "angular-infinite-scroll" # @dependency angular.module 'admin/controllers', ['infinite-scroll']
  "angular-chosen-localytics" # @dependency angular.module 'admin/controllers', ['localytics.directives']
  "directives/stats/static"
  "directives/site/navigation"
  "directives/stroll"
  "auth/user/roll-up"
  "services/profile"
  "services/stamp"
  "services/bank"
  "services/payments"
  "filters/search"
  'angular-ui-mask'
], (__interface__, _, numeral) ->

  RequestsController = ($scope, $http, profileService, stampService, bankService, $timeout, $base64, paymentsService) ->

    $scope.requestsList = []

    $scope.loadRequestsList = () ->
      paymentsService.getRequests().then (requestsData) ->

        $scope.requestsList = _.map(_.filter(_.map(_.toArray(requestsData), (k, f) ->
          if f < 3
            return
          k
        ), (listData) ->
          listData
        ), (d, j) ->
          $id: j
          requests: d
        )

        console.dir $scope.requestsList

      return

    return

  moduleController = [
    '$scope'
    '$http'
    'profile.service'
    'stamp.service'
    'bank.service'
    "$timeout"
    "$base64"
    "payments.service"
    RequestsController
  ]
