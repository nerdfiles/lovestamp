
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/requests  

 *# description

A glue controller for our Requests panel.
 */

(function() {
  define(["interface", "lodash", "numeral", "angular-infinite-scroll", "angular-chosen-localytics", "directives/stats/static", "directives/site/navigation", "directives/stroll", "auth/user/roll-up", "services/profile", "services/stamp", "services/bank", "services/payments", "filters/search", 'angular-ui-mask'], function(__interface__, _, numeral) {
    var RequestsController, moduleController;
    RequestsController = function($scope, $http, profileService, stampService, bankService, $timeout, $base64, paymentsService) {
      $scope.requestsList = [];
      $scope.loadRequestsList = function() {
        paymentsService.getRequests().then(function(requestsData) {
          $scope.requestsList = _.map(_.filter(_.map(_.toArray(requestsData), function(k, f) {
            if (f < 3) {
              return;
            }
            return k;
          }), function(listData) {
            return listData;
          }), function(d, j) {
            return {
              $id: j,
              requests: d
            };
          });
          return console.dir($scope.requestsList);
        });
      };
    };
    return moduleController = ['$scope', '$http', 'profile.service', 'stamp.service', 'bank.service', "$timeout", "$base64", "payments.service", RequestsController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
