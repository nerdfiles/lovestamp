
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/stamps  
@stamp.schema  

    stamp =  
      alias          : null
      merchantAlias  : null
      active         : false
      activity       : null
      defaultAddress : null
      address:  
        line1   : null
        line2   : null
        country : null
        city    : null
        zipCode : null
        state:  
          abbr : null
          name : null
        center:  
          lat  : null
          lng  : null
          zoom : 18
      hours:  
        timeZone   : null
        start      : null
        end        : null
        alwaysOpen : false

 *# description

Stamp Control module controller.
 */

(function() {
  define(["interface", "lodash", "leaflet-knn", 'directives/customEnter', "directives/site/navigation", "directives/stroll", "auth/user/roll-up", "services/profile", "services/stamp", "services/map", "services/notification", "services/crypto"], function(__interface__, _, leafletKnn) {

    /*
    @ngdoc directive  
    @name lovestamp.directive.setUrl  
    @element a  
    @function  
    
    @description  
    Set URL  
    
    @example  
        <a set-url>
          {{text}}
        </a>
     */
    var StampControlController, currentStamp, setUrl, stampGeoSettings, zoomSetting;
    setUrl = function($window, $location, profileService, userRequire) {
      return {
        restrict: 'A',
        scope: {
          merchantAlias: '=merchantAlias'
        },
        link: function($scope, element) {
          var _userRequire;
          _userRequire = userRequire();
          _userRequire.then(function(authData) {
            var userProfile;
            userProfile = profileService.getUserStatus();
            return userProfile.$loaded().then(function() {
              return element.on('click', function() {
                $location.path('/' + authData.handle + '/' + $scope.merchantAlias);
              });
            });
          });
        }
      };
    };

    /*
    @ngdoc directive  
    @name lovestamp.directive.currentStamp  
    @element li  
    
    @description  
    Current Stamp  
    
    @example  
        <li current-stamp>
     */
    currentStamp = function($timeout) {
      return {
        restrict: 'A',
        link: function($scope, element) {
          var iconLabel, iconList, input, listOption, listOptionButton, sourceElement, t;
          sourceElement = element;
          listOption = sourceElement.find('li').eq(0);
          listOptionButton = sourceElement.find('li').eq(1);
          iconList = listOptionButton.find('button').find('span');
          iconLabel = _.filter(iconList, function(el) {
            return angular.element(el).hasClass('fa');
          });
          input = listOption.find('md-input-container').find('input');
          input.on('blur', function() {
            angular.element(iconLabel).eq(0).addClass('is-dirty');
            angular.element(iconLabel).eq(1).removeClass('is-dirty');
            return listOption.removeClass('shown');
          });
          t = null;
          sourceElement.on('click', function() {
            angular.element(iconLabel).eq(0).toggleClass('is-dirty');
            angular.element(iconLabel).eq(1).toggleClass('is-dirty');
            listOption.toggleClass('shown');
            return t = $timeout(function() {
              return input.focus();
            }, 500);
          });
          _.forEach(element.find('li'), function(elem, index) {
            var childElement;
            childElement = angular.element(elem);
            if (index === 0) {
              return childElement.addClass('hidden');
            }
          });
        }
      };
    };
    __interface__.directive('setUrl', ['$window', '$location', 'profile.service', 'user.require', setUrl]).directive('ngModelOnblur', function() {
      return {
        restrict: 'A',
        require: 'ngModel',
        priority: 1,
        link: function(scope, element, attrs, ngModelCtrl) {
          var update;
          if (attrs.type === 'radio' || attrs.type === 'checkbox') {
            return;
          }
          update = function() {
            return scope.$apply(function() {
              ngModelCtrl.$setViewValue(element.val().trim());
              return ngModelCtrl.$render();
            });
          };
          return element.off('input').off('keydown').off('change').on('focus', function() {
            return scope.$apply(function() {
              return ngModelCtrl.$setPristine();
            });
          }).on('blur', update).on('keydown', function(e) {
            if (e.keyCode === 13) {
              return update();
            }
          });
        }
      };
    }).directive('currentStamp', ['$timeout', currentStamp]).filter("emptyifblank", function() {
      return function(object, query) {
        if (!query) {
          return {};
        } else {
          return object;
        }
      };
    });
    zoomSetting = 17;
    stampGeoSettings = {};

    /*
    @ngdoc controller
    @name lovestamp.controller.StampControlController  
    
    @description  
    Module Controller for Stamps Route.
     */
    StampControlController = function($scope, $http, $q, $window, stampService, user, $timeout, geolocation, leafletData, $filter, userRequire, mapService, notificationService, cryptoService, profileService) {
      var assetLayerGroup, currentPositionMarker, currentStampTimeout, data, geoData, locationMarkerGroup, map, mapTimeout, stampMarker, stampsMapDefaults, t, updateStampLocation, updateStampTimeout, validateApplyTimeout, validateTimeout, _userRequire;
      mapTimeout = null;
      updateStampTimeout = null;
      validateTimeout = null;
      validateApplyTimeout = null;
      currentStampTimeout = null;
      t = null;
      $scope.pageSectionTitle = 'Stamp Control Panel';
      $scope.forms = {};
      $scope.forms.stampdex = {};
      $scope.getCurrentStamp = null;
      $scope.stampList = [];
      $scope.stampControlList = [];
      $scope.countryFilter = void 0;
      currentPositionMarker = {};
      $scope.localityFilter = $scope.regionFilter = void 0;
      $scope.updatedStampDefault = false;
      _userRequire = userRequire();

      /*
      @name lovestamp.controller.StampControlController.addressLookup
      @inner
      
      @description
      Cycle through local list of stamps to match merchant alias.
       */
      $scope.addressLookup = function() {
        var foundStampMerchantAlias;
        foundStampMerchantAlias = false;
        _.forEach($scope.stampList, function(stampItself) {
          if (stampItself.merchantAlias === $scope.currentStamp.defaultAddress) {
            return foundStampMerchantAlias = $scope.currentStamp.defaultAddress;
          }
        });
        if (foundStampMerchantAlias !== false) {
          $scope.validateAddy(foundStampMerchantAlias);
        }
      };

      /*
      @name lovestamp.controller.StampControlController.validateAddy
      @function
      
      @description
      Validate stamp by alias found in monoatomic list link for stamp.
       */
      $scope.validateAddy = function(alias) {
        $scope.oldStampAddress = alias;
        $scope.updatedStampDefault = false;
        return mapService.getAddressByAlias(alias).then(function(validatedStampDefaultAddress) {
          validateTimeout = $timeout(function() {
            $scope.$apply(function() {
              var e, _activity, _address, _days, _hours;
              $scope.updatedStampDefault = true;
              try {
                $scope.currentStamp.defaultAddress = validatedStampDefaultAddress.alias;
                _address = validatedStampDefaultAddress.$meta.address;
                _hours = validatedStampDefaultAddress.$meta.hours;
                _days = validatedStampDefaultAddress.$meta.days;
                _activity = validatedStampDefaultAddress.$meta.activity;
                if (_hours) {
                  if (_hours.alwaysOpen) {
                    _hours.start = '';
                    _hours.end = '';
                  }
                }
                $scope.currentStamp.address = _address;
                $scope.currentStamp.hours = _hours;
                $scope.currentStamp.days = _days;
                $scope.currentStamp.activity = _activity;
              } catch (_error) {
                e = _error;
                notificationService.displayError('Unable to grab stamp details');
              }
            });
          }, 0);
        }, function(error) {
          validateApplyTimeout = $timeout(function() {
            $scope.$apply(function() {
              $scope.updatedStampDefault = false;
              $scope.currentStamp.defaultAddress = '';
            });
          }, 0);
        });
      };
      map = null;
      assetLayerGroup = null;
      stampMarker = null;
      locationMarkerGroup = null;
      data = {};
      stampGeoSettings = {};

      /* Loads a model for a Detail View of a stamp. Consider for ui-router. */
      stampsMapDefaults = {
        defaults: {
          tileLayer: 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png',
          tileLayerOptions: {
            opacity: 1,
            detectRetina: true,
            reuseTiles: true
          },
          keyboard: true,
          dragging: true,
          doubleClickZoom: true,
          tap: true,
          zoomControl: true
        }
      };
      _.extend($scope, stampsMapDefaults);

      /*
      updateStampLocation
      @inner
      
      @description
      updateStampLocation
      
      @param {object} _map
      @param {object} mapData
      @param {object} _data
       */
      updateStampLocation = function(_map, mapData, _data) {

        /*
        mapDragStart
        @inner updateStampLocation
        
        @description
        Map Drag Start
        
        @param {object:Event} e
        @return {undefined}
         */
        var LeafIcon, defaultMapMarker, deviceLocation, foundStaleLocation, mapDragEnd, mapDragStart, staleLocation, stampAddressConstruct, stampMarkerDragEnd, _stamp;
        mapDragStart = function(e) {
          var note;
          stampMarker.dragging.disable();
          note = 'You have "use same details as". To amend the address, you must clear that field.';
          if ($scope.currentStamp.defaultAddress !== null) {
            notificationService.displayNote(note);
            return;
          }
        };

        /*
        mapDragEnd
        @inner updateStampLocation
        
        @description
        Map Drag End
        
        @param {object:Event} e
        @return {undefined}
         */
        mapDragEnd = function(e) {
          stampMarker.dragging.enable();
        };
        _map.on('dragstart', mapDragStart);
        _map.on('dragend', mapDragEnd);
        _stamp = _.last($scope.stampControlList);
        if (_stamp.address && _stamp.address.location) {
          staleLocation = _stamp.address.location;
        }
        deviceLocation = {
          lat: _data.lat,
          lng: _data.lng
        };
        foundStaleLocation = staleLocation && staleLocation.lat && staleLocation.lng;
        stampAddressConstruct = {
          lat: foundStaleLocation ? staleLocation.lat : deviceLocation.lat,
          lng: foundStaleLocation ? staleLocation.lng : deviceLocation.lng
        };
        LeafIcon = L.Icon.extend({
          options: {
            iconSize: [48, 48],
            iconAnchor: [48 / 2, 48 / 2]
          }
        });
        defaultMapMarker = new LeafIcon({
          iconUrl: '/images/map-marker.png'
        });
        stampMarker = L.marker([stampAddressConstruct.lat, stampAddressConstruct.lng], {
          draggable: true,
          icon: defaultMapMarker
        }).addTo(_map);
        _map.setView(new L.LatLng(stampAddressConstruct.lat, stampAddressConstruct.lng), zoomSetting);

        /*
        stampMarkerDragEnd
        
        @description
        Stamp Marker Drag End
        
        @param {object:Event} e
        @return {undefined}
         */
        stampMarkerDragEnd = function(e) {
          var _location;
          if ($scope.currentStamp.defaultAddress !== null) {
            notificationService.displayNote('You have "use same details as". To amend the address, you must clear that field.');
            return;
          }
          _location = e.target._latlng;
          if (cryptoService.hash(JSON.stringify(stampAddressConstruct)) !== cryptoService.hash(JSON.stringify(_location))) {
            _.extend(_stamp.address.location, _location);
            updateStampTimeout = $timeout(function() {
              return $scope.$apply(function() {
                return $scope.updateStamp(_stamp, true);
              });
            }, 0);
          }
        };
        stampMarker.on('dragend', stampMarkerDragEnd);
        angular.extend(stampGeoSettings, {
          lat: stampAddressConstruct.lat,
          lng: stampAddressConstruct.lng,
          zoom: zoomSetting
        });
      };
      geoData = null;

      /*
      @name lovestamp.controller.StampControlController.loadDetail
      @function
      
      @description
      
      @param {object} stamp
       */
      $scope.loadDetail = function(stamp) {
        var personalMarkerHtml, stampAlias;
        $scope.stampControlList = [];
        personalMarkerHtml = null;
        stampAlias = {};
        _userRequire.then(function(authData) {
          var birthDate, e, handle, _stamp;
          handle = authData.handle;
          $scope.currentStamp = stampService.getStamp(stamp, handle);
          try {
            stamp._birthDate = stamp.birthDate ? stamp.birthDate.split('-') : stamp.birthDate.split('/');
            if (stamp._birthDate.length) {
              birthDate = [stamp._birthDate[1] + '-', stamp._birthDate[0] + '-', stamp._birthDate[2]].join('');
              stamp.birthDate = birthDate ? birthDate : null;
            } else {
              stamp.birthDate = birthDate ? birthDate : null;
            }
          } catch (_error) {
            e = _error;
            stamp.birthDate = birthDate ? birthDate : '';
          }
          stamp.registeredDate = stamp.registeredDate ? stamp.registeredDate : '';
          stamp.firstImpressionDate = stamp.firstImpressionDate ? stamp.firstImpressionDate : '';
          _stamp = stamp;
          angular.extend($scope.currentStamp, _stamp);
          $scope.stampControlList.push($scope.currentStamp);
          stampAlias = angular.extend({}, $scope.stampControlList[0]);
          $scope.$broadcast('getMap', _stamp);
        });
        angular.extend($scope, {
          center: stampGeoSettings
        });
      };

      /*
      @name lovestamp.controller.StampControlController.loadStampList
      @function
      
      @description
       */
      $scope.loadStampList = function() {
        var foundOriginUser, originUserId, stampOwnerList, _handle;
        foundOriginUser = false;
        originUserId = null;
        _handle = null;
        stampOwnerList = [];
        $scope.stampList = [];
        _userRequire.then(function(authData) {
          var handle;
          handle = authData.handle;
          return profileService.getUserStatus(handle).$loaded().then(function(d) {
            console.log(d.profile);
            return _.forEach(d.profile.networks, function(userInNetwork) {
              var preparedId, __preparedId, _preparedId;
              preparedId = userInNetwork.replace('[', '').replace(']', '');
              _preparedId = preparedId.split(':');
              __preparedId = _preparedId[1];
              return profileService.getUserById(__preparedId).then(function(givenUser) {
                if (givenUser && givenUser.$id) {
                  return stampService.getLocalStamps(givenUser.$id).$loaded().then(function(__d) {
                    return _.forEach(__d, function(___d) {
                      ___d.givenId = givenUser.$id;
                      return $scope.stampList.push(___d);
                    });
                  });
                }
              });
            });
          });
        });
      };

      /*
      @namespace loadZip  
      @description  
        Load zip code.
      @examples  
        - https://s3.amazonaws.com/zips.dryan.io/77006.json
          - {"locality": "Houston", "region": {"fips": "48", "abbr": "TX", "name": "Texas"}, "localities": ["Houston"], "postal_code": "77006", "lat": 29.741878, "lng": -95.38944, "type": "STANDARD", "counties": [{"fips": "201", "name": "Harris"}]}
      
      @param {string} _zipCode  
      @return {undefined}
       */
      $scope.loadZip = function(_zipCode) {
        var zipCode;
        zipCode = _zipCode;
        if (zipCode) {
          $http.get("https://s3.amazonaws.com/zips.dryan.io/" + zipCode + ".json").success(function(zipData) {
            $scope.locality = zipData.locality;
            $scope.localityFilter = {
              city: $scope.locality
            };
            $scope.region = zipData.region.name;
            return $scope.regionFilter = {
              name: $scope.region
            };
          });
        } else {
          $scope.localityFilter = $scope.regionFilter = void 0;
        }
      };

      /*
      @function  
      @description  
        Load cities from reference file.
      @namespace loadCities  
      @return {undefined}
       */
      $scope.loadCities = function() {
        $http.get('/reference/cities.json').success(function(cityData) {
          return $scope.cities = cityData.cities;
        });
      };

      /*
      @function chooseCity  
      @param {string} cityHandle  
      @return {undefined}
       */
      $scope.chooseCity = function(cityHandle) {};

      /*
      @namespace loadCountries
      @return {undefined}
       */
      $scope.loadCountries = function() {
        $http.get('/reference/countries.geo.json').success(function(countryData) {
          return $scope.countries = countryData.features;
        });
      };

      /*
      @namespace chooseCountry
      @return {undefined}
       */
      $scope.chooseCountry = function(countryHandle) {
        $scope.countryHandleSelected = countryHandle.properties.name;
        $scope.countryFilter = {
          country: $scope.countryHandleSelected
        };
      };

      /*
      @namespace loadStates
      @return {undefined}
       */
      $scope.loadStates = function() {
        $http.get('/reference/states.geo.json').success(function(stateData) {
          return $scope.states = stateData.features;
        });
      };

      /*
      @namespace chooseState
      @param {string} stateHandle
      @return {undefined}
       */
      $scope.chooseState = function(stateHandle) {};
      $scope.stampAdd = function() {
        _userRequire.then(function(authData) {
          var foundOriginUser, originUserId, _handle;
          _handle = authData.handle;
          foundOriginUser = false;
          originUserId = null;
          profileService.getUserStatus(_handle).$loaded().then(function(d) {
            var handle;
            _.forEach(d.profile.networks, function(userInNetwork) {
              var e, handle, __originUserId, _originUserId;
              if (userInNetwork.indexOf('[') !== -1) {
                foundOriginUser = true;
                try {
                  originUserId = userInNetwork.split(':');
                  _originUserId = originUserId[1].split(']');
                  __originUserId = _originUserId[0];
                  return handle = __originUserId;
                } catch (_error) {
                  e = _error;
                  return handle = _handle;
                }
              }
            });
            if (foundOriginUser !== true) {
              handle = _handle;
            }
            if (foundOriginUser) {
              return profileService.getUserById(handle).then(function(givenUser) {
                return stampService.lock($scope.getCurrentStamp, givenUser.$id);
              });
            } else {
              return stampService.lock($scope.getCurrentStamp, handle);
            }
          });
        });
      };

      /*
      @namespace updateStamp
      @param {object} stamp
      @return {undefined}
       */
      $scope.updateStamp = function(stamp, dragUpdate) {
        var foundOriginUser, givenId, originUserId;
        givenId = stamp.givenId;
        if ($scope.updatedStampDefault && dragUpdate) {
          return;
        }
        if (stamp.hours.alwaysOpen === void 0) {
          stamp.hours.alwaysOpen = false;
        }
        if (stamp.hours.start === void 0) {
          stamp.hours.start = '';
        }
        if (stamp.hours.end === void 0) {
          stamp.hours.end = '';
        }
        if (!stamp.days) {
          stamp.days = {
            monday: false,
            tuesday: false,
            wednesday: false,
            thursday: false,
            friday: false,
            saturday: false,
            sunday: false
          };
        }
        foundOriginUser = false;
        originUserId = null;
        return _userRequire.then(function(authData) {
          var handleUser, _handle;
          _handle = authData.handle;
          user = null;
          handleUser = _handle;
          return profileService.getUserStatus(_handle).$loaded().then(function(d) {
            var _u;
            _.forEach(d.profile.networks, function(userInNetwork) {
              var e, __originUserId, _originUserId;
              if (userInNetwork.indexOf('[') !== -1) {
                foundOriginUser = true;
                try {
                  originUserId = userInNetwork.split(':');
                  _originUserId = originUserId[1].split(']');
                  __originUserId = _originUserId[0];
                  return user = __originUserId;
                } catch (_error) {
                  e = _error;
                  return console.dir(e);
                }
              }
            });
            _u = null;
            return profileService.getUserById(user).then(function(givenUser) {
              if (foundOriginUser) {
                return _u = givenUser.$id;
              } else {
                return _u = handleUser;
              }
            })["finally"](function(uu) {
              var e;
              console.log(uu);
              console.log(_u);
              try {
                return profileService.getProfileById(_u).then(function(_d) {
                  stampService.updateStampAddress(stamp, givenId);
                  return notificationService.displayNote('Saved!');
                });
              } catch (_error) {
                e = _error;
                return notificationService.displayNote('Failed to update stamp address.');
              }
            });
          });
        });
      };
    };
    return ["$scope", "$http", "$q", "$window", "stamp.service", "user", "$timeout", "geolocation", "leafletData", "$filter", "user.require", "map.service", 'notification.service', 'crypto.service', 'profile.service', StampControlController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
