###
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module www/controllers/stamps  
@stamp.schema  

    stamp =  
      alias          : null
      merchantAlias  : null
      active         : false
      activity       : null
      defaultAddress : null
      address:  
        line1   : null
        line2   : null
        country : null
        city    : null
        zipCode : null
        state:  
          abbr : null
          name : null
        center:  
          lat  : null
          lng  : null
          zoom : 18
      hours:  
        timeZone   : null
        start      : null
        end        : null
        alwaysOpen : false

## description

Stamp Control module controller.

###
define [
  "interface"
  "lodash"
  "leaflet-knn"
  #"leaflet-osmbuildings"
  'directives/customEnter'
  "directives/site/navigation"
  "directives/stroll"
  "auth/user/roll-up"
  "services/profile"
  "services/stamp"
  "services/map"
  "services/notification"
  "services/crypto"
], (__interface__, _, leafletKnn) ->

  ###
  @ngdoc directive  
  @name lovestamp.directive.setUrl  
  @element a  
  @function  

  @description  
  Set URL  

  @example  
      <a set-url>
        {{text}}
      </a>
  ###
  setUrl = ($window, $location, profileService, userRequire) ->
    restrict: 'A'
    scope:
      merchantAlias: '=merchantAlias'
    link: ($scope, element) ->
      _userRequire = userRequire()
      _userRequire.then (authData) ->
        userProfile = profileService.getUserStatus()
        userProfile.$loaded().then () ->
          element.on 'click', () ->
            $location.path('/' + authData.handle + '/' + $scope.merchantAlias)
            return
      return

  ###
  @ngdoc directive  
  @name lovestamp.directive.currentStamp  
  @element li  

  @description  
  Current Stamp  

  @example  
      <li current-stamp>
  ###
  currentStamp = ($timeout) ->
    restrict: 'A'
    link: ($scope, element) ->

      sourceElement = element
      listOption = sourceElement.find('li').eq(0)
      listOptionButton = sourceElement.find('li').eq(1)
      iconList = listOptionButton.find('button').find('span')
      iconLabel = _.filter iconList, (el) -> angular.element(el).hasClass('fa')

      input = listOption.
        find('md-input-container').
        find('input')

      input.on 'blur', () ->
        angular.element(iconLabel).eq(0).addClass 'is-dirty'
        angular.element(iconLabel).eq(1).removeClass 'is-dirty'
        listOption.removeClass 'shown'

      t = null

      sourceElement.on 'click', () ->
        angular.element(iconLabel).eq(0).toggleClass 'is-dirty'
        angular.element(iconLabel).eq(1).toggleClass 'is-dirty'
        listOption.toggleClass 'shown'
        t = $timeout ->
          input.focus()
        , 500

      _.forEach element.find('li'), (elem, index) ->
        childElement = angular.element(elem)
        if index == 0
          childElement.addClass 'hidden'

      return

  __interface__

    .directive('setUrl', [
      '$window'
      '$location'
      'profile.service'
      'user.require'
      setUrl
    ])
    .directive('ngModelOnblur', () ->
      return {
        restrict: 'A'
        require: 'ngModel'
        priority: 1
        link: (scope, element, attrs, ngModelCtrl) ->
          if (attrs.type == 'radio' or attrs.type == 'checkbox')
            return
          update = () ->
            scope.$apply(() ->
              ngModelCtrl.$setViewValue(element.val().trim())
              ngModelCtrl.$render()
            )
          element.off('input').off('keydown').off('change').on('focus', () ->
            scope.$apply(() ->
              ngModelCtrl.$setPristine()
            )
          ).on('blur', update).on('keydown', (e) ->
            if e.keyCode == 13
              update()
          )
      }
    )

    .directive('currentStamp', [
      '$timeout'
      currentStamp
    ])

    .filter("emptyifblank", () ->
      return (object, query) ->
        if !query
          return {}
        else
          return object
    )

  zoomSetting = 17
  stampGeoSettings = {}

  ###
  @ngdoc controller
  @name lovestamp.controller.StampControlController  

  @description  
  Module Controller for Stamps Route.

  ###
  StampControlController = ($scope, $http, $q, $window, stampService, user, $timeout, geolocation, leafletData, $filter, userRequire, mapService, notificationService, cryptoService, profileService) ->

    mapTimeout = null
    updateStampTimeout = null
    validateTimeout = null
    validateApplyTimeout = null
    currentStampTimeout = null
    t = null

    $scope.pageSectionTitle = 'Stamp Control Panel'
    $scope.forms = {}
    $scope.forms.stampdex = {}
    $scope.getCurrentStamp = null
    $scope.stampList = []
    $scope.stampControlList = []
    $scope.countryFilter = undefined
    currentPositionMarker = {}
    $scope.localityFilter = $scope.regionFilter = undefined
    $scope.updatedStampDefault = false

    _userRequire = userRequire()

    ###
    @name lovestamp.controller.StampControlController.addressLookup
    @inner

    @description
    Cycle through local list of stamps to match merchant alias.

    ###
    $scope.addressLookup = () ->

      foundStampMerchantAlias = false

      _.forEach($scope.stampList, (stampItself) ->
        if stampItself.merchantAlias is $scope.currentStamp.defaultAddress
          foundStampMerchantAlias = $scope.currentStamp.defaultAddress
      )

      if foundStampMerchantAlias isnt false
        $scope.validateAddy(foundStampMerchantAlias)
      return

    ###
    @name lovestamp.controller.StampControlController.validateAddy
    @function

    @description
    Validate stamp by alias found in monoatomic list link for stamp.

    ###
    $scope.validateAddy = (alias) ->

      $scope.oldStampAddress = alias

      $scope.updatedStampDefault = false

      mapService.getAddressByAlias(alias).then((validatedStampDefaultAddress) ->

        validateTimeout = $timeout ->
          $scope.$apply () ->
            $scope.updatedStampDefault = true

            try
              $scope.currentStamp.defaultAddress = validatedStampDefaultAddress.alias # Should be the name of the stamp by alias, fallback to ID, etc.

              _address = validatedStampDefaultAddress.$meta.address
              _hours = validatedStampDefaultAddress.$meta.hours
              _days = validatedStampDefaultAddress.$meta.days
              _activity = validatedStampDefaultAddress.$meta.activity

              if _hours
                #if !_hours.alwaysOpen or (!_hours.start and !_hours.end)
                  #notificationService.displayNote 'Please select hours available!'

                if _hours.alwaysOpen
                  _hours.start = ''
                  _hours.end = ''

                #if (_hours.alwaysOpen == undefined or _hours.alwaysOpen == false) and (_hours.start == '' or _hours.end == '')
                  #notificationService.displayNote 'Please choose an hourly specification!'

              #if !_days
                #notificationService.displayError 'Please select days available!'

              #if !_activity
                #notificationService.displayError 'Please tell your fans an activity to perform!'

              #if !_address
                #notificationService.displayError 'Please fill in your address!'

              $scope.currentStamp.address = _address
              $scope.currentStamp.hours = _hours
              $scope.currentStamp.days = _days
              $scope.currentStamp.activity = _activity

            catch e
              notificationService.displayError 'Unable to grab stamp details'
            return
          return
        , 0
        return

      , (error) ->

        validateApplyTimeout = $timeout ->
          $scope.$apply () ->
            $scope.updatedStampDefault = false
            $scope.currentStamp.defaultAddress = '' # Should be the name of the stamp by alias, fallback to ID, etc.
            return
          return
        , 0
        return
      )

    map = null
    assetLayerGroup = null
    stampMarker = null
    locationMarkerGroup = null
    data = {}

    stampGeoSettings = {}

    ### Loads a model for a Detail View of a stamp. Consider for ui-router. ###
    stampsMapDefaults =
      defaults:
        #tileLayer      : "http : //{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png"
        #tileLayer       : "https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png"
        tileLayer       : 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png'
        tileLayerOptions:
          opacity      : 1
          detectRetina : true
          reuseTiles   : true
        keyboard        : true
        dragging        : true
        doubleClickZoom : true
        tap             : true
        zoomControl     : true
        #minZoom: zoomSetting
        #maxZoom: zoomSetting

    _.extend $scope, stampsMapDefaults

    ###
    updateStampLocation
    @inner

    @description
    updateStampLocation

    @param {object} _map
    @param {object} mapData
    @param {object} _data
    ###
    updateStampLocation = (_map, mapData, _data) ->

      ###
      mapDragStart
      @inner updateStampLocation

      @description
      Map Drag Start

      @param {object:Event} e
      @return {undefined}
      ###
      mapDragStart = (e) ->

        stampMarker.dragging.disable()
        note = 'You have "use same details as". To amend the address, you must clear that field.'

        if $scope.currentStamp.defaultAddress isnt null
          notificationService.displayNote note
          return

        return

      ###
      mapDragEnd
      @inner updateStampLocation

      @description
      Map Drag End

      @param {object:Event} e
      @return {undefined}
      ###
      mapDragEnd = (e) ->

        stampMarker.dragging.enable()
        return

      _map.on('dragstart', mapDragStart)
      _map.on('dragend', mapDragEnd)

      # @TODO Apply marker for merchant to orient to stamp visually.

      _stamp = _.last($scope.stampControlList)

      if _stamp.address and _stamp.address.location
        staleLocation = _stamp.address.location

      deviceLocation =
        lat: _data.lat
        lng: _data.lng

      foundStaleLocation = staleLocation and staleLocation.lat and staleLocation.lng

      stampAddressConstruct =
        lat: if foundStaleLocation then staleLocation.lat else deviceLocation.lat
        lng: if foundStaleLocation then staleLocation.lng else deviceLocation.lng

      LeafIcon = L.Icon.extend(
        options:
          iconSize: [
            48
            48
          ]
          iconAnchor: [
            48/2
            48/2
          ]
      )

      defaultMapMarker = new LeafIcon(
        iconUrl: '/images/map-marker.png'
      )

      stampMarker = L.marker(
        [
          stampAddressConstruct.lat
          stampAddressConstruct.lng
        ],
          draggable : true
          icon      : defaultMapMarker
      ).addTo _map

      _map.setView(new L.LatLng(
        stampAddressConstruct.lat,
        stampAddressConstruct.lng
      ), zoomSetting)

      ###
      stampMarkerDragEnd

      @description
      Stamp Marker Drag End

      @param {object:Event} e
      @return {undefined}
      ###
      stampMarkerDragEnd = (e) ->

        if $scope.currentStamp.defaultAddress isnt null
            notificationService.displayNote 'You have "use same details as". To amend the address, you must clear that field.'
            return

        _location = e.target._latlng

        if cryptoService.hash(JSON.stringify(stampAddressConstruct)) != cryptoService.hash(JSON.stringify(_location))

          _.extend _stamp.address.location, _location

          updateStampTimeout = $timeout ->
            $scope.$apply () ->
              $scope.updateStamp(_stamp, true)
          , 0
        return

      stampMarker.on 'dragend', stampMarkerDragEnd

      angular.extend stampGeoSettings,
        lat  : stampAddressConstruct.lat
        lng  : stampAddressConstruct.lng
        zoom : zoomSetting

      return

    # Cache device location for reuse after first stamp map marker is loaded.
    geoData = null

    #_getLocation = geolocation.getLocation()

    #$scope.$on 'getMap', (event, mapData) ->

      #if !geoData
        #_getLocation.then((_data) ->

          #locationData =
            #lat: _data.coords.latitude
            #lng: _data.coords.longitude

          #geoData = _.extend {}, locationData

          #if !map
            #leafletData.getMap('stamp').then((_map) ->
              #map = _map
              #updateStampLocation(map, mapData, geoData)
            #)
          #else
            #updateStampLocation(map, mapData, geoData)

        #)
      #else
          #if !map
            #leafletData.getMap('stamp').then((_map) ->
              #map = _map
              #updateStampLocation(map, mapData, geoData)
            #)
          #else
            #updateStampLocation(map, mapData, geoData)

    ###
    @name lovestamp.controller.StampControlController.loadDetail
    @function

    @description

    @param {object} stamp
    ###
    $scope.loadDetail = (stamp) ->

      $scope.stampControlList = []
      personalMarkerHtml = null
      stampAlias = {}

      _userRequire.then (authData) ->

        handle = authData.handle
        $scope.currentStamp = stampService.getStamp(stamp, handle)

        try
          stamp._birthDate = if stamp.birthDate then stamp.birthDate.split('-') else stamp.birthDate.split('/')
          if stamp._birthDate.length
            birthDate = [
              stamp._birthDate[1] + '-'
              stamp._birthDate[0] + '-'
              stamp._birthDate[2]
            ].join ''
            stamp.birthDate = if birthDate then birthDate else null
          else
            stamp.birthDate = if birthDate then birthDate else null
        catch e
          stamp.birthDate = if birthDate then birthDate else ''

        stamp.registeredDate = if stamp.registeredDate then stamp.registeredDate else ''
        stamp.firstImpressionDate = if stamp.firstImpressionDate then stamp.firstImpressionDate else ''
        _stamp = stamp

        angular.extend $scope.currentStamp, _stamp

        $scope.stampControlList.push $scope.currentStamp

        stampAlias = angular.extend {}, $scope.stampControlList[0]

        #if stampMarker and map
          #map.removeLayer locationMarkerGroup

        $scope.$broadcast 'getMap', _stamp
        return

      angular.extend $scope,
        center : stampGeoSettings

      return

    ###
    @name lovestamp.controller.StampControlController.loadStampList
    @function

    @description

    ###
    $scope.loadStampList = () ->

      foundOriginUser = false
      originUserId = null
      _handle = null

      stampOwnerList = []
      $scope.stampList = []

      _userRequire.then (authData) ->
        handle = authData.handle
        profileService.getUserStatus(handle).$loaded().then (d) ->
          console.log(d.profile)

          _.forEach d.profile.networks, (userInNetwork) ->
            preparedId = userInNetwork.replace('[', '').replace(']', '')
            _preparedId = preparedId.split(':')
            __preparedId = _preparedId[1]
            profileService.getUserById(__preparedId).then (givenUser) ->

              if givenUser and givenUser.$id
                stampService.getLocalStamps(givenUser.$id).$loaded().then (__d) ->
                  _.forEach __d, (___d) ->
                    ___d.givenId = givenUser.$id
                    $scope.stampList.push ___d

      return


    ###
    @namespace loadZip  
    @description  
      Load zip code.
    @examples  
      - https://s3.amazonaws.com/zips.dryan.io/77006.json
        - {"locality": "Houston", "region": {"fips": "48", "abbr": "TX", "name": "Texas"}, "localities": ["Houston"], "postal_code": "77006", "lat": 29.741878, "lng": -95.38944, "type": "STANDARD", "counties": [{"fips": "201", "name": "Harris"}]}

    @param {string} _zipCode  
    @return {undefined}  
    ###
    $scope.loadZip = (_zipCode) ->
      zipCode = _zipCode

      if zipCode
        $http.get("https://s3.amazonaws.com/zips.dryan.io/#{zipCode}.json").
          success (zipData) ->
            #$scope.zip = zipData
            $scope.locality = zipData.locality
            $scope.localityFilter =
              city: $scope.locality
            $scope.region = zipData.region.name
            $scope.regionFilter =
              name: $scope.region
      else
        $scope.localityFilter = $scope.regionFilter = undefined
      return


    ###
    @function  
    @description  
      Load cities from reference file.
    @namespace loadCities  
    @return {undefined}
    ###
    $scope.loadCities = () ->
      $http.get('/reference/cities.json').
        success (cityData) ->
          $scope.cities = cityData.cities
      return


    ###
    @function chooseCity  
    @param {string} cityHandle  
    @return {undefined}
    ###
    $scope.chooseCity = (cityHandle) ->
      return


    ###
    @namespace loadCountries
    @return {undefined}
    ###
    $scope.loadCountries = () ->
      $http.get('/reference/countries.geo.json').
        success (countryData) ->
          $scope.countries = countryData.features
      return


    ###
    @namespace chooseCountry
    @return {undefined}
    ###
    $scope.chooseCountry = (countryHandle) ->
      $scope.countryHandleSelected = countryHandle.properties.name
      $scope.countryFilter =
        country: $scope.countryHandleSelected
      return


    ###
    @namespace loadStates
    @return {undefined}
    ###
    $scope.loadStates = () ->
      $http.get('/reference/states.geo.json').
        success (stateData) ->
          $scope.states = stateData.features
      return


    ###
    @namespace chooseState
    @param {string} stateHandle
    @return {undefined}
    ###
    $scope.chooseState = (stateHandle) ->
      return


    $scope.stampAdd = () ->
      _userRequire.then (authData) ->
        _handle = authData.handle

        foundOriginUser = false
        originUserId = null

        profileService.getUserStatus(_handle).$loaded().then (d) ->
          _.forEach d.profile.networks, (userInNetwork) ->
            if userInNetwork.indexOf('[') isnt -1
              foundOriginUser = true
              try
                originUserId = userInNetwork.split(':')
                _originUserId = originUserId[1].split(']')
                __originUserId = _originUserId[0]
                handle = __originUserId
              catch e
                handle = _handle

          if foundOriginUser != true
            handle = _handle

          if foundOriginUser
            profileService.getUserById(handle).then (givenUser) ->
              stampService.lock $scope.getCurrentStamp, givenUser.$id
          else
            stampService.lock $scope.getCurrentStamp, handle
        return
      return


    ###
    @namespace updateStamp
    @param {object} stamp
    @return {undefined}
    ###
    $scope.updateStamp = (stamp, dragUpdate) ->
      givenId = stamp.givenId

      if $scope.updatedStampDefault and dragUpdate
        return

      if stamp.hours.alwaysOpen == undefined
        stamp.hours.alwaysOpen = false

      if stamp.hours.start == undefined
        stamp.hours.start = ''

      if stamp.hours.end == undefined
        stamp.hours.end = ''

      if not stamp.days
        stamp.days =
          monday    : false
          tuesday   : false
          wednesday : false
          thursday  : false
          friday    : false
          saturday  : false
          sunday    : false

      foundOriginUser = false
      originUserId = null

      _userRequire.then (authData) ->

        _handle = authData.handle
        user = null
        handleUser = _handle

        profileService.getUserStatus(_handle).$loaded().then (d) ->
          _.forEach d.profile.networks, (userInNetwork) ->
            if userInNetwork.indexOf('[') isnt -1
              foundOriginUser = true
              try
                originUserId = userInNetwork.split(':')
                _originUserId = originUserId[1].split(']')
                __originUserId = _originUserId[0]
                user = __originUserId
              catch e
                console.dir(e)
          _u = null

          profileService.getUserById(user).then((givenUser) ->
            if foundOriginUser
              _u = givenUser.$id
            else
              _u = handleUser
          ).finally (uu) ->
            console.log uu
            console.log _u

            try
              profileService.getProfileById(_u).then (_d) ->
                stampService.updateStampAddress(stamp, givenId)
                notificationService.displayNote('Saved!')
            catch e
              notificationService.displayNote('Failed to update stamp address.')


    return

  [
    "$scope"
    "$http"
    "$q"
    "$window"
    "stamp.service"
    "user"
    "$timeout"
    "geolocation"
    "leafletData"
    "$filter"
    "user.require"
    "map.service"
    'notification.service'
    'crypto.service'
    'profile.service'
    StampControlController
  ]
