# https://lovestamp.io

## Web Application Merchant Backend Architecture

The Merchant backend is most clearly a hypermedia application[0], wherein web 
sites and web applications live — transported via HTTP1 and HTTP2 over TCP/IP — 
and which are deployed into web browsers which are the most hostile development 
environments imaginable. We will address, from an implementation standpoint, the 
fact of web browsers’ hostility by employing both Limited Hypermedia View-based 
Modeling, within the context of AngularJS Directives, and HATEOAS (hypermedia as 
the engine of application). With these tools, our application should possess, then 
certainly objectively viable qualities to sustain Generational Status, namely:

1. Robust Security Modeling
2. Modularity
3. Asynchronous Parallel Modeling
4. Distributed and Scalable Feature Sets (aligned with KPI for MVP Measurement)
5. Componentized Analytics over Modules for Regression Testing Modeling
6. API Embedded Controls
7. Extensional Microschema Details (aligned with Integration Test Schemas;  
   think Migrations for angular-material, etc.)
8. Simplified Continuous Integration (aligned with E2E Test Models)


—
[0]: "The WWW is fundamentally a distributed hypermedia application." — Taylor, Medividovic, Dashofy (2010)

## Pages

1. Pages as Modular Controllers

## Schema Ontology

See https://schema.org/WebSite which provides for 

1. WebPageElements (https://schema.org/WebPageElement)
2. PotentialAction (for example: http://schema.org/SearchAction)
