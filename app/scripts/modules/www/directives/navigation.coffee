###*
###

#
#
#
define [
  "interface"
  "config"
  "WpHeaderController"
], (__interface__, environment) ->

  ###*
  @inner
  ###
  siteNavigation = ($timeout, $log) ->
    _t = if window.is_device then './scripts/' else environment.baseTemplateUrl
    restrict: "A"
    controller: 'SiteNavController'
    templateUrl: _t + 'modules/www/controllers/header/partials/nav.html'
    link: ($scope, element, attr) ->
      return

  deps = [
    "$timeout"
    "$log"
    siteNavigation
  ]

  __interface__
    .directive "siteNavigation", deps

  return
