
/**
@fileOverview

                        _
        _           _  (_)                  _           _
  ___ _| |_ _____ _| |_ _  ____ _____ ___ _| |_ _____ _| |_  ___
 /___|_   _|____ (_   _) |/ ___|_____)___|_   _|____ (_   _)/___)
|___ | | |_/ ___ | | |_| ( (___     |___ | | |_/ ___ | | |_|___ |
(___/   \__)_____|  \__)_|\____)    (___/   \__)_____|  \__|___/

@description

  A generic stats directive for rendering static, interactive data with C3 
  graphs.
 */

(function() {
  define(["interface", "d3", "c3", "config"], function(__interface__, d3, c3, environment) {
    var deps, staticStats;
    staticStats = function($timeout, $log, $compile) {
      var compile;
      return {
        restrict: "A",
        transclude: true,
        templateUrl: function(elem, attrs) {
          return attrs.templateUrl || (environment.baseTemplateUrl + "www/partials/static-stats.html");
        },
        scope: {
          data_source: "=datasource"
        },
        compile: compile = function(a, b, c) {
          return {
            pre: function($scope, element, attrs, controller) {},
            post: function($scope, element, attrs) {
              var graph, html;
              graph = "<div id='" + attrs.staticStats + "'></div>";
              html = $compile(graph)($scope);
              element.find("div").append(html);
              console.log($scope.data_source);
              $timeout(function() {
                var chart;
                return chart = c3.generate({
                  bindto: "#" + attrs.staticStats,
                  data: {
                    columns: $scope.data_source
                  }
                });
              }, 0);
            }
          };
        }
      };
    };
    deps = ["$timeout", "$log", "$compile", staticStats];
    __interface__.directive("staticStats", deps);
  });

}).call(this);

//# sourceMappingURL=static-stats.js.map
