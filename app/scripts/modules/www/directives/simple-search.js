
/**
 */

(function() {
  define(["interface"], function(__interface__) {

    /**
    @inner
     */
    var deps, simpleSearch;
    simpleSearch = function($timeout, $log) {
      return {
        restrict: "A",
        scope: {
          referral_scope: "=referralScope"
        },
        link: function($scope, element, attr) {
          console.dir(element);
          console.dir($scope);
        }
      };
    };
    "use strict";
    deps = ["$timeout", "$log", simpleSearch];
    __interface__.directive("simpleSearch", deps);
  });

}).call(this);

//# sourceMappingURL=simple-search.js.map
