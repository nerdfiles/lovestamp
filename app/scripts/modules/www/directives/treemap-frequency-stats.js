
/**
 * fileOverview

       _
     _| |_  ____ _____ _____ ____  _____ ____
    (_   _)/ ___) ___ | ___ |    \(____ |  _ \
      | |_| |   | ____| ____| | | / ___ | |_| |
       \__)_|   |_____)_____)_|_|_\_____|  __/
                                        |_|
        ___
       / __)
     _| |__ ____ _____  ____ _   _ _____ ____   ____ _   _
    (_   __) ___) ___ |/ _  | | | | ___ |  _ \ / ___) | | |
      | | | |   | ____| |_| | |_| | ____| | | ( (___| |_| |
      |_| |_|   |_____)\__  |____/|_____)_| |_|\____)\__  |
                          |_|                       (____/

            _           _
      ___ _| |_ _____ _| |_  ___
     /___|_   _|____ (_   _)/___)
    |___ | | |_/ ___ | | |_|___ |
    (___/   \__)_____|  \__|___/

 *# description

A specialized directive for rendering D3 recursive treemaps which are 
expressions of intensity circles where size and hue intensity of rectangular 
areas represent social data points corresponding to frequency of 
stampenings of certain types of social archetypes within D3's hierarchy 
layout system[0].

__Paradigms Examples__

___Social Connectors___

Smaller squares which are deeper in hue (so, vivid blue light versus vivid) 
are "connectors", suggesting that the fan (customer) does not personally 
receive stamps often, but has many friends who do, etc.

___Loyal Asocialists___

Larger squares with lighter hue are more highly frequent stampees who 
do not have many friends.

—
[0]: https://github.com/mbostock/d3/wiki/Hierarchy-Layout
 */

(function() {
  define(["interface", "lodash", "jquery", "d3", "config"], function(__interface__, _, $, d3, environment) {
    var deps, staticStats;
    staticStats = function($timeout, $log, $compile, $window) {

      /*
      static stats
       */
      var init, _compile;
      init = function(_element, _merchantSystem) {

        /* @inner */
        var cell, count, element, h, layoutStyle, merchantSystem, node, nodes, root, size, svg, treemap, w, x, y, zoom;
        element = '#' + _element;
        merchantSystem = _merchantSystem;
        w = null;
        h = null;
        layoutStyle = 'squarify';
        (function() {
          w = angular.element(document.querySelector('.monad--graph')).width();
          h = (w * .5) - 180;
          if (w < 390) {
            layoutStyle = 'squarify';
            return h = w * 3;
          }
        })();
        x = d3.scale.linear().range([0, w]);
        y = d3.scale.linear().range([0, h]);
        root = void 0;
        node = void 0;
        treemap = d3.layout.treemap().mode([layoutStyle]).round(false).size([w, h]).sticky(true).padding([10, 0, 0, 0]).value(function(d) {
          return d.size;
        });
        svg = d3.select(element).append('div').attr('class', 'chart').style('width', w + 'px').style('height', h + 'px').append('svg:svg').attr('width', w).attr('height', h).append('svg:g').attr('transform', 'translate(.5,.5)');
        size = function(d) {
          return d.size;
        };
        count = function(d) {
          return 1;
        };
        zoom = function(d) {
          var kx, ky, t;
          kx = w / d.dx;
          ky = h / d.dy;
          x.domain([d.x, d.x + d.dx]);
          y.domain([d.y, d.y + d.dy]);
          t = svg.selectAll('g.cell').transition().duration(d3.event.altKey ? 7500 : 750).attr('transform', function(d) {
            return 'translate(' + x(d.x) + ',' + y(d.y) + ')';
          });
          t.select('rect').attr('width', function(d) {
            return kx * d.dx - 1;
          }).attr('height', function(d) {
            return ky * d.dy - 1;
          });
          t.select('text').attr('x', function(d) {
            return kx * d.dx / 2;
          }).attr('y', function(d) {
            return ky * d.dy / 2;
          }).style("font-size", function(d) {
            var fontSize;
            fontSize = d.fontSize ? d.fontSize : "12px";
            return d.fontSize;
          }).style("opacity", function(d) {
            if (kx * d.dx > d.w) {
              return 1;
            } else {
              return 0;
            }
          });
          node = d;
          d3.event.stopPropagation();
        };
        node = root = merchantSystem;
        nodes = treemap.nodes(root).filter(function(d) {
          return !d.children;
        });
        cell = svg.selectAll('g').data(nodes).enter().append('svg:g').attr('class', 'cell').attr('transform', function(d) {
          return 'translate(' + d.x + ',' + d.y + ')';
        }).on('click', function(d) {
          return zoom(node === d ? root : d);
        }).on('touchstart', function(d) {
          return zoom(node === d ? root : d);
        });
        cell.append('svg:rect').attr('width', function(d) {
          return d.dx - 1;
        }).attr('height', function(d) {
          return d.dy - 1;
        }).style('fill', function(d) {
          var colorScale;
          if (d.parent && !_.isNaN(d.parent.totalColorSpace)) {
            if (d.intensity > 1) {
              colorScale = d3.scale.linear().domain([0, d.parent.totalColorSpace]).range(["#51c3e2", "#2464ae"]);
              return colorScale(d.intensity);
            } else {
              colorScale = d3.scale.linear().domain([0, d.parent.totalColorSpace]).range(["#d3d3d3", "#d9d9d9"]);
              return colorScale(d.intensity);
            }
          }
        });
        return cell.append('svg:a').attr("target", "_blank").attr("xlink:href", function(d) {
          var fb, gp, link, prefixUrl, tw, _prefix;
          tw = 'https://twitter.com/';
          gp = 'https://plus.google.com/';
          fb = 'https://facebook.com/';
          if (d.id) {
            _prefix = d.id.charAt(0);
            prefixUrl = _prefix === '@' ? tw : _prefix === '✚' ? gp : fb;
            return link = d.link = prefixUrl + d.id.replace(_prefix, '');
          }
        }).on('touchstart', function(d) {
          return $window.open(d.link, '_system');
        }).append('svg:text').attr('x', function(d) {
          return d.dx / 2;
        }).attr('y', function(d) {
          return d.dy / 2;
        }).attr('dy', function() {
          return '.15em';
        }).attr('text-anchor', 'middle').text(function(d) {
          return d.name;
        }).style('opacity', function(d) {
          d.w = this.getComputedTextLength();
          if (d.dx > d.w) {
            return 1;
          } else {
            return 0;
          }
        }).attr('class', function() {
          return '~font-effect-3d-float';
        }).style("font-size", function(d) {
          var _d;
          d.w = this.getComputedTextLength();
          _d = null;
          if (d.dx > d.w + (d.w * .8)) {
            _d = d.fontSize = "16px";
          } else {
            _d = d.fontSize = "12px";
          }
          return _d;
        });
      };
      _compile = function(_element, _attrs, transclude) {
        return {
          post: function($scope, element, attrs, controller) {
            var graph, html, t;
            graph = "<div id='" + attrs.staticStats + "'></div>";
            html = $compile(graph)($scope);
            element.find("div").append(html);
            t = null;
            $scope.$on('$destroy', function() {
              if (t) {
                return $timeout.cancel(t);
              }
            });
            t = $timeout(function() {
              if ($scope.data_source) {
                return init(attrs.staticStats, $scope.data_source);
              }
            }, 1500);
          }
        };
      };
      return {
        restrict: "A",
        transclude: true,
        priority: -1,
        terminal: false,
        templateUrl: function(elem, attrs) {
          return attrs.templateUrl || (environment.baseTemplateUrl + "modules/www/partials/treemap-frequency-stats.html");
        },
        scope: {
          data_source: "=datasource"
        },
        compile: _compile
      };
    };
    deps = ["$timeout", "$log", "$compile", "$window", staticStats];
    __interface__.directive("staticStats", deps);
  });

}).call(this);

//# sourceMappingURL=treemap-frequency-stats.js.map
