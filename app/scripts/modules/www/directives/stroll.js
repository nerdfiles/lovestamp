
/**
 */

(function() {
  define(["interface", "stroll"], function(__interface__) {

    /**
    @inner
     */
    var deps, listStroll;
    listStroll = function($timeout, $log) {
      return {
        restrict: "A",
        link: function($scope, element, attr) {
          stroll.bind(element[0], {
            live: true
          });
        }
      };
    };
    deps = ["$timeout", "$log", listStroll];
    __interface__.directive("listStroll", deps);
  });

}).call(this);

//# sourceMappingURL=stroll.js.map
