
/**
 */

(function() {
  define(["interface", "config", "WpHeaderController"], function(__interface__, environment) {

    /**
    @inner
     */
    var deps, siteNavigation;
    siteNavigation = function($timeout, $log) {
      var _t;
      _t = window.is_device ? './scripts/' : environment.baseTemplateUrl;
      return {
        restrict: "A",
        controller: 'SiteNavController',
        templateUrl: _t + 'modules/www/controllers/header/partials/nav.html',
        link: function($scope, element, attr) {}
      };
    };
    deps = ["$timeout", "$log", siteNavigation];
    __interface__.directive("siteNavigation", deps);
  });

}).call(this);

//# sourceMappingURL=navigation.js.map
