###*
@fileOverview

                        _
        _           _  (_)                  _           _
  ___ _| |_ _____ _| |_ _  ____ _____ ___ _| |_ _____ _| |_  ___
 /___|_   _|____ (_   _) |/ ___|_____)___|_   _|____ (_   _)/___)
|___ | | |_/ ___ | | |_| ( (___     |___ | | |_/ ___ | | |_|___ |
(___/   \__)_____|  \__)_|\____)    (___/   \__)_____|  \__|___/

@description

  A generic stats directive for rendering static, interactive data with C3 
  graphs.

###

define [
  "interface"
  "d3"
  "c3"
  "config"
], (__interface__, d3, c3, environment) ->

  staticStats = ($timeout, $log, $compile) ->
    restrict: "A"
    transclude: true
    templateUrl: (elem, attrs) ->
      attrs.templateUrl or (environment.baseTemplateUrl + "www/partials/static-stats.html")
    scope:
      data_source: "=datasource" # @see columns data structure spec below
    compile: compile = (a, b, c) ->
      pre: ($scope, element, attrs, controller) ->
        return
      post: ($scope, element, attrs) ->

        graph = "<div id='" + attrs.staticStats + "'></div>"

        html = $compile(graph)($scope)

        element.find("div").append html

        console.log $scope.data_source

        $timeout ->
          chart = c3.generate(
            bindto: "#" + attrs.staticStats
            data:
              columns: $scope.data_source
          )
        , 0

        return

  deps = [
    "$timeout"
    "$log"
    "$compile"
    staticStats
  ]

  __interface__.
    directive "staticStats", deps
  return
