###*
# fileOverview

       _
     _| |_  ____ _____ _____ ____  _____ ____
    (_   _)/ ___) ___ | ___ |    \(____ |  _ \
      | |_| |   | ____| ____| | | / ___ | |_| |
       \__)_|   |_____)_____)_|_|_\_____|  __/
                                        |_|
        ___
       / __)
     _| |__ ____ _____  ____ _   _ _____ ____   ____ _   _
    (_   __) ___) ___ |/ _  | | | | ___ |  _ \ / ___) | | |
      | | | |   | ____| |_| | |_| | ____| | | ( (___| |_| |
      |_| |_|   |_____)\__  |____/|_____)_| |_|\____)\__  |
                          |_|                       (____/

            _           _
      ___ _| |_ _____ _| |_  ___
     /___|_   _|____ (_   _)/___)
    |___ | | |_/ ___ | | |_|___ |
    (___/   \__)_____|  \__|___/

## description

A specialized directive for rendering D3 recursive treemaps which are 
expressions of intensity circles where size and hue intensity of rectangular 
areas represent social data points corresponding to frequency of 
stampenings of certain types of social archetypes within D3's hierarchy 
layout system[0].

__Paradigms Examples__

___Social Connectors___

Smaller squares which are deeper in hue (so, vivid blue light versus vivid) 
are "connectors", suggesting that the fan (customer) does not personally 
receive stamps often, but has many friends who do, etc.

___Loyal Asocialists___

Larger squares with lighter hue are more highly frequent stampees who 
do not have many friends.

—
[0]: https://github.com/mbostock/d3/wiki/Hierarchy-Layout

###

define [
  "interface"
  "lodash"
  "jquery"
  "d3"
  "config"
], (__interface__, _, $, d3, environment) ->

  staticStats = ($timeout, $log, $compile, $window) ->
    ###
    static stats
    ###

    init = (_element, _merchantSystem) ->
      ### @inner ###
      element = '#' + _element

      merchantSystem = _merchantSystem

      w = null
      h = null
      layoutStyle = 'squarify'

      (() ->
        w = angular.element(document.querySelector('.monad--graph')).width()
        h = (w * .5) - 180

        if w < 390
          #layoutStyle = 'slice'
          #layoutStyle = 'dice'
          #layoutStyle = 'slice-dice'
          layoutStyle = 'squarify'
          h = w * 3
      )()

      x = d3.scale.linear().range([
        0
        w
      ])

      y = d3.scale.linear().range([
        0
        h
      ])

      root = undefined
      node = undefined

      treemap = d3.layout.
        treemap().
        mode([
          layoutStyle
        ]).
        round(false).
        size([
            w
            h
          ]).
        sticky(true).
        padding([
            10
            0
            0
            0
          ]).
        value((d) ->
          d.size
        )

      svg = d3.select(element).
        append('div').
        attr('class', 'chart').
          style('width', w + 'px').
          style('height', h + 'px').
        append('svg:svg').
        attr('width', w).
        attr('height', h).
        append('svg:g').
        attr('transform', 'translate(.5,.5)')

      size = (d) ->
        d.size

      count = (d) ->
        1

      zoom = (d) ->

        kx = w / d.dx
        ky = h / d.dy

        x.domain [
          d.x
          d.x + d.dx
        ]
        y.domain [
          d.y
          d.y + d.dy
        ]

        t = svg.selectAll('g.cell').transition().
          duration(if d3.event.altKey then 7500 else 750).
          attr('transform', (d) ->
            'translate(' + x(d.x) + ',' + y(d.y) + ')'
        )

        t.select('rect').attr('width', (d) ->
          kx * d.dx - 1
        ).attr 'height', (d) ->
          ky * d.dy - 1

        t.select('text').attr('x', (d) ->
          kx * d.dx / 2
        ).attr('y', (d) ->
          ky * d.dy / 2
        )
        .style("font-size", (d) ->
          fontSize = if d.fontSize then d.fontSize else "12px"
          #return if (kx * d.dx > d.w) then "12px" else fontSize
          d.fontSize
        )
        .style("opacity", (d) ->
          return if (kx * d.dx > d.w) then 1 else 0
        )

        node = d
        d3.event.stopPropagation()
        return

      node = root = merchantSystem
      nodes = treemap.nodes(root).filter((d) ->
        !d.children
      )

      cell = svg.selectAll('g').data(nodes).
        enter().
        append('svg:g').
        attr('class', 'cell').
        attr('transform', (d) ->
          'translate(' + d.x + ',' + d.y + ')'
        )
        .on('click', (d) ->
          zoom if node == d then root else d
        )
        .on('touchstart', (d) ->
          zoom if node == d then root else d
        )

      cell.append('svg:rect').
        attr('width', (d) ->
          d.dx - 1
      ).attr('height', (d) ->
        d.dy - 1
      ).style 'fill', (d) ->
        if d.parent and ! _.isNaN(d.parent.totalColorSpace)
          if d.intensity > 1
            colorScale = d3.scale
              .linear()
              .domain([
                0
                d.parent.totalColorSpace
              ])
              .range([
                "#51c3e2"
                "#2464ae"
              ])
            return colorScale( d.intensity )
          else
            colorScale = d3.scale
              .linear()
              .domain([
                0
                d.parent.totalColorSpace
              ])
              .range([
                "#d3d3d3"
                "#d9d9d9"
              ])
            return colorScale( d.intensity )

      cell
        .append('svg:a')
        .attr("target", "_blank")
        .attr("xlink:href", (d) ->
          tw = 'https://twitter.com/'
          gp = 'https://plus.google.com/'
          fb = 'https://facebook.com/'
          if d.id
            _prefix = d.id.charAt(0)
            prefixUrl = if _prefix is '@' then tw else if _prefix is '✚' then gp else fb
            link = d.link = prefixUrl + d.id.replace(_prefix, '')
        )
        .on('touchstart', (d) ->
          $window.open(d.link,'_system')
        )
        .append('svg:text')
        .attr('x', (d) ->
          d.dx / 2
        )
        .attr('y', (d) ->
          d.dy / 2
        )
        .attr('dy', () ->
          '.15em'
        )
        .attr('text-anchor', 'middle')
        .text((d) ->
          d.name
        )
        .style('opacity', (d) ->
          d.w = @getComputedTextLength()
          if d.dx > d.w then 1 else 0
        )
        .attr('class', () ->
          '~font-effect-3d-float'
        )
        .style("font-size", (d) ->

          d.w = @getComputedTextLength()
          _d = null
          if d.dx > d.w + (d.w * .8)
            _d = d.fontSize = "16px"
          else
            _d = d.fontSize = "12px"
          _d
        )


    _compile = (_element, _attrs, transclude) ->
      post: ($scope, element, attrs, controller) ->

        graph = "<div id='" + attrs.staticStats + "'></div>"

        html = $compile(graph)($scope)

        element.find("div").append html

        t = null

        $scope.$on '$destroy', () ->
          if t
            $timeout.cancel t

        t = $timeout ->
          if $scope.data_source
            init(attrs.staticStats, $scope.data_source)
        , 1500
        return


    restrict    : "A"
    transclude  : true
    priority    : -1
    terminal    : false
    templateUrl : (elem, attrs) ->
      attrs.templateUrl or (environment.baseTemplateUrl + "modules/www/partials/treemap-frequency-stats.html")
    scope:
      data_source: "=datasource" # @see columns data structure spec below
    compile: _compile

  deps = [
    "$timeout"
    "$log"
    "$compile"
    "$window"
    staticStats
  ]

  __interface__.
    directive "staticStats", deps
  return
