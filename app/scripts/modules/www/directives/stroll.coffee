###*
###

#
#
#
define [
  "interface"
  "stroll"
], (__interface__) ->

  ###*
  @inner
  ###
  listStroll = ($timeout, $log) ->
    restrict: "A"
    link: ($scope, element, attr) ->
      stroll.bind( element[0], { live: true })
      return

  deps = [
    "$timeout"
    "$log"
    listStroll
  ]

  __interface__
    .directive "listStroll", deps
  return

