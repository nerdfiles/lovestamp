define [
  "interface"
  "lodash"
  "config"
  "services/authentication"
  "services/bank"
  "services/profile"
], (__interface__, _, environment) ->

  profileImageMaskContent = ($timeout, $rootScope, $interval, bankService, userRequire, profileService, $q, $state, $location) ->
    _t = if window.is_device then './scripts/' else environment.baseTemplateUrl
    restrict: 'A'
    templateUrl: _t + 'modules/index/partials/profile-image-mask.html'
    link: ($scope, element, attrs) ->
      _userRequire = userRequire()

      $scope.loadProfile = (handle) ->
        $location.path('fan/user/profile')
        return

      _ListTotal = 0

      _userRequire.then (user) ->
        try
          _handle = user.handle
          _currentUser = profileService.getProfile(_handle)
          _currentUser.then (_userProperties) ->
              _currentUserBitsCollected = _.pluck(_userProperties, 'bitsCollected')
              _currentUserBitsCollectedSanitized = _.filter _currentUserBitsCollected, (userProperty) ->
                userProperty
              _topUser = profileService.getTopUser()
              _topUser.then (topUserBitsCollected) ->
                $scope.bitsProgress = Math.floor((_currentUserBitsCollectedSanitized / topUserBitsCollected) * 100)
                $timeout ->
                  $scope.$apply()
                , 0
        catch e
          console.log 'User has been logged out.'

      #payoutBudgetTotal = bankService.getPayoutBudget()
      ## @TODO Cleanup.
      #profileService.getLoggedInUsersCount().then (usersLoggedInCount) ->
        #console.log usersLoggedInCount
      #_userRequire.then (user) ->
        #_handle = user.handle
        ## Get transactional data from user account.
        #successesTotal = profileService.getUserTransactions(_handle)
        ## Load transactional array to determine number of total stampenings against the fan
        #successesTotal.$loaded().then (u) ->
          #_List = _.map u, (_u) ->
            #successStampsList = _.toArray _u
            #successStampsList.length
          #_ListTotal = _.reduce _List, (sum, n) ->
            #sum + n
          #console.log _ListTotal
          #payoutBudgetTotal.$loaded().then (t) ->
            #_t = _ListTotal / t
            ## @note Testing out a delayed load interaction with leafletData performance.
            #$timeout ->
              #$scope.bitsProgress = _t
              #$scope.$apply()
            #, 50

      _userRequire.then (user) ->

        return  if not user

        $timeout ->
          $scope.$apply () ->
            currentProfile = $scope.currentProfile = profileService.currentProfile = user[user.provider].cachedUserProfile

            if currentProfile

              twitterBg = profileService.currentProfile.profile_image_url
              facebookBg = profileService.currentProfile.picture and profileService.currentProfile.picture.data
              googleBg = profileService.currentProfile.picture and not profileService.currentProfile.picture.data

              if twitterBg
                bg = currentProfile.profile_image_url.replace('_normal', '')

              if facebookBg
                bg = currentProfile.picture.data.url

              if googleBg
                ###
                    google_spec =
                      family_name : "Hah"
                      gender      : "other"
                      given_name  : "Ɐha"
                      id          : "100541351182813566938"
                      link        : "https://plus.google.com/100541351182813566938"
                      locale      : "en"
                      name        : "Ɐha Hah (nerdfiles)"
                      picture     : "https://lh5.googleusercontent.com/-rJqQ0GxcdY8/AAAAAAAAAAI/AAAAAAAAD6o/PcGS4XpysFo/photo.jpg"
                ###

                bg = currentProfile.picture

              anchor = element.find('a')

              if bg.indexOf('https') is -1
                _bg = bg.replace(/http/g, 'https')
              else
                _bg = bg

              if element and user
                anchor[0].setAttribute('style', 'background-image: url(' + _bg + ')')

        , 0

      return

  __interface__

  .directive 'profileImageMaskContent', [
    '$timeout'
    '$rootScope'
    '$interval'
    'bank.service'
    'user.require'
    'profile.service'
    '$q'
    '$state'
    '$location'
    profileImageMaskContent
  ]

  return
