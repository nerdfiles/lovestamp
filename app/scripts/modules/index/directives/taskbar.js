
/*
 * fileOverview

                      _     _
       _             | |   | |
     _| |_ _____  ___| |  _| |__  _____  ____
    (_   _|____ |/___) |_/ )  _ \(____ |/ ___)
      | |_/ ___ |___ |  _ (| |_) ) ___ | |
       \__)_____(___/|_| \_)____/\_____|_|

 *# description
 */

(function() {
  define(['interface', 'services/profile'], function(__interface__) {
    var taskbar;
    taskbar = function() {
      var linker;
      linker = function($scope, element, $attrs) {

        /*
        @inner
         */
      };
      return {

        /*
         */
        restrict: 'A',
        scope: {},
        link: linker
      };
    };
    __interface__.directive(['profile.service', taskbar]);
  });

}).call(this);

//# sourceMappingURL=taskbar.js.map
