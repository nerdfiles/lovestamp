(function() {
  define(["interface", "lodash", "config", "services/authentication", "services/bank", "services/profile"], function(__interface__, _, environment) {
    var profileImageMaskContent;
    profileImageMaskContent = function($timeout, $rootScope, $interval, bankService, userRequire, profileService, $q, $state, $location) {
      var _t;
      _t = window.is_device ? './scripts/' : environment.baseTemplateUrl;
      return {
        restrict: 'A',
        templateUrl: _t + 'modules/index/partials/profile-image-mask.html',
        link: function($scope, element, attrs) {
          var _ListTotal, _userRequire;
          _userRequire = userRequire();
          $scope.loadProfile = function(handle) {
            $location.path('fan/user/profile');
          };
          _ListTotal = 0;
          _userRequire.then(function(user) {
            var e, _currentUser, _handle;
            try {
              _handle = user.handle;
              _currentUser = profileService.getProfile(_handle);
              return _currentUser.then(function(_userProperties) {
                var _currentUserBitsCollected, _currentUserBitsCollectedSanitized, _topUser;
                _currentUserBitsCollected = _.pluck(_userProperties, 'bitsCollected');
                _currentUserBitsCollectedSanitized = _.filter(_currentUserBitsCollected, function(userProperty) {
                  return userProperty;
                });
                _topUser = profileService.getTopUser();
                return _topUser.then(function(topUserBitsCollected) {
                  $scope.bitsProgress = Math.floor((_currentUserBitsCollectedSanitized / topUserBitsCollected) * 100);
                  return $timeout(function() {
                    return $scope.$apply();
                  }, 0);
                });
              });
            } catch (_error) {
              e = _error;
              return console.log('User has been logged out.');
            }
          });
          _userRequire.then(function(user) {
            if (!user) {
              return;
            }
            return $timeout(function() {
              return $scope.$apply(function() {
                var anchor, bg, currentProfile, facebookBg, googleBg, twitterBg, _bg;
                currentProfile = $scope.currentProfile = profileService.currentProfile = user[user.provider].cachedUserProfile;
                if (currentProfile) {
                  twitterBg = profileService.currentProfile.profile_image_url;
                  facebookBg = profileService.currentProfile.picture && profileService.currentProfile.picture.data;
                  googleBg = profileService.currentProfile.picture && !profileService.currentProfile.picture.data;
                  if (twitterBg) {
                    bg = currentProfile.profile_image_url.replace('_normal', '');
                  }
                  if (facebookBg) {
                    bg = currentProfile.picture.data.url;
                  }
                  if (googleBg) {

                    /*
                        google_spec =
                          family_name : "Hah"
                          gender      : "other"
                          given_name  : "Ɐha"
                          id          : "100541351182813566938"
                          link        : "https://plus.google.com/100541351182813566938"
                          locale      : "en"
                          name        : "Ɐha Hah (nerdfiles)"
                          picture     : "https://lh5.googleusercontent.com/-rJqQ0GxcdY8/AAAAAAAAAAI/AAAAAAAAD6o/PcGS4XpysFo/photo.jpg"
                     */
                    bg = currentProfile.picture;
                  }
                  anchor = element.find('a');
                  if (bg.indexOf('https') === -1) {
                    _bg = bg.replace(/http/g, 'https');
                  } else {
                    _bg = bg;
                  }
                  if (element && user) {
                    return anchor[0].setAttribute('style', 'background-image: url(' + _bg + ')');
                  }
                }
              });
            }, 0);
          });
        }
      };
    };
    __interface__.directive('profileImageMaskContent', ['$timeout', '$rootScope', '$interval', 'bank.service', 'user.require', 'profile.service', '$q', '$state', '$location', profileImageMaskContent]);
  });

}).call(this);

//# sourceMappingURL=profile-image-mask-content.js.map
