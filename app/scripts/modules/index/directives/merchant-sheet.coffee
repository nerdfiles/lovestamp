###*
@fileOverview

                        _                              _
                       | |                 _          | |                  _
 ____  _____  ____ ____| |__  _____ ____ _| |_     ___| |__  _____ _____ _| |_
|    \| ___ |/ ___) ___)  _ \(____ |  _ (_   _)   /___)  _ \| ___ | ___ (_   _)
| | | | ____| |  ( (___| | | / ___ | | | || |_   |___ | | | | ____| ____| | |_
|_|_|_|_____)_|   \____)_| |_\_____|_| |_| \__)  (___/|_| |_|_____)_____)  \__)

@module index/directives
@description

  A directive for basic interactions and deployment of a bottom sheet.

###

define [
  "interface"
  "config"
  "directives/relExternal"
], (__interface__, environment) ->

    merchantSheet = ($timeout, $mdBottomSheet, $rootScope) ->
        restrict: 'A'
        link: ($scope, element) ->
            _t = if window.is_device then './scripts/' else environment.baseTemplateUrl

            $scope.$on('prestamp:sheet', (event, merchantData) ->
                $rootScope.merchantSheetData = merchantData
                $mdBottomSheet.show(
                    templateUrl   : _t + "modules/index/partials/merchant-sheet.html"
                    controller    : "MerchantSheetCtrl"
                )
            )

            return

    __interface__.

        directive('merchantSheet', [
            '$timeout'
            '$mdBottomSheet'
            '$rootScope'
            merchantSheet
        ])

    return
