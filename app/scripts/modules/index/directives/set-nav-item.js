(function() {
  define(["interface", "lodash"], function(__interface__, _) {
    __interface__.directive('setNavItem', [
      '$timeout', '$interval', 'user.require', '$q', 'localStorageService', function($timeout, $interval, userRequire, $q, localStorageService) {
        return {
          restrict: 'A',
          transclude: false,
          scope: false,
          link: function($scope, element, $attrs) {
            var axisXName, axisYName, body$, defaultXPosition, defaultYPosition, navItemName, ns, sep, setNavItem, _loadedXPosition, _loadedYPosition;
            defaultXPosition = parseInt($attrs.navItemDefaultXPosition, 10);
            defaultYPosition = parseInt($attrs.navItemDefaultYPosition, 10);
            body$ = document.querySelector('body');
            $(element.find('div').find('a')).on('dragstart.setNavItem', function() {
              return angular.element(body$).toggleClass('dragging');
            });
            $(element.find('div').find('a')).on('dragend.setNavItem', function() {
              return angular.element(body$).toggleClass('dragging');
            });
            navItemName = $attrs.navItemName;
            ns = 'navItem';
            axisXName = 'X';
            axisYName = 'Y';
            sep = '.';
            _loadedXPosition = localStorageService.get(ns + axisXName + sep + navItemName) || defaultXPosition;
            _loadedYPosition = localStorageService.get(ns + axisYName + sep + navItemName) || defaultYPosition;
            element.css('left', _loadedXPosition + 'px');
            element.css('top', _loadedYPosition + 'px');
            return setNavItem = function() {
              localStorageService.set(ns + axisXName + sep + navItemName, _loadedXPosition || defaultXPosition);
              localStorageService.set(ns + axisYName + sep + navItemName, _loadedYPosition || defaultYPosition);
            };
          }
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=set-nav-item.js.map
