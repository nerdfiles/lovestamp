
/**
@fileOverview

                        _                              _
                       | |                 _          | |                  _
 ____  _____  ____ ____| |__  _____ ____ _| |_     ___| |__  _____ _____ _| |_
|    \| ___ |/ ___) ___)  _ \(____ |  _ (_   _)   /___)  _ \| ___ | ___ (_   _)
| | | | ____| |  ( (___| | | / ___ | | | || |_   |___ | | | | ____| ____| | |_
|_|_|_|_____)_|   \____)_| |_\_____|_| |_| \__)  (___/|_| |_|_____)_____)  \__)

@module index/directives
@description

  A directive for basic interactions and deployment of a bottom sheet.
 */

(function() {
  define(["interface", "config", "directives/relExternal"], function(__interface__, environment) {
    var merchantSheet;
    merchantSheet = function($timeout, $mdBottomSheet, $rootScope) {
      return {
        restrict: 'A',
        link: function($scope, element) {
          var _t;
          _t = window.is_device ? './scripts/' : environment.baseTemplateUrl;
          $scope.$on('prestamp:sheet', function(event, merchantData) {
            $rootScope.merchantSheetData = merchantData;
            return $mdBottomSheet.show({
              templateUrl: _t + "modules/index/partials/merchant-sheet.html",
              controller: "MerchantSheetCtrl"
            });
          });
        }
      };
    };
    __interface__.directive('merchantSheet', ['$timeout', '$mdBottomSheet', '$rootScope', merchantSheet]);
  });

}).call(this);

//# sourceMappingURL=merchant-sheet.js.map
