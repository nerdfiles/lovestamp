define [
  "interface"
  "lodash"
], (__interface__, _) ->

  __interface__

  .directive 'setNavItem', [
    '$timeout'
    '$interval'
    'user.require'
    '$q'
    'localStorageService'
    ($timeout, $interval, userRequire, $q, localStorageService) ->
      restrict: 'A'
      transclude: false
      scope: false
      #require: '^ngDrag'
      #scope: {
        #'stampScreenDragData': '=stampScreenDragData'
        #'activityLinksDragData': '=activityLinksDragData'
        #'onDragComplete': '&onDragComplete'
        #'onDropComplete': '&onDropComplete'
      #}
      link: ($scope, element, $attrs) ->

        defaultXPosition = parseInt $attrs.navItemDefaultXPosition, 10
        defaultYPosition = parseInt $attrs.navItemDefaultYPosition, 10

        body$ = document.querySelector 'body'
        $(element.find('div').find('a')).on 'dragstart.setNavItem', () ->
            angular.element(body$).toggleClass 'dragging'

        $(element.find('div').find('a')).on 'dragend.setNavItem', () ->
            angular.element(body$).toggleClass 'dragging'

        navItemName = $attrs.navItemName

        ns = 'navItem'
        axisXName = 'X'
        axisYName = 'Y'
        sep = '.'

        _loadedXPosition = localStorageService.get(ns + axisXName + sep + navItemName) or defaultXPosition
        _loadedYPosition = localStorageService.get(ns + axisYName + sep + navItemName) or defaultYPosition

        element.css 'left', ( _loadedXPosition + 'px' )
        element.css 'top', ( _loadedYPosition + 'px' )

        setNavItem = () ->
          localStorageService.set(ns + axisXName + sep + navItemName, _loadedXPosition or defaultXPosition)
          localStorageService.set(ns + axisYName + sep + navItemName, _loadedYPosition or defaultYPosition)
          return

        #return

  ]
  return

