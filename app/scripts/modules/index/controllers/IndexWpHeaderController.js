
/**
@fileOverview

 _           _             _  _  _       _     _                 _
| |         | |           (_)(_)(_)     (_)   (_)               | |
| |____   __| |_____ _   _ _  _  _ ____  _______ _____ _____  __| |_____  ____
| |  _ \ / _  | ___ ( \ / ) || || |  _ \|  ___  | ___ (____ |/ _  | ___ |/ ___)
| | | | ( (_| | ____|) X (| || || | |_| | |   | | ____/ ___ ( (_| | ____| |
|_|_| |_|\____|_____|_/ \_)\_____/|  __/|_|   |_|_____)_____|\____|_____)_|
                                  |_|
 _______                            _ _
(_______)            _             | | |
 _       ___  ____ _| |_  ____ ___ | | | _____  ____
| |     / _ \|  _ (_   _)/ ___) _ \| | || ___ |/ ___)
| |____| |_| | | | || |_| |  | |_| | | || ____| |
 \______)___/|_| |_| \__)_|   \___/ \_)_)_____)_|

@module index
 */

(function() {
  define(["interface"], function(__interface__) {
    var IndexWpHeaderController;
    IndexWpHeaderController = function($scope) {
      return $scope.module = {
        wpheader: true
      };
    };
    return ["$scope", IndexWpHeaderController];
  });

}).call(this);

//# sourceMappingURL=IndexWpHeaderController.js.map
