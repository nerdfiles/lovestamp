
/*
 * fileOverview

                            _                       ______ _
                           | |                 _   / _____) |                  _
     ____  _____  ____ ____| |__  _____ ____ _| |_( (____ | |__  _____ _____ _| |_
    |    \| ___ |/ ___) ___)  _ \(____ |  _ (_   _)\____ \|  _ \| ___ | ___ (_   _)
    | | | | ____| |  ( (___| | | / ___ | | | || |_ _____) ) | | | ____| ____| | |_
    |_|_|_|_____)_|   \____)_| |_\_____|_| |_| \__|______/|_| |_|_____)_____)  \__)

 *# description

Introducing Merchant Bottomsheet.
 */

(function() {
  define(["interface", "lodash", "numeral", "config", "directives/relExternal", "angular-geotranslation", "services/profile", "services/stamp", "services/bank", "services/browser"], function(__interface__, _, numeral, environment) {
    var MerchantPreview, MerchantSheetCtrl;
    MerchantSheetCtrl = function($scope, $mdBottomSheet, $q, profileService, stampService, bankService, $rootScope, $timeout, userRequire, browserService, $window) {

      /*
      Merchant Bottomsheet Controller.
       */
      var merchantSheetData, urlPrefix;
      $scope.unsheet = function() {
        return $mdBottomSheet.hide();
      };
      $scope.openMerchantWebsite = function(_websiteLink, _target, _options) {
        var config;
        config = {
          url: _websiteLink,
          target: "_blank",
          options: []
        };
        browserService.open(config);
      };
      merchantSheetData = $scope.merchantSheetData;
      $scope.dlat = merchantSheetData.address.location.lat;
      $scope.dlon = merchantSheetData.address.location.lng;
      $scope.prefix = merchantSheetData.stampOwner.charAt(0);
      urlPrefix = {
        '@': 'https://twitter.com/',
        'ᶠ': 'https://facebook.com/',
        '✚': 'https://plus.google.com/'
      };
      $scope.socialPrefixActiveUrl = urlPrefix[$scope.prefix];
      $scope.openMerchantLink = function(_target, _options) {
        var config, geoData;
        geoData = $rootScope.geolocationAvailable;
        $scope.locationObject = {};
        $scope.locationObject.address = {};
        $scope.locationObject.address.location = geoData.coords;
        $scope.slat = $scope.locationObject.address.location.latitude;
        $scope.slon = $scope.locationObject.address.location.longitude;
        if ($window.is_ios || (navigator.platform.indexOf("iPhone") !== -1) || (navigator.platform.indexOf("iPod") !== -1) || (navigator.platform.indexOf("iPad") !== -1)) {
          $scope.merchantLink = "maps:" + "//maps.google.com/maps?saddr=" + $scope.slat + "," + $scope.slon + "&daddr=" + $scope.dlat + "," + $scope.dlon;
        } else {
          $scope.merchantLink = "http:" + "//maps.google.com/maps?saddr=" + $scope.slat + "," + $scope.slon + "&daddr=" + $scope.dlat + "," + $scope.dlon;
        }
        config = {
          url: $scope.merchantLink,
          target: "_blank",
          options: []
        };
        browserService.open(config);
      };
      stampService.getMerchantBitsTipped(merchantSheetData).then(function(tippedObjectDescription) {
        var _tippedObject;
        $scope.merchant = {};
        _tippedObject = {
          bitsTipped: tippedObjectDescription ? numeral(tippedObjectDescription).format(bankService.currencyFormat) : 0.00
        };
        _.extend(merchantSheetData, _tippedObject);
        profileService.getMerchantPictureInView(merchantSheetData).then(function(imageDataRef) {
          var merchantHandle, _imageData;
          _imageData = {
            generatedMerchantPictureInView: imageDataRef
          };
          _.extend(merchantSheetData, _imageData);
          merchantHandle = merchantSheetData.stampOwner;
          profileService.getMerchantStatus(merchantHandle).$loaded().then(function(merchantData) {
            $scope.merchant.merchantProfile = merchantData.profile;
            $scope.merchant.merchantOverview = merchantData.profile.overview;
            _.extend($scope.merchant, merchantSheetData);
            return $timeout(function() {
              return $rootScope.$broadcast('imageData:loaded', _imageData);
            }, 750);
          });
        });
      });
    };
    MerchantPreview = function($sce, $rootScope, $timeout) {

      /*
      Merchant Preview Directive.
       */
      var linker, _t;
      _t = window.is_device ? './scripts/' : environment.baseTemplateUrl;
      return {
        restrict: 'A',
        templateUrl: _t + 'modules/index/partials/merchant-preview.html',
        scope: {
          merchant: '=merchantConstruct'
        },
        link: linker = function($scope, element, $attr) {
          $rootScope.$on('imageData:loaded', function(event, imageData) {
            return $timeout(function() {
              return $scope.$apply(function() {
                var $element, cachedStyle, i, updatedCachedStyle, __i, _i;
                $element = angular.element(element.find('div'));
                $element.attr('itemprop', 'image');
                _i = '';
                i = imageData.generatedMerchantPictureInView || _i;
                __i = $sce.trustAsResourceUrl(i);
                cachedStyle = $element.attr('style');
                updatedCachedStyle = ['background-image: url(', __i, ')'].join('');
                return $element.attr('style', updatedCachedStyle);
              });
            }, 0);
          });
        }
      };
    };
    __interface__.controller("MerchantSheetCtrl", ["$scope", "$mdBottomSheet", "$q", "profile.service", "stamp.service", "bank.service", "$rootScope", "$timeout", "user.require", "browser.service", "$window", MerchantSheetCtrl]).directive('merchantPreview', ['$sce', '$rootScope', '$timeout', MerchantPreview]);
  });

}).call(this);

//# sourceMappingURL=merchantSheet.js.map
