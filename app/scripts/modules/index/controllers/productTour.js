
/**
Product Tour Controller
 */

(function() {
  define(["interface", "jquery", "services/authentication", "services/profile"], function(__interface__, $) {
    __interface__.controller("ProductTourCtrl", [
      "$scope", "$mdBottomSheet", "$q", "$rootScope", "$document", "$timeout", "leafletData", "user.require", "profile.service", "$compile", function($scope, $mdBottomSheet, $q, $rootScope, $document, $timeout, leafletData, userRequire, profileService, $compile) {
        var contentTimeout, index, lastIndex, _userRequire;
        contentTimeout = null;
        $scope.productTourLoaded = true;
        _userRequire = userRequire();
        $scope.$on('$destroy', function() {
          if (contentTimeout) {
            return $timeout.cancel(contentTimeout);
          }
        });
        _userRequire.then(function(authData) {
          var bg, currentProfile, e, facebookBg, googleBg, twitterBg;
          currentProfile = $scope.currentProfile = profileService.currentProfile = authData[authData.provider].cachedUserProfile;
          if (currentProfile) {
            twitterBg = profileService.currentProfile.profile_image_url;
            facebookBg = profileService.currentProfile.picture && profileService.currentProfile.picture.data;
            googleBg = profileService.currentProfile.picture && !profileService.currentProfile.picture.data;
            if (twitterBg) {
              bg = currentProfile.profile_image_url.replace('_normal', '');
            }
            if (facebookBg) {
              bg = currentProfile.picture.data.url;
            }
            if (googleBg) {

              /*
                  google_spec =
                    family_name : "Hah"
                    gender      : "other"
                    given_name  : "Ɐha"
                    id          : "100541351182813566938"
                    link        : "https://plus.google.com/100541351182813566938"
                    locale      : "en"
                    name        : "Ɐha Hah (nerdfiles)"
                    picture     : "https://lh5.googleusercontent.com/-rJqQ0GxcdY8/AAAAAAAAAAI/AAAAAAAAD6o/PcGS4XpysFo/photo.jpg"
               */
              bg = currentProfile.picture;
            }
            try {
              bg = bg;
            } catch (_error) {
              e = _error;
              console.log(e);
            }
            $scope.productTourInitilized = false;
          }
          return contentTimeout = $timeout(function() {
            var $contentProgressBar, a, html;
            $contentProgressBar = $('.content--progress-bar');
            html = "<span ng-show='!productTourInitialized'><img class='content--progress-bar-image' src='" + bg + "' /></span>";
            a = $compile(html)($scope);
            return $contentProgressBar.after(a);
          }, 0);
        });
        $scope.items = [
          {
            title: "Progress Bar",
            contentClass: "progress-bar",
            name: ["The progress bar shows your rewards activity. Click your picture to access your settings."],
            icon: "hangout"
          }, {
            title: "Get Stamped",
            contentClass: "collect",
            name: ["Click and show your device for stamping."],
            icon: "mail"
          }, {
            title: "Activity History",
            contentClass: "red",
            name: ["Trace your activity history on the map."],
            icon: "message"
          }, {
            title: "Merchant",
            contentClass: "blue",
            name: ["Click map icon for merchant details."],
            icon: "copy"
          }, {
            title: "Intensity Circles",
            contentClass: "yellow",
            name: ["Red gets darker the more a merchant stamps, blue the more your friends get stamped, and yellow the more you get stamped."],
            icon: "facebook"
          }, {
            title: "Reward Transfer",
            contentClass: "mobile",
            name: ["Drag and drop the bag to use your bits somewhere else (we support ChangeTip for now)."],
            icon: "twitter"
          }
        ];
        index = 0;
        $scope.productTourPageMessage = $scope.items[index].name;
        $scope.productTourPageMessageTitle = $scope.items[index].title;
        $scope.contentClass = $scope.items[index].contentClass;
        lastIndex = 0;

        /*
        @namespace productTourNext
        @return {undefined}
         */
        $scope.productTourNext = function() {
          var def;
          $scope.contentClass = "";
          $scope.productTourInitialized = true;
          def = $q.defer();
          if ($scope.items[index + 1]) {
            index++;
            def.resolve(index);
          } else {
            def.reject(index);
          }
          return def.promise.then(function() {
            var _index;
            _index = (index + 1) > ($scope.items.length - 1) ? $scope.items.length - 1 : index + 1;
            if (_index !== lastIndex) {
              $scope.icon = $rootScope.icon = $scope.items[index].icon;
              $scope.contentClass = $scope.items[index].contentClass;
              $scope.productTourPageMessage = $scope.items[index].name;
              $scope.productTourPageMessageTitle = $scope.items[index].title;
              lastIndex = index;
            } else {
              $scope.contentClass = "activity-history";
              angular.element($document[0].body).removeClass('pt--google');
              $mdBottomSheet.hide();
              $scope.productTourLoaded = true;
            }
            if ($scope.productTourLoaded) {
              return leafletData.getMap('index').then(function(map) {
                return map.invalidateSize(false);
              });
            }
          });
        };

        /*
        @namespace productTourPrevious
        @return {undefined}
         */
        $scope.productTourPrevious = function() {
          var def;
          $scope.contentClass = "";
          $scope.productTourInitialized = $scope.productTourInitialized === true ? false : true;
          def = $q.defer();
          if ($scope.items[index - 1]) {
            index--;
            def.resolve(index);
          } else {
            def.reject(index);
          }
          def.promise.then(function() {
            var _index;
            _index = (index - 1) < ($scope.items.length - 1) ? $scope.items.length + 1 : index - 1;
            if (_index !== lastIndex) {
              $scope.icon = $rootScope.icon = $scope.items[index].icon;
              $scope.contentClass = $scope.items[index].contentClass;
              $scope.productTourPageMessage = $scope.items[index].name;
              $scope.productTourPageMessageTitle = $scope.items[index].title;
              lastIndex = index;
            } else {
              $scope.contentClass = "activity-history";
              angular.element($document[0].body).removeClass('pt--google');
              $mdBottomSheet.hide();
              $scope.productTourLoaded = true;
            }
            if ($scope.productTourLoaded) {
              return leafletData.getMap('index').then(function(map) {
                return map.invalidateSize(false);
              });
            }
          });
        };

        /*
        @namespace listItemClick
        @return {undefined}
         */
        return $scope.listItemClick = function($index) {
          var clickedItem;
          $scope.counter = $index;
          index = $index;
          $scope.clickedItem = clickedItem = $scope.items[$index];
          $scope.icon = $rootScope.icon = $scope.items[$index].icon;
          $scope.contentClass = $scope.items[$index].contentClass;
          $scope.productTourPageMessage = $scope.items[$index].name;
          $scope.productTourPageMessageTitle = $scope.items[$index].title;
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=productTour.js.map
