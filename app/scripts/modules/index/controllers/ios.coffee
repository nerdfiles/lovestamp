###*
# fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module index/controllers/base  
@depends services/authentication  

## description

Landing page of the application. Primarily targets #android and #ios systems.

###

define [
    'interface'
    'lodash'
    'moment'
    'numeral'
    'config'
    'leaflet-knn'
    'one-color'
    'utils/async'
    'leaflet-markercluster'
    'geojson-tools'
    'utils/firebase'
    'auth/user/roll-up'
    'auth/access/conductor'
    'ProductTourCtrl'
    'index/MerchantSheetCtrl'
    'directives/preloader'
    'index/directives/profile-orb'
    'index/directives/merchant-sheet'
    'index/directives/set-nav-item'
    'directives/notification'
    'directives/loader'
    'directives/version'
    'directives/relExternal' # For outlinking merchant tooltips.
    'services/stamp'
    'services/bank'
    'services/map'
    'services/crypto'
    'services/browser'
    'angular-geotranslation'
    'services/notification'
    'directives/title'
    'services/profile'
    'jquery.debounce'
    #'components/push/directive'
    'directives/title'
], (__interface__, _, moment, numeral, environment, leafletKnn, color, async) ->

    currentPositionMarker = {}
    deviceGeoSettings = {}
    zoomSetting = if window.is_ios then 17 else 17
    maxZoomSetting = 18
    personalMarker = null
    #mapEye = null

    toggleLeafletLabel = (leafletData) ->
        ###
        @ngdoc directive  
        @name interface.directive:toggleLeafletLabel  
        @function toggleLeafletLabel  
        @description  

        A toggle for icon badges rendered above map markers of stamps of merchants.

        ###

        _toggleLeafletLabel = ($scope, element) ->
            ###
            @ngdoc directiveLink
            @inner

            Element checking for leaflet labels, and toggling.

            ###
            leafletData.getMap('index').then () ->

                $leafletLabelStart = environment.document.querySelectorAll '.leaflet-label'

                $scope.$on 'markers:available', () ->

                    $leafletLabelStart = environment.document.querySelectorAll '.leaflet-label'

                    groupsLabel = _.groupBy $leafletLabelStart, (_label) ->
                        _label.className

                    #flattenedGroups = _.extend.apply(_, groupsLabel)

                    _.forEach groupsLabel, (flattenedGroups, item) ->
                        ###
                        Collect elements to be labeled.
                        ###

                        a = environment.document.querySelector('.' + item.split(' ')[1])
                        angular.element(a).addClass('first')
                        return

                    $leafletClickable = environment.document.querySelectorAll 'img.leaflet-marker-icon'

                    _.forEach $leafletClickable, ($element) ->
                        ###
                        Set labels under initial conditions.
                        ###

                        angular.element($element).on 'dblclick', () ->

                            $leafletLabel = environment.document.querySelectorAll '.leaflet-label'

                            _.forEach $leafletLabel, ($elementLabel) ->
                                #angular.element($elementLabel).addClass 'leaflet-label--masked'
                                angular.element($elementLabel).addClass 'leaflet-label--hidden'
                                return
                            return

                        return

                    angular.element(element).on 'dblclick', () ->
                        ###
                        Remove labels under interaction conditions.
                        ###

                        $leafletLabel = environment.document.querySelectorAll '.leaflet-label'

                        _.forEach $leafletLabel, ($elementLabel) ->
                            angular.element($elementLabel).removeClass 'leaflet-label--hidden'
                            return
                        return

                    return
                return
            return

        return {
              restrict: 'A'
              scope: false
              link: _toggleLeafletLabel
          }

    # Implement toggleleafletlabel.
    __interface__.directive('toggleLeafletLabel', [
        'leafletData'
        toggleLeafletLabel
    ])


    clickRun = () ->
      restrict: 'A'
      scope: false
      link: ($scope, element) ->

        leafletLabel$ = document.querySelectorAll '.leaflet-label'
        leafletLetLabel = element.find('.leaflet-label')
        leafletLabel$ = angular.element(leafletLabel$)

        $scope.$on 'hideControlLayers', (event, controlObject) ->

          mapZoomConfig = controlObject
          leafletControlLayers = angular.element(element).find('.leaflet-control-layers input')

          _.forEach leafletControlLayers, (a) ->
            if angular.element(a).prop('checked') != controlObject
              angular.element(a).trigger('click')

        $scope.$on 'markers:preview', (event, $schema) ->

          leafletLabel$ = document.querySelectorAll '.leaflet-label'
          leafletLetLabel = element.find('.leaflet-label')
          leafletLabel$ = angular.element(leafletLabel$)

          leafletLetLabel.addClass 'disable'
          leafletLabel$.addClass 'disable'

        #$scope.$on 'markers:preview:available', (event, $schema) ->
          #return

          #leafletLabel$ = document.querySelectorAll '.leaflet-label'
          #leafletLetLabel = element.find('.leaflet-label')
          #leafletLabel$ = angular.element(leafletLabel$)

          #leafletLetLabel.removeClass 'disable'
          #leafletLetLabel.addClass 'enable'

          #leafletLabel$.addClass 'enable'
          #leafletLabel$.remove 'disable'

    __interface__.directive('clickRun', [
      clickRun
    ])


    IndexIosController = ($scope, $timeout, $rootScope, geolocation, leafletData, $window, $mdBottomSheet, firebaseUtils, userRequire, $location, $q, stampService, bankService, geoJsonUtils, $$geotranslate, notificationService, $stateParams, localStorageService, mapService, centerService, cryptoService, leafletExtended, $http, profileService, browserService, geolocationAvailable) ->

        # Prefabulations.
        oms = null
        $scope.showMap = true
        $scope.noReferrer = true
        $scope.alert = ""
        $scope.coords = {}
        $scope.merchantLinks = []
        $scope.cancelSheetMap = {}
        $scope.personalIntensityFound = false
        $scope.map = null
        $scope.page = {}
        $scope.page.cache = {}
        $scope.page.cache.geolocation = null
        $scope.mapHash = {}
        $scope.windowTitle = 'Home'

        ttt = null
        tttt = null
        ttttt = null
        tttttt = null
        ttttttt = null
        tttttttt = null
        ttttttttt = null

        $rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) ->
          if toState.name == 'home' or toState.name == 'fan-view' or toState.name == 'home-show' or toState.name == 'home-show-tour' or toState.name == 'home-show-zoom'
            $window.dispatchEvent(new Event('resize'))
        )

        noMapTimeout = $timeout ->
          element = angular.element(document.querySelector('body'))
          element.removeClass('no-map')
        , 500

        $scope.noMap = false

        $scope.$on('$destroy', () ->
          leafletData.unresolveMap('index')
        )

        _getMap = leafletData.getMap('index')

        LeafIcon = L.Icon.extend(options:
            iconSize   : [
                48
                48
            ]
            iconAnchor : [
                24
                48
            ]
        )

        PersonalLeafIcon = L.Icon.extend(
            options:
                iconSize:   [
                    16
                    16
                ]
                iconAnchor: [
                    16/2
                    16/2
                ]
        )

        # frex: Wednesday
        _now = moment().format('dddd')
        now = _now.toLowerCase()

        fullnow = moment().format('MM/DD/YYYY HH:mm')
        datePrefix = moment().format('MM/DD/YYYY')

        #timenow = _timenow = moment().format('HH:mm')

        merchantMarkerInactive = if $window.is_device then '../www/images/map-marker-inactive.png' else '/assets/images/map-marker-inactive.png'
        merchantMarkerActive = if $window.is_device then '../www/images/map-marker.png' else '/assets/images/map-marker.png'

        closeProductTour = null
        noMapTimeout = null

        timecard = (stampData) ->
            ###
            Every stamp needs a timecard if it's on an active map.
            ###

            def = $q.defer()

            if stampData.days and stampData.hours

                activeDays = _.filter(_.map(stampData.days, (val, dayName) ->
                    if val
                        return dayName
                ), (day) ->
                    day
                )

                currentlyActiveDay = _.find activeDays, (day) -> day is now

                if currentlyActiveDay is undefined or currentlyActiveDay is false

                    markerOption = new LeafIcon(
                        iconUrl: merchantMarkerInactive
                    )

                else

                    if stampData.hours and ! stampData.hours.alwaysOpen
                        startTime = stampData.hours.start
                        endTime = stampData.hours.end
                        _startTime = startTime.split(':')
                        _endTime = endTime.split(':')

                        st = parseInt(_startTime[0], 10)
                        et = parseInt(_endTime[0], 10)

                        __startTime = if st < 12 then (st + 12) else st
                        __endTime = if et < 12 then (et + 12) else et

                        ___startTime = [
                          __startTime
                          _startTime[1]
                        ].join ':'

                        ___endTime = [
                          __endTime
                          _endTime[1]
                        ].join ':'

                        markerOption = new LeafIcon(
                            iconUrl: merchantMarkerActive
                        )

                        # moment(moment().format('YYYY-D-MM HH:mm')).isBetween('2015-17-05 12:00', '2015-17-05 23:00')
                        if startTime and endTime and moment(fullnow).isBetween((datePrefix + ' ' + ___startTime), (datePrefix + ' ' + ___endTime))

                            markerOption = new LeafIcon(
                                iconUrl: merchantMarkerActive
                            )

                        else

                            markerOption = new LeafIcon(
                                iconUrl: merchantMarkerInactive
                            )

                    else

                        # Marker is active and is always open
                        markerOption = new LeafIcon(
                            iconUrl: merchantMarkerActive
                        )

                def.resolve markerOption

            else

                def.resolve false

            def.promise

        $scope.is_iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false )

        indexMapDefaults =
            defaults:
                tileLayer            : if $window.is_device != true then 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png' else 'http://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png'
                tileLayerOptions:
                    opacity      : 1
                    detectRetina : true
                    reuseTiles   : true
                keyboard            : true
                dragging            : true
                doubleClickZoom     : true
                zoomControl         : false
                zoomAnimation       : if $window.is_android then false else true
                fadeAnimation       : if $window.is_android then false else true
                touchZoom           : true
                markerZoomAnimation : if $window.is_android then false else true
                minZoom             : if $window.is_ios then 11 else 11

        _.extend $scope, indexMapDefaults

        # User auth detail.
        _userRequire = userRequire()

        $scope.productTourSeen = false

        _userRequire.then (authData) ->
          if !authData
            return
          profileService.getUserStatus(authData.handle).$loaded().then (profileData) ->
            $scope.productTourSeen = profileData.profile.productTour.seen

            indexed = false

            $scope.$watch('productTourSeen', (newVal) ->
              if newVal == true and indexed == false and geolocator.data
                $scope.noMap = false
                $scope.setMarker($scope.map, geolocator.data)
                indexed = true
            )

        unitedStatesLocationConstruct =
          latitude: 37.6,
          longitude: -95.665

        if cryptoService.hash(JSON.stringify(geolocationAvailable.geolocationAvailable.coords)) == cryptoService.hash(JSON.stringify(unitedStatesLocationConstruct))
          zoomSetting = 3
          _getLocation = geolocation.getLocation(null, unitedStatesLocationConstruct)
        else
          # Device geolocator detail.
          _getLocation = geolocation.getLocation()

        $scope.loadInBrowser = (websiteLink) ->
          config =
            url     : websiteLink
            target  : "_blank"
            options : []
          browserService.open(config)
          return

        $scope.doneLoading = false

        _getMap.then () ->
          $timeout ->
            $scope.$apply () ->
              $scope.doneLoading = true
          , 0

        $scope.watcherOn = false
        $scope.bgModeIsActive = false
        _watchLocation = geolocation.watchLocation()
        $scope.nearestMerchantCount = 0

        $scope.watcherTimeout = null
        $scope.nearestTimeout = null
        mAppInBackground = false

        document.addEventListener('pause', () ->
          mAppInBackground = true
        )
        document.addEventListener('resume', () ->
          mAppInBackground = false
        )

        if typeof cordova != 'undefined'
          #$scope.bgModeIsActive = cordova.plugins.backgroundMode.isActive()
          $scope.bgModeIsActive = mAppInBackground

        _basicWatcher = (data) ->

          _coords =
            lat: data.coords.latitude
            lng: data.coords.longitude

          lat = _coords.lat
          lng = _coords.lng

          if lat and lng and mAppInBackground == false

            # @example {lat: 50.5006, lng: 4.48649, zoom: 7, autoDiscover: false}
            centerConfigConstruct =
              autoDiscover : false
              zoom         : maxZoomSetting

            _.extend centerConfigConstruct, _coords
            $scope.centerConfigConstruct = centerConfigConstruct
            #centerService.save(centerConfigConstruct)

            if personalMarker
              personalMarker.setLatLng([
                lat
                lng
              ]).update()

          if $scope.watcherOn == false and mAppInBackground == true

            nearestListener = $scope.$watch 'nearestMerchantIndex.length', (nearestMerchantCount) ->
              $scope.nearestMerchantCount = nearestMerchantCount

              mlib =
                message1 : "LoveStamper nearby. Go get some love!"
                message2 : "LoveStamper nearby. Get stamped!"
                message3 : "LoveStamper nearby. Choose the red pill!"
                message4 : "LoveStamper nearby. Get impressed!"
                message5 : "LoveStamper nearby. Don't get Trumped, get LoveStamped!"
                message6 : "LoveStamper nearby. Get stamped and make America great again!"
                message7 : "LoveStamper nearby. Follow the blue line Neo!"
                message8 : 'LoveStamper nearby. Be impressed!'
                message9 : 'LoveStamper nearby. Get stamped!'
                message10 : 'LoveStamper nearby. Go get some love!'
                message11 : 'LoveStamper nearby. Get stamped and make America great again!'
                message12 : "LoveStamper nearby. Ain't everybody got time for that!"
                message13 : 'LoveStamper nearby. Yo mama got stamped already!'

              messageList = _.toArray(mlib)

              if parseInt($scope.nearestMerchantCount, 10) > 0
                _userRequire.then (authData) ->
                  if !authData
                    return

                  profileService.getUserStatus(authData.handle).$loaded().then (profileData) ->
                    if profileData.profile.pushNotifications.status != false
                      try
                        plugin.notification.local.add({
                          message : messageList.randomPrint()
                          sound   : null
                        })
                      catch e
                        notificationService.remotePost JSON.stringify(e), 'plugin.notification.local.add'

              else
                $scope.watcherOn = false
                # Once watch finds 0 merchants, ensure that it fires again 
                # but cancel the previous nearestListener.
                nearestListener()
                return

              # Do not execute nearestListener until fan reactivates the app 
              # (implied) since the watch is still running in background.
              $scope.watcherOn = true

        $scope.$on '$destroy', () ->
          leafletData.unresolveMap('index')

        $scope.changedLatLng = null
        $scope.bgGeoStarted = false

        nearestListener = null
        bgGeo = null

        # Devices-only wrapper
        document.addEventListener 'deviceready', (->

          plugin.notification.local.registerPermission((granted) ->
            if !granted
              cordova.plugins.backgroundMode.disable()
              if bgGeo
                bgGeo.stop()
          )

          _userRequire.then (authData) ->
            if !authData
              return

            localStorageService.set('provider', authData.provider)

            _getLocation.then () ->

              profileService.getUserStatus(authData.handle).$loaded().then (profileData) ->

                if profileData.profile.pushNotifications.status == true
                  cordova.plugins.backgroundMode.enable()

                if profileData.profile.pushNotifications.status == false
                  cordova.plugins.backgroundMode.disable()
                  if bgGeo
                    bgGeo.stop()
                  return

                plugin.notification.local.hasPermission((granted) ->

                  if !granted
                    cordova.plugins.backgroundMode.disable()
                    return

                  bgGeo = $window.plugins.backgroundGeoLocation

                  postLocationCallback = (response) ->
                    bgGeo.finish()

                  callbackFn = (location) ->

                    $rootScope.$on 'geolocation.position.changed', (event, geolocationConstruct) ->
                      $scope.changedLatLng = geolocationConstruct
                      return

                    postLocationCallback.call this
                    return

                  failureFn = () ->
                    notificationService.displayError 'Failed to update client location.'
                    return

                  cordova.plugins.backgroundMode.ondeactivate = ->

                    if nearestListener
                      nearestListener()

                    if $scope.bgGeoStarted
                      bgGeo.stop()
                      $scope.bgGeoStarted = false

                  cordova.plugins.backgroundMode.onactivate = ->
                    console.log 'Watching. . .'

                    bgGeo.configure(callbackFn, failureFn, {
                        desiredAccuracy  : 10,
                        stationaryRadius : 20,
                        distanceFilter   : 30,
                        activityType     : 'Fitness'
                        debug            : false
                    })

                    bgGeo.start()
                    bgGeo.changePace(true)
                    $scope.bgGeoStarted = true

                    return

                  return
                )
        ), false

        $scope.oldGeolocationConstruct = null
        c = null
        b = null

        $rootScope.$on 'geolocation.position.changed', (event, geolocationConstruct) ->
          #c = cryptoService.hash(JSON.stringify(geolocationConstruct.coords))
          #if $scope.oldGeolocationConstruct and $scope.oldGeolocationConstruct.coords
            #b = cryptoService.hash(JSON.stringify($scope.oldGeolocationConstruct.coords))

          #if c == b
            #return

          if mAppInBackground == true
            return

          if mAppInBackground == false
            if !$scope._nearestTimeout

              $scope._nearestTimeout = $timeout ->
                $scope.showNearestMerchant()
                $timeout.cancel $scope._nearestTimeout
                return
              , 15000

            if !$scope._watcherTimeout

              $scope.watcherTimeout = $timeout ->
                _basicWatcher(geolocationConstruct)
                $timeout.cancel $scope._watcherTimeout
                return
              , 30000

          $scope.oldGeolocationConstruct = geolocationConstruct
          return

        $scope.$on '$locationChangeStart', () ->
          if $scope._watcherTimeout
            $timeout.cancel $scope.watcherTimeout
            #delete $scope.watcherTimeout
            $scope.watcherTimeout = null
          if $scope._nearestTimeout
            $timeout.cancel $scope._nearestTimeout
            #delete $scope._nearestTimeout
            $scope._nearestTimeout = null
          if _watchLocation
            geolocation.clearWatch(_watchLocation)

        $scope.linksActivitySet = false

        $scope.showLinksActivityTracking = () ->
          ###
          @namespace showLinksActivityTracking
          @description

          A use case for Falconry's Scout service.

          ###

          linksActivityConstruct = $q.all [
            merchantMarket
            _getMap
          ]

          linksActivityConstruct.then (linkActivityTrackingConstruct) ->

            linkActivityTracking = linkActivityTrackingConstruct[0]
            map = linkActivityTrackingConstruct[1]

            if $scope.linksActivitySet == true
              new mapService.Falconry.Scout(linkActivityTracking, map)

            if $scope.linksActivitySet == false
              try
                new mapService.Falconry.Scout(linkActivityTracking, map)
                $scope.linksActivitySet = true
              catch e
                console.dir e
            else
              $scope.linksActivitySet = false
            return

          return


        ###
        @namespace clearMap
        @description

        A use case for Falconry's clearing service.

        @return {undefined}
        ###
        $scope.clearMap = mapService.Falconry.clearMap


        ###
        @namespace acl
        @description

        A use case for Falconry's predictive "ringer" service.

        ###
        $scope.mapService = {}
        $scope.mapService.acl = new mapService.Falconry.Ringer()
        AsyncColoringLibrary = $scope.mapService.acl

        $scope.indexConstruct = $q.all [
          _userRequire
          _getLocation
          _getMap
        ]

        geolocator = {}

        tt = null
        $scope.indexConstruct.then (data) ->
            ###
            # indexConstruct

            All aboard, async Promises.

            @param {Promise:object} data
            ###

            map = $scope.map = data[2]

            homescreenAddedCheck = localStorageService.get 'homescreenAdded'

            if $window.is_device != true
              if homescreenAddedCheck != 'true'

                notificationService.displayHomeScreenAdd('<div class="homescreenAdded--inner"><span class="ath-action-icon">&nbsp;</span> Add us to your home screen!</div>', {
                  hideDelay: 5000
                })

                localStorageService.set 'homescreenAdded', true

            #map.on('zoomanim', $.debounce(map._onZoomTransitionEnd, 1))
            #map.on('zoomanim', $.debounce(map._onCustomTransitionEnd, 1))

            authData = data[0]
            return    if not authData

            $scope.doneLoading = true
            geoData = geolocator.data = data[1]

            #$timeout ->
              #$scope.$apply () ->
                #$scope.noMap = false
                ##map.invalidateSize true
            #, 0

            #$scope.$watch 'productTourLoaded', (newValue, oldValue) ->
              #if ! newValue
                #tt = $timeout ->
                  #new leafletExtended.OSMBuildings(map).load()
                  #return
                #, 500

            $scope.setMarker(map, geoData)

            $scope.merchantsNearby = []

            #Pusher.subscribe('near_here', 'merchant_stamp_nearby', (merchantNearby) ->
                #for i in $scope.merchantsNearby.length
                    #if $scope.merchantsNearby[i].id is merchantNearby.id
                        #$scope.merchantsNearby[i] = merchantNearby
                        #break
            #)

            $scope.user = user = authData
            $rootScope.userLanded = true
            $rootScope.handle = user.handle
            return

        $scope.setMarker = (map, data) ->
            ###
            # setMarker

            @param {object} map
            @param {object} data Geodata (not GeoJson).
            ###

            if ! data or ! map
                return

            _.extend deviceGeoSettings,
                lat: data.coords.latitude
                lng: data.coords.longitude
                zoom: if $stateParams.zoomSetting then $stateParams.zoomSetting.int() else zoomSetting

            personalIconRef = if $window.is_device then '../www/images/map-marker--personal.png' else '/assets/images/map-marker--personal.png'
            personalMapMarker = new PersonalLeafIcon(
                iconUrl: personalIconRef
            )

            personalArea = L.circle([
                data.coords.latitude
                data.coords.longitude
            ], 152.4, { # meters.feet 500
                color: '#BDD4FE'
                fillColor: '#BDD4FE'
            })

            map.addLayer personalArea

            personalMarker = L.marker(
                [
                    data.coords.latitude,
                    data.coords.longitude
                ],
                    icon: personalMapMarker
                    zIndexOffset: 100
            )

            map.addLayer personalMarker
            #map.invalidateSize false

            return


        #signalMerchantNearby = (stamp) ->
            ####
            ## signalMerchantNearby
            ####

            #def = $q.defer()
            #$http.post('near-here/channel', stamp).success (stampStatus) ->
                #def.resolve stampStatus.isNearby
            #def.promise

        loadMerchantsByStamp = () ->
            ###
            # loadMerchantsByStamp

            Asynchronously load all available merchants with stamps into map view.

            ###

            def = $q.defer()
            linksActivityTracking = []

            mapAndUserLoaded = $q.all [
                _userRequire
                _getMap
                _getLocation
            ]

            mapAndUserLoaded.then (data) ->

                stampMarkers = new L.MarkerClusterGroup({
                  spiderfyOnMaxZoom          : false
                  showCoverageOnHover        : false
                  zoomToBoundsOnClick        : true
                  removeOutsideVisibleBounds : true
                  disableClusteringAtZoom    : 15
                })

                user = data[0]
                map = data[1]
                locationData = data[2]
                return  if not user
                return  if not map
                return  if not locationData

                $scope.merchants = stampService.getAllStamps()
                oms = new leafletExtended.OverlappingMarkerSpiderfier map

                $scope.merchants.$loaded().then (merchantData) ->

                    # Load Intensities Data
                    merchantsByIntensity = bankService.getIntensityMerchant(merchantData)

                    merchantsByIntensity.then (d) ->

                        _merchantData = d

                        totalMerchantSystem = angular.copy _merchantData

                        f = _.filter totalMerchantSystem, (m) ->
                          m.stamps and m.stamps.local

                        _availableStamps = []

                        merchantMarketData = _merchantData

                        _.forEach totalMerchantSystem, (merchantStamper) ->
                          if merchantStamper.stamps and merchantStamper.stamps.local

                            _.forEach merchantStamper.stamps.local, (localStamp, keyName) ->
                                _.extend localStamp,
                                    $id                         : keyName
                                    stampOwner                  : merchantStamper.$id

                                _availableStamps.push localStamp

                        flattenedAvailableStamps = _.filter(_availableStamps, (a) -> a.address)

                        $scope._flattenedAvailableStamps = flattenedAvailableStamps

                        # Other stamps hashified
                        _.forEach $scope._flattenedAvailableStamps, (_r) ->
                            try
                              loc = _.toArray(_r.address.location)
                              _r.hashed = cryptoService.hash(JSON.stringify(loc.join(',')))
                            catch e
                              console.log e
                            return

                        # For rendering to map
                        uniqueMerchant = _.map totalMerchantSystem, (_merchant) ->
                            _.filter _merchant.stamps.local, (localStampStore) ->
                                localStampStore.address

                        _flattenedMerchantSystem = _.extend.apply(_, uniqueMerchant)

                        flattenedMerchantSystem = _.map _flattenedMerchantSystem, (__merchantStamp) ->
                            if __merchantStamp.address
                                return __merchantStamp.address.location

                        # Second tangent
                        _.forEach(_merchantData, (merchant) ->

                            if ! merchant.stamps
                                return

                            if merchant.stamps and ! merchant.stamps.local
                                return

                            quotaColor = merchant.merchantIntensityQuota.int() / 100

                            merchantStampImpressionRed = color(AsyncColoringLibrary.defaultColorSetting.startRed).
                                saturation(quotaColor).
                                hex()

                            _.extend merchant,
                                merchantIntensityQuota      : merchant.merchantIntensityQuota
                                merchantIntensityQuotaColor : merchantStampImpressionRed

                            $scope.merchantLinks.push merchant

                            updatedMerchantSystem = _.map merchant.stamps.local, (localStamps, keyName) ->

                                _.extend localStamps,
                                    $id                         : keyName
                                    merchantIntensityQuota      : merchant.merchantIntensityQuota
                                    merchantIntensityQuotaColor : merchantStampImpressionRed
                                    stampOwner                  : merchant.$id

                            try
                                if user.handle
                                    _b = bankService.getIntensityFriendAndPersonal(user.handle, updatedMerchantSystem, totalMerchantSystem)
                            catch e
                                console.log 'Unable to load friend and personal aspects.'
                                return

                            _b.then (_stampSystem) ->

                                ((stampSystem) ->

                                    tttt = $timeout(() ->

                                        # @note This should be codified in our services.
                                        _merchantSystem = _.filter stampSystem, (_stamp) ->
                                            _stamp.address and _stamp.active is true

                                        async.map(_merchantSystem, AsyncColoringLibrary.color.bind(AsyncColoringLibrary), (error, _merchantSystem) ->

                                            highestYellow = color(AsyncColoringLibrary.defaultColorSetting.startYellow).
                                                saturation(bankService.getHighestYellow() / 0.15).
                                                hex()
                                            highestBlue = color(AsyncColoringLibrary.defaultColorSetting.startBlue).
                                                saturation(bankService.getHighestBlue() / 0.15).
                                                hex()
                                            highestRed = color(AsyncColoringLibrary.defaultColorSetting.startRed).
                                                saturation(bankService.getHighestRed() / 0.15).
                                                hex()

                                            try

                                              # Each merchant system is a list of stamps per merchant.
                                              _gms = _.uniq _merchantSystem, (_ms) ->
                                                loc = _.toArray(_ms.address.location)
                                                cryptoService.hash(JSON.stringify(loc.join(',')))
                                            catch e
                                              console.log e


                                            _.forEach(_gms, (_stampData) ->

                                                personalCircleIntensity = null
                                                friendCircleIntensity = null
                                                merchantCircleIntensity = null
                                                t = null

                                                stampData = _stampData

                                                if stampData and stampData.address
                                                    address = stampData.address
                                                else return

                                                if address and ! address.location

                                                    try

                                                        stampService.updateStampAddress(stampData, stampData.username).then (u) ->
                                                            console.log 'Updating Stamp Address. . .'
                                                            console.log u

                                                    catch e

                                                        notificationService.displayError 'Failed to update stamp address.'

                                                if address and address.location
                                                    merchantGeolocation = {}
                                                    _.extend merchantGeolocation, address.location
                                                    merchantLocationConstruct = [
                                                        merchantGeolocation.lat
                                                        merchantGeolocation.lng
                                                    ]
                                                    mapHashId = cryptoService.hash(merchantLocationConstruct.join(','))
                                                    m = L.marker(
                                                        merchantLocationConstruct,
                                                        zIndexOffset: 200
                                                    )

                                                    try
                                                      loc = _.toArray(stampData.address.location)
                                                      m.colls = cryptoService.hash(JSON.stringify(loc.join(',')))
                                                    catch e
                                                      console.log e

                                                    merchantObject =
                                                        merchant: stampData
                                                        location: m.getLatLng()

                                                    linksActivityTracking.push merchantObject

                                                    # Adjusts Intensity Circle to sit underneath the marker.
                                                    _merchantLocationConstruct = _.map merchantLocationConstruct, (coord, index) -> coord

                                                    #signalMerchantNearby(stampData)

                                                    # Increases in color and size the more the
                                                    # customer gets stamped by the merchant.
                                                    personalCircleIntensity = L.circle _merchantLocationConstruct, 40,
                                                        stroke      : true
                                                        weight      : 2
                                                        color       : highestYellow
                                                        fill        : true
                                                        #fillColor   : stampData.personalIntensityQuotaColor
                                                        fillColor   : if _.isNaN(stampData.personalIntensityQuota) then stampData.personalIntensityInactiveColor else stampData.personalIntensityQuotaColor
                                                        fillOpacity : 1
                                                        #fillOpacity : stampData.personalActivity
                                                        className   : 'personal-circle'

                                                    # Increases in color and size the more 
                                                    # the customer's friends get stamped by 
                                                    # the merchant.
                                                    friendCircleIntensity = L.circle _merchantLocationConstruct, 30,
                                                        stroke      : true
                                                        weight      : 2
                                                        color       : highestBlue
                                                        fill        : true
                                                        fillColor   : if _.isNaN(stampData.friendIntensityQuota) then stampData.friendIntensityInactiveColor else stampData.friendIntensityQuotaColor
                                                        fillOpacity : 1
                                                        #fillOpacity : stampData.friendActivity
                                                        className   : 'friend-circle'

                                                    # Increases in color and size the more
                                                    # the merchant stamps mobile device.
                                                    merchantCircleIntensity = L.circle _merchantLocationConstruct, 20,
                                                        stroke      : true
                                                        weight      : 2
                                                        color       : highestRed
                                                        fill        : true
                                                        fillColor   : if _.isNaN(stampData.merchantIntensityQuota) then stampData.merchantIntensityInactiveColor else stampData.merchantIntensityQuotaColor
                                                        fillOpacity : 1
                                                        className   : 'merchant-circle'

                                                    m.hy = highestYellow
                                                    m.hb = highestBlue
                                                    m.hr = highestRed

                                                    m.pa = stampData.personalActivity
                                                    m.fa = stampData.friendActivity
                                                    m.piq = stampData.personalIntensityQuotaColor
                                                    m.fiq = stampData.friendIntensityQuotaColor
                                                    m.miq = stampData.merchantIntensityQuotaColor

                                                    timecard(stampData).then((stampTime) ->

                                                        layerGroup = null

                                                        # Set marker icon based on time variables.
                                                        m.setIcon stampTime

                                                        $scope.mapHash[mapHashId] = m

                                                        # Attach intensity circles to unique marker.
                                                        m.personalCircleIntensity = personalCircleIntensity
                                                        m.friendCircleIntensity = friendCircleIntensity
                                                        m.merchantCircleIntensity = merchantCircleIntensity

                                                        try
                                                          m.anyDupes = _.filter $scope._flattenedAvailableStamps, (a) ->
                                                              loc = _.toArray(stampData.address.location)
                                                              return a.hashed == cryptoService.hash(JSON.stringify(loc.join(',')))

                                                          m.anyDupesOwner = _.filter $scope._flattenedAvailableStamps, (a) ->
                                                              _loc = _.toArray(stampData.address.location)
                                                              return a.stampOwner == stampData.stampOwner and a.hashed == cryptoService.hash(JSON.stringify(_loc.join(','))) and a.alias isnt stampData.alias
                                                        catch e
                                                          console.log e

                                                        m.hasSimpleDupes = _.size(m.anyDupes) > 1
                                                        m.hasOwnershipDupes = _.size(m.anyDupesOwner) == 0

                                                        oms.addMarker m

                                                        if m.hasSimpleDupes and m.hasOwnershipDupes

                                                            # Bind labels to spiderfied markers.
                                                            m.bindLabel('<span class="fa fa-plus-circle"></span>',
                                                                noHide    : true
                                                                offset    : [8, -37]
                                                                className : 'spiderfied-label--' + m.colls
                                                            )

                                                        else

                                                            # Bind intensity circles to non-spiderfied markers.
                                                            layerGroup = L.layerGroup().
                                                              addLayer(personalCircleIntensity).
                                                              addLayer(friendCircleIntensity).
                                                              addLayer(merchantCircleIntensity).
                                                              addTo(map)

                                                            if ! layerControl
                                                                layerControl = L.control.layers().addTo(map)

                                                            # Add layer controls for click toggling.
                                                            layerControl.addOverlay(layerGroup, "MS")

                                                        _t = null

                                                        cb = (b, c) ->
                                                            $scope.$broadcast 'prestamp:sheet', merchantObject.merchant
                                                            return

                                                        if m.hasSimpleDupes == false and m.hasOwnershipDupes == true
                                                            m.on 'contextmenu touchstart mouseenter mousedown', (_e) ->
                                                                cb(undefined, _e)
                                                                return

                                                        stampMarkers.addLayer m

                                                        mapZoomConfig =
                                                            start : map.getZoom()
                                                            end   : map.getZoom()

                                                        t = null

                                                        oms.addListener('spiderfy', () ->

                                                            $scope.disableBottomsheet = false

                                                            mLabels = document.querySelectorAll('.spiderfied-label--' + m.colls)

                                                            cz = (hide) ->
                                                                [].forEach.call(mLabels, (mLabel) ->
                                                                    a = angular.element(mLabel)
                                                                    if hide
                                                                      a.removeClass 'enable'
                                                                      a.addClass 'leaflet-label--hide-forever'
                                                                    else
                                                                      a.addClass 'enable'
                                                                      a.removeClass 'leaflet-label--hide-forever'
                                                                )

                                                            cz(true)

                                                            z = (_e) ->
                                                                if _e.type == 'contextmenu'
                                                                  timeoutTest = 0
                                                                else
                                                                  timeoutTest = 750
                                                                $scope.bs = $timeout ->
                                                                    if $scope.disableBottomsheet == false
                                                                        cb(undefined, _e)
                                                                    $scope.disableBottomsheet = false
                                                                , timeoutTest
                                                                return

                                                            m.on 'contextmenu touchstart mouseenter mousedown', z

                                                            oms.addListener('unspiderfy', () ->
                                                                cz(false)
                                                                $timeout.cancel $scope.bs
                                                                $scope.disableBottomsheet = true
                                                                m.off 'contextmenu touchstart mouseenter mousedown', z
                                                            )

                                                            $scope.$broadcast 'hideControlLayers', false

                                                        )

                                                        map.on 'zoomstart', (e) ->
                                                            mapZoomConfig.start = map.getZoom()
                                                            return

                                                        map.on('zoomend', (e) ->

                                                            mapZoomConfig.end = map.getZoom()

                                                            diff = mapZoomConfig.start - mapZoomConfig.end

                                                            _mci = 20
                                                            _fci = 30
                                                            _pci = 40

                                                            $scope.$broadcast 'hideControlLayers', false
                                                            try
                                                                if layerGroup
                                                                    map.removeLayer(layerGroup)
                                                            catch e
                                                              console.dir e


                                                            if mapZoomConfig.end == 16

                                                                _mci = 20
                                                                _fci = 30
                                                                _pci = 40

                                                                $scope.$broadcast 'markers:preview:available', { preview: true }


                                                            if mapZoomConfig.end == 17

                                                                _mci = 20
                                                                _fci = 30
                                                                _pci = 40

                                                                $scope.$broadcast 'markers:preview:available', { preview: true }

                                                                if m.hasSimpleDupes == false and m.hasOwnershipDupes == true
                                                                    $scope.$broadcast 'hideControlLayers', true
                                                                    try
                                                                        if layerGroup
                                                                            map.addLayer(layerGroup)
                                                                    catch e
                                                                      console.dir e


                                                            if mapZoomConfig.end == 18

                                                                _mci = 10
                                                                _fci = 15
                                                                _pci = 20

                                                                $scope.$broadcast 'markers:preview:available', { preview: true }

                                                                if m.hasSimpleDupes == false and m.hasOwnershipDupes == true
                                                                    $scope.$broadcast 'hideControlLayers', true
                                                                    try
                                                                        if layerGroup
                                                                            map.addLayer(layerGroup)
                                                                    catch e
                                                                      console.dir e

                                                            merchantCircleIntensity.setRadius(_mci)
                                                            personalCircleIntensity.setRadius(_pci)
                                                            friendCircleIntensity.setRadius(_fci)
                                                            map.invalidateSize()
                                                            return
                                                        )

                                                    )
                                                return
                                            )
                                        )

                                    , 50)

                                )(_stampSystem)
                            return
                        )

                        if stampMarkers
                            map.addLayer stampMarkers
                    return

                def.resolve linksActivityTracking

                map.on 'layeradd', () ->
                  ttttttt = $timeout ->
                    $scope.$apply () ->
                      $scope.$broadcast('markers:available', true)
                      $timeout.cancel ttttttt
                  , 0

                map.on 'focus', () ->
                  $scope.$broadcast('markers:available', true)

                map.on 'zoomend', () ->
                  $scope.$broadcast('markers:available', true)

                  $timeout ->
                    map.invalidateSize(
                      pan             : false,
                      debounceMoveend : true
                    )
                  , 500

                d = _.map linksActivityTracking, (arrayLike) ->

                    L1 = locationData.coords.latitude
                    l1 = locationData.coords.longitude
                    L2 = arrayLike.location.lat
                    l2 = arrayLike.location.lng

                    {
                        merchantObject : arrayLike.merchant
                        distance       : $$geotranslate.haversine(L1, l1, L2, l2)
                    }

                $timeout ->
                  $scope.$apply () ->
                    $scope.stampScreenShow = true
                , 0
                mapService.storedDistancesObject = d

            def.promise

        # Load Merchant Market
        merchantMarket = mapService.merchantMarket = loadMerchantsByStamp()

        $scope.$watch 'merchantLinks.length', (_merchantLinksData) ->
            if _merchantLinksData and _merchantLinksData > 0

                $scope.showNearestMerchant()

                _.forEach $scope.merchantLinks, (merchantLink) ->
                    if merchantLink.stamps and merchantLink.stamps.local
                        _.forEach merchantLink.stamps.local, (personalizedLocalStamp) ->
                            if personalizedLocalStamp.personalIntensityQuota > 0
                                $scope.personalIntensityFound = true
                                return
                            return
                        return
                    return


        $scope.$on '$locationChangeStart', (event, next, current) ->
            $mdBottomSheet.hide()

        calculateDistances = () ->
            ###
            # calculateDistances

            Use haversine method to build a list of distances out of merchant market

            ###

            checkDistances = $q.defer()

            _getLocation.then (data) ->

                merchantMarket.then (merchantGeoData) ->

                    $scope.$broadcast 'markers:preview', { preview: true }

                    _data = _.map merchantGeoData, (arrayLike) ->

                        L1 = data.coords.latitude
                        l1 = data.coords.longitude
                        L2 = arrayLike.location.lat
                        l2 = arrayLike.location.lng

                        $idByAddy = cryptoService.hash([
                          L2
                          l2
                        ].join(','))

                        _m = $scope.mapHash[$idByAddy]
                        if _m and _m.options and _m.options.icon
                          _m.status = if _m.options.icon.options.iconUrl.indexOf('inactive') is -1 then true else false

                        {
                            status: _m.status
                            merchantObject : arrayLike.merchant
                            distance       : $$geotranslate.haversine(L1, l1, L2, l2)
                        }

                    checkDistances.resolve _data

            checkDistances.promise

        $scope.loadStampScreen = () ->
            ###
            # loadStampScreen

            ## note

            Overcomplicated and must clean out scope on hazard of memory issues.

            ###

            handle = undefined

            # Get distance of loaded merchants in miles, check if any are within 
            # half a mile
            calculateDistances().then (merchantList) ->

                #maxDistance = 1.5 # @unit miles
                #maxDistance = 50 # @unit miles
                #maxDistance = 0.094697 # @unit feet.miles
                maxDistance = 500 # feet
                #maxDistance = 264000 # miles.feet; 50 miles

                # TODO Should preserve list.
                _merchantList = _.filter merchantList, (merchant, index) ->
                    m = merchant.distance * 5280 # miles to feet
                    if m < maxDistance
                        return m

                _getLocation.then (data) ->
                    $scope.merchants.$loaded().then (merchantData) ->

                        _localm = []
                        filteredMerchantData = _.filter merchantData, (merchant) ->
                            merchant.stamps and merchant.stamps.local

                        _.forEach filteredMerchantData, (_merchant) ->
                            _.forEach _merchant.stamps.local, (localStamp) ->
                                _localm.push localStamp

                        ___merchantList = _.map _merchantList, (__m) ->
                            __m.merchantObject

                        _filteredLocalM = _.filter _localm, (localm) ->
                            checked = true
                            _.forEach ___merchantList, (__m) ->
                                if localm.$id == __m.$id
                                    checked = false
                            checked

                        __filteredLocalM = _.filter _filteredLocalM, (m) -> m.address

                        expandedMerchantList = _.map __filteredLocalM, (_Merchant) ->
                            L1 = data.coords.latitude
                            l1 = data.coords.longitude
                            L2 = _Merchant.address.location.lat
                            l2 = _Merchant.address.location.lng

                            stampDistance = $$geotranslate.haversine(L1, l1, L2, l2)

                            _stampDistance = stampDistance * 5280

                            $idByAddy = cryptoService.hash([
                              L2
                              l2
                            ].join(','))

                            _m = $scope.mapHash[$idByAddy]
                            if _m and _m.options and _m.options.icon
                              _m.status = if _m.options.icon.options.iconUrl.indexOf('inactive') is -1 then true else false

                            if _stampDistance < maxDistance and _m.status is true
                              return {
                                  status: _m.status
                                  distance       : stampDistance
                                  merchantObject : _Merchant
                              }
                            else
                              return undefined

                        _expandedMerchantList = _.filter expandedMerchantList, (stampDeterminedDistance) -> stampDeterminedDistance

                        __merchantList = _merchantList.concat _expandedMerchantList
                        stampService.localValidatorStamps = __merchantList

                        $location.path('/fan/stamp')
                        return
                    return
                return
            return


        $scope.nearestMerchantFound = false
        $scope.nearestMerchantSet = false

        $scope.showNearestMerchant = () ->
            ###
            Show Nearest Merchant from Nearest Merchant Index.
            ###

            nearestMerchantConstruct = $q.all [
                _getLocation
                merchantMarket
            ]

            nearestMerchantConstruct.then (data) ->

                # First tangent
                tttttttt = $timeout ->

                    currentPosition = data[0]
                    a = data[1]

                    if $scope.changedLatLng
                      currentPosition = $scope.changedLatLng
                      #console.log $scope.changedLatLng

                    # @gives [[N, N], [N, N], ...]
                    arrayLikeMerchants = _.map a, (arrayLike) ->
                        [
                            arrayLike.location.lat
                            arrayLike.location.lng
                        ]

                    geoJsonizedMerchants = geoJsonUtils.
                        toGeoJSON(arrayLikeMerchants, 'linestring')

                    _merchantGeoMarket = L.geoJson(geoJsonizedMerchants)

                    $scope.nearestMerchantIndex = leafletKnn(_merchantGeoMarket).
                        nearest L.latLng(currentPosition.coords.latitude, currentPosition.coords.longitude),
                        1

                    if $scope.nearestMerchantIndex and _.size($scope.nearestMerchantIndex) > 0

                        m = _.last($scope.nearestMerchantIndex)

                        L1 = currentPosition.coords.latitude
                        l1 = currentPosition.coords.longitude

                        lat = m.lat
                        lon = m.lon

                        if !lat and !lon and !m and !L1 and !l1
                          $scope.nearestMerchantFound = false
                          return

                        mConstruct = [
                          lat
                          lon
                        ].join(',')

                        nearestDisplayValue = $$geotranslate.haversine(L1, l1, lat, lon)

                        _mConstruct = cryptoService.hash(mConstruct)

                        nearestMerchantMarker = $scope.mapHash[_mConstruct]

                        nearestDisplayText = numeral(nearestDisplayValue * 0.621371).format('0,0.00') # convert to miles

                        nearestMerchantMarker.bindLabel('<span class="~font-effect-outline ~font-effect-3d ~font-effect-3d-float" style="color: #47b7e9; color: #6694c4; color: #ffffff; color: rgba(0, 110, 183, .5); font-weight: 400; margin-left: -1rem; font-size: 1.5rem;">' + nearestDisplayText + ' mi</span>' + '<!--span style="color: #ECA503" class="fa fa-exclamation"></span-->',
                          noHide    : true
                          offset    : [-40, -37]
                          className : 'spiderfied-bullseye--' + nearestMerchantMarker.colls
                        )

                        $scope.nearestMerchantFound = true

                        if (nearestDisplayValue * 0.621371) > .1
                          $scope.nearestMerchantIndex.lenth = 0
                          #$scope.nearestMerchantFound = false

                , 250

        $scope.determineCurrentMerchant = () ->

            determinationConstruct = $q.all [
                _getMap
                _getLocation
            ]

            determinationConstruct.then (data) ->

                map = data[0]
                currentPosition = data[1]

                ttttttttt = $timeout ->
                    if _.size($scope.nearestMerchantIndex) > 0 and $scope.nearestMerchantSet == false
                        nearestMerchantObject = _.last($scope.nearestMerchantIndex)

                        _lat = nearestMerchantObject.lat
                        _lon = nearestMerchantObject.lon

                        map.panTo(new L.LatLng(_lat, _lon))
                        $scope.nearestMerchantSet = true
                    else
                        map.panTo(new L.latLng(currentPosition.coords.latitude, currentPosition.coords.longitude))
                        $scope.nearestMerchantSet = false

                , 250


        showProductTour = () ->
            ###*
            # showProductTour

            Product Tour.

            @return {undefined}
            ###

            def = $q.defer()
            $scope.alert = ""

            _userRequire.then (user) ->

                if user.handle

                    _t = if $window.is_device then './scripts/' else environment.baseTemplateUrl
                    $mdBottomSheet.show(
                        templateUrl  : _t + "modules/index/partials/bottom-sheet-grid-template.html"
                        controller   : "ProductTourCtrl"
                        #targetEvent : $event
                    ).then (clickedItem) ->

                        if clickedItem
                            $scope.productTourPageMessage = clickedItem.name
                            $scope.contentClass = clickedItem.contentClass
                        return

                return
            return


        _timeout = null

        $scope.showGridBottomSheet = () ->
            ###
            @namespace showGridBottomSheet
            @description

                A check for product tour routes and status.

            @param {object} $event
            @returns {undefined}
            ###

            #return    if not $stateParams.pageConstruct
            if $location.$$url.indexOf('show') != -1
              _timeout = $timeout ->
                  showProductTour()
                  $timeout.cancel _timeout
              , 500

            return


        _.extend $scope,
            center: deviceGeoSettings

        $scope.activityLinksDragData = { }
        $scope.stampScreenDragData = { }

        $scope.onRepositionActivityLinksCompleteDrag = (data, event) ->
          return

        $scope.onRepositionActivityLinksCompleteDrop = (data, event) ->
          return

        $scope.onRepositionNearedMerchantCompleteDrag = (data, event) ->
          return

        $scope.onRepositionNearedMerchantCompleteDrop = (data, event) ->
          return

        $scope.onRepositionStampScreenCompleteDrag = (data, event) ->
          return

        $scope.onRepositionStampScreenCompleteDrop = (data, event) ->
          return

        $scope.closeProductTour = () ->

          profileObject = {}
          profileObject.productTour = {}
          profileObject.productTour.seen = true

          _userRequire.then (_authData) ->
            if _authData
              user_handle = _authData.handle

              profileService.updateProfile(user_handle, profileObject)

          _getMap.then (_map) ->

            #closeProductTour = $timeout ->
              #_map.invalidateSize false
            #, 750

            $mdBottomSheet.hide()
            element = angular.element(document.querySelector('body'))
            element.removeClass('show--product-tour')
            element.removeClass('pt--undefined')
            element.removeClass('pt--hangout')
            element.removeClass('pt--mail')
            element.removeClass('pt--message')
            element.removeClass('pt--copy')
            element.removeClass('pt--facebook')
            element.removeClass('pt--twitter')
            element.removeClass('pt--google')
            if $window.is_device
              element.addClass 'product-tour--closed'
            #$scope.$broadcast 'hide:pt'

        return

    moduleController = [
        '$scope'
        '$timeout'
        '$rootScope'
        'geolocation'
        'leafletData'
        '$window'
        '$mdBottomSheet'
        'firebase.utils'
        'user.require'
        '$location'
        '$q'
        'stamp.service'
        'bank.service'
        'geoJsonUtils'
        '$$geotranslate'
        'notification.service'
        '$stateParams'
        'localStorageService'
        'map.service'
        'center.service'
        'crypto.service'
        'leaflet.extended'
        '$http'
        'profile.service'
        'browser.service'
        'geolocationAvailable'
        IndexIosController
    ]
