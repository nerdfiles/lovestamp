###*
Product Tour Controller
###
define ["interface", "jquery", "services/authentication", "services/profile"], (__interface__, $) ->

  __interface__

  .controller "ProductTourCtrl", [
    "$scope"
    "$mdBottomSheet"
    "$q"
    "$rootScope"
    "$document"
    "$timeout"
    "leafletData"
    "user.require"
    "profile.service"
    "$compile"
    ($scope, $mdBottomSheet, $q, $rootScope, $document, $timeout, leafletData, userRequire, profileService, $compile) ->


      contentTimeout = null
      $scope.productTourLoaded = true

      _userRequire = userRequire()

      $scope.$on '$destroy', () ->
        if contentTimeout
          $timeout.cancel contentTimeout

      _userRequire.then (authData) ->
        currentProfile = $scope.currentProfile = profileService.currentProfile = authData[authData.provider].cachedUserProfile

        if currentProfile

          twitterBg = profileService.currentProfile.profile_image_url
          facebookBg = profileService.currentProfile.picture and profileService.currentProfile.picture.data
          googleBg = profileService.currentProfile.picture and not profileService.currentProfile.picture.data

          if twitterBg
            bg = currentProfile.profile_image_url.replace('_normal', '')

          if facebookBg
            bg = currentProfile.picture.data.url

          if googleBg
            ###
                google_spec =
                  family_name : "Hah"
                  gender      : "other"
                  given_name  : "Ɐha"
                  id          : "100541351182813566938"
                  link        : "https://plus.google.com/100541351182813566938"
                  locale      : "en"
                  name        : "Ɐha Hah (nerdfiles)"
                  picture     : "https://lh5.googleusercontent.com/-rJqQ0GxcdY8/AAAAAAAAAAI/AAAAAAAAD6o/PcGS4XpysFo/photo.jpg"
            ###

            bg = currentProfile.picture

          try
            bg = bg
          catch e
            console.log e

          $scope.productTourInitilized = false


        contentTimeout = $timeout ->
          $contentProgressBar = $('.content--progress-bar')
          html = "<span ng-show='!productTourInitialized'><img class='content--progress-bar-image' src='#{bg}' /></span>"
          a = $compile(html)($scope)
          $contentProgressBar.after(a)
        , 0

      $scope.items = [
        {
          title        : "Progress Bar"
          contentClass : "progress-bar"
          name         : ["The progress bar shows your rewards activity. Click your picture to access your settings."]
          icon         : "hangout"
        }
        {
          title        : "Get Stamped"
          contentClass : "collect"
          name         : ["Click and show your device for stamping."]
          icon         : "mail"
        }
        {
          title        : "Activity History"
          contentClass : "red"
          name         : ["Trace your activity history on the map."]
          icon         : "message"
        }
        {
          title        : "Merchant"
          contentClass : "blue"
          name         : ["Click map icon for merchant details."]
          icon         : "copy"
        }
        {
          title        : "Intensity Circles"
          contentClass : "yellow"
          name         : ["Red gets darker the more a merchant stamps, blue the more your friends get stamped, and yellow the more you get stamped."]
          icon         : "facebook"
        }
        {
          title        : "Reward Transfer"
          contentClass : "mobile"
          #name         : ["Drag and drop the bag icon to transfer your bits to your destination of choice.", "Currently, we only support ChangeTip."]
          name         : ["Drag and drop the bag to use your bits somewhere else (we support ChangeTip for now)."]
          icon         : "twitter"
        }
      ]

      index = 0
      $scope.productTourPageMessage = $scope.items[index].name
      $scope.productTourPageMessageTitle = $scope.items[index].title
      $scope.contentClass = $scope.items[index].contentClass
      lastIndex = 0

      ###
      @namespace productTourNext
      @return {undefined}
      ###
      $scope.productTourNext = () ->
        $scope.contentClass = ""
        $scope.productTourInitialized = true

        def = $q.defer()

        if $scope.items[index + 1]
          index++
          def.resolve(index)
        else
          def.reject index

        def.promise.then () ->
          _index = if (index + 1) > ($scope.items.length - 1) then ($scope.items.length - 1) else (index + 1)
          if _index != lastIndex
            $scope.icon = $rootScope.icon = $scope.items[index].icon
            $scope.contentClass = $scope.items[index].contentClass
            $scope.productTourPageMessage = $scope.items[index].name
            $scope.productTourPageMessageTitle = $scope.items[index].title
            lastIndex = index
          else
            $scope.contentClass = "activity-history"
            angular.element($document[0].body).removeClass 'pt--google'
            $mdBottomSheet.hide()
            $scope.productTourLoaded = true

          if $scope.productTourLoaded
            leafletData.getMap('index').then (map) ->
              map.invalidateSize(false)


      ###
      @namespace productTourPrevious
      @return {undefined}
      ###
      $scope.productTourPrevious = () ->
        $scope.contentClass = ""
        $scope.productTourInitialized = if $scope.productTourInitialized == true then false else true

        def = $q.defer()

        if $scope.items[index - 1]
          index--
          def.resolve(index)
        else
          def.reject index

        def.promise.then () ->
          _index = if (index - 1) < ($scope.items.length - 1) then ($scope.items.length + 1) else (index - 1)
          if _index != lastIndex
            $scope.icon = $rootScope.icon = $scope.items[index].icon
            $scope.contentClass = $scope.items[index].contentClass
            $scope.productTourPageMessage = $scope.items[index].name
            $scope.productTourPageMessageTitle = $scope.items[index].title
            lastIndex = index
          else
            $scope.contentClass = "activity-history"
            angular.element($document[0].body).removeClass 'pt--google'
            $mdBottomSheet.hide()
            $scope.productTourLoaded = true

          #_index = if (index - 1) < 0 then 0 else (index - 1)
          #$scope.icon = $rootScope.icon = $scope.items[_index].icon
          #$scope.contentClass = $scope.items[_index].contentClass
          #$scope.productTourPageMessage = $scope.items[_index].name

          if $scope.productTourLoaded
            leafletData.getMap('index').then (map) ->
              map.invalidateSize(false)

        return


      ###
      @namespace listItemClick
      @return {undefined}
      ###
      $scope.listItemClick = ($index) ->
        $scope.counter = $index
        index = $index
        $scope.clickedItem = clickedItem = $scope.items[$index]
        $scope.icon = $rootScope.icon = $scope.items[$index].icon
        $scope.contentClass = $scope.items[$index].contentClass
        $scope.productTourPageMessage = $scope.items[$index].name
        $scope.productTourPageMessageTitle = $scope.items[$index].title
        return
  ]

  return
