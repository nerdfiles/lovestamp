
/**
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

@module index/controllers/base  
@depends services/authentication  

 *# description

Landing page of the application. Primarily targets #android and #ios systems.
 */

(function() {
  define(['interface', 'lodash', 'moment', 'numeral', 'config', 'leaflet-knn', 'one-color', 'utils/async', 'utils/firebase', 'auth/user/roll-up', 'auth/access/conductor', 'services/stamp', 'services/bank', 'services/map', 'services/crypto', 'services/browser', 'services/notification', 'services/profile', 'leaflet-markercluster', 'angular-geotranslation', 'geojson-tools', 'ProductTourCtrl', 'index/MerchantSheetCtrl', 'index/directives/profile-orb', 'index/directives/merchant-sheet', 'index/directives/set-nav-item', 'directives/title', 'directives/preloader', 'directives/notification', 'directives/loader', 'directives/version', 'directives/relExternal', 'jquery.debounce'], function(__interface__, _, moment, numeral, environment, leafletKnn, color, async) {
    var IndexController, clickRun, currentPositionMarker, deviceGeoSettings, maxZoomSetting, moduleController, personalMarker, toggleLeafletLabel, zoomSetting;
    currentPositionMarker = {};
    deviceGeoSettings = {};
    zoomSetting = 17;
    maxZoomSetting = 18;
    personalMarker = null;
    toggleLeafletLabel = function(leafletData) {

      /*
      @ngdoc directive  
      @name interface.directive:toggleLeafletLabel  
      @function toggleLeafletLabel  
      @description  
      
      A toggle for icon badges rendered above map markers of stamps of merchants.
       */
      var _toggleLeafletLabel;
      _toggleLeafletLabel = function($scope, element) {

        /*
        @ngdoc directiveLink
        @inner
        
        Element checking for leaflet labels, and toggling.
         */
        leafletData.getMap('index').then(function() {
          var $leafletLabelStart;
          $leafletLabelStart = environment.document.querySelectorAll('.leaflet-label');
          $scope.$on('markers:available', function() {
            var $leafletClickable, groupsLabel;
            $leafletLabelStart = environment.document.querySelectorAll('.leaflet-label');
            groupsLabel = _.groupBy($leafletLabelStart, function(_label) {
              return _label.className;
            });
            _.forEach(groupsLabel, function(flattenedGroups, item) {

              /*
              Collect elements to be labeled.
               */
              var a;
              a = environment.document.querySelector('.' + item.split(' ')[1]);
              angular.element(a).addClass('first');
            });
            $leafletClickable = environment.document.querySelectorAll('img.leaflet-marker-icon');
            _.forEach($leafletClickable, function($element) {

              /*
              Set labels under initial conditions.
               */
              angular.element($element).on('dblclick', function() {
                var $leafletLabel;
                $leafletLabel = environment.document.querySelectorAll('.leaflet-label');
                _.forEach($leafletLabel, function($elementLabel) {
                  angular.element($elementLabel).addClass('leaflet-label--hidden');
                });
              });
            });
            angular.element(element).on('dblclick', function() {

              /*
              Remove labels under interaction conditions.
               */
              var $leafletLabel;
              $leafletLabel = environment.document.querySelectorAll('.leaflet-label');
              _.forEach($leafletLabel, function($elementLabel) {
                angular.element($elementLabel).removeClass('leaflet-label--hidden');
              });
            });
          });
        });
      };
      return {
        restrict: 'A',
        scope: false,
        link: _toggleLeafletLabel
      };
    };
    __interface__.directive('toggleLeafletLabel', ['leafletData', toggleLeafletLabel]);
    clickRun = function() {
      return {
        restrict: 'A',
        scope: false,
        link: function($scope, element) {
          var leafletLabel$, leafletLetLabel;
          leafletLabel$ = document.querySelectorAll('.leaflet-label');
          leafletLetLabel = element.find('.leaflet-label');
          leafletLabel$ = angular.element(leafletLabel$);
          $scope.$on('hideControlLayers', function(event, controlObject) {
            var leafletControlLayers, mapZoomConfig;
            mapZoomConfig = controlObject;
            leafletControlLayers = angular.element(element).find('.leaflet-control-layers input');
            return _.forEach(leafletControlLayers, function(a) {
              if (angular.element(a).prop('checked') !== controlObject) {
                return angular.element(a).trigger('click');
              }
            });
          });
          return $scope.$on('markers:preview', function(event, $schema) {
            leafletLabel$ = document.querySelectorAll('.leaflet-label');
            leafletLetLabel = element.find('.leaflet-label');
            leafletLabel$ = angular.element(leafletLabel$);
            leafletLetLabel.addClass('disable');
            return leafletLabel$.addClass('disable');
          });
        }
      };
    };
    __interface__.directive('clickRun', [clickRun]);
    IndexController = function($scope, $timeout, $rootScope, geolocation, leafletData, $window, $mdBottomSheet, firebaseUtils, userRequire, $location, $q, stampService, bankService, geoJsonUtils, $$geotranslate, notificationService, $stateParams, localStorageService, mapService, centerService, cryptoService, leafletExtended, $http, profileService, browserService, geolocationAvailable) {
      var AsyncColoringLibrary, LeafIcon, PersonalLeafIcon, b, bgGeo, c, calculateDistances, closeProductTour, datePrefix, fullnow, geolocator, indexMapDefaults, loadMerchantsByStamp, mAppInBackground, merchantMarkerActive, merchantMarkerInactive, merchantMarket, nearestListener, noMapTimeout, now, oms, options, showProductTour, timecard, tt, ttt, tttt, ttttt, tttttt, ttttttt, tttttttt, ttttttttt, unitedStatesLocationConstruct, _basicWatcher, _getLocation, _getMap, _now, _ref, _timeout, _userRequire, _watchLocation;
      oms = null;
      $scope.showMap = true;
      $scope.noReferrer = true;
      $scope.alert = "";
      $scope.coords = {};
      $scope.merchantLinks = [];
      $scope.cancelSheetMap = {};
      $scope.personalIntensityFound = false;
      $scope.map = null;
      $scope.page = {};
      $scope.page.cache = {};
      $scope.page.cache.geolocation = null;
      $scope.mapHash = {};
      $scope.windowTitle = 'Home';
      ttt = null;
      tttt = null;
      ttttt = null;
      tttttt = null;
      ttttttt = null;
      tttttttt = null;
      ttttttttt = null;
      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (toState.name === 'home' || toState.name === 'fan-view' || toState.name === 'home-show' || toState.name === 'home-show-tour' || toState.name === 'home-show-zoom') {
          return $window.dispatchEvent(new Event('resize'));
        }
      });
      noMapTimeout = $timeout(function() {
        var element;
        element = angular.element(document.querySelector('body'));
        return element.removeClass('no-map');
      }, 500);
      $scope.noMap = void 0;
      $scope.$on('$destroy', function() {
        return leafletData.unresolveMap('index');
      });
      LeafIcon = L.Icon.extend({
        options: {
          iconSize: [48, 48],
          iconAnchor: [24, 48]
        }
      });
      PersonalLeafIcon = L.Icon.extend({
        options: {
          iconSize: [16, 16],
          iconAnchor: [16 / 2, 16 / 2]
        }
      });
      _now = moment().format('dddd');
      now = _now.toLowerCase();
      fullnow = moment().format('MM/DD/YYYY HH:mm');
      datePrefix = moment().format('MM/DD/YYYY');
      merchantMarkerInactive = $window.is_device ? '../www/images/map-marker-inactive.png' : '/assets/images/map-marker-inactive.png';
      merchantMarkerActive = $window.is_device ? '../www/images/map-marker.png' : '/assets/images/map-marker.png';
      closeProductTour = null;
      noMapTimeout = null;
      timecard = function(stampData) {

        /*
        Every stamp needs a timecard if it's on an active map.
         */
        var activeDays, currentlyActiveDay, def, endTime, et, markerOption, st, startTime, ___endTime, ___startTime, __endTime, __startTime, _endTime, _startTime;
        def = $q.defer();
        if (stampData.days && stampData.hours) {
          activeDays = _.filter(_.map(stampData.days, function(val, dayName) {
            if (val) {
              return dayName;
            }
          }), function(day) {
            return day;
          });
          currentlyActiveDay = _.find(activeDays, function(day) {
            return day === now;
          });
          if (currentlyActiveDay === void 0 || currentlyActiveDay === false) {
            markerOption = new LeafIcon({
              iconUrl: merchantMarkerInactive
            });
          } else {
            if (stampData.hours && !stampData.hours.alwaysOpen) {
              startTime = stampData.hours.start;
              endTime = stampData.hours.end;
              _startTime = startTime.split(':');
              _endTime = endTime.split(':');
              st = parseInt(_startTime[0], 10);
              et = parseInt(_endTime[0], 10);
              __startTime = st < 12 ? st + 12 : st;
              __endTime = et < 12 ? et + 12 : et;
              ___startTime = [__startTime, _startTime[1]].join(':');
              ___endTime = [__endTime, _endTime[1]].join(':');
              markerOption = new LeafIcon({
                iconUrl: merchantMarkerActive
              });
              if (startTime && endTime && moment(fullnow).isBetween(datePrefix + ' ' + ___startTime, datePrefix + ' ' + ___endTime)) {
                markerOption = new LeafIcon({
                  iconUrl: merchantMarkerActive
                });
              } else {
                markerOption = new LeafIcon({
                  iconUrl: merchantMarkerInactive
                });
              }
            } else {
              markerOption = new LeafIcon({
                iconUrl: merchantMarkerActive
              });
            }
          }
          def.resolve(markerOption);
        } else {
          def.resolve(false);
        }
        return def.promise;
      };
      $scope.is_iOS = (_ref = navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) != null ? _ref : {
        "true": false
      };
      indexMapDefaults = {
        defaults: {
          tileLayer: $window.is_device !== true ? 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png' : 'http://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png',
          tileLayerOptions: {
            opacity: 1,
            detectRetina: true,
            reuseTiles: true
          },
          keyboard: true,
          dragging: true,
          doubleClickZoom: true,
          zoomControl: false,
          zoomAnimation: $window.is_android ? true : true,
          fadeAnimation: $window.is_android ? false : true,
          touchZoom: true,
          markerZoomAnimation: $window.is_android ? true : true,
          minZoom: 11
        }
      };
      _.extend($scope, indexMapDefaults);
      unitedStatesLocationConstruct = {
        latitude: 37.6,
        longitude: -95.665
      };
      options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
      };
      if (cryptoService.hash(JSON.stringify(geolocationAvailable.geolocationAvailable.coords)) === cryptoService.hash(JSON.stringify(unitedStatesLocationConstruct))) {
        zoomSetting = 3;
        _getLocation = geolocation.getLocation(options, unitedStatesLocationConstruct);
      } else {
        _getLocation = geolocation.getLocation(options);
      }
      _userRequire = userRequire();
      _userRequire.then(function(authData) {
        if (!authData) {
          return;
        }
        return profileService.getUserStatus(authData.handle).$loaded().then(function(profileData) {
          var indexed;
          if (profileData.profile.productTour) {
            $scope.productTourSeen = profileData.profile.productTour.seen;
          }
          indexed = false;
          $scope.$watch('productTourSeen', function(e, newVal) {
            if (newVal === true && indexed === false && geolocator.data) {
              _getMap.then(function(map) {
                $scope.setMarker($scope.map, geolocator.data);
                return indexed = true;
              });
            }
            if (newVal === void 0 && indexed === false) {
              return indexed = true;
            }
          });
        });
      });
      $scope.loadInBrowser = function(websiteLink) {
        var config;
        config = {
          url: websiteLink,
          target: "_blank",
          options: []
        };
        browserService.open(config);
      };
      $scope.doneLoading = false;
      _getMap = leafletData.getMap('index');
      _getMap.then(function() {
        return $timeout(function() {
          return $scope.$apply(function() {
            return $scope.doneLoading = true;
          });
        }, 0);
      });
      $scope.watcherOn = false;
      $scope.bgModeIsActive = false;
      _watchLocation = geolocation.watchLocation();
      $scope.nearestMerchantCount = 0;
      $scope.watcherTimeout = null;
      $scope.nearestTimeout = null;
      mAppInBackground = false;
      document.addEventListener('pause', function() {
        return mAppInBackground = true;
      });
      document.addEventListener('resume', function() {
        return mAppInBackground = false;
      });
      if (typeof cordova !== 'undefined') {
        $scope.bgModeIsActive = mAppInBackground;
      }
      _basicWatcher = function(data) {
        var centerConfigConstruct, lat, lng, nearestListener, _coords;
        _coords = {
          lat: data.coords.latitude,
          lng: data.coords.longitude
        };
        lat = _coords.lat;
        lng = _coords.lng;
        if (lat && lng && mAppInBackground === false) {
          centerConfigConstruct = {
            autoDiscover: false,
            zoom: maxZoomSetting
          };
          _.extend(centerConfigConstruct, _coords);
          $scope.centerConfigConstruct = centerConfigConstruct;
          if (personalMarker) {
            personalMarker.setLatLng([lat, lng]).update();
          }
        }
        if ($scope.watcherOn === false && mAppInBackground === true) {
          return nearestListener = $scope.$watch('nearestMerchantIndex.length', function(nearestMerchantCount) {
            var messageList, mlib;
            $scope.nearestMerchantCount = nearestMerchantCount;
            mlib = {
              message1: "LoveStamper nearby. Go get some love!",
              message2: "LoveStamper nearby. Get stamped!",
              message3: "LoveStamper nearby. Choose the red pill!",
              message4: "LoveStamper nearby. Get impressed!",
              message5: "LoveStamper nearby. Don't get Trumped, get LoveStamped!",
              message6: "LoveStamper nearby. Get stamped and make America great again!",
              message7: "LoveStamper nearby. Follow the blue line Neo!",
              message8: 'LoveStamper nearby. Be impressed!',
              message9: 'LoveStamper nearby. Get stamped!',
              message10: 'LoveStamper nearby. Go get some love!',
              message11: 'LoveStamper nearby. Get stamped and make America great again!',
              message12: "LoveStamper nearby. Ain't everybody got time for that!",
              message13: 'LoveStamper nearby. Yo mama got stamped already!'
            };
            messageList = _.toArray(mlib);
            if (parseInt($scope.nearestMerchantCount, 10) > 0) {
              profileService.getUserStatus(authData.handle).$loaded().then(function(profileData) {
                var e;
                if (profileData.profile.pushNotifications.status !== false) {
                  try {
                    return plugin.notification.local.add({
                      message: messageList.randomPrint(),
                      sound: null
                    });
                  } catch (_error) {
                    e = _error;
                    return notificationService.remotePost(JSON.stringify(e), 'plugin.notification.local.add');
                  }
                }
              });
            } else {
              $scope.watcherOn = false;
              nearestListener();
              return;
            }
            return $scope.watcherOn = true;
          });
        }
      };
      $scope.$on('$destroy', function() {
        return leafletData.unresolveMap('index');
      });
      $scope.changedLatLng = null;
      $scope.bgGeoStarted = false;
      nearestListener = null;
      bgGeo = null;
      document.addEventListener('deviceready', (function() {
        plugin.notification.local.registerPermission(function(granted) {
          if (!granted) {
            cordova.plugins.backgroundMode.disable();
            if (bgGeo) {
              return bgGeo.stop();
            }
          }
        });
        return _userRequire.then(function(authData) {
          if (!authData) {
            return;
          }
          localStorageService.set('provider', authData.provider);
          return _getLocation.then(function() {
            return profileService.getUserStatus(authData.handle).$loaded().then(function(profileData) {
              if (profileData.profile.pushNotifications.status === true) {
                cordova.plugins.backgroundMode.enable();
              }
              if (profileData.profile.pushNotifications.status === false) {
                cordova.plugins.backgroundMode.disable();
                if (bgGeo) {
                  bgGeo.stop();
                }
                return;
              }
              return plugin.notification.local.hasPermission(function(granted) {
                var callbackFn, failureFn, postLocationCallback;
                if (!granted) {
                  cordova.plugins.backgroundMode.disable();
                  return;
                }
                bgGeo = $window.plugins.backgroundGeoLocation;
                postLocationCallback = function(response) {
                  return bgGeo.finish();
                };
                callbackFn = function(location) {
                  $rootScope.$on('geolocation.position.changed', function(event, geolocationConstruct) {
                    $scope.changedLatLng = geolocationConstruct;
                    if (!$scope.nearestTimeout) {
                      $scope.nearestTimeout = $timeout(function() {
                        $scope.showNearestMerchant();
                        return $timeout.cancel($scope.nearestTimeout);
                      }, 15000);
                    }
                    if (!$scope.watcherTimeout) {
                      $scope.watcherTimeout = $timeout(function() {
                        _basicWatcher(geolocationConstruct);
                        return $timeout.cancel($scope.watcherTimeout);
                      }, 30000);
                    }
                  });
                  postLocationCallback.call(this);
                };
                failureFn = function() {
                  notificationService.displayError('Failed to update client location.');
                };
                cordova.plugins.backgroundMode.ondeactivate = function() {
                  if (nearestListener) {
                    nearestListener();
                  }
                  if ($scope.watcherTimeout) {
                    $timeout.cancel($scope.watcherTimeout);
                    $scope.watcherTimeout = null;
                    $timeout.cancel($scope.nearestTimeout);
                    $scope.nearestTimeout = null;
                    $scope.watcherOn = false;
                  }
                  if ($scope.bgGeoStarted) {
                    bgGeo.stop();
                    return $scope.bgGeoStarted = false;
                  }
                };
                cordova.plugins.backgroundMode.onactivate = function() {
                  console.log('Watching. . .');
                  bgGeo.configure(callbackFn, failureFn, {
                    desiredAccuracy: 10,
                    stationaryRadius: 20,
                    distanceFilter: 30,
                    activityType: 'Fitness',
                    debug: false
                  });
                  if ($scope._nearestTimeout) {
                    $timeout.cancel($scope._nearestTimeout);
                  }
                  if ($scope._watcherTimeout) {
                    $timeout.cancel($scope._watcherTimeout);
                  }
                  bgGeo.start();
                  bgGeo.changePace(true);
                  $scope.bgGeoStarted = true;
                };
              });
            });
          });
        });
      }), false);
      $scope._nearestTimeout = null;
      $scope._watcherTimeout = null;
      $scope.oldGeolocationConstruct = null;
      c = null;
      b = null;
      $rootScope.$on('geolocation.position.changed', function(event, geolocationConstruct) {
        if (mAppInBackground === true) {
          if ($scope._nearestTimeout) {
            $timeout.cancel($scope._nearestTimeout);
          }
          if ($scope._watcherTimeout) {
            $timeout.cancel($scope._watcherTimeout);
          }
          return;
        }
        if (mAppInBackground === false) {
          if (!$scope._nearestTimeout) {
            $scope._nearestTimeout = $timeout(function() {
              $scope.showNearestMerchant();
              $timeout.cancel($scope._nearestTimeout);
            }, 15000);
          }
          if (!$scope._watcherTimeout) {
            $scope.watcherTimeout = $timeout(function() {
              _basicWatcher(geolocationConstruct);
              $timeout.cancel($scope._watcherTimeout);
            }, 30000);
          }
        }
        $scope.oldGeolocationConstruct = geolocationConstruct;
      });
      $scope.$on('$locationChangeStart', function() {
        if ($scope._watcherTimeout) {
          $timeout.cancel($scope.watcherTimeout);
          $scope.watcherTimeout = null;
        }
        if ($scope._nearestTimeout) {
          $timeout.cancel($scope._nearestTimeout);
          $scope._nearestTimeout = null;
        }
        if (_watchLocation) {
          return geolocation.clearWatch(_watchLocation);
        }
      });
      $scope.linksActivitySet = false;
      $scope.showLinksActivityTracking = function() {

        /*
        @namespace showLinksActivityTracking
        @description
        
        A use case for Falconry's Scout service.
         */
        var linksActivityConstruct;
        linksActivityConstruct = $q.all([merchantMarket, _getMap]);
        linksActivityConstruct.then(function(linkActivityTrackingConstruct) {
          var e, linkActivityTracking, map;
          linkActivityTracking = linkActivityTrackingConstruct[0];
          map = linkActivityTrackingConstruct[1];
          if ($scope.linksActivitySet === true) {
            new mapService.Falconry.Scout(linkActivityTracking, map);
          }
          if ($scope.linksActivitySet === false) {
            try {
              new mapService.Falconry.Scout(linkActivityTracking, map);
              $scope.linksActivitySet = true;
            } catch (_error) {
              e = _error;
              console.dir(e);
            }
          } else {
            $scope.linksActivitySet = false;
          }
        });
      };

      /*
      @namespace clearMap
      @description
      
      A use case for Falconry's clearing service.
      
      @return {undefined}
       */
      $scope.clearMap = mapService.Falconry.clearMap;

      /*
      @namespace acl
      @description
      
      A use case for Falconry's predictive "ringer" service.
       */
      $scope.mapService = {};
      $scope.mapService.acl = new mapService.Falconry.Ringer();
      AsyncColoringLibrary = $scope.mapService.acl;
      $scope.indexConstruct = $q.all([_userRequire, _getLocation, _getMap]);
      geolocator = {};
      tt = null;
      $scope.indexConstruct.then(function(data) {

        /*
         * indexConstruct
        
        All aboard, async Promises.
        
        @param {Promise:object} data
         */
        var authData, geoData, homescreenAddedCheck, map, user;
        authData = data[0];
        if (!authData) {
          return;
        }
        $scope.doneLoading = true;
        geoData = geolocator.data = data[1];
        map = $scope.map = data[2];
        homescreenAddedCheck = localStorageService.get('homescreenAdded');
        if ($window.is_device !== true) {
          if (homescreenAddedCheck !== 'true') {
            notificationService.displayHomeScreenAdd('<div class="homescreenAdded--inner"><span class="ath-action-icon">&nbsp;</span> Add us to your home screen!</div>', {
              hideDelay: 5000
            });
            localStorageService.set('homescreenAdded', true);
          }
        }
        $scope.$watch('productTourLoaded', function(newValue, oldValue) {
          if (!newValue) {
            return tt = $timeout(function() {}, 500);
          }
        });
        $scope.setMarker(map, geoData);
        $scope.merchantsNearby = [];
        $scope.user = user = authData;
        $rootScope.userLanded = true;
        $rootScope.handle = user.handle;
      });
      $scope.setMarker = function(map, data) {

        /*
         * setMarker
        
        @param {object} map
        @param {object} data Geodata (not GeoJson).
         */
        var personalArea, personalIconRef, personalMapMarker;
        if (!data || !map) {
          return;
        }
        _.extend(deviceGeoSettings, {
          lat: data.coords.latitude,
          lng: data.coords.longitude,
          zoom: $stateParams.zoomSetting ? $stateParams.zoomSetting.int() : zoomSetting
        });
        personalIconRef = $window.is_device ? '../www/images/map-marker--personal.png' : '/assets/images/map-marker--personal.png';
        personalMapMarker = new PersonalLeafIcon({
          iconUrl: personalIconRef
        });
        personalArea = L.circle([data.coords.latitude, data.coords.longitude], 152.4, {
          color: '#BDD4FE',
          fillColor: '#BDD4FE'
        });
        map.addLayer(personalArea);
        personalMarker = L.marker([data.coords.latitude, data.coords.longitude], {
          icon: personalMapMarker,
          zIndexOffset: 100
        });
        map.addLayer(personalMarker);
      };
      loadMerchantsByStamp = function() {

        /*
         * loadMerchantsByStamp
        
        Asynchronously load all available merchants with stamps into map view.
         */
        var def, linksActivityTracking, mapAndUserLoaded;
        def = $q.defer();
        linksActivityTracking = [];
        mapAndUserLoaded = $q.all([_userRequire, _getMap, _getLocation]);
        _getLocation.then(function() {
          return ttt = $timeout(function() {
            return $scope.$apply(function() {
              return $scope.noMap = false;
            });
          }, 0);
        }, function() {
          return ttt = $timeout(function() {
            return $scope.$apply(function() {
              return $scope.noMap = true;
            });
          }, 0);
        });
        mapAndUserLoaded.then(function(data) {
          var d, locationData, map, stampMarkers, user;
          stampMarkers = new L.MarkerClusterGroup({
            spiderfyOnMaxZoom: false,
            showCoverageOnHover: false,
            zoomToBoundsOnClick: true,
            removeOutsideVisibleBounds: true,
            disableClusteringAtZoom: 15
          });
          user = data[0];
          map = data[1];
          locationData = data[2];
          if (!user) {
            return;
          }
          if (!map) {
            return;
          }
          if (!locationData) {
            return;
          }
          $scope.merchants = stampService.getAllStamps();
          oms = new leafletExtended.OverlappingMarkerSpiderfier(map);
          $scope.merchants.$loaded().then(function(merchantData) {
            var merchantsByIntensity;
            merchantsByIntensity = bankService.getIntensityMerchant(merchantData);
            merchantsByIntensity.then(function(d) {
              var f, flattenedAvailableStamps, flattenedMerchantSystem, merchantMarketData, totalMerchantSystem, uniqueMerchant, _availableStamps, _flattenedMerchantSystem, _merchantData;
              _merchantData = d;
              totalMerchantSystem = angular.copy(_merchantData);
              f = _.filter(totalMerchantSystem, function(m) {
                return m.stamps && m.stamps.local;
              });
              _availableStamps = [];
              merchantMarketData = _merchantData;
              _.forEach(totalMerchantSystem, function(merchantStamper) {
                if (merchantStamper.stamps && merchantStamper.stamps.local) {
                  return _.forEach(merchantStamper.stamps.local, function(localStamp, keyName) {
                    _.extend(localStamp, {
                      $id: keyName,
                      stampOwner: merchantStamper.$id
                    });
                    return _availableStamps.push(localStamp);
                  });
                }
              });
              flattenedAvailableStamps = _.filter(_availableStamps, function(a) {
                return a.address;
              });
              $scope._flattenedAvailableStamps = flattenedAvailableStamps;
              _.forEach($scope._flattenedAvailableStamps, function(_r) {
                var e, loc;
                try {
                  loc = _.toArray(_r.address.location);
                  _r.hashed = cryptoService.hash(JSON.stringify(loc.join(',')));
                } catch (_error) {
                  e = _error;
                  console.log(e);
                }
              });
              uniqueMerchant = _.map(totalMerchantSystem, function(_merchant) {
                return _.filter(_merchant.stamps.local, function(localStampStore) {
                  return localStampStore.address;
                });
              });
              _flattenedMerchantSystem = _.extend.apply(_, uniqueMerchant);
              flattenedMerchantSystem = _.map(_flattenedMerchantSystem, function(__merchantStamp) {
                if (__merchantStamp.address) {
                  return __merchantStamp.address.location;
                }
              });
              _.forEach(_merchantData, function(merchant) {
                var e, merchantStampImpressionRed, quotaColor, updatedMerchantSystem, _b;
                if (!merchant.stamps) {
                  return;
                }
                if (merchant.stamps && !merchant.stamps.local) {
                  return;
                }
                quotaColor = merchant.merchantIntensityQuota.int() / 100;
                merchantStampImpressionRed = color(AsyncColoringLibrary.defaultColorSetting.startRed).saturation(quotaColor).hex();
                _.extend(merchant, {
                  merchantIntensityQuota: merchant.merchantIntensityQuota,
                  merchantIntensityQuotaColor: merchantStampImpressionRed
                });
                $scope.merchantLinks.push(merchant);
                updatedMerchantSystem = _.map(merchant.stamps.local, function(localStamps, keyName) {
                  return _.extend(localStamps, {
                    $id: keyName,
                    merchantIntensityQuota: merchant.merchantIntensityQuota,
                    merchantIntensityQuotaColor: merchantStampImpressionRed,
                    stampOwner: merchant.$id
                  });
                });
                try {
                  if (user.handle) {
                    _b = bankService.getIntensityFriendAndPersonal(user.handle, updatedMerchantSystem, totalMerchantSystem);
                  }
                } catch (_error) {
                  e = _error;
                  console.log('Unable to load friend and personal aspects.');
                  return;
                }
                _b.then(function(_stampSystem) {
                  return (function(stampSystem) {
                    return tttt = $timeout(function() {
                      var _merchantSystem;
                      _merchantSystem = _.filter(stampSystem, function(_stamp) {
                        return _stamp.address && _stamp.active === true;
                      });
                      return async.map(_merchantSystem, AsyncColoringLibrary.color.bind(AsyncColoringLibrary), function(error, _merchantSystem) {
                        var highestBlue, highestRed, highestYellow, _gms;
                        highestYellow = color(AsyncColoringLibrary.defaultColorSetting.startYellow).saturation(bankService.getHighestYellow() / 0.15).hex();
                        highestBlue = color(AsyncColoringLibrary.defaultColorSetting.startBlue).saturation(bankService.getHighestBlue() / 0.15).hex();
                        highestRed = color(AsyncColoringLibrary.defaultColorSetting.startRed).saturation(bankService.getHighestRed() / 0.15).hex();
                        try {
                          _gms = _.uniq(_merchantSystem, function(_ms) {
                            var loc;
                            loc = _.toArray(_ms.address.location);
                            return cryptoService.hash(JSON.stringify(loc.join(',')));
                          });
                        } catch (_error) {
                          e = _error;
                          console.log(e);
                        }
                        return _.forEach(_gms, function(_stampData) {
                          var address, friendCircleIntensity, loc, m, mapHashId, merchantCircleIntensity, merchantGeolocation, merchantLocationConstruct, merchantObject, personalCircleIntensity, stampData, t, _merchantLocationConstruct;
                          personalCircleIntensity = null;
                          friendCircleIntensity = null;
                          merchantCircleIntensity = null;
                          t = null;
                          stampData = _stampData;
                          if (stampData && stampData.address) {
                            address = stampData.address;
                          } else {
                            return;
                          }
                          if (address && !address.location) {
                            try {
                              stampService.updateStampAddress(stampData, stampData.username).then(function(u) {
                                return console.log('Updating Stamp Address. . .');
                              });
                            } catch (_error) {
                              e = _error;
                              notificationService.displayError('Failed to update stamp address.');
                            }
                          }
                          if (address && address.location) {
                            merchantGeolocation = {};
                            _.extend(merchantGeolocation, address.location);
                            merchantLocationConstruct = [merchantGeolocation.lat, merchantGeolocation.lng];
                            mapHashId = cryptoService.hash(merchantLocationConstruct.join(','));
                            m = L.marker(merchantLocationConstruct, {
                              zIndexOffset: 200
                            });
                            try {
                              loc = _.toArray(stampData.address.location);
                              m.colls = cryptoService.hash(JSON.stringify(loc.join(',')));
                            } catch (_error) {
                              e = _error;
                              console.log(e);
                            }
                            merchantObject = {
                              merchant: stampData,
                              location: m.getLatLng()
                            };
                            linksActivityTracking.push(merchantObject);
                            _merchantLocationConstruct = _.map(merchantLocationConstruct, function(coord, index) {
                              return coord;
                            });
                            personalCircleIntensity = L.circle(_merchantLocationConstruct, 40, {
                              stroke: true,
                              weight: 2,
                              color: highestYellow,
                              fill: true,
                              fillColor: _.isNaN(stampData.personalIntensityQuota) ? stampData.personalIntensityInactiveColor : stampData.personalIntensityQuotaColor,
                              fillOpacity: 1,
                              className: 'personal-circle'
                            });
                            friendCircleIntensity = L.circle(_merchantLocationConstruct, 30, {
                              stroke: true,
                              weight: 2,
                              color: highestBlue,
                              fill: true,
                              fillColor: _.isNaN(stampData.friendIntensityQuota) ? stampData.friendIntensityInactiveColor : stampData.friendIntensityQuotaColor,
                              fillOpacity: 1,
                              className: 'friend-circle'
                            });
                            merchantCircleIntensity = L.circle(_merchantLocationConstruct, 20, {
                              stroke: true,
                              weight: 2,
                              color: highestRed,
                              fill: true,
                              fillColor: _.isNaN(stampData.merchantIntensityQuota) ? stampData.merchantIntensityInactiveColor : stampData.merchantIntensityQuotaColor,
                              fillOpacity: 1,
                              className: 'merchant-circle'
                            });
                            m.hy = highestYellow;
                            m.hb = highestBlue;
                            m.hr = highestRed;
                            m.pa = stampData.personalActivity;
                            m.fa = stampData.friendActivity;
                            m.piq = stampData.personalIntensityQuotaColor;
                            m.fiq = stampData.friendIntensityQuotaColor;
                            m.miq = stampData.merchantIntensityQuotaColor;
                            timecard(stampData).then(function(stampTime) {
                              var cb, layerControl, layerGroup, mapZoomConfig, _t;
                              layerGroup = null;
                              m.setIcon(stampTime);
                              $scope.mapHash[mapHashId] = m;
                              m.personalCircleIntensity = personalCircleIntensity;
                              m.friendCircleIntensity = friendCircleIntensity;
                              m.merchantCircleIntensity = merchantCircleIntensity;
                              try {
                                m.anyDupes = _.filter($scope._flattenedAvailableStamps, function(a) {
                                  loc = _.toArray(stampData.address.location);
                                  return a.hashed === cryptoService.hash(JSON.stringify(loc.join(',')));
                                });
                                m.anyDupesOwner = _.filter($scope._flattenedAvailableStamps, function(a) {
                                  var _loc;
                                  _loc = _.toArray(stampData.address.location);
                                  return a.stampOwner === stampData.stampOwner && a.hashed === cryptoService.hash(JSON.stringify(_loc.join(','))) && a.alias !== stampData.alias;
                                });
                              } catch (_error) {
                                e = _error;
                                console.log(e);
                              }
                              m.hasSimpleDupes = _.size(m.anyDupes) > 1;
                              m.hasOwnershipDupes = _.size(m.anyDupesOwner) === 0;
                              oms.addMarker(m);
                              if (m.hasSimpleDupes && m.hasOwnershipDupes) {
                                m.bindLabel('<span class="fa fa-plus-circle"></span>', {
                                  noHide: true,
                                  offset: [8, -37],
                                  className: 'spiderfied-label--' + m.colls
                                });
                              } else {
                                layerGroup = L.layerGroup().addLayer(personalCircleIntensity).addLayer(friendCircleIntensity).addLayer(merchantCircleIntensity).addTo(map);
                                if (!layerControl) {
                                  layerControl = L.control.layers().addTo(map);
                                }
                                layerControl.addOverlay(layerGroup, "MS");
                              }
                              _t = null;
                              cb = function(b, c) {
                                $scope.$broadcast('prestamp:sheet', merchantObject.merchant);
                              };
                              if (m.hasSimpleDupes === false && m.hasOwnershipDupes === true) {
                                m.on('contextmenu touchstart mouseenter mousedown', function(_e) {
                                  cb(void 0, _e);
                                });
                              }
                              stampMarkers.addLayer(m);
                              mapZoomConfig = {
                                start: map.getZoom(),
                                end: map.getZoom()
                              };
                              t = null;
                              oms.addListener('spiderfy', function() {
                                var cz, mLabels, z;
                                $scope.disableBottomsheet = false;
                                mLabels = document.querySelectorAll('.spiderfied-label--' + m.colls);
                                cz = function(hide) {
                                  return [].forEach.call(mLabels, function(mLabel) {
                                    var a;
                                    a = angular.element(mLabel);
                                    if (hide) {
                                      a.removeClass('enable');
                                      return a.addClass('leaflet-label--hide-forever');
                                    } else {
                                      a.addClass('enable');
                                      return a.removeClass('leaflet-label--hide-forever');
                                    }
                                  });
                                };
                                cz(true);
                                z = function(_e) {
                                  var timeoutTest;
                                  if (_e.type === 'contextmenu') {
                                    timeoutTest = 0;
                                  } else {
                                    timeoutTest = 750;
                                  }
                                  $scope.bs = $timeout(function() {
                                    if ($scope.disableBottomsheet === false) {
                                      cb(void 0, _e);
                                    }
                                    return $scope.disableBottomsheet = false;
                                  }, timeoutTest);
                                };
                                m.on('contextmenu touchstart mouseenter mousedown', z);
                                oms.addListener('unspiderfy', function() {
                                  cz(false);
                                  $timeout.cancel($scope.bs);
                                  $scope.disableBottomsheet = true;
                                  return m.off('contextmenu touchstart mouseenter mousedown', z);
                                });
                                return $scope.$broadcast('hideControlLayers', false);
                              });
                              map.on('zoomstart', function(e) {
                                mapZoomConfig.start = map.getZoom();
                              });
                              return map.on('zoomend', function(e) {
                                var diff, _fci, _mci, _pci;
                                mapZoomConfig.end = map.getZoom();
                                diff = mapZoomConfig.start - mapZoomConfig.end;
                                _mci = 20;
                                _fci = 30;
                                _pci = 40;
                                $scope.$broadcast('hideControlLayers', false);
                                try {
                                  if (layerGroup) {
                                    map.removeLayer(layerGroup);
                                  }
                                } catch (_error) {
                                  e = _error;
                                  console.dir(e);
                                }
                                if (mapZoomConfig.end === 16) {
                                  _mci = 20;
                                  _fci = 30;
                                  _pci = 40;
                                  $scope.$broadcast('markers:preview:available', {
                                    preview: true
                                  });
                                }
                                if (mapZoomConfig.end === 17) {
                                  _mci = 20;
                                  _fci = 30;
                                  _pci = 40;
                                  $scope.$broadcast('markers:preview:available', {
                                    preview: true
                                  });
                                  if (m.hasSimpleDupes === false && m.hasOwnershipDupes === true) {
                                    $scope.$broadcast('hideControlLayers', true);
                                    try {
                                      if (layerGroup) {
                                        map.addLayer(layerGroup);
                                      }
                                    } catch (_error) {
                                      e = _error;
                                      console.dir(e);
                                    }
                                  }
                                }
                                if (mapZoomConfig.end === 18) {
                                  _mci = 10;
                                  _fci = 15;
                                  _pci = 20;
                                  $scope.$broadcast('markers:preview:available', {
                                    preview: true
                                  });
                                  if (m.hasSimpleDupes === false && m.hasOwnershipDupes === true) {
                                    $scope.$broadcast('hideControlLayers', true);
                                    try {
                                      if (layerGroup) {
                                        map.addLayer(layerGroup);
                                      }
                                    } catch (_error) {
                                      e = _error;
                                      console.dir(e);
                                    }
                                  }
                                }
                                merchantCircleIntensity.setRadius(_mci);
                                personalCircleIntensity.setRadius(_pci);
                                friendCircleIntensity.setRadius(_fci);
                              });
                            });
                          }
                        });
                      });
                    }, 50);
                  })(_stampSystem);
                });
              });
              if (stampMarkers) {
                return map.addLayer(stampMarkers);
              }
            });
          });
          def.resolve(linksActivityTracking);
          map.on('layeradd', function() {
            return ttttttt = $timeout(function() {
              return $scope.$apply(function() {
                $scope.$broadcast('markers:available', true);
                return $timeout.cancel(ttttttt);
              });
            }, 0);
          });
          map.on('focus', function() {
            return $scope.$broadcast('markers:available', true);
          });
          map.on('zoomend', function() {
            return $scope.$broadcast('markers:available', true);
          });
          d = _.map(linksActivityTracking, function(arrayLike) {
            var L1, L2, l1, l2;
            L1 = locationData.coords.latitude;
            l1 = locationData.coords.longitude;
            L2 = arrayLike.location.lat;
            l2 = arrayLike.location.lng;
            return {
              merchantObject: arrayLike.merchant,
              distance: $$geotranslate.haversine(L1, l1, L2, l2)
            };
          });
          $timeout(function() {
            return $scope.$apply(function() {
              return $scope.stampScreenShow = true;
            });
          }, 0);
          return mapService.storedDistancesObject = d;
        });
        return def.promise;
      };
      merchantMarket = mapService.merchantMarket = loadMerchantsByStamp();
      $scope.$watch('merchantLinks.length', function(_merchantLinksData) {
        if (_merchantLinksData && _merchantLinksData > 0) {
          $scope.showNearestMerchant();
          return _.forEach($scope.merchantLinks, function(merchantLink) {
            if (merchantLink.stamps && merchantLink.stamps.local) {
              _.forEach(merchantLink.stamps.local, function(personalizedLocalStamp) {
                if (personalizedLocalStamp.personalIntensityQuota > 0) {
                  $scope.personalIntensityFound = true;
                  return;
                }
              });
              return;
            }
          });
        }
      });
      $scope.$on('$locationChangeStart', function(event, next, current) {
        return $mdBottomSheet.hide();
      });
      calculateDistances = function() {

        /*
         * calculateDistances
        
        Use haversine method to build a list of distances out of merchant market
         */
        var checkDistances;
        checkDistances = $q.defer();
        _getLocation.then(function(data) {
          return merchantMarket.then(function(merchantGeoData) {
            var _data;
            $scope.$broadcast('markers:preview', {
              preview: true
            });
            _data = _.map(merchantGeoData, function(arrayLike) {
              var $idByAddy, L1, L2, l1, l2, _m;
              L1 = data.coords.latitude;
              l1 = data.coords.longitude;
              L2 = arrayLike.location.lat;
              l2 = arrayLike.location.lng;
              $idByAddy = cryptoService.hash([L2, l2].join(','));
              _m = $scope.mapHash[$idByAddy];
              if (_m && _m.options && _m.options.icon) {
                _m.status = _m.options.icon.options.iconUrl.indexOf('inactive') === -1 ? true : false;
              }
              return {
                status: _m.status,
                merchantObject: arrayLike.merchant,
                distance: $$geotranslate.haversine(L1, l1, L2, l2)
              };
            });
            return checkDistances.resolve(_data);
          });
        });
        return checkDistances.promise;
      };
      $scope.loadStampScreen = function() {

        /*
         * loadStampScreen
        
         *# note
        
        Overcomplicated and must clean out scope on hazard of memory issues.
         */
        var handle;
        handle = void 0;
        calculateDistances().then(function(merchantList) {
          var maxDistance, _merchantList;
          maxDistance = 500;
          _merchantList = _.filter(merchantList, function(merchant, index) {
            var m;
            m = merchant.distance * 5280;
            if (m < maxDistance) {
              return m;
            }
          });
          _getLocation.then(function(data) {
            $scope.merchants.$loaded().then(function(merchantData) {
              var expandedMerchantList, filteredMerchantData, ___merchantList, __filteredLocalM, __merchantList, _expandedMerchantList, _filteredLocalM, _localm;
              _localm = [];
              filteredMerchantData = _.filter(merchantData, function(merchant) {
                return merchant.stamps && merchant.stamps.local;
              });
              _.forEach(filteredMerchantData, function(_merchant) {
                return _.forEach(_merchant.stamps.local, function(localStamp) {
                  return _localm.push(localStamp);
                });
              });
              ___merchantList = _.map(_merchantList, function(__m) {
                return __m.merchantObject;
              });
              _filteredLocalM = _.filter(_localm, function(localm) {
                var checked;
                checked = true;
                _.forEach(___merchantList, function(__m) {
                  if (localm.$id === __m.$id) {
                    return checked = false;
                  }
                });
                return checked;
              });
              __filteredLocalM = _.filter(_filteredLocalM, function(m) {
                return m.address;
              });
              expandedMerchantList = _.map(__filteredLocalM, function(_Merchant) {
                var $idByAddy, L1, L2, l1, l2, stampDistance, _m, _stampDistance;
                L1 = data.coords.latitude;
                l1 = data.coords.longitude;
                L2 = _Merchant.address.location.lat;
                l2 = _Merchant.address.location.lng;
                stampDistance = $$geotranslate.haversine(L1, l1, L2, l2);
                _stampDistance = stampDistance * 5280;
                $idByAddy = cryptoService.hash([L2, l2].join(','));
                _m = $scope.mapHash[$idByAddy];
                if (_m && _m.options && _m.options.icon) {
                  _m.status = _m.options.icon.options.iconUrl.indexOf('inactive') === -1 ? true : false;
                }
                if (_stampDistance < maxDistance && _m.status === true) {
                  return {
                    status: _m.status,
                    distance: stampDistance,
                    merchantObject: _Merchant
                  };
                } else {
                  return void 0;
                }
              });
              _expandedMerchantList = _.filter(expandedMerchantList, function(stampDeterminedDistance) {
                return stampDeterminedDistance;
              });
              __merchantList = _merchantList.concat(_expandedMerchantList);
              stampService.localValidatorStamps = __merchantList;
              $location.path('/fan/stamp');
            });
          });
        });
      };
      $scope.nearestMerchantFound = false;
      $scope.nearestMerchantSet = false;
      $scope.showNearestMerchant = function() {

        /*
        Show Nearest Merchant from Nearest Merchant Index.
         */
        var nearestMerchantConstruct;
        nearestMerchantConstruct = $q.all([_getLocation, merchantMarket]);
        return nearestMerchantConstruct.then(function(data) {
          return tttttttt = $timeout(function() {
            var L1, a, arrayLikeMerchants, currentPosition, geoJsonizedMerchants, l1, lat, lon, m, mConstruct, nearestDisplayText, nearestDisplayValue, nearestLabelHtml, nearestMerchantMarker, _mConstruct, _merchantGeoMarket;
            currentPosition = data[0];
            a = data[1];
            if ($scope.changedLatLng) {
              currentPosition = $scope.changedLatLng;
            }
            arrayLikeMerchants = _.map(a, function(arrayLike) {
              return [arrayLike.location.lat, arrayLike.location.lng];
            });
            geoJsonizedMerchants = geoJsonUtils.toGeoJSON(arrayLikeMerchants, 'linestring');
            _merchantGeoMarket = L.geoJson(geoJsonizedMerchants);
            $scope.nearestMerchantIndex = leafletKnn(_merchantGeoMarket).nearest(L.latLng(currentPosition.coords.latitude, currentPosition.coords.longitude), 1);
            if ($scope.nearestMerchantIndex && _.size($scope.nearestMerchantIndex) > 0) {
              m = _.last($scope.nearestMerchantIndex);
              L1 = currentPosition.coords.latitude;
              l1 = currentPosition.coords.longitude;
              lat = m.lat;
              lon = m.lon;
              if (!lat && !lon && !m && !L1 && !l1) {
                $scope.nearestMerchantFound = false;
                return;
              }
              mConstruct = [lat, lon].join(',');
              nearestDisplayValue = $$geotranslate.haversine(L1, l1, lat, lon);
              _mConstruct = cryptoService.hash(mConstruct);
              nearestMerchantMarker = $scope.mapHash[_mConstruct];
              console.log(nearestMerchantMarker);
              nearestDisplayText = numeral(nearestDisplayValue * 0.621371).format('0,0.00');
              nearestLabelHtml = '<span class="~font-effect-outline ~font-effect-3d ~font-effect-3d-float" style="color: #47b7e9; color: #6694c4; color: #ffffff; color: rgba(0, 110, 183, .5); font-weight: 400; margin-left: -1rem; font-size: 1.5rem;">' + nearestDisplayText + ' mi</span>';
              if (!(nearestMerchantMarker.hasOwnershipDupes || nearestMerchantMarker.hasSimpleDupes)) {
                nearestMerchantMarker.bindLabel(nearestLabelHtml, {
                  noHide: true,
                  offset: [-40, -37],
                  className: 'marker-bullseye--' + nearestMerchantMarker.colls
                });
              }
              $scope.nearestMerchantFound = true;
              if ((nearestDisplayValue * 0.621371) > .1) {
                return $scope.nearestMerchantIndex.lenth = 0;
              }
            }
          }, 250);
        });
      };
      $scope.determineCurrentMerchant = function() {
        var determinationConstruct;
        determinationConstruct = $q.all([_getMap, _getLocation]);
        return determinationConstruct.then(function(data) {
          var currentPosition, map;
          map = data[0];
          currentPosition = data[1];
          return ttttttttt = $timeout(function() {
            var nearestMerchantObject, _lat, _lon;
            if (_.size($scope.nearestMerchantIndex) > 0 && $scope.nearestMerchantSet === false) {
              nearestMerchantObject = _.last($scope.nearestMerchantIndex);
              _lat = nearestMerchantObject.lat;
              _lon = nearestMerchantObject.lon;
              map.panTo(new L.LatLng(_lat, _lon));
              return $scope.nearestMerchantSet = true;
            } else {
              map.panTo(new L.latLng(currentPosition.coords.latitude, currentPosition.coords.longitude));
              return $scope.nearestMerchantSet = false;
            }
          }, 250);
        });
      };
      showProductTour = function() {

        /**
         * showProductTour
        
        Product Tour.
        
        @return {undefined}
         */
        var def;
        def = $q.defer();
        $scope.alert = "";
        _userRequire.then(function(user) {
          var _t;
          if (user.handle) {
            _t = $window.is_device ? './scripts/' : environment.baseTemplateUrl;
            $mdBottomSheet.show({
              templateUrl: _t + "modules/index/partials/bottom-sheet-grid-template.html",
              controller: "ProductTourCtrl"
            }).then(function(clickedItem) {
              if (clickedItem) {
                $scope.productTourPageMessage = clickedItem.name;
                $scope.contentClass = clickedItem.contentClass;
              }
            });
          }
        });
      };
      _timeout = null;
      $scope.showGridBottomSheet = function() {

        /*
        @namespace showGridBottomSheet
        @description
        
            A check for product tour routes and status.
        
        @param {object} $event
        @returns {undefined}
         */
        if ($location.$$url.indexOf('show') !== -1) {
          _timeout = $timeout(function() {
            showProductTour();
            return $timeout.cancel(_timeout);
          }, 500);
        }
      };
      _.extend($scope, {
        center: deviceGeoSettings
      });
      $scope.activityLinksDragData = {};
      $scope.stampScreenDragData = {};
      $scope.onRepositionActivityLinksCompleteDrag = function(data, event) {};
      $scope.onRepositionActivityLinksCompleteDrop = function(data, event) {};
      $scope.onRepositionNearedMerchantCompleteDrag = function(data, event) {};
      $scope.onRepositionNearedMerchantCompleteDrop = function(data, event) {};
      $scope.onRepositionStampScreenCompleteDrag = function(data, event) {};
      $scope.onRepositionStampScreenCompleteDrop = function(data, event) {};
      $scope.closeProductTour = function() {
        var profileObject;
        profileObject = {};
        profileObject.productTour = {};
        profileObject.productTour.seen = true;
        _userRequire.then(function(_authData) {
          var user_handle;
          if (_authData) {
            user_handle = _authData.handle;
            return profileService.updateProfile(user_handle, profileObject);
          }
        });
        return _getMap.then(function(_map) {
          var element;
          closeProductTour = $timeout(function() {
            return _map.invalidateSize(false);
          }, 750);
          $mdBottomSheet.hide();
          element = angular.element(document.querySelector('body'));
          element.removeClass('show--product-tour');
          element.removeClass('pt--undefined');
          element.removeClass('pt--hangout');
          element.removeClass('pt--mail');
          element.removeClass('pt--message');
          element.removeClass('pt--copy');
          element.removeClass('pt--facebook');
          element.removeClass('pt--twitter');
          element.removeClass('pt--google');
          if ($window.is_device) {
            return element.addClass('product-tour--closed');
          }
        });
      };
    };
    return moduleController = ['$scope', '$timeout', '$rootScope', 'geolocation', 'leafletData', '$window', '$mdBottomSheet', 'firebase.utils', 'user.require', '$location', '$q', 'stamp.service', 'bank.service', 'geoJsonUtils', '$$geotranslate', 'notification.service', '$stateParams', 'localStorageService', 'map.service', 'center.service', 'crypto.service', 'leaflet.extended', '$http', 'profile.service', 'browser.service', 'geolocationAvailable', IndexController];
  });

}).call(this);

//# sourceMappingURL=base.js.map
