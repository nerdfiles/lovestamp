###
# fileOverview

                            _                       ______ _
                           | |                 _   / _____) |                  _
     ____  _____  ____ ____| |__  _____ ____ _| |_( (____ | |__  _____ _____ _| |_
    |    \| ___ |/ ___) ___)  _ \(____ |  _ (_   _)\____ \|  _ \| ___ | ___ (_   _)
    | | | | ____| |  ( (___| | | / ___ | | | || |_ _____) ) | | | ____| ____| | |_
    |_|_|_|_____)_|   \____)_| |_\_____|_| |_| \__|______/|_| |_|_____)_____)  \__)

## description

Introducing Merchant Bottomsheet.

###

define [
  "interface"
  "lodash"
  "numeral"
  "config"
  "directives/relExternal"
  "angular-geotranslation"
  "services/profile"
  "services/stamp"
  "services/bank"
  "services/browser"
], (__interface__, _, numeral, environment) ->

  MerchantSheetCtrl = ($scope, $mdBottomSheet, $q, profileService, stampService, bankService, $rootScope, $timeout, userRequire, browserService, $window) ->
    ###
    Merchant Bottomsheet Controller.
    ###

    #_userRequire = userRequire()

    $scope.unsheet = () ->
      $mdBottomSheet.hide()

    $scope.openMerchantWebsite = (_websiteLink, _target, _options) ->
      config =
        url     : _websiteLink
        target  : "_blank"
        options : []
      browserService.open(config)
      return

    # Pull merchant sheet data from index contrived $rootScope executions
    merchantSheetData = $scope.merchantSheetData

    $scope.dlat = merchantSheetData.address.location.lat
    $scope.dlon = merchantSheetData.address.location.lng

    $scope.prefix = merchantSheetData.stampOwner.charAt(0)

    urlPrefix =
      '@' : 'https://twitter.com/'
      'ᶠ' : 'https://facebook.com/'
      '✚' : 'https://plus.google.com/'

    $scope.socialPrefixActiveUrl = urlPrefix[$scope.prefix]

    $scope.openMerchantLink = (_target, _options) ->

      geoData = $rootScope.geolocationAvailable

      $scope.locationObject = {}
      $scope.locationObject.address = {}
      $scope.locationObject.address.location = geoData.coords
      $scope.slat = $scope.locationObject.address.location.latitude
      $scope.slon = $scope.locationObject.address.location.longitude

      if $window.is_ios or (navigator.platform.indexOf("iPhone") != -1) or (navigator.platform.indexOf("iPod") != -1) or (navigator.platform.indexOf("iPad") != -1)
        $scope.merchantLink = "maps:" + "//maps.google.com/maps?saddr="+$scope.slat+","+$scope.slon+"&daddr="+$scope.dlat+","+$scope.dlon
      else
        $scope.merchantLink = "http:" + "//maps.google.com/maps?saddr="+$scope.slat+","+$scope.slon+"&daddr="+$scope.dlat+","+$scope.dlon

      config =
        url     : $scope.merchantLink
        target  : "_blank"
        options : []
      browserService.open(config)
      return

    stampService.getMerchantBitsTipped(merchantSheetData).then (tippedObjectDescription) ->
      $scope.merchant = {}
      _tippedObject =
        bitsTipped: if tippedObjectDescription then numeral(tippedObjectDescription).format(bankService.currencyFormat) else 0.00

      _.extend merchantSheetData, _tippedObject

      profileService.getMerchantPictureInView(merchantSheetData).then (imageDataRef) ->

        _imageData =
          generatedMerchantPictureInView: imageDataRef

        _.extend merchantSheetData, _imageData

        merchantHandle = merchantSheetData.stampOwner

        profileService.getMerchantStatus(merchantHandle).$loaded().then (merchantData) ->

          # Pull datapoints regarding merchant profile properties, typically for associating with stamps
          $scope.merchant.merchantProfile = merchantData.profile

          # Pull datapoints regarding merchant overview properties, typically for exposing interactions 
          # directly with merchants and the application Web-based administration backend.
          $scope.merchant.merchantOverview = merchantData.profile.overview

          _.extend $scope.merchant, merchantSheetData

          $timeout ->
            $rootScope.$broadcast 'imageData:loaded', _imageData
          , 750

        return
      return
    return


  MerchantPreview = ($sce, $rootScope, $timeout) ->
    ###
    Merchant Preview Directive.
    ###

    _t = if window.is_device then './scripts/' else environment.baseTemplateUrl
    restrict: 'A'
    templateUrl: _t + 'modules/index/partials/merchant-preview.html'
    scope: merchant: '=merchantConstruct'
    link: linker = ($scope, element, $attr) ->

      $rootScope.$on 'imageData:loaded', (event, imageData) ->

        $timeout ->
          $scope.$apply () ->
            $element = angular.element element.find('div')

            $element.attr 'itemprop', 'image'

            #_i = 'http://placehold.it/150x150&text=Preview unavailable'
            _i = ''

            i = imageData.generatedMerchantPictureInView or _i

            __i = $sce.trustAsResourceUrl i

            cachedStyle = $element.attr 'style'

            updatedCachedStyle = [
              'background-image: url('
              __i
              ')'
            ].join ''

            $element.attr 'style', ( updatedCachedStyle )

            #$element.attr 'src', __i
        , 0

      return


  # Implement Merchant Bottomsheet:
  #
  # 1. Controller (controller for pulling datapoints into Directive)
  # 2. Directive (leaflet accessible directive)

  __interface__

    .controller("MerchantSheetCtrl", [
      "$scope"
      "$mdBottomSheet"
      "$q"
      "profile.service"
      "stamp.service"
      "bank.service"
      "$rootScope"
      "$timeout"
      "user.require"
      "browser.service"
      "$window"
      MerchantSheetCtrl
    ])

    .directive('merchantPreview', [
      '$sce',
      '$rootScope'
      '$timeout'
      MerchantPreview
    ])

  return
