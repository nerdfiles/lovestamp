
/**
@fileOverview

 _____  ____ ____ _____  ___  ___
(____ |/ ___) ___) ___ |/___)/___)
/ ___ ( (__( (___| ____|___ |___ |
\_____|\____)____)_____|___/(___/

                      _
                     | |              _
  ____ ___  ____   __| |_   _  ____ _| |_ ___   ____
 / ___) _ \|  _ \ / _  | | | |/ ___|_   _) _ \ / ___)
( (__| |_| | | | ( (_| | |_| ( (___  | || |_| | |
 \____)___/|_| |_|\____|____/ \____)  \__)___/|_|

@module authorization
@description

  For authorization of microview level elements. Essentially this is a wrapper 
  directive which should authorize subelements to pages. It functions like a 
  CSRF token set in the <head> of a document, which might be extracted from 
  a <body> level AJAX-ified form; however, the backend might supply n-tokens 
  for any page-level request.

@usage

  <div access-conductor
    csrf-token="token"
  ></div>
 */

(function() {
  define(["interface"], function(__interface__) {
    var accessConductor, deps;
    accessConductor = function($timeout, $log) {
      return {
        restrict: "A",
        scope: false,
        link: function($scope, element, attr) {}
      };
    };
    deps = ["$timeout", "$log", accessConductor];
    __interface__.directive("accessConductor", deps);
  });

}).call(this);

//# sourceMappingURL=accessConductor.js.map
