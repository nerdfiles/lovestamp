define ['interface'], (__interface__) ->
  hoverClass = () ->
    restrict: 'A'
    scope: hoverClass: '@'
    link: (scope, element) ->
      element.on 'mouseenter', ->
        element.addClass scope.hoverClass
        return
      element.on 'mouseleave', ->
        element.removeClass scope.hoverClass
        return
      return

  __interface__.directive 'hoverClass', [
    hoverClass
  ]
  return
