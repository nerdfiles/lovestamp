# Application-Wide Directives

    define([
      'angularAMD'
    ], function (angularAMD) {
      angularAMD.directive('directiveName', [
        function () {
          return {
            restrict: 'A',
            link: function linker ($scope) {
            }
            // etc...
          };
        }
      ]);
    });
