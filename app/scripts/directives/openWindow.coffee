define ['interface'], (__interface__) ->
  __interface__.directive 'openWindow', [
    '$log'
    '$mdToast'
    '$compile'
    ($log, $mdToast, $compile) ->
      {
        restrict: 'A'
        scope:
          label : '@openWindowHref'
          uri   : '@openWindowUri'
        link: ($scope, element, $attrs) ->
          absent = -1

          if $scope.uri.indexOf('fan-view') isnt absent
            element.on 'click', (e) ->
              element.toggleClass 'md-toast-open-bottom'
              sep = '/'
              siteUrl = '.' + '/' + $scope.uri
              if e.target.className.indexOf('nav-label') isnt absent and element.hasClass('md-toast-open-bottom')
                  e.preventDefault()
                  $scope.openWindow siteUrl
              else
                  e.preventDefault()
                  $mdToast.hide()

              return

          responses =
            iphone6: [
              'width=375'
              'height=677'
            ]
            iphone5: [
              'width=320'
              'height=568'
            ]
            galaxys6: [
              'width=340'
              'height=640'
            ]


          $scope.openWindow = ($windowUrl) ->
            ###
            ###

            responsesMenu = [
              '<span onClick="window.open(\'' + $windowUrl + '\',\'' + $scope.label + '\',\'' + responses['iphone5'].join(',') + '\')">iPhone5</span>'
              '<span onClick="window.open(\'' + $windowUrl + '\',\'' + $scope.label + '\',\'' + responses['iphone6'].join(',') + '\')">iPhone6</span>'
              '<span onClick="window.open(\'' + $windowUrl + '\',\'' + $scope.label + '\',\'' + responses['galaxys6'].join(',') + '\')">Galaxy S4-6/Nexus 5</span>'
            ].join ''

            # @TODO In case of iOS/Android issues.
            #a.setAttribute("href", this.getAttribute("data-href"))
            #a.setAttribute("target", "_blank")
            #dispatch = document.createEvent("HTMLEvents")
            #dispatch.initEvent("click", true, true)
            #a.dispatchEvent(dispatch)

            _tmpl = '<div class="fan-view--menu">' + responsesMenu + '</div>'
            $mdToast.show(
              position  : 'fit'
              template  : _tmpl
              hideDelay : 0
              parent: element
            )
            return

          return

      }
  ]
