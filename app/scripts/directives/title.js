(function() {
  define(["interface", "config"], function(__interface__, environment) {
    var optimizedPageTitle;
    optimizedPageTitle = function($rootScope, $timeout) {
      var linker, sep, _sitePrefix;
      _sitePrefix = 'LoveStamp';
      sep = '›';
      linker = function($scope, element) {
        var nameChanger;
        nameChanger = function() {
          var body$, bodyClassList, pageName, pageNameConstructList, polishedName, title$, _formattedPage, _formattedSubtypeTitle, _nameConstruct, _pageName, _polishedName;
          title$ = document.querySelector('title');
          body$ = document.querySelector('body');
          bodyClassList = body$.classList;
          pageName = bodyClassList[4];
          pageNameConstructList = pageName.split('--');
          _pageName = pageNameConstructList.join(' ');
          _nameConstruct = [];
          _nameConstruct.push(_sitePrefix);
          _formattedSubtypeTitle = environment.isSubtype.charAt(0).toUpperCase() + environment.isSubtype.slice(1);
          _nameConstruct.push(_formattedSubtypeTitle);
          _nameConstruct.push(sep);
          if (!$scope.windowTitle) {
            _formattedPage = _pageName.charAt(0).toUpperCase() + _pageName.slice(1);
          } else {
            _formattedPage = $scope.windowTitle;
          }
          _nameConstruct.push(_formattedPage);
          polishedName = _nameConstruct.join(' ');
          _polishedName = polishedName.replace('index', 'Home');
          return document.title = _polishedName;
        };
        nameChanger();
        $rootScope.$on('$locationChangeSuccess', function() {
          return nameChanger();
        });
      };
      return {
        restrict: "A",
        link: linker,
        scope: false
      };
    };
    __interface__.directive("optimizedPageTitle", ['$rootScope', '$timeout', optimizedPageTitle]);
  });

}).call(this);

//# sourceMappingURL=title.js.map
