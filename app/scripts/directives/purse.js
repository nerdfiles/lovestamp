
/*
 * fileOverview

     ____  _   _  ____ ___ _____
    |  _ \| | | |/ ___)___) ___ |
    | |_| | |_| | |  |___ | ____|
    |  __/|____/|_|  (___/|_____)
    |_|

 *# description
 */

(function() {
  define(['interface', 'lodash', 'services/stamp', 'services/authentication'], function(__interface__, _) {
    var pendingPoolPurse;
    pendingPoolPurse = function(stampService, userRequire) {
      var linker;
      linker = function($scope, element) {
        var _userRequire;
        _userRequire = userRequire();
      };
      return {
        restrict: 'A',
        controllerAs: 'StampSuccessController',
        link: linker
      };
    };
    return __interface__.directive('pendingPoolPurse', ['stamp.service', 'user.require', pendingPoolPurse]);
  });

}).call(this);

//# sourceMappingURL=purse.js.map
