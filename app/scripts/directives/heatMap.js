
/**
@fileOverview

 _                       _______
| |                  _  (_______)
| |__  _____ _____ _| |_ _  _  _ _____ ____
|  _ \| ___ (____ (_   _) ||_|| (____ |  _ \
| | | | ____/ ___ | | |_| |   | / ___ | |_| |
|_| |_|_____)_____|  \__)_|   |_\_____|  __/

@note Perhaps a simple correspondent graph to Dashboard D3 recursive graphs.
@description

    Heat Map/Histrograph Linearization Reductions (Bar Graphs)
 */

(function() {
  define(['interface', 'angular', 'jquery', 'config', 'utils/dataDefender'], function(__interface, angular, $, environment) {

    /**
     * Link Content
     *
     * This directive can represent content as both a link to the content
     * (metadata) and the content itself.
     */
    __interface.register.directive('heatMap', [
      'dataDefender', '$timeout', function(dataDefender, $timeout) {

        /*
        @expects scale
         */
        var linker;
        linker = function($scope, $element, $attrs) {
          var firstScaleElementWidth, height;
          $scope.scale = dataDefender.isEmpty($attrs.scale) ? [
            {
              min: 0,
              max: 0.99,
              color: '#c75356'
            }, {
              min: 1,
              max: 1.99,
              color: '#e48846'
            }, {
              min: 2,
              max: 2.99,
              color: '#fecb48'
            }, {
              min: 3,
              max: 3.99,
              color: '#99c245'
            }, {
              min: 4,
              max: 4.99,
              color: '#62a455'
            }
          ] : angular.fromJson($attrs.scale);
          height = $($element).height();
          firstScaleElementWidth = 0;
          angular.forEach($scope.scale, function(scaleElement, i) {
            scaleElement.width = scaleElement.width || $($element).width() / $scope.scale.length;
            if (i === 0) {
              firstScaleElementWidth = scaleElement.width;
            }
            scaleElement.borderRadius = {
              topLeft: dataDefender.isEmpty(scaleElement.borderRadius) ? (i === 0 ? height / 2 : 0) : scaleElement.borderRadius.topLeft || 0,
              bottomLeft: dataDefender.isEmpty(scaleElement.borderRadius) ? (i === 0 ? height / 2 : 0) : scaleElement.borderRadius.bottomLeft || 0,
              topRight: dataDefender.isEmpty(scaleElement.borderRadius) ? (i === $scope.scale.length - 1 ? height / 2 : 0) : scaleElement.borderRadius.topRight || 0,
              bottomRight: dataDefender.isEmpty(scaleElement.borderRadius) ? (i === $scope.scale.length - 1 ? height / 2 : 0) : scaleElement.borderRadius.bottomRight || 0
            };
          });
          $($element).find('.icon').css('left', -(firstScaleElementWidth / 2));
          $timeout((function() {
            $($element).find('.icon').css('transition', 'left 1s ease-out');
            $($element).find('.icon').css('left', firstScaleElementWidth * parseFloat($scope.value) - 15);
          }), 250);
        };
        return {
          link: linker,
          restrict: 'A',
          templateUrl: environment.baseTemplateUrl + 'partials/heat-map.html',
          scope: {
            value: '=?'
          }
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=heatMap.js.map
