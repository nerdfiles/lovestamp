(function() {
  define(["interface"], function(__interface__) {
    var drtvName;
    drtvName = "relExternal";

    /**
    @module directives
    
    Semantic External Links
     */
    __interface__.directive(drtvName, [
      function() {
        var linker;
        linker = function($scope, element) {
          element.prop("target", "_blank");
          element.prop("rel", "external");
        };
        return {
          restrict: "A",
          link: linker
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=relExternal.js.map
