###
# fileOverview

                      _     _                 _
                     (_)   (_)               (_)
     _____ ____  ____ _     _ _____  ____ ___ _  ___  ____
    (____ |  _ \|  _ \ |   | | ___ |/ ___)___) |/ _ \|  _ \
    / ___ | |_| | |_| \ \ / /| ____| |  |___ | | |_| | | | |
    \_____|  __/|  __/ \___/ |_____)_|  (___/|_|\___/|_| |_|
          |_|   |_|

## description

A simple application version display.

@usage

    <div class="version-container">
        <span version></span>
    </div>

###

define [
    "interface"
], (__interface__) ->

  appVersion = ($http) ->
    ###
    Application version display directive.
    ###

    linker = ($scope, element) ->

      if ! window.is_device
        $http.
          get('/assets/package.json').
          success((pkg) ->

            version_prefix = 'v'
            version = pkg.version

            element.text(version_prefix + version)

            return
          )

      return

    restrict : 'A'
    link     : linker

  __interface__.
    directive "appVersion", [
      '$http'
      appVersion
    ]

  return
