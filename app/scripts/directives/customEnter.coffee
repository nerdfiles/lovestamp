###
@fileOverview
@description

    A custom enter key directive.

###
define [
    "interface"
], (__interface__) ->

  ###*
  Enter Key Directive
  ###
  __interface__.directive "customEnter", [->
    linker = ($scope, element, attrs) ->
      element.bind "keyup", (e) ->
        if e.which is 13
            $scope.$apply ->
            $scope.$eval attrs.customEnter
            return

        e.preventDefault()
        return
      return

    return (
        link: linker
        restrict: "A"
    )
  ]
  return

