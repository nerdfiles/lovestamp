
/*
 * fileOverview

                 _     _ _        _       _       _
                | |   (_) |      (_)     (_)     | |
     ____   ___ | |__  _| | _____ _       _ ____ | |  _
    |    \ / _ \|  _ \| | || ___ | |     | |  _ \| |_/ )
    | | | | |_| | |_) ) | || ____| |_____| | | | |  _ (
    |_|_|_|\___/|____/|_|\_)_____)_______)_|_| |_|_| \_)

@ngdoc

 *# description

An defensive ngClick directive primarily for Mobile Chrome devices.
 */

(function() {
  define(['interface'], function(__interface__) {
    var is_mobile_chrome, mobileLink;
    is_mobile_chrome = navigator.userAgent.match('CriOS') ? true : false;
    mobileLink = function($timeout) {
      var postLink;
      return {
        restrict: 'A',
        link: postLink = function($scope, element, attrs) {
          if (is_mobile_chrome && typeof attrs.ngClick !== 'undefined') {
            element.on('touchend', function(e) {
              return element[0].click();
            });
          }
        }
      };
    };
    __interface__.directive('mobileLink', ['$timeout', mobileLink]);
  });

}).call(this);

//# sourceMappingURL=mobileLink.js.map
