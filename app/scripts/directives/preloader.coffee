###
@fileOverflow

                       _                 _
                      | |               | |
     ____   ____ _____| | ___  _____  __| |_____  ____
    |  _ \ / ___) ___ | |/ _ \(____ |/ _  | ___ |/ ___)
    | |_| | |   | ____| | |_| / ___ ( (_| | ____| |
    |  __/|_|   |_____)\_)___/\_____|\____|_____)_|
    |_|

@description

    Appends a nested block to the <body> with "cached" elements of a parent-child relationship (.className > img), then throws them away on location change.

@example

    <div
        preloader preloader-image-set="[

          [ '.pt--hangout md-backdrop', 'background-image: url('/images/pt/product-tour-slice-1.png');' ],

          [ '.pt--mail md-backdrop', 'background-image: url('/images/pt/product-tour-slice-2.png');' ],

          [ '.pt--message md-backdrop', 'background-image: url('/images/pt/product-tour-slice-3.png');' ],

          [ '.pt--copy md-backdrop', 'background-image: url('/images/pt/product-tour-slice-4.png');' ],

          [ '.pt--facebook md-backdrop', 'background-image: url('/images/pt/product-tour-slice-5.png');' ],

          [ '.pt--twitter md-backdrop', 'background-image: url('/images/pt/product-tour-slice-6.png');' ],

          [ '.pt--google md-backdrop', 'background-image: url('/images/pt/product-tour-slice-7.png');' ]

        ]" class="image-gallery"
    ></div>

###

define [
    'interface'
    'lodash'
], (__interface__, _) ->

    __interface__.
        directive 'preloader', [
            '$http'
            '$compile'
            '$timeout'
            '$q'
            '$location'

            # Register the directive to AngularJS's $compileProvider on-demand or within 
            # R.js build schema.
            ($http, $compile, $timeout, $q, $location) ->
                restrict: 'A'
                link: ($scope, element, $attrs) ->
                    preloaderImageSet = JSON.parse $attrs.preloaderImageSet.replace(/&quot;/, '"', 'g')
                    length = preloaderImageSet.length

                    #def = $q.defer()

                    ###

                    @depends angular-material
                    @example [
                        {
                            '.pt-namedItem md-backdrop': 'background-image: url(/images/...png);'
                        },
                        {
                            '.pt-namedItem md-backdrop': 'background-image: url(/images/...png);'
                        },
                        {
                            '.pt-namedItem md-backdrop': 'background-image: url(/images/...png);'
                        }
                    ]

                    ###

                    loadedElementSet = []
                    loadedImageSet = []
                    cleanupList = []

                    if $location.path().indexOf('product-tour') isnt -1

                        _.forEach preloaderImageSet, (keyNameElementRangeList, imageRangeList) ->
                            loadedElementSet.push keyNameElementRangeList
                            loadedImageSet.push _imageObject = imageRangeList

                        preloaderList = _.map loadedElementSet, (ls) ->
                            _preloaderList = [ ls[0], ls[1] ]

                        $body = document.querySelector 'body'
                        pseudoParentContainer = null

                        _.forEach preloaderList, (cachedObject) ->

                            ###
                            Hoist like a mothertrucker.
                            ###
                            elementList = cachedObject[0].split ' '

                            parentElement = elementList[0]
                            childElement = elementList[1]

                            ###
                            Hokey pokey throwing elements about...
                            ###
                            _.forEach elementList, (_elementType) ->

                                if _elementType.indexOf('.') isnt -1
                                    pseudoParentContainer = '<div class="'+ parentElement.replace(/\./, '', 'g') + '"></div>'
                                else
                                    _pseudoDirectiveContainer = '<' + childElement + '></' + childElement + '>'

                                    pseudoDirectiveContainer = angular.
                                        element(_pseudoDirectiveContainer).
                                        wrap pseudoParentContainer

                                    _pseudoDirectiveContainer = $compile(pseudoDirectiveContainer)($scope)

                                    a = angular.element(pseudoParentContainer).append _pseudoDirectiveContainer

                                    cleanupList.push a

                                    angular.element($body).append a

                        ###
                        Dislodge preloaded <div>s from DOM.
                        ###
                        $scope.$on '$locationChangeStart', () ->
                            body$ = angular.element(document.querySelector('body'))
                            for i in cleanupList
                                angular.element(i).remove()

                    return
        ]
    return
