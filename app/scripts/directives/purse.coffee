###
# fileOverview

     ____  _   _  ____ ___ _____
    |  _ \| | | |/ ___)___) ___ |
    | |_| | |_| | |  |___ | ____|
    |  __/|____/|_|  (___/|_____)
    |_|

## description

###

define [
  'interface'
  'lodash'
  'services/stamp'
  'services/authentication'
], (__interface__, _) ->

    pendingPoolPurse = (stampService, userRequire) ->

      linker = ($scope, element) ->

        _userRequire = userRequire()

        return

      restrict: 'A'
      controllerAs: 'StampSuccessController'
      link: linker

    __interface__.directive 'pendingPoolPurse', [
      'stamp.service'
      'user.require'
      pendingPoolPurse
    ]
