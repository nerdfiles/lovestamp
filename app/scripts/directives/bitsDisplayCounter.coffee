define [
    "interface"
    "jquery"
    "jquery.easing"
], (__interface__, $) ->

    bitsDisplayCounter = ($rootScope) ->
        restrict: "A"
        link: ($scope, element) ->

            $scope.$on 'bitsCollectedValue:loaded', (event, bitsCollectedValue) ->

                $el = $ element

                $scope.value = parseFloat bitsCollectedValue.val

                $(percentage: 0).stop(true).animate(percentage: $scope.value,
                    duration : 2000,
                    easing: "easeOutExpo",
                    step: () ->
                        _percentage = Math.round(this.percentage * 10) / 10
                        $el.text(_percentage)

                ).promise().done(() ->
                    $el.text($scope.value)
                )

            return

    __interface__
        .directive "bitsDisplayCounter", [
            '$rootScope'
            bitsDisplayCounter
         ]
    return
