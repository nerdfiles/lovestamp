
/*
@fileOverflow

                       _                 _
                      | |               | |
     ____   ____ _____| | ___  _____  __| |_____  ____
    |  _ \ / ___) ___ | |/ _ \(____ |/ _  | ___ |/ ___)
    | |_| | |   | ____| | |_| / ___ ( (_| | ____| |
    |  __/|_|   |_____)\_)___/\_____|\____|_____)_|
    |_|

@description

    Appends a nested block to the <body> with "cached" elements of a parent-child relationship (.className > img), then throws them away on location change.

@example

    <div
        preloader preloader-image-set="[

          [ '.pt--hangout md-backdrop', 'background-image: url('/images/pt/product-tour-slice-1.png');' ],

          [ '.pt--mail md-backdrop', 'background-image: url('/images/pt/product-tour-slice-2.png');' ],

          [ '.pt--message md-backdrop', 'background-image: url('/images/pt/product-tour-slice-3.png');' ],

          [ '.pt--copy md-backdrop', 'background-image: url('/images/pt/product-tour-slice-4.png');' ],

          [ '.pt--facebook md-backdrop', 'background-image: url('/images/pt/product-tour-slice-5.png');' ],

          [ '.pt--twitter md-backdrop', 'background-image: url('/images/pt/product-tour-slice-6.png');' ],

          [ '.pt--google md-backdrop', 'background-image: url('/images/pt/product-tour-slice-7.png');' ]

        ]" class="image-gallery"
    ></div>
 */

(function() {
  define(['interface', 'lodash'], function(__interface__, _) {
    __interface__.directive('preloader', [
      '$http', '$compile', '$timeout', '$q', '$location', function($http, $compile, $timeout, $q, $location) {
        return {
          restrict: 'A',
          link: function($scope, element, $attrs) {
            var $body, cleanupList, length, loadedElementSet, loadedImageSet, preloaderImageSet, preloaderList, pseudoParentContainer;
            preloaderImageSet = JSON.parse($attrs.preloaderImageSet.replace(/&quot;/, '"', 'g'));
            length = preloaderImageSet.length;

            /*
            
            @depends angular-material
            @example [
                {
                    '.pt-namedItem md-backdrop': 'background-image: url(/images/...png);'
                },
                {
                    '.pt-namedItem md-backdrop': 'background-image: url(/images/...png);'
                },
                {
                    '.pt-namedItem md-backdrop': 'background-image: url(/images/...png);'
                }
            ]
             */
            loadedElementSet = [];
            loadedImageSet = [];
            cleanupList = [];
            if ($location.path().indexOf('product-tour') !== -1) {
              _.forEach(preloaderImageSet, function(keyNameElementRangeList, imageRangeList) {
                var _imageObject;
                loadedElementSet.push(keyNameElementRangeList);
                return loadedImageSet.push(_imageObject = imageRangeList);
              });
              preloaderList = _.map(loadedElementSet, function(ls) {
                var _preloaderList;
                return _preloaderList = [ls[0], ls[1]];
              });
              $body = document.querySelector('body');
              pseudoParentContainer = null;
              _.forEach(preloaderList, function(cachedObject) {

                /*
                Hoist like a mothertrucker.
                 */
                var childElement, elementList, parentElement;
                elementList = cachedObject[0].split(' ');
                parentElement = elementList[0];
                childElement = elementList[1];

                /*
                Hokey pokey throwing elements about...
                 */
                return _.forEach(elementList, function(_elementType) {
                  var a, pseudoDirectiveContainer, _pseudoDirectiveContainer;
                  if (_elementType.indexOf('.') !== -1) {
                    return pseudoParentContainer = '<div class="' + parentElement.replace(/\./, '', 'g') + '"></div>';
                  } else {
                    _pseudoDirectiveContainer = '<' + childElement + '></' + childElement + '>';
                    pseudoDirectiveContainer = angular.element(_pseudoDirectiveContainer).wrap(pseudoParentContainer);
                    _pseudoDirectiveContainer = $compile(pseudoDirectiveContainer)($scope);
                    a = angular.element(pseudoParentContainer).append(_pseudoDirectiveContainer);
                    cleanupList.push(a);
                    return angular.element($body).append(a);
                  }
                });
              });

              /*
              Dislodge preloaded <div>s from DOM.
               */
              $scope.$on('$locationChangeStart', function() {
                var body$, i, _i, _len, _results;
                body$ = angular.element(document.querySelector('body'));
                _results = [];
                for (_i = 0, _len = cleanupList.length; _i < _len; _i++) {
                  i = cleanupList[_i];
                  _results.push(angular.element(i).remove());
                }
                return _results;
              });
            }
          }
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=preloader.js.map
