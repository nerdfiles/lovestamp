###
# fileOverview

                      _    ___ _                   _
                  _  (_)  / __|_)              _  (_)
     ____   ___ _| |_ _ _| |__ _  ____ _____ _| |_ _  ___  ____
    |  _ \ / _ (_   _) (_   __) |/ ___|____ (_   _) |/ _ \|  _ \
    | | | | |_| || |_| | | |  | ( (___/ ___ | | |_| | |_| | | | |
    |_| |_|\___/  \__)_| |_|  |_|\____)_____|  \__)_|\___/|_| |_|

@module directives  

## description

Error messaging and remote-debugging directive.

###

define [
    "interface"
    "config"
    "services/notification"
], (__interface__, environment) ->


    notificationCtrl = ($scope, $mdToast) ->
        ###
        Notification Directive Controller.
        ###

        $scope.closeNotification = () ->
            $mdToast.hide()
            return

        return


    notificationDirective = (notificationService) ->
        ###
        Notification Directive displays current status from notification service.
        ###

        linker = ($scope) ->
            ### Set current status from notification service. ###

            $scope.currentStatus = notificationService.currentStatus
            return
        _t = if window.is_device then './scripts/' else (environment.baseTemplateUrl)

        return {
            controller  : 'notification.controller'
            link        : linker
            restrict    : "A"
            scope       : currentStatus: '=currentStatus'
            templateUrl : _t + "partials/notification.html"
        }


    __interface__

        .controller('notification.controller', [
            '$scope'
            '$mdToast'
            notificationCtrl
        ])

        .directive('notification', [
            'notification.service'
            notificationDirective
        ])

    return
