###
@fileOverview

@description

    A custom loader.

###
define [
  "interface"
  "config"
], (__interface__, environment) ->

  __interface__.directive "loader", [->

    ###*
    Linker

    @param {object} $scope
    @param {object} element
    @param {object} attrs
    ###
    linker = ($scope) ->
      scope.isLoading = true
      return

      t = if window.is_device then './scripts/' else environment.baseTemplateUrl

    return (
      link: linker
      restrict: "A"
      templateUrl: _t + "modules/www/controllers/login/partials/loader.html"
    )
  ]
  return
