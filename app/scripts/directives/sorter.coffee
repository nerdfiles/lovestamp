### global
   define: true
###

###
  Expects a parameter "sort-data" set to an array of companies or handle.
  (defaults to behaving like it's an array of companies)
 
  Also expects a "selected-sort-type" (currently we're initializing this with
  an undefined variable) that gets set when you select a sort type.
 
  @example
 
  <div data-sorter
        class="dropdown sorter"
        data-ng-show="pageSubType == 'list'"
        sort-data="companies"
        selected-sort-type="selectedType"
  ></div>

###

define [
  'interface'
  'angular'
  'config'
  'angular-material'
], (__interface, angular, environment) ->

  __interface.register.directive 'sorter', [

    # Register the directive to AngularJS's $compileProvider on-demand or within 
    # R.js build schema.
    () ->

        linker = ($scope, $element, $attrs) ->
          $scope.sortTypes = angular.fromJson($attrs.sortTypes)
          if $scope.sortState != undefined
            angular.forEach $scope.sortTypes, (sortType) ->
              if sortType.sortBy == $scope.sortState.sortColumn and sortType.sortOrder == 'asc' == $scope.sortState.ascendingSort
                $scope.sortState.setSortColumnLabel sortType.name
                return false
              return
          $scope.$watchCollection 'sortData', (sortData) ->
            if sortData != undefined
              options = {}

              ###*
              # Set Sort Types
              ###

              # handle sort
              if sortData.length > 0 and sortData[0].hasOwnProperty('isLoggedIn')
                handleSortTypes = $attrs.sortTypes or [
                  {
                    name: 'Social (A-Z)'
                    sortBy: 'handle'
                    sortOrder: 'asc'
                  }
                  {
                    name: 'Social (Z-A)'
                    sortBy: 'handle'
                    sortOrder: 'desc'
                  }
                  {
                    name: 'Distance'
                    sortBy: 'distance'
                    sortOrder: 'asc'
                  }
                  {
                    name: 'Company Name (A-Z)'
                    sortBy: 'companyName'
                    sortOrder: 'asc'
                  }
                  {
                    name: 'Company Name (Z-A)'
                    sortBy: 'companyName'
                    sortOrder: 'desc'
                  }
                ]

                options.propertyFindFunction = (sortDataObject, col) ->
                  sortDataObject.personSearchInfo[col]

                $scope.sortTypes = handleSortTypes
              # Sort entities by <select> loaded from sortType model.

              $scope.sortThis = (sortType) ->
                if sortType != undefined
                  # We're not currently messing with itemsInPage here,
                  # so allow it to persist from the default or however else
                  # the parent sets it.
                  $scope.sortState = angular.extend($scope.sortState,
                    pageNumber: 0
                    sortColumn: sortType.sortBy
                    ascendingSort: sortType.sortOrder == 'asc')
                  $scope.sortCallback $scope.sortState
                  $scope.sortState.setSortColumnLabel sortType.name
                return

            return
          return

        {
          scope:
            sortData: '='
            sortCallback: '='
            sortState: '='
          restrict: 'A'
          templateUrl: environment.baseTemplateUrl + 'partials/sorter.html'
          link: linker
        }
 ]
  return
