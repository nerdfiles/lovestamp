define [
  "interface"
], (__interface__) ->
  drtvName = "relExternal"

  ###*
  @module directives

  Semantic External Links
  ###
  __interface__.directive drtvName, [

    # Register the directive to AngularJS's $compileProvider on-demand or within 
    # R.js build schema.
    () ->

      linker = ($scope, element) ->
        element.prop "target", "_blank"
        element.prop "rel", "external"
        return

      return (
        restrict: "A"
        link: linker
      )
  ]
  return
