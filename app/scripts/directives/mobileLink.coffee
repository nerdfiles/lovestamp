###
# fileOverview

                 _     _ _        _       _       _
                | |   (_) |      (_)     (_)     | |
     ____   ___ | |__  _| | _____ _       _ ____ | |  _
    |    \ / _ \|  _ \| | || ___ | |     | |  _ \| |_/ )
    | | | | |_| | |_) ) | || ____| |_____| | | | |  _ (
    |_|_|_|\___/|____/|_|\_)_____)_______)_|_| |_|_| \_)

@ngdoc

## description

An defensive ngClick directive primarily for Mobile Chrome devices.

###

define [
  'interface'
], (__interface__) ->

  is_mobile_chrome = if navigator.userAgent.match('CriOS') then true else false

  mobileLink = ($timeout) ->
    restrict : 'A'
    link     : postLink = ($scope, element, attrs) ->
      if is_mobile_chrome and typeof attrs.ngClick != 'undefined'
        element.on 'touchend', (e) ->
          element[0].click()
      return

  __interface__.
  directive 'mobileLink', [
    '$timeout'
    mobileLink
  ]
  return
