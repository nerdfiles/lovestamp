define [
  "interface"
  "config"
], (__interface__, environment) ->

  optimizedPageTitle = ($rootScope, $timeout) ->

    _sitePrefix = 'LoveStamp'
    sep = '›'

    linker = ($scope, element) ->
      nameChanger = () ->

        title$ = document.querySelector 'title'

        body$ = document.querySelector 'body'
        bodyClassList = body$.classList

        pageName = bodyClassList[4]

        pageNameConstructList = pageName.split('--')
        _pageName = pageNameConstructList.join(' ')

        _nameConstruct = []

        _nameConstruct.push _sitePrefix
        _formattedSubtypeTitle = (environment.isSubtype.charAt(0).toUpperCase() + environment.isSubtype.slice(1))
        _nameConstruct.push _formattedSubtypeTitle
        _nameConstruct.push sep
        if !$scope.windowTitle
          _formattedPage = _pageName.charAt(0).toUpperCase() + _pageName.slice(1)
        else
          _formattedPage = $scope.windowTitle
        _nameConstruct.push _formattedPage

        polishedName = _nameConstruct.join ' '

        _polishedName = polishedName.replace('index', 'Home')

        document.title = _polishedName

      nameChanger()

      $rootScope.$on '$locationChangeSuccess', () ->
        nameChanger()

      return

    restrict : "A"
    link     : linker
    scope    : false

  __interface__.
    directive "optimizedPageTitle", [
      '$rootScope'
      '$timeout'
      optimizedPageTitle
    ]

  return
