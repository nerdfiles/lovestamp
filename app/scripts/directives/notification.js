
/*
 * fileOverview

                      _    ___ _                   _
                  _  (_)  / __|_)              _  (_)
     ____   ___ _| |_ _ _| |__ _  ____ _____ _| |_ _  ___  ____
    |  _ \ / _ (_   _) (_   __) |/ ___|____ (_   _) |/ _ \|  _ \
    | | | | |_| || |_| | | |  | ( (___/ ___ | | |_| | |_| | | | |
    |_| |_|\___/  \__)_| |_|  |_|\____)_____|  \__)_|\___/|_| |_|

@module directives  

 *# description

Error messaging and remote-debugging directive.
 */

(function() {
  define(["interface", "config", "services/notification"], function(__interface__, environment) {
    var notificationCtrl, notificationDirective;
    notificationCtrl = function($scope, $mdToast) {

      /*
      Notification Directive Controller.
       */
      $scope.closeNotification = function() {
        $mdToast.hide();
      };
    };
    notificationDirective = function(notificationService) {

      /*
      Notification Directive displays current status from notification service.
       */
      var linker, _t;
      linker = function($scope) {

        /* Set current status from notification service. */
        $scope.currentStatus = notificationService.currentStatus;
      };
      _t = window.is_device ? './scripts/' : environment.baseTemplateUrl;
      return {
        controller: 'notification.controller',
        link: linker,
        restrict: "A",
        scope: {
          currentStatus: '=currentStatus'
        },
        templateUrl: _t + "partials/notification.html"
      };
    };
    __interface__.controller('notification.controller', ['$scope', '$mdToast', notificationCtrl]).directive('notification', ['notification.service', notificationDirective]);
  });

}).call(this);

//# sourceMappingURL=notification.js.map
