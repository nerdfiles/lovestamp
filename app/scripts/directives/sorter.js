
/* global
   define: true
 */


/*
  Expects a parameter "sort-data" set to an array of companies or handle.
  (defaults to behaving like it's an array of companies)
 
  Also expects a "selected-sort-type" (currently we're initializing this with
  an undefined variable) that gets set when you select a sort type.
 
  @example
 
  <div data-sorter
        class="dropdown sorter"
        data-ng-show="pageSubType == 'list'"
        sort-data="companies"
        selected-sort-type="selectedType"
  ></div>
 */

(function() {
  define(['interface', 'angular', 'config', 'angular-material'], function(__interface, angular, environment) {
    __interface.register.directive('sorter', [
      function() {
        var linker;
        linker = function($scope, $element, $attrs) {
          $scope.sortTypes = angular.fromJson($attrs.sortTypes);
          if ($scope.sortState !== void 0) {
            angular.forEach($scope.sortTypes, function(sortType) {
              if (sortType.sortBy === $scope.sortState.sortColumn && (sortType.sortOrder === 'asc' && 'asc' === $scope.sortState.ascendingSort)) {
                $scope.sortState.setSortColumnLabel(sortType.name);
                return false;
              }
            });
          }
          $scope.$watchCollection('sortData', function(sortData) {
            var handleSortTypes, options;
            if (sortData !== void 0) {
              options = {};

              /**
               * Set Sort Types
               */
              if (sortData.length > 0 && sortData[0].hasOwnProperty('isLoggedIn')) {
                handleSortTypes = $attrs.sortTypes || [
                  {
                    name: 'Social (A-Z)',
                    sortBy: 'handle',
                    sortOrder: 'asc'
                  }, {
                    name: 'Social (Z-A)',
                    sortBy: 'handle',
                    sortOrder: 'desc'
                  }, {
                    name: 'Distance',
                    sortBy: 'distance',
                    sortOrder: 'asc'
                  }, {
                    name: 'Company Name (A-Z)',
                    sortBy: 'companyName',
                    sortOrder: 'asc'
                  }, {
                    name: 'Company Name (Z-A)',
                    sortBy: 'companyName',
                    sortOrder: 'desc'
                  }
                ];
                options.propertyFindFunction = function(sortDataObject, col) {
                  return sortDataObject.personSearchInfo[col];
                };
                $scope.sortTypes = handleSortTypes;
              }
              $scope.sortThis = function(sortType) {
                if (sortType !== void 0) {
                  $scope.sortState = angular.extend($scope.sortState, {
                    pageNumber: 0,
                    sortColumn: sortType.sortBy,
                    ascendingSort: sortType.sortOrder === 'asc'
                  });
                  $scope.sortCallback($scope.sortState);
                  $scope.sortState.setSortColumnLabel(sortType.name);
                }
              };
            }
          });
        };
        return {
          scope: {
            sortData: '=',
            sortCallback: '=',
            sortState: '='
          },
          restrict: 'A',
          templateUrl: environment.baseTemplateUrl + 'partials/sorter.html',
          link: linker
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=sorter.js.map
