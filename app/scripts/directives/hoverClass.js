(function() {
  define(['interface'], function(__interface__) {
    var hoverClass;
    hoverClass = function() {
      return {
        restrict: 'A',
        scope: {
          hoverClass: '@'
        },
        link: function(scope, element) {
          element.on('mouseenter', function() {
            element.addClass(scope.hoverClass);
          });
          element.on('mouseleave', function() {
            element.removeClass(scope.hoverClass);
          });
        }
      };
    };
    __interface__.directive('hoverClass', [hoverClass]);
  });

}).call(this);

//# sourceMappingURL=hoverClass.js.map
