(function() {
  define(['interface'], function(__interface__) {
    return __interface__.directive('openWindow', [
      '$log', '$mdToast', '$compile', function($log, $mdToast, $compile) {
        return {
          restrict: 'A',
          scope: {
            label: '@openWindowHref',
            uri: '@openWindowUri'
          },
          link: function($scope, element, $attrs) {
            var absent, responses;
            absent = -1;
            if ($scope.uri.indexOf('fan-view') !== absent) {
              element.on('click', function(e) {
                var sep, siteUrl;
                element.toggleClass('md-toast-open-bottom');
                sep = '/';
                siteUrl = '.' + '/' + $scope.uri;
                if (e.target.className.indexOf('nav-label') !== absent && element.hasClass('md-toast-open-bottom')) {
                  e.preventDefault();
                  $scope.openWindow(siteUrl);
                } else {
                  e.preventDefault();
                  $mdToast.hide();
                }
              });
            }
            responses = {
              iphone6: ['width=375', 'height=677'],
              iphone5: ['width=320', 'height=568'],
              galaxys6: ['width=340', 'height=640']
            };
            $scope.openWindow = function($windowUrl) {

              /*
               */
              var responsesMenu, _tmpl;
              responsesMenu = ['<span onClick="window.open(\'' + $windowUrl + '\',\'' + $scope.label + '\',\'' + responses['iphone5'].join(',') + '\')">iPhone5</span>', '<span onClick="window.open(\'' + $windowUrl + '\',\'' + $scope.label + '\',\'' + responses['iphone6'].join(',') + '\')">iPhone6</span>', '<span onClick="window.open(\'' + $windowUrl + '\',\'' + $scope.label + '\',\'' + responses['galaxys6'].join(',') + '\')">Galaxy S4-6/Nexus 5</span>'].join('');
              _tmpl = '<div class="fan-view--menu">' + responsesMenu + '</div>';
              $mdToast.show({
                position: 'fit',
                template: _tmpl,
                hideDelay: 0,
                parent: element
              });
            };
          }
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=openWindow.js.map
