(function() {
  define(["interface", "jquery", "jquery.easing"], function(__interface__, $) {
    var bitsDisplayCounter;
    bitsDisplayCounter = function($rootScope) {
      return {
        restrict: "A",
        link: function($scope, element) {
          $scope.$on('bitsCollectedValue:loaded', function(event, bitsCollectedValue) {
            var $el;
            $el = $(element);
            $scope.value = parseFloat(bitsCollectedValue.val);
            return $({
              percentage: 0
            }).stop(true).animate({
              percentage: $scope.value
            }, {
              duration: 2000,
              easing: "easeOutExpo",
              step: function() {
                var _percentage;
                _percentage = Math.round(this.percentage * 10) / 10;
                return $el.text(_percentage);
              }
            }).promise().done(function() {
              return $el.text($scope.value);
            });
          });
        }
      };
    };
    __interface__.directive("bitsDisplayCounter", ['$rootScope', bitsDisplayCounter]);
  });

}).call(this);

//# sourceMappingURL=bitsDisplayCounter.js.map
