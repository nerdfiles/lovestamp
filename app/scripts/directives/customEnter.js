
/*
@fileOverview
@description

    A custom enter key directive.
 */

(function() {
  define(["interface"], function(__interface__) {

    /**
    Enter Key Directive
     */
    __interface__.directive("customEnter", [
      function() {
        var linker;
        linker = function($scope, element, attrs) {
          element.bind("keyup", function(e) {
            if (e.which === 13) {
              $scope.$apply(function() {});
              $scope.$eval(attrs.customEnter);
              return;
            }
            e.preventDefault();
          });
        };
        return {
          link: linker,
          restrict: "A"
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=customEnter.js.map
