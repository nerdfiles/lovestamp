###*
@fileOverview

 _                       _______
| |                  _  (_______)
| |__  _____ _____ _| |_ _  _  _ _____ ____
|  _ \| ___ (____ (_   _) ||_|| (____ |  _ \
| | | | ____/ ___ | | |_| |   | / ___ | |_| |
|_| |_|_____)_____|  \__)_|   |_\_____|  __/

@note Perhaps a simple correspondent graph to Dashboard D3 recursive graphs.
@description

    Heat Map/Histrograph Linearization Reductions (Bar Graphs)

###

define [
  'interface'
  'angular'
  'jquery'
  'config'
  'utils/dataDefender'
], (__interface, angular, $, environment) ->

  ###*
  # Link Content
  #
  # This directive can represent content as both a link to the content
  # (metadata) and the content itself.
  ###

  __interface.register.directive 'heatMap', [
    'dataDefender'
    '$timeout'
    (dataDefender, $timeout) ->

      ###
      @expects scale
      ###
      linker = ($scope, $element, $attrs) ->

        # passed or default
        $scope.scale = if dataDefender.isEmpty($attrs.scale) then [
          {
            min: 0
            max: 0.99
            color: '#c75356'
          }
          {
            min: 1
            max: 1.99
            color: '#e48846'
          }
          {
            min: 2
            max: 2.99
            color: '#fecb48'
          }
          {
            min: 3
            max: 3.99
            color: '#99c245'
          }
          {
            min: 4
            max: 4.99
            color: '#62a455'
          }
        ] else angular.fromJson($attrs.scale)

        height = $($element).height()
        firstScaleElementWidth = 0

        angular.forEach $scope.scale, (scaleElement, i) ->
          scaleElement.width = scaleElement.width or $($element).width() / $scope.scale.length
          if i == 0
            firstScaleElementWidth = scaleElement.width
          scaleElement.borderRadius =
            topLeft: if dataDefender.isEmpty(scaleElement.borderRadius) then (if i == 0 then height / 2 else 0) else scaleElement.borderRadius.topLeft or 0
            bottomLeft: if dataDefender.isEmpty(scaleElement.borderRadius) then (if i == 0 then height / 2 else 0) else scaleElement.borderRadius.bottomLeft or 0
            topRight: if dataDefender.isEmpty(scaleElement.borderRadius) then (if i == $scope.scale.length - 1 then height / 2 else 0) else scaleElement.borderRadius.topRight or 0
            bottomRight: if dataDefender.isEmpty(scaleElement.borderRadius) then (if i == $scope.scale.length - 1 then height / 2 else 0) else scaleElement.borderRadius.bottomRight or 0
          return

        $($element).find('.icon').css 'left', -(firstScaleElementWidth / 2)

        $timeout (->
          $($element).find('.icon').css 'transition', 'left 1s ease-out'
          $($element).find('.icon').css 'left', firstScaleElementWidth * parseFloat($scope.value) - 15
          return
        ), 250

        return

      {
        link        : linker
        restrict    : 'A'
        templateUrl : environment.baseTemplateUrl + 'partials/heat-map.html'
        scope       : value                                                  : '=?'
      }
  ]
  return
