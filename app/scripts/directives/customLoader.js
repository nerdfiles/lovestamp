
/*
@fileOverview

@description

    A custom loader.
 */

(function() {
  define(["interface", "config"], function(__interface__, environment) {
    __interface__.directive("loader", [
      function() {

        /**
        Linker
        
        @param {object} $scope
        @param {object} element
        @param {object} attrs
         */
        var linker;
        linker = function($scope) {
          var t;
          scope.isLoading = true;
          return;
          return t = window.is_device ? './scripts/' : environment.baseTemplateUrl;
        };
        return {
          link: linker,
          restrict: "A",
          templateUrl: _t + "modules/www/controllers/login/partials/loader.html"
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=customLoader.js.map
