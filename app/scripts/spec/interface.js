(function() {
  define(["interface", "angularAMD"], function(__interface__, angularAMD) {
    console.log(angularAMD);
    describe("Web Application", function() {
      it("should depend on angularAMD", function() {
        expect(angularAMD).toBeDefined();
      });
      it("should define application name.", function() {
        expect(__interface__.name).toBe(angularAMD.appname());
      });
      it("should front-load Angular module ngRoute", function() {
        expect(__interface__.requires).toContain("ngRoute");
      });
      it("should front-load Angular module angular-gm", function() {
        expect(__interface__.requires).toContain("AngularGM");
      });
    });
    describe("cached property", function() {
      it("controllerProvider", function() {
        expect(angularAMD.getCachedProvider("$controllerProvider")).toBeDefined();
      });
      it("compileProvider", function() {
        expect(angularAMD.getCachedProvider("$compileProvider")).toBeDefined();
      });
      it("filterProvider", function() {
        expect(angularAMD.getCachedProvider("$filterProvider")).toBeDefined();
      });
      it("animateProvider", function() {
        expect(angularAMD.getCachedProvider("$animateProvider")).toBeDefined();
      });
      it("provide", function() {
        expect(angularAMD.getCachedProvider("$provide")).toBeDefined();
      });
      it("injector", function() {
        expect(angularAMD.getCachedProvider("$injector")).toBeDefined();
      });
    });
  });

}).call(this);

//# sourceMappingURL=interface.js.map
