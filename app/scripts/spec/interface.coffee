define [
  "interface"
  "angularAMD"
], (__interface__, angularAMD) ->
  console.log angularAMD
  describe "Web Application", ->
    it "should depend on angularAMD", ->
      expect(angularAMD).toBeDefined()
      return

    it "should define application name.", ->
      expect(__interface__.name).toBe angularAMD.appname()
      return

    it "should front-load Angular module ngRoute", ->
      expect(__interface__.requires).toContain "ngRoute"
      return

    it "should front-load Angular module angular-gm", ->
      expect(__interface__.requires).toContain "AngularGM"
      return

    return

  describe "cached property", ->
    it "controllerProvider", ->
      expect(angularAMD.getCachedProvider("$controllerProvider")).toBeDefined()
      return

    it "compileProvider", ->
      expect(angularAMD.getCachedProvider("$compileProvider")).toBeDefined()
      return

    it "filterProvider", ->
      expect(angularAMD.getCachedProvider("$filterProvider")).toBeDefined()
      return

    it "animateProvider", ->
      expect(angularAMD.getCachedProvider("$animateProvider")).toBeDefined()
      return

    it "provide", ->
      expect(angularAMD.getCachedProvider("$provide")).toBeDefined()
      return

    it "injector", ->
      expect(angularAMD.getCachedProvider("$injector")).toBeDefined()
      return

    return

  return

