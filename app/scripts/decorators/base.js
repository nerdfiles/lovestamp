(function() {
  define(["angular", "services/authentication"], function(angular) {

    /**
    Wraps ng-cloak so that, instead of simply waiting for Angular to compile, it waits until
    simpleLogin resolves with the remote Firebase services.
    <code>
    <div ng-cloak>Authentication has resolved.</div>
    </code>
     */
    return angular.module('decorators', []).provider([
      "$provide", function($provide) {
        return $provide.decorator("ngCloakDirective", [
          "$delegate", "simpleLogin", function($delegate, simpleLogin) {
            var directive, _compile;
            directive = $delegate[0];
            console.log(directive);
            _compile = directive.compile;
            directive.compile = function(element, attr) {
              simpleLogin.getUser().then(function() {
                _compile.call(directive, element, attr);
              });
            };
            return $delegate;
          }
        ]);
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=base.js.map
