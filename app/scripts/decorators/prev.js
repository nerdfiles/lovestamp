(function() {
  define(['angular'], function(angular) {
    var prev;
    prev = function(Element) {
      var children, parent, _i, _ref;
      parent = Element.parent();
      children = parent.children();
      for (_i = 0, _ref = children.length; 0 <= _ref ? _i <= _ref : _i >= _ref; 0 <= _ref ? _i++ : _i--) {
        if (children[_i] === Element[0]) {
          prev = children[_i - 1];
        }
      }
      return prev;
    };
    angular.element.prototype.prev = prev;
    return angular;
  });

}).call(this);

//# sourceMappingURL=prev.js.map
