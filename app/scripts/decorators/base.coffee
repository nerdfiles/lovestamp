define [
  "angular"
  "services/authentication"
], (angular) ->
  ###*
  Wraps ng-cloak so that, instead of simply waiting for Angular to compile, it waits until
  simpleLogin resolves with the remote Firebase services.
  <code>
  <div ng-cloak>Authentication has resolved.</div>
  </code>
  ###
  angular.module('decorators', []).provider [
    "$provide"
    ($provide) ->
      # adapt ng-cloak to wait for auth before it does its magic
      $provide.decorator "ngCloakDirective", [
        "$delegate"
        "simpleLogin"
        ($delegate, simpleLogin) ->
          directive = $delegate[0]
          console.log directive
          # make a copy of the old directive
          _compile = directive.compile
          directive.compile = (element, attr) ->
            simpleLogin.getUser().then ->
              # after auth, run the original ng-cloak directive
              _compile.call directive, element, attr
              return

            return
          # return the modified directive
          return $delegate
      ]
  ]
