###*
@fileOverview

    ___ _             _
   / __|_)           | |
 _| |__ _  ____ _____| |__  _____  ___ _____
(_   __) |/ ___) ___ |  _ \(____ |/___) ___ |
  | |  | | |   | ____| |_) ) ___ |___ | ____|
  |_|  |_|_|   |_____)____/\_____(___/|_____)

                               _
                              (_)
  ____ _____ ____  _____  ____ _  ____
 / _  | ___ |  _ \| ___ |/ ___) |/ ___)
( (_| | ____| | | | ____| |   | ( (___
 \___ |_____)_| |_|_____)_|   |_|\____)
(_____|

@module utils
@description

  A Firebase abstraction layer for translating with endpoints and basic CRUD 
  operations into REST-like semantics, with hope.

###

define [
  "interface"
], (__interface__) ->

  __interface__.factory "firebase.utils", [
      "$window"
      "FIREBASE_URL"
      "$firebaseObject"
      "$firebaseArray"
      ($window, FIREBASE_URL, $firebaseObject, $firebaseArray) ->

        ###*
        @inner
        ###
        pathRef = (args) ->
          i = 0
          while i < args.length
            if angular.isArray(args[i])
              args[i] = pathRef(args[i])
            else throw new Error("Argument " + i + " to firebaseRef is not a string: " + args[i])  if typeof args[i] isnt "string"
            i++
          args.join "/"

        ###*
        Example:
        <code>
          function(firebaseRef) {
            var ref = firebaseRef('path/to/data');
          }
        </code>

        @function
        @name firebaseRef
        @param {String|Array...} path relative path to the root folder in Firebase instance
        @return a Firebase instance
        ###
        firebaseRef = (path) ->
          ref = new $window.Firebase(FIREBASE_URL)
          args = Array::slice.call(arguments)
          ref = ref.child(pathRef(args))  if args.length
          ref

        ###*
        Create a $firebase reference with just a relative path. For example:

        <code>
        function(syncData) {
          // a regular $firebase ref
          $scope.widget = syncData('widgets/alpha');

          // or automatic 3-way binding
          syncData('widgets/alpha').$bind($scope, 'widget');
        }
        </code>

        Props is the second param passed into $firebase. It can also contain limit, startAt, endAt,
        and they will be applied to the ref before passing into $firebase

        @function
        @name syncData
        @param {String|Array...} path relative path to the root folder in Firebase instance
        @param {object} [props]
        @return a Firebase instance
        ###
        syncData = (path, props) ->
          ref = firebaseRef(path)
          props = angular.extend({}, props)
          angular.forEach [
            "limit"
            "startAt"
            "endAt"
          ], (k) ->
            if props.hasOwnProperty(k)
              v = props[k]
              ref = ref[k].apply(ref, (if angular.isArray(v) then v else [v]))
              delete props[k]
            return
          #$firebase ref, props
          ref

        ###*
        Write to a specified Firebase path.

        <code>
          writeData('users/profile', {
            address: { addressLine1: "", locality: ""}
          });
        </code>

        @function
        @name writeData
        @param {String|Array...} path relative path to the root folder in Firebase instance
        @param {object} To be written at the specified path.
        @return a Firebase instance
        ###
        writeData = (path, obj) ->
          ref = firebaseRef(path)
          props = angular.extend({}, obj)
          #$firebase ref, obj
          ref

        removeData = (path) ->
          ref = firebaseRef(path)
          #$firebase ref
          ref

        updateData = (path, obj) ->
          ref = firebaseRef(path)
          #props = angular.extend({}, obj)
          #$firebase ref, obj
          #console.log path
          #console.log obj
          ref

        transmuteData = (path, obj) ->
          ref = firebaseRef path

        return (
          syncObject: (path, factoryConfig) ->
            $firebaseObject syncData.apply(null, arguments)

          syncArray: (path, factoryConfig) ->
            $firebaseArray syncData.apply(null, arguments)

          transmuteObject: ((path, obj, factoryConfig) ->
              transmuteData.apply(null, arguments).transaction((obj) ->
                if obj == null
                  _obj = _.extend {}, obj
                else
                  return
              , (error, committed, snapshot) ->
                if error
                  console.log 'ERROR: transmutedObject'
                else if !committed
                  console.log 'ABORTED: transmutedObject'
                else
                  console.log 'UPDATED: transmutedObject'
              )
            )

          updateObject: (path, obj, factoryConfig) ->
            updateData.apply(null, arguments).update(obj, (error) ->
              if error
                console.dir error
            )

          writeObject: (path, obj, factoryConfig) ->
            writeData.apply(null, arguments).set obj

          pushObject: (path, obj, factoryConfig) ->
            writeData.apply(null, arguments).push obj

          removeObject: (path, factoryConfig) ->
            removeData.apply(null, arguments).remove()

          ref: firebaseRef
        )
    ]
  return
