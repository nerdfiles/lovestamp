
/**
@fileOverview

     _                   ______           ___                 _
    | |        _        (______)         / __)               | |
  __| |_____ _| |_ _____ _     _ _____ _| |__ _____ ____   __| |_____  ____
 / _  (____ (_   _|____ | |   | | ___ (_   __) ___ |  _ \ / _  | ___ |/ ___)
( (_| / ___ | | |_/ ___ | |__/ /| ____| | |  | ____| | | ( (_| | ____| |
 \____\_____|  \__)_____|_____/ |_____) |_|  |_____)_| |_|\____|_____)_|

@description

  Cultural wrappers for data type checks.
 */

(function() {
  define(['interface'], function(__interface) {

    /**
     * Factory function for dataTypeChecking object.
     * @returns Object with the following methods:
     *   isNull(data)
     *   isUndefined(data)
     *   isNullOrUndefined(data)
     *   isBoolean(data)
     *   isNumber(data)
     *   isPositiveNumber(data)
     *   isPositiveInteger(data)
     *   isString(data)
     *   isPrimitive(data)
     *   isBlank(data)
     *   isEmpty(data)
     *   isNullUndefinedOrEmpty(data)
     *   isRegExp(data)
     *   isDate(data)
     *   isFunction(data)
     *   isArray(data)
     *   isGenericObject(data)
     *   isDomObject(data)
     */
    var typeCheckers;
    typeCheckers = function() {
      var typeCheckMethod, typeCheckRegex;
      typeCheckMethod = Object.prototype.toString;
      typeCheckRegex = new RegExp('^\\[object (\\w+)\\]$');
      return {
        isNull: function(data) {
          return data === null;
        },
        isUndefined: function(data) {
          return data === void 0;
        },
        isNullOrUndefined: function(data) {
          return this.isNull(data) || this.isUndefined(data);
        },
        isBoolean: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1] === 'Boolean';
        },
        isNumber: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1] === 'Number';
        },
        isPositiveNumber: function(data) {
          return this.isNumber(data) && data > 0;
        },
        isInteger: function(data) {
          return this.isNumber(data) && parseInt(data, 10) === data;
        },
        isPositiveInteger: function(data) {
          return this.isInteger(data) && data > 0;
        },
        isString: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1] === 'String';
        },
        isPrimitive: function(data) {
          return this.isNullOrUndefined(data) || this.isBoolean(data) || this.isNumber(data) || this.isString(data);
        },
        isBlank: function(data) {
          if (!this.isString(data) && !this.isNumber(data)) {
            return false;
          }
          if (data === '' || data.toString().match(/^\s+$/)) {
            return true;
          } else {
            return false;
          }
        },
        isEmpty: function(data) {
          return this.isNullOrUndefined(data) || (this.isString(data) || this.isNumber(data)) && this.isBlank(data);
        },
        isNullUndefinedOrEmpty: function(data) {
          return !this.isString(data);
        },
        isRegExp: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1] === 'RegExp';
        },
        isDate: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1] === 'Date';
        },
        isFunction: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1] === 'Function';
        },
        isArray: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1] === 'Array';
        },
        isGenericObject: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1] === 'Object';
        },
        isDomObject: function(data) {
          return typeCheckMethod.call(data).match(typeCheckRegex)[1].match(/^HTML/) !== null;
        }
      };
    };
    __interface.service('dataTypeChecking', [typeCheckers]);
  });

  a;

}).call(this);

//# sourceMappingURL=dataDefender.js.map
