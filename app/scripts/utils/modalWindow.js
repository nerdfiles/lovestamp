(function() {
  define(['angular-material'], function() {
    var modalInstanceCtrl;
    modalInstanceCtrl = function($scope, $modalInstance, dataModel) {
      $scope.dataModel = dataModel;

      /**
       * @template modal-tmpl-savedsearch.html
       * @expects searchService.saveSearch(savedSearch)
       */
      $scope.save = function() {
        $modalInstance.close($scope.dataModel);
      };
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
      $scope.ok = function() {
        $modalInstance.dismiss('ok');
      };
      $scope.remove = function() {
        $modalInstance.close($scope.dataModel);
      };
    };
    return modalInstanceCtrl;
  });

}).call(this);

//# sourceMappingURL=modalWindow.js.map
