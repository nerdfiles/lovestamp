
/**
Wrapper to set addToHomescreen.
 */

(function() {
  define(["add-to-homescreen"], function() {
    var addToHomescreen;
    return addToHomescreen = window.addToHomescreen;
  });

}).call(this);

//# sourceMappingURL=addToHomescreen.js.map
