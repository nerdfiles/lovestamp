
/**
@fileOverview

    ___ _             _
   / __|_)           | |
 _| |__ _  ____ _____| |__  _____  ___ _____
(_   __) |/ ___) ___ |  _ \(____ |/___) ___ |
  | |  | | |   | ____| |_) ) ___ |___ | ____|
  |_|  |_|_|   |_____)____/\_____(___/|_____)

                               _
                              (_)
  ____ _____ ____  _____  ____ _  ____
 / _  | ___ |  _ \| ___ |/ ___) |/ ___)
( (_| | ____| | | | ____| |   | ( (___
 \___ |_____)_| |_|_____)_|   |_|\____)
(_____|

@module utils
@description

  A Firebase abstraction layer for translating with endpoints and basic CRUD 
  operations into REST-like semantics, with hope.
 */

(function() {
  define(["interface"], function(__interface__) {
    __interface__.factory("firebase.utils", [
      "$window", "FIREBASE_URL", "$firebaseObject", "$firebaseArray", function($window, FIREBASE_URL, $firebaseObject, $firebaseArray) {

        /**
        @inner
         */
        var firebaseRef, pathRef, removeData, syncData, transmuteData, updateData, writeData;
        pathRef = function(args) {
          var i;
          i = 0;
          while (i < args.length) {
            if (angular.isArray(args[i])) {
              args[i] = pathRef(args[i]);
            } else {
              if (typeof args[i] !== "string") {
                throw new Error("Argument " + i + " to firebaseRef is not a string: " + args[i]);
              }
            }
            i++;
          }
          return args.join("/");
        };

        /**
        Example:
        <code>
          function(firebaseRef) {
            var ref = firebaseRef('path/to/data');
          }
        </code>
        
        @function
        @name firebaseRef
        @param {String|Array...} path relative path to the root folder in Firebase instance
        @return a Firebase instance
         */
        firebaseRef = function(path) {
          var args, ref;
          ref = new $window.Firebase(FIREBASE_URL);
          args = Array.prototype.slice.call(arguments);
          if (args.length) {
            ref = ref.child(pathRef(args));
          }
          return ref;
        };

        /**
        Create a $firebase reference with just a relative path. For example:
        
        <code>
        function(syncData) {
          // a regular $firebase ref
          $scope.widget = syncData('widgets/alpha');
        
          // or automatic 3-way binding
          syncData('widgets/alpha').$bind($scope, 'widget');
        }
        </code>
        
        Props is the second param passed into $firebase. It can also contain limit, startAt, endAt,
        and they will be applied to the ref before passing into $firebase
        
        @function
        @name syncData
        @param {String|Array...} path relative path to the root folder in Firebase instance
        @param {object} [props]
        @return a Firebase instance
         */
        syncData = function(path, props) {
          var ref;
          ref = firebaseRef(path);
          props = angular.extend({}, props);
          angular.forEach(["limit", "startAt", "endAt"], function(k) {
            var v;
            if (props.hasOwnProperty(k)) {
              v = props[k];
              ref = ref[k].apply(ref, (angular.isArray(v) ? v : [v]));
              delete props[k];
            }
          });
          return ref;
        };

        /**
        Write to a specified Firebase path.
        
        <code>
          writeData('users/profile', {
            address: { addressLine1: "", locality: ""}
          });
        </code>
        
        @function
        @name writeData
        @param {String|Array...} path relative path to the root folder in Firebase instance
        @param {object} To be written at the specified path.
        @return a Firebase instance
         */
        writeData = function(path, obj) {
          var props, ref;
          ref = firebaseRef(path);
          props = angular.extend({}, obj);
          return ref;
        };
        removeData = function(path) {
          var ref;
          ref = firebaseRef(path);
          return ref;
        };
        updateData = function(path, obj) {
          var ref;
          ref = firebaseRef(path);
          return ref;
        };
        transmuteData = function(path, obj) {
          var ref;
          return ref = firebaseRef(path);
        };
        return {
          syncObject: function(path, factoryConfig) {
            return $firebaseObject(syncData.apply(null, arguments));
          },
          syncArray: function(path, factoryConfig) {
            return $firebaseArray(syncData.apply(null, arguments));
          },
          transmuteObject: (function(path, obj, factoryConfig) {
            return transmuteData.apply(null, arguments).transaction(function(obj) {
              var _obj;
              if (obj === null) {
                return _obj = _.extend({}, obj);
              } else {

              }
            }, function(error, committed, snapshot) {
              if (error) {
                return console.log('ERROR: transmutedObject');
              } else if (!committed) {
                return console.log('ABORTED: transmutedObject');
              } else {
                return console.log('UPDATED: transmutedObject');
              }
            });
          }),
          updateObject: function(path, obj, factoryConfig) {
            return updateData.apply(null, arguments).update(obj, function(error) {
              if (error) {
                return console.dir(error);
              }
            });
          },
          writeObject: function(path, obj, factoryConfig) {
            return writeData.apply(null, arguments).set(obj);
          },
          pushObject: function(path, obj, factoryConfig) {
            return writeData.apply(null, arguments).push(obj);
          },
          removeObject: function(path, factoryConfig) {
            return removeData.apply(null, arguments).remove();
          },
          ref: firebaseRef
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=firebase-generic.js.map
