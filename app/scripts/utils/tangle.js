
/**
Wrapper to set Tangle to global.
 */

(function() {
  define(["tangle"], function(Tangle) {
    window.Tangle = Tangle;
  });

}).call(this);

//# sourceMappingURL=tangle.js.map
