define [ 'config' ], (environment) ->
  require [ 'async!//maps.googleapis.com/maps/api/js' + '?v=3.exp' + '&key=' + environment.google.maps.key + '&sensor=false' + '&libraries=geometry,places' ]
