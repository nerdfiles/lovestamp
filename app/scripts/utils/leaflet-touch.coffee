define [
  'leaflet'
], (L) ->

  L.Map.mergeOptions
    touchExtend: true

  L.Map.TouchExtend = L.Handler.extend

    initialize: (map) ->
      this._map = map
      this._container = map._container
      this._pane = map._panes.overlayPane

    addHooks: ->
      L.DomEvent.on(this._container, 'touchstart', this._onTouchStart, this)
      L.DomEvent.on(this._container, 'touchend', this._onTouchEnd, this)
      L.DomEvent.on(this._container, 'touchmove', this._onTouchMove, this)

    removeHooks:  () ->
      L.DomEvent.off(this._container, 'touchstart', this._onTouchStart)
      L.DomEvent.off(this._container, 'touchend', this._onTouchEnd)
      L.DomEvent.off(this._container, 'touchmove', this._onTouchMove)

    _onTouchEvent: (e, type) ->
      return unless this._map._loaded

      touch = e.touches[0]
      containerPoint = L.point(touch.clientX, touch.clientY)
      layerPoint = this._map.containerPointToLayerPoint(containerPoint)
      latlng = this._map.layerPointToLatLng(layerPoint)

      this._map.fire type,
        latlng         : latlng
        layerPoint     : layerPoint
        containerPoint : containerPoint
        originalEvent  : e

    _onTouchStart:  (e) ->
      @_onTouchEvent(e, 'touchstart')

    _onTouchEnd:  (e) ->
      return unless this._map._loaded
      this._map.fire 'touchend', 
        originalEvent: e

    _onTouchMove:  (e) ->
      @_onTouchEvent(e, 'touchmove')

  L.Map.addInitHook('addHandler', 'touchExtend', L.Map.TouchExtend)
  return
