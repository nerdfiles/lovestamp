
/**
Wrapper to set HammerJS to global.
 */

(function() {
  define(["hammer"], function(Hammer) {
    window.Hammer = Hammer;
  });

}).call(this);

//# sourceMappingURL=hammer.js.map
