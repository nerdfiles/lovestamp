define [ 'angular-material' ], ->

  modalInstanceCtrl = ($scope, $modalInstance, dataModel) ->
    $scope.dataModel = dataModel
    # == Interaction Model == //

    ###*
    # @template modal-tmpl-savedsearch.html
    # @expects searchService.saveSearch(savedSearch)
    ###

    $scope.save = ->
      $modalInstance.close $scope.dataModel
      return

    $scope.cancel = ->
      $modalInstance.dismiss 'cancel'
      return

    $scope.ok = ->
      $modalInstance.dismiss 'ok'
      return

    $scope.remove = ->
      $modalInstance.close $scope.dataModel
      return

    return

  modalInstanceCtrl
