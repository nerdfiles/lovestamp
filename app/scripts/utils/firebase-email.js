(function() {
  define(["interface", "utils/firebase"], function(__interface__) {
    "use strict";
    __interface__.factory("email.change", [
      "firebase.utils", "$q", "$rootScope", function(firebaseUtils, $q, $rootScope) {
        return function(password, oldEmail, newEmail, simpleLogin) {
          var authNewAccount, authOldAccount, context, copyProfile, createNewAccount, loadOldProfile, removeOldLogin, removeOldProfile;
          authOldAccount = function() {
            return simpleLogin.login(context.old.email, password).then(function(user) {
              context.old.uid = user.uid;
            });
          };
          loadOldProfile = function() {
            var def;
            def = $q.defer();
            context.old.ref = firebaseUtils.ref("users", context.old.uid);
            context.old.ref.once("value", (function(snap) {
              var dat;
              dat = snap.val();
              if (dat === null) {
                def.reject(oldEmail + " not found");
              } else {
                context.old.name = dat.name;
                def.resolve();
              }
            }), function(err) {
              def.reject(err);
            });
            return def.promise;
          };
          createNewAccount = function() {
            return simpleLogin.createAccount(context.curr.email, password, context.old.name).then(function(user) {
              context.curr.uid = user.uid;
            });
          };
          copyProfile = function() {
            var d, profile;
            d = $q.defer();
            context.curr.ref = firebaseUtils.ref("users", context.curr.uid);
            profile = {
              email: context.curr.email,
              name: context.old.name || ""
            };
            context.curr.ref.set(profile, function(err) {
              if (err) {
                d.reject(err);
              } else {
                d.resolve();
              }
            });
            return d.promise;
          };
          removeOldProfile = function() {
            var d;
            d = $q.defer();
            context.old.ref.remove(function(err) {
              if (err) {
                d.reject(err);
              } else {
                d.resolve();
              }
            });
            return d.promise;
          };
          removeOldLogin = function() {
            var def;
            def = $q.defer();
            simpleLogin.removeUser(context.old.email, password).then((function() {
              def.resolve();
            }), function(err) {
              def.reject(err);
            });
            return def.promise;
          };
          authNewAccount = function() {
            return simpleLogin.login(context.curr.email, password);
          };
          context = {
            old: {
              email: oldEmail
            },
            curr: {
              email: newEmail
            }
          };
          $rootScope.authChangeInProgress = true;
          return authOldAccount().then(loadOldProfile).then(createNewAccount).then(copyProfile).then(authOldAccount).then(removeOldProfile).then(removeOldLogin).then(authNewAccount);
        };
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=firebase-email.js.map
