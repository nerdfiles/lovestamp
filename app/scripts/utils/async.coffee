###*
Wrapper to set caolan/async to global.
###
define ["helpers/async"], (async) ->
  window.async = async
