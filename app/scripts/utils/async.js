
/**
Wrapper to set caolan/async to global.
 */

(function() {
  define(["helpers/async"], function(async) {
    return window.async = async;
  });

}).call(this);

//# sourceMappingURL=async.js.map
