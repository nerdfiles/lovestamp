define [
  "interface"
  "utils/firebase"
], (__interface__) ->
  "use strict"
  __interface__.factory "email.change", [
    "firebase.utils"
    "$q"
    "$rootScope"
    (firebaseUtils, $q, $rootScope) ->
      return (password, oldEmail, newEmail, simpleLogin) ->
        
        # this prevents the routes.js logic from redirecting to the login page
        # while we log out of the old account and into the new one, see routes.js
        
        # execute activities in order; first we authenticate the user
        
        # then we fetch old account details
        
        # then we create a new account
        
        # then we copy old account info
        
        # and once they safely exist, then we can delete the old ones
        # we have to authenticate as the old user again
        
        # and now authenticate as the new user
        authOldAccount = ->
          simpleLogin.login(context.old.email, password).then (user) ->
            context.old.uid = user.uid
            return

        loadOldProfile = ->
          def = $q.defer()
          context.old.ref = firebaseUtils.ref("users", context.old.uid)
          context.old.ref.once "value", ((snap) ->
            dat = snap.val()
            if dat is null
              def.reject oldEmail + " not found"
            else
              context.old.name = dat.name
              def.resolve()
            return
          ), (err) ->
            def.reject err
            return

          def.promise
        createNewAccount = ->
          simpleLogin.createAccount(context.curr.email, password, context.old.name).then (user) ->
            context.curr.uid = user.uid
            return

        copyProfile = ->
          d = $q.defer()
          context.curr.ref = firebaseUtils.ref("users", context.curr.uid)
          profile =
            email: context.curr.email
            name: context.old.name or ""

          context.curr.ref.set profile, (err) ->
            if err
              d.reject err
            else
              d.resolve()
            return

          d.promise
        removeOldProfile = ->
          d = $q.defer()
          context.old.ref.remove (err) ->
            if err
              d.reject err
            else
              d.resolve()
            return

          d.promise
        removeOldLogin = ->
          def = $q.defer()
          simpleLogin.removeUser(context.old.email, password).then (->
            def.resolve()
            return
          ), (err) ->
            def.reject err
            return

          def.promise
        authNewAccount = ->
          simpleLogin.login context.curr.email, password
        context =
          old:
            email: oldEmail

          curr:
            email: newEmail

        $rootScope.authChangeInProgress = true
        return authOldAccount().then(loadOldProfile).then(createNewAccount).then(copyProfile).then(authOldAccount).then(removeOldProfile).then(removeOldLogin).then(authNewAccount)
        return
  ]
  return

