###*
Wrapper to set HammerJS to global.
###
define ["hammer"], (Hammer) ->
  window.Hammer = Hammer
  return
