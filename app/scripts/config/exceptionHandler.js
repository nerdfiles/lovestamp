
/**
@fileOverview
@module environment/exceptionHandler
 */

(function() {
  define([], function() {
    var extendExceptionHandler;
    extendExceptionHandler = function($delegate, logger, loggly) {
      var logError;
      logError = logger.getLogFn("lovestamp", "error");
      return function(exception, cause) {
        var errorData;
        $delegate(exception, cause);
        errorData = {
          exception: exception,
          cause: cause
        };
        loggly.log({
          logLevel: 'error',
          data: errorData.exception + ' — ' + errorData.cause
        });
        logError(null, errorData, true);
      };
    };
    angular.module('decorators', []).factory([
      "$provide", function($provide) {
        return $provide.decorator("$exceptionHandler", ["$delegate", "logger", "loggly", extendExceptionHandler]);
      }
    ]);
  });

}).call(this);

//# sourceMappingURL=exceptionHandler.js.map
