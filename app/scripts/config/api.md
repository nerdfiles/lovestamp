# LoveStamp API

The Contractual API Data Plan for lovestamp.io front ends, clients, third 
party integrations, etc.

## Canonical Base

    http://data.lovestamp.io/mobile/

## Endpoints

LoveStamp provides endpoints for virtual stamping, which entails integration with 
third party payment network use case flows and payment interaction systems.

### User

#### Type

    Detail

#### Method

    GET

#### Schema

    http://data.lovestamp.io/mobile/users/{{string:handle}}

#### Examples

    http://data.lovestamp.io/mobile/users/+13193000000000 # Google Plus
    http://data.lovestamp.io/mobile/users/@RattelyrDragon # Twitter

### Search Users

#### Type

    List

#### Method

    GET

#### Schema

    http://data.lovestamp.io/mobile/users

#### Parameters

    searchValue  {string}
    limit        {number}
    offset       {number}

#### Examples

    http://data.lovestamp.io/mobile/users?searchValue=pepsico&limit=8&offset=0

### Merchants with Stamps

#### Type

    List

#### Method

    GET

#### Schema

    http://data.lovestamp.io/mobile/users/{{string:handle}}/stamps

#### Parameters

    limit        {number}
    offset       {number}

#### Examples

    http://data.lovestamp.io/mobile/users/+13193000000000/stamps?limit=4&offset=11

### Stamp Requests

#### Type

    List

#### Method

    GET

#### Schema

    http://data.lovestamp.io/mobile/requests

#### Parameters

    searchValue  {string}

#### Examples

    http://data.lovestamp.io/mobile/requests?searchValue=dorem+iternum+falor

### Stamp Requests

#### Type

    Detail

#### Method

    GET

#### Schema

    http://data.lovestamp.io/mobile/requests/{{string:handle}}

#### Examples

    http://data.lovestamp.io/mobile/requests/@TheLoveStamper

### Stamp Request Metadata

Retrieves a list of Merchants corresponding to the given metadata attribute.

#### Type

    List

#### Method

    GET

#### Schema

    http://data.lovestamp.io/mobile/requests/{{string:attrName}}/{{string:attrString}}

#### Root

    http://data.lovestamp.io/mobile/requests/

#### Examples

    http://data.lovestamp.io/mobile/requests/orgName/LoveStamp
    http://data.lovestamp.io/mobile/requests/email/hello@lovestamp.io
    http://data.lovestamp.io/mobile/requests/referrer/Internet

### Virtual Stamping

An endpoint to enable virtual stampings on behalf of participating merchants.

#### Type

    Detail

#### Method

    POST

#### Schema

    http://data.lovestamp.io/mobile/virtual/stamp

#### Recipe

Single object should contain the listed parameters:

    {
      merchant : {string},
      fan      : {string}
    }

### Virtual Registration

#### Type

    Detail

#### Method

    POST

#### Schema

    http://data.lovestamp.io/mobile/virtual/registration

#### Recipe

Single object should contain the listed parameters:

    {
      merchant : {string},
      prefix   : {string}
    }

### Social

#### Type

    Detail

#### Method

    GET Read

#### Schema

    http://data.lovestamp.io/mobile/users/{{string:handle}}/social

#### Examples

    http://data.lovestamp.io/mobile/users/+13193000000000 # Google Plus
    http://data.lovestamp.io/mobile/users/@RattelyrDragon # Twitter

#### [Type](https://github.com/firebase/blaze_compiler#example)

    Detail Update (Write)

#### Method

    POST

#### Schema

    http://data.lovestamp.io/mobile/users/{{string:handle}}/social

##### Schema Microdata Rules

    properties:
      from:
        type: string
        #enforce the from field is *always* correct on creation,
        #and that only the *box owner* can delete
        constraint:  (auth.username == next     && createOnly()) ||
                     ($userid === auth.username && deleteOnly())

      #you can't delete single field due to parent's required
      to:      {type: string, constraint:  createOrDelete()}
      message: {type: string, constraint:  createOrDelete()}

    required: [from, to, message] # all messages require all the fields to be defined
                                  #(or none if the message does not exist)

    additionalProperties: false   #prevent spurious data being part of a message

    examples: #examples of inline testing
      - {from: "bill", to: "tom", message: "hey Tom!"}
    nonexamples:
      - {to: "tom", message: "hey Tom!"} #not allowed because from is missing

#### Examples

    http://data.lovestamp.io/mobile/users/+13193000000000/social # Google Plus
    http://data.lovestamp.io/mobile/users/@RattelyrDragon/social # Twitter

## Syntax Implementation

Use ``blaze_compiler`` to render client authentication and authorization 
hypermedia API controls through DOM updates and synchronization.

### Blaze Schema and Security Language

**This section is non-normative.

Implementations of Separate Access Control will tokenize DOM elements 
in line with microservices authorization API rendered in AngularJS 
custom directives.

