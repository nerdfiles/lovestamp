###*
@fileOverview

 _
| |
| |__  _____  ___ _____
|  _ \(____ |/___) ___ |
| |_) ) ___ |___ | ____|
|____/\_____(___/|_____)

@description

  Environment config base.

###

define [], () ->

  configInterface = {}

  # @note Swappable applicatioin dependencies.
  configInterface.applicationDependencies = [
    'leaflet-directive'
    'ui.router'
    'ngAria'
    'ngRoute'
    'ngMaterial'
    'ngAnimate'
    'geolocation'
    'LocalStorageModule'
    #'ngResource'
    'ngSanitize'
    'ngTouch'
    'ngMessages'
    'firebase'
    'angularPayments'
    #'elasticsearch'
    'loggly'
    'ngCordova'
    'hmTouchEvents'
    'ngDraggable'
    'base64'
    'messageBusModule'
    'materialDatePicker'
    'once'
    'localytics.directives'
  ]

  configInterface.document = window.document
  configInterface.location = window.location
  configInterface.isFanSubdomain = configInterface.location.host.indexOf("fan") == 0 # found 'fan' subdomain
  configInterface.isSubtype = if configInterface.isFanSubdomain then "fan" else "merchant"
  configInterface.baseTemplateUrl = ''
  configInterface.localDevelopment = false
  configInterface.debug ?=
    frontEnd      : false
    api           : false
    snowshoestamp : false


  #               _
  #              (_)
  #   _____ ____  _
  #  (____ |  _ \| |
  #  / ___ | |_| | |
  #  \_____|  __/|_|
  #        |_|
  #
  #  @depends https://www.firebase.com/
  #  @description
  #
  #  Leverage Firebase for microservices to authenticate and authorize our
  #  data model into marshaled delineations.
  #
  #  @example
  #
  #  http://lovestamp.firebaseio.com/

  configInterface.api ?=
    baseUrl        : "lovestamp.firebaseio.com"
    version        : "v1"
    preparedString : ''


  #                   _
  #         _        (_)
  #   ___ _| |_  ____ _ ____  _____
  #  /___|_   _)/ ___) |  _ \| ___ |
  # |___ | | |_| |   | | |_| | ____|
  # (___/   \__)_|   |_|  __/|_____)
  #                    |_|

  if Stripe
    #Stripe.setPublishableKey('pk_test_IngHIKiNxnpwobugse6bqqOP')
    Stripe.setPublishableKey('pk_live_D6VVTjIEY5uJ1ca3CRoB6V6L')

  #  _                   _
  # | |                 | |
  # | | ___   ____  ____| |_   _
  # | |/ _ \ / _  |/ _  | | | | |
  # | | |_| ( (_| ( (_| | | |_| |
  #  \_)___/ \___ |\___ |\_)__  |
  #         (_____(_____| (____/

  configInterface.loggly ?=
    logglyApiKey : '0802010a-2498-4c1e-922f-1ed76e4257c2'


  #                           _
  #                          | |
  #    ____  ___   ___   ____| | _____
  #   / _  |/ _ \ / _ \ / _  | || ___ |
  #  ( (_| | |_| | |_| ( (_| | || ____|
  #   \___ |\___/ \___/ \___ |\_)_____)
  #  (_____|           (_____|

  configInterface.google ?=
    maps:
      streetView : 'https://maps.googleapis.com/maps/api/streetview'
      geocode    : 'https://maps.googleapis.com/maps/api/geocode/json'
      key        : 'AIzaSyCmBsWXSBDayooGPEYf3jgrUZYItFySGrY'


  #                               _
  #                              | |
  #    ___ ____   ___  _ _ _  ___| |__   ___  _____
  #   /___)  _ \ / _ \| | | |/___)  _ \ / _ \| ___ |
  #  |___ | | | | |_| | | | |___ | | | | |_| | ____|
  #  (___/|_| |_|\___/ \___/(___/|_| |_|\___/|_____)

  configInterface.snowshoestamp ?=
    appKey    : "9da8da12326263876b34"
    SID       : "869"
    appSecret : "8d07fd8a2af42de5f669587d79b41e969b6bf8ef"


  console.log configInterface
  configInterface
