###*
@fileOverview
@module environment/exceptionHandler
###
define [
], () ->

    extendExceptionHandler = ($delegate, logger, loggly) ->

        logError = logger.getLogFn("lovestamp", "error")

        (exception, cause) ->

            $delegate exception, cause

            errorData =
                exception: exception
                cause: cause

            loggly.log
                logLevel: 'error'
                data: errorData.exception + ' — ' + errorData.cause

            logError null, errorData, true
            return

    angular.module('decorators', []).factory [
        "$provide"
        ($provide) ->
            $provide.decorator "$exceptionHandler", [
                "$delegate"
                "logger"
                "loggly"
                extendExceptionHandler
            ]
    ]
    return
