
/*
 * fileOverview

     _
    | |
    | |__  _____  ___ _____
    |  _ \(____ |/___) ___ |
    | |_) ) ___ |___ | ____|
    |____/\_____(___/|_____)

 *# description

Environment config base.
 */

(function() {
  define([], function() {
    var configInterface;
    configInterface = {};
    configInterface.document = window.document;
    configInterface.location = window.location;
    configInterface.isFanSubdomain = configInterface.location.pathname.indexOf("fan") !== -1;
    configInterface.isSubtype = configInterface.isFanSubdomain ? "fan" : "merchant";
    configInterface.baseTemplateUrl = '/assets/scripts/';
    configInterface.localDevelopment = false;
    configInterface.pusherServiceAppKey = '5f8d32296b6e593ef1bc';
    if (configInterface.debug == null) {
      configInterface.debug = {
        frontEnd: false,
        api: false,
        snowshoestamp: false
      };
    }
    if (configInterface.api == null) {
      configInterface.api = {
        baseUrl: "lovestamp.firebaseio.com",
        version: "v1",
        preparedString: ''
      };
    }
    if (configInterface.loggly == null) {
      configInterface.loggly = {
        logglyApiKey: '0802010a-2498-4c1e-922f-1ed76e4257c2'
      };
    }
    if (configInterface.google == null) {
      configInterface.google = {
        maps: {
          streetView: 'https://maps.googleapis.com/maps/api/streetview',
          geocode: 'https://maps.googleapis.com/maps/api/geocode/json',
          key: 'AIzaSyCmBsWXSBDayooGPEYf3jgrUZYItFySGrY'
        }
      };
    }
    if (configInterface.snowshoestamp == null) {
      configInterface.snowshoestamp = {
        appKey: "9da8da12326263876b34",
        SID: "869",
        appSecret: "8d07fd8a2af42de5f669587d79b41e969b6bf8ef"
      };
    }
    return configInterface;
  });

}).call(this);

//# sourceMappingURL=base.js.map
