# Configurable Front End Environment Specification

## XML Config

### Android

    android:allowTaskReparenting=["true" | "false"]
    android:allowBackup=["true" | "false"]
    android:backupAgent="string"
    android:banner="drawable resource"
    android:debuggable=["true" | "false"]
    android:description="string resource"
    android:enabled=["true" | "false"]
    android:hasCode=["true" | "false"]
    android:hardwareAccelerated=["true" | "false"]
    android:icon="drawable resource"
    android:isGame=["true" | "false"]
    android:killAfterRestore=["true" | "false"]
    android:largeHeap=["true" | "false"]
    android:label="string resource"
    android:logo="drawable resource"
    android:manageSpaceActivity="string"
    android:name="string"
    android:permission="string"
    android:persistent=["true" | "false"]
    android:process="string"
    android:restoreAnyVersion=["true" | "false"]
    android:requiredAccountType="string"
    android:restrictedAccountType="string"
    android:supportsRtl=["true" | "false"]
    android:taskAffinity="string"
    android:testOnly=["true" | "false"]
    android:theme="resource or theme"
    android:uiOptions=["none" | "splitActionBarWhenNarrow"]
    android:vmSafeMode=["true" | "false"] >

## Device Deployments

__The following section is non-normative.__

Device Deployments should conform to ``ngCordova`` wrapper API specification 
details and the following list of plugins:

    $ cordova plugin add org.apache.cordova.geolocation
    $ cordova plugin add https://github.com/phonegap-build/PushPlugin.git


### Supported By

    npm install grunt-cordovacli --save-dev

### Native Device Support List

    Android 5.1
    Android 5.0
    Android 4.4
    Android 4.3
    Android 4.2
    Android 4.1
    Android 4.0
    Google Nexus 7 HD Emulator 4.4
    Google Nexus 7 HD Emulator 4.3
    Google Nexus 7C Emulator 4.4
    Google Nexus 7C Emulator 4.3
    Google Nexus 7C Emulator 4.2
    Google Nexus 7C Emulator 4.1
    HTC Evo 3D Emulator 4.0
    HTC One X Emulator 4.1
    HTC One X Emulator 4.0
    LG Nexus 4 Emulator 4.4
    LG Nexus 4 Emulator 4.3
    LG Nexus 4 Emulator 4.2
    LG Optimus 3D Emulator 4.0
    Motorola Atrix HD Emulator 4.1
    Motorola Atrix HD Emulator 4.0
    Motorola Droid 4 Emulator 4.0
    Motorola Droid Razr Emulator 4.1
    Motorola Droid Razr Emulator 4.0
    Motorola Photon Q 4G Emulator 4.0
    Samsung Galaxy Nexus Emulator 4.4
    Samsung Galaxy Nexus Emulator 4.3
    Samsung Galaxy Nexus Emulator 4.2
    Samsung Galaxy Nexus Emulator 4.1
    Samsung Galaxy Nexus Emulator 4.0
    Samsung Galaxy Note 10.1 Emulator 4.1
    Samsung Galaxy Note 10.1 Emulator 4.0
    Samsung Galaxy Note Emulator 4.1
    Samsung Galaxy Note Emulator 4.0
    Samsung Galaxy S2 Emulator 4.1
    Samsung Galaxy S2 Emulator 4.0
    Samsung Galaxy S3 Emulator 4.4
    Samsung Galaxy S3 Emulator 4.3
    Samsung Galaxy S3 Emulator 4.2
    Samsung Galaxy S3 Emulator 4.1
    Samsung Galaxy S4 Emulator 4.4
    Samsung Galaxy S4 Emulator 4.3
    Samsung Galaxy S4 Emulator 4.2
    Samsung Galaxy Tab 3 Emulator 4.2

__The following section is non-normative.__

## Continuous Integration Strategies

__The following section is non-normative.__

Hypermedia Application should conform to https://schema.org/WebSite metadata 
and microdata implementation details.

## JScrambler (Secure Static Builds)

__The following section is normative.__

JScrambler builds should prefer Security-first configurations.
