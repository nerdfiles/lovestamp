# LoveStamp PTags

    <script
      src='https://embeddables.lovestamp.com/v1/?data-listen=form+button'
      data-listen='form button'
    ></script>

Is this actually possible without window.setTimeout? Can both implementations
cohabitate — perhaps a failover capture. Either way, @data-listen is a pseudo- 
element prototype.

Is it a security measure such that LoveStamp PTags must assume some 
arbitrarily long length of time before the submit of a primary 
https://schema.org/potentialAction.

The main idea is that participating sites will embed per product, perhaps 
following the Amazon Product Affiliates API.
