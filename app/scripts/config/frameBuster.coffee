###*
@module config/frameBuster.js
@fileOverview
The framebuster is a basic client-side protection against certain types of 
man-in-the-middle attacks. It generally ensures that the client payload is 
restricted in terms of DOM accessibility after check of HTTP headers, who 
will tell the rendering engine that it should be governed only by a white-
list of domains. A particular fraud attempt might involve <iframe> 
implementations themselves will inherit the top window of the browser. The 
result will be a window that is arrested with a maximizing series of 
data to the address bar, thus rendering the frame oversized for the 
parser that follows the rule limit on GET requests. It's pretty flippin' 
simple and viable.
###

(->
  ((u, instance) ->
    noop = ->
    l = undefined
    t = undefined
    w = undefined
    w = instance
    t = w.top
    l = t.location
    t.onbeforeunload = noop
    unless u.hostsList[l.hostname]
      l.replace u["default"] + "?blacklisted_site=" + encodeURIComponent(l.href)
    else
      u.hostsList[l.hostname]
  )
    hostsList:

      "local.lovestamp.io"          : 1
      "dev.lovestamp.io"            : 1
      "fan.lovestamp.io"            : 1
      "merchant.lovestamp.io"       : 1
      "merchants.lovestamp.io"      : 1
      "admin.lovestamp.io"          : 1
      "fan.lovestamp.io"            : 1
      "fans.lovestamp.io"           : 1
      "fan-local.lovestamp.io"      : 1
      "merchant-local.lovestamp.io" : 1
      "staging.lovestamp.io"        : 1
      "qa.lovestamp.io"             : 1
      "lovestamp.io"                : 1
      "192.168.2.16"                : 1
      "192.168.2.19"                : 1
      "192.168.2.3"                 : 1
      "192.168.1.198"               : 1
      "192.168.2.15"                : 1
      "10.0.0.12"                   : 1
      "10.1.10.170"                 : 1
      "192.168.2.135"               : 1
      "192.168.2.136"               : 1
      "192.168.2.4"                 : 1
      "192.168.2.132"               : 1
      "192.168.2.2"                 : 1
      "192.168.2.145"               : 1
      "192.168.2.14"                : 1
      "192.168.2.6"                 : 1
      "192.168.2.14"                : 1
      "192.168.1.24"                : 1
      "192.168.2.129"               : 1
      "192.168.2.3"                 : 1
      "192.168.2.5"                 : 1
      "192.168.2.152"               : 1
      "192.168.2.154"               : 1
      "192.168.2.133"               : 1
      "192.168.2.12"                : 1
      "192.168.2.8"                 : 1
      "192.168.2.146"               : 1
      "192.168.2.11"                : 1
      "192.168.2.135"               : 1
      "192.168.2.13"                : 1
      "192.168.2.7"                 : 1
      "192.168.2.130"               : 1
      "192.168.2.134"               : 1
      "192.168.2.131"               : 1
      "localhost"                   : 1

    default: self.location.href
  , this
  return
).call this
