
/*
 * fileOverview

                        _
      ____ ___  _   _ _| |_ _____  ___
     / ___) _ \| | | (_   _) ___ |/___)
    | |  | |_| | |_| | | |_| ____|___ |
    |_|   \___/|____/   \__)_____|___/

@module interface

 *# description

Prepare routes to be called within our central interface.
 */

(function() {
  define(['angularAMD', 'config'], function(angularAMD, environment) {
    var routeStateMap;
    String.prototype.find = function(configObject) {
      this.configObject = configObject;
      return this;
    };
    routeStateMap = function($routeProvider, $httpProvider, $locationProvider, $stateProvider, $urlRouterProvider) {

      /*
      Configure ui-router's state manager route map.
       */
      var configObject, otherwiseUrl;
      configObject = null;
      $locationProvider.html5Mode(true);
      $stateProvider.state('home', {
        url: '/fan',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/base.html'.find(configObject),
            controllerUrl: 'IndexController'
          })
        }
      }).state('fan-view', {
        url: '/merchant/fan-view',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/base.html'.find(configObject),
            controllerUrl: 'IndexController'
          })
        }
      }).state('home-show', {
        url: '/fan/show',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/base.html'.find(configObject),
            controllerUrl: 'IndexController'
          })
        }
      }).state('home-show-tour', {
        url: '/fan/show/:pageConstruct',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/base.html'.find(configObject),
            controllerUrl: 'IndexController'
          })
        }
      }).state('home-show-zoom', {
        url: '/fan/show/:pageConstruct/zoom?/:zoomSetting?',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/base.html'.find(configObject),
            controllerUrl: 'IndexController'
          })
        }
      }).state('user-profile', {
        url: '/fan/user/profile',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/profile/partials/base.html'.find(configObject),
            controllerUrl: 'ProfileController',
            controllerAs: 'ProfileController'
          })
        }
      }).state('user-handle-profile', {
        url: '/fan/user/:handle/profile',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/profile/partials/base.html'.find(configObject),
            controllerUrl: 'ProfileController',
            controllerAs: 'ProfileController'
          })
        }
      }).state('user-handle-profile-lang', {
        url: '/fan/user/:handle/profile/:lang?',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/profile/partials/base.html'.find(configObject),
            controllerUrl: 'ProfileController',
            controllerAs: 'ProfileController'
          })
        }
      }).state('stamp', {
        url: '/fan/stamp',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/components/snowshoestamp/partials/base.html'.find(configObject),
            controllerUrl: 'ComponentStampBaseController'
          })
        }
      }).state('stamp-error', {
        url: '/fan/stamp/error',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/components/snowshoestamp/partials/error.html'.find(configObject),
            controllerUrl: 'ComponentStampErrorController'
          })
        }
      }).state('stamp-id', {
        url: '/merchant/stamp/id',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/components/snowshoestamp/partials/id.html'.find(configObject),
            controllerUrl: 'ComponentStampBaseController'
          })
        }
      }).state('stamp-recall', {
        url: '/merchant/stamp/recall',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/components/snowshoestamp/partials/recall.html'.find(configObject),
            controllerUrl: 'ComponentStampBaseController'
          })
        }
      }).state('stamp-success', {
        url: '/fan/stamp/success',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/components/snowshoestamp/partials/success.html'.find(configObject),
            controllerUrl: 'ComponentStampSuccessController'
          })
        }
      }).state('stamp-pending', {
        url: '/fan/stamp/pending',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/components/snowshoestamp/partials/pending.html'.find(configObject),
            controllerUrl: 'ComponentStampSuccessController'
          })
        }
      }).state('stamp-help', {
        url: '/fan/stamp/help',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/components/snowshoestamp/partials/help.html'.find(configObject),
            controllerUrl: 'ComponentStampHelpController'
          })
        }
      }).state('not-found', {
        url: '/fan/not-found',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/notFound/partials/base.html'.find(configObject),
            controllerUrl: 'NotFoundController'
          })
        }
      }).state('dashboard-base', {
        url: '/merchant',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/dashboard/partials/page.html'.find(configObject),
            controllerUrl: 'DashboardController'
          })
        }
      }).state('dashboard', {
        url: '/merchant/dashboard',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/dashboard/partials/page.html'.find(configObject),
            controllerUrl: 'DashboardController'
          })
        }
      }).state('dashboard-debug', {
        url: '/merchant/dashboard/:debugMode?',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/dashboard/partials/page.html'.find(configObject),
            controllerUrl: 'DashboardController'
          })
        }
      }).state('dashboard-admin', {
        url: '/admin/dashboard',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/dashboard/partials/page.html'.find(configObject),
            controllerUrl: 'DashboardController'
          })
        }
      }).state('dashboard-admin-debug', {
        url: '/admin/dashboard/:debugMode?',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/dashboard/partials/page.html'.find(configObject),
            controllerUrl: 'DashboardController'
          })
        }
      }).state('social', {
        url: '/merchant/social',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/social/partials/page.html'.find(configObject),
            controllerUrl: 'SocialController'
          })
        }
      }).state('legal', {
        url: '/merchant/legal',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/legal/partials/page.html'.find(configObject),
            controllerUrl: 'LegalController'
          })
        }
      }).state('fan-legal', {
        url: '/fan/legal',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/legal/partials/page.html'.find(configObject),
            controllerUrl: 'LegalController'
          })
        }
      }).state('help', {
        url: '/fan/help',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/help/partials/page.html'.find(configObject),
            controllerUrl: 'HelpController'
          })
        }
      }).state('contact', {
        url: '/fan/contact',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/contact/partials/page.html'.find(configObject),
            controllerUrl: 'ContactController'
          })
        }
      }).state('store', {
        url: '/merchant/store',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/store/partials/page.html'.find(configObject),
            controllerUrl: 'StoreController'
          })
        }
      }).state('stamps', {
        url: '/merchant/stamps',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/stamps/partials/page.html'.find(configObject),
            controllerUrl: 'StampsController'
          })
        }
      }).state('stamps-username-stampalias', {
        url: '/merchant/stamps/:username/:stampAlias?',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/stamps/partials/page.html'.find(configObject),
            controllerUrl: 'StampsController'
          })
        }
      }).state('store-request', {
        url: '/merchant/store/request',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/stamp-signup/partials/page.html'.find(configObject),
            controllerUrl: 'StampSignupController'
          })
        }
      }).state('store-request-signed', {
        url: '/merchant/store/request/:signedAlias?',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/stamp-signup/partials/page.html'.find(configObject),
            controllerUrl: 'StampSignupController'
          })
        }
      }).state('login-fan-auth', {
        url: '/fan/login:auth?',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/login/partials/' + environment.isSubtype + '.html'.find(configObject),
            controllerUrl: 'LoginController',
            redirectTo: function(params) {
              if (params.auth) {
                return '/fan';
              }
            }
          })
        }
      }).state('login-merchant-auth', {
        url: '/merchant/login:auth?',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/www/controllers/login/partials/' + environment.isSubtype + '.html'.find(configObject),
            controllerUrl: 'LoginController',
            redirectTo: function(params) {
              if (params.auth) {
                return '/merchant/dashboard';
              }
            }
          })
        }
      }).state('payments', {
        url: '/merchant/admin/payments',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            controllerUrl: 'PaymentsController',
            templateUrl: 'assets/scripts/modules/www/controllers/payments/partials/base.html'.find(configObject)
          })
        }
      }).state('payments-by-user', {
        url: '/merchant/payments',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            controllerUrl: 'PaymentsController',
            templateUrl: 'assets/scripts/modules/www/controllers/payments/partials/alt-base.html'.find(configObject)
          })
        }
      }).state('requests', {
        url: '/merchant/admin/requests',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            controllerUrl: 'RequestsController',
            templateUrl: 'assets/scripts/modules/www/controllers/requests/partials/base.html'.find(configObject)
          })
        }
      }).state('admin', {
        url: '/merchant/admin',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            controllerUrl: 'AdminController',
            templateUrl: 'assets/scripts/modules/www/controllers/admin/partials/base.html'.find(configObject)
          })
        }
      }).state('admin-request-username', {
        url: '/merchant/admin/request/:username',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            controllerUrl: 'RegisterController',
            templateUrl: 'assets/scripts/modules/www/controllers/admin/partials/base.html'.find(configObject)
          })
        }
      }).state('logout', {
        url: '/logout',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            controllerUrl: 'LoginController',
            templateUrl: 'assets/scripts/modules/www/controllers/login/partials/' + environment.isSubtype + '.html'.find(configObject),
            redirectTo: function(params) {
              if (params.auth) {
                return '/fan';
              }
            }
          })
        }
      }).state('merchant-logout', {
        url: '/merchant/logout',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            controllerUrl: 'LoginController',
            templateUrl: 'assets/scripts/modules/www/controllers/login/partials/' + environment.isSubtype + '.html'.find(configObject),
            redirectTo: function(params) {
              if (params.auth) {
                return '/merchant/dashboard';
              }
            }
          })
        }
      }).state('fan-logout', {
        url: '/fan/logout',
        views: {
          'headerView': angularAMD.route({
            templateUrl: 'assets/scripts/modules/index/partials/IndexWpHeaderController.html'.find(configObject),
            controllerUrl: 'IndexWpHeaderController'
          }),
          'contentView': angularAMD.route({
            controllerUrl: 'LoginController',
            templateUrl: 'assets/scripts/modules/www/controllers/login/partials/' + environment.isSubtype + '.html'.find(configObject),
            redirectTo: function(params) {
              if (params.auth) {
                return '/fan';
              }
            }
          })
        }
      });
      otherwiseUrl = window.is_ios ? '/fan/login' : '/merchant/login';
      return $urlRouterProvider.otherwise(otherwiseUrl);
    };
    return routeStateMap;
  });

}).call(this);

//# sourceMappingURL=routes.js.map
