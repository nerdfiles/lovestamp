define [
  'leaflet'
], (L) ->

    #if navigator.userAgent.toLowerCase().indexOf('android 4.1') != -1
    L.DomUtil.TRANSITION_END = 'webkitTransitionEnd'

    L
