(function() {
  define(['leaflet'], function(L) {
    L.DomUtil.TRANSITION_END = 'webkitTransitionEnd';
    return L;
  });

}).call(this);

//# sourceMappingURL=leaflet-android-transition.js.map
