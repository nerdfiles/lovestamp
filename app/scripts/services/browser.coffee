###
# fileOverview

     _
    | |
    | |__   ____ ___  _ _ _  ___ _____  ____
    |  _ \ / ___) _ \| | | |/___) ___ |/ ___)
    | |_) ) |  | |_| | | | |___ | ____| |
    |____/|_|   \___/ \___/(___/|_____)_|

## description

Defensively facilitates InAppBrowser, mostly.

###

define [
  'interface'
], (__interface__) ->

  browserService = ($window) ->
    ###
    Internal browser service.
    ###


    serviceInterface =
      deviceReady: false
      InAppBrowser: $window


    deviceCheck = () ->
      ###
      Native services check and preparation.
      ###

      serviceInterface.deviceReady = true

      if typeof cordova == 'undefined'
        serviceInterface.deviceReady = 
        if cordova.InAppBrowser and $window.is_device
          serviceInterface.InAppBrowser = cordova.InAppBrowser

          serviceInterface.InAppBrowser.addEventListener 'loadstop', serviceInterface.changeBackgroundColor
          serviceInterface.InAppBrowser.addEventListener 'exit', serviceInterface.iabClose
      return


    document.addEventListener("deviceready", deviceCheck, false)


    serviceInterface.changeBackgroundColor = ->
      serviceInterface.InAppBrowser.insertCSS({
        code: 'body { background: #ffffff; }'
      }, () ->
        console.log 'Set background of InAppBrowser.'
        return
      )
      return


    serviceInterface.iabClose = (event) ->
      serviceInterface.InAppBrowser.removeEventListener 'loadstop', serviceInterface.changeBackgroundColor
      serviceInterface.InAppBrowser.removeEventListener 'exit', serviceInterface.iabClose
      return


    serviceInterface.addEvent = (config) ->
      ###
      Add event.

      @param {object} config
        :eventName {string} "loadstart", "loadstop", "loaderror", "exit"
        :callback {function}
      ###

      _config = config or {}
      serviceInterface.cachedEventName = _config.eventName or null
      serviceInterface.cachedCallback = _config.callback or null
      serviceInterface.InAppBrowser.addEventListener(serviceInterface.cachedEventName, serviceInterface.cachedCallback)


    serviceInterface.removeEvent = (config) ->
      ###
      Remove event.
      ###

      _config = config or {}

      serviceInterface.cachedEventName = _config.eventName or null
      serviceInterface.cachedCallback = _config.callback or null
      serviceInterface.InAppBrowser.removeEventListener(serviceInterface.cachedEventName, serviceInterface.cachedCallback)


    serviceInterface.removeLastEvent = () ->
      ###
      Remove cached event.
      ###
      if serviceInterface.cachedEventName and serviceInterface.cachedCallback
        serviceInterface.InAppBrowser.removeEventListener(serviceInterface.cachedEventName, serviceInterface.cachedCallback)
      else
        console.log 'No InAppBrowser event in cache.'


    serviceInterface.close = () ->
      ###
      Close cached browser window.
      ###

      if serviceInterface.lastOpenedConfig
        return serviceInterface.lastOpenedConfig.close()
      else
        console.log 'No InAppBrowser window available to close.'
      return


    serviceInterface.show = () ->
      ###
      Show or reopen cached browser window
      ###

      if serviceInterface.lastOpenedConfig
        return serviceInterface.lastOpenedConfig.show()
      else
        console.log 'No InAppBrowser window is open.'
      return


    serviceInterface.execScript = (config) ->
      ###
      Execute script (within added events, etc.).

      @param {object} config
        :injectDetails {object} 
          {
            file: '/path/to/file'
            code: ''
          }
        :callback {function}
      ###
      _config = config or {}

      if serviceInterface.lastOpenedConfig and (typeof serviceInterface.lastOpenedConfig.executeScript != 'undefined')
        return serviceInterface.lastOpenedConfig.executeScript(
          _config.injectDetails,
          _config.callback
        )
      return


    serviceInterface.open = (config) ->
      ###
      Open a browser window with a desired URL.

      @param {object} config
        :url {string} "http://google.com"
        :target {string} "_self", "_blank", "_system"
        :options {array} ["location=yes"] @see https://github.com/apache/cordova-plugin-inappbrowser#cordovainappbrowseropen
      ###

      _config = config or {}

      _options = _config.options or null
      _options.push('closebuttoncaption=◂')
      _config.options = if _options then _options.join('&') else null

      serviceInterface.lastOpenedConfig = serviceInterface.InAppBrowser.open(_config.url, _config.target, _config.options)


    serviceInterface


  __interface__.
  service 'browser.service', [
    '$window'
    browserService
  ]
