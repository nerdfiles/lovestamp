
/*
 * fileOverview

     ____  _____ ____
    |    \(____ |  _ \
    | | | / ___ | |_| |
    |_|_|_\_____|  __/
                |_|

@module services  

 *# description

An map service for angular-leaflet-directive that represents a library of 
tools for creating Material Design configurations within Leaflet Maps.
 */

(function() {
  define(['interface', 'config', 'lodash', 'one-color', 'leaflet-knn', 'parse-address', 'utils/firebase', 'services/authentication'], function(__interface__, environment, _, color, leafletKnn, parseAddress) {
    var mapService;
    mapService = function($http, $timeout, $mdToast, $window, loggly, leafletData, $q, firebaseUtils, userRequire) {

      /*
      Client-side service for map rendering and painting.
       */
      var serviceInterface;
      serviceInterface = {
        Owlish: {},
        Falconry: {}
      };
      serviceInterface.storedDistancesObject = null;
      serviceInterface.colls = [];
      serviceInterface.defaultAddresses = [];
      serviceInterface.f = void 0;
      serviceInterface.getAddressByAlias = function(alias) {

        /*
        @param {string} alias  
        @return {Promise:string}
         */
        var def, sep, _userRequire;
        sep = '/';
        def = $q.defer();
        _userRequire = userRequire();
        _userRequire.then(function(authData) {
          var handle, _f;
          handle = authData.handle;
          _f = firebaseUtils.syncArray('users' + sep + handle + sep + 'stamps/local');
          _f.$loaded().then(function(localStamps) {
            var _localStamps;
            _localStamps = _.filter(localStamps, function(stamp) {
              if (alias === stamp.merchantAlias) {
                return stamp;
              }
            });
            if (_localStamps.length) {
              def.resolve({
                $meta: _.last(_localStamps),
                alias: alias
              });
            } else {
              def.reject(false);
            }
          });
        });
        return def.promise;
      };
      serviceInterface.Owlish.verifyAddress = function(addressString) {

        /*
        Process address string and verify address against Google's Geocoding API.
        
        @param {string} addressString
        @namespace verifyAddress
        @return {Promise}
         */
        var a, address, baseAddressApi, combinedAddress, formattedAddress, key, prefix_address, prefix_key, urlApi, _address, _addressString, _tmpAddress;
        key = environment.google.maps.key;
        urlApi = environment.google.maps.geocode;
        prefix_address = 'address=';
        prefix_key = 'key=';
        _addressString = addressString.split('\n');
        formattedAddress = _addressString.join(' ');
        _tmpAddress = parseAddress(formattedAddress);
        _address = _.map(_tmpAddress, function(addressComponent, keyname) {
          if (keyname === 'text') {
            return addressComponent;
          }
        });
        address = _.last(_.filter(_address, function(addressAtom) {
          return addressAtom;
        }));
        a = address.replace(/\ /g, '+');
        combinedAddress = [prefix_address + a, prefix_key + key].join('&');
        baseAddressApi = [urlApi, combinedAddress].join('?');
        console.log(baseAddressApi);
        return $http.get(baseAddressApi);
      };
      serviceInterface.Falconry.clearMap = function(pathConstruct, map) {

        /*
        @namespace clearMap
        @return {undefined}
         */
        map.removeLayer(pathConstruct);
      };
      serviceInterface.customerCowPath = void 0;
      serviceInterface.Falconry.Scout = function(_linksActivityTracking, map) {

        /**
        Show Links Activity Tracking
        
        @event ngClick
        @namespace showLinksActivityTracking
        @return {undefined}
         */
        var customerCowPath, linksActivityTracking, ___linksActivityTracking, __linksActivityTracking, _bounds;
        linksActivityTracking = _linksActivityTracking;
        __linksActivityTracking = _.filter(linksActivityTracking, function(personalIntensityLink) {
          return personalIntensityLink.merchant.personalIntensityQuota;
        });
        ___linksActivityTracking = _.map(__linksActivityTracking, function(locationObject) {
          return {
            lat: locationObject.location.lat,
            lng: locationObject.location.lng
          };
        });
        if (serviceInterface.customerCowPath) {
          serviceInterface.Falconry.clearMap(serviceInterface.customerCowPath, map);
          serviceInterface.customerCowPath = void 0;
        } else {
          customerCowPath = L.polyline(___linksActivityTracking, {
            color: '#006EB7',
            dashArray: '5,5',
            noClip: true,
            smoothFactor: .5
          });
          serviceInterface.customerCowPath = customerCowPath.addTo(map);
          _bounds = serviceInterface.customerCowPath.getBounds();
          map.fitBounds(_bounds);
        }
        return map.invalidateSize();
      };
      serviceInterface.checkedMerchant = false;
      serviceInterface.Falconry.MerchantGate = function(merchantMarket, pointPath) {

        /**
        Cycle through merchant stamp within bounds.
        Merchants may Walk Mode a stamp such that they tether via the Internet 
        a stamp to their device's geocoordinates.
        
        @param {array} pointPath @use $scope.linkActivityTracking after init.
         */
        var def;
        def = $q.defer();
        _getLocation.then(function(data) {
          return merchantMarket.then(function(d) {
            var $latitude_y, $longitude_x, $points_polygon, $vertices_x, $vertices_y, i, j, _ref;
            pointPath = pointPath || d;
            $vertices_x = _.map(pointPath, function(arrayLike) {
              return arrayLike.lat;
            });
            $vertices_y = _.map(pointPath, function(arrayLike) {
              return arrayLike.lng;
            });
            $points_polygon = $vertices_x.length;
            $latitude_y = data.coords.latitude;
            $longitude_x = data.coords.longitude;
            i = j = serviceInterface.checkedMerchant = 0;
            i = 0;
            j = $points_polygon - 1;
            while (i < $points_polygon) {
              if ((($vertices_y[i] > $latitude_y && $latitude_y !== (_ref = $vertices_y[j])) && _ref > $latitude_y) && $longitude_x < ($vertices_x[j] - $vertices_x[i]) * ($latitude_y - $vertices_y[i]) / ($vertices_y[j] - $vertices_y[i]) + $vertices_x[i]) {
                serviceInterface.checkedMerchant = !serviceInterface.checkedMerchant;
              }
              j = i++;
            }
            return def.resolve(serviceInterface.checkedMerchant);
          });
        });
        return def.promise;
      };
      serviceInterface.Falconry.Ringer = function() {
        return {

          /*
          @namespace Ringer
          @description
          
              Asynchronously coloring map markers' intensity circles.
           */
          defaultColorSetting: {
            lightness: .90,
            alphaActive: .75,
            alphaInactive: .25,
            inactiveBlue: '#D3D3D3',
            inactiveYellow: '#D9D9D9',
            startBlue: '#4188c8',
            startYellow: '#edc71b',
            startRed: '#eb2227'
          },
          color: function(_merchantNode, callback) {
            var f, merchantStampImpressionBlue, merchantStampImpressionYellow, merchantToFriendActivity, merchantToPersonalActivity, p, _f, _p;
            _f = _merchantNode.friendIntensityQuota || 0;
            _p = _merchantNode.personalIntensityQuota || 0;

            /*
            @see Notes on perceptual availability in services/bank.
            
            (Math.log1p(.2) / Math.E) * 100
            6.707235242686742
            (Math.log1p(.5) / Math.E) * 100
            14.916227738534996
            (Math.log1p(.9) / Math.E) * 100
            23.612484895881916
             */
            f = (Math.log1p(_f) / Math.E) * 100;
            p = (Math.log1p(_p) / Math.E) * 100;
            merchantToPersonalActivity = this.defaultColorSetting.alphaInactive;
            merchantToFriendActivity = this.defaultColorSetting.alphaInactive;
            merchantStampImpressionYellow = color(this.defaultColorSetting.inactiveYellow).lightness(this.defaultColorSetting.lightness);
            merchantStampImpressionBlue = color(this.defaultColorSetting.inactiveBlue).lightness(this.defaultColorSetting.lightness);
            if (p > 0) {
              merchantStampImpressionYellow = color(this.defaultColorSetting.startYellow).saturation(p);
              merchantToPersonalActivity = this.defaultColorSetting.alphaActive;
            }
            if (f > 0) {
              merchantStampImpressionBlue = color(this.defaultColorSetting.startBlue).saturation(f);
              merchantToFriendActivity = this.defaultColorSetting.alphaActive;
            }
            _.extend(_merchantNode, {
              personalActivity: merchantToPersonalActivity,
              friendActivity: merchantToFriendActivity,
              personalIntensityInactiveColor: '#D9D9D9',
              friendIntensityInactiveColor: '#D3D3D3',
              friendIntensityQuotaColor: merchantStampImpressionBlue.hex(),
              personalIntensityQuotaColor: merchantStampImpressionYellow.hex()
            });
            $timeout(function() {
              return callback(null, _merchantNode);
            }, 0);
          }
        };
      };
      return serviceInterface;
    };
    __interface__.service('center.service', [
      '$location', 'localStorageService', 'geolocation', '$timeout', function($location, localStorageService, geolocation, $timeout) {

        /*
        @usage
        
            centerService.save(lat: 50.5006, lng: 4.48649, zoom: 7, autoDiscover: false)
         */
        var locationParameters, serviceInterface;
        serviceInterface = {
          center: {}
        };
        locationParameters = $location.search();
        if (locationParameters.initLng !== void 0 && !isNaN(parseFloat(locationParameters.initLng)) && locationParameters.initLat !== void 0 && !isNaN(parseFloat(locationParameters.initLat)) && locationParameters.initZoom !== void 0 && !isNaN(parseInt(locationParameters.initZoom))) {
          serviceInterface.center = {
            lat: parseFloat(locationParameters.initLat),
            lng: parseFloat(locationParameters.initLng),
            zoom: parseInt(locationParameters.initZoom),
            autoDiscover: false
          };
        } else {
          if (localStorageService.keys().indexOf('center') > -1) {
            serviceInterface.center = JSON.parse(localStorageService.get('center'));
          }
        }
        serviceInterface.save = function(center) {
          center.lat = parseFloat(center.lat);
          center.lng = parseFloat(center.lng);
          localStorageService.set('center', center);
          _.extend(serviceInterface.center, center);
        };
        return serviceInterface;
      }
    ]);
    return __interface__.factory("map.service", ["$http", "$timeout", "$mdToast", "$window", 'loggly', 'leafletData', '$q', 'firebase.utils', 'user.require', mapService]);
  });

}).call(this);

//# sourceMappingURL=map.js.map
