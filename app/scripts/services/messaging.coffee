###
# fileOverview

                                       _
                                      (_)
     ____  _____  ___  ___ _____  ____ _ ____   ____
    |    \| ___ |/___)/___|____ |/ _  | |  _ \ / _  |
    | | | | ____|___ |___ / ___ ( (_| | | | | ( (_| |
    |_|_|_|_____|___/(___/\_____|\___ |_|_| |_|\___ |
                                (_____|       (_____|

@module services
## description

An in-app messaging service, passing messages to node-mailer.

###

define [
  'interface'
  'utils/firebase'
], (__interface__) ->

  messagingService = (firebaseUtils, $timeout, $q, $http) ->

    serviceInterface = {}

    serviceInterface.sendMessage = () ->
      return

    serviceInterface.updateMessage = () ->
      return

    serviceInterface

  __interface__

    .factory 'messaging.service', [
      "firebase.utils"
      "$timeout"
      "$q"
      "$http"
      messagingService
    ]
