###
# fileOverview
                                _ 
                               | |
      ___  ___  _   _ ____   __| |
     /___)/ _ \| | | |  _ \ / _  |
    |___ | |_| | |_| | | | ( (_| |
    (___/ \___/|____/|_| |_|\____|

## description

A cross-browser sound service with fallback to emulate in browser.

###

define [
  'interface'
], (__interface__) ->

  window.ua = navigator.userAgent.toLowerCase()
  window.isAndroid = /android/.test(ua)
  window.isiOs = /(iphone|ipod|ipad)/.test(ua)
  window.isStandAlone = window.navigator.standalone
  window.isiPad = ua.match(/ipad/)
  window.isDevice = 'ontouchstart' in window
  window.isChrome = "chrome" in window
  window.isMoz = "mozAnimationStartTime" in window

  window.Media = (src, mediaSuccess, mediaError, mediaStatus) ->
    ###
    @param {string} src
      A URI containing the audio content. (DOMString)
    @param {function} mediaSuccess (Optional)
      The callback that executes after a Media object has completed the current play, record, or stop action. (Function)
    @param {function} mediaError (Optional)
      The callback that executes if an error occurs. (Function)
    @param {function} mediaStatus (Optional)
      The callback that executes to indicate status changes. (Function)
    ###

    if typeof Audio != 'function' and typeof Audio != 'object'
      console.warn 'HTML5 Audio is not supported in this browser'

    sound = new Audio
    sound.src = src
    sound.addEventListener 'ended', mediaSuccess, false
    sound.load()
    {
      getCurrentPosition: (mediaSuccess, mediaError) ->
        mediaSuccess sound.currentTime
        return
      getDuration: ->
        if isNaN(sound.duration) then -1 else sound.duration
      play: ->
        sound.play()
        return
      pause: ->
        sound.pause()
        return
      release: ->
      seekTo: (milliseconds) ->
      setVolume: (volume) ->
        sound.volume = volume
        return
      startRecord: ->
      stopRecord: ->
      stop: ->
        sound.pause()
        if mediaSuccess
          mediaSuccess()
        return

    }

  soundService = ($q, $window) ->
    ###
    ###

    serviceInterface = {}


    _logError = (src, err) ->
      console.log src
      console.error 'media error',
        code    : err.code
        message : getErrorMessage(err.code)
      return


    serviceInterface.loadMedia = (src, onError, onStatus, onStop) ->

      def = $q.defer()

      if isMoz
        postFixExtension = '.ogg'
      else
        postFixExtension = '.mp3'

      src = src + postFixExtension

      if window.is_device
        document.addEventListener('deviceready', ->

          mediaSuccess = ->
            if onStop
              onStop()
            return

          mediaError = (err) ->
            _logError src, err
            if onError
              console.log(onError(err))
            return

          mediaStatus = (status) ->
            if onStatus
              console.log(onStatus(status))
            return

          #if ionic.Platform.isAndroid() or ionic.Platform.isIOS()
          src = './' + src

          def.resolve new ($window.Media)(src, mediaSuccess, mediaError, mediaStatus)
          return
        )

      else

        mediaSuccess = ->
          if onStop
            onStop()
          return

        mediaError = (err) ->
          _logError src, err
          if onError
            console.log(onError(err))
          return

        mediaStatus = (status) ->
          if onStatus
            console.log(onStatus(status))
          return

        def.resolve new ($window.Media)(src, mediaSuccess, mediaError, mediaStatus)

      def.promise


    serviceInterface.getStatusMessage = (status) ->
      message = null
      if status == 0
        message = 'Media.MEDIA_NONE'
      else if status == 1
        message = 'Media.MEDIA_STARTING'
      else if status == 2
        message = 'Media.MEDIA_RUNNING'
      else if status == 3
        message = 'Media.MEDIA_PAUSED'
      else if status == 4
        message = 'Media.MEDIA_STOPPED'
      else
        message = 'Unknown status <' + status + '>'
      message


    serviceInterface.getErrorMessage = (code) ->
      message = null
      if code == 1
        message = 'MediaError.MEDIA_ERR_ABORTED'
      else if code == 2
        message = 'MediaError.MEDIA_ERR_NETWORK'
      else if code == 3
        message = 'MediaError.MEDIA_ERR_DECODE'
      else if code == 4
        message = 'MediaError.MEDIA_ERR_NONE_SUPPORTED'
      else
        message = 'Unknown code <' + code + '>'
      message

    serviceInterface

  __interface__.
    factory 'sound.service', [
      '$q'
      '$window'
      soundService
    ]
