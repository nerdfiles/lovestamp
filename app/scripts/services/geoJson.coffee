###
# fileOverview

                       _______                
                      (_______)               
      ____ _____  ___      _  ___  ___  ____  
     / _  | ___ |/ _ \ _  | |/___)/ _ \|  _ \ 
    ( (_| | ____| |_| | |_| |___ | |_| | | | |
     \___ |_____)\___/ \___/(___/ \___/|_| |_|
    (_____|                                   

## description

  GeoJson utilities wrapper for on-demand loading.

###

define [
  "interface"
  'geojson-tools-src'
], (__interface__, geoJsonTools) ->

  serviceInterface = () ->
    ###
    @ngdoc factory  
    @name interface.factory:geoJsonUtils  
    @function geoJsonUtils  
    @description

      GeoJson utilities.

    @return geoJsonUtils
    ###

    geoJsonTools

  # Implement geoJsonUtils.
  __interface__.
    factory('geoJsonUtils', [
      serviceInterface
    ])
