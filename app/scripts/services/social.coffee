###
# fileOverview

                      _       _
                     (_)     | |
      ___  ___   ____ _ _____| |
     /___)/ _ \ / ___) (____ | |
    |___ | |_| ( (___| / ___ | |
    (___/ \___/ \____)_\_____|\_)

@module services

## description

Potential actions for enabling users to create "friend" relations and properties.

###

define [
  'interface'
  'utils/firebase'
], (__interface__) ->

  socialService = ($firebaseUtils, $timeout, $q) ->
    serviceInterface = {}
    serviceInterface.addFriend = () ->
      return
    serviceInterface.removeFriend = () ->
      return
    serviceInterface.favoriteMerchant = () ->
      return
    serviceInterface.unfavoriteMerchant = () ->
      return
    serviceInterface.postToProfile = () ->
      return
    serviceInterface

  __interface__

    .factory 'social.service', [
      "firebase.utils"
      "$timeout"
      "$q"
      socialService
    ]
