
/*
 * fileOverview

        _
  ___ _| |_ _____ ____  ____
 /___|_   _|____ |    \|  _ \
|___ | | |_/ ___ | | | | |_| |
(___/   \__)_____|_|_|_|  __/
                       |_|

 *# description

Stamp service loads unique stamps for the given dashboard and facilitates 
stamp platform controllers.
 */

(function() {
  define(["interface", "lodash", "numeral", "config", "services/profile", "services/notification", "services/crypto", 'utils/firebase'], function(__interface__, _, numeral, environment) {
    var purseService, stampService;
    stampService = function(firebaseUtils, $http, $q, userRequire, profileService, notificationService, cryptoService, $timeout) {
      var serviceInterface;
      serviceInterface = {};
      serviceInterface.localValidatorStamps = [];
      serviceInterface.collisionStore = [];
      serviceInterface.primedStamps = {};
      serviceInterface.getStamp = function(stamp, username) {

        /*
        Get Stamp by Username
        @namespace getStamp
         */
        if (serviceInterface.primedStamps[stamp.$id]) {
          return serviceInterface.primedStamps[stamp.$id];
        }
        return serviceInterface.primedStamps[stamp.$id] = firebaseUtils.syncObject('users/' + username + '/stamps/local/' + stamp.$id);
      };
      serviceInterface.getAllStamps = function() {

        /*
        Get All Stamps
        
        @namespace getAllStamps
        @TODO Refactor by filtering Users' local stamp data.
         */
        return firebaseUtils.syncArray('users');
      };
      serviceInterface.getLatestBitsCollected = function(stampId, handle) {

        /*
        @namespace getLatestBitsCollected
        @param {string} stampId
        @return {Promise} bitsReport
         */
        var checked, sep, _def;
        _def = $q.defer();
        checked = false;
        sep = '/';
        $timeout(function() {
          var user;
          user = firebaseUtils.syncArray('users' + sep + handle);
          return user.$loaded().then(function(usersData) {
            _.forEach(usersData, function(v, k) {
              if (v && v.stats && v.stats.success) {
                return _.forEach(v.stats.success, function(sv, sk) {
                  if (sk === stampId) {
                    checked = true;
                    return _def.resolve(sv);
                  }
                });
              }
            });
            if (!checked) {
              _def.reject(false);
            }
          });
        }, 1000);
        return _def.promise;
      };
      serviceInterface.getMerchantBitsTipped = function(localStamp) {

        /*
        @namespace getMerchantBitsTipped
        @description
        
            Augmenting merchant bottomsheet data with the total number of bits 
            tipped per fan (customer) which is an aggregate of all the merchant 
            stampenings to that customer's profile.
        
        @return {Promise} bitsTipped
         */
        var addressOfLocalStamp, def, hashedAddressOfLocalStamp, id, listOfBits, loc, merchantStatus, stampOwner, _userRequire;
        def = $q.defer();
        id = localStamp.$id;
        addressOfLocalStamp = localStamp.address;
        loc = _.toArray(addressOfLocalStamp.location);
        hashedAddressOfLocalStamp = cryptoService.hash(JSON.stringify(loc.join(',')));
        stampOwner = localStamp.stampOwner;
        _userRequire = userRequire();
        merchantStatus = profileService.getMerchantStatus(stampOwner);
        listOfBits = [];
        _userRequire.then(function(userData) {
          var handle;
          handle = userData.handle;
          return merchantStatus.$loaded().then(function(merchantData) {
            var d, l, _result;
            _result = 0;
            d = merchantData.stamps.local;
            if (merchantData.stamps && merchantData.stamps.local) {
              _.forEach(d, function(a, _id) {
                var cb, hashedAddress, stampHistoryList;
                if (!a.address) {
                  return;
                }
                loc = _.toArray(a.address.location);
                hashedAddress = cryptoService.hash(JSON.stringify(loc.join(',')));
                if (hashedAddress === hashedAddressOfLocalStamp) {
                  stampHistoryList = profileService.getStampHistory(handle, _id);
                  cb = function() {
                    var _def;
                    _def = $q.defer();
                    stampHistoryList.$loaded().then(function(history) {
                      _.forEach(history, function(n) {
                        if (n && n.bitsCollected && _.isNumber(n.bitsCollected)) {
                          return _result = _result + parseFloat(n.bitsCollected);
                        }
                      });
                      return _def.resolve(_result);
                    });
                    return _def.promise;
                  };
                  return listOfBits.push(cb());
                }
              });
              l = $q.all(listOfBits);
              return l.then(function(data) {
                _result = _.first(data);
                return def.resolve(numeral(_result).format('0.00'));
              });
            }
          });
        });
        return def.promise;
      };
      serviceInterface.getStampBitsTipped = function(localStamp) {

        /*
        @namespace getStampBitsTipped
        @description
        
          Copy of getMerchantBitsTipped.
         */
        return serviceInterface.getMerchantBitsTipped(localStamp);
      };
      serviceInterface.getStampIdBySerial = function(serial) {

        /*
        @namespace getStampIdBySerial
        @description
        
          Get stamps by ID from a particular SnowShoeStamp serial number.
        
        @param {string} serial
        @return {Promise} stampSerial
         */
        var def, found, users;
        def = $q.defer();
        found = false;
        users = firebaseUtils.syncArray('users');
        users.$loaded().then(function(usersData) {
          return _.forEach(usersData, function(v, k) {
            if (v.stamps && v.stamps.local && found === false) {
              return _.forEach(v.stamps.local, function(lv, lk) {
                if (serial === lv.receipt.validation.stamp.serial) {
                  def.resolve(lk);
                  return found = true;
                }
              });
            }
          });
        });
        return def.promise;
      };

      /*
      @namespace verifiedAddressConstruct
      @param {object} stamp
      @return {Promise} Pick yr API, be it SmartyStreets or Google or whatever else.
       */
      serviceInterface.verifiedAddressConstruct = function(baseAddressApi, stamp) {
        var def, zipCodeComponent;
        def = $q.defer();
        zipCodeComponent = stamp.address.zipCode;
        $http.get(baseAddressApi).success(function(verifiedAddress) {
          var longNames, sn, types, _verifiedAddress, _verifiedGeometry, _verifiedLocation;
          _verifiedAddress = verifiedAddress.results[0].address_components;
          _verifiedGeometry = verifiedAddress.results[0].geometry;
          _verifiedLocation = _verifiedGeometry.location;
          types = _.map(_.pluck(_verifiedAddress, 'types'), function(types) {
            return _.first(types);
          });
          longNames = _.pluck(_verifiedAddress, 'long_name');
          _verifiedAddress = _.zipObject(types, longNames);
          sn = _verifiedAddress.street_number ? _verifiedAddress.street_number : '';
          stamp.address.line1 = sn + ' ' + _verifiedAddress.route;
          stamp.address.line2 = '';
          stamp.address.zipCode = _verifiedAddress.postal_code || '';
          stamp.address.cityLabel = _verifiedAddress.locality;
          stamp.address.stateLabel = _verifiedAddress.administrative_area_level_1;
          stamp.address.countryLabel = _verifiedAddress.country;
          stamp.address.location = _verifiedLocation;
          return def.resolve(stamp);
        });
        return def.promise;
      };

      /*
      @namespace updateStampAddress
      @param {object} stamp
      @param {string}
       */
      serviceInterface.updateStampAddress = function(stamp, user, dragUpdate) {
        var address, baseAddressApi, combinedAddress, def, key, prefix_address, prefix_key, urlApi, _city, _country, _line1, _line2, _location, _state, _tmpAddress;
        key = environment.google.maps.key;
        urlApi = environment.google.maps.geocode;
        prefix_address = 'address=';
        prefix_key = 'key=';
        if (!dragUpdate) {
          _line1 = stamp.address.line1 || null;
          _line2 = stamp.address.line2 || null;
          _city = stamp.address.cityLabel || null;
          _state = stamp.address.stateLabel || null;
          _country = stamp.address.countryLabel || null;
          _tmpAddress = [_line1, _line2, _city, _state, _country].join('+');
          address = _tmpAddress;
          combinedAddress = [prefix_address + address, prefix_key + key].join('&');
        } else {
          _location = stamp.address.location;
          _tmpAddress = [_location.lat, _location.lng].join(',');
          address = _tmpAddress;
          combinedAddress = [prefix_address + address, prefix_key + key].join('&');
        }
        baseAddressApi = [urlApi, combinedAddress].join('?');
        def = $q.defer();
        serviceInterface.verifiedAddressConstruct(baseAddressApi, stamp).then(function(updatedStamp) {
          var _stamp;
          _stamp = updatedStamp;
          def.resolve(_stamp);
          if (_.isString(user)) {
            return serviceInterface.updateStamp(user, _stamp);
          } else {
            return user.then(function(authData) {
              var handle;
              handle = authData.handle;
              return serviceInterface.updateStamp(handle, _stamp);
            });
          }
        });
        return def.promise;
      };

      /*
      @namespace requestForUser
       */
      serviceInterface.requestForUser = function(username, requestConstruct) {
        var prefix, requestEndpoint;
        prefix = window.is_device ? '/data/' : '';
        requestEndpoint = prefix + '/stamp/' + username + '/request';
        return $http.post(requestEndpoint, requestConstruct).success(function(data, status, headers, config) {}).error(function(data, status, headers, config) {});
      };

      /*
      Search Stamps by Query
      @namespace searchStampsByQuery
       */
      serviceInterface.stampList = {};

      /*
      Get Local Stamps by Username
      @namespace getStampList
       */
      serviceInterface.getStampList = function(username) {
        if (serviceInterface.stampList[username]) {
          return serviceInterface.stampList[username];
        }
        return serviceInterface.stampList[username] = firebaseUtils.syncArray('users/' + username + '/stamps/local');
      };
      serviceInterface.localStampList = {};

      /*
      Get Local Stamps by Username
      @namespace getLocalStamps
       */
      serviceInterface.getLocalStamps = function(username) {
        if (serviceInterface.localStampList[username]) {
          return serviceInterface.localStampList[username];
        }
        return serviceInterface.localStampList[username] = firebaseUtils.syncArray('users/' + username + '/stamps/local');
      };

      /*
      Add Local Stamp to User Profile
      @namespace addStamp
       */
      serviceInterface.addStamp = function(localStampConstruct) {
        var alias, birth, passes, receipt, username;
        username = localStampConstruct.username;
        passes = localStampConstruct.passes;
        alias = localStampConstruct.alias;
        receipt = localStampConstruct.receipt;
        birth = localStampConstruct.birth;
        if (!passes.length || !passes) {
          return;
        }
        return firebaseUtils.pushObject("users/" + username + "/stamps/local", localStampConstruct);
      };

      /*
      Update Local Stamp
      @namespace updateStamp
       */
      serviceInterface._tempLocals = {};
      serviceInterface.updateStamp = function(handle, stamp) {
        var defaultActivationProgressStatus, e, updateObject, __id, _def, _id, _tempLocals;
        __id = null;
        _def = $q.defer();
        updateObject = void 0;
        if (serviceInterface._tempLocals[handle]) {
          _tempLocals = serviceInterface._tempLocals[handle];
        } else {
          _tempLocals = serviceInterface._tempLocals[handle] = firebaseUtils.syncArray("users/" + handle + "/stamps/local");
        }
        _tempLocals.$loaded().then(function(_locals) {
          return _.forEach(_locals, function(v, k) {
            if (v.alias === stamp.alias) {
              return _def.resolve(v.$id);
            }
          });
        });
        defaultActivationProgressStatus = 1;
        if (stamp && stamp.address && stamp.address.line2) {
          stamp.address.line2 = stamp.address.line2;
        }
        if (stamp.localStamp && stamp.localStamp.address) {
          stamp.localStamp.address.line2 = stamp.localStamp.address.line2;
        }
        try {
          updateObject = angular.extend({}, {
            active: stamp.active || stamp.localStamp ? true : false,
            merchantAlias: stamp.merchantAlias ? stamp.merchantAlias : stamp.localStamp && stamp.localStamp.merchantAlias ? stamp.localStamp.merchantAlias : '',
            activity: stamp.activity ? stamp.activity : stamp.localStamp && stamp.localStamp.activity ? stamp.localStamp.activity : '',
            defaultAddress: stamp.defaultAddress ? stamp.defaultAddress : stamp.localStamp && stamp.localStamp.defaultAddress ? stamp.localStamp.defaultAddress : stamp.merchantAlias || stamp.alias || stamp.$id,
            address: stamp.address ? stamp.address : stamp.localStamp.address,
            alias: stamp.alias ? stamp.alias : stamp.localStamp.alias,
            days: stamp.days ? stamp.days : stamp.localStamp.days,
            hours: stamp.hours ? stamp.hours : stamp.localStamp.hours,
            statusSaturation: stamp.statusSaturation ? stamp.statusSaturation : stamp.localStamp && stamp.localStamp.activationProgressStatus ? stamp.localStamp.activationProgressStatus : defaultActivationProgressStatus
          });
        } catch (_error) {
          e = _error;
          console.log(e);
        }
        _id = stamp.$id || stamp.id;
        if (!_id) {
          return _def.promise.then(function(id) {
            _id = id;
            return firebaseUtils.updateObject("users/" + handle + "/stamps/local/" + _id, updateObject);
          });
        } else {
          return firebaseUtils.updateObject("users/" + handle + "/stamps/local/" + _id, updateObject);
        }
      };

      /*
      Assign Stamp
      @namespace assignStamp
      
      Primarily for admins.
       */
      serviceInterface.assignStamp = function(handle, localStampConstruct) {
        var oldPath, _localStampConstruct, _localStampId, _newOwner, _oldOwner;
        _newOwner = handle.$id;
        _localStampConstruct = localStampConstruct.localStamp;
        _localStampConstruct.statusSaturation = 1;
        _localStampId = localStampConstruct.id;
        _oldOwner = localStampConstruct.user.$id;
        oldPath = "users/" + _oldOwner + ("/stamps/local/" + _localStampId);
        firebaseUtils.removeObject(oldPath);
        return firebaseUtils.pushObject("users/" + _newOwner + "/stamps/local", _localStampConstruct);
      };

      /*
      Link Stamp to user
       */
      serviceInterface.lock = function(stampAlias, username) {
        var allStamps, s, _localStampId, _newOwner, _oldOwnerUsername;
        _newOwner = username;
        _localStampId = null;
        _oldOwnerUsername = null;
        s = null;
        allStamps = serviceInterface.getAllStamps();
        allStamps.$loaded().then(function(users) {
          var oldPath, _oldOwner;
          _.forEach(users, function(u) {
            var hasRole;
            hasRole = u.role && u.role["default"];
            if (u.stamps && u.stamps.local && hasRole === 'admin') {
              return _.forEach(u.stamps.local, function(stamp, keyName) {
                var _registeredDate;
                if (stamp.alias === stampAlias) {
                  _registeredDate = moment();
                  stamp.registeredDate = _registeredDate.format("MM/DD/YYYY HH:mm:ss");
                  stamp.statusSaturation = 2;
                  s = stamp;
                  _localStampId = keyName;
                  return _oldOwnerUsername = u.$id;
                }
              });
            }
          });
          if (!_localStampId) {
            return notificationService.displayError('Lock unsuccessful');
          }
          _oldOwner = _oldOwnerUsername;
          oldPath = "users/" + _oldOwner + "/stamps/local/" + _localStampId;
          firebaseUtils.removeObject(oldPath);
          firebaseUtils.pushObject("users/" + _newOwner + "/stamps/local", s);
        });
      };
      return serviceInterface;
    };
    purseService = function(firebaseUtils, $http, $q, userRequire, profileService, notificationService, cryptoService) {
      var serviceInterface;
      serviceInterface = {
        purse: []
      };
      serviceInterface.drag = function(item, handle) {

        /*
        Reset the pending pool's purse on drag events, analogous event on frontend to the CT POST bypass.
        
        A tip URL should be created after drag.
         */
        serviceInterface.reset(handle);
        serviceInterface.add(item, {
          drag: true
        }, handle);
      };
      serviceInterface.reset = function(handle) {

        /*
        Reset the pending pool.
         */
        var profileRoot, sep;
        sep = '/';
        serviceInterface.purse.length = 0;
        profileRoot = firebaseUtils.syncObject('users' + sep + handle + sep('profile'));
        profileRoot.set({
          purse: 0.00
        });
      };
      serviceInterface.add = function(item, dragStatus, handle) {

        /*
        Add to pending pool under both drag and stamp success.
         */
        var updatedPurse;
        if (dragStatus && dragStatus.drag === true) {
          return;
        } else {
          serviceInterface.load(item);
        }
        updatedPurse = $q.all(serviceInterface.purse);
        updatedPurse.then(function(purseObject) {
          serviceInterface.purse.length = 0;
        });
      };
      serviceInterface.load = function(item) {

        /*
        Load a request to the pending pool.
         */
        serviceInterface.purse.push(item);
      };
      return serviceInterface;
    };
    return __interface__.factory("stamp.service", ["firebase.utils", "$http", "$q", "user.require", "profile.service", "notification.service", "crypto.service", "$timeout", stampService]).factory("purse.service", ["firebase.utils", "$http", "$q", "user.require", "profile.service", "notification.service", "crypto.service", purseService]);
  });

}).call(this);

//# sourceMappingURL=stamp.js.map
