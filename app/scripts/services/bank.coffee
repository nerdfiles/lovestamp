###
# fileOverview

     _                 _
    | |               | |
    | |__  _____ ____ | |  _
    |  _ \(____ |  _ \| |_/ )
    | |_) ) ___ | | | |  _ (
    |____/\_____|_| |_|_| \_)

@module services

## description

A series of abstractions for our controllers, be they many and slim.

###

define [
    "interface"
    'lodash'
    'utils/firebase'
    "services/profile"
    "services/notification"
    "services/stamp"
], (__interface__, _) ->

    bankService = (firebaseUtils, $rootScope, $http, $q, $timeout, profileService, notificationService, $window, stampService) ->
        ###
        Bank service.
        ###

        serviceInterface = {}

        serviceInterface.currencyFormat = '0,0.00'
        serviceInterface.bitsFormat = '0.00'

        serviceInterface.merchantsLocallyApplied = undefined

        serviceInterface.y = 0
        serviceInterface.b = 0
        serviceInterface.r = 0


        startBudgetCron = () ->
            ###
            @inner
            ###

            $http.get("https://lovestamp.io/budget/roll-up")


        captureGlobalUserList = () ->
          ###
          @inner
          ###
          def = $q.defer()
          stampService.getAllStamps().$loaded().then (userData) ->
            globalUserList = []
            _.forEach(userData, (user, keyUsername) ->
              if user.stamps and user.stamps.local and user.stamps.local.stampImpressionsRed
                globalUserList.push user
            )
            def.resolve globalUserList
          def.promise


        getIntensityMerchant = (_merchants) ->
            ###
            All merchants within a given viewport. Find their respective 
            intensities based on aggregate impressions.

            @param {array} _merchants
            ###

            accum = []

            # Filter merchants
            merchants = _.filter _merchants, (user) ->
                user.stamps and user.stamps.local

            # Create factor
            t = []
            _.forEach merchants, (_merchant) ->
                return  if ! _merchant.stamps
                return  if ! _merchant.stamps.stats
                return  if ! _merchant.stamps.stats.local
                t.push(parseInt(_merchant.stamps.stats.local.stampImpressionsRed, 10))

            totalMerchantsStampImpressionsRed = _.reduce t, (sum, n) ->
                sum + n

            # Augment merchants with intensity calculation
            _.forEach(merchants, (_merchant) ->
                def = $q.defer()
                if ! _merchant
                    return
                handle = _merchant.$id
                merchantsLocallyApplied = firebaseUtils.syncObject('users/' + handle + '/stamps/stats/local/stampImpressionsRed')
                merchantsLocallyApplied.$loaded().then (totalStampImpressionsRed) ->
                    val = ((totalStampImpressionsRed.$value / totalMerchantsStampImpressionsRed) * 100)
                    serviceInterface.setHighestRed val
                    _.extend _merchant,
                        merchantIntensityQuota: val
                    def.resolve _merchant
                accum.push def.promise
            )

            $q.all accum

        getIntensityFriendAndPersonal = (handle, _stamps, totalMerchantSystem) ->
            ###
            @param {string} handle
            @param {array} _array
            @param {array} totalMerchantSystem
            ###

            # Geocached stamps.
            accum = []
            console.dir _stamps

            # Augment merchants with intensity calculation
            _.forEach(_stamps, (_localStamp) ->

                _def = $q.defer()
                keyName = _localStamp.$id
                stampImpressionsRedOwner = _localStamp.stampOwner
                userStatus = profileService.getUserStatus stampImpressionsRedOwner

                userStatus.$loaded().then((user) ->

                    # Initialize intensity data for all stamps
                    _.extend _localStamp,
                        personalIntensityQuota: 0
                        friendIntensityQuota: 0

                    _def.resolve(_localStamp)

                    if user.stamps and !user.stamps.stats

                        # Initialize intensity data for all stamps
                        _.extend _localStamp,
                            personalIntensityQuota: 0
                            friendIntensityQuota: 0

                        _def.resolve(_localStamp)

                    else

                        if user.stamps and user.stamps.stats and !user.stamps.stats.local
                            return
                        stampImpressionsRed = parseInt(user.stamps.stats.local.stampImpressionsRed, 10)

                        ySum = 0
                        bSum = 0

                        # Broad aggregation for stamps without data per user
                        _.forEach totalMerchantSystem, (successfulStampenings) ->
                            if successfulStampenings and successfulStampenings.stamps and successfulStampenings.stamps.stats
                                _.forEach successfulStampenings.stamps.stats.success, (stampObject) ->
                                    return  if ! stampObject
                                    bSum = bSum + parseInt(stampObject.stampImpressionsBlue, 10)
                                    return
                            return

                        _.forEach totalMerchantSystem, (successfulStampenings) ->
                            if successfulStampenings and successfulStampenings.stamps and successfulStampenings.stamps.stats
                                _.forEach successfulStampenings.stamps.stats.success, (stampObject) ->
                                    return  if ! stampObject
                                    ySum = ySum + parseInt(stampObject.stampImpressionsYellow, 10)
                                    return
                            return

                        _.forEach totalMerchantSystem, (merchantSystem) ->
                            if merchantSystem and merchantSystem.stamps and merchantSystem.stamps.stats and merchantSystem.stamps.stats.success
                                _.forEach merchantSystem.stamps.stats.success[_localStamp.$id], (ms) ->
                                    _.extend _localStamp,
                                        personalIntensityQuota: ms.stampImpressionsYellow / stampImpressionsRed
                                        friendIntensityQuota: ms.stampImpressionsBlue / stampImpressionsRed
                                        #personalIntensityQuota: ySum / stampImpressionsRed
                                        #friendIntensityQuota: bSum / stampImpressionsRed

                        if _localStamp.stampOwner != handle
                            _def.resolve(_localStamp)

                        if serviceInterface.merchantsLocallyApplied
                          merchantsLocallyApplied = serviceInterface.merchantsLocallyApplied
                        else
                          merchantsLocallyApplied = serviceInterface.merchantsLocallyApplied = firebaseUtils.syncArray('users/' + handle + '/stamps/stats/success')

                        merchantsLocallyApplied.$loaded().then((stampsByColor) ->

                            aggregateStampImpressionsYellow = 0
                            aggregateStampImpressionsBlue = 0

                            # Aggregate yellow and blue
                            _.forEach stampsByColor, (n) ->
                                aggregateStampImpressionsYellow += n.stampImpressionsYellow or 0
                                aggregateStampImpressionsBlue += n.stampImpressionsBlue or 0

                            _.forEach(stampsByColor, (_stampByColor) ->
                                ((stampByColor) ->

                                  if keyName == _stampByColor.$id

                                      return  if ! user.stamps
                                      return  if ! user.stamps.stats
                                      return  if _.isNaN(user.stamps.stats.local.stampImpressionsRed)

                                      # For each local stamp of the merchant, check if stamp/stats/success has that stamp's ID
                                      _.forEach(user.stamps.local, (storedLocalStamp) ->
                                            return  if storedLocalStamp.alias == _localStamp.alias
                                            return  if !storedLocalStamp.address or !_localStamp.address

                                            l1 = _.toArray(storedLocalStamp.address.location)
                                            l2 = _.toArray(_localStamp.address.location)
                                            # Same address.
                                            if JSON.stringify(l1.join(',')) == JSON.stringify(l2.join(','))
                                                # Same addresses for stamps with Yellow ranking
                                                _personalIntensityQuota = aggregateStampImpressionsYellow / stampImpressionsRed
                                                _friendIntensityQuota = aggregateStampImpressionsBlue / stampImpressionsRed
                                            else
                                                # Different addresses for stamps with Yellow ranking
                                                _personalIntensityQuota = _stampByColor.stampImpressionsYellow / stampImpressionsRed
                                                _friendIntensityQuota = _stampByColor.stampImpressionsBlue / stampImpressionsRed

                                            if _.isNaN(_friendIntensityQuota)
                                              _friendIntensityQuota = 0

                                            if _.isNaN(_personalIntensityQuota)
                                              _personalIntensityQuota = 0

                                            serviceInterface.setHighestBlue _friendIntensityQuota
                                            serviceInterface.setHighestYellow _personalIntensityQuota

                                            _.extend _localStamp,
                                                personalIntensityQuota: _personalIntensityQuota
                                                friendIntensityQuota: _friendIntensityQuota

                                      )
                                )(_stampByColor)
                            )
                            _def.resolve _localStamp
                        )
                )
                accum.push _def.promise
            )

            $q.all accum


        ###
        @description

          Functions for setting maxima and minima colors for our 
          spectrum analysis of merchants' stamps.

        ###
        serviceInterface.setHighestYellow = (y) ->
            return serviceInterface.y  if not y
            if y > serviceInterface.y and ! _.isNaN y
                serviceInterface.y = y

        serviceInterface.setHighestBlue = (b) ->
            return serviceInterface.b  if not b
            if b > serviceInterface.b and ! _.isNaN b
                serviceInterface.b = b

        serviceInterface.setHighestRed = (r) ->
            return serviceInterface.r  if not r
            if r > serviceInterface.r and ! _.isNaN r
                serviceInterface.r = r

        serviceInterface.getHighestBlue = serviceInterface.setHighestBlue
        serviceInterface.getHighestYellow = serviceInterface.setHighestYellow
        serviceInterface.getHighestRed = serviceInterface.setHighestRed

        serviceInterface.getIntensityMerchant = getIntensityMerchant

        serviceInterface.getIntensityFriendAndPersonal = getIntensityFriendAndPersonal

        ###*
        @namespace getPayoutBudget
        @description

            Payout Budget shapes the upper and lower bounds for payouts.

        @return {Promise} payoutBudget
        ###
        serviceInterface.getPayoutBudget = ->
            firebaseUtils.syncObject('balances/payoutBudget')


        ###*
        @namespace updatePayoutBudget
        @description

            Update payout budget.

        @param {number} newPayoutBudget
        @return {Promise} balances/payoutBudget
        ###
        serviceInterface.updatePayoutBudget = (newPayoutBudget) ->
            firebaseUtils.updateObject 'balances',
                payoutBudget: newPayoutBudget


        ###*
        @namespace updateRevenue
        @description

            Update revenue.

        @param {number} newRevenueTotal
        @return {Promise} balances/revenue
        ###
        serviceInterface.updateRevenue = (newRevenueTotal) ->
            firebaseUtils.updateObject 'balances',
                revenue: parseInt(newRevenueTotal)


        ###*
        @namespace updateSetBudget
        @description

            Redact LoveStamp's current budget.

        @param {number} newSetBudget
        @return {object} A complex object with updates to set budget.
        ###
        serviceInterface.updateSetBudget = (newSetBudget) ->
            _aggregatePendingPoolVal = $q.defer()
            _aggregatePendingPool = 0
            b = parseInt(newSetBudget, 10)

            stampService.getAllStamps().$loaded().then (usersData) ->
                _.forEach usersData, (userData) ->


                    if userData.profile and userData.profile.purse
                        console.log userData
                        _purse = parseFloat(userData.profile.purse)
                        console.log _purse
                        _aggregatePendingPool = (_purse + parseFloat(_aggregatePendingPool))

                _aggregatePendingPoolVal.resolve _aggregatePendingPool

            _aggregatePendingPoolVal.promise.then (aggregatePurses) ->
                console.log aggregatePurses
                _b =
                    payoutShadowBudget : b - (if aggregatePurses then aggregatePurses else 0)
                    payoutBudget       : b
                    setBudget          : b
                    revenue            : 0

                firebaseUtils.updateObject('balances', _b)
            return


        ###*
        @namespace getRevenue
        @description

            Revenue of LoveStamp; may its sparks always fly upwards.

        ###
        serviceInterface.getRevenueTotal = ->
            firebaseUtils.syncObject('balances/revenue')


        ###*
        @namespace getSetBudget
        @description

            Manual budget setter.

        ###
        serviceInterface.getSetBudget = ->
            firebaseUtils.syncObject('balances/setBudget')


        serviceInterface.resetBudget = (newBudget, newRevenue) ->
            ###*
            "6.121 The propositions of logic demonstrate the logical properties of propositions by combining them so as to form propositions that say nothing."
            — TLP, L.W.

            Reset the ecosystem.

            ###

            newBudget = if newBudget then newBudget else 0
            newRevenue = if newRevenue then newRevenue else 0
            $http.get("https://lovestamp.io/budget/reset/#{newBudget}/#{newRevenue}")


        serviceInterface.callbackURL = null

        serviceInterface.getPurse = (handle) ->
          ###
          Get Purse

          Retrieve data on a particular Social Media User's (Fan) purse.
          ###

          purseGetUrl = 'users/' + handle + '/profile/purse'
          def = $q.defer()

          f = firebaseUtils.syncObject(purseGetUrl)

          f.$loaded().then (purseData) ->

            def.resolve {
                value: numeral(purseData.$value).format serviceInterface.currencyFormat
            }

          portSpec = if $window.location.port != "" then (':' + $window.location.port) else ''
          apiPrefix = if $window.is_device or portSpec == '' then '/data' else ''
          apiBase = if $window.is_device then 'https://lovestamp.io/data' else ($window.location.protocol + "//" + $window.location.hostname + portSpec + apiPrefix)

          #serviceInterface.callbackURL = 'http://' + $window.location.hostname + portSpec + '/purse'
          serviceInterface.callbackURL = apiBase + '/purse'

          def.promise

        serviceInterface.draggedPurse = (handle, _purseObject) ->
          ###
          Dragged purse.
          ###

          def = $q.defer()

          _value = _purseObject.value

          purseUpdateUrl = @callbackURL

          purseObject =
              $meta        : _purseObject
              data         : _purseObject
              handle       : handle
              createTipUrl : true

          console.dir purseObject

          $http.post(purseUpdateUrl, purseObject)
               .success((purseData) ->
                   def.resolve purseData
               )
               .error((errorData) ->
                   def.reject errorData
               )

          return def.promise


        ###*
        @namespace startBudgetCron
        @description

            Start budget cron tasks.

        ###
        serviceInterface.startBudgetCron = startBudgetCron

        # Return Service Interface.
        serviceInterface

    __interface__

      .factory "bank.service", [
        "firebase.utils"
        "$rootScope"
        "$http"
        "$q"
        "$timeout"
        "profile.service"
        "notification.service"
        "$window"
        "stamp.service"
        bankService
      ]
