###
# fileOverview

        _
  ___ _| |_ _____ ____  ____
 /___|_   _|____ |    \|  _ \
|___ | | |_/ ___ | | | | |_| |
(___/   \__)_____|_|_|_|  __/
                       |_|

## description

Stamp service loads unique stamps for the given dashboard and facilitates 
stamp platform controllers.

###

define [
  "interface"
  "lodash"
  "numeral"
  "config"
  "services/profile"
  "services/notification"
  "services/crypto"
  'utils/firebase'
], (__interface__, _, numeral, environment) ->

  stampService = (firebaseUtils, $http, $q, userRequire, profileService, notificationService, cryptoService, $timeout) ->

    serviceInterface = {}

    serviceInterface.localValidatorStamps = []
    serviceInterface.collisionStore = []

    serviceInterface.primedStamps = {}

    serviceInterface.getStamp = (stamp, username) ->
      ###
      Get Stamp by Username
      @namespace getStamp
      ###

      if serviceInterface.primedStamps[stamp.$id]
        return serviceInterface.primedStamps[stamp.$id]
      serviceInterface.primedStamps[stamp.$id] = firebaseUtils.syncObject('users/' + username + '/stamps/local/' + stamp.$id)


    serviceInterface.getAllStamps = () ->
      ###
      Get All Stamps

      @namespace getAllStamps
      @TODO Refactor by filtering Users' local stamp data.
      ###

      firebaseUtils.syncArray('users')


    serviceInterface.getLatestBitsCollected = (stampId, handle) ->
      ###
      @namespace getLatestBitsCollected
      @param {string} stampId
      @return {Promise} bitsReport
      ###

      _def = $q.defer()
      checked = false
      sep = '/'
      $timeout ->
        user = firebaseUtils.syncArray('users' + sep + handle)

        user.$loaded().then (usersData) ->
          _.forEach usersData, (v, k) ->
            if v and v.stats and v.stats.success
              _.forEach v.stats.success, (sv, sk) ->
                if sk == stampId
                  checked = true
                  _def.resolve sv
          if not checked
            _def.reject false
          return
      , 1000

      _def.promise

    serviceInterface.getMerchantBitsTipped = (localStamp) ->
      ###
      @namespace getMerchantBitsTipped
      @description

          Augmenting merchant bottomsheet data with the total number of bits 
          tipped per fan (customer) which is an aggregate of all the merchant 
          stampenings to that customer's profile.

      @return {Promise} bitsTipped
      ###

      def = $q.defer()
      id = localStamp.$id
      addressOfLocalStamp = localStamp.address
      loc = _.toArray(addressOfLocalStamp.location)
      hashedAddressOfLocalStamp = cryptoService.hash(JSON.stringify(loc.join(',')))
      stampOwner = localStamp.stampOwner
      _userRequire = userRequire()

      merchantStatus = profileService.getMerchantStatus(stampOwner)

      listOfBits = []

      _userRequire.then (userData) ->
        handle = userData.handle

        merchantStatus.$loaded().then (merchantData) ->
          _result = 0

          d = merchantData.stamps.local

          if merchantData.stamps and merchantData.stamps.local

            _.forEach d, (a, _id) ->
              return  if !a.address

              loc = _.toArray(a.address.location)
              hashedAddress = cryptoService.hash(JSON.stringify(loc.join(',')))
              if hashedAddress == hashedAddressOfLocalStamp

                # A history of a particular stamp's application to the logged in user.
                stampHistoryList = profileService.getStampHistory(handle, _id)

                cb = () ->
                  _def = $q.defer()

                  stampHistoryList.$loaded().then (history) ->

                    _.forEach history, (n) ->
                      if n and n.bitsCollected and _.isNumber(n.bitsCollected)
                        _result = _result + parseFloat(n.bitsCollected)

                    _def.resolve _result

                  _def.promise

                listOfBits.push cb()

            l = $q.all listOfBits

            l.then (data) ->
              _result = _.first data
              def.resolve numeral(_result).format('0.00')

      def.promise


    serviceInterface.getStampBitsTipped = (localStamp) ->
      ###
      @namespace getStampBitsTipped
      @description

        Copy of getMerchantBitsTipped.

      ###

      serviceInterface.getMerchantBitsTipped(localStamp)


    serviceInterface.getStampIdBySerial = (serial) ->
      ###
      @namespace getStampIdBySerial
      @description

        Get stamps by ID from a particular SnowShoeStamp serial number.

      @param {string} serial
      @return {Promise} stampSerial
      ###

      def = $q.defer()

      found = false
      users = firebaseUtils.syncArray('users')

      users.$loaded().then (usersData) ->

        _.forEach usersData, (v, k) ->

          if v.stamps and v.stamps.local and found == false
            _.forEach v.stamps.local, (lv, lk) ->
              if serial == lv.receipt.validation.stamp.serial
                def.resolve lk
                found = true

        #if  found == false
          #def.reject found
          #return

      def.promise


    ###
    @namespace verifiedAddressConstruct
    @param {object} stamp
    @return {Promise} Pick yr API, be it SmartyStreets or Google or whatever else.
    ###

    serviceInterface.verifiedAddressConstruct = (baseAddressApi, stamp) ->

      def = $q.defer()

      zipCodeComponent = stamp.address.zipCode

      # Validate give address.
      $http.
        get(baseAddressApi).
        success (verifiedAddress) ->

          _verifiedAddress = verifiedAddress.results[0].address_components
          _verifiedGeometry = verifiedAddress.results[0].geometry
          _verifiedLocation = _verifiedGeometry.location

          # Array of labels for given address
          types = _.map _.pluck(_verifiedAddress, 'types'), (types) ->
            _.first(types) # @example locality

          # Array of values for given address
          longNames = _.pluck _verifiedAddress, 'long_name'

          # @example {
          #   street_number               : "1600",
          #   route                       : "Amphitheatre Parkway",
          #   locality                    : "Mountain View",
          #   administrative_area_level_1 : "CA",
          #   country                     : "United States",
          #   postal_code                 : "94043"
          # }
          _verifiedAddress = _.zipObject( types, longNames )

          sn = if _verifiedAddress.street_number then _verifiedAddress.street_number else ''
          stamp.address.line1 = sn + ' ' + _verifiedAddress.route
          stamp.address.line2 = ''
          stamp.address.zipCode = _verifiedAddress.postal_code or ''
          stamp.address.cityLabel = _verifiedAddress.locality
          stamp.address.stateLabel = _verifiedAddress.administrative_area_level_1
          stamp.address.countryLabel = _verifiedAddress.country
          stamp.address.location = _verifiedLocation

          def.resolve stamp

      def.promise


    ###
    @namespace updateStampAddress
    @param {object} stamp
    @param {string}
    ###

    serviceInterface.updateStampAddress = (stamp, user, dragUpdate) ->

      key = environment.google.maps.key
      urlApi = environment.google.maps.geocode

      prefix_address = 'address='
      prefix_key = 'key='

      if ! dragUpdate

        # Prepare URL construct.
        # @TODO Generalize for multiple API support.
        _line1 = stamp.address.line1 or null
        _line2 = stamp.address.line2 or null
        _city = stamp.address.cityLabel or null
        _state = stamp.address.stateLabel or null
        _country = stamp.address.countryLabel or null

        _tmpAddress = [
          _line1
          _line2
          _city
          _state
          _country
        ].join '+'

        address = _tmpAddress

        combinedAddress = [
          prefix_address + address
          prefix_key + key
        ].join '&'

      else

        _location = stamp.address.location

        _tmpAddress = [
          _location.lat
          _location.lng
        ].join ','

        address = _tmpAddress

        combinedAddress = [
          prefix_address + address
          prefix_key + key
        ].join '&'

      baseAddressApi = [
        urlApi
        combinedAddress
      ].join '?'
      def = $q.defer()

      serviceInterface.verifiedAddressConstruct(baseAddressApi, stamp).then (updatedStamp) ->

        _stamp = updatedStamp

        def.resolve _stamp

        if _.isString(user)
          serviceInterface.updateStamp(user, _stamp)
        else
          # If for whatever reason we're dealing with an object representation 
          # of the current logged in user, than try to find its handle.
          user.then (authData) ->
            handle = authData.handle
            serviceInterface.updateStamp(handle, _stamp)

      def.promise


    ###
    @namespace requestForUser
    ###
    serviceInterface.requestForUser = (username, requestConstruct) ->
      # Pass our validated form submission to API endpoint.
      # @depends node-mailer
      prefix = if window.is_device then '/data/' else ''
      requestEndpoint = (prefix + '/stamp/' + username + '/request')
      $http.post(requestEndpoint, requestConstruct).
        success((data, status, headers, config) ->
          return
        ).
        error((data, status, headers, config) ->
          return
        )


    ###
    Search Stamps by Query
    @namespace searchStampsByQuery
    ###
    #serviceInterface.searchStampsByQuery = () ->

      #reqRef = firebaseUtils.ref

      #search = (index, type, searchTerm, callback) ->

        #reqRef = queue.child('request').push(
          #index: index
          #type: type
          #query: searchTerm
        #)

        ## read the replies from https://<INSTANCE>.firebaseio.com/search/response
        #queue.child('response/' + reqRef.key()).on('value', fn = (snap) ->

          #if snap.val() != null            # wait for data
            #snap.ref().off('value', fn)    # stop listening
            #snap.ref().remove()            # clear the queue
            #callback(snap.val())

        #)

        #return

      #search('users', 'stamps', '*foo*', (data) ->
        #if data.hits
          #data.hits.forEach((hit) ->
          #)
      #)

      #return firebaseUtils.syncArray('search')


    serviceInterface.stampList = {}

    ###
    Get Local Stamps by Username
    @namespace getStampList
    ###

    serviceInterface.getStampList = (username) ->
      if serviceInterface.stampList[username]
        return serviceInterface.stampList[username]
      serviceInterface.stampList[username] = firebaseUtils.syncArray('users/' + username + '/stamps/local')



    serviceInterface.localStampList = {}

    ###
    Get Local Stamps by Username
    @namespace getLocalStamps
    ###

    serviceInterface.getLocalStamps = (username) ->
      if serviceInterface.localStampList[username]
        return serviceInterface.localStampList[username]
      serviceInterface.localStampList[username] = firebaseUtils.syncArray('users/' + username + '/stamps/local')


    ###
    Add Local Stamp to User Profile
    @namespace addStamp
    ###
    serviceInterface.addStamp = (localStampConstruct) ->
      username = localStampConstruct.username
      passes = localStampConstruct.passes
      alias = localStampConstruct.alias
      receipt = localStampConstruct.receipt
      birth = localStampConstruct.birth
      if ! passes.length or ! passes
        return
      firebaseUtils.pushObject("users/" + username + "/stamps/local", localStampConstruct)


    ###
    Update Local Stamp
    @namespace updateStamp
    ###
    serviceInterface._tempLocals = {}
    serviceInterface.updateStamp = (handle, stamp) ->

      __id = null
      _def = $q.defer()
      updateObject = undefined

      if serviceInterface._tempLocals[handle]
        _tempLocals = serviceInterface._tempLocals[handle]
      else
        _tempLocals = serviceInterface._tempLocals[handle] = firebaseUtils.syncArray("users/" + handle + "/stamps/local")

      _tempLocals.$loaded().then (_locals) ->
        _.forEach _locals, (v, k) ->
          if v.alias == stamp.alias
            _def.resolve v.$id

      defaultActivationProgressStatus = 1

      if stamp and stamp.address and stamp.address.line2
        stamp.address.line2 = stamp.address.line2

      if stamp.localStamp and stamp.localStamp.address
        stamp.localStamp.address.line2 = stamp.localStamp.address.line2

      try
        updateObject = angular.extend {},
          active           : if stamp.active or stamp.localStamp then true else false
          merchantAlias    : if stamp.merchantAlias then stamp.merchantAlias else if stamp.localStamp and stamp.localStamp.merchantAlias then stamp.localStamp.merchantAlias else ''
          activity         : if stamp.activity then stamp.activity else if stamp.localStamp and stamp.localStamp.activity then stamp.localStamp.activity else ''
          defaultAddress   : if stamp.defaultAddress then stamp.defaultAddress else if stamp.localStamp and stamp.localStamp.defaultAddress then stamp.localStamp.defaultAddress else stamp.merchantAlias or stamp.alias or stamp.$id
          address          : if stamp.address then stamp.address else stamp.localStamp.address
          alias            : if stamp.alias then stamp.alias else stamp.localStamp.alias
          days             : if stamp.days then stamp.days else stamp.localStamp.days
          hours            : if stamp.hours then stamp.hours else stamp.localStamp.hours
          statusSaturation : if stamp.statusSaturation then stamp.statusSaturation else if stamp.localStamp and stamp.localStamp.activationProgressStatus then stamp.localStamp.activationProgressStatus else defaultActivationProgressStatus
      catch e
        console.log e

      _id = stamp.$id or stamp.id
      if ! _id
        _def.promise.then (id) ->
          _id = id
          return firebaseUtils.updateObject("users/" + handle + "/stamps/local/" + _id, updateObject)
      else
        return firebaseUtils.updateObject("users/" + handle + "/stamps/local/" + _id, updateObject)


    ###
    Assign Stamp
    @namespace assignStamp

    Primarily for admins.
    ###
    serviceInterface.assignStamp = (handle, localStampConstruct) ->

      # New owner of stamp
      _newOwner = handle.$id
      # Stamp to be pushed
      _localStampConstruct = localStampConstruct.localStamp
      _localStampConstruct.statusSaturation = 1
      _localStampId = localStampConstruct.id
      _oldOwner = localStampConstruct.user.$id

      ## Cleanup
      oldPath = "users/" + _oldOwner + "/stamps/local/#{_localStampId}"

      firebaseUtils.removeObject(oldPath)

      firebaseUtils.pushObject("users/" + _newOwner + "/stamps/local", _localStampConstruct)


    ###
    Link Stamp to user
    ###
    serviceInterface.lock = (stampAlias, username) ->

      # New owner of stamp
      _newOwner = username
      # Stamp to be pushed
      #_localStampId = stampAlias
      _localStampId = null

      _oldOwnerUsername = null
      s = null
      allStamps = serviceInterface.getAllStamps()
      allStamps.$loaded().then (users) ->
        _.forEach users, (u) ->
          hasRole = u.role and u.role.default
          if u.stamps and u.stamps.local and hasRole == 'admin'
            _.forEach u.stamps.local, (stamp, keyName) ->
              if stamp.alias == stampAlias
                _registeredDate = moment()
                stamp.registeredDate = _registeredDate.format "MM/DD/YYYY HH:mm:ss"
                stamp.statusSaturation = 2
                s = stamp
                _localStampId = keyName
                _oldOwnerUsername = u.$id

        return notificationService.displayError('Lock unsuccessful')  if not _localStampId

        _oldOwner = _oldOwnerUsername

        ## Cleanup
        oldPath = "users/#{_oldOwner}/stamps/local/#{_localStampId}"

        firebaseUtils.removeObject(oldPath)

        firebaseUtils.pushObject("users/#{_newOwner}/stamps/local", s)
        return
      return


    # Return our service interface.
    serviceInterface


  purseService = (firebaseUtils, $http, $q, userRequire, profileService, notificationService, cryptoService) ->

    serviceInterface =
      purse: []

    serviceInterface.drag = (item, handle) ->
      ###
      Reset the pending pool's purse on drag events, analogous event on frontend to the CT POST bypass.

      A tip URL should be created after drag.
      ###

      serviceInterface.reset(handle)

      serviceInterface.add(item, drag: true, handle)
      return


    serviceInterface.reset = (handle) ->
      ###
      Reset the pending pool.
      ###

      sep = '/'
      serviceInterface.purse.length = 0
      profileRoot = firebaseUtils.syncObject('users' + sep + handle + sep 'profile')
      profileRoot.set purse: 0.00
      return


    serviceInterface.add = (item, dragStatus, handle) ->
      ###
      Add to pending pool under both drag and stamp success.
      ###

      if dragStatus and dragStatus.drag == true

        #serviceInterface.pendingPool.load item

        #updatedPurse = $q.all serviceInterface.pendingPool.purse

        #updatedPurse.then (purseObject) ->
            #return

        return
      else
        serviceInterface.load item

      updatedPurse = $q.all serviceInterface.purse

      updatedPurse.then (purseObject) ->
        serviceInterface.purse.length = 0
        return

      return


    serviceInterface.load = (item) ->
      ###
      Load a request to the pending pool.
      ###

      serviceInterface.purse.push item
      return

    serviceInterface

  __interface__

    .factory("stamp.service", [
      "firebase.utils"
      "$http"
      "$q"
      "user.require"
      "profile.service"
      "notification.service"
      "crypto.service"
      "$timeout"
      stampService
    ])

    .factory("purse.service", [
      "firebase.utils"
      "$http"
      "$q"
      "user.require"
      "profile.service"
      "notification.service"
      "crypto.service"
      purseService
    ])
