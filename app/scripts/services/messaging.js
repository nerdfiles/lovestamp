
/*
 * fileOverview

                                       _
                                      (_)
     ____  _____  ___  ___ _____  ____ _ ____   ____
    |    \| ___ |/___)/___|____ |/ _  | |  _ \ / _  |
    | | | | ____|___ |___ / ___ ( (_| | | | | ( (_| |
    |_|_|_|_____|___/(___/\_____|\___ |_|_| |_|\___ |
                                (_____|       (_____|

@module services
 *# description

An in-app messaging service, passing messages to node-mailer.
 */

(function() {
  define(['interface', 'utils/firebase'], function(__interface__) {
    var messagingService;
    messagingService = function(firebaseUtils, $timeout, $q, $http) {
      var serviceInterface;
      serviceInterface = {};
      serviceInterface.sendMessage = function() {};
      serviceInterface.updateMessage = function() {};
      return serviceInterface;
    };
    return __interface__.factory('messaging.service', ["firebase.utils", "$timeout", "$q", "$http", messagingService]);
  });

}).call(this);

//# sourceMappingURL=messaging.js.map
