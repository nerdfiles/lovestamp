
/**
 * fileOverview

                        ___ _ _
                       / __|_) |
     ____   ____ ___ _| |__ _| | _____
    |  _ \ / ___) _ (_   __) | || ___ |
    | |_| | |  | |_| || |  | | || ____|
    |  __/|_|   \___/ |_|  |_|\_)_____)
    |_|

@module services  

 *# description

  These profiles are not necessarily part of authenticated users. Only 
  merchants really require profiles, whereas fans really only need 
  the ability to log in based on a "username" (Firebase's 
  ``$auth.provider !== null``) which can either be stored on the client 
  device or it can simply be remembered by the person.
 */

(function() {
  define(["interface", "lodash", "config", "utils/firebase"], function(__interface__, _, environment) {
    var profileService;
    profileService = function(firebaseUtils, $q, $timeout, $http, localStorageService, $window) {

      /*
      A client-side service for dealing with profile display information and 
      interactivity.
       */
      var serviceInterface;
      serviceInterface = {};
      serviceInterface.socialAspectsEnabled = [];
      serviceInterface.connectedProviders = [];
      serviceInterface.runningValidations = [];
      serviceInterface.allUsers = void 0;
      serviceInterface.topUser = void 0;
      serviceInterface.stampSuccessData = {};
      serviceInterface.stampHistoryList = {};
      serviceInterface.users = void 0;

      /*
      @description
      
        Social URL prefixes.
       */
      serviceInterface.urlPrefix = {
        '@': 'https://twitter.com/',
        'ᶠ': 'https://facebook.com/',
        '✚': 'https://plus.google.com/'
      };

      /*
      @description
      
        Social prefixes available for clients to login via OAuth.
       */
      serviceInterface.socialPrefixesAvailable = {
        twitter: '@',
        facebook: 'ᶠ',
        google: '✚'
      };
      serviceInterface.socialProviderActive = null;
      if (serviceInterface.socialPrefixesEnabled == null) {
        serviceInterface.socialPrefixesEnabled = {};
      }
      serviceInterface.socialPrefixActive = null;
      _.extend(serviceInterface.socialPrefixesEnabled, _.pick(serviceInterface.socialPrefixesAvailable, 'twitter'));
      _.extend(serviceInterface.socialPrefixesEnabled, _.pick(serviceInterface.socialPrefixesAvailable, 'google'));
      _.extend(serviceInterface.socialPrefixesEnabled, _.pick(serviceInterface.socialPrefixesAvailable, 'facebook'));
      serviceInterface.socialAspectsAvailable = function(username) {

        /*
        @description
        
          Social Aspects available/enabled configuration model.
         */
        console.log('Loading Social Aspects. . .');
        _.forEach(serviceInterface.socialPrefixesEnabled, function(socialAspectHandle, socialAspectProvider) {
          var loadedSocialAspect;
          loadedSocialAspect = localStorageService.get(socialAspectProvider);
          if (loadedSocialAspect) {
            serviceInterface.socialAspectsEnabled.push(loadedSocialAspect);
          }
        });
        firebaseUtils.updateObject("users/" + username + "/profile/extended", serviceInterface.socialAspectsEnabled);
      };

      /*
      @description
      
        We hold validations under several use-case scenarios which might align 
        with data transactions account:
      
            1. Requests
            2. Users
            3. Payments
       */
      serviceInterface.getMerchantPictureInView = function(merchantSheetData) {

        /*
        @description
        
          Recasts a hash object as an array of label and value pairs.
        
        @namespace getMerchantPictureInView
        @param {object} merchantSheetData
        @return {Promise:string}
         */
        var def, googleImageServiceBaseUrl, googleImageServiceParameterListConstruct, googleImageServiceParams, googleImageServiceParamsUrlConstruct, locationObject, streetViewUrlComponents, streetViewUrlConstruct;
        def = $q.defer();
        googleImageServiceBaseUrl = environment.google.maps.streetView;
        locationObject = merchantSheetData.address.location;
        googleImageServiceParams = {
          size: $window.innerWidth + "x500",
          location: "" + locationObject.lat + "," + locationObject.lng,
          fov: '130',
          heading: '300',
          pitch: '10',
          key: environment.google.maps.key
        };

        /*
        @example ['propertyLabel1=propertyValue1', 'propertyLabel2=propertyValue2', ...]
        @return {array} stringifiedProperty
         */
        googleImageServiceParamsUrlConstruct = _.map(googleImageServiceParams, function(propertyValue, propertyLabel) {
          var propertySeparator, stringifiedProperty;
          propertySeparator = '=';
          return stringifiedProperty = propertyLabel + propertySeparator + propertyValue;
        });
        googleImageServiceParameterListConstruct = _.reduce(googleImageServiceParamsUrlConstruct, function(m, i, k) {
          var parameterSeparator;
          parameterSeparator = '&';
          if (k > 0) {
            return m + parameterSeparator + i;
          }
        });
        streetViewUrlComponents = [googleImageServiceBaseUrl, googleImageServiceParameterListConstruct];

        /*
        @description
        
          Joins URL and parameter list for Google Street View API search.
        
        @return P1?P2
         */
        streetViewUrlConstruct = _.reduce(streetViewUrlComponents, function(m, i) {
          var querySeparator;
          querySeparator = '?';
          return m + querySeparator + i;
        });
        $timeout(function() {
          return def.resolve(streetViewUrlConstruct);
        }, 0);
        return def.promise;
      };
      serviceInterface.addValidation = function(v) {

        /*
        @namespace addValidation
        @description
        
          Add a validation to the profile of the user to keep interim track.
        
          Use-cases are for ID, Normal Stamp Impression, Stamp Recall.
         */
        this.runningValidations.push(v);
      };
      serviceInterface.clearValidations = function() {
        this.runningValidations = [];
      };
      serviceInterface.getLoggedInUsersCount = function() {

        /*
        Get Logged In Users Count
        @namespace getLoggedInUserCount
        @description
        
          Collect logged in users to determine progress against top user.
         */
        var def, _sum, _users;
        if (serviceInterface.users) {
          _users = serviceInterface.users;
        } else {
          _users = serviceInterface.users = firebaseUtils.syncArray('users');
        }
        def = $q.defer();
        _sum = null;
        _users.$loaded().then(function(__users) {
          _sum = _.map(__users, function(_user) {
            if (_user.isLoggedIn) {
              return _user.isLoggedIn;
            }
          });
          _sum = _.filter(_sum, function(s) {
            return s;
          });
          return def.resolve(_sum);
        });
        return def.promise;
      };
      serviceInterface.userData = void 0;
      serviceInterface.getUserStatus = function(username) {

        /*
        Get User Status
        @namespace getUpdateStatus
        @description
        
          Get user status data.
         */
        var userData;
        return userData = serviceInterface.userData = firebaseUtils.syncObject('users/' + username);
      };
      serviceInterface.getUserRequests = function(username) {

        /*
        Get Request Status
        @namespace getUserRequests
        @description
        
          Get request status data.
         */
        var requestData;
        if (serviceInterface.requestData) {
          requestData = serviceInterface.requestData;
        } else {
          requestData = serviceInterface.requestData = firebaseUtils.syncObject('requests/' + username);
        }
        return requestData;
      };
      serviceInterface.getMerchantStatus = function(username) {

        /*
        Get User Status
        @namespace getMerchantStatus
        @description
        
          Get merchant status data.
         */
        var merchantData;
        merchantData = firebaseUtils.syncObject('users/' + username);
        return merchantData;
      };
      serviceInterface.getStampHistory = function(handle, stampId) {

        /*
        Get Stampening History
        @namespace stampHistory
        @description
        
          Get a history of the customer's stampenings from a particular stamp.
         */
        if (serviceInterface.stampHistoryList[stampId]) {
          return serviceInterface.stampHistoryList[stampId];
        }
        return serviceInterface.stampHistoryList[stampId] = firebaseUtils.syncArray('users/' + handle + '/stamps/stats/success/' + stampId);
      };
      serviceInterface.getUserTransactions = function(username) {

        /*
        Get User Transactions
        @namespace getUserTransactions
        @description
        
          Get user transactions for displaying stats on front end and for use 
          in front end display calculations.
         */
        if (serviceInterface.stampSuccessData[username]) {
          return serviceInterface.stampSuccessData[username];
        }
        return serviceInterface.stampSuccessData[username] = firebaseUtils.syncArray('users/' + username + '/stamps/success');
      };
      serviceInterface.getTopUser = function() {

        /*
        Get Top User
        @namespace getTopUser
        @description
        
          The top user within LoveStamp's balance sheet.
         */
        var def, topUser;
        def = $q.defer();
        if (serviceInterface.topUser) {
          topUser = serviceInterface.topUser;
        } else {
          topUser = serviceInterface.topUser = firebaseUtils.syncObject('balances/topUser');
        }
        topUser.$loaded().then(function(topUserBitsCollected) {
          return def.resolve(parseFloat(topUserBitsCollected.$value));
        });
        return def.promise;
      };
      serviceInterface.loadedProfile = void 0;
      serviceInterface.getProfile = function(username) {

        /**
        @namespace getProfile
        @description
        
          Get social profile data.
         */
        var def, loadedProfile;
        def = $q.defer();
        if (serviceInterface.loadedProfile) {
          loadedProfile = serviceInterface.loadedProfile;
        } else {
          loadedProfile = serviceInterface.loadedProfile = firebaseUtils.syncObject('users/' + username + '/profile');
        }
        loadedProfile.$loaded().then(function(userProfileData) {
          var __userProfileData, _userProfileData;
          _userProfileData = _.map(userProfileData, function(property, key) {
            var obj;
            if (key.indexOf('$') !== -1) {

            } else {
              obj = {};
              obj[key] = property;
              return obj;
            }
          });
          __userProfileData = _.filter(_userProfileData, function(u) {
            return u;
          });
          return def.resolve(__userProfileData);
        });
        return def.promise;
      };
      serviceInterface.getProfileById = function(id) {

        /**
        @namespace getProfileById
        @description
        
          Get social profile data by Id.
         */
        var def, _users;
        _users = firebaseUtils.syncArray('users');
        def = $q.defer();
        _users.$loaded().then(function(__users) {
          var givenUser;
          givenUser = _.last(_.filter(_.map(__users, function(_user, handle) {
            if (_user.$id === id || _user._id === id) {
              _user.handle = handle;
              return _user;
            }
          })));
          return def.resolve(givenUser);
        });
        return def.promise;
      };
      serviceInterface.getUserById = function(id) {

        /**
        @namespace getProfileById
        @description
        
          Get social profile data by Id.
         */
        var def, _users;
        _users = firebaseUtils.syncArray('users');
        def = $q.defer();
        _users.$loaded().then(function(__users) {
          var givenUser;
          givenUser = _.last(_.filter(_.map(__users, function(_user) {
            if (_user._id === id) {
              return _user;
            }
          })));
          return def.resolve(givenUser);
        });
        return def.promise;
      };
      serviceInterface.updateProfile = function(username, formObject) {

        /*
        Update Profile
        @namespace updateProfile
        @description
        
          Update user profile.
         */
        var e, f;
        try {
          f = firebaseUtils.updateObject("users/" + username + "/profile", formObject);
        } catch (_error) {
          e = _error;
          console.log(e);
          f = firebaseUtils.transmuteObject("users/" + username + "/profile", formObject);
        }
        return f;
      };
      serviceInterface.updateUserData = function(username, formObject) {

        /*
        Update Profile
        @namespace updateProfile
        @description
        
          Update user profile.
         */
        var e, f;
        try {
          f = firebaseUtils.updateObject("users/" + username, formObject);
        } catch (_error) {
          e = _error;
          console.log(e);
          f = firebaseUtils.transmuteObject("users/" + username, formObject);
        }
        return f;
      };
      serviceInterface.updateProfileBranch = function(handle, _potentialAction, formObject) {

        /*
        @see https://schema.org/potentialAction/
        @description 
        
          A specification for leaf structures for profile elements.
        
        @examples
        
          /profile.user.bio.content.moreObjectCollections.anotherObjectCollection
          /profile.bio.address.shipping.content
          /profile.address.billing.content
         */
        var hashBlock, longNames, merchantSchemaObject, metaObjectCollection, potentialAction, types, verifiedAddress, _verifiedAddress, _verifiedGeometry, _verifiedLocation;
        metaObjectCollection = formObject;
        potentialAction = _potentialAction.length ? _potentialAction.split('.') : [];
        merchantSchemaObject = _.reduce(potentialAction, function(m, i, k) {
          var parameterSeparator;
          parameterSeparator = '/';
          if (k > 0) {
            return m + parameterSeparator + i;
          }
        });
        if (formObject.status === 'ZERO_RESULTS' || formObject === '') {
          return;
        }
        hashBlock = {
          "user/bio/content": "bio",
          "address/shipping/content": "addressShipping",
          "address/billing/content": "addressBilling",
          "overview": "overview"
        };
        if (hashBlock[merchantSchemaObject] === 'bio') {
          return firebaseUtils.writeObject("users/" + handle + "/profile/" + hashBlock[merchantSchemaObject], metaObjectCollection);
        } else if (hashBlock[merchantSchemaObject] === 'overview') {
          return firebaseUtils.writeObject("users/" + handle + "/profile/" + hashBlock[merchantSchemaObject], metaObjectCollection);
        } else {
          verifiedAddress = metaObjectCollection;
          _verifiedAddress = verifiedAddress.results[0].address_components;
          _verifiedGeometry = verifiedAddress.results[0].geometry;
          _verifiedLocation = _verifiedGeometry.location;
          types = _.map(_.pluck(_verifiedAddress, 'types'), function(types) {
            return _.first(types);
          });
          longNames = _.pluck(_verifiedAddress, 'long_name');
          _verifiedAddress = _.zipObject(types, longNames);
          return firebaseUtils.writeObject("users/" + handle + "/profile/" + hashBlock[merchantSchemaObject], _verifiedAddress);
        }
      };

      /*
      @namespace updateUser
      
      @description
      
        Update a user object under administrative forms.
      
      @return {object:Promise} updatedUserObject
       */
      serviceInterface.updateUser = function(username, updateObject) {
        var updatedUserObject;
        return updatedUserObject = firebaseUtils.updateObject("users/" + username, updateObject);
      };

      /*
      @namespace getNetworkUsers
      @description
      
        Get list of users connected to the primary user.
      
      @param {string} _provider
      @param {string} id
      @return {object:Promise} foundOriginUserNetworks.network
       */
      serviceInterface.getNetworksByUserId = function(_provider, id) {
        var def;
        def = $q.defer();
        this.getAllUsers().$loaded().then(function(_userList) {
          var foundOriginUser, foundOriginUserNetworks, networks, profileIds;
          foundOriginUser = false;
          foundOriginUserNetworks = null;
          _.forEach(_userList, function(_user) {
            var userNetworks;
            if (_user.profile && _user.profile.networks) {
              userNetworks = _.toArray(_user.profile.networks);
              _.forEach(userNetworks, function(networkConstruct) {
                var e;
                try {
                  if (networkConstruct.replace('[', '').replace(']', '') === id) {
                    foundOriginUser = true;
                    foundOriginUserNetworks = {
                      'networks': _user.profile.networks
                    };
                  }
                } catch (_error) {
                  e = _error;
                  return console.log('Could not find network user.');
                }
              });
            }
            if (foundOriginUser) {

            }
          });
          profileIds = {};
          networks = {};
          if (foundOriginUserNetworks) {
            _.extend(foundOriginUserNetworks.networks, JSON.parse("{ \"" + _provider + "\": \"[" + id + "]\" }"));
            _.extend(networks, foundOriginUserNetworks);
          } else {
            _.extend(profileIds, JSON.parse("{ \"" + _provider + "\": \"" + id + "\" }"));
            _.extend(networks, {
              'networks': profileIds
            });
          }
          return def.resolve(networks);
        });
        return def.promise;
      };
      serviceInterface.getAllUsers = function() {

        /*
        @namespace getAllUsers
        @endpoint users/
        @return {array} users
        @description
        
          A collection of users to be used in administrative forms.
         */
        if (serviceInterface.allUsers) {
          return serviceInterface.allUsers;
        }
        return serviceInterface.allUsers = firebaseUtils.syncArray('users');
      };
      return serviceInterface;
    };
    return __interface__.factory("profile.service", ["firebase.utils", "$q", "$timeout", "$http", "localStorageService", "$window", profileService]);
  });

}).call(this);

//# sourceMappingURL=profile.js.map
