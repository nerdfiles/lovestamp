###
# fileOverview

                                          _
     ____  _____ _   _ ____  _____ ____ _| |_  ___
    |  _ \(____ | | | |    \| ___ |  _ (_   _)/___)
    | |_| / ___ | |_| | | | | ____| | | || |_|___ |
    |  __/\_____|\__  |_|_|_|_____)_| |_| \__|___/
    |_|         (____/

@module services  

## description

###

define [
  "interface"
  "lodash"
  "config"
  "utils/firebase"
], (__interface__, _, environment) ->

  paymentsService = (firebaseUtils, $q, $timeout, $http) ->
    ###
    @description

    A client-side service for dealing with payments display information and 
    interactivity.
    ###

    serviceInterface = {}
    serviceInterface.users = undefined
    serviceInterface.loadedPayments = undefined
    serviceInterface.loadedRequests = undefined


    serviceInterface.getRequests = () ->
      ###
      @namespace getRequests
      @description

      Pull requests for stamps from merchants.
      ###

      def = $q.defer()

      if serviceInterface.loadedRequest
        loadedRequests = serviceInterface.loadedRequests
      else
        loadedRequests = serviceInterface.loadedRequests = firebaseUtils.syncObject('requests')

      loadedRequests.$loaded().then (d) ->
        def.resolve d

      def.promise


    serviceInterface.update = (strategy) ->
      ###
      Update Payments

      @namespace update
      @description

      Stampministrators can update Stripe receipts.
      ###

      receipt = undefined

      if strategy is 'stripe'

        receipt = (handle, type, $id, formObject) ->
          console.log ("payments/" + handle + "/" + type + "/" + $id + formObject)
          paymentConstruct = firebaseUtils.updateObject("payments/" + handle + "/" + type + "/" + $id, formObject)
          return

      return {
        receipt: receipt
      }


    serviceInterface.getPayments = (username, options) ->
      ###*
      @namespace getPayments
      @description

        Get Stripe payments data.

      ###

      def = $q.defer()

      config = _.extend {}, options

      if serviceInterface.loadedPayments
        loadedPayments = serviceInterface.loadedPayments
      else
        if username
          loadedPayments = serviceInterface.loadedPayments = firebaseUtils.syncObject('payments/' + username)
        else
          loadedPayments = serviceInterface.loadedPayments = firebaseUtils.syncObject('payments')

      if options and options.callee
        options.callee(loadedPayments).then (userPaymentsData) ->

          _userPaymentsData = _.map userPaymentsData, (property, key) ->
            return  if key.indexOf('$') isnt -1
            else
              obj = {}
              obj[key] = property
              return obj
          __userPaymentsData = _.filter _userPaymentsData, (u) ->
            u
          def.resolve __userPaymentsData

      else
        loadedPayments.$loaded().then (userPaymentsData) ->

          _userPaymentsData = _.map userPaymentsData, (property, key) ->
            return  if key.indexOf('$') isnt -1
            else
              obj = {}
              obj[key] = property
              return obj
          __userPaymentsData = _.filter _userPaymentsData, (u) ->
            u
          def.resolve __userPaymentsData

      def.promise

    serviceInterface.allUsers = undefined


    serviceInterface.getAllUsers = () ->
      ###
      @namespace getAllUsers
      @endpoint users/
      @return {array} users
      @description

        A collection of users to be used in administrative forms.

      ###

      if serviceInterface.allUsers
        return serviceInterface.allUsers
      serviceInterface.allUsers = firebaseUtils.syncArray('users')


    # Return our Service Interface.
    serviceInterface

  __interface__.
    factory "payments.service", [
        "firebase.utils"
        "$q"
        "$timeout"
        "$http"
        paymentsService
      ]
