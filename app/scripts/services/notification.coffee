###
# fileOverview

                      _    ___ _                   _
                  _  (_)  / __|_)              _  (_)
     ____   ___ _| |_ _ _| |__ _  ____ _____ _| |_ _  ___  ____
    |  _ \ / _ (_   _) (_   __) |/ ___|____ (_   _) |/ _ \|  _ \
    | | | | |_| || |_| | | |  | ( (___/ ___ | | |_| | |_| | | | |
    |_| |_|\___/  \__)_| |_|  |_|\____)_____|  \__)_|\___/|_| |_|

## description

An notification service for consolidating error messaging and feeding 
special-case error directives and notifications.

###

define [
  'interface'
  'utils/firebase'
], (__interface__) ->

  notificationService = ($http, $q, $mdToast, $window, loggly, firebaseUtils, $timeout, $interval) ->
    ###
    @namespace notificationService  
    @description  

    Our service interface for dealing with remote and local error caching for proper debugging of unique external clients.

    Dependencies:

    1. Loggly
    2. AngularJS
    3. Possibly Firebase.

    ###

    serviceInterface = {}
    serviceInterface.currentStatus = null
    serviceInterface.logglyQueue = []
    content = null

    serviceInterface.cancelTransfer = false


    serviceInterface.displayHomeScreenAdd = (response, options) ->
      config = if options then options else {}
      _config = _.extend {}, config
      content = response
      noteConfig =
        hideDelay     : _config.hideDelay
        preserveScope : true
        template      : content

      $mdToast.show(noteConfig)


    serviceInterface.displayLocationNote = (noteConstruct) ->
      $mdToast.show(noteConstruct)

    serviceInterface.displayError = serviceInterface.displayNote = (response, options) ->
      ###
      @namespace displayError
      ###

      def = $q.defer()
      config = if options then options else {}
      _config = _.extend {}, config
      content = response

      console.dir _config

      noteConfig =
          hideDelay     : _config.hideDelay
          scope         : _config.scope
          preserveScope : true
          template      : content

      if $window.DEBUG
          serviceInterface.remotePost content

      if !options

          $mdToast.show($mdToast.simple().content(content))

      else

          onCompleteCheck = false

          $mdToast.show(noteConfig)

          $interval ->
              if notificationService.cancelTransfer == true
                  def.reject onCompleteCheck
                  $mdToast.hide()
          , 50

          if _config.onComplete
              t = $timeout ->
                  if notificationService.cancelTransfer == true
                      $timeout.cancel t
                      def.reject onCompleteCheck
                      return

                  _config.onComplete().then((transferStatus) ->
                      return def.reject(onCompleteCheck)  if transferStatus == null
                      onCompleteCheck = true
                      serviceInterface.displayNote 'Thanks! Check your ChangeTip Wallet now!'
                      def.resolve onCompleteCheck
                  , (transferStatus) ->
                      return def.reject(onCompleteCheck)  if transferStatus == null
                      if transferStatus == false
                          serviceInterface.displayError 'Failed to make transfer!'
                      def.reject onCompleteCheck
                  )
              , 5000
          else
              def.reject onCompleteCheck

          if _config.onComplete
              return def.promise

      return


    serviceInterface.remotePost = (status, type) ->
      ###
      @param {string} status
      @param {string} type
      @return {Promise:object} Firebase object.
      ###

      logglyQueue = serviceInterface.logglyQueue or []

      # Local AppCache possibly?
      serviceInterface.currentStatus = status

      _type = type or 'error'

      # The "remote" error library for LoveStamp.
      logglyQueue.push
        type: _type
        description: status

      currentStatus = serviceInterface.currentStatus = logglyQueue.pop()

      # Simple publish to Loggly.
      loggly.log currentStatus

      # Simple publish to Firebase.
      firebaseUtils.pushObject "app/errors", currentStatus


    serviceInterface

  # Implement notification service.
  __interface__

    .factory "notification.service", [
      "$http"
      "$q"
      "$mdToast"
      "$window"
      'loggly'
      'firebase.utils'
      '$timeout'
      '$interval'
      notificationService
    ]
