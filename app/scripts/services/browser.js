
/*
 * fileOverview

     _
    | |
    | |__   ____ ___  _ _ _  ___ _____  ____
    |  _ \ / ___) _ \| | | |/___) ___ |/ ___)
    | |_) ) |  | |_| | | | |___ | ____| |
    |____/|_|   \___/ \___/(___/|_____)_|

 *# description

Defensively facilitates InAppBrowser, mostly.
 */

(function() {
  define(['interface'], function(__interface__) {
    var browserService;
    browserService = function($window) {

      /*
      Internal browser service.
       */
      var deviceCheck, serviceInterface;
      serviceInterface = {
        deviceReady: false,
        InAppBrowser: $window
      };
      deviceCheck = function() {

        /*
        Native services check and preparation.
         */
        serviceInterface.deviceReady = true;
        if (typeof cordova === 'undefined') {
          serviceInterface.deviceReady = cordova.InAppBrowser && $window.is_device ? (serviceInterface.InAppBrowser = cordova.InAppBrowser, serviceInterface.InAppBrowser.addEventListener('loadstop', serviceInterface.changeBackgroundColor), serviceInterface.InAppBrowser.addEventListener('exit', serviceInterface.iabClose)) : void 0;
        }
      };
      document.addEventListener("deviceready", deviceCheck, false);
      serviceInterface.changeBackgroundColor = function() {
        serviceInterface.InAppBrowser.insertCSS({
          code: 'body { background: #ffffff; }'
        }, function() {
          console.log('Set background of InAppBrowser.');
        });
      };
      serviceInterface.iabClose = function(event) {
        serviceInterface.InAppBrowser.removeEventListener('loadstop', serviceInterface.changeBackgroundColor);
        serviceInterface.InAppBrowser.removeEventListener('exit', serviceInterface.iabClose);
      };
      serviceInterface.addEvent = function(config) {

        /*
        Add event.
        
        @param {object} config
          :eventName {string} "loadstart", "loadstop", "loaderror", "exit"
          :callback {function}
         */
        var _config;
        _config = config || {};
        serviceInterface.cachedEventName = _config.eventName || null;
        serviceInterface.cachedCallback = _config.callback || null;
        return serviceInterface.InAppBrowser.addEventListener(serviceInterface.cachedEventName, serviceInterface.cachedCallback);
      };
      serviceInterface.removeEvent = function(config) {

        /*
        Remove event.
         */
        var _config;
        _config = config || {};
        serviceInterface.cachedEventName = _config.eventName || null;
        serviceInterface.cachedCallback = _config.callback || null;
        return serviceInterface.InAppBrowser.removeEventListener(serviceInterface.cachedEventName, serviceInterface.cachedCallback);
      };
      serviceInterface.removeLastEvent = function() {

        /*
        Remove cached event.
         */
        if (serviceInterface.cachedEventName && serviceInterface.cachedCallback) {
          return serviceInterface.InAppBrowser.removeEventListener(serviceInterface.cachedEventName, serviceInterface.cachedCallback);
        } else {
          return console.log('No InAppBrowser event in cache.');
        }
      };
      serviceInterface.close = function() {

        /*
        Close cached browser window.
         */
        if (serviceInterface.lastOpenedConfig) {
          return serviceInterface.lastOpenedConfig.close();
        } else {
          console.log('No InAppBrowser window available to close.');
        }
      };
      serviceInterface.show = function() {

        /*
        Show or reopen cached browser window
         */
        if (serviceInterface.lastOpenedConfig) {
          return serviceInterface.lastOpenedConfig.show();
        } else {
          console.log('No InAppBrowser window is open.');
        }
      };
      serviceInterface.execScript = function(config) {

        /*
        Execute script (within added events, etc.).
        
        @param {object} config
          :injectDetails {object} 
            {
              file: '/path/to/file'
              code: ''
            }
          :callback {function}
         */
        var _config;
        _config = config || {};
        if (serviceInterface.lastOpenedConfig && (typeof serviceInterface.lastOpenedConfig.executeScript !== 'undefined')) {
          return serviceInterface.lastOpenedConfig.executeScript(_config.injectDetails, _config.callback);
        }
      };
      serviceInterface.open = function(config) {

        /*
        Open a browser window with a desired URL.
        
        @param {object} config
          :url {string} "http://google.com"
          :target {string} "_self", "_blank", "_system"
          :options {array} ["location=yes"] @see https://github.com/apache/cordova-plugin-inappbrowser#cordovainappbrowseropen
         */
        var _config, _options;
        _config = config || {};
        _options = _config.options || null;
        _options.push('closebuttoncaption=◂');
        _config.options = _options ? _options.join('&') : null;
        return serviceInterface.lastOpenedConfig = serviceInterface.InAppBrowser.open(_config.url, _config.target, _config.options);
      };
      return serviceInterface;
    };
    return __interface__.service('browser.service', ['$window', browserService]);
  });

}).call(this);

//# sourceMappingURL=browser.js.map
