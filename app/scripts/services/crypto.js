
/**
 * fileOverview

                              _        
      ____  ____ _   _ ____ _| |_ ___  
     / ___)/ ___) | | |  _ (_   _) _ \ 
    ( (___| |   | |_| | |_| || || |_| |
     \____)_|    \__  |  __/  \__)___/ 
                (____/|_|              

 *# description

Provides client-side cryptographic functionality.

This uses the crypto-js libraries, which are piecemeal - so if you need to
add more ciphers or functions, you will also need to rearrange which files
from crypto-js are included.

@see: https://code.google.com/p/crypto-js/

 *# layout

    decryptAES 
    encryptAES 
    hash 
    hashWithSha1 
    md5
 */

(function() {
  define(["interface", "crypto.AES"], function(__interface__) {
    var cryptoService;
    cryptoService = function() {
      var serviceInterface;
      serviceInterface = {};
      serviceInterface.hash = function(str) {

        /**
        Hash a string using the default cipher.
        
        @param {string} str String to be hashed  
        @return {string} Hashed string.
         */
        return this.hashWithSha1(str);
      };
      serviceInterface.md5 = function(str) {
        if (typeof str !== "string") {
          return "";
        }
        return Crypto.MD5(str).toString();
      };
      serviceInterface.hashWithSha1 = function(str) {

        /*
        Hash a string using the SHA1 cipher.
        
        @param {string} str String to be hashed  
        @return {string} Hashed string.
         */
        if (typeof str !== "string") {
          return "";
        }
        return Crypto.SHA1(str).toString();
      };
      serviceInterface.encryptAES = function(msg, pass) {

        /*
        Encrypt a string using Advanced Encryption Standard. Combine with  
        Authentication Scheme: http://www.cryptopp.com/wiki/Authenticated_Encryption  
        
        @param {string} msg Message to be encrypted, likely a token.  
        @return {string} pass Passphrase.
         */
        var encrypted;
        return encrypted = Crypto.AES.encrypt(msg, pass);
      };
      serviceInterface.decryptAES = function(enc, pass) {

        /*
        Decrypt a string using Advanced Encryption Algorithm.  
        
        @param {string} enc Encrypted string.  
        @return {string} pass Passphrase.
         */
        var decrypted;
        return decrypted = Crypto.AES.decrypt(enc, pass);
      };
      return serviceInterface;
    };
    return __interface__.factory("crypto.service", [cryptoService]);
  });

}).call(this);

//# sourceMappingURL=crypto.js.map
