###
# fileOverview
hen you need to locally sort an array of objects, use the functionality
provided by this service.

Usage:

$scope.sorter = sorterService.createSorter($scope.array, options);
$scope.sorter.sort('property', 'asc');

TODO: Should adapt for infinite scroll pages.

NOTE: this only supports sorting on a single column/parameter at a time.
###

define [ 'interface' ], (__interface) ->

  sorterService = ->
    serviceInterface = {}

    ###*
    # Create a sorter interface that can sort the provided array in place
    # by a specific column.
    #
    # Form of options object is:
    # {
    #    // Which column is sorted at the outset?
    #    defaultColumn: 'property',
    #    // Either boolean to apply to all columns, or an object of booleans by
    #    // column. Omitted columns default to ascending.
    #    defaultAscending: true || {
    #        property_1: true,
    #        property_2: false,
    #        ...
    #    }
    #    // Is sorting going to be case sensitive?
    #    caseSensitive: false,
      *
      *   // allows you to pass in a function that'll return the property within the sort data object
      *   // that you want to sort on
    #    propertyFindFunction: function(sortDataObject, column) {
    #      ...
    #      return value;
    #    }
    # }
    #
    # @param {array} array
    #     Array to be sorted.
    # @param {string} options
    #     Options object.
    # @return {object}
    #     The sorter interface.
    ###

    serviceInterface.createSorter = (array, options) ->
      array = array or []
      options = options or {}
      if options.defaultAscending == undefined
        options.defaultAscending = true
      if options.caseSensitive == undefined
        options.caseSensitive = false
      sorter = 
        details:
          column: options.defaultColumn
          ascending: true
          defaultAscending: options.defaultAscending
          caseSensitive: options.caseSensitive
        array: array
        sort: (column, direction) ->
          self = this
          if column
            @details.column = column
          if direction
            @ascending = direction == 'asc'
          col = @details.column
          @array.sort (a, b) ->
            if !a or typeof a != 'object' or !b or typeof b != 'object'
              return 0
            aval = a[col]
            bval = b[col]
            if options.propertyFindFunction != undefined
              aval = options.propertyFindFunction(a, col)
              bval = options.propertyFindFunction(b, col)
            if aval == bval
              return 0
            if aval == undefined or bval == undefined
              if aval != undefined
                return 1
              else
                return -1
            # Check for first character: if it is a number
            if aval != null and !isNaN(aval[0])
              aval = parseFloat(aval)
            # Check for first character: if it is a number
            if bval != null and !isNaN(bval[0])
              bval = parseFloat(bval)
            if !self.details.caseSensitive
              if typeof aval == 'string'
                aval = aval.toLowerCase()
              if typeof bval == 'string'
                bval = bval.toLowerCase()
            if self.ascending
              if aval < bval then -1 else 1
            else
              if aval > bval then -1 else 1
          return
        resort: (column) ->
          # Switching columns on the sort, so figure out the
          # direction to sort in.
          if column and @details.column != column
            @details.column = column
            if typeof @details.defaultAscending == 'boolean'
              @details.ascending = @details.defaultAscending
            else if @details.defaultAscending and typeof @details.defaultAscending == 'object'
              @details.ascending = @details.defaultAscending[column] != false
          else
            @details.ascending = !@details.ascending
          @sort()
          return
        reverse: ->
          @resort()
          return
        getSortClass: (column) ->
          if @details.column != column
            return ''
          if @details.ascending then 'asc' else 'desc'
      # Run the initial sort.
      sorter.sort()
      sorter

    serviceInterface

  __interface.
    factory 'sorterService', [ sorterService ]
