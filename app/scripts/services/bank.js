
/*
 * fileOverview

     _                 _
    | |               | |
    | |__  _____ ____ | |  _
    |  _ \(____ |  _ \| |_/ )
    | |_) ) ___ | | | |  _ (
    |____/\_____|_| |_|_| \_)

@module services

 *# description

A series of abstractions for our controllers, be they many and slim.
 */

(function() {
  define(["interface", 'lodash', 'utils/firebase', "services/profile", "services/notification", "services/stamp"], function(__interface__, _) {
    var bankService;
    bankService = function(firebaseUtils, $rootScope, $http, $q, $timeout, profileService, notificationService, $window, stampService) {

      /*
      Bank service.
       */
      var captureGlobalUserList, getIntensityFriendAndPersonal, getIntensityMerchant, serviceInterface, startBudgetCron;
      serviceInterface = {};
      serviceInterface.currencyFormat = '0,0.00';
      serviceInterface.bitsFormat = '0.00';
      serviceInterface.merchantsLocallyApplied = void 0;
      serviceInterface.y = 0;
      serviceInterface.b = 0;
      serviceInterface.r = 0;
      startBudgetCron = function() {

        /*
        @inner
         */
        return $http.get("https://lovestamp.io/budget/roll-up");
      };
      captureGlobalUserList = function() {

        /*
        @inner
         */
        var def;
        def = $q.defer();
        stampService.getAllStamps().$loaded().then(function(userData) {
          var globalUserList;
          globalUserList = [];
          _.forEach(userData, function(user, keyUsername) {
            if (user.stamps && user.stamps.local && user.stamps.local.stampImpressionsRed) {
              return globalUserList.push(user);
            }
          });
          return def.resolve(globalUserList);
        });
        return def.promise;
      };
      getIntensityMerchant = function(_merchants) {

        /*
        All merchants within a given viewport. Find their respective 
        intensities based on aggregate impressions.
        
        @param {array} _merchants
         */
        var accum, merchants, t, totalMerchantsStampImpressionsRed;
        accum = [];
        merchants = _.filter(_merchants, function(user) {
          return user.stamps && user.stamps.local;
        });
        t = [];
        _.forEach(merchants, function(_merchant) {
          if (!_merchant.stamps) {
            return;
          }
          if (!_merchant.stamps.stats) {
            return;
          }
          if (!_merchant.stamps.stats.local) {
            return;
          }
          return t.push(parseInt(_merchant.stamps.stats.local.stampImpressionsRed, 10));
        });
        totalMerchantsStampImpressionsRed = _.reduce(t, function(sum, n) {
          return sum + n;
        });
        _.forEach(merchants, function(_merchant) {
          var def, handle, merchantsLocallyApplied;
          def = $q.defer();
          if (!_merchant) {
            return;
          }
          handle = _merchant.$id;
          merchantsLocallyApplied = firebaseUtils.syncObject('users/' + handle + '/stamps/stats/local/stampImpressionsRed');
          merchantsLocallyApplied.$loaded().then(function(totalStampImpressionsRed) {
            var val;
            val = (totalStampImpressionsRed.$value / totalMerchantsStampImpressionsRed) * 100;
            serviceInterface.setHighestRed(val);
            _.extend(_merchant, {
              merchantIntensityQuota: val
            });
            return def.resolve(_merchant);
          });
          return accum.push(def.promise);
        });
        return $q.all(accum);
      };
      getIntensityFriendAndPersonal = function(handle, _stamps, totalMerchantSystem) {

        /*
        @param {string} handle
        @param {array} _array
        @param {array} totalMerchantSystem
         */
        var accum;
        accum = [];
        console.dir(_stamps);
        _.forEach(_stamps, function(_localStamp) {
          var keyName, stampImpressionsRedOwner, userStatus, _def;
          _def = $q.defer();
          keyName = _localStamp.$id;
          stampImpressionsRedOwner = _localStamp.stampOwner;
          userStatus = profileService.getUserStatus(stampImpressionsRedOwner);
          userStatus.$loaded().then(function(user) {
            var bSum, merchantsLocallyApplied, stampImpressionsRed, ySum;
            _.extend(_localStamp, {
              personalIntensityQuota: 0,
              friendIntensityQuota: 0
            });
            _def.resolve(_localStamp);
            if (user.stamps && !user.stamps.stats) {
              _.extend(_localStamp, {
                personalIntensityQuota: 0,
                friendIntensityQuota: 0
              });
              return _def.resolve(_localStamp);
            } else {
              if (user.stamps && user.stamps.stats && !user.stamps.stats.local) {
                return;
              }
              stampImpressionsRed = parseInt(user.stamps.stats.local.stampImpressionsRed, 10);
              ySum = 0;
              bSum = 0;
              _.forEach(totalMerchantSystem, function(successfulStampenings) {
                if (successfulStampenings && successfulStampenings.stamps && successfulStampenings.stamps.stats) {
                  _.forEach(successfulStampenings.stamps.stats.success, function(stampObject) {
                    if (!stampObject) {
                      return;
                    }
                    bSum = bSum + parseInt(stampObject.stampImpressionsBlue, 10);
                  });
                }
              });
              _.forEach(totalMerchantSystem, function(successfulStampenings) {
                if (successfulStampenings && successfulStampenings.stamps && successfulStampenings.stamps.stats) {
                  _.forEach(successfulStampenings.stamps.stats.success, function(stampObject) {
                    if (!stampObject) {
                      return;
                    }
                    ySum = ySum + parseInt(stampObject.stampImpressionsYellow, 10);
                  });
                }
              });
              _.forEach(totalMerchantSystem, function(merchantSystem) {
                if (merchantSystem && merchantSystem.stamps && merchantSystem.stamps.stats && merchantSystem.stamps.stats.success) {
                  return _.forEach(merchantSystem.stamps.stats.success[_localStamp.$id], function(ms) {
                    return _.extend(_localStamp, {
                      personalIntensityQuota: ms.stampImpressionsYellow / stampImpressionsRed,
                      friendIntensityQuota: ms.stampImpressionsBlue / stampImpressionsRed
                    });
                  });
                }
              });
              if (_localStamp.stampOwner !== handle) {
                _def.resolve(_localStamp);
              }
              if (serviceInterface.merchantsLocallyApplied) {
                merchantsLocallyApplied = serviceInterface.merchantsLocallyApplied;
              } else {
                merchantsLocallyApplied = serviceInterface.merchantsLocallyApplied = firebaseUtils.syncArray('users/' + handle + '/stamps/stats/success');
              }
              return merchantsLocallyApplied.$loaded().then(function(stampsByColor) {
                var aggregateStampImpressionsBlue, aggregateStampImpressionsYellow;
                aggregateStampImpressionsYellow = 0;
                aggregateStampImpressionsBlue = 0;
                _.forEach(stampsByColor, function(n) {
                  aggregateStampImpressionsYellow += n.stampImpressionsYellow || 0;
                  return aggregateStampImpressionsBlue += n.stampImpressionsBlue || 0;
                });
                _.forEach(stampsByColor, function(_stampByColor) {
                  return (function(stampByColor) {
                    if (keyName === _stampByColor.$id) {
                      if (!user.stamps) {
                        return;
                      }
                      if (!user.stamps.stats) {
                        return;
                      }
                      if (_.isNaN(user.stamps.stats.local.stampImpressionsRed)) {
                        return;
                      }
                      return _.forEach(user.stamps.local, function(storedLocalStamp) {
                        var l1, l2, _friendIntensityQuota, _personalIntensityQuota;
                        if (storedLocalStamp.alias === _localStamp.alias) {
                          return;
                        }
                        if (!storedLocalStamp.address || !_localStamp.address) {
                          return;
                        }
                        l1 = _.toArray(storedLocalStamp.address.location);
                        l2 = _.toArray(_localStamp.address.location);
                        if (JSON.stringify(l1.join(',')) === JSON.stringify(l2.join(','))) {
                          _personalIntensityQuota = aggregateStampImpressionsYellow / stampImpressionsRed;
                          _friendIntensityQuota = aggregateStampImpressionsBlue / stampImpressionsRed;
                        } else {
                          _personalIntensityQuota = _stampByColor.stampImpressionsYellow / stampImpressionsRed;
                          _friendIntensityQuota = _stampByColor.stampImpressionsBlue / stampImpressionsRed;
                        }
                        if (_.isNaN(_friendIntensityQuota)) {
                          _friendIntensityQuota = 0;
                        }
                        if (_.isNaN(_personalIntensityQuota)) {
                          _personalIntensityQuota = 0;
                        }
                        serviceInterface.setHighestBlue(_friendIntensityQuota);
                        serviceInterface.setHighestYellow(_personalIntensityQuota);
                        return _.extend(_localStamp, {
                          personalIntensityQuota: _personalIntensityQuota,
                          friendIntensityQuota: _friendIntensityQuota
                        });
                      });
                    }
                  })(_stampByColor);
                });
                return _def.resolve(_localStamp);
              });
            }
          });
          return accum.push(_def.promise);
        });
        return $q.all(accum);
      };

      /*
      @description
      
        Functions for setting maxima and minima colors for our 
        spectrum analysis of merchants' stamps.
       */
      serviceInterface.setHighestYellow = function(y) {
        if (!y) {
          return serviceInterface.y;
        }
        if (y > serviceInterface.y && !_.isNaN(y)) {
          return serviceInterface.y = y;
        }
      };
      serviceInterface.setHighestBlue = function(b) {
        if (!b) {
          return serviceInterface.b;
        }
        if (b > serviceInterface.b && !_.isNaN(b)) {
          return serviceInterface.b = b;
        }
      };
      serviceInterface.setHighestRed = function(r) {
        if (!r) {
          return serviceInterface.r;
        }
        if (r > serviceInterface.r && !_.isNaN(r)) {
          return serviceInterface.r = r;
        }
      };
      serviceInterface.getHighestBlue = serviceInterface.setHighestBlue;
      serviceInterface.getHighestYellow = serviceInterface.setHighestYellow;
      serviceInterface.getHighestRed = serviceInterface.setHighestRed;
      serviceInterface.getIntensityMerchant = getIntensityMerchant;
      serviceInterface.getIntensityFriendAndPersonal = getIntensityFriendAndPersonal;

      /**
      @namespace getPayoutBudget
      @description
      
          Payout Budget shapes the upper and lower bounds for payouts.
      
      @return {Promise} payoutBudget
       */
      serviceInterface.getPayoutBudget = function() {
        return firebaseUtils.syncObject('balances/payoutBudget');
      };

      /**
      @namespace updatePayoutBudget
      @description
      
          Update payout budget.
      
      @param {number} newPayoutBudget
      @return {Promise} balances/payoutBudget
       */
      serviceInterface.updatePayoutBudget = function(newPayoutBudget) {
        return firebaseUtils.updateObject('balances', {
          payoutBudget: newPayoutBudget
        });
      };

      /**
      @namespace updateRevenue
      @description
      
          Update revenue.
      
      @param {number} newRevenueTotal
      @return {Promise} balances/revenue
       */
      serviceInterface.updateRevenue = function(newRevenueTotal) {
        return firebaseUtils.updateObject('balances', {
          revenue: parseInt(newRevenueTotal)
        });
      };

      /**
      @namespace updateSetBudget
      @description
      
          Redact LoveStamp's current budget.
      
      @param {number} newSetBudget
      @return {object} A complex object with updates to set budget.
       */
      serviceInterface.updateSetBudget = function(newSetBudget) {
        var b, _aggregatePendingPool, _aggregatePendingPoolVal;
        _aggregatePendingPoolVal = $q.defer();
        _aggregatePendingPool = 0;
        b = parseInt(newSetBudget, 10);
        stampService.getAllStamps().$loaded().then(function(usersData) {
          _.forEach(usersData, function(userData) {
            var _purse;
            if (userData.profile && userData.profile.purse) {
              console.log(userData);
              _purse = parseFloat(userData.profile.purse);
              console.log(_purse);
              return _aggregatePendingPool = _purse + parseFloat(_aggregatePendingPool);
            }
          });
          return _aggregatePendingPoolVal.resolve(_aggregatePendingPool);
        });
        _aggregatePendingPoolVal.promise.then(function(aggregatePurses) {
          var _b;
          console.log(aggregatePurses);
          _b = {
            payoutShadowBudget: b - (aggregatePurses ? aggregatePurses : 0),
            payoutBudget: b,
            setBudget: b,
            revenue: 0
          };
          return firebaseUtils.updateObject('balances', _b);
        });
      };

      /**
      @namespace getRevenue
      @description
      
          Revenue of LoveStamp; may its sparks always fly upwards.
       */
      serviceInterface.getRevenueTotal = function() {
        return firebaseUtils.syncObject('balances/revenue');
      };

      /**
      @namespace getSetBudget
      @description
      
          Manual budget setter.
       */
      serviceInterface.getSetBudget = function() {
        return firebaseUtils.syncObject('balances/setBudget');
      };
      serviceInterface.resetBudget = function(newBudget, newRevenue) {

        /**
        "6.121 The propositions of logic demonstrate the logical properties of propositions by combining them so as to form propositions that say nothing."
        — TLP, L.W.
        
        Reset the ecosystem.
         */
        newBudget = newBudget ? newBudget : 0;
        newRevenue = newRevenue ? newRevenue : 0;
        return $http.get("https://lovestamp.io/budget/reset/" + newBudget + "/" + newRevenue);
      };
      serviceInterface.callbackURL = null;
      serviceInterface.getPurse = function(handle) {

        /*
        Get Purse
        
        Retrieve data on a particular Social Media User's (Fan) purse.
         */
        var apiBase, apiPrefix, def, f, portSpec, purseGetUrl;
        purseGetUrl = 'users/' + handle + '/profile/purse';
        def = $q.defer();
        f = firebaseUtils.syncObject(purseGetUrl);
        f.$loaded().then(function(purseData) {
          return def.resolve({
            value: numeral(purseData.$value).format(serviceInterface.currencyFormat)
          });
        });
        portSpec = $window.location.port !== "" ? ':' + $window.location.port : '';
        apiPrefix = $window.is_device || portSpec === '' ? '/data' : '';
        apiBase = $window.is_device ? 'https://lovestamp.io/data' : $window.location.protocol + "//" + $window.location.hostname + portSpec + apiPrefix;
        serviceInterface.callbackURL = apiBase + '/purse';
        return def.promise;
      };
      serviceInterface.draggedPurse = function(handle, _purseObject) {

        /*
        Dragged purse.
         */
        var def, purseObject, purseUpdateUrl, _value;
        def = $q.defer();
        _value = _purseObject.value;
        purseUpdateUrl = this.callbackURL;
        purseObject = {
          $meta: _purseObject,
          data: _purseObject,
          handle: handle,
          createTipUrl: true
        };
        console.dir(purseObject);
        $http.post(purseUpdateUrl, purseObject).success(function(purseData) {
          return def.resolve(purseData);
        }).error(function(errorData) {
          return def.reject(errorData);
        });
        return def.promise;
      };

      /**
      @namespace startBudgetCron
      @description
      
          Start budget cron tasks.
       */
      serviceInterface.startBudgetCron = startBudgetCron;
      return serviceInterface;
    };
    return __interface__.factory("bank.service", ["firebase.utils", "$rootScope", "$http", "$q", "$timeout", "profile.service", "notification.service", "$window", "stamp.service", bankService]);
  });

}).call(this);

//# sourceMappingURL=bank.js.map
