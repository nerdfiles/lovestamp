
/*
 * fileOverview

                                          _
     ____  _____ _   _ ____  _____ ____ _| |_  ___
    |  _ \(____ | | | |    \| ___ |  _ (_   _)/___)
    | |_| / ___ | |_| | | | | ____| | | || |_|___ |
    |  __/\_____|\__  |_|_|_|_____)_| |_| \__|___/
    |_|         (____/

@module services  

 *# description
 */

(function() {
  define(["interface", "lodash", "config", "utils/firebase"], function(__interface__, _, environment) {
    var paymentsService;
    paymentsService = function(firebaseUtils, $q, $timeout, $http) {

      /*
      @description
      
      A client-side service for dealing with payments display information and 
      interactivity.
       */
      var serviceInterface;
      serviceInterface = {};
      serviceInterface.users = void 0;
      serviceInterface.loadedPayments = void 0;
      serviceInterface.loadedRequests = void 0;
      serviceInterface.getRequests = function() {

        /*
        @namespace getRequests
        @description
        
        Pull requests for stamps from merchants.
         */
        var def, loadedRequests;
        def = $q.defer();
        if (serviceInterface.loadedRequest) {
          loadedRequests = serviceInterface.loadedRequests;
        } else {
          loadedRequests = serviceInterface.loadedRequests = firebaseUtils.syncObject('requests');
        }
        loadedRequests.$loaded().then(function(d) {
          return def.resolve(d);
        });
        return def.promise;
      };
      serviceInterface.update = function(strategy) {

        /*
        Update Payments
        
        @namespace update
        @description
        
        Stampministrators can update Stripe receipts.
         */
        var receipt;
        receipt = void 0;
        if (strategy === 'stripe') {
          receipt = function(handle, type, $id, formObject) {
            var paymentConstruct;
            console.log("payments/" + handle + "/" + type + "/" + $id + formObject);
            paymentConstruct = firebaseUtils.updateObject("payments/" + handle + "/" + type + "/" + $id, formObject);
          };
        }
        return {
          receipt: receipt
        };
      };
      serviceInterface.getPayments = function(username, options) {

        /**
        @namespace getPayments
        @description
        
          Get Stripe payments data.
         */
        var config, def, loadedPayments;
        def = $q.defer();
        config = _.extend({}, options);
        if (serviceInterface.loadedPayments) {
          loadedPayments = serviceInterface.loadedPayments;
        } else {
          if (username) {
            loadedPayments = serviceInterface.loadedPayments = firebaseUtils.syncObject('payments/' + username);
          } else {
            loadedPayments = serviceInterface.loadedPayments = firebaseUtils.syncObject('payments');
          }
        }
        if (options && options.callee) {
          options.callee(loadedPayments).then(function(userPaymentsData) {
            var __userPaymentsData, _userPaymentsData;
            _userPaymentsData = _.map(userPaymentsData, function(property, key) {
              var obj;
              if (key.indexOf('$') !== -1) {

              } else {
                obj = {};
                obj[key] = property;
                return obj;
              }
            });
            __userPaymentsData = _.filter(_userPaymentsData, function(u) {
              return u;
            });
            return def.resolve(__userPaymentsData);
          });
        } else {
          loadedPayments.$loaded().then(function(userPaymentsData) {
            var __userPaymentsData, _userPaymentsData;
            _userPaymentsData = _.map(userPaymentsData, function(property, key) {
              var obj;
              if (key.indexOf('$') !== -1) {

              } else {
                obj = {};
                obj[key] = property;
                return obj;
              }
            });
            __userPaymentsData = _.filter(_userPaymentsData, function(u) {
              return u;
            });
            return def.resolve(__userPaymentsData);
          });
        }
        return def.promise;
      };
      serviceInterface.allUsers = void 0;
      serviceInterface.getAllUsers = function() {

        /*
        @namespace getAllUsers
        @endpoint users/
        @return {array} users
        @description
        
          A collection of users to be used in administrative forms.
         */
        if (serviceInterface.allUsers) {
          return serviceInterface.allUsers;
        }
        return serviceInterface.allUsers = firebaseUtils.syncArray('users');
      };
      return serviceInterface;
    };
    return __interface__.factory("payments.service", ["firebase.utils", "$q", "$timeout", "$http", paymentsService]);
  });

}).call(this);

//# sourceMappingURL=payments.js.map
