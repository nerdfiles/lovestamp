
/*
 * fileOverview
hen you need to locally sort an array of objects, use the functionality
provided by this service.

Usage:

$scope.sorter = sorterService.createSorter($scope.array, options);
$scope.sorter.sort('property', 'asc');

TODO: Should adapt for infinite scroll pages.

NOTE: this only supports sorting on a single column/parameter at a time.
 */

(function() {
  define(['interface'], function(__interface) {
    var sorterService;
    sorterService = function() {
      var serviceInterface;
      serviceInterface = {};

      /**
       * Create a sorter interface that can sort the provided array in place
       * by a specific column.
       *
       * Form of options object is:
       * {
       *    // Which column is sorted at the outset?
       *    defaultColumn: 'property',
       *    // Either boolean to apply to all columns, or an object of booleans by
       *    // column. Omitted columns default to ascending.
       *    defaultAscending: true || {
       *        property_1: true,
       *        property_2: false,
       *        ...
       *    }
       *    // Is sorting going to be case sensitive?
       *    caseSensitive: false,
        *
        *   // allows you to pass in a function that'll return the property within the sort data object
        *   // that you want to sort on
       *    propertyFindFunction: function(sortDataObject, column) {
       *      ...
       *      return value;
       *    }
       * }
       *
       * @param {array} array
       *     Array to be sorted.
       * @param {string} options
       *     Options object.
       * @return {object}
       *     The sorter interface.
       */
      serviceInterface.createSorter = function(array, options) {
        var sorter;
        array = array || [];
        options = options || {};
        if (options.defaultAscending === void 0) {
          options.defaultAscending = true;
        }
        if (options.caseSensitive === void 0) {
          options.caseSensitive = false;
        }
        sorter = {
          details: {
            column: options.defaultColumn,
            ascending: true,
            defaultAscending: options.defaultAscending,
            caseSensitive: options.caseSensitive
          },
          array: array,
          sort: function(column, direction) {
            var col, self;
            self = this;
            if (column) {
              this.details.column = column;
            }
            if (direction) {
              this.ascending = direction === 'asc';
            }
            col = this.details.column;
            this.array.sort(function(a, b) {
              var aval, bval;
              if (!a || typeof a !== 'object' || !b || typeof b !== 'object') {
                return 0;
              }
              aval = a[col];
              bval = b[col];
              if (options.propertyFindFunction !== void 0) {
                aval = options.propertyFindFunction(a, col);
                bval = options.propertyFindFunction(b, col);
              }
              if (aval === bval) {
                return 0;
              }
              if (aval === void 0 || bval === void 0) {
                if (aval !== void 0) {
                  return 1;
                } else {
                  return -1;
                }
              }
              if (aval !== null && !isNaN(aval[0])) {
                aval = parseFloat(aval);
              }
              if (bval !== null && !isNaN(bval[0])) {
                bval = parseFloat(bval);
              }
              if (!self.details.caseSensitive) {
                if (typeof aval === 'string') {
                  aval = aval.toLowerCase();
                }
                if (typeof bval === 'string') {
                  bval = bval.toLowerCase();
                }
              }
              if (self.ascending) {
                if (aval < bval) {
                  return -1;
                } else {
                  return 1;
                }
              } else {
                if (aval > bval) {
                  return -1;
                } else {
                  return 1;
                }
              }
            });
          },
          resort: function(column) {
            if (column && this.details.column !== column) {
              this.details.column = column;
              if (typeof this.details.defaultAscending === 'boolean') {
                this.details.ascending = this.details.defaultAscending;
              } else if (this.details.defaultAscending && typeof this.details.defaultAscending === 'object') {
                this.details.ascending = this.details.defaultAscending[column] !== false;
              }
            } else {
              this.details.ascending = !this.details.ascending;
            }
            this.sort();
          },
          reverse: function() {
            this.resort();
          },
          getSortClass: function(column) {
            if (this.details.column !== column) {
              return '';
            }
            if (this.details.ascending) {
              return 'asc';
            } else {
              return 'desc';
            }
          }
        };
        sorter.sort();
        return sorter;
      };
      return serviceInterface;
    };
    return __interface.factory('sorterService', [sorterService]);
  });

}).call(this);

//# sourceMappingURL=sorter.js.map
