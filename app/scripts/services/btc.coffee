###
# fileOverview

     _               
    | |     _        
    | |__ _| |_ ____ 
    |  _ (_   _) ___)
    | |_) )| |( (___ 
    |____/  \__)____)

## layout

      amountMaxima 
      amountMinima 
    ▶ btceHttpPost 
      customerInteractionFrequency 
      friendBehaviorFrequency 
      generatedAmountFactorConstant 
      generatedMarketRangeFromMinMax 
      merchantStampingFrequency 
    ▶ randomizedAmount 
      spotPrice 
      totalNotifications 
      totalSatoshis 
      totalStamps 
      totalStampsEarned 
      totalStampsRedeemed 
      serviceInterface 

###

define [
  'interface'
  'bitcoinjs'
], (__interface__) ->

  __interface__

  .service 'btc.service', [
    "$q"
    bitcoin
  ]

  bitcoin = ($q) ->

    serviceInterface = {}

    serviceInterface.btceHttpPost = (keyPair, method, params, nonce) ->
      ###
      BTC-E trade and transaction histroy/actions.
      ###

      return "{'error':'missing key pair'}"  if keyPair is `undefined`
      params = ""  if params is `undefined`

      # Cleanup keypair, remove all \s (any whitespace)
      keyPair = keyPair.replace(/[\s]/g, "")

      # Keypair example: "AFE730YV-S9A4FXBJ-NQ12HXS9-CA3S3MPM-CKQLU0PG,96a00f086824ddfddd9085a5c32b8a7b225657ae2fe9c4483b4c109fab6bf1a7"
      keyPair = keyPair.split(",")
      pubKey = keyPair[0]
      privKey = keyPair[1]

      # As specified on the BTC-e api (https://btc-e.com/api/documentation) the
      # nonce POST parameter must be an incrementing integer (>0). The easiest
      # implementation is the use of a timestamp (TS), so there is no need
      # for persistant storage. Preferable, the resolution of the TS should be
      # small enough the handle the desired call-frequency (a sleep of the TS
      # resolution can fix this but I don't like such a waste). Another
      # consideration is the sizeof the nonce supported by BTC-e. Experiments
      # revealed this is a 32 bit unsigned number. The native JavaScript TS,
      # stored in a float, can be 53 bits and has a resolution of 1 ms.

      # This time stamp counts amount of 200ms ticks starting from Jan 1st, 2014 UTC
      # On 22 Mar 2041 01:17:39 UTC, it will overflow the 32 bits and will fail
      # the nonce key for BTC-e
      nonce = Math.floor((Date.now() - Date.UTC(2014, 0)) / 200)  if nonce is `undefined`

      # Construct payload message
      msg = "nonce=" + nonce + "&method=" + method + params
      msgSign = Utilities.computeHmacSignature(Utilities.MacAlgorithm.HMAC_SHA_512, msg, privKey)

      # Convert encoded message from byte[] to hex string
      msgSignHex = []
      i = 0

      while i < msgSign.length
        # Doing it nibble by nibble makes sure we keep leading zero's
        msgSignHex.push ((msgSign[i] >>> 4) & 0xF).toString(16)
        msgSignHex.push (msgSign[i] & 0xF).toString(16)
        i++

      msgSignHex = msgSignHex.join("")
      httpHeaders =
        Key  : pubKey
        Sign : msgSignHex

      fetchOptions =
        method  : "post"
        headers : httpHeaders
        payload : msg

      reponse = UrlFetchApp.fetch("https://btc-e.com/tapi", fetchOptions)
      reponse.getContentText()

    serviceInterface.randomizedAmount = (firebaseUtils, $q, $timeout, $stateParams) ->
      ###
      Find bits collected per a single stamp given a handle. Update that stamp with BTC-E 
      feed data to collocate trends based on PRNG. Use arby to inform which coins to trade 
      on; update each stamp with mock trade data at stampenings per querified/filtered 
      social handles.
      ###

      return (impressionsAllowance = 300, provider = 'twitter', handle) ->
        def = $q.defer()
        sep = '/'

        ref = firebaseUtils.ref(handle + sep + 'friends' + sep + provider) # A list of friends, as { handle, provider }
        ref.$loaded().then (friendHandle) ->
          users = $scope.users = firebaseUtils.ref('users' + sep + friendHandle)
          users.$loaded().then (friend) ->
            # Look for friend.stamps.stats.success.{{stampId}}.bitsCollected
            console.dir friend.stamps.stats.success


        #ref.child('history/').child($stateParams.folderID).once('value',
          #(ss) ->
            #ss.forEach((childSnapshot) ->
              #console.log childSnapshot
            #)
          #)
        #ref.child('users/').child(childSnapshot.handle()).once('value',
          #(user) ->
            #$timeout(() ->
              #$scope.users.push(user.val())
              #console.log($scope.users)
            #)
          #)

        #ref.child('users/').child(childSnapshotHandle()).set
        #, (err) ->
          #$timeout () ->
            #if err
              #def.reject err
            #else
              #def.resolve ref
            #return
          #return
        #ref.promise

    serviceInterface.generatedAmountFactorConstant = () -> true
    serviceInterface.generatedMarketRangeFromMinMax = () -> true
    serviceInterface.spotPrice = () -> true
    serviceInterface.amountMaxima = () -> true
    serviceInterface.amountMinima = () -> true
    serviceInterface.friendBehaviorFrequency = () -> true
    serviceInterface.customerInteractionFrequency = () -> true
    serviceInterface.merchantStampingFrequency = () -> true
    serviceInterface.totalStamps = () -> true
    serviceInterface.totalSatoshis = () -> true
    serviceInterface.totalNotifications = () -> true
    serviceInterface.totalStampsEarned = () -> true
    serviceInterface.totalStampsRedeemed = () -> true
    serviceInterface

  return
