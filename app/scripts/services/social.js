
/*
 * fileOverview

                      _       _
                     (_)     | |
      ___  ___   ____ _ _____| |
     /___)/ _ \ / ___) (____ | |
    |___ | |_| ( (___| / ___ | |
    (___/ \___/ \____)_\_____|\_)

@module services

 *# description

Potential actions for enabling users to create "friend" relations and properties.
 */

(function() {
  define(['interface', 'utils/firebase'], function(__interface__) {
    var socialService;
    socialService = function($firebaseUtils, $timeout, $q) {
      var serviceInterface;
      serviceInterface = {};
      serviceInterface.addFriend = function() {};
      serviceInterface.removeFriend = function() {};
      serviceInterface.favoriteMerchant = function() {};
      serviceInterface.unfavoriteMerchant = function() {};
      serviceInterface.postToProfile = function() {};
      return serviceInterface;
    };
    return __interface__.factory('social.service', ["firebase.utils", "$timeout", "$q", socialService]);
  });

}).call(this);

//# sourceMappingURL=social.js.map
