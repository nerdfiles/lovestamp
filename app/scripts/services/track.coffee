###*
@fileOverview
Stamp service loads unique stamps for the given dashboard and facilitates stamp platform controllers.
###

#
#
#                              ______                   _
#        _                    / _____)                 (_)
#  ___ _| |_ _____ ____  ____( (____  _____  ____ _   _ _  ____ _____
# /___|_   _|____ |    \|  _ \\____ \| ___ |/ ___) | | | |/ ___) ___ |
#|___ | | |_/ ___ | | | | |_| |____) ) ____| |    \ V /| ( (___| ____|
#(___/   \__)_____|_|_|_|  __(______/|_____)_|     \_/ |_|\____)_____)
#                       |_|
#
# 
define [
  "interface"
  'utils/firebase'
], (__interface__) ->

  userTrack = (firebaseUtils, $q, $timeout) ->
    (handle) ->
      ref = firebaseUtils.ref("users", handle)
      #def = $q.defer()
      history = ref.child("history")
      #console.dir history
      return

  __interface__.
    factory "user.track", [
      "firebase.utils"
      "$q"
      "$timeout"
      userTrack
    ]
