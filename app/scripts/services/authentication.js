
/*
 * fileOverview

                       _                      _                   _
                   _  | |                 _  (_)              _  (_)
     _____ _   _ _| |_| |__  _____ ____ _| |_ _  ____ _____ _| |_ _  ___  ____
    (____ | | | (_   _)  _ \| ___ |  _ (_   _) |/ ___|____ (_   _) |/ _ \|  _ \
    / ___ | |_| | | |_| | | | ____| | | || |_| ( (___/ ___ | | |_| | |_| | | | |
    \_____|____/   \__)_| |_|_____)_| |_| \__)_|\____)_____|  \__)_|\___/|_| |_|

 *# description

  The authenticationService definition (with Entitlements).
 */

(function() {
  define(["interface", "lodash", "utils/firebase", "services/profile", "services/notification"], function(__interface__, _) {
    var loginSimple, profileCreate, user, userRequire;
    user = function(loginSimple, $window, notificationService) {

      /*
      User object specification of basic properties.
       */
      var currentUser, e, handle, _displayName, _provider, _temporaryUser, _username, _username_prefix;
      currentUser = loginSimple.getUser();
      if (currentUser && currentUser.$$state && currentUser.$$state.value) {
        _temporaryUser = currentUser.$$state.value;
        _provider = _temporaryUser.provider;
        _username = _temporaryUser[_provider].username || _temporaryUser[_provider].id;
        _username_prefix = _provider === 'twitter' ? '@' : _provider === 'google' ? '✚' : 'ᶠ';
        handle = _username_prefix + _username;
        _displayName = _temporaryUser[_provider].displayName ? _temporaryUser[_provider].displayName : null;
        if (auth.anonymouslyLoggingIn === true) {
          angular.extend(currentUser.$$state.value, {
            handle: '@LoveStampio',
            anonymouslyLoggedIn: true
          });
        } else {
          angular.extend(currentUser.$$state.value, {
            handle: handle
          });
        }
        try {
          angular.extend(currentUser.$$state.value, {
            displayName: _displayName
          });
        } catch (_error) {
          e = _error;
          notificationService.displayNote('Failed to capture display name.');
        }
      }
      return currentUser;
    };
    userRequire = function(loginSimple, $q, $rootScope, profileService, notificationService) {

      /*
      
      @function
      @inner
      @description
      
        A simple wrapper on loginSimple.getUser() that rejects the promise
        if the user does not exists (i.e. makes user required)
       */
      return function() {
        return loginSimple.getUser().then(function(user) {
          var bg, currentProfile, facebookBg, googleBg, handle, twitterBg, _displayName, _provider, _temporaryUser, _username, _username_prefix;
          if (user) {
            _temporaryUser = user;
            _provider = _temporaryUser.provider;
            _username = _temporaryUser[_provider].username || _temporaryUser[_provider].id;
            currentProfile = profileService.currentProfile = user[user.provider].cachedUserProfile;
            if (currentProfile) {
              _displayName = currentProfile.displayName || currentProfile.screen_name || currentProfile.name;

              /*
              provider: twitter
                {
                    "id": 1048197164,
                    "id_str": "1048197164",
                    "name": "RattelyrDragon",
                    "screen_name": "RattelyrDragon",
                    "location": "",
                    "profile_location": null,
                    "description": "Rattelyr dragons are territorial creatures. They like to burrow through loose earth and surprise opponents by bursting out of the soil.",
                    "url": null,
                    "entities": {
                        "description": {
                            "urls": []
                        }
                    },
                    "protected": false,
                    "followers_count": 54,
                    "friends_count": 159,
                    "listed_count": 4,
                    "created_at": "Sun Dec 30 15:49:05 +0000 2012",
                    "favourites_count": 73,
                    "utc_offset": null,
                    "time_zone": null,
                    "geo_enabled": false,
                    "verified": false,
                    "statuses_count": 67,
                    "lang": "en",
                    "status": {
                        "created_at": "Tue Jun 30 18:32:31 +0000 2015",
                        "id": 615951061237436400,
                        "id_str": "615951061237436417",
                        "text": "Ulterior States [IamSatoshi Documentary] https://t.co/hAnpwnjCff via @YouTube",
                        "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
                        "truncated": false,
                        "in_reply_to_status_id": null,
                        "in_reply_to_status_id_str": null,
                        "in_reply_to_user_id": null,
                        "in_reply_to_user_id_str": null,
                        "in_reply_to_screen_name": null,
                        "geo": null,
                        "coordinates": null,
                        "place": null,
                        "contributors": null,
                        "retweet_count": 0,
                        "favorite_count": 0,
                        "entities": {
                            "hashtags": [],
                            "symbols": [],
                            "user_mentions": [
                                {
                                    "screen_name": "YouTube",
                                    "name": "YouTube",
                                    "id": 10228272,
                                    "id_str": "10228272",
                                    "indices": [
                                        69,
                                        77
                                    ]
                                }
                            ],
                            "urls": [
                                {
                                    "url": "https://t.co/hAnpwnjCff",
                                    "expanded_url": "https://youtu.be/yQGQXy0RIIo",
                                    "display_url": "youtu.be/yQGQXy0RIIo",
                                    "indices": [
                                        41,
                                        64
                                    ]
                                }
                            ]
                        },
                        "favorited": false,
                        "retweeted": false,
                        "possibly_sensitive": false,
                        "lang": "ro"
                    },
                    "contributors_enabled": false,
                    "is_translator": false,
                    "is_translation_enabled": false,
                    "profile_background_color": "131516",
                    "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/751293601/516347dd4135c82927886800eefd48df.jpeg",
                    "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/751293601/516347dd4135c82927886800eefd48df.jpeg",
                    "profile_background_tile": true,
                    "profile_image_url": "http://pbs.twimg.com/profile_images/3042487582/d6436b5945913a0bdd12f10414a7efb0_normal.jpeg",
                    "profile_image_url_https": "https://pbs.twimg.com/profile_images/3042487582/d6436b5945913a0bdd12f10414a7efb0_normal.jpeg",
                    "profile_banner_url": "https://pbs.twimg.com/profile_banners/1048197164/1424560794",
                    "profile_link_color": "222222",
                    "profile_sidebar_border_color": "FFFFFF",
                    "profile_sidebar_fill_color": "DDEEF6",
                    "profile_text_color": "333333",
                    "profile_use_background_image": true,
                    "has_extended_profile": false,
                    "default_profile": false,
                    "default_profile_image": false,
                    "following": false,
                    "follow_request_sent": false,
                    "notifications": false,
                    "suspended": false,
                    "needs_phone_verification": false
                }
              
              @provider google
              
              @provider facebook
               */
              twitterBg = currentProfile.profile_image_url;
              facebookBg = currentProfile.picture && currentProfile.picture.data;
              googleBg = currentProfile.picture && !currentProfile.picture.data;
              if (twitterBg) {
                bg = currentProfile.profile_image_url.replace('_normal', '');
              }
              if (facebookBg) {
                bg = currentProfile.picture.data.url;
              }
              if (googleBg) {

                /*
                    google_spec =
                      family_name : "Hah"
                      gender      : "other"
                      given_name  : "Ɐha"
                      id          : "100541351182813566938"
                      link        : "https://plus.google.com/100541351182813566938"
                      locale      : "en"
                      name        : "Ɐha Hah (nerdfiles)"
                      picture     : "https://lh5.googleusercontent.com/-rJqQ0GxcdY8/AAAAAAAAAAI/AAAAAAAAD6o/PcGS4XpysFo/photo.jpg"
                 */
                bg = currentProfile.picture;
              }
              _username_prefix = _provider === 'twitter' ? '@' : _provider === 'google' ? '✚' : 'ᶠ';
              handle = _username_prefix + _username;
              angular.extend(user, {
                handle: handle
              });
              profileService.updateUser(handle, {
                lastLogin: new Date(),
                isLoggedIn: true,
                _id: _temporaryUser[_provider].id,
                canonicalPicture: bg || null,
                displayName: _displayName || null,
                hasToken: _temporaryUser[_provider].accessToken,
                hasTokenExpires: _temporaryUser.expires
              });
            }
            return (user ? user : $q.reject({
              authRequired: true
            }));
          } else {

          }
        });
      };
    };
    loginSimple = function($location, $q, $firebaseAuth, firebaseUtils, createProfile, profileService, localStorageService, $http, $timeout, notificationService) {

      /*
      loginSimple.
       */
      var auth, fns, listeners, statusChange;
      statusChange = function() {
        fns.user = auth.$getAuth();
        angular.forEach(listeners, function(fn) {
          fn(fns.user);
        });
      };
      auth = $firebaseAuth(firebaseUtils.ref());
      listeners = [];
      fns = {
        authMethod: null,
        user: null,
        requireAuth: function() {

          /*
          Require Authentication'd User Data
           */
          return auth.$requireAuth();
        },
        getUser: function() {

          /*
          Get user
           */
          return auth.$waitForAuth();
        },
        anonLogin: function() {

          /*
          Anonymous login for iOS users.
           */
          var def;
          def = $q.defer();
          auth.authAnonymously(function(error, authData) {
            if (error) {
              console.log("Authentication Failed!", error);
              return def.reject(error);
            } else {
              console.log("Authenticated successfully with payload:", authData);
              auth.anonymouslyLoggingIn = true;
              authData.handle = '@LoveStampio';
              authData.anonymouslyLoggedIn = true;
              return def.resolve(authData);
            }
          });
          return def.promise;
        },
        providerLogin: function(provider, captureOnly) {

          /*
          Provider Login
           */
          var authOptionsConstruct, def, __scope, _scope;
          authOptionsConstruct = null;
          def = $q.defer();
          if (provider === 'twitter') {
            authOptionsConstruct = {
              remember: 'sessionOnly'
            };
          }
          if (provider === 'google') {
            _scope = 'https://www.googleapis.com/auth/plus.login, https://www.googleapis.com/auth/plus.me, https://www.googleapis.com/auth/plus.circles.read';
            __scope = 'https://www.googleapis.com/auth/plus.login, https://www.googleapis.com/auth/plus.me, https://www.googleapis.com/auth/plus.stream.write, https://www.googleapis.com/auth/plus.circles.read';
            authOptionsConstruct = {
              remember: 'sessionOnly',
              scope: _scope
            };
          }
          if (provider === 'facebook') {
            authOptionsConstruct = {
              remember: 'sessionOnly',
              scope: 'user_friends'
            };
          }
          if (captureOnly) {
            auth.$authWithOAuthPopup(provider, authOptionsConstruct).then(function(authData) {
              return def.resolve(authData);
            });
          } else {
            auth.$authWithOAuthPopup(provider, authOptionsConstruct).then(function(authData) {
              return def.resolve(authData);
            })["catch"](function(error) {
              if (error.code === 'TRANSPORT_UNAVAILABLE') {
                return auth.$authWithOAuthRedirect(provider, authOptionsConstruct).then(function(authData) {
                  return def.resolve(authData);
                });
              } else {
                return notificationService.displayError('Unable to auth.');
              }
            });
          }
          return def.promise;
        },
        login: function(email, pass) {

          /*
          @param {string} email
          @param {string} pass
          @description
          
            authenticate so we have permission to write to Firebase
            store user data in Firebase after creating account
          
          @returns {*}
           */
          return auth.$authWithPassword({
            email: email,
            password: pass
          }, {
            rememberMe: true
          });
        },
        relogin: function(provider, accessId, accessToken, accessTokenSecret) {
          if (provider === 'twitter') {
            return auth.$authWithOAuthToken(provider, {
              'user_id': accessId,
              'oauth_token': accessToken,
              'oauth_token_secret': accessTokenSecret
            }, function(error, authData) {
              if (error) {
                console.log('Login Failed!', error);
              } else {
                console.log('Authenticated successfully with payload:', authData);
              }
            });
          } else {
            return auth.$authWithOAuthToken(provider, accessToken, function(error, authData) {
              if (error) {
                console.log('Login Failed!', error);
              } else {
                console.log('Authenticated successfully with payload:', authData);
              }
            });
          }
        },
        logout: function() {

          /*
          Logout.
           */
          fns.getUser().then(function(user) {
            var handle, removeDeviceToken, _handle;
            handle = null;
            if (user && user.provider) {
              _handle = user.handle;
              localStorageService.set('provider', user.provider);
              removeDeviceToken = function() {
                console.log(this);
              };
              profileService.updateUser(_handle, {
                isLoggedIn: false
              });
            }
            return auth.$unauth();
          });
        },
        createAccount: function(email, pass, name) {

          /*
          Create e-mail-based account.
           */
          return auth.$createUser({
            email: email,
            password: pass
          }).then(function() {
            return fns.login(email, pass);
          }).then(function(user) {
            return createProfile(user.uid, email, name).then(function() {
              return user;
            });
          });
        },
        changePassword: function(email, oldpass, newpass) {

          /*
          Change pass on administrator account.
           */
          return auth.$changePassword({
            email: email,
            oldPassword: oldpass,
            newPassword: newpass
          });
        },
        changeEmail: function(password, oldEmail, newEmail) {

          /*
          Change E-mail on Administrator.
           */
          return changeEmail(password, oldEmail, newEmail, this);
        },
        removeUser: function(email, pass) {

          /*
          Remove Administrator.
           */
          return auth.$removeUser({
            email: email,
            password: pass
          });
        },
        watch: function(cb, $scope) {

          /*
          Authentication Watcher.
           */
          var unbind;
          fns.getUser().then(function(user) {
            cb(user);
          });
          listeners.push(cb);
          unbind = function() {
            var i;
            i = listeners.indexOf(cb);
            if (i > -1) {
              listeners.splice(i, 1);
            }
          };
          if ($scope) {
            $scope.$on("$destroy", unbind);
          }
          return unbind;
        }
      };
      auth.$onAuth(statusChange);
      statusChange();
      return fns;
    };
    profileCreate = function(firebaseUtils, $q, $timeout) {

      /*
      Profile creation.
       */
      return function(id, email, name) {
        var def, firstPartOfEmail, ref, ucfirst;
        firstPartOfEmail = function(email) {
          return ucfirst(email.substr(0, email.indexOf("@")) || "");
        };
        ucfirst = function(str) {
          var f;
          str += "";
          f = str.charAt(0).toUpperCase();
          return f + str.substr(1);
        };
        ref = firebaseUtils.ref("users", id);
        def = $q.defer();
        ref.set({
          email: email,
          name: name || firstPartOfEmail(email)
        }, function(err) {
          $timeout(function() {
            if (err) {
              def.reject(err);
            } else {
              def.resolve(ref);
            }
          });
        });
        return def.promise;
      };
    };
    return __interface__.factory("user", ["login.simple", "$window", "notification.service", user]).factory("user.require", ["login.simple", "$q", "$rootScope", "profile.service", "notification.service", userRequire]).factory("login.simple", ["$location", "$q", "$firebaseAuth", "firebase.utils", "profile.create", "profile.service", "localStorageService", "$http", "$timeout", "notification.service", loginSimple]).factory("profile.create", ["firebase.utils", "$q", "$timeout", profileCreate]);
  });

}).call(this);

//# sourceMappingURL=authentication.js.map
