###
# fileOverview

     ____  _____ ____
    |    \(____ |  _ \
    | | | / ___ | |_| |
    |_|_|_\_____|  __/
                |_|

@module services  

## description

An map service for angular-leaflet-directive that represents a library of 
tools for creating Material Design configurations within Leaflet Maps.

###

define [
  'interface'
  'config'
  'lodash'
  'one-color'
  'leaflet-knn'
  'parse-address'
  'utils/firebase'
  'services/authentication'
], (__interface__, environment, _, color, leafletKnn, parseAddress) ->

  mapService = ($http, $timeout, $mdToast, $window, loggly, leafletData, $q, firebaseUtils, userRequire) ->
    ###
    Client-side service for map rendering and painting.
    ###

    serviceInterface =
        Owlish: {}
        Falconry: {}
    serviceInterface.storedDistancesObject = null
    serviceInterface.colls = []
    serviceInterface.defaultAddresses = []
    serviceInterface.f = undefined

    serviceInterface.getAddressByAlias = (alias) ->
      ###
      @param {string} alias  
      @return {Promise:string}
      ###

      sep = '/'
      def = $q.defer()
      _userRequire = userRequire()
      _userRequire.then (authData) ->
        handle = authData.handle
        _f = firebaseUtils.syncArray('users' + sep + handle + sep + 'stamps/local')

        _f.$loaded().then (localStamps) ->
          _localStamps = _.filter localStamps, (stamp) ->
            if alias is stamp.merchantAlias
              return stamp
          if _localStamps.length
            def.resolve {
              $meta: _.last(_localStamps)
              alias: alias
            }
          else
            def.reject false
          return
        return
      def.promise


    serviceInterface.Owlish.verifyAddress = (addressString) ->
      ###
      Process address string and verify address against Google's Geocoding API.

      @param {string} addressString
      @namespace verifyAddress
      @return {Promise}
      ###

      key = environment.google.maps.key
      urlApi = environment.google.maps.geocode
      prefix_address = 'address='
      prefix_key = 'key='

      _addressString = addressString.split('\n')
      formattedAddress = _addressString.join ' '

      _tmpAddress = parseAddress formattedAddress

      _address = _.map _tmpAddress, (addressComponent, keyname) ->
        if keyname is 'text'
          return addressComponent

      address = _.last(_.filter(_address, (addressAtom) ->
        addressAtom
      ))

      a = address.replace(/\ /g, '+')

      combinedAddress = [
        prefix_address + a
        prefix_key + key
      ].join '&'

      baseAddressApi = [
        urlApi
        combinedAddress
      ].join '?'

      console.log baseAddressApi

      $http.get(baseAddressApi)


    serviceInterface.Falconry.clearMap = (pathConstruct, map) ->
        ###
        @namespace clearMap
        @return {undefined}
        ###

        map.removeLayer pathConstruct
        return

    serviceInterface.customerCowPath = undefined
    serviceInterface.Falconry.Scout = (_linksActivityTracking, map) ->
        ###*
        Show Links Activity Tracking

        @event ngClick
        @namespace showLinksActivityTracking
        @return {undefined}
        ###

        linksActivityTracking = _linksActivityTracking

        __linksActivityTracking = _.filter linksActivityTracking, (personalIntensityLink) ->
            personalIntensityLink.merchant.personalIntensityQuota

        ___linksActivityTracking = _.map __linksActivityTracking, (locationObject) ->
            {
                lat: locationObject.location.lat
                lng: locationObject.location.lng
            }

        if serviceInterface.customerCowPath
            serviceInterface.Falconry.clearMap serviceInterface.customerCowPath, map
            serviceInterface.customerCowPath = undefined
        else
            # @see http://leafletjs.com/reference.html#polyline
            customerCowPath = L.polyline(___linksActivityTracking, {
                color: '#006EB7'
                dashArray: '5,5'
                noClip: true
                smoothFactor: .5
            })
            serviceInterface.customerCowPath = customerCowPath.addTo(map)

            _bounds = serviceInterface.customerCowPath.getBounds()
            map.fitBounds _bounds
        map.invalidateSize()


    serviceInterface.checkedMerchant = false
    serviceInterface.Falconry.MerchantGate = (merchantMarket, pointPath) ->
        ###*
        Cycle through merchant stamp within bounds.
        Merchants may Walk Mode a stamp such that they tether via the Internet 
        a stamp to their device's geocoordinates.

        @param {array} pointPath @use $scope.linkActivityTracking after init.
        ###

        def = $q.defer()
        _getLocation.then (data) ->

            merchantMarket.then (d) ->
                pointPath = pointPath or d
                # @gives [N, N, ...]
                $vertices_x = _.map pointPath, (arrayLike) ->
                    arrayLike.lat

                # @gives [N, N, ...]
                $vertices_y = _.map pointPath, (arrayLike) ->
                    arrayLike.lng

                $points_polygon = $vertices_x.length

                $latitude_y = data.coords.latitude
                $longitude_x = data.coords.longitude
                i = j = serviceInterface.checkedMerchant = 0
                i = 0
                j = $points_polygon - 1
                while i < $points_polygon
                    if $vertices_y[i] > $latitude_y != $vertices_y[j] > $latitude_y and $longitude_x < ($vertices_x[j] - $vertices_x[i]) * ($latitude_y - $vertices_y[i]) / ($vertices_y[j] - $vertices_y[i]) + $vertices_x[i]
                        serviceInterface.checkedMerchant = !serviceInterface.checkedMerchant
                    j = i++

                def.resolve serviceInterface.checkedMerchant
        def.promise


    serviceInterface.Falconry.Ringer = () ->
        ###
        @namespace Ringer
        @description

            Asynchronously coloring map markers' intensity circles.

        ###

        defaultColorSetting:
            lightness     : .90
            alphaActive   : .75
            alphaInactive : .25
            inactiveBlue  : '#D3D3D3'
            inactiveYellow : '#D9D9D9'
            startBlue     : '#4188c8'
            startYellow   : '#edc71b'
            startRed      : '#eb2227'
        color: (_merchantNode, callback) ->

            _f = _merchantNode.friendIntensityQuota or 0
            _p = _merchantNode.personalIntensityQuota or 0

            ###
            @see Notes on perceptual availability in services/bank.

            (Math.log1p(.2) / Math.E) * 100
            6.707235242686742
            (Math.log1p(.5) / Math.E) * 100
            14.916227738534996
            (Math.log1p(.9) / Math.E) * 100
            23.612484895881916
            ###
            f = (Math.log1p(_f) / Math.E) * 100
            p = (Math.log1p(_p) / Math.E) * 100

            merchantToPersonalActivity = @defaultColorSetting.alphaInactive
            merchantToFriendActivity = @defaultColorSetting.alphaInactive

            merchantStampImpressionYellow = color(@defaultColorSetting.inactiveYellow).
                lightness(@defaultColorSetting.lightness)
            merchantStampImpressionBlue = color(@defaultColorSetting.inactiveBlue).
                lightness(@defaultColorSetting.lightness)

            if p > 0
                merchantStampImpressionYellow = color(@defaultColorSetting.startYellow).saturation(p)
                merchantToPersonalActivity = @defaultColorSetting.alphaActive
            if f > 0
                merchantStampImpressionBlue = color(@defaultColorSetting.startBlue).saturation(f)
                merchantToFriendActivity = @defaultColorSetting.alphaActive

            _.extend _merchantNode,
                personalActivity               : merchantToPersonalActivity
                friendActivity                 : merchantToFriendActivity
                personalIntensityInactiveColor : '#D9D9D9'
                friendIntensityInactiveColor   : '#D3D3D3'
                friendIntensityQuotaColor      : merchantStampImpressionBlue.hex()
                personalIntensityQuotaColor    : merchantStampImpressionYellow.hex()

            $timeout ->
                callback null, _merchantNode
            , 0
            return


    # Return map service interface.
    serviceInterface

  __interface__.service 'center.service', [
    '$location'
    'localStorageService'
    'geolocation'
    '$timeout'
    ($location, localStorageService, geolocation, $timeout) ->
      ###
      @usage

          centerService.save(lat: 50.5006, lng: 4.48649, zoom: 7, autoDiscover: false)

      ###

      serviceInterface =
        center: {}

      locationParameters = $location.search()

      # Get center from url if available
      if locationParameters.initLng != undefined and !isNaN(parseFloat(locationParameters.initLng)) and locationParameters.initLat != undefined and !isNaN(parseFloat(locationParameters.initLat)) and locationParameters.initZoom != undefined and !isNaN(parseInt(locationParameters.initZoom))
        serviceInterface.center =
          lat          : parseFloat(locationParameters.initLat)
          lng          : parseFloat(locationParameters.initLng)
          zoom         : parseInt(locationParameters.initZoom)
          autoDiscover : false
      else
        if localStorageService.keys().indexOf('center') > -1
          serviceInterface.center = JSON.parse(localStorageService.get('center'))
        #else
          #$timeout ->
            #_getLocation = geolocation.getLocation().then (data) ->
                #_.extend serviceInterface.center,
                    #lat: data.coords.latitude
                    #lng: data.coords.longitude
          #, 0

      serviceInterface.save = (center) ->
        center.lat = parseFloat(center.lat)
        center.lng = parseFloat(center.lng)
        localStorageService.set 'center', center
        _.extend serviceInterface.center, center
        #console.log center
        return

      serviceInterface
  ]


  # Implement map service as factory.
  __interface__

    .factory "map.service", [
      "$http"
      "$timeout"
      "$mdToast"
      "$window"
      'loggly'
      'leafletData'
      '$q'
      'firebase.utils'
      'user.require'
      mapService
    ]
