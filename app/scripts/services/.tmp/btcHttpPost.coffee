btceHttpPost = (keyPair, method, params, nonce) ->
  return "{'error':'missing key pair'}"  if keyPair is `undefined`
  params = ""  if params is `undefined`
  
  # Cleanup keypair, remove all \s (any whitespace)
  keyPair = keyPair.replace(/[\s]/g, "")
  
  # Keypair example: "AFE730YV-S9A4FXBJ-NQ12HXS9-CA3S3MPM-CKQLU0PG,96a00f086824ddfddd9085a5c32b8a7b225657ae2fe9c4483b4c109fab6bf1a7"
  keyPair = keyPair.split(",")
  pubKey = keyPair[0]
  privKey = keyPair[1]
  
  # As specified on the BTC-e api (https://btc-e.com/api/documentation) the
  # nonce POST parameter must be an incrementing integer (>0). The easiest
  # implementation is the use of a timestamp (TS), so there is no need
  # for persistant storage. Preferable, the resolution of the TS should be
  # small enough the handle the desired call-frequency (a sleep of the TS
  # resolution can fix this but I don't like such a waste). Another
  # consideration is the sizeof the nonce supported by BTC-e. Experiments
  # revealed this is a 32 bit unsigned number. The native JavaScript TS,
  # stored in a float, can be 53 bits and has a resolution of 1 ms.
  
  # This time stamp counts amount of 200ms ticks starting from Jan 1st, 2014 UTC
  # On 22 Mar 2041 01:17:39 UTC, it will overflow the 32 bits and will fail
  # the nonce key for BTC-e
  nonce = Math.floor((Date.now() - Date.UTC(2014, 0)) / 200)  if nonce is `undefined`
  
  # Construct payload message
  msg = "nonce=" + nonce + "&method=" + method + params
  msgSign = Utilities.computeHmacSignature(Utilities.MacAlgorithm.HMAC_SHA_512, msg, privKey)
  
  # Convert encoded message from byte[] to hex string
  msgSignHex = []
  i = 0

  while i < msgSign.length
    
    # Doing it nibble by nibble makes sure we keep leading zero's
    msgSignHex.push ((msgSign[i] >>> 4) & 0xF).toString(16)
    msgSignHex.push (msgSign[i] & 0xF).toString(16)
    i++
  msgSignHex = msgSignHex.join("")
  httpHeaders =
    Key: pubKey
    Sign: msgSignHex

  fetchOptions =
    method: "post"
    headers: httpHeaders
    payload: msg

  reponse = UrlFetchApp.fetch("https://btc-e.com/tapi", fetchOptions)
  reponse.getContentText()
