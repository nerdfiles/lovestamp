
/*
 * fileOverview

                      _    ___ _                   _
                  _  (_)  / __|_)              _  (_)
     ____   ___ _| |_ _ _| |__ _  ____ _____ _| |_ _  ___  ____
    |  _ \ / _ (_   _) (_   __) |/ ___|____ (_   _) |/ _ \|  _ \
    | | | | |_| || |_| | | |  | ( (___/ ___ | | |_| | |_| | | | |
    |_| |_|\___/  \__)_| |_|  |_|\____)_____|  \__)_|\___/|_| |_|

 *# description

An notification service for consolidating error messaging and feeding 
special-case error directives and notifications.
 */

(function() {
  define(['interface', 'utils/firebase'], function(__interface__) {
    var notificationService;
    notificationService = function($http, $q, $mdToast, $window, loggly, firebaseUtils, $timeout, $interval) {

      /*
      @namespace notificationService  
      @description  
      
      Our service interface for dealing with remote and local error caching for proper debugging of unique external clients.
      
      Dependencies:
      
      1. Loggly
      2. AngularJS
      3. Possibly Firebase.
       */
      var content, serviceInterface;
      serviceInterface = {};
      serviceInterface.currentStatus = null;
      serviceInterface.logglyQueue = [];
      content = null;
      serviceInterface.cancelTransfer = false;
      serviceInterface.displayHomeScreenAdd = function(response, options) {
        var config, noteConfig, _config;
        config = options ? options : {};
        _config = _.extend({}, config);
        content = response;
        noteConfig = {
          hideDelay: _config.hideDelay,
          preserveScope: true,
          template: content
        };
        return $mdToast.show(noteConfig);
      };
      serviceInterface.displayLocationNote = function(noteConstruct) {
        return $mdToast.show(noteConstruct);
      };
      serviceInterface.displayError = serviceInterface.displayNote = function(response, options) {

        /*
        @namespace displayError
         */
        var config, def, noteConfig, onCompleteCheck, t, _config;
        def = $q.defer();
        config = options ? options : {};
        _config = _.extend({}, config);
        content = response;
        console.dir(_config);
        noteConfig = {
          hideDelay: _config.hideDelay,
          scope: _config.scope,
          preserveScope: true,
          template: content
        };
        if ($window.DEBUG) {
          serviceInterface.remotePost(content);
        }
        if (!options) {
          $mdToast.show($mdToast.simple().content(content));
        } else {
          onCompleteCheck = false;
          $mdToast.show(noteConfig);
          $interval(function() {
            if (notificationService.cancelTransfer === true) {
              def.reject(onCompleteCheck);
              return $mdToast.hide();
            }
          }, 50);
          if (_config.onComplete) {
            t = $timeout(function() {
              if (notificationService.cancelTransfer === true) {
                $timeout.cancel(t);
                def.reject(onCompleteCheck);
                return;
              }
              return _config.onComplete().then(function(transferStatus) {
                if (transferStatus === null) {
                  return def.reject(onCompleteCheck);
                }
                onCompleteCheck = true;
                serviceInterface.displayNote('Thanks! Check your ChangeTip Wallet now!');
                return def.resolve(onCompleteCheck);
              }, function(transferStatus) {
                if (transferStatus === null) {
                  return def.reject(onCompleteCheck);
                }
                if (transferStatus === false) {
                  serviceInterface.displayError('Failed to make transfer!');
                }
                return def.reject(onCompleteCheck);
              });
            }, 5000);
          } else {
            def.reject(onCompleteCheck);
          }
          if (_config.onComplete) {
            return def.promise;
          }
        }
      };
      serviceInterface.remotePost = function(status, type) {

        /*
        @param {string} status
        @param {string} type
        @return {Promise:object} Firebase object.
         */
        var currentStatus, logglyQueue, _type;
        logglyQueue = serviceInterface.logglyQueue || [];
        serviceInterface.currentStatus = status;
        _type = type || 'error';
        logglyQueue.push({
          type: _type,
          description: status
        });
        currentStatus = serviceInterface.currentStatus = logglyQueue.pop();
        loggly.log(currentStatus);
        return firebaseUtils.pushObject("app/errors", currentStatus);
      };
      return serviceInterface;
    };
    return __interface__.factory("notification.service", ["$http", "$q", "$mdToast", "$window", 'loggly', 'firebase.utils', '$timeout', '$interval', notificationService]);
  });

}).call(this);

//# sourceMappingURL=notification.js.map
