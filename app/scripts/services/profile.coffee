###*
# fileOverview

                        ___ _ _
                       / __|_) |
     ____   ____ ___ _| |__ _| | _____
    |  _ \ / ___) _ (_   __) | || ___ |
    | |_| | |  | |_| || |  | | || ____|
    |  __/|_|   \___/ |_|  |_|\_)_____)
    |_|

@module services  

## description

  These profiles are not necessarily part of authenticated users. Only 
  merchants really require profiles, whereas fans really only need 
  the ability to log in based on a "username" (Firebase's 
  ``$auth.provider !== null``) which can either be stored on the client 
  device or it can simply be remembered by the person.

###

define [
  "interface"
  "lodash"
  "config"
  "utils/firebase"
], (__interface__, _, environment) ->

  profileService = (firebaseUtils, $q, $timeout, $http, localStorageService, $window) ->

    ###
    A client-side service for dealing with profile display information and 
    interactivity.
    ###

    serviceInterface = {}
    serviceInterface.socialAspectsEnabled = []
    serviceInterface.connectedProviders = []
    serviceInterface.runningValidations = []
    serviceInterface.allUsers = undefined
    serviceInterface.topUser = undefined
    serviceInterface.stampSuccessData = {}
    serviceInterface.stampHistoryList = {}
    serviceInterface.users = undefined

    ###
    @description

      Social URL prefixes.

    ###
    serviceInterface.urlPrefix =
      '@': 'https://twitter.com/'
      'ᶠ': 'https://facebook.com/'
      '✚': 'https://plus.google.com/'


    ###
    @description

      Social prefixes available for clients to login via OAuth.

    ###
    serviceInterface.socialPrefixesAvailable =
      twitter  : '@' # at/about-symbol
      facebook : 'ᶠ' # modifier letter small f
      google   : '✚' # not-quus

    serviceInterface.socialProviderActive = null
    serviceInterface.socialPrefixesEnabled ?= {}
    serviceInterface.socialPrefixActive = null

    # Enable Twitter
    _.extend serviceInterface.socialPrefixesEnabled,
      _.pick serviceInterface.socialPrefixesAvailable, 'twitter'

    # Enable Google+
    _.extend serviceInterface.socialPrefixesEnabled,
      _.pick serviceInterface.socialPrefixesAvailable, 'google'

    # Enable Facebook
    _.extend serviceInterface.socialPrefixesEnabled,
      _.pick serviceInterface.socialPrefixesAvailable, 'facebook'


    serviceInterface.socialAspectsAvailable = (username) ->
      ###
      @description

        Social Aspects available/enabled configuration model.

      ###

      console.log 'Loading Social Aspects. . .'
      _.forEach serviceInterface.socialPrefixesEnabled, (socialAspectHandle, socialAspectProvider) ->
        loadedSocialAspect = localStorageService.get(socialAspectProvider)
        if loadedSocialAspect
          serviceInterface.socialAspectsEnabled.push loadedSocialAspect
        return
      firebaseUtils.updateObject("users/" + username + "/profile/extended", serviceInterface.socialAspectsEnabled)
      return


    ###
    @description

      We hold validations under several use-case scenarios which might align 
      with data transactions account:

          1. Requests
          2. Users
          3. Payments

    ###
    serviceInterface.getMerchantPictureInView = (merchantSheetData) ->
      ###
      @description

        Recasts a hash object as an array of label and value pairs.

      @namespace getMerchantPictureInView
      @param {object} merchantSheetData
      @return {Promise:string}
      ###

      def = $q.defer()
      googleImageServiceBaseUrl = environment.google.maps.streetView
      locationObject = merchantSheetData.address.location

      # Easy to read URL config.
      googleImageServiceParams =
        size     : $window.innerWidth + "x500"
        #size     : "400x400"
        location : "#{locationObject.lat},#{locationObject.lng}"
        fov      : '130'
        heading  : '300'
        pitch    : '10'
        key      : environment.google.maps.key

      ###
      @example ['propertyLabel1=propertyValue1', 'propertyLabel2=propertyValue2', ...]
      @return {array} stringifiedProperty
      ###
      googleImageServiceParamsUrlConstruct = _.map(googleImageServiceParams, (propertyValue, propertyLabel) ->
        propertySeparator = '='
        stringifiedProperty = propertyLabel + propertySeparator + propertyValue
      )

      # @note _.reduce method is faster than native Array.prototype.join: https://jsperf.com/reduce-lodash-array-join
      googleImageServiceParameterListConstruct = _.reduce googleImageServiceParamsUrlConstruct, (m, i, k) ->
        parameterSeparator = '&'
        if k > 0
          return m + parameterSeparator + i

      streetViewUrlComponents = [
        googleImageServiceBaseUrl
        googleImageServiceParameterListConstruct
      ]

      ###
      @description

        Joins URL and parameter list for Google Street View API search.

      @return P1?P2
      ###
      streetViewUrlConstruct = _.reduce streetViewUrlComponents, (m, i) ->
        querySeparator = '?'
        m + querySeparator + i

      $timeout ->
        def.resolve streetViewUrlConstruct
      , 0

      def.promise


    serviceInterface.addValidation = (v) ->
      ###
      @namespace addValidation
      @description

        Add a validation to the profile of the user to keep interim track.

        Use-cases are for ID, Normal Stamp Impression, Stamp Recall.

      ###

      @runningValidations.push v
      return

    serviceInterface.clearValidations = () ->
      @runningValidations = []
      return


    serviceInterface.getLoggedInUsersCount = () ->
      ###
      Get Logged In Users Count
      @namespace getLoggedInUserCount
      @description

        Collect logged in users to determine progress against top user.

      ###
      if serviceInterface.users
        _users = serviceInterface.users
      else
        _users = serviceInterface.users = firebaseUtils.syncArray('users')

      def = $q.defer()
      _sum = null
      _users.$loaded().then (__users) ->
        _sum = _.map __users, (_user) ->
          if _user.isLoggedIn
            return _user.isLoggedIn
        _sum = _.filter _sum, (s) ->
          s
        def.resolve _sum
      def.promise


    serviceInterface.userData = undefined

    serviceInterface.getUserStatus = (username) ->
      ###
      Get User Status
      @namespace getUpdateStatus
      @description

        Get user status data.

      ###

      userData = serviceInterface.userData = firebaseUtils.syncObject('users/' + username)


    serviceInterface.getUserRequests = (username) ->
      ###
      Get Request Status
      @namespace getUserRequests
      @description

        Get request status data.

      ###

      if serviceInterface.requestData
        requestData = serviceInterface.requestData
      else
        requestData = serviceInterface.requestData = firebaseUtils.syncObject('requests/' + username)

      requestData


    serviceInterface.getMerchantStatus = (username) ->
      ###
      Get User Status
      @namespace getMerchantStatus
      @description

        Get merchant status data.

      ###

      merchantData = firebaseUtils.syncObject('users/' + username)
      merchantData


    serviceInterface.getStampHistory = (handle, stampId) ->
      ###
      Get Stampening History
      @namespace stampHistory
      @description

        Get a history of the customer's stampenings from a particular stamp.

      ###

      if serviceInterface.stampHistoryList[stampId]
        return serviceInterface.stampHistoryList[stampId]

      serviceInterface.stampHistoryList[stampId] = firebaseUtils.syncArray('users/' + handle + '/stamps/stats/success/' + stampId)


    serviceInterface.getUserTransactions = (username) ->
      ###
      Get User Transactions
      @namespace getUserTransactions
      @description

        Get user transactions for displaying stats on front end and for use 
        in front end display calculations.

      ###

      if serviceInterface.stampSuccessData[username]
        return serviceInterface.stampSuccessData[username]

      serviceInterface.stampSuccessData[username] = firebaseUtils.syncArray('users/' + username + '/stamps/success')


    serviceInterface.getTopUser = () ->
      ###
      Get Top User
      @namespace getTopUser
      @description

        The top user within LoveStamp's balance sheet.

      ###

      def = $q.defer()

      if serviceInterface.topUser
        topUser = serviceInterface.topUser
      else
        topUser = serviceInterface.topUser = firebaseUtils.syncObject('balances/topUser')

      topUser.$loaded().then (topUserBitsCollected) ->
        def.resolve(parseFloat(topUserBitsCollected.$value))
      def.promise


    serviceInterface.loadedProfile = undefined

    serviceInterface.getProfile = (username) ->
      ###*
      @namespace getProfile
      @description

        Get social profile data.

      ###

      def = $q.defer()

      if serviceInterface.loadedProfile
        loadedProfile = serviceInterface.loadedProfile
      else
        loadedProfile = serviceInterface.loadedProfile = firebaseUtils.syncObject('users/' + username + '/profile')

      loadedProfile.$loaded().then (userProfileData) ->
        _userProfileData = _.map userProfileData, (property, key) ->
          return  if key.indexOf('$') isnt -1
          else
            obj = {}
            obj[key] = property
            return obj
        __userProfileData = _.filter _userProfileData, (u) ->
          u
        def.resolve __userProfileData

      def.promise


    serviceInterface.getProfileById = (id) ->
      ###*
      @namespace getProfileById
      @description

        Get social profile data by Id.

      ###

      _users = firebaseUtils.syncArray('users')

      def = $q.defer()

      _users.$loaded().then (__users) ->
        givenUser = _.last _.filter _.map __users, (_user, handle) ->
          if _user.$id == id or _user._id == id
            _user.handle = handle
            return _user
        #console.log givenUser
        def.resolve givenUser
      def.promise


    serviceInterface.getUserById = (id) ->
      ###*
      @namespace getProfileById
      @description

        Get social profile data by Id.

      ###

      _users = firebaseUtils.syncArray('users')

      def = $q.defer()

      _users.$loaded().then (__users) ->
        givenUser = _.last _.filter _.map __users, (_user) ->
          if _user._id == id
            return _user
        #console.log givenUser
        def.resolve givenUser
      def.promise


    serviceInterface.updateProfile = (username, formObject) ->
      ###
      Update Profile
      @namespace updateProfile
      @description

        Update user profile.

      ###

      try
        f = firebaseUtils.updateObject("users/" + username + "/profile", formObject)
      catch e
        console.log e
        f = firebaseUtils.transmuteObject("users/" + username + "/profile", formObject)

      f

    serviceInterface.updateUserData = (username, formObject) ->
      ###
      Update Profile
      @namespace updateProfile
      @description

        Update user profile.

      ###

      try
        f = firebaseUtils.updateObject("users/" + username, formObject)
      catch e
        console.log e
        f = firebaseUtils.transmuteObject("users/" + username, formObject)

      f



    serviceInterface.updateProfileBranch = (handle, _potentialAction, formObject) ->
      ###
      @see https://schema.org/potentialAction/
      @description 

        A specification for leaf structures for profile elements.

      @examples

        /profile.user.bio.content.moreObjectCollections.anotherObjectCollection
        /profile.bio.address.shipping.content
        /profile.address.billing.content

      ###

      metaObjectCollection = formObject
      potentialAction = if _potentialAction.length then _potentialAction.split '.' else []

      # Join 'em up.
      merchantSchemaObject = _.reduce potentialAction, (m, i, k) ->
        parameterSeparator = '/'
        if k > 0
          return m + parameterSeparator + i

      if formObject.status is 'ZERO_RESULTS' or formObject is ''

        return

      hashBlock =
        "user/bio/content"         : "bio"
        "address/shipping/content" : "addressShipping"
        "address/billing/content"  : "addressBilling"
        "overview"                 : "overview"

      if hashBlock[merchantSchemaObject] is 'bio'
        firebaseUtils.writeObject("users/" + handle + "/profile/" + hashBlock[merchantSchemaObject], metaObjectCollection)
      else if hashBlock[merchantSchemaObject] is 'overview'
        firebaseUtils.writeObject("users/" + handle + "/profile/" + hashBlock[merchantSchemaObject], metaObjectCollection)
      else

        verifiedAddress = metaObjectCollection
        _verifiedAddress = verifiedAddress.results[0].address_components
        _verifiedGeometry = verifiedAddress.results[0].geometry
        _verifiedLocation = _verifiedGeometry.location

        # Array of labels for given address
        types = _.map _.pluck(_verifiedAddress, 'types'), (types) ->
          _.first(types) # @example locality

        # Array of values for given address
        longNames = _.pluck _verifiedAddress, 'long_name'

        # @example {
        #   street_number               : "1600",
        #   route                       : "Amphitheatre Parkway",
        #   locality                    : "Mountain View",
        #   administrative_area_level_1 : "CA",
        #   country                     : "United States",
        #   postal_code                 : "94043"
        # }
        _verifiedAddress = _.zipObject( types, longNames )

        firebaseUtils.writeObject("users/" + handle + "/profile/" + hashBlock[merchantSchemaObject], _verifiedAddress)


    ###
    @namespace updateUser

    @description

      Update a user object under administrative forms.

    @return {object:Promise} updatedUserObject
    ###
    serviceInterface.updateUser = (username, updateObject) ->
      updatedUserObject = firebaseUtils.updateObject("users/" + username, updateObject)


    ###
    @namespace getNetworkUsers
    @description

      Get list of users connected to the primary user.

    @param {string} _provider
    @param {string} id
    @return {object:Promise} foundOriginUserNetworks.network
    ###
    serviceInterface.getNetworksByUserId = (_provider, id) ->
      def = $q.defer()

      @getAllUsers().$loaded().then (_userList) ->
        foundOriginUser = false
        foundOriginUserNetworks = null

        _.forEach _userList, (_user) ->
          if _user.profile and _user.profile.networks

            userNetworks = _.toArray(_user.profile.networks) # ['network:id', 'network:id', ...]

            _.forEach(userNetworks, (networkConstruct) ->
              #console.log networkConstruct

              try
                if networkConstruct.replace('[', '').replace(']', '') == id
                  foundOriginUser = true
                  foundOriginUserNetworks = { 'networks': _user.profile.networks }
                  return
              catch e
                console.log 'Could not find network user.'
            )

          if foundOriginUser
            return

        profileIds = {}
        networks = {}

        if foundOriginUserNetworks
          _.extend foundOriginUserNetworks.networks, JSON.parse("{ \"#{_provider}\": \"[#{id}]\" }")
          _.extend networks, foundOriginUserNetworks
        else
          _.extend profileIds, JSON.parse("{ \"#{_provider}\": \"#{id}\" }")
          _.extend networks, { 'networks': profileIds }

        def.resolve networks

      def.promise


    serviceInterface.getAllUsers = () ->
      ###
      @namespace getAllUsers
      @endpoint users/
      @return {array} users
      @description

        A collection of users to be used in administrative forms.

      ###

      if serviceInterface.allUsers
        return serviceInterface.allUsers
      serviceInterface.allUsers = firebaseUtils.syncArray('users')


    # Return our Service Interface.
    serviceInterface

  __interface__.
    factory "profile.service", [
        "firebase.utils"
        "$q"
        "$timeout"
        "$http"
        "localStorageService"
        "$window"
        profileService
      ]
