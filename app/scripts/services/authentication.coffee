###
# fileOverview

                       _                      _                   _
                   _  | |                 _  (_)              _  (_)
     _____ _   _ _| |_| |__  _____ ____ _| |_ _  ____ _____ _| |_ _  ___  ____
    (____ | | | (_   _)  _ \| ___ |  _ (_   _) |/ ___|____ (_   _) |/ _ \|  _ \
    / ___ | |_| | | |_| | | | ____| | | || |_| ( (___/ ___ | | |_| | |_| | | | |
    \_____|____/   \__)_| |_|_____)_| |_| \__)_|\____)_____|  \__)_|\___/|_| |_|

## description

  The authenticationService definition (with Entitlements).

###

define [
  "interface"
  "lodash"
  "utils/firebase"
  "services/profile"
  "services/notification"
], (__interface__, _) ->

  user = (loginSimple, $window, notificationService) ->
    ###
    User object specification of basic properties.
    ###

    currentUser = loginSimple.getUser()

    if currentUser and currentUser.$$state and currentUser.$$state.value

      _temporaryUser = currentUser.$$state.value
      _provider = _temporaryUser.provider

      _username = _temporaryUser[_provider].username or _temporaryUser[_provider].id
      _username_prefix = if _provider is 'twitter' then '@' else if _provider == 'google' then '✚' else 'ᶠ'
      handle = _username_prefix + _username

      _displayName = if _temporaryUser[_provider].displayName then _temporaryUser[_provider].displayName else null

      # We use a custom handle to pass around the frontend and backend of the application. Hash for UUID.
      if auth.anonymouslyLoggingIn == true
        angular.extend currentUser.$$state.value, handle: '@LoveStampio', anonymouslyLoggedIn: true
      else
        angular.extend currentUser.$$state.value, handle: handle

      try
        # Collect a non-normative display name for the social media user.
        angular.extend currentUser.$$state.value, displayName: _displayName
      catch e
        notificationService.displayNote 'Failed to capture display name.'

    currentUser


  userRequire = (loginSimple, $q, $rootScope, profileService, notificationService) ->
    ###

    @function
    @inner
    @description

      A simple wrapper on loginSimple.getUser() that rejects the promise
      if the user does not exists (i.e. makes user required)

    ###

    return () ->
      loginSimple.getUser().then (user) ->

        if user

          _temporaryUser = user
          _provider = _temporaryUser.provider
          _username = _temporaryUser[_provider].username or _temporaryUser[_provider].id

          currentProfile = profileService.currentProfile = user[user.provider].cachedUserProfile

          if currentProfile

            _displayName = currentProfile.displayName or currentProfile.screen_name or currentProfile.name

            ###
            provider: twitter
              {
                  "id": 1048197164,
                  "id_str": "1048197164",
                  "name": "RattelyrDragon",
                  "screen_name": "RattelyrDragon",
                  "location": "",
                  "profile_location": null,
                  "description": "Rattelyr dragons are territorial creatures. They like to burrow through loose earth and surprise opponents by bursting out of the soil.",
                  "url": null,
                  "entities": {
                      "description": {
                          "urls": []
                      }
                  },
                  "protected": false,
                  "followers_count": 54,
                  "friends_count": 159,
                  "listed_count": 4,
                  "created_at": "Sun Dec 30 15:49:05 +0000 2012",
                  "favourites_count": 73,
                  "utc_offset": null,
                  "time_zone": null,
                  "geo_enabled": false,
                  "verified": false,
                  "statuses_count": 67,
                  "lang": "en",
                  "status": {
                      "created_at": "Tue Jun 30 18:32:31 +0000 2015",
                      "id": 615951061237436400,
                      "id_str": "615951061237436417",
                      "text": "Ulterior States [IamSatoshi Documentary] https://t.co/hAnpwnjCff via @YouTube",
                      "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
                      "truncated": false,
                      "in_reply_to_status_id": null,
                      "in_reply_to_status_id_str": null,
                      "in_reply_to_user_id": null,
                      "in_reply_to_user_id_str": null,
                      "in_reply_to_screen_name": null,
                      "geo": null,
                      "coordinates": null,
                      "place": null,
                      "contributors": null,
                      "retweet_count": 0,
                      "favorite_count": 0,
                      "entities": {
                          "hashtags": [],
                          "symbols": [],
                          "user_mentions": [
                              {
                                  "screen_name": "YouTube",
                                  "name": "YouTube",
                                  "id": 10228272,
                                  "id_str": "10228272",
                                  "indices": [
                                      69,
                                      77
                                  ]
                              }
                          ],
                          "urls": [
                              {
                                  "url": "https://t.co/hAnpwnjCff",
                                  "expanded_url": "https://youtu.be/yQGQXy0RIIo",
                                  "display_url": "youtu.be/yQGQXy0RIIo",
                                  "indices": [
                                      41,
                                      64
                                  ]
                              }
                          ]
                      },
                      "favorited": false,
                      "retweeted": false,
                      "possibly_sensitive": false,
                      "lang": "ro"
                  },
                  "contributors_enabled": false,
                  "is_translator": false,
                  "is_translation_enabled": false,
                  "profile_background_color": "131516",
                  "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/751293601/516347dd4135c82927886800eefd48df.jpeg",
                  "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/751293601/516347dd4135c82927886800eefd48df.jpeg",
                  "profile_background_tile": true,
                  "profile_image_url": "http://pbs.twimg.com/profile_images/3042487582/d6436b5945913a0bdd12f10414a7efb0_normal.jpeg",
                  "profile_image_url_https": "https://pbs.twimg.com/profile_images/3042487582/d6436b5945913a0bdd12f10414a7efb0_normal.jpeg",
                  "profile_banner_url": "https://pbs.twimg.com/profile_banners/1048197164/1424560794",
                  "profile_link_color": "222222",
                  "profile_sidebar_border_color": "FFFFFF",
                  "profile_sidebar_fill_color": "DDEEF6",
                  "profile_text_color": "333333",
                  "profile_use_background_image": true,
                  "has_extended_profile": false,
                  "default_profile": false,
                  "default_profile_image": false,
                  "following": false,
                  "follow_request_sent": false,
                  "notifications": false,
                  "suspended": false,
                  "needs_phone_verification": false
              }

            @provider google

            @provider facebook

            ###

            twitterBg = currentProfile.profile_image_url
            facebookBg = currentProfile.picture and currentProfile.picture.data
            googleBg = currentProfile.picture and not currentProfile.picture.data

            if twitterBg
              bg = currentProfile.profile_image_url.replace('_normal', '')

            if facebookBg
              bg = currentProfile.picture.data.url

            if googleBg
              ###
                  google_spec =
                    family_name : "Hah"
                    gender      : "other"
                    given_name  : "Ɐha"
                    id          : "100541351182813566938"
                    link        : "https://plus.google.com/100541351182813566938"
                    locale      : "en"
                    name        : "Ɐha Hah (nerdfiles)"
                    picture     : "https://lh5.googleusercontent.com/-rJqQ0GxcdY8/AAAAAAAAAAI/AAAAAAAAD6o/PcGS4XpysFo/photo.jpg"
              ###

              bg = currentProfile.picture

            _username_prefix = if _provider is 'twitter' then '@' else if _provider == 'google' then '✚' else 'ᶠ'

            handle = _username_prefix + _username

            angular.extend user, handle: handle

            #console.log _temporaryUser

            profileService.updateUser(handle, {
              lastLogin       : new Date()
              isLoggedIn      : true
              _id             : _temporaryUser[_provider].id
              canonicalPicture: bg or null
              displayName     : _displayName or null
              hasToken        : _temporaryUser[_provider].accessToken
              hasTokenExpires : _temporaryUser.expires
            })

          return (if user then user else $q.reject(authRequired: true))
        else
          #return notificationService.displayError 'Unable to authenticate user.'
          return

  loginSimple = ($location, $q, $firebaseAuth, firebaseUtils, createProfile, profileService, localStorageService, $http, $timeout, notificationService) ->
    ###
    loginSimple.
    ###

    statusChange = () ->
      fns.user = auth.$getAuth()
      angular.forEach listeners, (fn) ->
        fn fns.user
        return
      return

    auth = $firebaseAuth firebaseUtils.ref()

    listeners = []
    fns =
      authMethod  : null
      user        : null
      requireAuth : ->
        ###
        Require Authentication'd User Data
        ###

        auth.$requireAuth()

      getUser: ->
        ###
        Get user
        ###

        auth.$waitForAuth()

      anonLogin: () ->
        ###
        Anonymous login for iOS users.
        ###
        def = $q.defer()

        auth.authAnonymously((error, authData) ->
          if error
            console.log("Authentication Failed!", error)
            def.reject error
          else
            console.log("Authenticated successfully with payload:", authData)
            auth.anonymouslyLoggingIn = true
            authData.handle = '@LoveStampio'
            authData.anonymouslyLoggedIn = true
            def.resolve authData
        )

        def.promise

      providerLogin: (provider, captureOnly) ->
        ###
        Provider Login
        ###

        authOptionsConstruct = null
        def = $q.defer()

        if provider == 'twitter'
            authOptionsConstruct =
                remember : 'sessionOnly'

        if provider == 'google'

            _scope = 'https://www.googleapis.com/auth/plus.login,
                      https://www.googleapis.com/auth/plus.me,
                      https://www.googleapis.com/auth/plus.circles.read'

            __scope = 'https://www.googleapis.com/auth/plus.login,
                       https://www.googleapis.com/auth/plus.me,
                       https://www.googleapis.com/auth/plus.stream.write,
                       https://www.googleapis.com/auth/plus.circles.read'

            authOptionsConstruct =
                remember : 'sessionOnly'
                scope    : _scope

        if provider == 'facebook'

            authOptionsConstruct =
                remember : 'sessionOnly'
                scope    : 'user_friends'

        if captureOnly

            auth.$authWithOAuthPopup(provider, authOptionsConstruct).then (authData) ->
                def.resolve authData
            #, (errorData) ->
                #auth.$authWithOAuthRedirect(provider, authOptionsConstruct).then (authData) ->
                    #def.resolve authData

        else

            auth.$authWithOAuthPopup(provider, authOptionsConstruct).then((authData) ->
                def.resolve authData
            ).catch((error) ->
              if error.code == 'TRANSPORT_UNAVAILABLE'
                auth.$authWithOAuthRedirect(provider, authOptionsConstruct).then((authData) ->
                  def.resolve authData
                )
              else notificationService.displayError 'Unable to auth.'
            )

        def.promise


      login: (email, pass) ->
        ###
        @param {string} email
        @param {string} pass
        @description

          authenticate so we have permission to write to Firebase
          store user data in Firebase after creating account

        @returns {*}
        ###

        auth.$authWithPassword
          email: email
          password: pass
        ,
          rememberMe: true

      relogin: (provider, accessId, accessToken, accessTokenSecret) ->

        if provider == 'twitter'
          # Authenticate with Twitter using an existing OAuth 1.0a credential set
          auth.$authWithOAuthToken provider, {
            'user_id'            : accessId
            'oauth_token'        : accessToken
            'oauth_token_secret' : accessTokenSecret
          }, (error, authData) ->
            if error
              console.log 'Login Failed!', error
            else
              console.log 'Authenticated successfully with payload:', authData
            return
        else
          # Authenticate with Facebook/Google using an existing OAuth 2.0 access token
          auth.$authWithOAuthToken provider, accessToken, (error, authData) ->
            if error
              console.log 'Login Failed!', error
            else
              console.log 'Authenticated successfully with payload:', authData
            return

      logout: () ->
        ###
        Logout.
        ###

        fns.getUser().then (user) ->
          handle = null

          if user and user.provider

            _handle = user.handle

            localStorageService.set('provider', user.provider)

            removeDeviceToken = ->
              console.log @
              return

            profileService.updateUser(_handle, {
              isLoggedIn: false
            })

          auth.$unauth()
        return

      createAccount: (email, pass, name) ->
        ###
        Create e-mail-based account.
        ###

        auth.$createUser(
          email: email
          password: pass
        ).then(() ->
          fns.login email, pass
        ).then((user) ->
          createProfile(user.uid, email, name).then ->
            user
        )

      changePassword: (email, oldpass, newpass) ->
        ###
        Change pass on administrator account.
        ###

        auth.$changePassword
          email: email
          oldPassword: oldpass
          newPassword: newpass

      changeEmail: (password, oldEmail, newEmail) ->
        ###
        Change E-mail on Administrator.
        ###

        changeEmail password, oldEmail, newEmail, this

      removeUser: (email, pass) ->
        ###
        Remove Administrator.
        ###

        auth.$removeUser
          email: email
          password: pass

      watch: (cb, $scope) ->
        ###
        Authentication Watcher.
        ###

        fns.getUser().then (user) ->
          cb user
          return

        listeners.push cb
        unbind = ->
          i = listeners.indexOf(cb)
          listeners.splice i, 1  if i > -1
          return

        $scope.$on "$destroy", unbind  if $scope
        unbind

    auth.$onAuth statusChange
    statusChange()

    return fns


  profileCreate = (firebaseUtils, $q, $timeout) ->
    ###
    Profile creation.
    ###

    return (id, email, name) ->

      firstPartOfEmail = (email) ->
        ucfirst email.substr(0, email.indexOf("@")) or ""

      ucfirst = (str) ->
        # credits: http://kevin.vanzonneveld.net
        str += ""
        f = str.charAt(0).toUpperCase()
        f + str.substr(1)

      ref = firebaseUtils.ref("users", id)
      def = $q.defer()
      ref.set
        email: email
        name: name or firstPartOfEmail(email)
      , (err) ->
        $timeout ->
          if err
            def.reject err
          else
            def.resolve ref
          return

        return

      def.promise


  # Implement factory interfaces.
  __interface__

    .factory("user", [
      "login.simple"
      "$window"
      "notification.service"
      user
    ])

    .factory("user.require", [
      "login.simple"
      "$q"
      "$rootScope"
      "profile.service"
      "notification.service"
      userRequire
    ])

    .factory("login.simple", [
      "$location"
      "$q"
      "$firebaseAuth"
      "firebase.utils"
      "profile.create"
      "profile.service"
      #"email.change"
      "localStorageService"
      "$http"
      "$timeout"
      "notification.service"
      loginSimple
    ])

    .factory "profile.create", [
      "firebase.utils"
      "$q"
      "$timeout"
      profileCreate
    ]
