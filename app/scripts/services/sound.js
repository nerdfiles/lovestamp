
/*
 * fileOverview
                                _ 
                               | |
      ___  ___  _   _ ____   __| |
     /___)/ _ \| | | |  _ \ / _  |
    |___ | |_| | |_| | | | ( (_| |
    (___/ \___/|____/|_| |_|\____|

 *# description

A cross-browser sound service with fallback to emulate in browser.
 */

(function() {
  var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  define(['interface'], function(__interface__) {
    var soundService;
    window.ua = navigator.userAgent.toLowerCase();
    window.isAndroid = /android/.test(ua);
    window.isiOs = /(iphone|ipod|ipad)/.test(ua);
    window.isStandAlone = window.navigator.standalone;
    window.isiPad = ua.match(/ipad/);
    window.isDevice = __indexOf.call(window, 'ontouchstart') >= 0;
    window.isChrome = __indexOf.call(window, "chrome") >= 0;
    window.isMoz = __indexOf.call(window, "mozAnimationStartTime") >= 0;
    window.Media = function(src, mediaSuccess, mediaError, mediaStatus) {

      /*
      @param {string} src
        A URI containing the audio content. (DOMString)
      @param {function} mediaSuccess (Optional)
        The callback that executes after a Media object has completed the current play, record, or stop action. (Function)
      @param {function} mediaError (Optional)
        The callback that executes if an error occurs. (Function)
      @param {function} mediaStatus (Optional)
        The callback that executes to indicate status changes. (Function)
       */
      var sound;
      if (typeof Audio !== 'function' && typeof Audio !== 'object') {
        console.warn('HTML5 Audio is not supported in this browser');
      }
      sound = new Audio;
      sound.src = src;
      sound.addEventListener('ended', mediaSuccess, false);
      sound.load();
      return {
        getCurrentPosition: function(mediaSuccess, mediaError) {
          mediaSuccess(sound.currentTime);
        },
        getDuration: function() {
          if (isNaN(sound.duration)) {
            return -1;
          } else {
            return sound.duration;
          }
        },
        play: function() {
          sound.play();
        },
        pause: function() {
          sound.pause();
        },
        release: function() {},
        seekTo: function(milliseconds) {},
        setVolume: function(volume) {
          sound.volume = volume;
        },
        startRecord: function() {},
        stopRecord: function() {},
        stop: function() {
          sound.pause();
          if (mediaSuccess) {
            mediaSuccess();
          }
        }
      };
    };
    soundService = function($q, $window) {

      /*
       */
      var serviceInterface, _logError;
      serviceInterface = {};
      _logError = function(src, err) {
        console.log(src);
        console.error('media error', {
          code: err.code,
          message: getErrorMessage(err.code)
        });
      };
      serviceInterface.loadMedia = function(src, onError, onStatus, onStop) {
        var def, mediaError, mediaStatus, mediaSuccess, postFixExtension;
        def = $q.defer();
        if (isMoz) {
          postFixExtension = '.ogg';
        } else {
          postFixExtension = '.mp3';
        }
        src = src + postFixExtension;
        if (window.is_device) {
          document.addEventListener('deviceready', function() {
            var mediaError, mediaStatus, mediaSuccess;
            mediaSuccess = function() {
              if (onStop) {
                onStop();
              }
            };
            mediaError = function(err) {
              _logError(src, err);
              if (onError) {
                console.log(onError(err));
              }
            };
            mediaStatus = function(status) {
              if (onStatus) {
                console.log(onStatus(status));
              }
            };
            src = './' + src;
            def.resolve(new $window.Media(src, mediaSuccess, mediaError, mediaStatus));
          });
        } else {
          mediaSuccess = function() {
            if (onStop) {
              onStop();
            }
          };
          mediaError = function(err) {
            _logError(src, err);
            if (onError) {
              console.log(onError(err));
            }
          };
          mediaStatus = function(status) {
            if (onStatus) {
              console.log(onStatus(status));
            }
          };
          def.resolve(new $window.Media(src, mediaSuccess, mediaError, mediaStatus));
        }
        return def.promise;
      };
      serviceInterface.getStatusMessage = function(status) {
        var message;
        message = null;
        if (status === 0) {
          message = 'Media.MEDIA_NONE';
        } else if (status === 1) {
          message = 'Media.MEDIA_STARTING';
        } else if (status === 2) {
          message = 'Media.MEDIA_RUNNING';
        } else if (status === 3) {
          message = 'Media.MEDIA_PAUSED';
        } else if (status === 4) {
          message = 'Media.MEDIA_STOPPED';
        } else {
          message = 'Unknown status <' + status + '>';
        }
        return message;
      };
      serviceInterface.getErrorMessage = function(code) {
        var message;
        message = null;
        if (code === 1) {
          message = 'MediaError.MEDIA_ERR_ABORTED';
        } else if (code === 2) {
          message = 'MediaError.MEDIA_ERR_NETWORK';
        } else if (code === 3) {
          message = 'MediaError.MEDIA_ERR_DECODE';
        } else if (code === 4) {
          message = 'MediaError.MEDIA_ERR_NONE_SUPPORTED';
        } else {
          message = 'Unknown code <' + code + '>';
        }
        return message;
      };
      return serviceInterface;
    };
    return __interface__.factory('sound.service', ['$q', '$window', soundService]);
  });

}).call(this);

//# sourceMappingURL=sound.js.map
