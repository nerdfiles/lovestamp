
/**
@fileOverview
Stamp service loads unique stamps for the given dashboard and facilitates stamp platform controllers.
 */

(function() {
  define(["interface", 'utils/firebase'], function(__interface__) {
    var userTrack;
    userTrack = function(firebaseUtils, $q, $timeout) {
      return function(handle) {
        var history, ref;
        ref = firebaseUtils.ref("users", handle);
        history = ref.child("history");
      };
    };
    return __interface__.factory("user.track", ["firebase.utils", "$q", "$timeout", userTrack]);
  });

}).call(this);

//# sourceMappingURL=track.js.map
