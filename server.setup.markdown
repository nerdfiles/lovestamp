# LoveStamp admin

## Log in

$ sshpass -p t0d4y1sth3d4y! sshroot@ 104.131.89.71

## Preliminary Configuration Steps

1. $ sudo apt-get install build-essentials
2. $ sudo apt-ge install apache2 # ignore this, we’re using Node.js’s Express Server for both Hosting and API
3. $ sudo apt-get update
4. $ sudo apt-get install git tig legit git-flow

### Configuration of swap memory

Reference: https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04

1. $ sudo swapon -s # to check for swapfile: 
2. Or try: $ free -m #check for memory allocations
3. $ sudo fallocate -l 4G /swapfile # create new swap file
4. $ ls -lh /swapfile # review new swap file
5. $ sudo chmod 600 /swapfile # set permissions
6. $ ls -lh /swapfile # check permissions
7. $ sudo mkswap /swapfile # set up the swap space
8. $ sudo swapon /swapfile # accordingly
9. $ sudo swapon -s # verify that it is running

#### Swappiness

1. $ cat /proc/sys/vm/swappiness # check for swappiness; we want a value closer to 10 for virtual private servers
2. $ sudo sysctl vm.swappiness=10
3. $ sudo vim /etc/sysctl.conf # @TODO Add ``vm.swappiness=10``

#### System Pressure

1. $ cat /proc/sys/vm/vfs_cache_pressure # will likely output 100, which implies that inode information is being removed too quickly
2. $ sudo sysctl vm.vfs_cache_pressure=50
3. $ sudo vim /etc/sysctl.conf # @TODO Add ``vm.vfs_cache_pressure = 50``

### Configuration of Firewall and Security 

1. TBD.

### Configuration for Deployments

Review ``./Gruntfile.js``.

## Front End Configuration

We are building to node ``0.10.33``. ``apt-get``’s node won’t do. Here are 
a few more installation commands to consider:

    $ sudo apt-get install -y build-essential openssl libssl-dev pkg-config
    $ sudo apt-get install build-essential

For building SASS files on Ubuntu systems:

### Installing Compass Dependencies

    $ sudo apt-get install ruby-dev
    $ gem install compass
    $ gem install sass
    $ gem install foundation

### Installing Grunt Dependencies

Just in case ``npm`` isn’t getting dependencies right:

    $ cd /var/www/html
    $ rm -rf node_modules
    $ sudo npm cache clear
    $ sudo npm cache clean # try both!!!
    $ npm install

### For Sprite Generation

For ``spritesmith``:

    $ sudo apt-get install imagemagick libmagick9-dev

To be fair, our ``devDependencies`` should cover tools like ``spritesmith`` 
and PhoneGap.

### Reverse Proxy via ``nginx``

Review our ``nginx`` reverse proxy file:

    $ sudo apt-get install nginx # lives at /etc/nginx/sites-enabled/default

For running our Node.js server:

    $ sudo npm install pm2 -g
    $ pm2 list
    $ pm2 start /var/www/html/server/base.js

For ``bower`` and other tools:

    $ npm install -g bower
    $ npm install -g grunt-cli

### PhoneGap

Our application lives here: https://build.phonegap.com/apps/1247822/plugins

## The Needful

    $ grunt setup
    $ grunt build

Have fun !
